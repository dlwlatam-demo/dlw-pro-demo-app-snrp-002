import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { INavbarData, Perfil } from '../interfaces/sidenav.interface';
import { MenuOpciones } from '../models/auth/Menu.model';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  private baseURL: string = `${environment.apiUrlMtoSeguridad}/sarfsunarp/api/v1`;

  constructor( private http: HttpClient ) { }

  getMenuOptions( perfil: string ): Observable<any> {
    return this.http.get( `${ this.baseURL }/mtoseguridad/perfilesOpciones/menu/${ perfil }` );
  }

  menu: INavbarData[] = [
    {
      label: 'MANTENIMIENTO',
      codigo: MenuOpciones.MANTENIMIENTOS,
      icon: 'build',
      items: [
        {
          label: 'Configuración',
          codigo: MenuOpciones.CONFIGURACION,
          items: [
            { 
              label: 'Configurar Documentos', 
              codigo: MenuOpciones.Configuracion_de_Documentos,
              routerLink: '/SARF/mantenimientos/configuracion/configurar_documentos/bandeja'
            },
            {
              label: 'Configurar Logos de Documento',
              codigo: MenuOpciones.Configuracion_Logo_Documento,
              routerLink: '/SARF/mantenimientos/configuracion/configurar_logo_documento/bandeja'
            },
            { 
              label: 'Tipo de Carta Orden',
              codigo: MenuOpciones.Tipo_de_Carta_Orden,
              routerLink: '/SARF/mantenimientos/configuracion/tipo_de_carta_orden/bandeja' 
            },
            { 
              label: 'Tipo de Memorándum',
              codigo: MenuOpciones.Tipo_de_Memorandum,
              routerLink: '/SARF/mantenimientos/configuracion/tipo_memorandum/bandeja' 
            },
            { 
              label: 'Firma Digital',
              codigo: MenuOpciones.Configuracion_Firma_Digital,
              routerLink: '/SARF/mantenimientos/configuracion/firma_digital' 
            }
          ]
        },
        {
          label: 'Maestras',
          codigo: MenuOpciones.MAESTRAS,
          items: [
            { 
              label: 'Cuentas Bancarias',
              codigo: MenuOpciones.Cuentas_Bancarias,
              routerLink: '/SARF/mantenimientos/maestras/cuentas_bancarias/bandeja' 
            },
            { 
              label: 'Plan de Cuentas',
              codigo: MenuOpciones.Plan_de_Cuentas,
              routerLink: '/SARF/mantenimientos/maestras/plan_de_cuentas/bandeja' 
            },
            { 
              label: 'Tasas de Distribución',
              codigo: MenuOpciones.Tasas_de_Distribucion,
              routerLink: '/SARF/mantenimientos/maestras/tasas_de_distribucion/bandeja' 
            },
            {
              label: 'Clasificadores',
              codigo: MenuOpciones.Clasificadores,
              routerLink: '/SARF/mantenimientos/maestras/clasificadores/bandeja'
            },
            { 
              label: 'Información para Distribución',
              codigo: MenuOpciones.Informacion_para_Distribucion,
              routerLink: '/SARF/mantenimientos/maestras/informacion_para_distribucion/bandeja' 
            }
          ]
        },
        {
          label: 'Documentos',
          codigo: MenuOpciones.DOCUMENTOS,
          items: [
            { 
              label: 'Constancia de Abono',
              codigo: MenuOpciones.Constancia_de_Abono,
              routerLink: '/SARF/mantenimientos/documentos/constancia_de_abono/bandeja' 
            },
            { 
              label: 'Recibo de Ingresos',
              codigo: MenuOpciones.Recibo_de_Ingresos,
              routerLink: '/SARF/mantenimientos/documentos/recibos_de_ingreso/bandeja' 
            },
            { 
              label: 'Comprobante de Pago',
              codigo: MenuOpciones.Comprobante_de_Pago,
              routerLink: '/SARF/mantenimientos/documentos/comprobantes_de_pago/bandeja' 
            },
            {
              label: 'Carta Orden',
              codigo: MenuOpciones.Carta_Orden,
              routerLink: '/SARF/mantenimientos/documentos/carta_orden/bandeja' 
            },
            { 
              label: 'Memorándum',
              codigo: MenuOpciones.Memoramdun,
              routerLink: '/SARF/mantenimientos/documentos/memorandum/bandeja' 
            },
            { 
              label: 'Cheques',
              codigo: MenuOpciones.Cheques,
              routerLink: '/SARF/mantenimientos/documentos/cheques' 
            },
            { 
              label: 'Documentos para Firmar',
              codigo: MenuOpciones.Documentos_para_firmar,
              routerLink: '/SARF/mantenimientos/documentos/documento_para_firma/bandeja' 
            },
            {
              label: 'Documentos Varios Solicitar Firma',
              codigo: MenuOpciones.Documentos_Varios_Solicitar_Firma,
              routerLink: '/SARF/mantenimientos/documentos/documentos_varios_solicitar_firma/bandeja' 
            },
            {
              label: 'Enviar Documentos',
              codigo: MenuOpciones.Enviar_Documentos,
              routerLink: '/SARF/mantenimientos/documentos/documentos_enviar/bandeja' 
            }
          ]
        },
      ]
    },
    {
      label: 'PROCESOS',
      codigo: MenuOpciones.PROCESOS,
      icon: 'list_alt',
      items: [
        {
          label: 'Cerrar/Abrir Mes',
          codigo: MenuOpciones.CERRAR_ABRIR_MES,
          routerLink: '/SARF/procesos/cerrar_abrir_mes'
        },
        {
          label: 'Consolidación',
          codigo: MenuOpciones.CONSOLIDACION,
          items: [
            { 
              label: 'Recaudación Medios Electrónicos',
              codigo: MenuOpciones.CD_Recaudacion_Medios_Electronicos,
              routerLink: '/SARF/procesos/consolidacion/recaudacion_medios_electronicos/bandeja'
            },
            {
              label: 'Recaudación APP SUNARP, QR, SID',
              codigo: MenuOpciones.CD_Recaudacion_APP_SUNARP_QR_SID,
              routerLink: '/SARF/procesos/consolidacion/recaudacion_app_qr_sid/bandeja'
            },
            {
              label: 'Recaudación SPRL-Virtual',
              codigo: MenuOpciones.CD_Recaudacion_SPRL_Virtual,
              routerLink: '/SARF/procesos/consolidacion/recaudacion_sprl_virtual/bandeja'
            },
            {
              label: 'Recaudación Págalo.pe',
              codigo: MenuOpciones.CD_Recaudacion_Pagalo_pe,
              routerLink: '/SARF/procesos/consolidacion/recaudacion_pagalo_pe/bandeja'
            },
            {
              label: 'Recaudación MACMYPE',
              codigo: MenuOpciones.CD_Recaudacion_MACMYPE,
              routerLink: '/SARF/procesos/consolidacion/recaudacion_macmype/bandeja'
            }, 
            {
              label: 'Recaudación SCUNAC vs Informes Hermes',
              codigo: MenuOpciones.CD_Recaudacion_SCUNAC_vs_Informes_Hermes,
              routerLink: '/SARF/procesos/consolidacion/recaudacion_scunac_remesas/bandeja'
            }
          ]
        },
        {
          label: 'Consolidación Bancaria',
          codigo: MenuOpciones.CONSOLIDACION_BANCARIA,
          items: [
            {
              label: 'Recaudación Medios Electrónicos',
              codigo: MenuOpciones.CB_Recaudacion_Medios_Electronicos,
              routerLink: '/SARF/procesos/bancaria/recaudacion_pos/bandeja'
            },
            {
              label: 'Recaudación APP SUNARP, QR, SID',
              codigo: MenuOpciones.CB_Recaudacion_Medios_Electronicos,
              routerLink: '/SARF/procesos/bancaria/recaudacion_bancaria_app_qr_sid/bandeja'
            },
            {
              label: 'Recaudación Págalo.pe',
              codigo: MenuOpciones.CB_Recaudacion_Pagalo_pe,
              routerLink: '/SARF/procesos/bancaria/recaudacion_pagalo_pe/bandeja'
            },
            {
              label: 'Recaudación MACMYPE',
              codigo: MenuOpciones.CB_Recaudacion_MACMYPE,
              routerLink: '/SARF/procesos/bancaria/recaudacion_macmype/bandeja'
            },
            {
              label: 'Recaudación Transferencia vía CCI',
              codigo: MenuOpciones.CB_Recaudacion_Transferencia_via_CCI,
              routerLink: '/SARF/procesos/bancaria/recaudacion_cci_sprl/bandeja'
            },
            {
              label: 'Bancaria Depósitos Hermes',
              codigo: MenuOpciones.CB_Depositos_Hermes,
              routerLink: '/SARF/procesos/bancaria/recaudacion_hermes/bandeja'
            },
            {
              label: 'Libro Banco SIAF vs EEBB MEF',
              codigo: MenuOpciones.CB_Libro_Banco_SIAF,
              routerLink: '/SARF/procesos/bancaria/recaudacion_siaf_mef/bandeja'
            }
          ]
        }
      ]
    },
    {
      label: 'REPORTES',
      codigo: MenuOpciones.REPORTES,
      icon: 'content_paste_search',
      items: [
        {
          label: 'Comprobantes de Pago',
          codigo: MenuOpciones.COMPROBANTES_DE_PAGO,
          items: [
            {
              label: 'Recibo por Honorarios',
              codigo: MenuOpciones.Reporte_Recibo_por_Honorarios,
              routerLink: '/SARF/reportes/comprobantes_pago/recibo_por_honorarios'
            },
            {
              label: 'Por Cuenta Contable',
              codigo: MenuOpciones.Reporte_por_Cuenta_Contable,
              routerLink: '/SARF/reportes/comprobantes_pago/por_cuenta_contable'
            }
          ]
        },
        {
          label: 'Recibos de Ingreso',
          codigo: MenuOpciones.RECIBO_DE_INGRESO,
          items: [
            {
              label: 'Clasificadores',
              codigo: MenuOpciones.Reporte_Clasificadores,
              routerLink: '/SARF/reportes/recibo_ingreso/clasificadores'
            }
          ]
        },
        {
          label: 'Recaudación Medios Electrónicos',
          codigo: MenuOpciones.RECAUDACION_MEDIOS_ELECTRONICOS,
          items: [
            {
              label: 'Recaudación Diario POS',
              codigo: MenuOpciones.Reporte_de_Recaudacion_POS,
              routerLink: '/SARF/reportes/recaudacion_medios_electronicos/recaudacion_diario_pos'
            },
            {
              label: 'Consolidado de Recaudación POS',
              codigo: MenuOpciones.Reporte_Consolidado_de_Recaudacion,
              routerLink: '/SARF/reportes/recaudacion_medios_electronicos/consolidado_recaudacion_pos'
            }
          ]
        },
        {
          label: 'Recaudación APP SUNARP, QR, SID',
          codigo: MenuOpciones.RECAUDACION_APP_SUNARP,
          items: [
            {
              label: 'Recaudación APP SUNARP',
              codigo: MenuOpciones.Recaudacion_APP_SUNARP,
              routerLink: '/SARF/reportes/recaudacion_app_qr_sid/recaudacion_app_sunarp'
            },
            {
              label: 'Recaudación QR',
              codigo: MenuOpciones.Recaudacion_QR,
              routerLink: '/SARF/reportes/recaudacion_app_qr_sid/recaudacion_qr'
            },
            {
              label: 'Recaudación SID',
              codigo: MenuOpciones.Recaudacion_SID,
              routerLink: '/SARF/reportes/recaudacion_app_qr_sid/recaudacion_sid'
            },
            {
              label: 'Cuadro de Distribución APP SUNARP',
              codigo: MenuOpciones.Cuadro_de_Distribucion_APP_SUNARP,
              routerLink: '/SARF/reportes/recaudacion_app_qr_sid/cuadro_distribucion_app_sunarp'
            },
            {
              label: 'Cuadro de Distribución QR',
              codigo: MenuOpciones.Cuadro_de_Distribucion_QR,
              routerLink: '/SARF/reportes/recaudacion_app_qr_sid/cuadro_distribucion_qr'
            },
            {
              label: 'Cuadro de Distribución SID',
              codigo: MenuOpciones.Cuadro_de_Distribucion_SID,
              routerLink: '/SARF/reportes/recaudacion_app_qr_sid/cuadro_distribucion_sid'
            }
          ]
        },
        {
          label: 'Recaudación SPRL',
          codigo: MenuOpciones.RECAUDACION_SPRL,
          items: [
            {
              label: 'Recaudación SPRL',
              codigo: MenuOpciones.Reporte_de_Recaudacion_SPRL,
              routerLink: '/SARF/reportes/recaudacion_sprl/reporte_de_recaudacion'
            },
            {
              label: 'Consumo de Servicio de Publicidad SPRL',
              codigo: MenuOpciones.Consumo_Servicio_Publicidad_SPRL,
              routerLink: '/SARF/reportes/recaudacion_sprl/consumo_publicidad_sprl'
            },
            {
              label: 'Consumo de Servicio de Publicidad sin Identificación',
              codigo: MenuOpciones.Reporte_Consumo_Servicio_Publ_sin_Iden,
              routerLink: '/SARF/reportes/recaudacion_sprl/consumo_publicidad_sin_identificacion'
            },
            {
              label: 'Consumo de Servicio de Inscripción',
              codigo: MenuOpciones.Reporte_Consumo_Servicio_Inscripcion,
              routerLink: '/SARF/reportes/recaudacion_sprl/consumo_servicio_inscripcion'
            },
            {
              label: 'Distribución de Consumos SPRL',
              codigo: MenuOpciones.Reporte_Distribucion_Consumos_SPRL,
              routerLink: '/SARF/reportes/recaudacion_sprl/distribucion_consumos_sprl'
            }
          ]
        },
        {
          label: 'Recaudación Págalo.pe',
          codigo: MenuOpciones.RECAUDACION_PAGALO_PE,
          items: [
            {
              label: 'Ingresos en Cuenta Págalo.pe',
              codigo: MenuOpciones.Reporte_ingresos_en_cuenta_Pagalo_pe,
              routerLink: '/SARF/reportes/recaudacion_pagalo_pe/ingresos_cuenta_pagalo_pe'
            },
            {
              label: 'Recaudación Registrada Págalo.pe',
              codigo: MenuOpciones.Recaudacion_Registrada_Pagalo_pe,
              routerLink: '/SARF/reportes/recaudacion_pagalo_pe/recaudacion_registrada_pagalo_pe'
            }
          ]
        },
        {
          label: 'Recaudación MACMYPE',
          codigo: MenuOpciones.RECAUDACION_MACMYPE,
          items: [
            {
              label: 'Cuadro de Recaudación MACMYPE',
              codigo: MenuOpciones.Cuadro_de_Recaudacion_MACMYPE,
              routerLink: '/SARF/reportes/recaudacion_macmype/cuadro_recaudacion_macmype'
            },
            {
              label: 'Gastos MACMYPE',
              codigo: MenuOpciones.Reporte_de_Gastos_MACMYPE,
              routerLink: '/SARF/reportes/recaudacion_macmype/gastos_macmype'
            },
            {
              label: 'Conciliación de Recaudación MACMYPE',
              codigo: MenuOpciones.Conciliacion_de_Recaudacion_MACMYPE,
              routerLink: '/SARF/reportes/recaudacion_macmype/conciliacion_recaudacion_macmype'
            }
          ]
        },
        {
          label: 'Recaudación Transferencia vía CCI',
          codigo: MenuOpciones.RECAUDACION_TRANSFERENCIA_CCI,
          items: [
            {
              label: 'Recaudación por Transferencia vía CCI',
              codigo: MenuOpciones.Recaudacion_por_Transferencia_vía_CCI,
              routerLink: '/SARF/reportes/recaudacion_via_cci/recaudacion_transferencia_via_cci'
            },
            {
              label: 'Abono a la Cuenta Id del SPRL',
              codigo: MenuOpciones.Reporte_Abono_a_la_cuenta_id_del_SPRL,
              routerLink: '/SARF/reportes/recaudacion_via_cci/abono_cuenta_id_sprl'
            }
          ]
        },
        {
          label: 'Recaudación SCUNAC - CUT',
          codigo: MenuOpciones.RECAUDACION_SCUNAC_CUT,
          items: [
            {
              label: 'Recaudación Diario',
              codigo: MenuOpciones.Reporte_Recaudacion_Diario,
              routerLink: '/SARF/reportes/recaudacion_scunac_cut/recaudacion_diario'
            },
            {
              label: 'Consolidado Mensual',
              codigo: MenuOpciones.Reporte_Consolidado_Mensual,
              routerLink: '/SARF/reportes/recaudacion_scunac_cut/consolidado_mensual'
            }
          ]
        },
        {
          label: 'Tasas Registrales',
          codigo: MenuOpciones.TASAS_REGISTRALES,
          routerLink: '/SARF/reportes/tasas_registrales'
        },
        {
          label: 'Conciliación Libro Banco SIAF',
          codigo: MenuOpciones.CONCILIACION_LIBRO_BANCO_SIAF,
          routerLink: '/SARF/reportes/conciliacion_libro_banco'
        }
      ]
    },
    {
      label: 'SEGURIDAD',
      codigo: MenuOpciones.SEGURIDAD,
      icon: 'verified_user',
      items: [
        {
          label: 'Perfiles',
          codigo: MenuOpciones.PERFILES,
          routerLink: '/SARF/seguridad/perfiles/bandeja',
        },
        {
          label: 'Usuarios',
          codigo: MenuOpciones.USUARIOS,
          routerLink: '/SARF/seguridad/usuarios/bandeja',
        }
      ]
    },
    {
      label: 'Volver al Menú Principal',
      icon: 'arrow_back',
      routerLink: '/menu'
    }
  ];

  perfiles: Perfil[] = [
    {
      label: 'Administrador',
      codigo: '61'
    },
    {
      label: 'Tesorero',
      codigo: '62'
    },
    {
      label: 'Operador Ingresos',
      codigo: '63'
    },
    {
      label: 'Operador Egresos',
      codigo: '64'
    },
    {
      label: 'Contador',
      codigo: '65'
    },
    {
      label: 'Encargado de Archivo',
      codigo: '66'
    },
    {
      label: 'Responsable de Cuenta',
      codigo: '67'
    },
    {
      label: 'Responsable de Caja',
      codigo: '68'
    },
    {
      label: 'Cajero',
      codigo: '69'
    },
    {
      label: 'Coordinador de Tesorería',
      codigo: '70'
    },
    {
      label: 'Responsable Suplente de Cuenta',
      codigo: '81'
    }
  ]
}
