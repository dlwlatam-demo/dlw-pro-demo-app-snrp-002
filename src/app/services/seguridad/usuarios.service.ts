import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { 
  IUsuario, 
  IUsuarioPerfil, 
  IUsuarioLocal, 
  IPerfilUsuario,
  IUsuarioSistema
} from '../../interfaces/seguridad/usuario.interface';

import { environment } from '../../../environments/environment';
import { IResponse } from '../../interfaces/general.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private baseURL: string = `${ environment.apiUrlMtoSeguridad }/sarfsunarp/api/v1/mtoseguridad`;

  constructor( private http: HttpClient ) { }

  getUsuariosBandeja( pathBandeja: string ): Observable< IUsuario[] > {
    const value: string[] = pathBandeja.split('/');
    const params: string = `coZonaRegi=${ value['0'] }&coOficRegi=${ value['1'] }&coLocaAten=${ value['2'] }&coPerf=${ value['3'] }
                            &idUsua=${ value['4'] }&nuDocuIden=${ value['5'] }&apPateEmpl=${ value['7'] }&noEmpl=${ value['6'] }&inEstd=${ value['8'] }`;

    return this.http.get< IUsuario[] >( `${ this.baseURL }/usuarios/bandeja?${ params }` );
  }

  getUsuarioById( coUsua: number ): Observable< IUsuario > {
    return this.http.get< IUsuario >( `${ this.baseURL }/usuarios/${ coUsua }` );
  }

  getEmpleadoByDocumento( tipoDocu: string, nroDocu: string ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/empleado?tiDocuIden=${ tipoDocu }&nuDocuIden=${ nroDocu }` );
  }

  getUsuarioPerfilList( coUsua: number ): Observable< IUsuarioPerfil[] > {
    return this.http.get< IUsuarioPerfil[] >( `${ this.baseURL }/usuariosPerfiles/bandeja/${ coUsua }/${ environment.sistema }` );
  }

  getBandejaUsuarioLocales( coUsua: number ): Observable< IUsuarioLocal[] > {
    return this.http.get< IUsuarioLocal[] >( `${ this.baseURL }/usuariosLocales/bandeja/${ coUsua }` );
  }

  getUsuarioPorLocales( coUsua: number, coZonaRegi: string, coOficRegi: string ): Observable< IUsuarioLocal[] > {
    return this.http.get< IUsuarioLocal[] >( `${ this.baseURL }/usuariosLocales/${ coUsua }/${ coZonaRegi }/${ coOficRegi }` );
  }

  getUsuarioAD( usuarioAD: string ): Observable< IResponse > {
    return this.http.get< IResponse >( `${ this.baseURL }/buscarUsuarioAD/${ usuarioAD }` );
  }

  getUsuarioPorPerfil( coPerf: number ): Observable< IPerfilUsuario[] > {
    return this.http.get< IPerfilUsuario[] >( `${ this.baseURL }/usuariosPorPerfil/bandeja/${ coPerf }/${ environment.sistema }` );
  }

  getUsuariosPorSistema(coPerf: number ): Observable< IUsuarioSistema[] > {
    return this.http.get< IUsuarioSistema[] >( `${ this.baseURL }/usuariosSistema/${ coPerf }/${ environment.sistema }` );
  }

  createEmpleado( data: any ): Observable< any > {
    return this.http.post( `${ this.baseURL }/empleado`, data );
  }

  createOrUpdateUsuario( code: number, data: any ): Observable< any > {
    if ( code != 0 )
      return this.http.put( `${ this.baseURL }/usuario`, data );
    else
      return this.http.post( `${ this.baseURL }/usuario`, data );
  }

  updateUsuarioPorLocal( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/usuarioLocal`, data );
  }

  updateUsuarioPerfil( data: any ): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/usuarioPerfil`, data );
  }

  anularUsuarios( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/usuarios/anula`, data );
  }

  activarUsuarios( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/usuarios/activa`, data );
  }
}
