import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IPerfil } from '../../interfaces/seguridad/perfil.interface';
import { IDocumentoEstado } from '../../interfaces/perfiles.interface';

import { BodyAsignarEstados } from '../../models/seguridad/perfil.model';

import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PerfilesService {

  private baserURL: string = `${ environment.apiUrlMtoSeguridad }/sarfsunarp/api/v1/mtoseguridad`;
  
  constructor( private http: HttpClient ) { }

  getPerfilesBandeja( noPerfil: string, inEstd: string ): Observable< IPerfil[] > {
    return this.http.get< IPerfil[] >( `${ this.baserURL }/perfiles/bandeja?noPerfil=${ noPerfil }&inTipoPerf=&coSist=20&inEstd=${ inEstd }` );
  }

  getPerfilById( coPerf: number ): Observable< IPerfil > {
    return this.http.get< IPerfil >( `${ this.baserURL }/perfiles/${ coPerf }` );
  }

  getPerfilByOption( coPerf: number ): Observable< any > {
    return this.http.get< any >( `${ this.baserURL }/perfilesOpciones/bandeja/20/${ coPerf }` );
  }

  getPerfiles(): Observable< IPerfil[] > {
    return this.http.get< IPerfil[] >( `${ this.baserURL }/perfiles/bandeja?noPerfil=&inTipoPerf=&coSist=20&inEstd=A` );
  }

  getEstadosPorPerfil(coPerf: number, coUsuaLogi: number ): Observable< IDocumentoEstado[] > {
    return this.http.get< IDocumentoEstado[] >( `${ this.baserURL }/perfiles/lista/estado/documento?coPerf=${ coPerf }&coUsuaLogi=${ coUsuaLogi }` );
  }

  createOrUpdatePerfil( id: number, data: any ): Observable< any > {
    if ( id != 0 )
      return this.http.put< any >( `${ this.baserURL }/perfil`, data );
    else
      return this.http.post< any >( `${ this.baserURL }/perfil`, data );
  }

  anularPerfiles( data: any ): Observable< any > {
    return this.http.put( `${ this.baserURL }/perfiles/anula`, data );
  }

  activarPerfiles( data: any ): Observable< any > {
    return this.http.put( `${ this.baserURL }/perfiles/activa`, data );
  }

  asignarOpcionesPerfil( data: any ): Observable< any > {
    return this.http.put( `${ this.baserURL }/perfilOpcion`, data );
  }

  asignarEstadosPerfil( body: BodyAsignarEstados ): Observable< any > {
    return this.http.put( `${ this.baserURL }/perfiles/actualiza/estado/documento`, body );
  }

  
}
