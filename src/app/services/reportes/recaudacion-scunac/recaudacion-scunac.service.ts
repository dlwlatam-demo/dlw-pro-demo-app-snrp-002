import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionScunacService {

  private baseURL: string = `${ environment.apiUrlReportes }/sarfsunarp/api/v1/reportes`;

  constructor( private http: HttpClient ) { }

  reporteRecaudacionDiario( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/scunac/diaria/asigFinanc`, data );
  }

  reporteConsolidadoMensual( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/scunac/consolidadoAsigFinanc`, data );
  }

  timeOutConsolidadoMensual( guid: string ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/recaudacion/scunac/async/status/consolidadoAsigFinanc/${ guid }` );
  }
}
