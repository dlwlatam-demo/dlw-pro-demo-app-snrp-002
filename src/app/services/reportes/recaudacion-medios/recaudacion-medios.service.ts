import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionMediosService {

  private baseURL: string = `${ environment.apiUrlReportes }/sarfsunarp/api/v1/reportes`;

  constructor( private http: HttpClient ) { }

  reporteRecaudacionDiarioPOS( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/mediosElectronicos/diaria`, data );
  }

  reporteConsolidadoRecaudacionPOS( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/mediosElectronicos/consolidado`, data );
  }
}
