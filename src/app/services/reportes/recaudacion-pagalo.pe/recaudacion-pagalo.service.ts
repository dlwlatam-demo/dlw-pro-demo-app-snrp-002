import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionPagaloService {

  private baseURL: string = `${ environment.apiUrlReportes }/sarfsunarp/api/v1/reportes`;

  constructor( private http: HttpClient ) { }

  reporteIngresosCuentaPagalo( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/pagalope/cuenta`, data );
  }

  reporteRecaudacionRegistradaPagalo( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/pagalope/registrada`, data );
  }
}
