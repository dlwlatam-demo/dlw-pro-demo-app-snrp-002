import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReporteRecibosService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento`;
  
  constructor( private http: HttpClient ) { }

  reporteClasificador( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/documentos/listarClasifiador/ReciboIngreso`, data );
  }
}
