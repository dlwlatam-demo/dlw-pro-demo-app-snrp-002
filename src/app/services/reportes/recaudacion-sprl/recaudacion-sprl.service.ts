import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionSprlService {

  private baseURL: string = `${ environment.apiUrlReportes }/sarfsunarp/api/v1/reportes`;

  constructor( private http: HttpClient ) { }

  reporteRecaudacionSPRL( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/SPRL`, data );
  }

  reporteConsumoPublicidadSPRL( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/SPRL/consumo/publicidad`, data );
  }
  
  timeOutConsumoPublicidadSPRL( guid: string ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/recaudacion/SPRL/async/status/consumo/publicidad/${ guid }` );
  }

  reporteConsumoPublicidadSinIdent( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/SPRL/consumo/publicidad/noIdentificada`, data );
  }

  reporteConsumoServicioInscripcion( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/SPRL/consumo/inscripcion`, data );
  }

  reporteDistribucionConsumosSPRL( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/SPRL/distribucion`, data );
  }
}
