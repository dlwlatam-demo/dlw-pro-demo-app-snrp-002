import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TasasRegistralesService {

  private baseURL: string = `${ environment.apiUrlReportes }/sarfsunarp/api/v1/reportes`;

  constructor( private http: HttpClient ) { }

  reporteRecaudacionTasasRegistrales( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/tasas/registral`, data );
  }
}
