import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionMacmypeService {

  private baseURL: string = `${ environment.apiUrlReportes }/sarfsunarp/api/v1/reportes`;

  constructor( private http: HttpClient ) { }

  reporteRecaudacionCuadroMACMYPE( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/macmype/cuadro`, data );
  }

  reporteGastosMACMYPE( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/macmype/gastos`, data );
  }

  reporteConciliacionRecaudacionMACMYPE( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/macmype/conciliacion`, data );
  }
}
