import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionAppService {

  private baseURL: string = `${ environment.apiUrlReportes }/sarfsunarp/api/v1/reportes`;

  constructor( private http: HttpClient ) { }

  reporteRecaudacionAPP( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/recaudacionesApp/bandeja`, data );
  }

  reporteRecaudacionQR( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/recaudacionesQR/bandeja`, data );
  }

  reporteRecaudacionSID( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/recaudacion/recaudacionesSid/bandeja`, data );
  }

  reporteCuadroDistribucionAPP( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/distribucion/distribucionesApp/bandeja`, data );
  }

  reporteCuadroDistribucionQR( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/distribucion/distribucionesQR/bandeja`, data );
  }

  reporteCuadroDistribucionSID( data: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/distribucion/distribucionesSid/bandeja`, data );
  }
}
