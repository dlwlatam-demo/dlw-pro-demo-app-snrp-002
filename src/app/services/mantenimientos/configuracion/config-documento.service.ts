import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IConfigDocumento } from '../../../interfaces/configuracion/configurar-documento.interface';

import { ConfigurarDocumento } from '../../../models/mantenimientos/configuracion/config-documento.model';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigDocumentoService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/configuracion`;

  constructor( private http: HttpClient ) { }

  getBandejaConfiguracion( tipo: string, des: string, inRegi: string ): Observable< ConfigurarDocumento[] > {
    return this.http.get< ConfigurarDocumento[] >( `${ this.baseURL }/documentos/bandeja/${ tipo }?noTipo=${ des }&inRegi=${ inRegi }` );
  }

  getConfiguracionById( tipo: string, id: number ): Observable< IConfigDocumento > {
    return this.http.get< IConfigDocumento >( `${ this.baseURL }/documentos/${ tipo }/${ id }`);
  }

  createOrUpdateDocumento( id: number, data: ConfigurarDocumento ): Observable< any > {
    if ( id != 0 )
      return this.http.put< any >( `${ this.baseURL }/documento`, data );
    else
      return this.http.post< any >( `${ this.baseURL }/documento`, data );
  }

  anularDocumentos( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/documentos/anula`, data );
  }

  activarDocumentos( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/documentos/activa`, data );
  }
}
