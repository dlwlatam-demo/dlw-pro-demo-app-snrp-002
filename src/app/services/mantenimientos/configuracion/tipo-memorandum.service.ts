import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { TipoMemorandum } from '../../../models/mantenimientos/configuracion/tipo-memo.model';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipoMemorandumService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/configuracion`;

  constructor( private http: HttpClient ) { }

  getBandejaTipoMemorandum( des: string, inRegi: string ): Observable< TipoMemorandum[] > {
    return this.http.get< TipoMemorandum[] >( `${ this.baseURL }/memorandums/bandeja?nombre=${ des }&inRegi=${ inRegi }` );
  }

  getTipoMemorandumById( idTipo: number ): Observable< TipoMemorandum > {
    return this.http.get< TipoMemorandum >( `${ this.baseURL }/memorandums/${ idTipo }`);
  }

  createOrUpdate( id: number, data: TipoMemorandum ): Observable< any > {
    if ( id != 0 )
      return this.http.put< TipoMemorandum >( `${ this.baseURL }/memorandum`, data );
    else
      return this.http.post< TipoMemorandum >( `${ this.baseURL }/memorandum`, data );
  }

  anularTipoMemorandum( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/memorandums/anula`, data );
  }

  activarTipoMemorandum( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/memorandums/activa`, data );
  }
}
