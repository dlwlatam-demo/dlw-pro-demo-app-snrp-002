import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IResponse } from '../../../interfaces/general.interface';
import { ITipoCartaOrden, ITipoCartaById, ITipoCartaVariables } from '../../../interfaces/configuracion/tipo-carta-orden.interface';

import { BodyTipoCartaOrden } from '../../../models/mantenimientos/configuracion/tipo-carta-orden.model';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipoCartaOrdenService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/configuracion`;

  constructor( private http: HttpClient ) { }

  getBandejaTipoCarta( tipo: string, inRegi: string, coGrup: string, noDocu: string ): Observable< ITipoCartaOrden[] > {
    return this.http.get< ITipoCartaOrden[] >( `${ this.baseURL }/cartasOrdenes/bandeja?nombre=${ tipo }&inRegi=${ inRegi }&coGrup=${ coGrup }&noDocu=${ noDocu }` );
  }

  getTipoCartaById( idTipo: number ): Observable< ITipoCartaById > {
    return this.http.get< ITipoCartaById >( `${ this.baseURL }/cartasOrdenes/${ idTipo }`);
  }

  getTipoCartaVariables(): Observable< ITipoCartaVariables[] > {
    return this.http.get< ITipoCartaVariables[] >( `${ this.baseURL }/documentos/conf/003` );
  }

  createOrUpdateCartaOrden( id: number, body: BodyTipoCartaOrden ): Observable< any > {
    if ( id != 0 )
      return this.http.put< IResponse >( `${ this.baseURL }/cartaOrden`, body );
    else
      return this.http.post< IResponse >( `${ this.baseURL }/cartaOrden`, body );
  }

  anularCartasOrden( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/cartasOrdenes/anula`, data );
  }

  activarCartasOrden( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/cartasOrdenes/activa`, data );
  }
}
