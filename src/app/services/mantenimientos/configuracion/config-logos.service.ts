import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IConfigLogo } from '../../../interfaces/configuracion/configurar-logos';
import { IResponse } from '../../../interfaces/general.interface';

import { BodyConfigLogos } from '../../../models/mantenimientos/configuracion/config-logos.model';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigLogosService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/configuracion`;

  constructor( private http: HttpClient ) { }

  getLogosDocumentos( tipoDocumento: string ): Observable< IConfigLogo > {
    return this.http.get< IConfigLogo >( `${ this.baseURL }/documentos/lista/DatosImagen/${ tipoDocumento }` );
  }

  updateLogosDocumentos( body: BodyConfigLogos ): Observable< IResponse > {
    return this.http.put< IResponse >( `${ this.baseURL }/documentos/editar/imagen`, body );
  }
}
