import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IFirmaPerfil, IPerfilDocumento } from '../../../interfaces/configuracion/firma-digital.interface';

import { BodyFirmaDigital, FirmaDigital } from '../../../models/mantenimientos/configuracion/firma-digital.model';

import { environment } from '../../../../environments/environment';
import { IResponse } from 'src/app/interfaces/general.interface';

@Injectable({
  providedIn: 'root'
})
export class FirmaDigitalService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento`;

  constructor( private http: HttpClient ) { }

  getBandejaFirmaDigital( tipoDocu: string ): Observable< FirmaDigital[] > {
    return this.http.get< FirmaDigital[] >( `${ this.baseURL }/configuracion/firmasDigitales/bandeja?tiDocu=${ tipoDocu }` );
  }

  getPerfilPorFirma( idConfFirm: number ): Observable< IFirmaPerfil[] > {
    return this.http.get< IFirmaPerfil[] >( `${ this.baseURL }/documentos/documentosFirmaConf/${ idConfFirm }` );
  }

  getPerfilPorDocumento( tiDocu: string ): Observable< IPerfilDocumento[] > {
    return this.http.get< IPerfilDocumento[] >( `${ this.baseURL }/documentos/perfilesPorDocumento/${ tiDocu }` );
  }

  updateUsuarioFirmaDocumento( bodyFirmaDigital: BodyFirmaDigital ): Observable< IResponse > {
    return this.http.put< IResponse >( `${ this.baseURL }/documentos/editarUsuarioFirmaDocumento`, bodyFirmaDigital );
  }

  
}
