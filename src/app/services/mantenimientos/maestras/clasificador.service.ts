import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IClasificador } from '../../../interfaces/configuracion/clasificador.interface';
import { IResponse } from '../../../interfaces/general.interface';

import { BodyEliminateActivateClsf, BodyClasificador } from '../../../models/mantenimientos/maestras/clasificador.model';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClasificadorService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento`;

  constructor( private http: HttpClient ) { }

  getBandejaClasificadores( coClsf: string, deClsf: string, inRegi: string ): Observable<IClasificador[]> {
    const params: string = `${ coClsf }/${ deClsf }/${ inRegi }`;

    return this.http.get< IClasificador[] >( `${ this.baseURL }/clasificador/listar/${ params }` );
  }

  getClasificadorById( coClsf: string ): Observable< IClasificador[] > {
    return this.http.get< IClasificador[] >( `${ this.baseURL }/clasificador/seleccionar/${ coClsf }` );
  }

  createClasificador( body: BodyClasificador ): Observable< IResponse > {
    return this.http.post< IResponse >( `${ this.baseURL }/clasificador/crearClasificador`, body );
  }

  updateClasificador( body: BodyClasificador ): Observable< IResponse > {
    return this.http.put< IResponse >( `${ this.baseURL }/clasificador/editarClasificador`, body );
  }

  anularClasificador( body: BodyEliminateActivateClsf ): Observable< IResponse > {
    return this.http.put< IResponse >( `${ this.baseURL }/clasificador/anularClasificador`, body );
  }

  activarClasificador( body: BodyEliminateActivateClsf ): Observable< IResponse > {
    return this.http.put< IResponse >( `${ this.baseURL }/clasificador/activarClasificador`, body );
  }
}
