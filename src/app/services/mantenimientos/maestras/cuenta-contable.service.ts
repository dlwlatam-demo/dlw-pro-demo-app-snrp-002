import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { PlanCuentaContable } from '../../../models/mantenimientos/maestras/plan-cuenta.model';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CuentaContableService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/maestras`;

  constructor( private http: HttpClient ) { }

  getBandejaCuentaContable( coCuenCont: string, noCuenCont: string, inRegi: string ): Observable< PlanCuentaContable[] > {
    const params: string = `?coCuenCont=${ coCuenCont }&noCuenCont=${ noCuenCont }&inRegi=${ inRegi }`;
    
    return this.http.get< PlanCuentaContable[] >( `${ this.baseURL }/planesCuenta/bandeja/${ params }` );
  }

  getCuentaContableById( coCuenCont: string ): Observable< PlanCuentaContable > {
    return this.http.get< PlanCuentaContable >( `${ this.baseURL }/planesCuenta/${ coCuenCont }` );
  }

  createOrUpdate( code: number, data: PlanCuentaContable ): Observable< any > {
    if ( code == 0 )
      return this.http.post< PlanCuentaContable >( `${ this.baseURL }/planCuenta`, data );
    else
      return this.http.put< PlanCuentaContable >( `${ this.baseURL }/planCuenta`, data );
  }

  addCuentaContable( cuenta: PlanCuentaContable ): Observable< PlanCuentaContable > {
    return this.http.post< PlanCuentaContable >( `${ this.baseURL }/plan_cuentas`, cuenta );
  }

  updateCuentaContable( id: number, cuenta: PlanCuentaContable ): Observable< PlanCuentaContable > {
    return this.http.put< PlanCuentaContable >( `${ this.baseURL }/plan_cuentas/${ id }`, cuenta );
  }

  anularCuentaContable( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/planesCuenta/anula`, data );
  }

  activarCuentaContable( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/planesCuenta/activa`, data );
  }
}
