import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { InformacionDistribucion } from '../../../models/mantenimientos/maestras/informacion-distribucion.model';

import { environment } from '../../../../environments/environment';
import { InformacionDistribucionExport } from 'src/app/models/mantenimientos/maestras/Informacion-distribucion-export.model';

@Injectable({
  providedIn: 'root'
})
export class InformacionDistribucionService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/maestras`;

  constructor( private http: HttpClient ) { }

  /*getBandejaInfoDistribucion(datos:any): Observable< InformacionDistribucion[] > {
    return this.http.post< InformacionDistribucion[] >( `${ this.baseURL }/informacionDistribucion/exportar`, datos );
  }*/
  getBandejaInfoDistribucion(datos:any): Observable< InformacionDistribucionExport > {
    return this.http.post< InformacionDistribucionExport >( `${ this.baseURL }/informacionDistribucion/exportar`, datos );
  }

  crearInfoDistribucion(datos:any): Observable< any > {
    return this.http.post< InformacionDistribucion[] >( `${ this.baseURL }/informacionDistribucion/importar`, datos );
  }

  getBandejaInfoDistribucionFile(datos:any): Observable< InformacionDistribucionExport > {
    return this.http.post< InformacionDistribucionExport >( `${ this.baseURL }/informacionDistribucion/exportarFile`, datos );
  }
}
