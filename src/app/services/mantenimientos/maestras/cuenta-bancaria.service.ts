import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { CuentaBancaria } from '../../../models/mantenimientos/maestras/cuenta-bancaria.model';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CuentaBancariaService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/maestras`;

  constructor( private http: HttpClient ) { }

  getBandejaCuentaBancaria( idBanc: number, nuCuenBanc: string, inRegi: string ): Observable< CuentaBancaria[] > {
    const params: string = `${ idBanc }?nuCuenBanc=${ nuCuenBanc }&inRegi=${ inRegi }`

    return this.http.get< CuentaBancaria[] >( `${ this.baseURL }/cuentasBancarias/bandeja/${ params }` );
  }

  getCuentaBancariaById( idBanc: number, nuCuenBanc: string ): Observable< CuentaBancaria > {
    return this.http.get< CuentaBancaria >( `${ this.baseURL }/cuentasBancarias/${ idBanc }/${ nuCuenBanc }` );
  }

  createOrUpdateCuenta( msg: string, data: CuentaBancaria ): Observable< any > {
    if ( msg == 'crear' )
      return this.http.post< CuentaBancaria >( `${ this.baseURL }/cuentaBancaria`, data );
    else
      return this.http.put< CuentaBancaria >( `${ this.baseURL }/cuentaBancaria`, data );
  }

  anularCuentaBancaria( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/cuentasBancarias/anula`, data );
  }

  activarCuentaBancaria( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/cuentasBancarias/activa`, data );
  }
}
