import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { TasaDistribucion } from '../../../models/mantenimientos/maestras/tasas-distribucion.model';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TasasDistribucionService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/maestras`;

  constructor( private http: HttpClient ) { }

  getBandejaTasaDistribucion(deZonaRegi: string, inRegi: string): Observable< TasaDistribucion[] > {
    const params: string = `?deZonaRegi=${ deZonaRegi }&inRegi=${ inRegi }`;
    return this.http.get< TasaDistribucion[] >( `${ this.baseURL }/tasasDistribucion/bandeja${ params }` );
  }

  getTasaDistribucionById( id: string ): Observable< TasaDistribucion > {
    return this.http.get< TasaDistribucion >( `${ this.baseURL }/tasasDistribucion/${ id }`);
  }

  addTasaDistribucion( tasa: TasaDistribucion ): Observable< any > {
    return this.http.post< TasaDistribucion >( `${ this.baseURL }/tasaDistribucion`, tasa );
  }

  updateTasaDistribucion( tasa: TasaDistribucion ): Observable< any > {
    return this.http.put< TasaDistribucion >( `${ this.baseURL }/tasaDistribucion`, tasa );
  }

  anularTasaDistribucion( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/tasasDistribucion/anula`, data );
  }

  activarTasaDistribucion( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/tasasDistribucion/activa`, data );
  }
}
