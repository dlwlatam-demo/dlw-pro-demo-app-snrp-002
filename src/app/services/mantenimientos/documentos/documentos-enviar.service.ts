import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';
import { BodyAnularActivarDocumentosEnviar, BodyDocumentosEnviar, BodyEditarDocumentosEnviar, BodyGrabarFirmasDocumentosEnviar, BodyNuevoDocumentosEnviar, BodyEnviarDocumentosEnviar } from 'src/app/models/mantenimientos/documentos/documentos-enviar.models';
import { DocumentosEnviarBandeja, IDocumentosEnviar, IHistorialFirmasDocumentosEnviar } from 'src/app/interfaces/documentos-enviar.interface';
import { IDocumentosEnviarSustento } from '../../../interfaces/documentos-enviar.interface';
import { BodyDocumentosEnviarSustento } from '../../../models/mantenimientos/documentos/documentos-enviar.models';

@Injectable({
  providedIn: 'root'
})
export class DocumentoEnviarService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento`;

  constructor( private http: HttpClient ) { }

  public getBandejaDocumentoEnviar(body: BodyDocumentosEnviar): Observable< DocumentosEnviarBandeja > {
    return this.http.post< DocumentosEnviarBandeja >( `${ this.baseURL }/documentos/enviarDocumentos/bandeja`, body );
  }

  public nuevoDocumentoEnviar(body: BodyNuevoDocumentosEnviar): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/documentos/enviarDocumentos`, body );
  }

  public editarDocumentoEnviar(body: BodyEditarDocumentosEnviar): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/documentos/enviarDocumentos/editar`, body );
  }

  public anularDocumentoEnviar(body: BodyAnularActivarDocumentosEnviar): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/documentos/enviarDocumentos/anular`, body );
  }

  public activarDocumentoEnviar(body: BodyAnularActivarDocumentosEnviar): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/documentos/enviarDocumentos/activar`, body );
  }

  public enviarReenviarDocumento(title: string, body: BodyEnviarDocumentosEnviar): Observable< any > {
    if ( title == 'enviar' ) {
      return this.http.post< any >( `${ this.baseURL }/documentos/enviarDocumentos/enviar`, body );
    } else {
      return this.http.post< any >( `${ this.baseURL }/documentos/reenviarDocumentos/reenviar`, body );
    }
  }

  public grabarFirmasDocumentoEnviar(body: BodyGrabarFirmasDocumentosEnviar): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/documentos/documentoFirma`, body );
  }

  public getDocumentoEnviarById(id: string): Observable< IDocumentosEnviar > {
    return this.http.get< IDocumentosEnviar >( `${ this.baseURL }/documentos/enviarDocumentos/${id}` );
  }

  public getDocumentoEnviarSustento( idDocuVari: number ): Observable< IDocumentosEnviarSustento[] > {
    return this.http.get< IDocumentosEnviarSustento[] >( `${ this.baseURL }/documentos/enviarDocumentos/sustento/bandeja/${idDocuVari}` );
  }

  public updateDocumentoEnviarSustento( body: BodyDocumentosEnviarSustento ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/documentos/enviarDocumentos/sustento`, body );
  }
}