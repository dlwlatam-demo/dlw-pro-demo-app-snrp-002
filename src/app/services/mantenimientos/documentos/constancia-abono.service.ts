import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { bodyMail, ConstanciaAbono, ConstanciaAbonoFiltro, historiaFirmaConstancia } from '../../../models/mantenimientos/documentos/constancia-abono.model';
import { ConstanciaAbonoBandeja } from 'src/app/models/mantenimientos/documentos/constancia-abono-bandeja.model';
import { ResponsePdf } from 'src/app/models/mantenimientos/documentos/response-pdf.model';
import { ConstanciaAbonoSustento } from 'src/app/models/mantenimientos/documentos/constancia-abono-sustento.model';

import { DocumentoPdf } from 'src/app/models/mantenimientos/documentos/documento-pdf.model';
@Injectable({
  providedIn: 'root'
})
export class ConstanciaAbonoService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/documentos`;
  private baseURLParam: string = `${ environment.apiUrlParametros }/sarfsunarp/api/v1/parametros`;

  constructor( private http: HttpClient ) { }

  /*getBandejaConstanciaAbono(datos:any): Observable< ConstanciaAbono[] > {
    return this.http.post< ConstanciaAbono[] >( `${ this.baseURL }/constanciasAbono/bandeja`,datos );
  }*/

  getBandejaConstanciaAbono(datos:any): Observable< ConstanciaAbonoBandeja > {
    return this.http.post< ConstanciaAbonoBandeja >( `${ this.baseURL }/constanciasAbono/bandeja`,datos );
  }

  getConstanciaAbonoById( id: number ): Observable< ConstanciaAbono > {
    return this.http.get< ConstanciaAbono >( `${ this.baseURL }/constanciasAbono/${ id }` );
  }
  
  getConstanciaAbonoPdfById( id: number ): Observable< DocumentoPdf > {
    return this.http.get< DocumentoPdf >( `${ this.baseURL }/constanciaAbono/pdf/${ id }` );
  }

  getDocumentoFirmaHistoricoById( id: number ): Observable< historiaFirmaConstancia[] > {
    return this.http.get< historiaFirmaConstancia[] >( `${ this.baseURL }/documentoFirmaHistorico/bandeja/${ id }` );
  }

   // /sarfsunarp/api/v1/mantenimiento/documentos/documentoFirmaHistorico/bandeja/{idDocuFirm}
  saveBandejaConstanciaAbonoSustento(datos:any): Observable< ConstanciaAbono[] > {
    return this.http.put< ConstanciaAbono[] >( `${ this.baseURL }/constanciaAbonoSustento`,datos );
  }
  
  getBandejaConstanciaAbonoSustento( idConsAbon: number ): Observable< ConstanciaAbonoSustento[] > {
    return this.http.get< ConstanciaAbonoSustento[] >( `${ this.baseURL }/constanciasAbonoSustento/bandeja/${idConsAbon}` );
  }

  enviarEmail(datos:bodyMail): Observable< any > {
    return this.http.post< any >( `${ this.baseURLParam }/mail`,datos );
  }

  getConstanciaAbonoPdf( id: number ): Observable< ResponsePdf > {
    return this.http.get< ResponsePdf >( `${ this.baseURL }/constanciaAbono/pdf/${ id }` );
  }

}
