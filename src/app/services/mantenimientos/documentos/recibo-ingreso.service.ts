import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { historiaFirmaReciboIngreso, ReciboIngreso } from 'src/app/models/mantenimientos/documentos/recibo-ingreso.model';
import { CuentaBancaria } from 'src/app/models/mantenimientos/maestras/cuenta-bancaria.model';
import { ReciboIngresoBandeja } from 'src/app/models/mantenimientos/documentos/recibo-ingreso-bandeja.model';
import { DocumentoPdf } from 'src/app/models/mantenimientos/documentos/documento-pdf.model';
import { IListBeneRecibo, IReciboSustento } from '../../../interfaces/documentos/recibo-ingreso.interface';
import { BodyEstadoReciboIngreso, BodyValidarRecibo, BodyNombreRecibo } from '../../../models/mantenimientos/documentos/recibo-ingreso.model';
import { IResponse } from 'src/app/interfaces/general.interface';
import { ImagenesEnviarDocumento } from '../../../models/archivo-adjunto.model';


@Injectable({
    providedIn: 'root'
  })
  export class ReciboIngresoService {
    private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/documentos`;
    private baseURL_1: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/configuracion/documentos`;
    private baseURL_2: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/maestras`;

    constructor( private http: HttpClient ) { }

    createOrUpdateCuenta( msg: string, data: ReciboIngreso ): Observable< ReciboIngreso > {
      if ( msg == 'crear' )
        return this.http.post< ReciboIngreso >( `${ this.baseURL }/reciboIngreso`, data );
      else
        return this.http.put< ReciboIngreso >( `${ this.baseURL }/reciboIngreso`, data );
    }
    
    getBandejaReciboIngreso(datos:any): Observable< ReciboIngresoBandeja > {
        return this.http.post< ReciboIngresoBandeja >( `${ this.baseURL }/recibosIngreso/bandeja`,datos );
    }

    getTipoReciboIngreso (value: string): Observable< ReciboIngreso[] > {
      return this.http.get< ReciboIngreso[] >( `${ this.baseURL_1 }/bandeja/${value}?noTipo=&inRegi=A` );

    }
    
    getDocumentoFirmaHistoricoById( id: number ): Observable< historiaFirmaReciboIngreso[] > {
      return this.http.get< historiaFirmaReciboIngreso[] >( `${ this.baseURL }/documentoFirmaHistorico/bandeja/${ id }` );
    }

    getReciboIngresoById( id: number ): Observable< ReciboIngreso > {
        return this.http.get< ReciboIngreso >( `${ this.baseURL }/recibosIngreso/${ id }` );
    }
	
	  getReciboIngresoPdfById( id: number ): Observable< DocumentoPdf > {
        return this.http.get< DocumentoPdf >( `${ this.baseURL }/reciboIngreso/pdf/${ id }` );
    }

    getRecibosSustentos( id: number ): Observable< IReciboSustento[] > {
      return this.http.get< IReciboSustento[] >( `${ this.baseURL }/recibosIngresoSustento/bandeja/${ id }`)
    }

    getCuentaBancariaById( idBanc: number, nuCuenBanc: string ): Observable< CuentaBancaria > {
      return this.http.get< CuentaBancaria >( `${ this.baseURL_2 }/cuentasBancarias/${ idBanc }/${ nuCuenBanc }` );
    }

    getIdImagenesEnviarDocumentos( idDocuEnvi: number ): Observable<ImagenesEnviarDocumento[]> {
      return this.http.get<ImagenesEnviarDocumento[]>( `${ this.baseURL }/enviarDocumentos/lista/idImagenes/${ idDocuEnvi }` );
    }

    getObtenerNombresRecibo( body: BodyNombreRecibo ): Observable< IListBeneRecibo > {
      return this.http.post< IListBeneRecibo >( `${ this.baseURL }/recibosIngresoBene/bandeja`, body );
    }
	
    anularReciboIngreso( datos:any ): Observable< ReciboIngreso > {
      return this.http.put< ReciboIngreso >( `${ this.baseURL }/recibosIngreso/anula`,datos  );
    }

    activarReciboIngreso( datos:any ): Observable< ReciboIngreso > {
      return this.http.put< ReciboIngreso >( `${ this.baseURL }/recibosIngreso/activa`,datos  );
    }

    cambiarEstadoReciboIngreso( body: BodyEstadoReciboIngreso ): Observable< ReciboIngreso > {
      return this.http.put< ReciboIngreso >( `${ this.baseURL }/reciboIngreso/lista/actualizar`, body );
    }

    copiarReciboIngreso( datos:any ): Observable< ReciboIngreso > {
      return this.http.put< ReciboIngreso >( `${ this.baseURL }/reciboIngreso/copi`,datos  );
    }

    resrvarReciboIngreso( datos:any ): Observable< ReciboIngreso > {
      return this.http.put< ReciboIngreso >( `${ this.baseURL }/reciboIngreso/rese`,datos  );
    }

    noUtilizarrReciboIngreso( datos:any ): Observable< ReciboIngreso > {
      return this.http.put< ReciboIngreso >( `${ this.baseURL }/reciboIngreso/nout`,datos  );
    }

    sustentoGuardarReciboIngreso( datos:any ): Observable< ReciboIngreso > {
      return this.http.put< ReciboIngreso >( `${ this.baseURL }/reciboIngresoSustento`,datos  );
    }

    autorizarModificacionRegistros( body: BodyEstadoReciboIngreso ): Observable< IResponse > {
      return this.http.post< IResponse >( `${ this.baseURL }/reciboIngreso/masivo/registrar`, body );
    }
  
    validarAccionReciboIngreso( body: BodyValidarRecibo ): Observable< IResponse > {
      return this.http.post< IResponse >( `${ this.baseURL }/valiReciboIngreso`, body );
    }
    
  }