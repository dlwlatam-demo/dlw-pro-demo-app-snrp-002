import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { 
  IComprobantePago, 
  IComprobantePagoById, 
  IRazonSocial, 
  IComprobanteSustento 
} from 'src/app/interfaces/comprobante-pago.interface';

import { 
  BodyComprobantePago, 
  BodyCopiarComprobantePago, 
  BodyEditarComprobantePago, 
  BodyEliminarActivarComprobantePago, 
  BodyGenerarCheques, 
  BodyNuevoComprobantePago, 
  BodyReservarComprobantePago,
  BodyComprobanteSustento, 
  BodyCambiarEstadoComprobantePago,
  BodyNoUtilizadoComprobante
} from './../../../models/mantenimientos/documentos/comprobante-pago.model';
import { BodyValidarComprobante, BodyBeneficiarioBandeja, BodyTipoOperBandeja } from '../../../models/mantenimientos/documentos/comprobante-pago.model';
import { DocumentoPdf } from '../../../models/mantenimientos/documentos/documento-pdf.model';

import { environment } from '../../../../environments/environment';
import { IResponse } from '../../../interfaces/general.interface';
import { IBeneficiarioBandeja, ITipoOperacionBandeja } from '../../../interfaces/comprobante-pago.interface';

@Injectable({
  providedIn: 'root'
})
export class ComprobantePagoService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/documentos`;

  constructor( private http: HttpClient ) { }

  public getBandejaComprobantePago(body: BodyComprobantePago): Observable< IComprobantePago > {
    return this.http.post< IComprobantePago >( `${ this.baseURL }/comprobantesPago/bandeja`, body);
  }

  public createComprobantePago(body: BodyNuevoComprobantePago): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/comprobantePago`, body);
  }

  public editarComprobantePago(body: BodyEditarComprobantePago): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/comprobantePago`, body);
  }

  public anularComprobantePago(body: BodyEliminarActivarComprobantePago): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/comprobantesPago/anula`, body);
  }

  public activarComprobantePago(body: BodyEliminarActivarComprobantePago): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/comprobantesPago/activa`, body);
  }

  public cambiarEstadoReservadoNoUtilizado(body: BodyNoUtilizadoComprobante): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/comprobantePago/actualizar`, body);
  }

  public getComprobantePagoById( id: number ): Observable< IComprobantePagoById > {
    return this.http.get< IComprobantePagoById >( `${ this.baseURL }/comprobantesPago/${ id }` );
  }
  
  public getComprobantePagoPdfById( id: number ): Observable< DocumentoPdf > {
    return this.http.get< DocumentoPdf >( `${ this.baseURL }/comprobantePago/pdf/${ id }` );
  }

  public getComprobanteSustento( idCompPago: number ): Observable< IComprobanteSustento[] > {
    return this.http.get< IComprobanteSustento[] >( `${ this.baseURL }/comprobantesPagoSustento/bandeja/${ idCompPago }`)
  }

  public getComprobanteSustentoSIAF( idCompPago: number ): Observable< IComprobanteSustento[] > {
    return this.http.get< IComprobanteSustento[] >( `${ this.baseURL }/comprobantesPagoSustentoSiaf/bandeja/${ idCompPago }`)
  }

  public getHistorialFirmaComprobantes( idDocuFirm: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/documentoFirmaHistorico/bandeja/${ idDocuFirm }` );
  }

  public getBeneficiariosComprobanteBandeja( body: BodyBeneficiarioBandeja ): Observable< IBeneficiarioBandeja[] > {
    return this.http.post< IBeneficiarioBandeja[] >( `${ this.baseURL }/comprobantePago/beneficiario/bandeja`, body );
  }

  public getBeneficiariosById( body: BodyBeneficiarioBandeja ): Observable< IBeneficiarioBandeja > {
    return this.http.post< IBeneficiarioBandeja >( `${ this.baseURL }/comprobantePago/beneficiario`, body );
  }

  public getTipoOperacionBandeja( body: BodyTipoOperBandeja ): Observable< ITipoOperacionBandeja[] > {
    return this.http.post< ITipoOperacionBandeja[] >( `${ this.baseURL }/comprobantePago/tipoOperacion/bandeja`, body );
  }

  public updateComprobantePagoSustento( body: BodyComprobanteSustento ): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/comprobantePagoSustento`, body );
  }

  public copiarComprobantePago( body: BodyCopiarComprobantePago ): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/comprobantePago/copi`, body );
  }

  public reservarComprobantePago( body: BodyReservarComprobantePago ): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/comprobantePago/rese`, body );
  }

  public noUtilizadoComprobantePago( body: BodyReservarComprobantePago ): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/comprobantePago/nout`, body );
  }

  public generarCheques( body: BodyGenerarCheques ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/cheque/generar`, body );
  }

  public getRazonSocial( numeroRuc: string ): Observable< IRazonSocial > {
    return this.http.get< IRazonSocial >( `${ this.baseURL }/proveedores/${numeroRuc}` );
  }

  public autorizarModificacionRegistros( body: BodyCambiarEstadoComprobantePago ): Observable<IResponse> {
    return this.http.put< IResponse >( `${ this.baseURL }/comprobantesPago/autoriza`, body );
  }

  public validarAccionComprobantePago( body: BodyValidarComprobante ): Observable<IResponse> {
    return this.http.post< IResponse >( `${ this.baseURL }/comprobantePago/validar`, body );
  }
}
