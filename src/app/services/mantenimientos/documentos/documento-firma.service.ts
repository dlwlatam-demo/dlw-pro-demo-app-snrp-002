import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BodyDocumentoFirma, DocumentoFirmar, BodyFirmaMasivo } from '../../../models/mantenimientos/documentos/documento-firma.model';

import { environment } from '../../../../environments/environment';
import { IFirmarDocumentos, IHistorialFirmas } from 'src/app/interfaces/firmar-documentos.interface';
import { IDocumentoSustento } from '../../../interfaces/firmar-documentos.interface';
import { DocumentoPdf } from 'src/app/models/mantenimientos/documentos/documento-pdf.model';
import { IResponse } from 'src/app/interfaces/general.interface';

@Injectable({
  providedIn: 'root'
})
export class DocumentoFirmaService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/documentos`;

  constructor( private http: HttpClient ) { }

  public getBandejaDocumentoFirma(body: BodyDocumentoFirma): Observable< IFirmarDocumentos > {
    return this.http.post< IFirmarDocumentos >( `${ this.baseURL }/documentoFirma/bandeja`, body );
  }

  public rechazarDocumentoFirma(id: number): Observable< any[] > {
    return this.http.put< any[] >( `${ this.baseURL }/documentoFirma/rechazar/${ id }`, null );
  }

  public subirDocumentoFirmado( idDocuFirm: number, idGuidDocu: string, inFirmManu: string ): Observable< any > {
    return this.http.put< IResponse >( `${ this.baseURL }/documentoFirma/aprobar/${ idDocuFirm }/${ idGuidDocu }/${ inFirmManu }`, '')
  }

  public getHistoricalFirmas(id: number): Observable< IHistorialFirmas[] > {
    return this.http.get< IHistorialFirmas[] >( `${ this.baseURL }/documentoFirmaHistorico/bandeja/${ id }` );
  }

  public getDocumentoFirmaSustentoBandeja( idDocuFirm: number ): Observable< IDocumentoSustento[] > {
    return this.http.get< IDocumentoSustento[] >( `${ this.baseURL }/documentoFirmaSustento/bandeja/${ idDocuFirm }` )
  }

  getDocumentoFirmaById( id: number ): Observable< DocumentoFirmar > {
    return this.http.get< DocumentoFirmar >( `${ this.baseURL }/documentosFirma/${ id }` );
  }
  
  getDocumentoFirmaPdfById( id: number ): Observable< DocumentoPdf > {
    return this.http.get< DocumentoPdf >( `${ this.baseURL }/documentosFirma/pdf/${ id }` );
  }
  
  setDocumentoFirma( body: any ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/documentoFirma`, body );
  }

  validarDocumentoFirmaMasivo( body: BodyFirmaMasivo ): Observable< IResponse > {
    return this.http.put< IResponse >( `${ this.baseURL }/validarDocumentoFirmaMasivo`, body );
  }
  
}
