import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';
import { BodyAnularActivarDocumentosVarios, BodyDocumentosVarios, BodyEditarDocumentosVarios, BodyGrabarFirmasDocumentosVarios, BodyNuevoDocumentosVarios, BodySolicitarFirmasDocumentosVarios } from 'src/app/models/mantenimientos/documentos/documentos-varios.models';
import { DocumentosVariosBandeja, IDocumentosVarios, IHistorialFirmasDocumentosVarios } from 'src/app/interfaces/documentos-varios.interface';
import { IDocumentosVariosSustento } from '../../../interfaces/documentos-varios.interface';
import { BodyDocumentosVariosSustento } from '../../../models/mantenimientos/documentos/documentos-varios.models';

@Injectable({
  providedIn: 'root'
})
export class DocumentoVariosService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento`;

  constructor( private http: HttpClient ) { }

  public getBandejaDocumentoVarios(body: BodyDocumentosVarios): Observable< DocumentosVariosBandeja > {
    return this.http.post< DocumentosVariosBandeja >( `${ this.baseURL }/documentos/documentosVarios/bandeja`, body );
  }

  public nuevoDocumentoVarios(body: BodyNuevoDocumentosVarios): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/documentos/documentosVarios`, body );
  }

  public editarDocumentoVarios(body: BodyEditarDocumentosVarios): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/documentos/documentosVarios/editar`, body );
  }

  public anularDocumentoVarios(body: BodyAnularActivarDocumentosVarios): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/documentos/documentosVarios/anular`, body );
  }

  public activarDocumentoVarios(body: BodyAnularActivarDocumentosVarios): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/documentos/documentosVarios/activar`, body );
  }

  public solicitarFirmasDocumentoVarios(body: BodySolicitarFirmasDocumentosVarios): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/documentos/documentosVarios/solicitarFirma`, body );
  }

  public grabarFirmasDocumentoVarios(body: BodyGrabarFirmasDocumentosVarios): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/documentos/documentoFirma`, body );
  }

  public historialFirmasDocumentoVarios(id: number): Observable< IHistorialFirmasDocumentosVarios[] > {
    return this.http.get< IHistorialFirmasDocumentosVarios[] >( `${ this.baseURL }/documentos/documentoFirmaHistorico/bandeja/${id}`);
  }

  public getDocumentoVariosById(id: string): Observable< IDocumentosVarios > {
    return this.http.get< IDocumentosVarios >( `${ this.baseURL }/documentos/documentosVarios/${id}` );
  }

  public getDocumentoVariosSustento( idDocuVari: number ): Observable< IDocumentosVariosSustento[] > {
    return this.http.get< IDocumentosVariosSustento[] >( `${ this.baseURL }/documentos/documentosVarios/sustento/bandeja/${idDocuVari}` );
  }

  public updateDocumentoVariosSustento( body: BodyDocumentosVariosSustento ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/documentos/documentosVarios/sustento`, body );
  }
}