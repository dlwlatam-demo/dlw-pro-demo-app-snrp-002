import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IResponse } from 'src/app/interfaces/general.interface';
import { BodyEliminarActivarMemorandum, BodyNuevoMemorandum, BodyMemorandumSustento } from 'src/app/models/mantenimientos/documentos/memorandum.model';
import { IMemorandum } from 'src/app/interfaces/memorandum.interface';
import { DocumentoPdf } from 'src/app/models/mantenimientos/documentos/documento-pdf.model';
import { IMemorandumSustento } from 'src/app/interfaces/memorandum.interface';

@Injectable({
  providedIn: 'root'
})
export class MemorandumService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/documentos`;

  constructor( private http: HttpClient ) { }

  public getBandejaMemorandum(body: any): Observable<any> {
    return this.http.post<any>( `${ this.baseURL }/memorandums/bandeja`, body);
  }

  public getBandejaMemorandumById(idMemo: any): Observable< IMemorandum > {
    return this.http.get<IMemorandum>( `${ this.baseURL }/memorandums/${idMemo}`);
  }

  public getMemorandumPdfById(idMemo: any): Observable< DocumentoPdf > {
    return this.http.get<DocumentoPdf>( `${ this.baseURL }/memorandum/pdf/${idMemo}`);
  }

  public createMemorandum(body: BodyNuevoMemorandum): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/memorandum`, body);
  }

  public editMemorandum(body: BodyNuevoMemorandum): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/memorandum`, body);
  }

  public anulaMemorandum(body: BodyEliminarActivarMemorandum): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/memorandums/anula`, body);
  }

  public activateMemorandum(body: BodyEliminarActivarMemorandum): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/memorandums/activa`, body);
  }

  public getMemorandumSustento( idMemo: number ): Observable< IMemorandumSustento[] > {
    return this.http.get< IMemorandumSustento[] >( `${ this.baseURL }/memorandumsSustento/bandeja/${ idMemo }` );
  }

  public updateMemorandumSustento( body: BodyMemorandumSustento ): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/memorandumSustento`, body );
  }
  
}
