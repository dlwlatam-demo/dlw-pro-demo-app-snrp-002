import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICartaOrdenById, INuevaCartaOrden, ICartaSustento, ICartaAnexo } from '../../../interfaces/carta-orden.interface';

import { 
  BodyEditarCartaOrden, 
  BodyEliminarActivarCartaOrden, 
  BodyNuevaCartaOrden, 
  BodyCartaAnexo, 
  BodyCartaSustento 
} from '../../../models/mantenimientos/documentos/carta-orden.model';
import { CartaOrdenBandeja } from 'src/app/models/mantenimientos/documentos/carta-orden-bandeja.model';
import { DocumentoPdf } from 'src/app/models/mantenimientos/documentos/documento-pdf.model';

import { environment } from '../../../../environments/environment';
import { BodyCartaCorreo } from '../../../models/mantenimientos/documentos/carta-orden.model';

@Injectable({
  providedIn: 'root'
})
export class CartaOrdenService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento/documentos`;

  constructor( private http: HttpClient ) { }

  public getBandejaCartaOrden(body: any): Observable< CartaOrdenBandeja > {
    return this.http.post< CartaOrdenBandeja >( `${ this.baseURL }/cartasOrdenes/bandeja`, body);
  }

  public createCartaOrden(body: BodyNuevaCartaOrden): Observable< INuevaCartaOrden > {
    return this.http.post< INuevaCartaOrden >( `${ this.baseURL }/cartaOrden`, body);
  }

  public editarCartaOrden(body: BodyEditarCartaOrden): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/cartaOrden`, body);
  }

  public anularCartaOrden(body: BodyEliminarActivarCartaOrden): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/cartasOrdenes/anula`, body);
  }

  public activarCartaOrden(body: BodyEliminarActivarCartaOrden): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/cartasOrdenes/activa`, body);
  }

  public cartaOrdenDataById(id: string): Observable< ICartaOrdenById > {
    return this.http.get< ICartaOrdenById >( `${ this.baseURL }/cartasOrdenes/${ id }`);
  }

  public getCartaOrdenDataById(id: number): Observable< ICartaOrdenById > {
    return this.http.get< ICartaOrdenById >( `${ this.baseURL }/cartasOrdenes/${ id }`);
  }

  public cartaOrdenPdfById(id: number): Observable< DocumentoPdf > {
    return this.http.get< DocumentoPdf >( `${ this.baseURL }/cartaOrdenNew/pdf/${ id }`);
  }

  public getCartaOrdenSustento( idCartOrde: number ): Observable< ICartaSustento[] > {
    return this.http.get< ICartaSustento[] >( `${ this.baseURL }/cartasOrdenesSustento/bandeja/${ idCartOrde }`)
  }

  public updateCartaOrdenSustento( body: BodyCartaSustento ): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/cartaOrdenSustento`, body );
  }

  public getCartaOrdenAnexo( idCartOrde: number ): Observable< ICartaAnexo[] > {
    return this.http.get< ICartaAnexo[] >( `${ this.baseURL }/cartasOrdenesAnexo/bandeja/${ idCartOrde }` );
  }

  public updateCartaOrdenAnexo( body: BodyCartaAnexo ): Observable< any > {
    return this.http.put< any >( `${ this.baseURL }/cartaOrdenAnexo`, body );
  }

  public getHistorialFirmaCartaOrden( idDocuFirm: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/documentoFirmaHistorico/bandeja/${ idDocuFirm }` );
  }

  public enviarEmail( body: BodyCartaCorreo ): Observable< any > {
    return this.http.post< any >( `${ this.baseURL }/cartasOrdenes/enviarMail`, body );
  }
}
