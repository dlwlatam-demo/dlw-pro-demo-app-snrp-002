import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ChequeConsulta, ChequeRegister, ChequeOperations } from 'src/app/models/mantenimientos/documentos/cheques.model';
import { IResponse } from 'src/app/interfaces/general.interface';
import { BodyImprimirAnularDevolverCheques } from '../../../models/mantenimientos/documentos/cheques.model';

@Injectable({
  providedIn: 'root'
})
export class ChequesService {

  private baseURL: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento`;

  constructor(private http: HttpClient) { }

  public getBandejaCheque(body: ChequeConsulta): Observable<any> {
    return this.http.post<any>( `${this.baseURL}/documentos/cheques/bandeja`, body);
  }

  public createCheque(body: ChequeRegister): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/documentos/cheque`, body);
  }

  public editCheque(body: ChequeRegister): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/documentos/cheque`, body);
  }

  public anulaCheque(body: ChequeOperations): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/documentos/cheques/anula`, body);
  }

  public activateCheque(body: ChequeOperations): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/documentos/cheques/activa`, body);
  }

  public notUsedCheque(body: ChequeOperations): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/documentos/cheque/nout`, body);
  }

  public requestPrintingCheque(body: ChequeOperations): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/documentos/cheques/solicitaImpresion`, body);
  }

  public solicitarImpresionCheque( idCheque: number ): Observable<IResponse> {
    return this.http.get<IResponse>( `${ this.baseURL }/documentos/cheques/${ idCheque }` );
  }

  public impresionDeCheque( data: BodyImprimirAnularDevolverCheques ): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/documentos/cheques/impresion/masivo`, data );
  }

  public solicitarAnulacionCheque( body: BodyImprimirAnularDevolverCheques ): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/documentos/cheques/solicitaAnulacion`, body );
  }

  public solicitarDevolucionCheque( body: BodyImprimirAnularDevolverCheques ): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/documentos/cheques/solicitaDevolucion`, body );
  }
}
