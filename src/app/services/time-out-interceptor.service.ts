import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, EMPTY, Observable, throwError, timeout } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthCheckService } from './auth/auth-check.service';
import { AuthService } from './auth/auth.service';
import { SharedMessageService } from './shared-message.service';
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class TimeOutInterceptorService implements HttpInterceptor {



  constructor(private msg: SharedMessageService ,private authcheckservice: AuthCheckService, 
                private authService: AuthService, private utilService: UtilService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let token = this.authcheckservice.getToken();
    let roles: string[] = JSON.parse( localStorage.getItem('roles_sarf')! ) || ['0'];
    let perfil = localStorage.getItem('perfil_sarf') || roles['0'];

    
 
     if(!req.url.includes('/oauth/token')){

      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
          coPerf: perfil!,
        }
      });

     }
 
     
    return next.handle(req).pipe(timeout(environment.apiTimeOut), catchError((e: HttpErrorResponse) => {
      
      if (e.status == 0 || !e.status) {
        this.utilService.onCloseLoading();
        // this.utilService.onShowAlert('error', 'Atención', 'No hubo respuesta del servidor. Falla en la conexión');
        this.msg.sendMessage(e.message)
      return EMPTY;
      }
      return  throwError(() => e)
    }))
  }
}
