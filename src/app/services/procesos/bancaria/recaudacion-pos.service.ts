import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConciliacionManual, RecaudacionPos, RecaudacionPos2 } from 'src/app/models/procesos/bancaria/recaudacion-pos.model';
import { RecaudacionPosNiubiz } from 'src/app/models/procesos/bancaria/recaud-pos-niubiz.model';
import { environment } from 'src/environments/environment';
import { IPostOperacionScunac, IResConciliacionManual2, IDataTransacPos } from 'src/app/interfaces/consolidacion-pos';
import { IResponse } from 'src/app/interfaces/general.interface';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionPosService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos/consolidacion`;
  private baseURL2: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos`;

  constructor( private http: HttpClient ) { }

  public getBandejaConciliacion(data: any): Observable<any> {
    return this.http.post<any>(`${this.baseURL }/conciliaciones/bandeja`, data);
  }

  public procesarRecaudacionPos(data: RecaudacionPos): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacion`, data );
  }

  public reprocesaRecaudacionPos(body: RecaudacionPos): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacion/procesa/${body.idCncl}`, body);
  }

  public anulaRecaudacionPos(body: RecaudacionPos2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/anula`, body);
  }

  public activateRecaudacionPos(body: RecaudacionPos2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/activa`, body);
  }

  public openRecaudacionPos(body: RecaudacionPos2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/abrir`, body);
  }

  public closeRecaudacionPos(body: RecaudacionPos2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/cerrar`, body);
  }

  public getOperacionesScunac(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesScunac/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public actualizarIdOperacionScunac(body: IPostOperacionScunac): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL2 }/sarf/regDetaPago`, body );
  }  

  public getDetalleConciliarManual(idCncl: number, nuFila: number): Observable<IResConciliacionManual2> {
    return this.http.get<IResConciliacionManual2>( `${this.baseURL}/operacionesNiubiz/${idCncl}/${nuFila}`);
  }
  public saveConciliarManual(body: ConciliacionManual): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/operacionNiubiz`, body);
  }

  public getRecaudacionById( idCncl: number ): Observable< RecaudacionPos > {
    return this.http.get< RecaudacionPos >( `${ this.baseURL }/conciliaciones/${ idCncl }`);
  }

  public getRecaudacionMEF( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL2}/conciliacion/bancaria/mef/${idCncl}`, {params, observe: 'response'});
  }

  public getRecaudacionSCUNAC( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesScunacLocal/bandeja/${idCncl}`, {params, observe: 'response'});
  }


}