import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RecaudacionCciSprl, RecaudacionCciSprl2 } from 'src/app/models/procesos/bancaria/recaudacion-cci-sprl.model';
import { environment } from 'src/environments/environment';
import { IPostOperacionScunac, IDataTransacCciSprl } from 'src/app/interfaces/consolidacion-cci-sprl';
import { IResponse } from 'src/app/interfaces/general.interface';
import { ConciliacionManualMefCci } from '../../../models/procesos/bancaria/recaudacion-cci-sprl.model';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionCciSprlService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos/consolidacion`;
  private baseURL2: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos`;

  constructor( private http: HttpClient ) { }

  public getBandejaConciliacion(data: any): Observable<any> {
    return this.http.post<any>(`${this.baseURL }/conciliaciones/bandeja`, data);
  }

  public procesarRecaudacionCciSprl(data: RecaudacionCciSprl): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacion`, data );
  }

  public reprocesaRecaudacionCciSprl(body: RecaudacionCciSprl): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacion/procesa/${body.idCncl}`, body);
  }

  public anulaRecaudacionCciSprl(body: RecaudacionCciSprl2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/anula`, body);
  }

  public activateRecaudacionCciSprl(body: RecaudacionCciSprl2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/activa`, body);
  }

  public openRecaudacionCciSprl(body: RecaudacionCciSprl2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/abrir`, body);
  }

  public closeRecaudacionCciSprl(body: RecaudacionCciSprl2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/cerrar`, body);
  }

  public getOperacionesScunac(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesScunac/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public actualizarIdOperacionScunac(body: IPostOperacionScunac): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL2 }/sarf/regDetaPago`, body );
  }  


  public getRecaudacionById( idCncl: number ): Observable< RecaudacionCciSprl > {
    return this.http.get< RecaudacionCciSprl >( `${ this.baseURL }/conciliaciones/${ idCncl }`);
  }

  public getRecaudacionNiubiz( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    return this.http.get< any >( `${ this.baseURL }/operacionesNiubiz/bandeja/${ idCncl }/${ inCncl }`);
  }

  public getRecaudacionEBMEF(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL2}/conciliacion/bancaria/mef/${idCncl}`, {params, observe: 'response'});
  }

  public getArchivosTxSprl( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL2}/conciliacion/bancaria/sprl/transfCCI/${idCncl}`, {params, observe: 'response'});
  }

  public getConciliacionesSPRL( idCncl: number, nuSecu: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/conciliacionesSPRL/${ idCncl }/${ nuSecu }`);
  }

  public getBancariaTotalesCciSprl( idCncl: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL2 }/conciliacion/bancaria/totalCCI/${ idCncl }`);
  }

  public getEditarBancariaMEF( idCncl: number, nuFila: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL2 }/conciliacion/bancaria/listar/${idCncl}/${nuFila}` );
  }

  public saveEstadoBancarioCCIMef( body: ConciliacionManualMefCci ): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL2 }/conciliacion/bancaria/editar`, body )
  }

  public getEditarBancariaSPRL( idCncl: number, nuSecu: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL2 }/conciliacion/bancaria/operacionTransfCCI/${idCncl}/${nuSecu}` );
  }

  public saveEstadoBancarioCCISprl( body: any ): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL2 }/conciliacion/bancaria/actualizar/estadoOperacion`, body );
  }

}