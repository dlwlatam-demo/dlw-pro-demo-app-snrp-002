import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConciliacionManual, ConciliacionManual2, RecaudacionSiafMef, RecaudacionSiafMef2 } from 'src/app/models/procesos/bancaria/recaudacion-siaf-mef.model';
import { environment } from 'src/environments/environment';
import { IPostOperacionScunac, IResConciliacionManual2, IDataTransacSiafMef } from 'src/app/interfaces/consolidacion-siaf-mef';
import { IResponse } from 'src/app/interfaces/general.interface';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionSiafMefService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos/consolidacion`;
  private baseURL2: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos`;

  constructor( private http: HttpClient ) { }

  public getBandejaConciliacion(data: any): Observable<any> {
    return this.http.post<any>(`${this.baseURL }/conciliacionesSiaf/bandeja`, data);
  }

  public procesarRecaudacionSiafMef(data: RecaudacionSiafMef): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacionSiaf`, data );
  }

  public reprocesaRecaudacionSiafMef(body: RecaudacionSiafMef): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacionSiaf`, body);
  }

  public anulaRecaudacionSiafMef(body: RecaudacionSiafMef2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacionesSiaf/anula`, body);
  }

  public activateRecaudacionSiafMef(body: RecaudacionSiafMef2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacionesSiaf/activa`, body);
  }

  public openRecaudacionSiafMef(body: RecaudacionSiafMef2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacionesSiaf/abrir`, body);
  }

  public closeRecaudacionSiafMef(body: RecaudacionSiafMef2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacionesSiaf/cerrar`, body);
  }

  public getOperacionesScunac(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesScunac/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public actualizarIdOperacionScunac(body: IPostOperacionScunac): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL2 }/sarf/regDetaPago`, body );
  }  

  public getDetalleConciliarManual(idCncl: number, nuFila: number): Observable<IResConciliacionManual2> {
    return this.http.get<IResConciliacionManual2>( `${this.baseURL}/operacionesNiubiz/${idCncl}/${nuFila}`);
  }
  public saveConciliarManual(body: ConciliacionManual): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/operacionNiubiz`, body);
  }

  public getRecaudacionById( idCncl: number ): Observable< RecaudacionSiafMef > {
    return this.http.get< RecaudacionSiafMef >( `${ this.baseURL }/conciliaciones/${ idCncl }`);
  }

  public getLibroBancosSiaf( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/libroBancosSiaf/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public getConciliacionesSiafTotal(idCncl: number): Observable<any> {
    return this.http.get<any>( `${this.baseURL}/conciliacionesSiaf/total/${idCncl}`);
  }

  public getEstadoBancarioSiaf( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/estadoBancarioSiaf/bandeja/${idCncl}`, {params, observe: 'response'});
  }


  public getEditarLibroBancosSiaf( idCncl: number, nuSecu: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/libroBancosSiaf/${ idCncl }/${ nuSecu }`);
  }


  public getEditarEBMef( idCncl: number, nuSecu: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/estadoBancarioSiaf/${ idCncl }/${ nuSecu }`);
  }



  public saveLibroBancosSiaf(body: ConciliacionManual): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/libroBancosSiaf`, body);
  }
  
  public saveNewEstadoBancarioMef(body: ConciliacionManual2): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/estadoBancarioSiaf`, body);
  }
  
  public saveEstadoBancarioMef(body: ConciliacionManual2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/estadoBancarioSiaf`, body);
  }
}