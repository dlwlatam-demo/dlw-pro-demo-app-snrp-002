import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RecaudacionHermes, RecaudacionHermes2 } from 'src/app/models/procesos/bancaria/recaudacion-hermes.model';
import { environment } from 'src/environments/environment';
import { IPostOperacionScunac, IDataTransacHermes } from 'src/app/interfaces/consolidacion-hermes';
import { IResponse } from 'src/app/interfaces/general.interface';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionHermesService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos/consolidacion`;
  private baseURL2: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos`;

  constructor( private http: HttpClient ) { }

  public getBandejaConciliacion(data: any): Observable<any> {
    return this.http.post<any>(`${this.baseURL }/conciliacionesSiaf/bandeja`, data);
  }

  public procesarRecaudacionHermes(data: RecaudacionHermes): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacionSiaf`, data );
  }

  public reprocesaRecaudacionHermes(body: RecaudacionHermes): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacionSiaf`, body);
  }

  public anulaRecaudacionHermes(body: RecaudacionHermes2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacionesSiaf/anula`, body);
  }

  public activateRecaudacionHermes(body: RecaudacionHermes2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacionesSiaf/activa`, body);
  }

  public openRecaudacionHermes(body: RecaudacionHermes2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacionesSiaf/abrir`, body);
  }

  public closeRecaudacionHermes(body: RecaudacionHermes2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacionesSiaf/cerrar`, body);
  }

  public getOperacionesScunac(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesScunac/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public actualizarIdOperacionScunac(body: IPostOperacionScunac): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL2 }/sarf/regDetaPago`, body );
  }  


  public getRecaudacionById( idCncl: number ): Observable< RecaudacionHermes > {
    return this.http.get< RecaudacionHermes >( `${ this.baseURL }/conciliaciones/${ idCncl }`);
  }

  public getRecaudacionNiubiz( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    return this.http.get< any >( `${ this.baseURL }/operacionesNiubiz/bandeja/${ idCncl }/${ inCncl }`);
  }

  public getRecaudacionHermes(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/libroBancosSiaf/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public getArchivosMef( idCnclSiaf: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/estadoBancarioSiaf/bandeja/${idCnclSiaf}`, {params, observe: 'response'});
  }

  public getConciliacionesSPRL( idCncl: number, nuSecu: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/conciliacionesSPRL/${ idCncl }/${ nuSecu }`);
  }

  public getBancariaTotalesHermes( idCncl: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL2 }/conciliacion/bancaria/total/${ idCncl }`);
  }


}