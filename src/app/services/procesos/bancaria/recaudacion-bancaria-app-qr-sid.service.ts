import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IResponse } from '../../../interfaces/general.interface';
import { IPostOperacionScunac } from '../../../interfaces/consolidacion-app-qr-sid';

import { TramaBancariaAppQrSid, BancariaAppQrSid } from '../../../models/procesos/bancaria/recaudacion-bancaria-app-qr-sid.model';

import { environment } from '../../../../environments/environment';
import { RecaudacionMediosElectronicosNiubiz } from '../../../models/procesos/consolidacion/recaud-medios-electronicos-niubiz.model';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionBancariaAppQrSidService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos`;

  constructor( private http: HttpClient ) { }

  public getRecaudacionMEF( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${ this.baseURL }/conciliacion/bancaria/mef/${idCncl}`, {params, observe: 'response'});
  }

  public getRecaudacionNiubiz( idCncl: number, inCncl: number ): Observable< any > {
    return this.http.get< RecaudacionMediosElectronicosNiubiz[] >( `${ this.baseURL }/consolidacion/operacionesNiubiz/bandeja/${ idCncl }/${ inCncl }`);
  }

  public getBandejaConciliacion( data: any ): Observable<any> {
    return this.http.post<any>(`${ this.baseURL }/consolidacion/conciliaciones/bandeja`, data);
  }

  public anulaBancariaAppQrSid(body: TramaBancariaAppQrSid): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/consolidacion/conciliaciones/anula`, body);
  }

  public activateBancariaAppQrSid(body: TramaBancariaAppQrSid): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/consolidacion/conciliaciones/activa`, body);
  }

  public openBancariaAppQrSid(body: TramaBancariaAppQrSid): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/consolidacion/conciliaciones/abrir`, body);
  }

  public closeBancariaAppQrSid(body: TramaBancariaAppQrSid): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/consolidacion/conciliaciones/cerrar`, body);
  }

  public getOperacionesScunac(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${ this.baseURL }/consolidacion/operacionesScunac/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public actualizarIdOperacionScunac(body: IPostOperacionScunac): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/sarf/regDetaPago`, body );
  }

  public procesarBancariaAppQrSid(data: BancariaAppQrSid): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/consolidacion/conciliacion`, data );
  }

  public reprocesaBancariaAppQrSid(body: BancariaAppQrSid): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/consolidacion/conciliacion/procesa/${body.idCncl}`, body);
  }
}
