import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CerrarAbrirMesService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos`;

  constructor( private http: HttpClient ) { }

  getRegistrosPorMes( nuAno: string, nuMes: string ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/mensual/cierresMensuales/bandeja/${ nuAno }/${ nuMes }` );
  }

  cerrarAbrirMes( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/mensual/cierreMensual`, data );
  }

  getCbo_Anios( anio: number ) {
    const arrAnio: any[] = [
      { 'deAnio': anio - 5 },
      { 'deAnio': anio - 4 },
      { 'deAnio': anio - 3 },
      { 'deAnio': anio - 2 },
      { 'deAnio': anio - 1 },
      { 'deAnio': anio },
      { 'deAnio': anio + 1 }
    ];

    return arrAnio;
  }
}
