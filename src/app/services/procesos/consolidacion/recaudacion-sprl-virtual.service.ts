import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConciliacionManual, RecaudacionSprl, RecaudacionSprl2 } from 'src/app/models/procesos/consolidacion/recaudacion-sprl-virtual.model';
import { RecaudacionSprlNiubiz } from 'src/app/models/procesos/consolidacion/recaud-sprl-virtual-niubiz.model';
import { environment } from 'src/environments/environment';
import { IPostOperacionScunac, IResConciliacionManual2, IDataTransacSprl } from 'src/app/interfaces/consolidacion-sprl-virtual';
import { IResponse } from 'src/app/interfaces/general.interface';
import { ConciliacionNIUBIZ } from '../../../models/procesos/consolidacion/recaudacion-sprl-virtual.model';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionSprlService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos/consolidacion`;
  private baseURL2: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos`;

  constructor( private http: HttpClient ) { }

  public getBandejaConciliacion(data: any): Observable<any> {
    return this.http.post<any>(`${this.baseURL }/conciliaciones/bandeja`, data);
  }

  public procesarRecaudacionSprl(data: RecaudacionSprl): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacion`, data );
  }

  public reprocesaRecaudacionSprl(body: RecaudacionSprl): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacion/procesa/${body.idCncl}`, body);
  }

  public anulaRecaudacionSprl(body: RecaudacionSprl2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/anula`, body);
  }

  public activateRecaudacionSprl(body: RecaudacionSprl2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/activa`, body);
  }

  public openRecaudacionSprl(body: RecaudacionSprl2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/abrir`, body);
  }

  public closeRecaudacionSprl(body: RecaudacionSprl2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/cerrar`, body);
  }

  public getOperacionesScunac(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesScunac/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public actualizarIdOperacionScunac(body: IPostOperacionScunac): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL2 }/sarf/regDetaPago`, body );
  }  

  public getDetalleConciliarManual(idCncl: number, nuFila: number): Observable<IResConciliacionManual2> {
    return this.http.get<IResConciliacionManual2>( `${this.baseURL}/operacionesNiubiz/${idCncl}/${nuFila}`);
  }
  public saveConciliarManual(body: ConciliacionManual): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/operacionNiubiz`, body);
  }

  public getRecaudacionById( idCncl: number ): Observable< RecaudacionSprl > {
    return this.http.get< RecaudacionSprl >( `${ this.baseURL }/conciliaciones/${ idCncl }`);
  }

  public getRecaudacionNiubiz( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    return this.http.get< any >( `${ this.baseURL }/operacionesNiubiz/bandeja/${ idCncl }/${ inCncl }`);
  }

  public getOperacionesTotalNiubiz(idCncl: number): Observable<HttpResponse<any>> {
    return this.http.get<any>( `${this.baseURL}/operacionesNiubizTipoOper/bandeja/${idCncl}`);
  }

  public getTransaccionesSPRL( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/conciliacionesSPRL/bandeja/${idCncl}`, {params, observe: 'response'});
  }


  public getConciliacionesSPRL( idCncl: number, nuSecu: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/conciliacionesSPRL/${ idCncl }/${ nuSecu }`);
  }

  public getConciliacionesNIUBIZ( idCncl: number, nuFila: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/operacionesNiubiz/${ idCncl }/${ nuFila }`);
  }

  public saveConciliarSPRL(body: ConciliacionManual): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacionesSPRL/editar`, body);
  }

  public saveConciliarNIUBIZ(body: ConciliacionNIUBIZ): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/operacionNiubiz`, body);
  }
}