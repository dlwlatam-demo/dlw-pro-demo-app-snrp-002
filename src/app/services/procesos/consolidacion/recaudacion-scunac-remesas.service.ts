import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConciliacionManual, RecaudacionScunacRemesas, RecaudacionScunacRemesas2 } from 'src/app/models/procesos/consolidacion/recaudacion-scunac-remesas.model';
import { environment } from 'src/environments/environment';
import { 
  IPostOperacionScunac,
  IResConciliacionManual2,
  IListOtrosHermes 
} from 'src/app/interfaces/consolidacion-scunac-remesas';
import { IResponse } from 'src/app/interfaces/general.interface';
import { BodyOtrosHermes, BodyAnularActivarOtrosHermes } from '../../../models/procesos/consolidacion/recaudacion-scunac-remesas.model';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionScunacRemesasService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos/consolidacion`;
  private baseURL2: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos`;

  constructor( private http: HttpClient ) { }

  public getBandejaConciliacion(data: any): Observable<any> {
    return this.http.post<any>(`${this.baseURL }/conciliaciones/bandeja`, data);
  }

  public procesarRecaudacionScunacRemesas(data: RecaudacionScunacRemesas): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacion`, data );
  }

  public reprocesaRecaudacionScunacRemesas(body: RecaudacionScunacRemesas): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacion/procesa/${body.idCncl}`, body);
  }

  public anulaRecaudacionScunacRemesas(body: RecaudacionScunacRemesas2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/anula`, body);
  }

  public activateRecaudacionScunacRemesas(body: RecaudacionScunacRemesas2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/activa`, body);
  }

  public openRecaudacionScunacRemesas(body: RecaudacionScunacRemesas2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/abrir`, body);
  }

  public closeRecaudacionScunacRemesas(body: RecaudacionScunacRemesas2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/cerrar`, body);
  }

  public getOperacionesScunac(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesScunac/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public actualizarIdOperacionScunac(body: IPostOperacionScunac): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL2 }/sarf/regDetaPago`, body );
  }  

  public getDetalleConciliarManual(idCncl: number, nuFila: number): Observable<IResConciliacionManual2> {
    return this.http.get<IResConciliacionManual2>( `${this.baseURL}/operacionesNiubiz/${idCncl}/${nuFila}`);
  }
  public saveConciliarManual(body: ConciliacionManual): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/operacionNiubiz`, body);
  }

  public getRecaudacionById( idCncl: number ): Observable< RecaudacionScunacRemesas > {
    return this.http.get< RecaudacionScunacRemesas >( `${ this.baseURL }/conciliaciones/${ idCncl }`);
  }

  public getRecaudacionScunac( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesScunac/bandeja/${idCncl}`, {params, observe: 'response'});
  }


  public getTransaccionesHermes( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesHermes/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public getOtrasOperacionesHERMES( idCncl: number ): Observable<IListOtrosHermes> {
    return this.http.get< IListOtrosHermes >( `${ this.baseURL }/operacionesScunacOtro/bandeja/${ idCncl }` );
  }

  public saveOrUpdateOperacionesHERMES( body: BodyOtrosHermes, code: number ): Observable<IResponse> {
    if ( code === 0 ) {
      return this.http.post<IResponse>( `${ this.baseURL }/operacionesScunacLocal`, body );
    } else {
      return this.http.put<IResponse>( `${ this.baseURL }/operacionesScunacLocal`, body );
    }
  }

  public anularOtrasOperacionesHERMES( body: BodyAnularActivarOtrosHermes ): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/operacionesScunacLocal/anular`, body );
  }

  public activarOtrasOperacionesHERMES( body: BodyAnularActivarOtrosHermes ): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/operacionesScunacLocal/activar`, body );
  }

  public reprocesarOtrasOperacionesHERMES( idCncl: number ): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacion/procesarConciliacion/${ idCncl }`, '' );
  }

  public getConciliacionesSPRL( idCncl: number, nuSecu: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/conciliacionesSPRL/${ idCncl }/${ nuSecu }`);
  }

  public saveConciliarSPRL(body: ConciliacionManual): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacionesSPRL/editar`, body);
  }
}