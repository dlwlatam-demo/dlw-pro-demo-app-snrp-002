import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConciliacionManual, RecaudacionAppQrSid, RecaudacionAppQrSid2 } from 'src/app/models/procesos/consolidacion/recaudacion-app-qr-sid.model';
import { environment } from 'src/environments/environment';
import { IPostOperacionScunac, IResConciliacionManual2, IDataTransacAppQrSid } from 'src/app/interfaces/consolidacion-app-qr-sid';
import { IResponse } from 'src/app/interfaces/general.interface';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionAppQrSidService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos/consolidacion`;
  private baseURL2: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos`;

  constructor( private http: HttpClient ) { }

  public getBandejaConciliacion(data: any): Observable<any> {
    return this.http.post<any>(`${this.baseURL }/conciliaciones/bandeja`, data);
  }

  public procesarRecaudacionAppQrSid(data: RecaudacionAppQrSid): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacion`, data );
  }

  public reprocesaRecaudacionAppQrSid(body: RecaudacionAppQrSid): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacion/procesa/${body.idCncl}`, body);
  }

  public anulaRecaudacionAppQrSid(body: RecaudacionAppQrSid2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/anula`, body);
  }

  public activateRecaudacionAppQrSid(body: RecaudacionAppQrSid2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/activa`, body);
  }

  public openRecaudacionAppQrSid(body: RecaudacionAppQrSid2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/abrir`, body);
  }

  public closeRecaudacionAppQrSid(body: RecaudacionAppQrSid2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/cerrar`, body);
  }

  public getOperacionesScunac(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesScunac/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public actualizarIdOperacionScunac(body: IPostOperacionScunac): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL2 }/sarf/regDetaPago`, body );
  }  

  public getDetalleConciliarManual(idCncl: number, nuFila: number): Observable<IResConciliacionManual2> {
    return this.http.get<IResConciliacionManual2>( `${this.baseURL}/operacionesNiubiz/${idCncl}/${nuFila}`);
  }
  public saveConciliarManual(body: ConciliacionManual): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/operacionNiubiz`, body);
  }

  public getRecaudacionById( idCncl: number ): Observable< RecaudacionAppQrSid > {
    return this.http.get< RecaudacionAppQrSid >( `${ this.baseURL }/conciliaciones/${ idCncl }`);
  }

  public getRecaudacionNiubiz( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    return this.http.get< any >( `${ this.baseURL }/operacionesNiubiz/bandeja/${ idCncl }/${ inCncl }`);
  }

  public getOperacionesTotalNiubiz(idCncl: number): Observable<HttpResponse<any>> {
    return this.http.get<any>( `${this.baseURL}/operacionesNiubizTipoOper/bandeja/${idCncl}`);
  }

  public getVentasConciliadasAPP(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/conciliacionesApp/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public getVentasConciliadasQR(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/conciliacionesQR/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public getVentasConciliadasSID(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/conciliacionesSid/bandeja/${idCncl}`, {params, observe: 'response'});
  }




  public getTransaccionesSPRL( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/conciliacionesSPRL/bandeja/${idCncl}`, {params, observe: 'response'});
  }


  public getConciliacionesSPRL( idCncl: number, nuSecu: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/conciliacionesSPRL/${ idCncl }/${ nuSecu }`);
  }

  public saveConciliarSPRL(body: ConciliacionManual): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacionesSPRL/editar`, body);
  }
}