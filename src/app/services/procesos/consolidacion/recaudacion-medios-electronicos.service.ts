import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConciliacionMovimiento, ConciliacionPorMonto, RecaudacionMediosElectronicos, RecaudacionMediosElectronicos2 } from 'src/app/models/procesos/consolidacion/recaudacion-medios-electronicos.model';
import { RecaudacionMediosElectronicosNiubiz } from 'src/app/models/procesos/consolidacion/recaud-medios-electronicos-niubiz.model';
import { RecaudacionMediosElectronicosDiners } from 'src/app/models/procesos/consolidacion/recaud-medios-electronicos-diners.model';
import { RecaudacionMediosElectronicosAmex } from 'src/app/models/procesos/consolidacion/recaud-medios-electronicos-amex.model';
import { environment } from 'src/environments/environment';
import { IConciliacionPorMonto3, IPostOperacionScunac, IResConciliacionPorMonto2, ITotalPorOficina, IVariosEfectivoAnulacion } from 'src/app/interfaces/consolidacion';
import { IResponse } from 'src/app/interfaces/general.interface';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionMediosElectronicosService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos/consolidacion`;
  private baseURL2: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos`;

  constructor( private http: HttpClient ) { }

  public getBandejaConciliacion(data: any): Observable<any> {
    return this.http.post<any>(`${this.baseURL }/conciliaciones/bandeja`, data);
  }

  public procesarRecaudacionME(data: RecaudacionMediosElectronicos): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacion`, data );
  }

  public reprocesaRecaudacionME(body: RecaudacionMediosElectronicos): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacion/procesa/${body.idCncl}`, body);
  }

  public anulaRecaudacionME(body: RecaudacionMediosElectronicos2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/anula`, body);
  }

  public activateRecaudacionME(body: RecaudacionMediosElectronicos2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/activa`, body);
  }

  public openRecaudacionME(body: RecaudacionMediosElectronicos2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/abrir`, body);
  }

  public closeRecaudacionME(body: RecaudacionMediosElectronicos2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/cerrar`, body);
  }

  public getOperacionesScunac(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesScunac/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public actualizarIdOperacionScunac(body: IPostOperacionScunac): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL2 }/sarf/regDetaPago`, body );
  }  

  public totalPerOfficeRecaudacionME(idCncl: number): Observable<ITotalPorOficina[]> {
    return this.http.get<ITotalPorOficina[]>( `${this.baseURL}/operacionesNiubizTotal/bandeja/${idCncl}`);
  } 

  public getDetalleConciliarPorMonto(idCncl: number, nuFila: number): Observable<IResConciliacionPorMonto2> {
    return this.http.get<IResConciliacionPorMonto2>( `${this.baseURL}/operacionesNiubiz/${idCncl}/${nuFila}`);
  }

  public getBandejaConciliarPorMonto(idCncl: number, nuFila: number, tiProc: number): Observable<IConciliacionPorMonto3[]> {
    return this.http.get<IConciliacionPorMonto3[]>( `${this.baseURL}/conciliacionesScunac/bandeja/${idCncl}/${nuFila}/${tiProc}`);
  }

  public saveConciliarPorMonto(body: ConciliacionPorMonto): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/operacionNiubiz`, body);
  }

  public getBandejaConciliacionesMovimiento(idCncl: number, tiMovi: string): Observable<IVariosEfectivoAnulacion[]> {
    return this.http.get<IVariosEfectivoAnulacion[]>( `${this.baseURL}/conciliacionesMovimiento/bandeja/${idCncl}/${tiMovi}`);
  }

  public createConciliacionMovimiento(body: ConciliacionMovimiento): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacionMovimiento`, body);
  }

  public editConciliacionMovimiento(body: ConciliacionMovimiento): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacionMovimiento`, body);
  }

  public getRecaudacionById( idCncl: number ): Observable< RecaudacionMediosElectronicos > {
    return this.http.get< RecaudacionMediosElectronicos >( `${ this.baseURL }/conciliaciones/${ idCncl }`);
  }

  public getRecaudacionNiubiz( idCncl: number, inCncl: number ): Observable< any > {
    return this.http.get< RecaudacionMediosElectronicosNiubiz[] >( `${ this.baseURL }/operacionesNiubiz/bandeja/${ idCncl }/${ inCncl }`);
  }

  public getRecaudacionDiners( idCncl: number, inCncl: number ): Observable< any > {
    return this.http.get< RecaudacionMediosElectronicosDiners[] >( `${ this.baseURL }/operacionesDiners/bandeja/${ idCncl }/${ inCncl }`);
  }

  public getRecaudacionAmex( idCncl: number, inCncl: number ): Observable< any > {
    return this.http.get< RecaudacionMediosElectronicosAmex[] >( `${ this.baseURL }/operacionesAmex/bandeja/${ idCncl }/${ inCncl }`);
  }
}