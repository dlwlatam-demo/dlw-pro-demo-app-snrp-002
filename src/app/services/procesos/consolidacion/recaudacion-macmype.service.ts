import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RecaudacionMacmype, RecaudacionMacmype2 } from 'src/app/models/procesos/consolidacion/recaudacion-macmype.model';
import { environment } from 'src/environments/environment';
import { IPostOperacionScunac, IDataTransacMacmype } from 'src/app/interfaces/consolidacion-macmype';
import { IResponse } from 'src/app/interfaces/general.interface';

@Injectable({
  providedIn: 'root'
})
export class RecaudacionMacmypeService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos/consolidacion`;
  private baseURL2: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos`;

  constructor( private http: HttpClient ) { }

  public getBandejaConciliacion(data: any): Observable<any> {
    return this.http.post<any>(`${this.baseURL }/conciliaciones/bandeja`, data);
  }

  public procesarRecaudacionMacmype(data: RecaudacionMacmype): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL }/conciliacion`, data );
  }

  public reprocesaRecaudacionMacmype(body: RecaudacionMacmype): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliacion/procesa/${body.idCncl}`, body);
  }

  public anulaRecaudacionMacmype(body: RecaudacionMacmype2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/anula`, body);
  }

  public activateRecaudacionMacmype(body: RecaudacionMacmype2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/activa`, body);
  }

  public openRecaudacionMacmype(body: RecaudacionMacmype2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/abrir`, body);
  }

  public closeRecaudacionMacmype(body: RecaudacionMacmype2): Observable<IResponse> {
    return this.http.put<IResponse>( `${ this.baseURL }/conciliaciones/cerrar`, body);
  }

  public getOperacionesScunac(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL}/operacionesScunac/bandeja/${idCncl}`, {params, observe: 'response'});
  }

  public actualizarIdOperacionScunac(body: IPostOperacionScunac): Observable<IResponse> {
    return this.http.post<IResponse>( `${ this.baseURL2 }/sarf/regDetaPago`, body );
  }  


  public getRecaudacionById( idCncl: number ): Observable< RecaudacionMacmype > {
    return this.http.get< RecaudacionMacmype >( `${ this.baseURL }/conciliaciones/${ idCncl }`);
  }

  public getRecaudacionNiubiz( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    return this.http.get< any >( `${ this.baseURL }/operacionesNiubiz/bandeja/${ idCncl }/${ inCncl }`);
  }

  public getRecaudacionTxSftpBN(idCncl: number, inCncl: number): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL2}/conciliacion/bancaria/sftp/${idCncl}`, {params, observe: 'response'});
  }

  public getTransaccionesSPRL( idCncl: number, inCncl: number ): Observable< HttpResponse<any> > {
    let params = new HttpParams();
    params = params.append('inCncl', inCncl);
    return this.http.get<any>( `${this.baseURL2}/conciliacion/bancaria/sprl/macm/${idCncl}`, {params, observe: 'response'});
  }

  public getConciliacionesSPRL( idCncl: number, nuSecu: number ): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/conciliacionesSPRL/${ idCncl }/${ nuSecu }`);
  }


}