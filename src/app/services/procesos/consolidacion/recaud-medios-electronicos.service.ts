import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { RecaudacionMediosElectronicos } from '../../../models/procesos/consolidacion/recaud-medios-electronicos.model';
import { RecaudacionMediosElectronicosNiubiz } from '../../../models/procesos/consolidacion/recaud-medios-electronicos-niubiz.model';
import { RecaudacionMediosElectronicosDiners } from '../../../models/procesos/consolidacion/recaud-medios-electronicos-diners.model';
import { RecaudacionMediosElectronicosAmex } from '../../../models/procesos/consolidacion/recaud-medios-electronicos-amex.model';

import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecaudMedElectService {

  private baseURL: string = `${ environment.apiUrlProcesos }/sarfsunarp/api/v1/procesos/consolidacion`;

  constructor( private http: HttpClient ) { }

  getBandejaConciliacion( data: any ): Observable< RecaudacionMediosElectronicos[] > {
    return this.http.post< RecaudacionMediosElectronicos[] >( `${ this.baseURL }/conciliaciones/bandeja`, data );
  }

  createRecaudacion( data: RecaudacionMediosElectronicos ): Observable< RecaudacionMediosElectronicos > {
      return this.http.post< RecaudacionMediosElectronicos >( `${ this.baseURL }/conciliacion`, data );
  }

  getRecaudacionById( idCncl: number ): Observable< RecaudacionMediosElectronicos > {
    return this.http.get< RecaudacionMediosElectronicos >( `${ this.baseURL }/conciliaciones/${ idCncl }`);
  }

  getRecaudacionNiubiz( idCncl: number, inCncl: number ): Observable< RecaudacionMediosElectronicosNiubiz[] > {
    return this.http.get< RecaudacionMediosElectronicosNiubiz[] >( `${ this.baseURL }/operacionesNiubiz/bandeja/${ idCncl }/${ inCncl }`);
  }

  getRecaudacionDiners( idCncl: number, inCncl: number ): Observable< RecaudacionMediosElectronicosDiners[] > {
    return this.http.get< RecaudacionMediosElectronicosDiners[] >( `${ this.baseURL }/operacionesDiners/bandeja/${ idCncl }/${ inCncl }`);
  }

  getRecaudacionAmex( idCncl: number, inCncl: number ): Observable< RecaudacionMediosElectronicosAmex[] > {
    return this.http.get< RecaudacionMediosElectronicosAmex[] >( `${ this.baseURL }/operacionesAmex/bandeja/${ idCncl }/${ inCncl }`);
  }

/*
  anularDocumentos( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/documentos/anula`, data );
  }

  activarDocumentos( data: any ): Observable< any > {
    return this.http.put( `${ this.baseURL }/documentos/activa`, data );
  }*/
}
