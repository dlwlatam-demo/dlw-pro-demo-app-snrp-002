import { Injectable } from '@angular/core';
import { BehaviorSubject, distinctUntilChanged, Observable, skipWhile } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedMessageService {

  private b : BehaviorSubject<string> = new BehaviorSubject('');

  public readonly obs: Observable<string> = this.b.asObservable().pipe(skipWhile((v,_i) => !v), distinctUntilChanged())

  private s: BehaviorSubject<string> = new BehaviorSubject('')

  readonly sob$: Observable<string> = this.s.asObservable()

  private vp:  BehaviorSubject<string> = new BehaviorSubject('')
  readonly vp$: Observable<string> = this.vp.asObservable()


  vp_(u: any) {
    this.vp.next(u)
  }

  updateS() {
   this.s.next('') 
  }

  sendMessage(msg: string) { 
    this.b.next(msg) 
  }


 
}
