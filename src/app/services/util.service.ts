import { Injectable } from '@angular/core';
import Swal, { SweetAlertResult } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  onShowProcessLoading(mensaje: string) {
    Swal.fire({
      heightAuto: false,
      title: "Atención",  
      html: `${mensaje}. <br><b><span style='font-size: 14px;'>¡Espere un momento, por favor!</span></b>`,
      allowOutsideClick: false,
      customClass: {
        popup: 'sweet_popup-class_3',
        title: 'sweet_title',
        htmlContainer: 'sweet_htmlContainer_2',
        actions: 'sweet_actions',
        loader: 'sweet_loader'
      },
      didOpen: () => {
        Swal.showLoading();             
      },      
    });
  } 

  onShowAlert( icon: string = 'info', title = 'Mensaje al usuario', html: string ) {
    let ruta: string = "";
    switch (icon) {
      case "success":
        ruta = 'assets/img/alerts/sweet_success.PNG';
        break;
      case "warning":
        ruta = 'assets/img/alerts/sweet_warning.PNG';
        break;
      case "info":
        ruta = 'assets/img/alerts/sweet_info.PNG';
        break;
      case "question":
        ruta = 'assets/img/alerts/sweet_question.PNG';
        break;
      case "error":
        ruta = 'assets/img/alerts/sweet_error.PNG';
        break;
      default:
        ruta = 'assets/img/alerts/sweet_info.PNG';
        break;
    }

    Swal.fire({
      heightAuto: false,     
      title,
      html,   
      imageUrl: ruta,
      confirmButtonText: 'Aceptar',
      allowOutsideClick: false,
      customClass: {
        popup: 'sweet_popup-class',
        title: 'sweet_title',
        htmlContainer: 'sweet_htmlContainer_3',
        closeButton: 'sweet_closeButton',
        confirmButton: 'sweet_confirmnButton', 
        actions: 'sweet_actions_2'       
      }        
      }).then((result) => {    
    });
  }

  onShowAlertPromise( icon: string = 'info', title = 'Mensaje al usuario', html: string ) : Promise<SweetAlertResult> {
    let ruta: string = "";
    switch (icon) {
      case "success":
        ruta = 'assets/img/alerts/sweet_success.PNG';
        break;
      case "warning":
        ruta = 'assets/img/alerts/sweet_warning.PNG';
        break;
      case "info":
        ruta = 'assets/img/alerts/sweet_info.PNG';
        break;
      case "question":
        ruta = 'assets/img/alerts/sweet_question.PNG';
        break;
      case "error":
        ruta = 'assets/img/alerts/sweet_error.PNG';
        break;
      default:
        ruta = 'assets/img/alerts/sweet_info.PNG';
        break;
    }

    return Swal.fire({
      heightAuto: false,     
      title,
      html,   
      imageUrl: ruta,
      confirmButtonText: 'Aceptar',
      allowOutsideClick: false,
      customClass: {
        popup: 'sweet_popup-class',
        title: 'sweet_title',
        htmlContainer: 'sweet_htmlContainer_3',
        closeButton: 'sweet_closeButton',
        confirmButton: 'sweet_confirmnButton', 
        actions: 'sweet_actions_2'       
      }        
    });
  }
 
  onShowConfirm(message: string) : Promise<SweetAlertResult> {  
    return Swal.fire({
      heightAuto: false,
      html: `<span style="font-size: 14px;"><b>${message}</b></span>`,
      imageUrl: 'assets/img/alerts/sweet_question.PNG',
      padding: '1.5rem',
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonText: 'Aceptar',       
      cancelButtonText: 'Cancelar',      
      reverseButtons: false, 
      allowOutsideClick: false,       
      customClass: {
        popup: 'sweet_popup-class',
        closeButton: 'sweet_closeButton',
        confirmButton: 'sweet_confirmnButton', 
        cancelButton: 'sweet_cancelButton'               
      }
    })
  } 

  onShowProcessSaveEdit() {
    Swal.fire({
      heightAuto: false,
      title: "Atención",          
      html: "Guardando el registro",
      allowOutsideClick: false,
      customClass: {
        popup: 'sweet_popup-class_3',
        title: 'sweet_title',
        htmlContainer: 'sweet_htmlContainer_2',
        actions: 'sweet_actions',
        loader: 'sweet_loader'
      },
      didOpen: () => {
        Swal.showLoading();             
      },      
    });
  }

  onShowProcess(proceso: string) {
    let message: string;
    if (proceso === 'crear' || proceso === 'editar') {
      message = 'Guardando el registro';
    } else if (proceso === 'anular') {
      message = 'Anulando el/los registro(s)';
    } else if (proceso === 'activar') {
      message = 'Activando el/los registro(s)';
    } else if (proceso === 'abrir') {
      message = 'Abriendo el/los registro(s)';
    } else if (proceso === 'cerrar') {
      message = 'Cerrando el/los registro(s)';
    } else if (proceso === 'cambiar') {
      message = 'Actualizando estado a No Utilizado';
    } else if (proceso === 'autorizar') {
      message = 'Autorizando la modificación';
    } else if (proceso === 'anulacion') {
      message = 'Solicitando la anulación del/de los cheque(s)';
    } else if (proceso === 'devolucion') {
      message = 'Solicitando la devolución del/de los cheque(s)';
    } else {
      message = proceso;
    }
    Swal.fire({
      heightAuto: false,
      title: "Atención",          
      html: `${message}`,
      allowOutsideClick: false,
      customClass: {
        popup: 'sweet_popup-class_3',
        title: 'sweet_title',
        htmlContainer: 'sweet_htmlContainer_2',
        actions: 'sweet_actions',
        loader: 'sweet_loader'
      },
      didOpen: () => {
        Swal.showLoading();             
      },      
    });
  }

  onShowMessageErrorSystem () {
    this.onShowAlert("warning", "Atención", "El servicio no esta disponible en estos momentos, inténtelo mas tarde.");
  }

  onCloseLoading() {
    Swal.close();
  }
  
}