import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, throwError, timeout } from 'rxjs';

import { ICargaArchivo } from '../interfaces/archivo-adjunto.interface';
import { 
  Estado, 
  Caja, 
  Documento, 
  ZonaRegistral, 
  OficinaRegistral, 
  Local, 
  MesAnio 
} from '../interfaces/combos.interface';
import { IZonaRegistral, IOficinaRegistral, ILocalAtencion } from '../interfaces/combosV2.interface';
import { IPersona, IResponse2, IParametros } from '../interfaces/general.interface';

import { BodyFirmaBeanList } from '../models/mantenimientos/documentos/documento-firma.model';

import { AuthCheckService } from './auth/auth-check.service';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  
  private baseURL: string = `${ environment.apiUrlParametros }/sarfsunarp/api/v1/parametros`;
  private baseURLCtas: string = `${ environment.apiUrlMantenimiento }/sarfsunarp/api/v1/mantenimiento`;
  private baseURLSeg: string = `${ environment.apiUrlMtoSeguridad }/sarfsunarp/api/v1/mtoseguridad`;

  private headers = new HttpHeaders({ token: this.authCheck.getToken(), coPerf: localStorage.getItem('perfil_sarf') })

  constructor(
    private http: HttpClient,
    private authCheck: AuthCheckService
  ) { }

  getCbo_TipoDocumento() {
    const arrTipoDocumento: Documento[] = [
      { "coTipoDocu": "1", "deTipoDocu": "Recibo de Ingreso" }, 
      { "coTipoDocu": "2", "deTipoDocu": "Comprobante de Pago" }
    ];
    
    return arrTipoDocumento;
  }

  getCbo_Estado() {
    const arrEstado: Estado[] = [
      { "coEstado": 'A', 'deEstado': 'Activo' },
      { 'coEstado': 'I', 'deEstado': 'Inactivo' }
    ];

    return arrEstado;
  }

  getCbo_Zonas_Usuario(coUsua: string):Observable< any >{
    return this.http.get<ZonaRegistral[]>( `${ this.baseURLSeg }/zonasRegistrales/0` );
  }

  getCbo_Oficinas_Zonas(coUsua: string, coZonaRegi: string): Observable< any > {
    return this.http.get<OficinaRegistral[]>( `${ this.baseURLSeg }/oficinasRegistrales/0/${ coZonaRegi }` );
  }

  getCbo_Locales_Ofic(coUsua: string, coZonaRegi: string, coOficRegi: string): Observable< any > {
    return this.http.get<Local[]>( `${ this.baseURLSeg }/localesAtencion/0/${ coZonaRegi }/${ coOficRegi }` );
  }

  getCbo_ZonasRegistrales(): Observable< IZonaRegistral[] > {
    return this.http.get< IZonaRegistral[] >( `${ this.baseURLCtas }/documentos/zonasRegistrales/A` );
  }

  getCbo_OficinasRegistrales( coZonaRegi: string ): Observable< IOficinaRegistral[] > {
    return this.http.get< IOficinaRegistral[] >( `${ this.baseURLCtas }/documentos/oficinasRegistrales/${ coZonaRegi }/A` );
  }

  getCbo_LocalesAtencion( coZonaRegi: string, coOficRegi: string ): Observable< ILocalAtencion[] > {
    return this.http.get< ILocalAtencion[] >( `${ this.baseURLCtas }/documentos/localesAtencion/${ coZonaRegi }/${ coOficRegi }/A` );
  }

  getCbo_CargosUsuario(): Observable< any > {
    return this.http.get< any >( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=013&cinEstd=A`)
  }

  getCbo_Documento(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=302&cinEstd=A` );
  }

  getCbo_EstadoRecibo(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=303&cinEstd=A` );
  }

  getCbo_EstadoComprobante(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=304&cinEstd=A` );
  }

  getCbo_EstadoCarta(): Observable< IResponse2[] > {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=305&cinEstd=A` );
  }

  getCbo_EstadoConstancia(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=306&cinEstd=A` );
  }

  getCbo_TipoRecaudacion(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=307&cinEstd=A` );
  }

  getCbo_EstadoCheque(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=308&cinEstd=A` );
  }

  getCbo_EstadoDocumento(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=309&cinEstd=A` );
  }

  getCbo_EstadoCierre(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=310&cinEstd=A` );
  }

  getCbo_EstadoMemorandum(): Observable< IResponse2[] > {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=311&cinEstd=A` );
  }

  getCbo_EstadoRecaudacion(): Observable<IResponse2[]> {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=312&cinEstd=A` );
  }

  getCbo_EstadoRecaudacionScunac(): Observable<IResponse2[]> {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=312&cinEstd=A` );
  }

  getCbo_EstadoReferencia(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=314&cinEstd=A` );
  }

  getCbo_EstadoTipoDocumento(): Observable< IResponse2[] > {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=315&cinEstd=A` );
  }

  getCbo_TipoDocumentoIdentidad(): Observable<IResponse2[]> {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=316&cinEstd=A` );
  }

  getCbo_TipoDocumentosVarios(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=317&cinEstd=A` );
  }

  getCbo_EstadoDocumentosVarios(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=318&cinEstd=A` );
  }

  getCbo_PerfilDocumentosVarios(): Observable< IResponse2[] > {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=319&cinEstd=A` );
  }

  getCbo_TipoProcesoEnviar(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=320&cinEstd=A` );
  }

  getCbo_EstadoDocumentosEnviar(): Observable< any > {
    return this.http.get( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=321&cinEstd=A` );
  }

  getCbo_TipoDocumentoFirmas(): Observable<IResponse2[]> {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=322&cinEstd=A` );
  }

  getCbo_OperadorSPRL(): Observable< IResponse2[] > {
    return this.http.get< IResponse2[] >( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=323&cinEstd=A`);
  }

  getCbo_TipoConciliacion(): Observable< IResponse2[] > {
    return this.http.get< IResponse2[] >( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=324&cinEstd=A`);
  }

  getCbo_TipoMovimiento(): Observable< IResponse2[] > {
    return this.http.get< IResponse2[] >( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=325&cinEstd=A`);
  }

  getCbo_PerfilDocumentosEnviar(): Observable< IResponse2[] > {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=326&cinEstd=A` );
  }

  getCbo_EstadoSprl(): Observable<IResponse2[]> {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=327&cinEstd=A` );
  }
  
  getCbo_Operadores(): Observable< IResponse2[] > {
    return this.http.get< IResponse2[] >( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=328&cinEstd=A`);
  }

  getCbo_OperadoresAppQrSid(): Observable< IResponse2[] > {
    return this.http.get< IResponse2[] >( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=329&cinEstd=A`);
  }

  getCbo_EstadoSiafMefLibro(): Observable<IResponse2[]> {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=330&cinEstd=A` );
  }

  getCbo_EstadoSiafMefBanco(): Observable<IResponse2[]> {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=331&cinEstd=A` );
  }

  getCbo_DescargarConceptos(): Observable<IResponse2[]> {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=332&cinEstd=A` );
  }

  getCbo_CodigoAnulacion(): Observable<IResponse2[]> {
    return this.http.get<IResponse2[]>( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=333&cinEstd=A` );
  }

  getCbo_TipoConciliacionBancaria(): Observable< IResponse2[] > {
    return this.http.get< IResponse2[] >( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=334&cinEstd=A`);
  }

  getCbo_GruposCarta(): Observable< IResponse2[] > {
    return this.http.get< IResponse2[] >( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=335&cinEstd=A`);
  }

  getCbo_TipoDocumentoLogos(): Observable< IResponse2[] > {
    return this.http.get< IResponse2[] >( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=336&cinEstd=A`);
  }

  getCbo_MotivoLicencia(): Observable< IResponse2[] > {
    return this.http.get< IResponse2[] >( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=337&cinEstd=A`);
  }

  getCbo_MediosDePago(): Observable< IResponse2[] > {
    return this.http.get< IResponse2[] >( `${ this.baseURL }/catalogos/bandeja?ccodigoMaster=338&cinEstd=A`);
  }

  getCbo_Caja() {
    const arrCaja: Caja[] = [
      { "coCaja": "D", "deCaja": "Debe" }, 
      { "coCaja": "H", "deCaja": "Haber" }
    ];    
    return arrCaja;
  }  

  getCbo_Bancos(): Observable< any > {
    return this.http.get( `${ this.baseURL }/instituciones/bandeja/001?cinEstd=A` );
  }

  getParametrosDelSistema( idParm: number ): Observable< IParametros[] > {
    return this.http.get< IParametros[] >( `${ this.baseURL }/parametros/bandeja?idParm=${ idParm }&inRegi=A` );
  }

  getCbo_Cuentas( idBanc: number ) : Observable< any >{
    return this.http.get( `${ this.baseURLCtas }/maestras/cuentasBancarias/bandeja/${ idBanc }?nuCuenBanc=&inRegi=A` );
  }

  getCbo_MesesAnio() {
    const arrMes: MesAnio[] = [
      { 'coMes': '1', 'deMes': 'ENERO' },
      { 'coMes': '2', 'deMes': 'FEBRERO' },
      { 'coMes': '3', 'deMes': 'MARZO' },
      { 'coMes': '4', 'deMes': 'ABRIL' },
      { 'coMes': '5', 'deMes': 'MAYO' },
      { 'coMes': '6', 'deMes': 'JUNIO' },
      { 'coMes': '7', 'deMes': 'JULIO' },
      { 'coMes': '8', 'deMes': 'AGOSTO' },
      { 'coMes': '9', 'deMes': 'SEPTIEMBRE' },
      { 'coMes': '10', 'deMes': 'OCTUBRE' },
      { 'coMes': '11', 'deMes': 'NOVIEMBRE' },
      { 'coMes': '12', 'deMes': 'DICIEMBRE' },
    ];
    return arrMes;
  }

  getUploadingEndpoint(): string {
    return `${ environment.apiUrlDigital }/sarfsunarp/api/v1/digital/archivo/carga`;
  }

  getUploadingEndpointObservable( file: File ): Observable<ICargaArchivo> {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    return this.http.post<ICargaArchivo>( `${ environment.apiUrlDigital }/sarfsunarp/api/v1/digital/archivo/carga`, formData );
  }

  grabarAdjunto( data: any ): Observable< any > {
    return this.http.post(`${ environment.apiUrlDigital }/sarfsunarp/api/v1/digital/archivo/adjunto`, data, { headers: this.headers });
  }

  eliminarAdjunto( idGuidDocu: string, perfil: string, token: string ): Observable< any > {
    return this.http.delete(`${ environment.apiUrlDigital }/sarfsunarp/api/v1/digital/archivo/${ idGuidDocu }`, { headers: this.headers });
  }
  
  subeRedis( body: BodyFirmaBeanList ): Observable< any >  {
    return this.http.post(`${ environment.apiUrlDigital }/sarfsunarp/api/v1/digital/proceso/firma/sube-redis`, body, { headers: this.headers })
  }

  downloadManager(guid: string) {
    return `${ environment.apiUrlManager }/sarfsunarp/api/v1/digital/obtenerAdjunSarf/${ guid }`
  }
  
  downloadManagerImagen(guid: string): Observable<any> {
    return this.http.get<any>(`${ environment.apiUrlManager }/sarfsunarp/api/v1/digital/obtenerAdjunSarf/${ guid }`,{responseType: 'blob' as 'json'});
  }
  
  obtenerDatosPorDNI(numero: string): Observable<IPersona> {
    return this.http.get<IPersona>(`${ this.baseURL }/valida/dni/${numero}`);
  }

  chequesEnMemoria(): Observable< any > {
    return this.http.get<any>( `${ environment.apiUrlDigital }/sarfsunarp/api/v1/digital/archivo/carga/cheque` );
  }
  
}
