import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';

import { AuthService } from './auth.service';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthCheckService {
  constructor(private router: Router, private authservice: AuthService, private spinner: NgxSpinnerService) { }

  verificar_autorizacion(e: any):boolean{
    Swal.close();
    this.spinner.hide();
    if((e.status==401)||e.status==403){
      switch(e.status){
        case 401:
          if(e.error.error_description.includes("Access token expired")){
            Swal.fire({ icon: 'info', title: `Su sesión ha expirado por límite de tiempo en el ${environment.no_sistema}`, text:'Para volver a iniciar sesión por favor ingrese nuevamente sus credenciales válidas(usuario de red y contraseña)', allowOutsideClick: false });
          }
          else if(e.error.error_description.includes("Full authentication is required to access this resource")){
            Swal.fire({ icon: 'warning', title: `Usted necesita autenticacion para acceder a este recurso en el ${environment.no_sistema}`, text:'Por favor ingresar al sistema con sus credenciales válidas(usuario de red y contraseña)', allowOutsideClick: false });

          }
          else if(e.error.error_description.includes("Cannot convert access token to JSON")){
            Swal.fire({ icon: 'error', title: `Su autenticación es invalida en el ${environment.no_sistema}`, text:'Por favor ingresar al sistema con sus credenciales(usuario de red y contraseña), si persiste el problema por favor comunicarse con Mesa de Ayuda de su Zona Registral', allowOutsideClick: false });
          }
          this.authservice.logout();
        break;
        case 403:
          Swal.fire({ icon: 'warning', title: `Usted no tiene autorización para acceder a este recurso en el ${environment.no_sistema}`, text: `Por favor consultar con el Administrador del ${environment.no_sistema}`, allowOutsideClick: false });
          this.router.navigate(['/']);
        break;
      }
      return true
    }
    else{
      if(e.status==500){
        if(e.error.message.includes('Connection refused: no further information:')){
          Swal.fire({ icon: 'info', title: 'El servidor donde se procesan sus funcionalidad se encuentra en actualización o apagado', text:'Por favor esperar máximo 5 minutos, si este mensaje persiste por favor comunicarse con Mesa de Ayuda o Soporte del Sistema', allowOutsideClick: false });
        }
        else{
          Swal.fire({ icon: 'warning', title: `Se ha producido un error inesperado en el ${environment.no_sistema}`, text: `Error: ${e.status}`, allowOutsideClick: false });
        }
      }
      else if (e.status==503){
        Swal.fire({ icon: 'warning', title: 'El servicio no se encuentra disponible (Error 503)', text: `Error: ${e.status}`, allowOutsideClick: false });

      }
      else{
        Swal.fire({ icon: 'warning', title: `Se ha producido un error inesperado en el ${environment.no_sistema}`, text: `Error: ${e.status}`, allowOutsideClick: false });
      }
    }
    return false;
  }
 
  verificar_token(cabecera:HttpHeaders){
    let token = this.authservice.token;
    
    if(token!=null){
      return cabecera.append('Authorization','Bearer '+token)
    }
    return cabecera;
  }

  getToken() :string{
    return this.authservice.token;
  }


}
