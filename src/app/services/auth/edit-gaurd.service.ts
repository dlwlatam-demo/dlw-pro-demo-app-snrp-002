// import { Injectable } from '@angular/core';
// import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
// import { SharingInformationService } from 'src/app/core/services/sharing-services.service';

// @Injectable({
//   providedIn: 'root'
// })
// export class EditGaurdService implements CanActivate {

//   constructor(
//                 private router: Router,
//                 private sharingInformationService: SharingInformationService
//             ) { }

//     canActivate( _route: ActivatedRouteSnapshot, _state: RouterStateSnapshot ) {
//         const isEditCartaOrden = this.sharingInformationService.isEditCartaOrden;
//         console.log("isEditCartaOrden", isEditCartaOrden);
        
//         if (isEditCartaOrden) {
//             return true;
//         } else {
//             this.router.navigate(['/SARF/mantenimientos/documentos/carta_orden/bandeja']);
//             return false;
//         }
//     }
// }