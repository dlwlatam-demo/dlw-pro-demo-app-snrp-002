import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { JwtHelperService } from '@auth0/angular-jwt';

import { Usuario } from '../../models/auth/Usuario';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _usuario?: Usuario;
  private _token?: string ;
  private _jti?: string ;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  roles !: string[];


  private jwtservice:JwtHelperService= new JwtHelperService();
  constructor(private http: HttpClient, private router: Router) {

  }

  public get token(): string {
    if (this._token != null) {
      return this._token;
    } else if ( this._token == null &&  localStorage.getItem("t_sarf") != null ) {
      this._token = localStorage.getItem("t_sarf")!;
      return this._token;
    }
    return null!;
  }

  obtenerDatosToken(accesstoken: string): any {
    if (accesstoken != null) {
      return this.jwtservice.decodeToken(accesstoken);
    }
    return null;
  }


  login(usuario: string, password: string): Observable<any> {
 
   const urlEndpoint =  `${environment.apiUrlSeguridad}/oauth/token` ;

    const credenciales = btoa("sarf_sunarp" + ":" + "Delaware2202");
    const httpHeaders_option = 
    new HttpHeaders({
      "Content-Type": "application/x-www-form-urlencoded",
       Authorization: "Basic " + credenciales,
    });
 
    let params = new URLSearchParams();
    params.set("username", usuario);
    params.set("password", password);
    params.set("grant_type", "password");

 
    return this.http.post<any>(urlEndpoint, params.toString(), {  headers: httpHeaders_option });
  }

  guardarToken(accesstoken: string, jti: string): void {
    this._token = accesstoken;
    this._jti= jti;
    localStorage.setItem("t_sarf", accesstoken);
    localStorage.setItem("t_jsarf", jti);
  }

  logout() {

 
    this.router.navigate(["/login"]);
    this._token = null!;
 
    this.invalidarToken(localStorage.getItem('t_jsarf')!).subscribe(
      _response => {

      localStorage.removeItem("t_sarf");
      localStorage.removeItem("t_jsarf");
      
      },
      (error) => {
        console.log( error );
    });
    
    localStorage.clear();
 
  }

  isAuthenticated() {
    let payload = this.obtenerDatosToken(this.token);
    if (payload != null && payload.user_name && payload.user_name.length > 0) {
      return true;
    }
    return false;
  }

  guardarUsuario(accesstoken: string): void {
 
    let payload = this.obtenerDatosToken(accesstoken);
    this._usuario = new Usuario();
  
    this._usuario = new Usuario();
    this._usuario.user_name = payload.user_name;
    this._usuario.nombre_completo = payload.nombre_completo;
    this._usuario.roles = payload.authorities;
    
    localStorage.setItem("nombre_sarf", payload.nombre_completo);
    localStorage.setItem("user_sarf", payload.user_name);
    localStorage.setItem("roles_sarf", JSON.stringify(this._usuario.roles));
    
  }
 
  invalidarToken(jti: string): Observable<any> {
    return this.http.delete<number>(`${environment.apiUrlSeguridad}/invalidarToken/${jti}`);
  }
 
  getUserAd(): string{
    return localStorage.getItem('user_sarf')!;
  }

  getOptionEnable(valor: string): boolean {

     let accionesPerfil : string [] = [];
  
      this.roles=JSON.parse(localStorage.getItem('roles_sarf')!);
      this.roles!.forEach(r => {  
        let perfiles : string[] =r.split('-');

        accionesPerfil.push(perfiles[2]+'_'+perfiles[3]);
		if(accionesPerfil.includes(valor)) {
			return;
		}

     })
 
      return accionesPerfil.includes(valor);
  
  }


  getIsPerfil(valor: string): boolean {

     let accionesPerfil : string [] = [];
  
      this.roles=JSON.parse(localStorage.getItem('roles_sarf')!);
	  
      this.roles!.forEach(r => {  
        let perfiles : string[] =r.split('-');

		if(!accionesPerfil.some(e => e === perfiles[1])) {
			accionesPerfil.push(perfiles[1]);
			if(accionesPerfil.includes(valor)) {
				return;
			}
		}
     })
 
      return accionesPerfil.includes(valor);
  
  }


}


