import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as XLSX from "xlsx";

@Component({
  selector: 'app-excelsheet',
  templateUrl: './excelsheet.component.html',
  styleUrls: ['./excelsheet.component.scss']
})
export class ExcelsheetComponent implements OnInit {

  @Output()
  nameFile = new EventEmitter<string>()

  @Output()
  uploaded = new EventEmitter<any>()

  @Output()
  onDelete = new EventEmitter<string>()

  data: [][];

  constructor() { 
    this.data = [];
  }

  ngOnInit(): void {
  }

  onFileChange(event: any){
    const target : DataTransfer = <DataTransfer> (event.target);
    if(target.files.length == 0) return;
    if(target.files.length > 1) throw new Error('No puede seleccionar varios archivos');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary', 
      raw: false,
      dateNF: 'dd/MM/yyyy' });
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      this.data = (XLSX.utils.sheet_to_json(ws, { header: 1, raw: false, dateNF: 'dd/MM/yyyy'})); 
      this.uploaded.emit(this.data);
      this.nameFile.emit(target.files[0].name);
    }
    reader.readAsBinaryString(target.files[0]);   
  }
}
