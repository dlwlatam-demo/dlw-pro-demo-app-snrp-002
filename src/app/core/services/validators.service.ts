import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidatorsService {

  constructor() { }

  padTo2Digits(num: number): string {
	  return num.toString().padStart(2, '0');
  }

  formatDate(date: Date): string {
	  return (
		[
		  this.padTo2Digits(date.getDate()),
		  this.padTo2Digits(date.getMonth() + 1),
		  date.getFullYear(),
		].join('/') 
	  );
	}

  formatDate2(date: any): string {
	  return (
		[
		  this.padTo2Digits(date.getDate()),
		  this.padTo2Digits(date.getMonth() + 1),
		  date.getFullYear(),
		].join('/') 
	  );
	}

  formatDateWithHours(date: Date): string {
	  return (
		[
		  this.padTo2Digits(date.getDate()),
		  this.padTo2Digits(date.getMonth() + 1),
		  date.getFullYear(),
		].join('/')  +
		' ' +
		[
		  this.padTo2Digits(date.getHours()),
		  this.padTo2Digits(date.getMinutes())
		].join(':') 
	  );
	}

  formatDateWithHoursForExcel(date: Date): string {
	  return (
		[
		  date.getFullYear(),
		  this.padTo2Digits(date.getMonth() + 1),
		  this.padTo2Digits(date.getDate()),
		].join('')  +
		'_' +
		[
		  this.padTo2Digits(date.getHours()),
		  this.padTo2Digits(date.getMinutes())
		].join('') 
	  );
	}

  formatDateWithHoursAndSeconds(date: Date): string {
	  return (
		[
		  this.padTo2Digits(date.getDate()),
		  this.padTo2Digits(date.getMonth() + 1),
		  date.getFullYear(),
		].join('/')  +
		' ' +
		[
		  this.padTo2Digits(date.getHours()),
		  this.padTo2Digits(date.getMinutes()),
		  this.padTo2Digits(date.getSeconds())
		].join(':') 
	  );
	}

  reFormateaFecha(fecha?: string): any {
    // origen: YYYY-MM-DD HH24:MI:SS, retorna: DD/MM/YYYY
    if(fecha) {
      if(fecha.length > 10) {
        let anio = fecha.substr(0, 4);
        let mes = fecha.substr(5, 2);
        let dia = fecha.substr(8, 2);
        
        return dia + "/" + mes + "/" + anio;
      }

      if(fecha.length === 10) {
        let anio = fecha.substr(0, 4);
        let mes = fecha.substr(5, 2);
        let dia = fecha.substr(8, 2);
        
        return dia + "/" + mes + "/" + anio;
      }
    
    }
    else
      return null; 
    
    }

  reFormateaFechaConHoras(fecha?: string): any {
    // origen: YYYY-MM-DD HH24:MI:SS, retorna: DD/MM/YYYY HH24:MI:SS
    if(fecha) {
      if(fecha.length > 10) {
        let anio = fecha.substr(0, 4);
        let mes = fecha.substr(5, 2);
        let dia = fecha.substr(8, 2);
        let horas = fecha.substring(11);
        
        return dia + "/" + mes + "/" + anio + " " + horas;
      }
      else {
        let anio = fecha.substr(0, 4);
        let mes = fecha.substr(5, 2);
        let dia = fecha.substr(8, 2);
        
        return dia + "/" + mes + "/" + anio;
      }
    
    }
    else
      return null; 
    
    }
  
    reFormateaDate(fecha?: Date): any {
    // origen: YYYY-MM-DD HH24:MI:SS, retorna: DD/MM/YYYY HH24:MI:SS
    if(fecha) {
      const dt = new Date(fecha)
      let anio: string = "" + dt.getFullYear();
      let mes: string = "0" + dt.getMonth();
      let dia: string = "0" + dt.getDay();
      let horas: string = "0" + dt.getHours();
      let minutos: string = "0" + dt.getMinutes();
      let segundos: string = "0" + dt.getSeconds();
      
      return dia.substr(-2, 2) + "/" + mes.substr(-2, 2) + "/" + anio + " " + horas.substr(-2, 2) + ":" + minutos.substr(-2, 2) + ":" + segundos.substr(-2, 2);
    }
    else
      return null; 
    
    }
}
