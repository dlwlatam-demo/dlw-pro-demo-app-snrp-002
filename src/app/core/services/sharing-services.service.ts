import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IDataFiltrosCartaOrden, IVerEditarCartaOrden } from 'src/app/interfaces/carta-orden.interface';
import { ComprobantePagoValue, IDataFiltrosRecibo, IVerEditarComprobantePago } from 'src/app/interfaces/comprobante-pago.interface';
import { IDataFiltrosRecaudacionME, IRecaudacionMediosElectronicos } from 'src/app/interfaces/consolidacion';
import { IDataFiltrosRecaudacionSprl, IRecaudacionSprl } from 'src/app/interfaces/consolidacion-sprl-virtual';
import { IDataFiltrosRecaudacionPagaloPe, IRecaudacionPagaloPe } from 'src/app/interfaces/consolidacion-pagalo-pe';
import { IDataFiltrosRecaudacionMacmype, IRecaudacionMacmype } from 'src/app/interfaces/consolidacion-macmype';
import { IDataFiltrosRecaudacionScunacRemesas, IRecaudacionScunacRemesas } from 'src/app/interfaces/consolidacion-scunac-remesas';
import { IDataFiltrosRecaudacionSiafMef, IRecaudacionSiafMef } from 'src/app/interfaces/consolidacion-siaf-mef';
import { IDataFiltrosRecaudacionCciSprl, IRecaudacionCciSprl } from 'src/app/interfaces/consolidacion-cci-sprl';
import { IDataFiltrosRecaudacionHermes, IRecaudacionHermes } from 'src/app/interfaces/consolidacion-hermes';

import { IDataFiltrosRecaudacionPos, IRecaudacionPos } from 'src/app/interfaces/consolidacion-pos';
import { IDataFiltrosRecaudacionAppQrSid, IRecaudacionAppQrSid } from 'src/app/interfaces/consolidacion-app-qr-sid';

import { IDataFiltrosDocumentosVarios } from 'src/app/interfaces/documentos-varios.interface';
import { IDataFiltrosDocumentosEnviar } from 'src/app/interfaces/documentos-enviar.interface';
import { IBandejaProcess } from 'src/app/interfaces/global.interface';
import { IDataFiltrosMemorandum } from 'src/app/interfaces/memorandum.interface';
import { IDataFiltrosDocumentoFirma } from '../../interfaces/firmar-documentos.interface';
import { IDataFiltrosCheques } from '../../interfaces/cheque.interface';
import { IBandejaConfiguracion } from '../../interfaces/global.interface';
import { ConfigurarDocumento } from '../../models/mantenimientos/configuracion/config-documento.model';
import { IDataFiltrosBancariaAppQrSid, IBancariaAppQrSid } from '../../interfaces/consolidacion-bancaria-app-qr-sid.interface';
import { IDataFiltrosTipoCartaOrden } from '../../interfaces/configuracion/tipo-carta-orden.interface';
import { IDataFiltrosConfiguracion } from '../../interfaces/configuracion/configurar-documento.interface';
@Injectable({
    providedIn: 'root'
})
export class SharingInformationService {
    
    private irBandejaConfigurarDoucmento: boolean = true;
    private irBandejaTipoCartaOrden: boolean = true;
    private irBandejaMemorandum: boolean = true;
    private irBandejaCartaOrden: boolean = true;
    private irBandejaComprobantePago: boolean = true;
    private irBandejaDocumentosVarios: boolean = true;
    private irBandejaDocumentosEnviar: boolean = true;
    private irBandejaRecaudacionMediosElectronicos: boolean = true;
    private irBandejaRecaudacionSprl: boolean = true;
    private irBandejaRecaudacionPagaloPe: boolean = true;
    private irBandejaRecaudacionMacmype: boolean = true;
    private irBandejaRecaudacionScunacRemesas: boolean = true;
    private irBandejaRecaudacionPos: boolean = true;
    private irBandejaRecaudacionSiafMef: boolean = true;
    private irBandejaRecaudacionCciSprl: boolean = true;
    private irBandejaRecaudacionHermes: boolean = true;
    private irBandejaRecaudacionAppQrSid: boolean = true;
    private irBandejaBancariaAppQrSid: boolean = true;

    public cartaOrdenProceso: IBandejaProcess = {
        idMemo: null, 
        proceso: ''         
    }   
    // public isEditCartaOrden = false;
    
    private dataFiltrosCartaOrden: IDataFiltrosCartaOrden = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaTipoCartaOrden: [],
        listaEstadoCartaOrden: [],
        listaGrupos: [],
        bodyCartaOrden: null,
        esBusqueda: false,
        esCancelar: false
    }

    private memorandumProceso: IBandejaProcess = {
        idMemo: null, 
        proceso: ''         
    }

    private dataFiltrosMemorandum: IDataFiltrosMemorandum = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaTipoMemorandum: [],
        listaEstadoMemorandum: [],
        bodyMemorandum: null,
        esBusqueda: false,
        esCancelar: false
    }

    private comprobantePago: ComprobantePagoValue = null;
    
    private comprobantePagoProceso: IBandejaProcess = {
        idMemo: null, 
        proceso: ''         
    }

    private dataFiltrosComprobantePago: IDataFiltrosRecibo = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaTipoRecibo: [],
        listaEstadoRecibo: [],
        listaMediosPago: [],
        bodyRecibo: null,
        esBusqueda: false,
        esCancelar: false
    }

    private tipoCartaOrdenProceso: IBandejaProcess = {
        idMemo: null,
        proceso: ''
    }

    private dataFiltrosTipoCartaOrden: IDataFiltrosTipoCartaOrden = {
        bodyRecibo: null,
        esBusqueda: false,
        esCancelar: false
    }

    private configurarDocumento: ConfigurarDocumento = null;

    private dataFiltrosConfigurarDocumento: IDataFiltrosConfiguracion = {
        bodyConfig: null,
        esBusqueda: false,
        esCancelar: false
    }

    private configurarDocumentoProceso: IBandejaConfiguracion = {
        configDocu: null,
        tipoDocu: '',
        proceso: ''
    }

    private dataFiltrosCheques: IDataFiltrosCheques = {
        listaZonaRegistral: [],
        listaOficinaRegistral: [],
        listaLocal: [],
        listaEstadoCheque: [],
        listaBancos: [],
        listaCuentas: [],
        bodyCheques: null,
        esBusqueda: false,
        esCancelar: false
    }

    private documentosVariosProceso: IBandejaProcess = {
        idMemo: null, 
        proceso: ''         
    }

    private dataFiltrosDocumentosVarios: IDataFiltrosDocumentosVarios = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaTipoDocumento: [],
        listaEstadoRecibo: [],
        bodyDocumentosVarios: null,
        esBusqueda: false,
        esCancelar: false
    }

    private dataFiltrosDocumentoFirma: IDataFiltrosDocumentoFirma = {
        listaZonaRegistral: [],  
        listaOficinaRegistral: [],
        listaLocal: [],
        listaTipoDocumento: [],
        listaPerfil: [],
        listaEstado: [],
        bodyDocumentoFirma: null, 
        esBusqueda: false,
        esCancelar: false
    }


    private documentosEnviarProceso: IBandejaProcess = {
        idMemo: null, 
        proceso: ''         
    }

    private dataFiltrosDocumentosEnviar: IDataFiltrosDocumentosEnviar = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaTipoDocumento: [],
        listaEstadoRecibo: [],
        bodyDocumentosEnviar: null,
        esBusqueda: false,
        esCancelar: false
    }

    private dataFiltrosRecaudacionME: IDataFiltrosRecaudacionME = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaEstado: [],        
        bodyRecaudacionME: null,
        esBusqueda: false,
        esCancelar: false
    }

    private recaudacionMediosElectronicos: IRecaudacionMediosElectronicos = null;
    private esConsultarMediosElectronicos: boolean = false;

    private dataFiltrosRecaudacionSprl: IDataFiltrosRecaudacionSprl = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaOperador: [],
        listaEstado: [],        
        bodyRecaudacionSprl: null,
        esBusqueda: false,
        esCancelar: false
    }

    private recaudacionSprl: IRecaudacionSprl = null;
    private esConsultarSprl: boolean = false;


    private dataFiltrosRecaudacionPagaloPe: IDataFiltrosRecaudacionPagaloPe = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaEstado: [],        
        bodyRecaudacionPagaloPe: null,
        esBusqueda: false,
        esCancelar: false
    }

    private recaudacionPagaloPe: IRecaudacionPagaloPe = null;
    private esConsultarPagaloPe: boolean = false;


    private dataFiltrosRecaudacionMacmype: IDataFiltrosRecaudacionMacmype = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaEstado: [],        
        bodyRecaudacionMacmype: null,
        esBusqueda: false,
        esCancelar: false
    }

    private recaudacionMacmype: IRecaudacionMacmype = null;
    private esConsultarMacmype: boolean = false;


    private dataFiltrosRecaudacionScunacRemesas: IDataFiltrosRecaudacionScunacRemesas = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaEstado: [],        
        bodyRecaudacionScunacRemesas: null,
        esBusqueda: false,
        esCancelar: false
    }

    private recaudacionScunacRemesas: IRecaudacionScunacRemesas = null;
    private esConsultarScunacRemesas: boolean = false;

// --------------------

    private dataFiltrosRecaudacionPos: IDataFiltrosRecaudacionPos = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaOperador: [],
        listaEstado: [],        
        bodyRecaudacionPos: null,
        esBusqueda: false,
        esCancelar: false
    }

    private recaudacionPos: IRecaudacionPos = null;
    private esConsultarPos: boolean = false;


    private dataFiltrosRecaudacionSiafMef: IDataFiltrosRecaudacionSiafMef = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaOperador: [],
        listaEstado: [],        
        bodyRecaudacionSiafMef: null,
        esBusqueda: false,
        esCancelar: false
    }

    private recaudacionSiafMef: IRecaudacionSiafMef = null;
    private esConsultarSiafMef: boolean = false;


    private dataFiltrosRecaudacionCciSprl: IDataFiltrosRecaudacionCciSprl = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaEstado: [],        
        bodyRecaudacionCciSprl: null,
        esBusqueda: false,
        esCancelar: false
    }

    private recaudacionCciSprl: IRecaudacionCciSprl = null;
    private esConsultarCciSprl: boolean = false;


    private dataFiltrosRecaudacionHermes: IDataFiltrosRecaudacionHermes = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaEstado: [],        
        bodyRecaudacionHermes: null,
        esBusqueda: false,
        esCancelar: false
    }

    private recaudacionHermes: IRecaudacionHermes = null;
    private esConsultarHermes: boolean = false;


    private dataFiltrosRecaudacionAppQrSid: IDataFiltrosRecaudacionAppQrSid = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaTipoConciliacion: [],
        listaOperador: [],
        listaEstado: [],        
        bodyRecaudacionAppQrSid: null,
        esBusqueda: false,
        esCancelar: false
    }

    private recaudacionAppQrSid: IRecaudacionAppQrSid = null;
    private esConsultarAppQrSid: boolean = false;


    private dataFiltrosBancariaAppQrSid: IDataFiltrosBancariaAppQrSid = {
        listaZonaRegistral: [], 
        listaOficinaRegistral: [],
        listaLocal: [],
        listaTipoConciliacion: [],
        listaOperador: [],
        listaEstado: [],        
        bodyBancariaAppQrSid: null,
        esBusqueda: false,
        esCancelar: false
    }

    private bancariaAppQrSid: IBancariaAppQrSid = null;
    private esConsultarBancariaAppQrSid: boolean = false;


    private irRutaBandejaConfiguraDocumento: BehaviorSubject<boolean> = new BehaviorSubject<boolean>( this.irBandejaConfigurarDoucmento );
    private irRutaBandejaTipoCartaOrden: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaTipoCartaOrden);
    private irRutaBandejaMemorandum: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaMemorandum);
    private irRutaBandejaCartaOrden: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaCartaOrden);
    private irRutaBandejaComprobantePago: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaComprobantePago);
    private irRutaBandejaDocumentosVarios: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaDocumentosVarios);
    private irRutaBandejaDocumentosEnviar: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaDocumentosEnviar);
    private irRutaBandejaRecaudacionMediosElectronicos: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaRecaudacionMediosElectronicos);
    private irRutaBandejaRecaudacionSprl: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaRecaudacionSprl);
    private irRutaBandejaRecaudacionPagaloPe: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaRecaudacionPagaloPe);
    private irRutaBandejaRecaudacionMacmype: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaRecaudacionMacmype);
    private irRutaBandejaRecaudacionScunacRemesas: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaRecaudacionScunacRemesas);

    private irRutaBandejaRecaudacionPos: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaRecaudacionPos);
    private irRutaBandejaRecaudacionSiafMef: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaRecaudacionSiafMef);
    private irRutaBandejaRecaudacionCciSprl: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaRecaudacionCciSprl);
    private irRutaBandejaRecaudacionHermes: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaRecaudacionHermes);

    private irRutaBandejaRecaudacionAppQrSid: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaRecaudacionAppQrSid);
    private irRutaBandejaBancariaAppQrSid: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.irBandejaBancariaAppQrSid);

    private compartirCartaOrdenProceso: BehaviorSubject<IBandejaProcess> = new BehaviorSubject<IBandejaProcess>(this.cartaOrdenProceso);
    private compartirDataFiltrosCartaOrden: BehaviorSubject<IDataFiltrosCartaOrden> = new BehaviorSubject<IDataFiltrosCartaOrden>(this.dataFiltrosCartaOrden);    
    private compartirMemorandumProceso: BehaviorSubject<IBandejaProcess> = new BehaviorSubject<IBandejaProcess>(this.memorandumProceso);    
    private compartirDataFiltrosMemorandum: BehaviorSubject<IDataFiltrosMemorandum> = new BehaviorSubject<IDataFiltrosMemorandum>(this.dataFiltrosMemorandum);    
    private compartirComprobantePago: BehaviorSubject<ComprobantePagoValue> = new BehaviorSubject<ComprobantePagoValue>(this.comprobantePago);
    private compartirComprobantePagoProceso: BehaviorSubject<IBandejaProcess> = new BehaviorSubject<IBandejaProcess>(this.comprobantePagoProceso);
    private compartirConfigurarDocumento: BehaviorSubject<ConfigurarDocumento> = new BehaviorSubject<ConfigurarDocumento>(this.configurarDocumento);
    private compartirConfigurarDocumentoProceso: BehaviorSubject<IBandejaConfiguracion> = new BehaviorSubject<IBandejaConfiguracion>( this.configurarDocumentoProceso );
    private compartirDataFiltrosConfigurarDocumentoOrden: BehaviorSubject<IDataFiltrosConfiguracion> = new BehaviorSubject<IDataFiltrosConfiguracion>( this.dataFiltrosConfigurarDocumento );
    private compartirDataFiltrosComprobantePagoOrden: BehaviorSubject<IDataFiltrosRecibo> = new BehaviorSubject<IDataFiltrosRecibo>(this.dataFiltrosComprobantePago);     
    private compartirTipoCartaOrdenProceso: BehaviorSubject<IBandejaProcess> = new BehaviorSubject<IBandejaProcess>(this.tipoCartaOrdenProceso);
    private compartirDataFiltrosTipoCartaOrdenOrden: BehaviorSubject<IDataFiltrosTipoCartaOrden> = new BehaviorSubject<IDataFiltrosTipoCartaOrden>(this.dataFiltrosTipoCartaOrden);
    private compartirDocumentosVariosProceso: BehaviorSubject<IBandejaProcess> = new BehaviorSubject<IBandejaProcess>(this.comprobantePagoProceso);
    private compartirDocumentosEnviarProceso: BehaviorSubject<IBandejaProcess> = new BehaviorSubject<IBandejaProcess>(this.comprobantePagoProceso);
    private compartirDataFiltrosDocumentosFirma: BehaviorSubject<IDataFiltrosDocumentoFirma> = new BehaviorSubject<IDataFiltrosDocumentoFirma>(this.dataFiltrosDocumentoFirma);
    private compartirDataFiltrosDocumentosVarios: BehaviorSubject<IDataFiltrosDocumentosVarios> = new BehaviorSubject<IDataFiltrosDocumentosVarios>(this.dataFiltrosDocumentosVarios);     
    private compartirDataFiltrosDocumentosEnviar: BehaviorSubject<IDataFiltrosDocumentosEnviar> = new BehaviorSubject<IDataFiltrosDocumentosEnviar>(this.dataFiltrosDocumentosEnviar);     
    private compartirDataFiltrosCheques: BehaviorSubject<IDataFiltrosCheques> = new BehaviorSubject<IDataFiltrosCheques>(this.dataFiltrosCheques);
    private compartirDataFiltrosRecaudacionMediosElectronicos: BehaviorSubject<IDataFiltrosRecaudacionME> = new BehaviorSubject<IDataFiltrosRecaudacionME>(this.dataFiltrosRecaudacionME);    
    private compartirRecaudacionMediosElectronicos: BehaviorSubject<IRecaudacionMediosElectronicos> = new BehaviorSubject<IRecaudacionMediosElectronicos>(this.recaudacionMediosElectronicos);    
    private compartirEsConsultarMediosElectronicos: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.esConsultarMediosElectronicos);    
    private compartirDataFiltrosRecaudacionSprl: BehaviorSubject<IDataFiltrosRecaudacionSprl> = new BehaviorSubject<IDataFiltrosRecaudacionSprl>(this.dataFiltrosRecaudacionSprl);    
    private compartirRecaudacionSprl: BehaviorSubject<IRecaudacionSprl> = new BehaviorSubject<IRecaudacionSprl>(this.recaudacionSprl);    
    private compartirEsConsultarSprl: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.esConsultarSprl);    
    private compartirDataFiltrosRecaudacionPagaloPe: BehaviorSubject<IDataFiltrosRecaudacionPagaloPe> = new BehaviorSubject<IDataFiltrosRecaudacionPagaloPe>(this.dataFiltrosRecaudacionPagaloPe);    
    private compartirRecaudacionPagaloPe: BehaviorSubject<IRecaudacionPagaloPe> = new BehaviorSubject<IRecaudacionPagaloPe>(this.recaudacionPagaloPe);    
    private compartirEsConsultarPagaloPe: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.esConsultarPagaloPe);    
    private compartirDataFiltrosRecaudacionMacmype: BehaviorSubject<IDataFiltrosRecaudacionMacmype> = new BehaviorSubject<IDataFiltrosRecaudacionMacmype>(this.dataFiltrosRecaudacionMacmype);    
    private compartirRecaudacionMacmype: BehaviorSubject<IRecaudacionMacmype> = new BehaviorSubject<IRecaudacionMacmype>(this.recaudacionMacmype);    
    private compartirEsConsultarMacmype: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.esConsultarMacmype);    
    private compartirDataFiltrosRecaudacionScunacRemesas: BehaviorSubject<IDataFiltrosRecaudacionScunacRemesas> = new BehaviorSubject<IDataFiltrosRecaudacionScunacRemesas>(this.dataFiltrosRecaudacionScunacRemesas);    
    private compartirRecaudacionScunacRemesas: BehaviorSubject<IRecaudacionScunacRemesas> = new BehaviorSubject<IRecaudacionScunacRemesas>(this.recaudacionScunacRemesas);    
    private compartirEsConsultarScunacRemesas: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.esConsultarScunacRemesas);    

    private compartirDataFiltrosRecaudacionPos: BehaviorSubject<IDataFiltrosRecaudacionPos> = new BehaviorSubject<IDataFiltrosRecaudacionPos>(this.dataFiltrosRecaudacionPos);    
    private compartirRecaudacionPos: BehaviorSubject<IRecaudacionPos> = new BehaviorSubject<IRecaudacionPos>(this.recaudacionPos);    
    private compartirEsConsultarPos: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.esConsultarPos);    

    private compartirDataFiltrosRecaudacionSiafMef: BehaviorSubject<IDataFiltrosRecaudacionSiafMef> = new BehaviorSubject<IDataFiltrosRecaudacionSiafMef>(this.dataFiltrosRecaudacionSiafMef);    
    private compartirRecaudacionSiafMef: BehaviorSubject<IRecaudacionSiafMef> = new BehaviorSubject<IRecaudacionSiafMef>(this.recaudacionSiafMef);    
    private compartirEsConsultarSiafMef: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.esConsultarSiafMef);    
    private compartirDataFiltrosRecaudacionCciSprl: BehaviorSubject<IDataFiltrosRecaudacionCciSprl> = new BehaviorSubject<IDataFiltrosRecaudacionCciSprl>(this.dataFiltrosRecaudacionCciSprl);    
    private compartirRecaudacionCciSprl: BehaviorSubject<IRecaudacionCciSprl> = new BehaviorSubject<IRecaudacionCciSprl>(this.recaudacionCciSprl);    
    private compartirEsConsultarCciSprl: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.esConsultarCciSprl);    

    private compartirDataFiltrosRecaudacionHermes: BehaviorSubject<IDataFiltrosRecaudacionHermes> = new BehaviorSubject<IDataFiltrosRecaudacionHermes>(this.dataFiltrosRecaudacionHermes);    
    private compartirRecaudacionHermes: BehaviorSubject<IRecaudacionHermes> = new BehaviorSubject<IRecaudacionHermes>(this.recaudacionHermes);    
    private compartirEsConsultarHermes: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.esConsultarHermes);    

    private compartirDataFiltrosRecaudacionAppQrSid: BehaviorSubject<IDataFiltrosRecaudacionAppQrSid> = new BehaviorSubject<IDataFiltrosRecaudacionAppQrSid>(this.dataFiltrosRecaudacionAppQrSid);    
    private compartirRecaudacionAppQrSid: BehaviorSubject<IRecaudacionAppQrSid> = new BehaviorSubject<IRecaudacionAppQrSid>(this.recaudacionAppQrSid);    
    private compartirEsConsultarAppQrSid: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.esConsultarAppQrSid);    

    private compartirDataFiltrosBancariaAppQrSid: BehaviorSubject<IDataFiltrosBancariaAppQrSid> = new BehaviorSubject<IDataFiltrosBancariaAppQrSid>(this.dataFiltrosBancariaAppQrSid);
    private compartirBancariaAppQrSid: BehaviorSubject<IBancariaAppQrSid> = new BehaviorSubject<IBancariaAppQrSid>(this.bancariaAppQrSid);
    private compartirEsConsultarBancariaAppQrSid: BehaviorSubject<boolean> = new BehaviorSubject<boolean>( this.esConsultarBancariaAppQrSid );

    get irRutaBandejaConfigurarDocumentoObservable() {
        return this.irRutaBandejaConfiguraDocumento.asObservable();
    }

    set irRutaBandejaConfigurarDocumentoObservableData( data: boolean ) {
        this.irRutaBandejaConfiguraDocumento.next(data);
    }

    get irRutaBandejaTipoCartaOrdenObservable() {
        return this.irRutaBandejaTipoCartaOrden.asObservable();
    }

    set irRutaBandejaTipoCartaOrdenObservableData(data: boolean) {             
        this.irRutaBandejaTipoCartaOrden.next(data);
    }
    
    get irRutaBandejaCartaOrdenObservable() {
        return this.irRutaBandejaCartaOrden.asObservable();
    }

    set irRutaBandejaCartaOrdenObservableData(data: boolean) {             
        this.irRutaBandejaCartaOrden.next(data);
    }

    get irRutaBandejaMemorandumObservable() {
        return this.irRutaBandejaMemorandum.asObservable();
    }

    set irRutaBandejaMemorandumObservableData(data: boolean) {             
        this.irRutaBandejaMemorandum.next(data);
    }

    get irRutaBandejaComprobantePagoObservable() {
        return this.irRutaBandejaComprobantePago.asObservable();
    }

    set irRutaBandejaComprobantePagoObservableData(data: boolean) {             
        this.irRutaBandejaComprobantePago.next(data);
    }

    get irRutaBandejaDocumentosVariosObservable() {
        return this.irRutaBandejaDocumentosVarios.asObservable();
    }

    set irRutaBandejaDocumentosVariosObservableData(data: boolean) {             
        this.irRutaBandejaDocumentosVarios.next(data);
    }

    get irRutaBandejaDocumentosEnviarObservable() {
        return this.irRutaBandejaDocumentosEnviar.asObservable();
    }

    set irRutaBandejaDocumentosEnviarObservableData(data: boolean) {             
        this.irRutaBandejaDocumentosEnviar.next(data);
    }

    get irRutaBandejaRecaudacionMediosElectronicosObservable() {
        return this.irRutaBandejaRecaudacionMediosElectronicos.asObservable();
    }

    set irRutaBandejaRecaudacionMediosElectronicosObservableData(data: boolean) {             
        this.irRutaBandejaRecaudacionMediosElectronicos.next(data);
    }


    get irRutaBandejaRecaudacionSprlObservable() {
        return this.irRutaBandejaRecaudacionSprl.asObservable();
    }

    set irRutaBandejaRecaudacionSprlObservableData(data: boolean) {             
        this.irRutaBandejaRecaudacionSprl.next(data);
    }


    get irRutaBandejaRecaudacionPagaloPeObservable() {
        return this.irRutaBandejaRecaudacionPagaloPe.asObservable();
    }

    set irRutaBandejaRecaudacionPagaloPeObservableData(data: boolean) {             
        this.irRutaBandejaRecaudacionPagaloPe.next(data);
    }


    get irRutaBandejaRecaudacionMacmypeObservable() {
        return this.irRutaBandejaRecaudacionMacmype.asObservable();
    }

    set irRutaBandejaRecaudacionMacmypeObservableData(data: boolean) {             
        this.irRutaBandejaRecaudacionMacmype.next(data);
    }


    get irRutaBandejaRecaudacionScunacRemesasObservable() {
        return this.irRutaBandejaRecaudacionScunacRemesas.asObservable();
    }

    set irRutaBandejaRecaudacionScunacRemesasObservableData(data: boolean) {             
        this.irRutaBandejaRecaudacionScunacRemesas.next(data);
    }



    get irRutaBandejaRecaudacionPosObservable() {
        return this.irRutaBandejaRecaudacionPos.asObservable();
    }

    set irRutaBandejaRecaudacionPosObservableData(data: boolean) {             
        this.irRutaBandejaRecaudacionPos.next(data);
    }



    get irRutaBandejaRecaudacionSiafMefObservable() {
        return this.irRutaBandejaRecaudacionSiafMef.asObservable();
    }

    set irRutaBandejaRecaudacionSiafMefObservableData(data: boolean) {             
        this.irRutaBandejaRecaudacionSiafMef.next(data);
    }


    get irRutaBandejaRecaudacionCciSprlObservable() {
        return this.irRutaBandejaRecaudacionCciSprl.asObservable();
    }

    set irRutaBandejaRecaudacionCciSprlObservableData(data: boolean) {             
        this.irRutaBandejaRecaudacionCciSprl.next(data);
    }


    get irRutaBandejaRecaudacionHermesObservable() {
        return this.irRutaBandejaRecaudacionHermes.asObservable();
    }

    set irRutaBandejaRecaudacionHermesObservableData(data: boolean) {             
        this.irRutaBandejaRecaudacionHermes.next(data);
    }


    get irRutaBandejaRecaudacionAppQrSidObservable() {
        return this.irRutaBandejaRecaudacionAppQrSid.asObservable();
    }

    set irRutaBandejaRecaudacionAppQrSidObservableData(data: boolean) {             
        this.irRutaBandejaRecaudacionAppQrSid.next(data);
    }

    set irRutaBandejaBancariaAppQrSidObservableData( data: boolean ) {
        this.irRutaBandejaBancariaAppQrSid.next( data );
    }


//-------------------------------
    get compartirCartaOrdenProcesoObservable() {
        return this.compartirCartaOrdenProceso.asObservable();
    }

    set compartirCartaOrdenProcesoObservableData(data: IBandejaProcess) {
        this.compartirCartaOrdenProceso.next(data);
    }

    get compartirDataFiltrosCartaOrdenObservable() {
        return this.compartirDataFiltrosCartaOrden.asObservable();
    }

    set compartirDataFiltrosCartaOrdenObservableData(data: IDataFiltrosCartaOrden) {      
        this.compartirDataFiltrosCartaOrden.next(data);
    }
//------------------------------------
    get compartirMemorandumProcesobservable() {
        return this.compartirMemorandumProceso.asObservable();
    }

    set compartirMemorandumProcesobservableData(data: IBandejaProcess) {      
        this.compartirMemorandumProceso.next(data);
    }

    get compartirDataFiltrosMemorandumObservable() {
        return this.compartirDataFiltrosMemorandum.asObservable();
    }

    set compartirDataFiltrosMemorandumObservableData(data: IDataFiltrosMemorandum) {      
        this.compartirDataFiltrosMemorandum.next(data);
    }
//-------------------------------------

    get compartirComprobantePagoObservable() {
        return this.compartirComprobantePago.asObservable();
    }

    set compartirComprobantePagoObservableData(data: ComprobantePagoValue) {
        this.compartirComprobantePago.next(data);
    }

    get compartirComprobantePagoProcesoObservable() {
        return this.compartirComprobantePagoProceso.asObservable();
    }

    set compartirComprobantePagoProcesoObservableData(data: IBandejaProcess) {
        this.compartirComprobantePagoProceso.next(data);
    }

    get compartirDataFiltrosComprobantePagoObservable() {
        return this.compartirDataFiltrosComprobantePagoOrden.asObservable();
    }

    set compartirDataFiltrosComprobantePagoObservableData(data: IDataFiltrosRecibo) {   
        this.compartirDataFiltrosComprobantePagoOrden.next(data);
    }

//------------------------------------

    get compartirTipoCartaOrdenProcesoObservable() {
        return this.compartirTipoCartaOrdenProceso.asObservable();
    }

    set compartirTipoCartaOrdenProcesoObservableData(data: IBandejaProcess) {
        this.compartirTipoCartaOrdenProceso.next(data);
    }
    
    get compartirDataFiltrosTipoCartaOrdenObservable() {
        return this.compartirDataFiltrosTipoCartaOrdenOrden.asObservable();
    }

    set compartirDataFiltrosTipoCartaOrdenObservableData(data: IDataFiltrosTipoCartaOrden) {   
        this.compartirDataFiltrosTipoCartaOrdenOrden.next(data);
    }

//------------------------------------

    get compartirConfigurarDocumentoObservable() {
        return this.compartirConfigurarDocumentoProceso.asObservable();
    }
    set compartirConfigurarDocumentoProcesoObservableData( data: IBandejaConfiguracion ) {
        this.compartirConfigurarDocumentoProceso.next( data );
    }

    get compartirDataFiltrosConfiguracionDocumentoObservable() {
        return this.compartirDataFiltrosConfigurarDocumentoOrden.asObservable();
    }

    set compartirDataFiltrosConfigurarDocumentoObservableData( data: IDataFiltrosConfiguracion ) {
        this.compartirDataFiltrosConfigurarDocumentoOrden.next(data);
    }

//-------------------------------------

    get compartirDataFiltrosChequesObservable() {
        return this.compartirDataFiltrosCheques.asObservable();
    }

    set compartirDataFiltrosChequesObservableData(data: IDataFiltrosCheques) {
        this.compartirDataFiltrosCheques.next(data);
    }
    
//-------------------------------------

    get compartirDataFiltrosDocumentoFirmaObservable() {
        return this.compartirDataFiltrosDocumentosFirma.asObservable();
    }

    set compartirDataFiltrosDocumentoFirmaObservableData(data: IDataFiltrosDocumentoFirma) {
        this.compartirDataFiltrosDocumentosFirma.next(data);
    } 
    
//-------------------------------------

    get compartirDocumentosVariosProcesoObservable() {
        return this.compartirDocumentosVariosProceso.asObservable();
    }

    set compartirDocumentosVariosProcesoObservableData(data: IBandejaProcess) {
        this.compartirDocumentosVariosProceso.next(data);
    }

    get compartirDataFiltrosDocumentosVariosObservable() {
        return this.compartirDataFiltrosDocumentosVarios.asObservable();
    }

    set compartirDataFiltrosDocumentosVariosObservableData(data: IDataFiltrosDocumentosVarios) {   
        console.log("data", data);
        this.compartirDataFiltrosDocumentosVarios.next(data);
    }

//-------------------------------------

    get compartirDocumentosEnviarProcesoObservable() {
        return this.compartirDocumentosEnviarProceso.asObservable();
    }

    set compartirDocumentosEnviarProcesoObservableData(data: IBandejaProcess) {
        this.compartirDocumentosEnviarProceso.next(data);
    }

    get compartirDataFiltrosDocumentosEnviarObservable() {
        return this.compartirDataFiltrosDocumentosEnviar.asObservable();
    }

    set compartirDataFiltrosDocumentosEnviarObservableData(data: IDataFiltrosDocumentosEnviar) {   
        console.log("data", data);
        this.compartirDataFiltrosDocumentosEnviar.next(data);
    }

//-------------------------------------


    get compartirDataFiltrosRecaudacionMediosElectronicosObservable() {
        return this.compartirDataFiltrosRecaudacionMediosElectronicos.asObservable();
    }

    set compartirDataFiltrosRecaudacionMediosElectronicosObservableData(data: IDataFiltrosRecaudacionME) {      
        this.compartirDataFiltrosRecaudacionMediosElectronicos.next(data);
    }

    get compartirRecaudacionMediosElectronicosObservable() {
        return this.compartirRecaudacionMediosElectronicos.asObservable();
    }

    set compartirRecaudacionMediosElectronicosObservableData(data: IRecaudacionMediosElectronicos) {      
        this.compartirRecaudacionMediosElectronicos.next(data);
    }

    get compartirEsConsultarMediosElectronicosObservable() {
        return this.compartirEsConsultarMediosElectronicos.asObservable();
    }

    set compartirEsConsultarMediosElectronicoObservableData(data: boolean) {      
        this.compartirEsConsultarMediosElectronicos.next(data);
    }

//-------------------------------------


    get compartirDataFiltrosRecaudacionSprlObservable() {
        return this.compartirDataFiltrosRecaudacionSprl.asObservable();
    }

    set compartirDataFiltrosRecaudacionSprlObservableData(data: IDataFiltrosRecaudacionSprl) {      
        this.compartirDataFiltrosRecaudacionSprl.next(data);
    }

    get compartirRecaudacionSprlObservable() {
        return this.compartirRecaudacionSprl.asObservable();
    }

    set compartirRecaudacionSprlObservableData(data: IRecaudacionSprl) {      
        this.compartirRecaudacionSprl.next(data);
    }

    get compartirEsConsultarSprlObservable() {
        return this.compartirEsConsultarSprl.asObservable();
    }

    set compartirEsConsultarSprlObservableData(data: boolean) {      
        this.compartirEsConsultarSprl.next(data);
    }

//-------------------------------------


    get compartirDataFiltrosRecaudacionPagaloPeObservable() {
        return this.compartirDataFiltrosRecaudacionPagaloPe.asObservable();
    }

    set compartirDataFiltrosRecaudacionPagaloPeObservableData(data: IDataFiltrosRecaudacionPagaloPe) {      
        this.compartirDataFiltrosRecaudacionPagaloPe.next(data);
    }

    get compartirRecaudacionPagaloPeObservable() {
        return this.compartirRecaudacionPagaloPe.asObservable();
    }

    set compartirRecaudacionPagaloPeObservableData(data: IRecaudacionPagaloPe) {      
        this.compartirRecaudacionPagaloPe.next(data);
    }

    get compartirEsConsultarPagaloPeObservable() {
        return this.compartirEsConsultarPagaloPe.asObservable();
    }

    set compartirEsConsultarPagaloPeObservableData(data: boolean) {      
        this.compartirEsConsultarPagaloPe.next(data);
    }

//-------------------------------------


    get compartirDataFiltrosRecaudacionMacmypeObservable() {
        return this.compartirDataFiltrosRecaudacionMacmype.asObservable();
    }

    set compartirDataFiltrosRecaudacionMacmypeObservableData(data: IDataFiltrosRecaudacionMacmype) {      
        this.compartirDataFiltrosRecaudacionMacmype.next(data);
    }

    get compartirRecaudacionMacmypeObservable() {
        return this.compartirRecaudacionMacmype.asObservable();
    }

    set compartirRecaudacionMacmypeObservableData(data: IRecaudacionMacmype) {      
        this.compartirRecaudacionMacmype.next(data);
    }

    get compartirEsConsultarMacmypeObservable() {
        return this.compartirEsConsultarMacmype.asObservable();
    }

    set compartirEsConsultarMacmypeObservableData(data: boolean) {      
        this.compartirEsConsultarMacmype.next(data);
    }
	
//-------------------------------------


    get compartirDataFiltrosRecaudacionScunacRemesasObservable() {
        return this.compartirDataFiltrosRecaudacionScunacRemesas.asObservable();
    }

    set compartirDataFiltrosRecaudacionScunacRemesasObservableData(data: IDataFiltrosRecaudacionScunacRemesas) {      
        this.compartirDataFiltrosRecaudacionScunacRemesas.next(data);
    }

    get compartirRecaudacionScunacRemesasObservable() {
        return this.compartirRecaudacionScunacRemesas.asObservable();
    }

    set compartirRecaudacionScunacRemesasObservableData(data: IRecaudacionScunacRemesas) {      
        this.compartirRecaudacionScunacRemesas.next(data);
    }

    get compartirEsConsultarScunacRemesasObservable() {
        return this.compartirEsConsultarScunacRemesas.asObservable();
    }

    set compartirEsConsultarScunacRemesasObservableData(data: boolean) {      
        this.compartirEsConsultarScunacRemesas.next(data);
    }
	
	
	//-------------------------------------


    get compartirDataFiltrosRecaudacionPosObservable() {
        return this.compartirDataFiltrosRecaudacionPos.asObservable();
    }

    set compartirDataFiltrosRecaudacionPosObservableData(data: IDataFiltrosRecaudacionPos) {      
        this.compartirDataFiltrosRecaudacionPos.next(data);
    }

    get compartirRecaudacionPosObservable() {
        return this.compartirRecaudacionPos.asObservable();
    }

    set compartirRecaudacionPosObservableData(data: IRecaudacionPos) {      
        this.compartirRecaudacionPos.next(data);
    }

    get compartirEsConsultarPosObservable() {
        return this.compartirEsConsultarPos.asObservable();
    }

    set compartirEsConsultarPosObservableData(data: boolean) {      
        this.compartirEsConsultarPos.next(data);
    }
	
		
	//-------------------------------------


    get compartirDataFiltrosRecaudacionSiafMefObservable() {
        return this.compartirDataFiltrosRecaudacionSiafMef.asObservable();
    }

    set compartirDataFiltrosRecaudacionSiafMefObservableData(data: IDataFiltrosRecaudacionSiafMef) {      
        this.compartirDataFiltrosRecaudacionSiafMef.next(data);
    }

    get compartirRecaudacionSiafMefObservable() {
        return this.compartirRecaudacionSiafMef.asObservable();
    }

    set compartirRecaudacionSiafMefObservableData(data: IRecaudacionSiafMef) {      
        this.compartirRecaudacionSiafMef.next(data);
    }

    get compartirEsConsultarSiafMefObservable() {
        return this.compartirEsConsultarSiafMef.asObservable();
    }

    set compartirEsConsultarSiafMefObservableData(data: boolean) {      
        this.compartirEsConsultarSiafMef.next(data);
    }
	
	
//-------------------------------------


    get compartirDataFiltrosRecaudacionCciSprlObservable() {
        return this.compartirDataFiltrosRecaudacionCciSprl.asObservable();
    }

    set compartirDataFiltrosRecaudacionCciSprlObservableData(data: IDataFiltrosRecaudacionCciSprl) {      
        this.compartirDataFiltrosRecaudacionCciSprl.next(data);
    }

    get compartirRecaudacionCciSprlObservable() {
        return this.compartirRecaudacionCciSprl.asObservable();
    }

    set compartirRecaudacionCciSprlObservableData(data: IRecaudacionCciSprl) {      
        this.compartirRecaudacionCciSprl.next(data);
    }

    get compartirEsConsultarCciSprlObservable() {
        return this.compartirEsConsultarCciSprl.asObservable();
    }

    set compartirEsConsultarCciSprlObservableData(data: boolean) {      
        this.compartirEsConsultarCciSprl.next(data);
    }
	
	
//-------------------------------------


    get compartirDataFiltrosRecaudacionHermesObservable() {
        return this.compartirDataFiltrosRecaudacionHermes.asObservable();
    }

    set compartirDataFiltrosRecaudacionHermesObservableData(data: IDataFiltrosRecaudacionHermes) {      
        this.compartirDataFiltrosRecaudacionHermes.next(data);
    }

    get compartirRecaudacionHermesObservable() {
        return this.compartirRecaudacionHermes.asObservable();
    }

    set compartirRecaudacionHermesObservableData(data: IRecaudacionHermes) {      
        this.compartirRecaudacionHermes.next(data);
    }

    get compartirEsConsultarHermesObservable() {
        return this.compartirEsConsultarHermes.asObservable();
    }

    set compartirEsConsultarHermesObservableData(data: boolean) {      
        this.compartirEsConsultarHermes.next(data);
    }
	
	//-------------------------------------


    get compartirDataFiltrosRecaudacionAppQrSidObservable() {
        return this.compartirDataFiltrosRecaudacionAppQrSid.asObservable();
    }

    set compartirDataFiltrosRecaudacionAppQrSidObservableData(data: IDataFiltrosRecaudacionAppQrSid) {      
        this.compartirDataFiltrosRecaudacionAppQrSid.next(data);
    }

    get compartirRecaudacionAppQrSidObservable() {
        return this.compartirRecaudacionAppQrSid.asObservable();
    }

    set compartirRecaudacionAppQrSidObservableData(data: IRecaudacionAppQrSid) {      
        this.compartirRecaudacionAppQrSid.next(data);
    }

    get compartirEsConsultarAppQrSidObservable() {
        return this.compartirEsConsultarAppQrSid.asObservable();
    }

    set compartirEsConsultarAppQrSidObservableData(data: boolean) {      
        this.compartirEsConsultarAppQrSid.next(data);
    }

    //-----------------------------------------------------------

    get compartirDataFiltrosBancariaAppQrSidObservable() {
        return this.compartirDataFiltrosBancariaAppQrSid.asObservable();
    }

    set compartirDataFiltrosBancariaAppQrSidObservableData( data: IDataFiltrosBancariaAppQrSid ) {
        this.compartirDataFiltrosBancariaAppQrSid.next( data );
    }

    get compartirBancariaAppQrSidObservable() {
        return this.compartirBancariaAppQrSid.asObservable();
    }

    set compartirBancariaAppQrSidObservableData( data: IBancariaAppQrSid ) {
        this.compartirBancariaAppQrSid.next( data );
    }

    get compartirEsConsultarBancariaAppQrSidObservable() {
        return this.compartirEsConsultarBancariaAppQrSid.asObservable();
    }

    set compartirEsConsultarBancariaAppQrSidObservableData( data: boolean ) {
        this.compartirEsConsultarBancariaAppQrSid.next( data );
    }
	
	
}