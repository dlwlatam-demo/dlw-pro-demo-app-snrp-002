
export class Funciones {
    //Validar Email
    emailPattern: string = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";
    
    IsNumberKey(event: any) {
        if (event.charCode >= 48 && event.charCode <= 57) {
            return true;
        }
        return false;
    }   

    IsNumberKey2(event: any) {
      let texto: string = event.target.value;
      let existePunto: boolean = false;
      let pos: number;
      for (let i = 0; i < texto.length; i++) {
        if (texto.substring(i, i + 1) === '.') {
          pos = i;
          existePunto = true;
          break;
        }
      }
      if (texto.length === 0 && ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 45)) {        
        return true;
      } else if ((texto.length === 1 && texto.substring(0, 1) === '0' && event.charCode !== 46) && ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46)) {
        return false;
      } else if ((texto.length === 1 && texto.substring(0, 1) === '0' && event.charCode === 46) && ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46)) {
        return true;
      } else if ((texto.length === 1 && texto.substring(0, 1) === '-' && event.charCode === 46) && ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46)) {
        return false;
      } else if ((texto.length === 1 && texto.substring(0, 1) === '-' && event.charCode !== 46) && ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46)) {
        return true;
      } else if ((texto.length === 1 && texto.substring(0, 1) !== '0') && ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46)) {
        return true;
      } else if ((texto.length === 2 && texto.substring(0, 2) === '-0' && event.charCode !== 46) && ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46)) {
        return false;
      } else if ((texto.length === 2 && texto.substring(0, 2) === '-0' && event.charCode === 46) && ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46)) {
        return true;
      } else if ((texto.length > 1 && !existePunto) && ((event.charCode >= 48 && event.charCode <= 57) || event.charCode === 46)) {
        return true;
      } else if ((texto.length > 1 && existePunto && texto.length <= pos + 2) && (event.charCode >= 48 && event.charCode <= 57)) {
        return true;
      } else if ((texto.length > 1 && existePunto && texto.length > pos + 2)) {
        return false;
      }
      return false;
    } 

    IsTextKey(event: any) {      
      if (event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 32 || event.charCode == 100 
        ||  event.charCode == 225 ||  event.charCode == 233 ||  event.charCode == 237 ||  event.charCode == 243 ||  event.charCode == 250 ||  event.charCode == 193
        ||  event.charCode == 201 ||  event.charCode == 205 ||  event.charCode == 211 ||  event.charCode == 218 ||  event.charCode == 241 ||  event.charCode == 209) {
        return true;
      }
      return false;
    } 
 
    convertirFechaStringToDate(fechaString: string): Date {
      let val = fechaString.split("/");
      let date: Date = new Date(val[2] + '/' + val[1] + '/' + val[0]);
      return date;
    }

    convertirFechaDateToString(fecha: Date): string {
        let fechaResultado: string;
        let diaFecha = fecha.getDate();
        let mesFecha = fecha.getMonth() + 1;
        let anioFecha = fecha.getFullYear();
    
        let diaFinal: string;
        if (diaFecha < 10) {
          diaFinal = '0' + diaFecha;
        } else {
          diaFinal = diaFecha.toString();
        }
    
        let mesFinal: string;
        if (mesFecha < 10) {
          mesFinal = '0' + mesFecha;
        } else {
          mesFinal = mesFecha.toString();
        }
        fechaResultado = diaFinal + '/' + mesFinal + '/' + anioFecha;
        return fechaResultado;
    }

    convertirFechaLargaDateToString(fecha: Date): string {
	
      let fechaResultado: string;
      let diaFecha = fecha.getDate();
      let mesFecha = fecha.getMonth() + 1;
      let anioFecha = fecha.getFullYear();
      let hora = fecha.getHours();
      let minuto = fecha.getMinutes();
      let segundo = fecha.getSeconds();
  
      let diaFinal: string;
      if (diaFecha < 10) {
        diaFinal = '0' + diaFecha;
      } else {
        diaFinal = diaFecha.toString();
      }
  
      let mesFinal: string;
      if (mesFecha < 10) {
        mesFinal = '0' + mesFecha;
      } else {
        mesFinal = mesFecha.toString();
      }

      let horaFinal: string;
      if (hora < 10) {
        horaFinal = '0' + hora;
      } else if (hora >= 10) {
        horaFinal = hora.toString();
      } else {
        horaFinal = '00';
      }

      let minutoFinal: string;
      if (minuto < 10) {
        minutoFinal = '0' + minuto;
      } else if (minuto >= 10) {
        minutoFinal = minuto.toString();
      } else {
        minutoFinal = '00';
      }

      let segundoFinal: string;
      if (segundo < 10) {
        segundoFinal = '0' + segundo;
      } else if (segundo >= 10) {
        segundoFinal = segundo.toString();
      } else {
        segundoFinal = '00';
      }

      fechaResultado = `${diaFinal}/${mesFinal}/${anioFecha} ${horaFinal}:${minutoFinal}:${segundoFinal}`
      return fechaResultado;
  }

    eliminarEspaciosEnBlanco(texto: string): string {
      let textoFinal: string = "";
      let divisiones: any[] = texto.split(" ");
      let divionesTextoFinal: any [] = [];
      divisiones.forEach((element: string)=> {
        if (element !== "") {
          divionesTextoFinal.push(element);
        }     
      });
  
      divionesTextoFinal.forEach((element, i)=> {
        if (i === divionesTextoFinal.length - 1) {
          textoFinal += element;
        } else {
          textoFinal += element + " ";
        }       
      });    
      return textoFinal;
    }
    
    // /^(0[1-9]|1\d|2[0-3]):([0-5]\d):([0-5]\d)$/
    // /(\d{2}\/\d{2}\/\d{4})\s(\d{1,2}:\d{1,2})(:\d{1,2})?/g
    // este es dd/mm/aaaa hh:mm:ss
    // /^([0-2][0-9]|3[0-1])(\/)(0[1-9]|1[0-2])\2(\d{4})(\s)([0-1][0-9]|2[0-3])(:)([0-5][0-9])(:)([0-5][0-9])$/
    validarFormatoFecha(fecha: string): boolean {
      const DATE_REGEX = /^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
      // Comprobar formato dd/mm/yyyy, que el mes no sea mayor de 12 y los días mahyores de 31
      if (!fecha.match(DATE_REGEX)) {
        return false;
      }      
      // Comprobar los días del mes
      const day = parseInt(fecha.split('/')[0]);
      const month = parseInt(fecha.split('/')[1]);
      const year = parseInt(fecha.split('/')[2]);
      const monthDays = new Date(year, month, 0).getDate();
      if (day > monthDays) {
        return false;
      }
      return true;
    }

    validarExisteFecha(fecha: string) {
      let fechaf = fecha.split("/");
      let day = fechaf[0];
      let month = fechaf[1];
      let year = fechaf[2];
      let date = new Date(Number(year), Number(month), 0);
      if((Number(day) - 0) > (date.getDate() - 0)) {
        return false;
      }
      return true;
    }

    validarSoloNumeros(campo: any) {
      if(isNaN(campo)) {
        return false;
      } 
      return true;
    }

    // texto: 20021220
    formatearFechaCorta(texto: string) {
      const anio: string = texto.substring(0, 4);
      const mes: string = texto.substring(4, 6);
      const dia: string = texto.substring(6, 8);
      return `${dia}/${mes}/${anio} 00:00:00`;    
    }

    // texto: 03/08/2022 => 03/08/2022 00:00:00
    formatearFechaCorta2(texto: string) {     
      return `${texto} 00:00:00`;    
    }

    formatearFechaLarga(texto: string) {
	
      const anio: string = texto.substring(0, 4);
      const mes: string = texto.substring(4, 6);
      const dia: string = texto.substring(6, 8);
      const hora: string = texto.substring(8, 10);
      const minuto: string = texto.substring(10, 12);
      const segundo: string = (texto.length === 12) ? '00' : texto.substring(12, 14);
      return `${dia}/${mes}/${anio} ${hora}:${minuto}:${segundo}`;    
    }

    obtenerFechaStringDeFechaExcel(dias: number) {
      let fechaResultado: string;
      const fechainicio: Date = new Date("1900-01-01");
      const fechaResult: Date  = new Date(fechainicio.setDate(fechainicio.getDate() + dias - 1));
      fechaResultado = this.convertirFechaDateToString(fechaResult);
      return `${fechaResultado} 00:00:00`;
    }
	
	// texto: 02-10-2022
    formatearFechaCortaDMY(texto: string) {
	  const fecha: string = texto.replace(/-/g, '/');
      return `${fecha} 00:00:00`;    
    }
	
	
	// texto: 02-10-2022 00:00:00
    formatearFechaLargaDMY(texto: string) {
	  const fecha: string = texto.replace(/-/g, '/');
      return `${fecha}`;    
    }

    convertirTextoANumero( number: string ): string {
      let parteTexto: string[] = number.split('.');
      parteTexto[0] = parteTexto[0].replace(/\,/g, '');
      return parteTexto.join('.');
    }


    insertarTexto( frase: string, texto: string ) {
      const palabrasArray: string[] = frase.split(' ');
      const posicion: number = Math.floor(Math.random() * palabrasArray.length);

      palabrasArray.splice( posicion, 0, texto );
      return palabrasArray.join(' ');
    }

    insertarTextoPrueba( frase: string, texto: string, posicion: number ): string {
      let fraseCompleta: string = '';

      const parteInicial: string = frase.slice(0, posicion);
      if ( parteInicial.includes('<strong>') ) {
        fraseCompleta = parteInicial + `[${ texto }]` + frase.slice(posicion);
      } else {
        fraseCompleta = parteInicial + `<strong>[${ texto }]</strong>` + frase.slice(posicion);
      }
      return fraseCompleta; 
    }
	
    
}