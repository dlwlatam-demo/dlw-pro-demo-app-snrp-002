import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { first } from 'rxjs';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';

@Injectable({
  providedIn: 'root'
})
export class RefreshRecaudacionHermesGuard implements CanActivate {

  constructor(
    private router: Router,
    private sharingInformationService: SharingInformationService
  ) { }

  async canActivate() {    
    const irRutaBandeja: boolean = await this.sharingInformationService.irRutaBandejaRecaudacionHermesObservable.pipe(first()).toPromise();
    if (irRutaBandeja) {   
      this.router.navigate(['/SARF/procesos/bancaria/recaudacion_hermes/bandeja']);           
      return false; 
    }
    return true;
  } 
  
}
