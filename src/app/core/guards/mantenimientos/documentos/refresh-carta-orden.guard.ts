import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { first } from 'rxjs';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';

@Injectable({
  providedIn: 'root'
})
export class RefreshCartaOrdenGuard implements CanActivate {
  
  constructor(
    private router: Router,
    private sharingInformationService: SharingInformationService
  ) { }

  async canActivate() {    
    const irRutaBandeja: boolean = await this.sharingInformationService.irRutaBandejaCartaOrdenObservable.pipe(first()).toPromise();
    if (irRutaBandeja) {   
      this.router.navigate(['/SARF/mantenimientos/documentos/carta_orden/bandeja']);           
      return false; 
    }
    return true;
  } 
}
