import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { first } from 'rxjs';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';

@Injectable({
  providedIn: 'root'
})
export class RefreshDocumentosVariosGuard implements CanActivate {
  
  constructor(
    private router: Router,
    private sharingInformationService: SharingInformationService
  ) { }

  async canActivate() {    
    const irRutaBandeja: boolean = await this.sharingInformationService.irRutaBandejaDocumentosVariosObservable.pipe(first()).toPromise();
    console.log("irRutaBandeja", irRutaBandeja);
    
    if (irRutaBandeja) {   
      this.router.navigate(['/SARF/mantenimientos/documentos/documentos_varios_solicitar_firma/bandeja']);           
      return false; 
    }
    return true;
  } 
}
