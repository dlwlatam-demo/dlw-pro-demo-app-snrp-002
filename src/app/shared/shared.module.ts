import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgPrimeModule } from '../ng-prime/ng-prime.module';
import { MaterialModule } from '../material/material.module';

import { HeaderComponent } from './header/header.component';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { SublevelMenuComponent } from './sidenav/sublevel-menu.component';
import { ExcelSheetComponent } from './excel-sheet/excel-sheet.component';
import { ExcelSheet2Component } from './excel-sheet/excel-sheet2.component';
import { ExcelSheet3Component } from './excel-sheet/excel-sheet3.component';
import { SftpTextComponent } from './excel-sheet/sftp-text.component';


@NgModule({
  declarations: [
    HeaderComponent,
    MenuItemComponent,
    SidenavComponent,
    SublevelMenuComponent,
    ExcelSheetComponent,
	  ExcelSheet2Component,
	  ExcelSheet3Component,
	  SftpTextComponent
  ],
  exports: [
    HeaderComponent,
    MenuItemComponent,
    SidenavComponent,
    ExcelSheetComponent,
	  ExcelSheet2Component,
	  ExcelSheet3Component,
	  SftpTextComponent
  ],
  imports: [
    CommonModule,
    NgPrimeModule,
    MaterialModule
  ]
})
export class SharedModule { }
