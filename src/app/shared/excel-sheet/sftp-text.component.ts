import { Component, EventEmitter, Input, Output, OnInit, OnChanges } from '@angular/core';
import { UtilService } from 'src/app/services/util.service';
import { CANTIDAD_CAMPOS, TAMANO_TEXTOS, POSITION_FIELDS_PAGALO_PE, INDEX_FIELDS_PAGALO_PE, POSITION_FIELDS_MACMYPE, INDEX_FIELDS_MACMYPE, SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { IDataFileSftpText, IFileSftpText } from '../../interfaces/consolidacion-pagalo-pe';
import { FileSFTP } from 'src/app/models/procesos/consolidacion/recaudacion-pagalo-pe.model';
import { GeneralService } from '../../services/general.service';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'button-sftp-text',
  templateUrl: './sftp-text.component.html',
  styleUrls: ['./sftp-text.component.scss']
})
export class SftpTextComponent implements OnInit, OnChanges {

  public nameFile: string = '';
  public idGuid: string = '';
  public maxFileSize!: number;
  public uploadingEndpoint!: string;
  private data_source: string = '';
  private data_last: string[][] = []; 
  private numberFields: number;
  private posInitialData: number = null;
  @Input() servicioDePago: string = '';  
  @Output() eventDataFileSftpText = new EventEmitter<IDataFileSftpText>();
  @Output() uploaded = new EventEmitter<any>(); //clicked - void

  constructor(
    private funciones: Funciones,
    private utilService: UtilService,
    private generalService: GeneralService
  ) { }

  ngOnInit(): void {   
    this.uploadingEndpoint = this.generalService.getUploadingEndpoint();
    this.maxFileSize = environment.maxFileSize
  }

  public uploadFiles(event: any) { //onClick
    for (let file of event.files) {
      const f = { fileId: event.originalEvent.body.idCarga, fileName: file.name }
      this.uploaded.emit(f);
    }
    // this.clicked.emit();
  }

  public captureFileFtp(event: any) {
    console.log('eventUpload', event);
    this.uploadFiles( event );
    this.data_source = '';
    this.data_last = []; 
    const capturedFile: File = event.files[0]; //event.target.files[0];
    const extension = this.getFileExtension(capturedFile.name); 
    if (!(extension === '.txt' || extension === '.TXT')) {
      this.utilService.onShowAlert("warning", "Atención", "Solo se admite archivos de text (*.txt)"); 
      return;
    }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
	
      this.data_source = e.target.result;                 
      this.getDataByTypeOfService(this.servicioDePago);  
      if (this.data_last.length === 0) {
        this.utilService.onShowAlert("warning", "Atención", `El archivo de texto para el servicio de ${this.servicioDePago} no contiene registros con ${this.numberFields} campos.`);       
        return;
      }
  
      const validation = this.validateFieldConfiguration(this.servicioDePago); 
      if (!validation.isValid) {
        this.utilService.onShowAlert("warning", "Atención", validation.message);         
        return;
      }

      this.nameFile = capturedFile.name;
      this.idGuid = event.originalEvent.body.idCarga;
      this.getListFiles(this.servicioDePago);   
    }
    reader.readAsBinaryString(capturedFile);
  }

  private getListFiles(nombreServicio: string) {
    if (nombreServicio === SERVICIO_DE_PAGO.PAGALO_PE) {
      this.getListFilesPagaloPe_Sftp();
    } 
    else if (nombreServicio === SERVICIO_DE_PAGO.MACMYPE) {
      this.getListFilesPagaloPe_Sftp();
    } 
  }

	private ExcelDateToJSDate1(serial: number) : Date{
	   var utc_days  = Math.floor(serial - 25568);
	   var utc_value = utc_days * 86400;                                        
	   var date_info = new Date(utc_value * 1000);

	   var fractional_day = serial - Math.floor(serial) + 0.0000001;

	   var total_seconds = Math.floor(86400 * fractional_day);

	   var seconds = total_seconds % 60;

	   total_seconds -= seconds;

	   var hours = Math.floor(total_seconds / (60 * 60));
	   var minutes = Math.floor(total_seconds / 60) % 60;

	   return new Date(date_info.getFullYear(), date_info.getMonth(), date_info.getDate(), hours, minutes, seconds);
	}

  private getListFilesPagaloPe_Sftp() {
    let recordList: IFileSftpText[] = [];
   
	const result = this.data_source.split(/\r?\n/);
    for (let i = 0; i < result.length  - 1; i++) {
		let rowItem: string[] = this.parseRow(result[i]);
		if(rowItem.length > 1) {
			let item = new FileSFTP();      
			item.fila = (i + 1).toString();      
			item.a = rowItem[0];
			item.b = rowItem[1];
			item.c = rowItem[2];
			item.d = rowItem[3];
			item.e = rowItem[4];
			item.f = rowItem[5];
			item.g = rowItem[6];
			item.h = rowItem[7];
			item.i = rowItem[8];
			item.j = rowItem[9];
			item.k = rowItem[10];
			
			recordList.push(item);
		}
      
    }
    let dataFileSftpText: IDataFileSftpText = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
    this.eventDataFileSftpText.emit(dataFileSftpText);    
  }

  private parseRow(data: string): string[] {
	let rowData: string[] = [];
	
	let sizeRow = TAMANO_TEXTOS.PAGALO_PE;
	let coTasa = data.substring(INDEX_FIELDS_PAGALO_PE.CO_TASA-1, INDEX_FIELDS_PAGALO_PE.CO_TIPO_DOC - 1);
	if(coTasa !== "99999" && coTasa !== "" ) {
	rowData.push(data.substring(INDEX_FIELDS_PAGALO_PE.CO_TASA-1, INDEX_FIELDS_PAGALO_PE.CO_TIPO_DOC - 1));
	rowData.push(data.substring(INDEX_FIELDS_PAGALO_PE.CO_TIPO_DOC-1, INDEX_FIELDS_PAGALO_PE.NU_DOCU_IDEN - 1));
	rowData.push(data.substring(INDEX_FIELDS_PAGALO_PE.NU_DOCU_IDEN-1, INDEX_FIELDS_PAGALO_PE.CA_OPER - 1));
	rowData.push(data.substring(INDEX_FIELDS_PAGALO_PE.CA_OPER-1, INDEX_FIELDS_PAGALO_PE.IM_OPER - 1));
	rowData.push(data.substring(INDEX_FIELDS_PAGALO_PE.IM_OPER-1, INDEX_FIELDS_PAGALO_PE.FE_OPER - 1));
	rowData.push(data.substring(INDEX_FIELDS_PAGALO_PE.FE_OPER-1, INDEX_FIELDS_PAGALO_PE.NU_SECU - 1));
	rowData.push(data.substring(INDEX_FIELDS_PAGALO_PE.NU_SECU-1, INDEX_FIELDS_PAGALO_PE.HO_OPER - 1));
	rowData.push(data.substring(INDEX_FIELDS_PAGALO_PE.HO_OPER-1, INDEX_FIELDS_PAGALO_PE.CO_OFIC - 1));
	rowData.push(data.substring(INDEX_FIELDS_PAGALO_PE.CO_OFIC-1, INDEX_FIELDS_PAGALO_PE.CO_CAJA - 1));
	rowData.push(data.substring(INDEX_FIELDS_PAGALO_PE.CO_CAJA-1, INDEX_FIELDS_PAGALO_PE.NO_DEFI - 1));
	
	rowData.push(data.substring(INDEX_FIELDS_PAGALO_PE.NO_DEFI, sizeRow - 1));
	}
    return rowData;
  }

  private getFileExtension(nombreArchivo: string): string {    
    let lista: number[] = [];   
    for (let i = 0; i < nombreArchivo.length; i++) {
      if (nombreArchivo.substring(i, i + 1) === '.') {
        lista.push(i);          
      }         
    }
    return nombreArchivo.substring(lista[lista.length - 1], nombreArchivo.length)   
  }    

  private getDataByTypeOfService(service: string) {   
    this.posInitialData = null;
	let tamTextos: number = 0;
    if (service === SERVICIO_DE_PAGO.PAGALO_PE) {
      this.numberFields = CANTIDAD_CAMPOS.PAGALO_PE
	  tamTextos = TAMANO_TEXTOS.PAGALO_PE
    } else if (service === SERVICIO_DE_PAGO.MACMYPE) {
      this.numberFields = CANTIDAD_CAMPOS.MACMYPE
	  tamTextos = TAMANO_TEXTOS.MACMYPE
    } else {
      this.numberFields = 0;
    }   
	if(this.numberFields > 0) {
		
		const result = this.data_source.split(/\r?\n/);
		this.data_last = [];
		if(result.length > 1 && result[1].length === tamTextos) {
			for (let i = 1; i < result.length - 1; i++) {
				if (service === SERVICIO_DE_PAGO.PAGALO_PE || service === SERVICIO_DE_PAGO.MACMYPE) {
					let rowItem: string[] = this.parseRow(result[i]);
					if(rowItem.length > 1) 
						this.data_last.push(rowItem);
				}
			}
			this.posInitialData = 0;
		}
	}
  }

  private validateFieldConfiguration(service: string) {
    
    let response = {
      isValid: true,
      message: ''
    }    
	  

    for (let i = 0; i < this.data_last.length; i++) {      

      if (service === SERVICIO_DE_PAGO.PAGALO_PE) {
      
        // validar el formato del campo 'CO_TASA'     
        let coTasa: string = (this.data_last[i][POSITION_FIELDS_PAGALO_PE.CO_TASA as number] as any) ? (this.data_last[i][POSITION_FIELDS_PAGALO_PE.CO_TASA as number] as any).toString() : '';    
        if (coTasa && coTasa.length !== 5) {
          response.isValid = false;
          response.message = `El campo "CO_TASA" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 5 dígitos`;
          break;   
        }
        // validar que el campo 'CO_TASA' contenga solo números
        if (coTasa && !this.funciones.validarSoloNumeros(coTasa)) {
          response.isValid = false;
          response.message = `El campo "CO_TASA" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }
		
        // validar el formato del campo 'CO_TIPO_DOC'     
        let coTipoDoc: string = (this.data_last[i][POSITION_FIELDS_PAGALO_PE.CO_TIPO_DOC as number] as any) ? (this.data_last[i][POSITION_FIELDS_PAGALO_PE.CO_TIPO_DOC as number] as any).toString() : '';    
        if (coTipoDoc && coTipoDoc.length !== 1) {
          response.isValid = false;
          response.message = `El campo "CO_TIPO_DOC" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 1 dígito`;
          break;   
        }
        // validar que el campo 'CO_TIPO_DOC' contenga solo números
        if (coTipoDoc && !this.funciones.validarSoloNumeros(coTipoDoc)) {
          response.isValid = false;
          response.message = `El campo "CO_TIPO_DOC" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }
		
        // validar el formato del campo 'NU_DOCU_IDEN'     
        let nuDocuIden: string = (this.data_last[i][POSITION_FIELDS_PAGALO_PE.NU_DOCU_IDEN as number] as any) ? (this.data_last[i][POSITION_FIELDS_PAGALO_PE.NU_DOCU_IDEN as number] as any).toString() : '';    
        nuDocuIden = nuDocuIden.trim();
	    if (nuDocuIden && nuDocuIden.length < 5) {
          response.isValid = false;
          response.message = `El campo "NU_DOCU_IDEN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con dígitos mínimos`;
          break;   
        }
        // validar que el campo 'NU_DOCU_IDEN' contenga solo números
        if (nuDocuIden && !this.funciones.validarSoloNumeros(nuDocuIden)) {
          response.isValid = false;
          response.message = `El campo "NU_DOCU_IDEN" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }
				
        // validar el formato del campo 'CA_OPER'     
        let caOper: string = (this.data_last[i][POSITION_FIELDS_PAGALO_PE.CA_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_PAGALO_PE.CA_OPER as number] as any).toString() : '';    
        caOper = caOper.trim();
	    if (caOper && caOper.length != 4) {
          response.isValid = false;
          response.message = `El campo "CA_OPER" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 4 dígitos`;
          break;   
        }
        // validar que el campo 'CA_OPER' contenga solo números
        if (caOper && !this.funciones.validarSoloNumeros(caOper)) {
          response.isValid = false;
          response.message = `El campo "CA_OPER" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }
				
        // validar el formato del campo 'IM_OPER'     
        let imOper: string = (this.data_last[i][POSITION_FIELDS_PAGALO_PE.IM_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_PAGALO_PE.IM_OPER as number] as any).toString() : '';    
        imOper = imOper.trim();
	    if (imOper && imOper.length != 13) {
          response.isValid = false;
          response.message = `El campo "IM_OPER" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 13 dígitos`;
          break;   
        }
        // validar que el campo 'IM_OPER' contenga solo números
        if (imOper && !this.funciones.validarSoloNumeros(imOper)) {
          response.isValid = false;
          response.message = `El campo "IM_OPER" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }		
		

        // // Validar formato del campo 'FECHA OPERACIÓN'
        let feOper: string = (this.data_last[i][POSITION_FIELDS_PAGALO_PE.FE_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_PAGALO_PE.FE_OPER as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(feOper);       
        if (feOper && fecha1.length !== 8 ) {
          response.isValid = false;
          response.message = `El campo "FECHA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (8 dígitos): los cuatro primeros dígitos son del año, seguido de los dos dígitos del mes, seguido de los dos dígitos del día.`;
          break; 
        }

        // validar el formato del campo 'NU_SECU'     
        let nuSecu: string = (this.data_last[i][POSITION_FIELDS_PAGALO_PE.NU_SECU as number] as any) ? (this.data_last[i][POSITION_FIELDS_PAGALO_PE.NU_SECU as number] as any).toString() : '';    
        nuSecu = nuSecu.trim();
	    if (nuSecu && nuSecu.length != 6) {
          response.isValid = false;
          response.message = `El campo "NU_SECU" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 6 dígitos`;
          break;   
        }
        // validar que el campo 'NU_SECU' contenga solo números
        if (nuSecu && !this.funciones.validarSoloNumeros(nuSecu)) {
          response.isValid = false;
          response.message = `El campo "NU_SECU" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }		
		
        // validar el formato del campo 'HO_OPER'     
        let hoOper: string = (this.data_last[i][POSITION_FIELDS_PAGALO_PE.HO_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_PAGALO_PE.HO_OPER as number] as any).toString() : '';    
        hoOper = hoOper.trim();
	    if (hoOper && hoOper.length != 6) {
          response.isValid = false;
          response.message = `El campo "HO_OPER" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 6 dígitos`;
          break;   
        }
        // validar que el campo 'HO_OPER' contenga solo números
        if (hoOper && !this.funciones.validarSoloNumeros(hoOper)) {
          response.isValid = false;
          response.message = `El campo "HO_OPER" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }	

        // validar el formato del campo 'CO_OFIC'     
        let coOfic: string = (this.data_last[i][POSITION_FIELDS_PAGALO_PE.CO_OFIC as number] as any) ? (this.data_last[i][POSITION_FIELDS_PAGALO_PE.CO_OFIC as number] as any).toString() : '';    
        coOfic = coOfic.trim();
	    if (coOfic && coOfic.length != 5) {
          response.isValid = false;
          response.message = `El campo "CO_OFIC" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 5 dígitos`;
          break;   
        }
        // validar que el campo 'CO_OFIC' contenga solo números
        if (coOfic && !this.funciones.validarSoloNumeros(coOfic)) {
          response.isValid = false;
          response.message = `El campo "CO_OFIC" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }	
		
        // validar el formato del campo 'CO_CAJA'     
        let coCaja: string = (this.data_last[i][POSITION_FIELDS_PAGALO_PE.CO_CAJA as number] as any) ? (this.data_last[i][POSITION_FIELDS_PAGALO_PE.CO_CAJA as number] as any).toString() : '';    
        coCaja = coCaja.trim();
	    if (coCaja && coCaja.length != 4) {
          response.isValid = false;
          response.message = `El campo "CO_CAJA" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 4 dígitos`;
          break;   
        }
        // validar que el campo 'CO_CAJA' contenga solo números
        if (coCaja && !this.funciones.validarSoloNumeros(coCaja)) {
          response.isValid = false;
          response.message = `El campo "CO_CAJA" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }	
		

      } 
	  else if (service === SERVICIO_DE_PAGO.MACMYPE) {
      
        // validar el formato del campo 'CO_TASA'     
        let coTasa: string = (this.data_last[i][POSITION_FIELDS_MACMYPE.CO_TASA as number] as any) ? (this.data_last[i][POSITION_FIELDS_MACMYPE.CO_TASA as number] as any).toString() : '';    
        if (coTasa && coTasa.length !== 5) {
          response.isValid = false;
          response.message = `El campo "CO_TASA" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 5 dígitos`;
          break;   
        }
        // validar que el campo 'CO_TASA' contenga solo números
        if (coTasa && !this.funciones.validarSoloNumeros(coTasa)) {
          response.isValid = false;
          response.message = `El campo "CO_TASA" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }
		
        // validar el formato del campo 'CO_TIPO_DOC'     
        let coTipoDoc: string = (this.data_last[i][POSITION_FIELDS_MACMYPE.CO_TIPO_DOC as number] as any) ? (this.data_last[i][POSITION_FIELDS_MACMYPE.CO_TIPO_DOC as number] as any).toString() : '';    
        if (coTipoDoc && coTipoDoc.length !== 1) {
          response.isValid = false;
          response.message = `El campo "CO_TIPO_DOC" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 1 dígito`;
          break;   
        }
        // validar que el campo 'CO_TIPO_DOC' contenga solo números
        if (coTipoDoc && !this.funciones.validarSoloNumeros(coTipoDoc)) {
          response.isValid = false;
          response.message = `El campo "CO_TIPO_DOC" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }
		
        // validar el formato del campo 'NU_DOCU_IDEN'     
        let nuDocuIden: string = (this.data_last[i][POSITION_FIELDS_MACMYPE.NU_DOCU_IDEN as number] as any) ? (this.data_last[i][POSITION_FIELDS_MACMYPE.NU_DOCU_IDEN as number] as any).toString() : '';    
        nuDocuIden = nuDocuIden.trim();
	    if (nuDocuIden && nuDocuIden.length < 5) {
          response.isValid = false;
          response.message = `El campo "NU_DOCU_IDEN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con dígitos mínimos`;
          break;   
        }
        // validar que el campo 'NU_DOCU_IDEN' contenga solo números
        if (nuDocuIden && !this.funciones.validarSoloNumeros(nuDocuIden)) {
          response.isValid = false;
          response.message = `El campo "NU_DOCU_IDEN" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }
				
        // validar el formato del campo 'CA_OPER'     
        let caOper: string = (this.data_last[i][POSITION_FIELDS_MACMYPE.CA_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_MACMYPE.CA_OPER as number] as any).toString() : '';    
        caOper = caOper.trim();
	    if (caOper && caOper.length != 4) {
          response.isValid = false;
          response.message = `El campo "CA_OPER" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 4 dígitos`;
          break;   
        }
        // validar que el campo 'CA_OPER' contenga solo números
        if (caOper && !this.funciones.validarSoloNumeros(caOper)) {
          response.isValid = false;
          response.message = `El campo "CA_OPER" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }
				
        // validar el formato del campo 'IM_OPER'     
        let imOper: string = (this.data_last[i][POSITION_FIELDS_MACMYPE.IM_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_MACMYPE.IM_OPER as number] as any).toString() : '';    
        imOper = imOper.trim();
	    if (imOper && imOper.length != 13) {
          response.isValid = false;
          response.message = `El campo "IM_OPER" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 13 dígitos`;
          break;   
        }
        // validar que el campo 'IM_OPER' contenga solo números
        if (imOper && !this.funciones.validarSoloNumeros(imOper)) {
          response.isValid = false;
          response.message = `El campo "IM_OPER" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }		
		

        // // Validar formato del campo 'FECHA OPERACIÓN'
        let feOper: string = (this.data_last[i][POSITION_FIELDS_MACMYPE.FE_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_MACMYPE.FE_OPER as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(feOper);       
        if (feOper && fecha1.length !== 8 ) {
          response.isValid = false;
          response.message = `El campo "FECHA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (8 dígitos): los cuatro primeros dígitos son del año, seguido de los dos dígitos del mes, seguido de los dos dígitos del día.`;
          break; 
        }

        // validar el formato del campo 'NU_SECU'     
        let nuSecu: string = (this.data_last[i][POSITION_FIELDS_MACMYPE.NU_SECU as number] as any) ? (this.data_last[i][POSITION_FIELDS_MACMYPE.NU_SECU as number] as any).toString() : '';    
        nuSecu = nuSecu.trim();
	    if (nuSecu && nuSecu.length != 6) {
          response.isValid = false;
          response.message = `El campo "NU_SECU" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 6 dígitos`;
          break;   
        }
        // validar que el campo 'NU_SECU' contenga solo números
        if (nuSecu && !this.funciones.validarSoloNumeros(nuSecu)) {
          response.isValid = false;
          response.message = `El campo "NU_SECU" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }		
		
        // validar el formato del campo 'HO_OPER'     
        let hoOper: string = (this.data_last[i][POSITION_FIELDS_MACMYPE.HO_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_MACMYPE.HO_OPER as number] as any).toString() : '';    
        hoOper = hoOper.trim();
	    if (hoOper && hoOper.length != 6) {
          response.isValid = false;
          response.message = `El campo "HO_OPER" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 6 dígitos`;
          break;   
        }
        // validar que el campo 'HO_OPER' contenga solo números
        if (hoOper && !this.funciones.validarSoloNumeros(hoOper)) {
          response.isValid = false;
          response.message = `El campo "HO_OPER" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }	

        // validar el formato del campo 'CO_OFIC'     
        let coOfic: string = (this.data_last[i][POSITION_FIELDS_MACMYPE.CO_OFIC as number] as any) ? (this.data_last[i][POSITION_FIELDS_MACMYPE.CO_OFIC as number] as any).toString() : '';    
        coOfic = coOfic.trim();
	    if (coOfic && coOfic.length != 5) {
          response.isValid = false;
          response.message = `El campo "CO_OFIC" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 5 dígitos`;
          break;   
        }
        // validar que el campo 'CO_OFIC' contenga solo números
        if (coOfic && !this.funciones.validarSoloNumeros(coOfic)) {
          response.isValid = false;
          response.message = `El campo "CO_OFIC" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }	
		
        // validar el formato del campo 'CO_CAJA'     
        let coCaja: string = (this.data_last[i][POSITION_FIELDS_MACMYPE.CO_CAJA as number] as any) ? (this.data_last[i][POSITION_FIELDS_MACMYPE.CO_CAJA as number] as any).toString() : '';    
        coCaja = coCaja.trim();
	    if (coCaja && coCaja.length != 4) {
          response.isValid = false;
          response.message = `El campo "CO_CAJA" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 4 dígitos`;
          break;   
        }
        // validar que el campo 'CO_CAJA' contenga solo números
        if (coCaja && !this.funciones.validarSoloNumeros(coCaja)) {
          response.isValid = false;
          response.message = `El campo "CO_CAJA" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }	
		

      } 
    }  
    return response;  
  }

  ngOnChanges() {     
    this.nameFile = '';
    this.data_source = '';
    this.data_last = [];      
  }

}
