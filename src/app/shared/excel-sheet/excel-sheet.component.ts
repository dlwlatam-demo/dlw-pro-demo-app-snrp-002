import { Component, EventEmitter, Input, Output, OnInit, OnChanges } from '@angular/core';
import { UtilService } from 'src/app/services/util.service';
import * as XLSX from 'xlsx';
import { CANTIDAD_CAMPOS, POSITION_FIELDS_AMEX, POSITION_FIELDS_DINERS, POSITION_FIELDS_NIUBIZ, POSITION_FIELDS_NIUBIZ_APP_QR_SID, POSITION_FIELDS_HERMES, POSITION_FIELDS_CCI_SPRL, SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { IDataFileExcel, IFileAmex, IFileDiners, IFileNiubiz } from '../../interfaces/consolidacion';
import { FileAmex, FileDiners, FileNiubiz } from 'src/app/models/procesos/consolidacion/recaudacion-medios-electronicos.model';
import { GeneralService } from '../../services/general.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'button-excel-sheet',
  templateUrl: './excel-sheet.component.html',
  styleUrls: ['./excel-sheet.component.scss']
})
export class ExcelSheetComponent implements OnInit, OnChanges {

  public nameFile: string = '';
  public idGuid: string = '';
  public maxFileSize!: number;
  public uploadingEndpoint!: string;
  private data_source: [][] = [];
  private data_last: [][] = []; 
  private numberFields: number;
  private posInitialData: number = null;
  @Input() servicioDePago: string = '';  
  @Output() eventDataFileExcel = new EventEmitter<IDataFileExcel>();
  @Output() uploaded = new EventEmitter<any>();//clicked - void

  constructor(
    private funciones: Funciones,
    private utilService: UtilService,
    private generalService: GeneralService
  ) { }

  ngOnInit(): void {   
    this.uploadingEndpoint = this.generalService.getUploadingEndpoint();
    this.maxFileSize = environment.maxFileSize
  }

  public uploadFiles(event: any) { //onClick
    for (let file of event.files) {
      const f = { fileId: event.originalEvent.body.idCarga, fileName: file.name }
      this.uploaded.emit(f);
    }
    // this.clicked.emit();
  }

  public captureFile(event: any) {
    this.uploadFiles( event );
    this.data_source = [];
    this.data_last = []; 
    const capturedFile: File = event.files[0]; //event.target.files[0];
    const extension = this.getFileExtension(capturedFile.name);
    if (!(extension === '.xls' || extension === '.xlsx')) {
      this.utilService.onShowAlert("warning", "Atención", "Solo se admite archivos de excel (*.xls, *.xlsx)"); 
      return;
    }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb_source: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
      const wsname_source : string = wb_source.SheetNames[0];
      const ws_source: XLSX.WorkSheet = wb_source.Sheets[wsname_source]; 
      this.data_source = (XLSX.utils.sheet_to_json(ws_source, { header: 1 }));
      this.getDataByTypeOfService(this.servicioDePago);
      if (this.data_last.length === 0) {
        console.log('data_last2', this.data_last.length);
        this.utilService.onShowAlert("warning", "Atención", `El archivo de excel para el servicio de ${this.servicioDePago} no contiene registros con ${this.numberFields} campos.`);       
        return;
      }
  
      const validation = this.validateFieldConfiguration(this.servicioDePago); 
      if (!validation.isValid) {
        this.utilService.onShowAlert("warning", "Atención", validation.message);         
        return;
      }

      this.nameFile = capturedFile.name;
      this.idGuid = event.originalEvent.body.idCarga;
      this.getListFiles(this.servicioDePago);   
    }
    reader.readAsBinaryString(capturedFile);
  }

  private getListFiles(nombreServicio: string) {
    if (nombreServicio === SERVICIO_DE_PAGO.NIUBIZ) {
      this.getListFiles_NIUBIZ();
    } else if (nombreServicio === SERVICIO_DE_PAGO.DINERS) {
      this.getListFiles_DINERS();
    } else if (nombreServicio === SERVICIO_DE_PAGO.AMEX) {
      this.getListFiles_AMEX();
    } else if (nombreServicio === SERVICIO_DE_PAGO.NIUBIZ_SPRL) {
      this.getListFilesSprl_NIUBIZ();
    } else if (nombreServicio === SERVICIO_DE_PAGO.HERMES) {
      this.getListFiles_HERMES();
    } else if (nombreServicio === SERVICIO_DE_PAGO.BANCARIA_POS) {
      this.getListFiles_BANCARIA_POS();
    } else if (nombreServicio === SERVICIO_DE_PAGO.BANCARIA_POS_DINERS) {
      this.getListFiles_BANCARIA_POS();
    } else if (nombreServicio === SERVICIO_DE_PAGO.PAGALO_PE_BN) {
      this.getListFiles_PAGALO_PE_BN();
    } else if (nombreServicio === SERVICIO_DE_PAGO.MACMYPE_BN) {
      this.getListFiles_MACMYPE_BN();
    } else if (nombreServicio === SERVICIO_DE_PAGO.SIAF_MEF) {
      this.getListFiles_SIAF_MEF();
    } else if (nombreServicio === SERVICIO_DE_PAGO.SIAF_MEF_BN) {
      this.getListFiles_SIAF_MEF_BN();
    } else if (nombreServicio === SERVICIO_DE_PAGO.CCI_SPRL) {
      this.getListFiles_CCI_SPRL();
    } else if (nombreServicio === SERVICIO_DE_PAGO.HERMES_SIAF) {
      this.getListFiles_HERMES_SIAF();
    } else if (nombreServicio === SERVICIO_DE_PAGO.HERMES_BN) {
      this.getListFiles_HERMES_BN();
    } else if (nombreServicio === SERVICIO_DE_PAGO.NIUBIZ_APP_QR_SID) {
      this.getListFiles_NIUBIZ_APP_QR_SID();
    } else if ( nombreServicio === SERVICIO_DE_PAGO.BANCARIA_APP_QR_SID ) {
      this.getListFiles_BANCARIA_APP_QR_SID();
    }
	
	
  }

	private ExcelDateToJSDate1(serial: number) : Date{
	   var utc_days  = Math.floor(serial - 25568);
	   var utc_value = utc_days * 86400;                                        
	   var date_info = new Date(utc_value * 1000);

	   var fractional_day = serial - Math.floor(serial) + 0.0000001;

	   var total_seconds = Math.floor(86400 * fractional_day);

	   var seconds = total_seconds % 60;

	   total_seconds -= seconds;

	   var hours = Math.floor(total_seconds / (60 * 60));
	   var minutes = Math.floor(total_seconds / 60) % 60;

	   return new Date(date_info.getFullYear(), date_info.getMonth(), date_info.getDate(), hours, minutes, seconds);
	}


  private getListFiles_HERMES() {
    let recordList: IFileNiubiz[] = [];
	let j = 0;
	let noBanc = '';
    for (let i = 0; i < this.data_last.length; i++) {
		let item = new FileNiubiz();      
        item.fila = (j + 1).toString();     

		if (typeof (this.data_last[i][4 as number] as any) === "undefined") {
			// NO_BANC
			noBanc = (this.data_last[i][0 as number] as any).toString();
			i++;
		}
		item.a =  noBanc;
		// NO_USUA
		item.b = (this.data_last[i][0 as number] as any).toString(); 
		// FE_OPER
		item.c = this.funciones.convertirFechaDateToString(this.ExcelDateToJSDate1((this.data_last[i][2 as number] as any).toString()));
		// NU_OPER
		item.d = (this.data_last[i][4 as number] as any)?(this.data_last[i][4 as number] as any).toString() : ''; 
		// DE_MONE
		item.e = (this.data_last[i][5 as number] as any)?(this.data_last[i][5 as number] as any).toString() : ''; 
		// IM_CONT
		item.f = (this.data_last[i][6 as number] as any)?(this.data_last[i][6 as number] as any).toString() : ''; 
		// IM_APER
		item.g = (this.data_last[i][8 as number] as any)?(this.data_last[i][8 as number] as any).toString() : ''; 
		// NU_ACTA
		item.h = (this.data_last[i][10 as number] as any)?(this.data_last[i][10 as number] as any).toString() : 0; 
		// NU_ACTA_INTE
		item.i = (this.data_last[i][11 as number] as any)?(this.data_last[i][11 as number] as any).toString() : 0; 
		// IM_DIFE
		item.j = (this.data_last[i][12 as number] as any)?(this.data_last[i][12 as number] as any).toString() : ''; 
		// DE_ANOM : ''
		item.k = (this.data_last[i][13 as number] as any)?(this.data_last[i][13 as number] as any).toString() : ''; 
		// OB_HERM
		item.l = (this.data_last[i][14 as number] as any)?(this.data_last[i][14 as number] as any).toString() : ''; 
		// DE_RESP
		item.m = (this.data_last[i][15 as number] as any)?(this.data_last[i][15 as number] as any).toString() : ''; 

		recordList.push(item);
		j++;
		
    }
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
    this.eventDataFileExcel.emit(dataFileExcel);    
  }
  
  private getFechaLarga(data: any):string {  
	return data?(!isNaN(Number(data))?this.funciones.convertirFechaLargaDateToString(this.ExcelDateToJSDate1(data)): this.isValidDateLongFormat(data)?data :'') : '';
  }
  
  private getFechaCorta(data: any):string {  
	return data?(!isNaN(Number(data))? this.funciones.convertirFechaDateToString(this.ExcelDateToJSDate1(data.toString())): this.isValidDate(data)?data :'') : '';
  }

	
private isValidDateLongFormat(dateString: string): boolean {
  const regex = /^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d\s([0-1]?[0-9]|2?[0-3]):([0-5]\d):([0-5]\d)$/;
  return regex.test(dateString);
}


private isValidDate(dateString: string): boolean {
  const regex = /^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d$/;
  return regex.test(dateString);
}

  private getListFilesSprl_NIUBIZ() {
    let recordList: IFileNiubiz[] = [];

    for (let i = 0; i < this.data_last.length; i++) {
      let tipoOperacion: string = (this.data_last[i][7 as number] as any) ? (this.data_last[i][7 as number] as any).toString() : '';         
      let textoValidar = (tipoOperacion !== '') ? (this.funciones.eliminarEspaciosEnBlanco(tipoOperacion.toUpperCase())) : '';
      if ((!(textoValidar.substring(0, 5) === 'SALDO') || textoValidar === '') && (this.data_last[i][0 as number] as any)){
	  
        let item = new FileNiubiz();      
        item.fila = (i + 1).toString();      
        item.a = (this.data_last[i][0 as number] as any || (this.data_last[i][0 as number] as any) === 0) ? (this.data_last[i][0 as number] as any).toString() : '';
        item.b = (this.data_last[i][1 as number] as any || (this.data_last[i][1 as number] as any) === 0) ? (this.data_last[i][1 as number] as any).toString() : '';
        item.c = (this.data_last[i][2 as number] as any || (this.data_last[i][2 as number] as any) === 0) ? (this.data_last[i][2 as number] as any).toString() : '';
        item.d = (this.data_last[i][3 as number] as any || (this.data_last[i][3 as number] as any) === 0) ? (this.data_last[i][3 as number] as any).toString() : '';
        item.e = this.getFechaLarga(this.data_last[i][4 as number] as any);
        item.f = this.getFechaCorta(this.data_last[i][5 as number] as any);		
        item.g = (this.data_last[i][6 as number] as any || (this.data_last[i][6 as number] as any) === 0) ? (this.data_last[i][6 as number] as any).toString() : '';
        item.h = (this.data_last[i][7 as number] as any || (this.data_last[i][7 as number] as any) === 0) ? (this.data_last[i][7 as number] as any).toString() : '';
        item.i = (this.data_last[i][8 as number] as any || (this.data_last[i][8 as number] as any) === 0) ? (this.data_last[i][8 as number] as any).toString() : '';
        item.j = (this.data_last[i][9 as number] as any || (this.data_last[i][9 as number] as any) === 0) ? (this.data_last[i][9 as number] as any).toString() : '';
        item.k = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';
        item.l = (this.data_last[i][11 as number] as any || (this.data_last[i][11 as number] as any) === 0) ? (this.data_last[i][11 as number] as any).toString() : '';
        item.m = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
        item.n = (this.data_last[i][13 as number] as any || (this.data_last[i][13 as number] as any) === 0) ? (this.data_last[i][13 as number] as any).toString() : '';
        item.o = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';
        item.p = (this.data_last[i][15 as number] as any || (this.data_last[i][15 as number] as any) === 0) ? (this.data_last[i][15 as number] as any).toString() : '';
        item.q = (this.data_last[i][16 as number] as any || (this.data_last[i][16 as number] as any) === 0) ? (this.data_last[i][16 as number] as any).toString() : '';
        item.r = (this.data_last[i][17 as number] as any || (this.data_last[i][17 as number] as any) === 0) ? (this.data_last[i][17 as number] as any).toString() : '';
        item.s = (this.data_last[i][18 as number] as any || (this.data_last[i][18 as number] as any) === 0) ? (this.data_last[i][18 as number] as any).toString() : '';
        item.t = (this.data_last[i][19 as number] as any || (this.data_last[i][19 as number] as any) === 0) ? (this.data_last[i][19 as number] as any).toString() : '';
        item.u = (this.data_last[i][20 as number] as any || (this.data_last[i][20 as number] as any) === 0) ? (this.data_last[i][20 as number] as any).toString() : '';
        item.v = (this.data_last[i][21 as number] as any || (this.data_last[i][21 as number] as any) === 0) ? (this.data_last[i][21 as number] as any).toString() : '';
        item.w = (this.data_last[i][22 as number] as any || (this.data_last[i][22 as number] as any) === 0) ? (this.data_last[i][22 as number] as any).toString() : '';
        item.x = (this.data_last[i][23 as number] as any || (this.data_last[i][23 as number] as any) === 0) ? (this.data_last[i][23 as number] as any).toString() : '';
        item.y = (this.data_last[i][24 as number] as any || (this.data_last[i][24 as number] as any) === 0) ? (this.data_last[i][24 as number] as any).toString() : '';
        item.z = (this.data_last[i][25 as number] as any || (this.data_last[i][25 as number] as any) === 0) ? (this.data_last[i][25 as number] as any).toString() : '';
        recordList.push(item);
      }
    }
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
    this.eventDataFileExcel.emit(dataFileExcel);    
  }

  private getListFiles_NIUBIZ() {
    let recordList: IFileNiubiz[] = [];
    for (let i = 0; i < this.data_last.length; i++) {
      let tipoOperacion: string = (this.data_last[i][7 as number] as any) ? (this.data_last[i][7 as number] as any).toString() : '';         
      let textoValidar = (tipoOperacion !== '') ? (this.funciones.eliminarEspaciosEnBlanco(tipoOperacion.toUpperCase())) : '';
      if (!(textoValidar.substring(0, 5) === 'SALDO') || textoValidar === '') {
        let item = new FileNiubiz();      
        item.fila = (i + 1).toString();      
        item.a = (this.data_last[i][0 as number] as any || (this.data_last[i][0 as number] as any) === 0) ? (this.data_last[i][0 as number] as any).toString() : '';
        item.b = (this.data_last[i][1 as number] as any || (this.data_last[i][1 as number] as any) === 0) ? (this.data_last[i][1 as number] as any).toString() : '';
        item.c = (this.data_last[i][2 as number] as any || (this.data_last[i][2 as number] as any) === 0) ? (this.data_last[i][2 as number] as any).toString() : '';
        item.d = (this.data_last[i][3 as number] as any || (this.data_last[i][3 as number] as any) === 0) ? (this.data_last[i][3 as number] as any).toString() : '';
        item.e = (this.data_last[i][4 as number] as any) ? this.funciones.formatearFechaLarga((this.data_last[i][4 as number] as any).toString()) : '';
        item.f = (this.data_last[i][5 as number] as any) ? this.funciones.formatearFechaCorta((this.data_last[i][5 as number] as any).toString()) : '';
        item.g = (this.data_last[i][6 as number] as any || (this.data_last[i][6 as number] as any) === 0) ? (this.data_last[i][6 as number] as any).toString() : '';
        item.h = (this.data_last[i][7 as number] as any || (this.data_last[i][7 as number] as any) === 0) ? (this.data_last[i][7 as number] as any).toString() : '';
        item.i = (this.data_last[i][8 as number] as any || (this.data_last[i][8 as number] as any) === 0) ? (this.data_last[i][8 as number] as any).toString() : '';
        item.j = (this.data_last[i][9 as number] as any || (this.data_last[i][9 as number] as any) === 0) ? (this.data_last[i][9 as number] as any).toString() : '';
        item.k = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';
        item.l = (this.data_last[i][11 as number] as any || (this.data_last[i][11 as number] as any) === 0) ? (this.data_last[i][11 as number] as any).toString() : '';
        item.m = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
        item.n = (this.data_last[i][13 as number] as any || (this.data_last[i][13 as number] as any) === 0) ? (this.data_last[i][13 as number] as any).toString() : '';
        item.o = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';
        item.p = (this.data_last[i][15 as number] as any || (this.data_last[i][15 as number] as any) === 0) ? (this.data_last[i][15 as number] as any).toString() : '';
        item.q = (this.data_last[i][16 as number] as any || (this.data_last[i][16 as number] as any) === 0) ? (this.data_last[i][16 as number] as any).toString() : '';
        item.r = (this.data_last[i][17 as number] as any || (this.data_last[i][17 as number] as any) === 0) ? (this.data_last[i][17 as number] as any).toString() : '';
        item.s = (this.data_last[i][18 as number] as any || (this.data_last[i][18 as number] as any) === 0) ? (this.data_last[i][18 as number] as any).toString() : '';
        item.t = (this.data_last[i][19 as number] as any || (this.data_last[i][19 as number] as any) === 0) ? (this.data_last[i][19 as number] as any).toString() : '';
        item.u = (this.data_last[i][20 as number] as any || (this.data_last[i][20 as number] as any) === 0) ? (this.data_last[i][20 as number] as any).toString() : '';
        item.v = (this.data_last[i][21 as number] as any || (this.data_last[i][21 as number] as any) === 0) ? (this.data_last[i][21 as number] as any).toString() : '';
        item.w = (this.data_last[i][22 as number] as any || (this.data_last[i][22 as number] as any) === 0) ? (this.data_last[i][22 as number] as any).toString() : '';
        item.x = (this.data_last[i][23 as number] as any || (this.data_last[i][23 as number] as any) === 0) ? (this.data_last[i][23 as number] as any).toString() : '';
        recordList.push(item);
      }
    }
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
    this.eventDataFileExcel.emit(dataFileExcel);    
  }

  private getListFiles_DINERS() {
    let recordList: IFileDiners[] = [];  
    for (let i = 0; i < this.data_last.length; i++) { 
      let item = new FileDiners();      
      item.fila = (i + 1).toString();      
      item.a = (this.data_last[i][0 as number] as any || (this.data_last[i][0 as number] as any) === 0) ? (this.data_last[i][0 as number] as any).toString() : '';
      item.b = (this.data_last[i][1 as number] as any || (this.data_last[i][1 as number] as any) === 0) ? (this.data_last[i][1 as number] as any).toString() : '';   
      item.c = (this.data_last[i][2 as number] as any) ? this.funciones.obtenerFechaStringDeFechaExcel((this.data_last[i][2 as number] as any)) : '';
      item.d = (this.data_last[i][3 as number] as any || (this.data_last[i][3 as number] as any) === 0) ? (this.data_last[i][3 as number] as any).toString() : '';
      item.e = (this.data_last[i][4 as number] as any || (this.data_last[i][4 as number] as any) === 0) ? (this.data_last[i][4 as number] as any).toString() : '';
      item.f = (this.data_last[i][5 as number] as any || (this.data_last[i][5 as number] as any) === 0) ? (this.data_last[i][5 as number] as any).toString() : '';
      item.g = (this.data_last[i][6 as number] as any || (this.data_last[i][6 as number] as any) === 0) ? (this.data_last[i][6 as number] as any).toString() : '';
      item.h = (this.data_last[i][7 as number] as any || (this.data_last[i][7 as number] as any) === 0) ? (this.data_last[i][7 as number] as any).toString() : '';
      item.i = (this.data_last[i][8 as number] as any || (this.data_last[i][8 as number] as any) === 0) ? (this.data_last[i][8 as number] as any).toString() : '';
      item.j = (this.data_last[i][9 as number] as any || (this.data_last[i][9 as number] as any) === 0) ? (this.data_last[i][9 as number] as any).toString() : '';
      item.k = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';
      item.l = (this.data_last[i][11 as number] as any || (this.data_last[i][11 as number] as any) === 0) ? (this.data_last[i][11 as number] as any).toString() : '';      
      recordList.push(item);
    }  
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
    this.eventDataFileExcel.emit(dataFileExcel);
  }

  private getListFiles_AMEX() {
    let recordList: IFileAmex[] = [];  
    for (let i = 0; i < this.data_last.length; i++) {       
      let item = new FileAmex();      
      item.fila = (i + 1).toString();      
      item.a = (this.data_last[i][0 as number] as any || (this.data_last[i][0 as number] as any) === 0) ? (this.data_last[i][0 as number] as any).toString() : '';
      item.b = (this.data_last[i][1 as number] as any) ? this.funciones.formatearFechaCorta2((this.data_last[i][1 as number] as any).toString()) : '';
      item.c = (this.data_last[i][2 as number] as any || (this.data_last[i][2 as number] as any) === 0) ? (this.data_last[i][2 as number] as any).toString() : '';      
      item.d = (this.data_last[i][3 as number] as any || (this.data_last[i][3 as number] as any) === 0) ? (this.data_last[i][3 as number] as any).toString() : '';
      item.e = (this.data_last[i][4 as number] as any || (this.data_last[i][4 as number] as any) === 0) ? (this.data_last[i][4 as number] as any).toString() : '';
      item.f = (this.data_last[i][5 as number] as any || (this.data_last[i][5 as number] as any) === 0) ? (this.data_last[i][5 as number] as any).toString() : '';
      item.g = (this.data_last[i][6 as number] as any || (this.data_last[i][6 as number] as any) === 0) ? (this.data_last[i][6 as number] as any).toString() : '';
      item.h = (this.data_last[i][7 as number] as any || (this.data_last[i][7 as number] as any) === 0) ? (this.data_last[i][7 as number] as any).toString() : '';
      item.i = (this.data_last[i][8 as number] as any || (this.data_last[i][8 as number] as any) === 0) ? (this.data_last[i][8 as number] as any).toString() : '';
      item.j = (this.data_last[i][9 as number] as any || (this.data_last[i][9 as number] as any) === 0) ? (this.data_last[i][9 as number] as any).toString() : '';      
      item.k = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';
      item.l = (this.data_last[i][11 as number] as any || (this.data_last[i][11 as number] as any) === 0) ? (this.data_last[i][11 as number] as any).toString() : '';  
      item.m = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
      item.n = (this.data_last[i][13 as number] as any || (this.data_last[i][13 as number] as any) === 0) ? (this.data_last[i][13 as number] as any).toString() : '';     
      recordList.push(item);
    }  
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
    this.eventDataFileExcel.emit(dataFileExcel);
  }



  private getListFiles_BANCARIA_POS() {
    let recordList: IFileNiubiz[] = [];  
    for (let i = 0; i < this.data_last.length; i++) {     

		if((this.data_last[i][2 as number] as any)) {
		  let item = new FileNiubiz();      
		  item.fila = (i + 1).toString();      
		  item.a = (this.data_last[i][2 as number] as any) ? (this.data_last[i][2 as number] as any).toString() : '';
		  item.b = (this.data_last[i][5 as number] as any || (this.data_last[i][5 as number] as any) === 0) ? (this.data_last[i][5 as number] as any).toString() : '';
		  item.c = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';      
		  item.d = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
		  item.e = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';

		  recordList.push(item);
		}  
	}
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
	console.log ("lista = " + recordList.length);
    this.eventDataFileExcel.emit(dataFileExcel); 
  }
  
  

  private getListFiles_PAGALO_PE_BN() {
    let recordList: IFileNiubiz[] = [];  
    for (let i = 0; i < this.data_last.length; i++) {     

		if((this.data_last[i][2 as number] as any)) {
		  let item = new FileNiubiz();      
		  item.fila = (i + 1).toString();      
		  item.a = (this.data_last[i][2 as number] as any) ? (this.data_last[i][2 as number] as any).toString() : '';
		  item.b = (this.data_last[i][5 as number] as any || (this.data_last[i][5 as number] as any) === 0) ? (this.data_last[i][5 as number] as any).toString() : '';
		  item.c = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';      
		  item.d = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
		  item.e = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';

		  recordList.push(item);
		}  
	}
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
	console.log ("lista = " + recordList.length);
    this.eventDataFileExcel.emit(dataFileExcel); 
  }
  
  private getListFiles_MACMYPE_BN() {
    let recordList: IFileNiubiz[] = [];  
    for (let i = 0; i < this.data_last.length; i++) {     

		if((this.data_last[i][2 as number] as any)) {
		  let item = new FileNiubiz();      
		  item.fila = (i + 1).toString();      
		  item.a = (this.data_last[i][2 as number] as any) ? (this.data_last[i][2 as number] as any).toString() : '';
		  item.b = (this.data_last[i][5 as number] as any || (this.data_last[i][5 as number] as any) === 0) ? (this.data_last[i][5 as number] as any).toString() : '';
		  item.c = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';      
		  item.d = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
		  item.e = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';

		  recordList.push(item);
		}  
	}
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
	console.log ("lista = " + recordList.length);
    this.eventDataFileExcel.emit(dataFileExcel); 
  }
    
  private getListFiles_SIAF_MEF() {
    let recordList: IFileNiubiz[] = [];  
    for (let i = 0; i < this.data_last.length; i++) {     

		if((this.data_last[i][2 as number] as any)) {
		  let item = new FileNiubiz();      
		  item.fila = (i + 1).toString();      
		  item.a = (this.data_last[i][0 as number] as any || (this.data_last[i][0 as number] as any) === 0) ? (this.data_last[i][0 as number] as any).toString() : '';
		  item.b = (this.data_last[i][1 as number] as any || (this.data_last[i][1 as number] as any) === 0) ? (this.data_last[i][1 as number] as any).toString() : '';
		  item.c = (this.data_last[i][2 as number] as any || (this.data_last[i][2 as number] as any) === 0) ? (this.data_last[i][2 as number] as any).toString() : '';      
		  item.d = this.funciones.convertirFechaDateToString(this.ExcelDateToJSDate1((this.data_last[i][3 as number] as any).toString()));
		  item.e = (this.data_last[i][4 as number] as any || (this.data_last[i][4 as number] as any) === 0) ? (this.data_last[i][4 as number] as any).toString() : '';
		  item.f = (this.data_last[i][5 as number] as any || (this.data_last[i][5 as number] as any) === 0) ? (this.data_last[i][5 as number] as any).toString() : '';
		  item.g = (this.data_last[i][6 as number] as any || (this.data_last[i][6 as number] as any) === 0) ? (this.data_last[i][6 as number] as any).toString() : '';
		  item.h = (this.data_last[i][7 as number] as any || (this.data_last[i][7 as number] as any) === 0) ? (this.data_last[i][7 as number] as any).toString() : '';
		  item.i = (this.data_last[i][8 as number] as any || (this.data_last[i][8 as number] as any) === 0) ? (this.data_last[i][8 as number] as any).toString() : '';
		  item.j = (this.data_last[i][9 as number] as any || (this.data_last[i][9 as number] as any) === 0) ? (this.data_last[i][9 as number] as any).toString() : '';      
		  item.k = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';
		  item.l = (this.data_last[i][11 as number] as any || (this.data_last[i][11 as number] as any) === 0) ? (this.data_last[i][11 as number] as any).toString() : '';  
		  item.m = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
		  item.n = (this.data_last[i][13 as number] as any || (this.data_last[i][13 as number] as any) === 0) ? (this.data_last[i][13 as number] as any).toString() : '';   
		  item.o = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';
		  item.p = (this.data_last[i][15 as number] as any || (this.data_last[i][15 as number] as any) === 0) ? (this.data_last[i][15 as number] as any).toString() : '';
		  
		  recordList.push(item);
		}  
	}
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
	console.log ("lista = " + recordList.length);
    this.eventDataFileExcel.emit(dataFileExcel); 
  }
  
  
  private getListFiles_SIAF_MEF_BN() {
    let recordList: IFileNiubiz[] = [];  
    for (let i = 0; i < this.data_last.length; i++) {     

		if((this.data_last[i][2 as number] as any)) {
		  let item = new FileNiubiz();      
		  item.fila = (i + 1).toString();      
		  item.a = (this.data_last[i][2 as number] as any) ? (this.data_last[i][2 as number] as any).toString() : '';
		  item.b = (this.data_last[i][5 as number] as any || (this.data_last[i][5 as number] as any) === 0) ? (this.data_last[i][5 as number] as any).toString() : '';
		  item.c = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';      
		  item.d = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
		  item.e = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';

		  recordList.push(item);
		}  
	}
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      lista: recordList
    }
	console.log ("lista = " + recordList.length);
    this.eventDataFileExcel.emit(dataFileExcel); 
  }
    
   
  private getListFiles_HERMES_SIAF() {
    let recordList: IFileNiubiz[] = [];  
    for (let i = 0; i < this.data_last.length; i++) {     

		if((this.data_last[i][2 as number] as any)) {
		  let item = new FileNiubiz();      
		  item.fila = (i + 1).toString();      
		  item.a = (this.data_last[i][0 as number] as any || (this.data_last[i][0 as number] as any) === 0) ? (this.data_last[i][0 as number] as any).toString() : '';
		  item.b = (this.data_last[i][1 as number] as any || (this.data_last[i][1 as number] as any) === 0) ? (this.data_last[i][1 as number] as any).toString() : '';
		  item.c = (this.data_last[i][2 as number] as any || (this.data_last[i][2 as number] as any) === 0) ? (this.data_last[i][2 as number] as any).toString() : '';      
		  item.d = this.funciones.convertirFechaDateToString(this.ExcelDateToJSDate1((this.data_last[i][3 as number] as any).toString()));
		  item.e = (this.data_last[i][4 as number] as any || (this.data_last[i][4 as number] as any) === 0) ? (this.data_last[i][4 as number] as any).toString() : '';
		  item.f = (this.data_last[i][5 as number] as any || (this.data_last[i][5 as number] as any) === 0) ? (this.data_last[i][5 as number] as any).toString() : '';
		  item.g = (this.data_last[i][6 as number] as any || (this.data_last[i][6 as number] as any) === 0) ? (this.data_last[i][6 as number] as any).toString() : '';
		  item.h = (this.data_last[i][7 as number] as any || (this.data_last[i][7 as number] as any) === 0) ? (this.data_last[i][7 as number] as any).toString() : '';
		  item.i = (this.data_last[i][8 as number] as any || (this.data_last[i][8 as number] as any) === 0) ? (this.data_last[i][8 as number] as any).toString() : '';
		  item.j = (this.data_last[i][9 as number] as any || (this.data_last[i][9 as number] as any) === 0) ? (this.data_last[i][9 as number] as any).toString() : '';      
		  item.k = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';
		  item.l = (this.data_last[i][11 as number] as any || (this.data_last[i][11 as number] as any) === 0) ? (this.data_last[i][11 as number] as any).toString() : '';  
		  item.m = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
		  item.n = (this.data_last[i][13 as number] as any || (this.data_last[i][13 as number] as any) === 0) ? (this.data_last[i][13 as number] as any).toString() : '';   
		  item.o = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';
		  item.p = (this.data_last[i][15 as number] as any || (this.data_last[i][15 as number] as any) === 0) ? (this.data_last[i][15 as number] as any).toString() : '';
		  
		  recordList.push(item);
		}  
	}
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
	console.log ("lista = " + recordList.length);
    this.eventDataFileExcel.emit(dataFileExcel); 
  }
  
  
  private getListFiles_HERMES_BN() {
    let recordList: IFileNiubiz[] = [];  
    for (let i = 0; i < this.data_last.length; i++) {     

		if((this.data_last[i][2 as number] as any)) {
		  let item = new FileNiubiz();      
		  item.fila = (i + 1).toString();      
		  item.a = (this.data_last[i][2 as number] as any) ? (this.data_last[i][2 as number] as any).toString() : '';
		  item.b = (this.data_last[i][5 as number] as any || (this.data_last[i][5 as number] as any) === 0) ? (this.data_last[i][5 as number] as any).toString() : '';
		  item.c = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';      
		  item.d = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
		  item.e = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';

		  recordList.push(item);
		}  
	}
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      lista: recordList
    }
	console.log ("lista = " + recordList.length);
    this.eventDataFileExcel.emit(dataFileExcel); 
  }
  
  private getListFiles_CCI_SPRL() {
    let recordList: IFileNiubiz[] = [];  
    for (let i = 0; i < this.data_last.length; i++) {     

		if((this.data_last[i][2 as number] as any)) {
		  let item = new FileNiubiz();      
		  item.fila = (i + 1).toString();      
		  item.a = (this.data_last[i][2 as number] as any) ? (this.data_last[i][2 as number] as any).toString() : '';
		  item.b = (this.data_last[i][5 as number] as any || (this.data_last[i][5 as number] as any) === 0) ? (this.data_last[i][5 as number] as any).toString() : '';
		  item.c = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';      
		  item.d = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
		  item.e = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';

		  recordList.push(item);
		}  
	}
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
	console.log ("lista = " + recordList.length);
    this.eventDataFileExcel.emit(dataFileExcel); 
  }
  
  
  private getListFiles_NIUBIZ_APP_QR_SID() {
    let recordList: IFileNiubiz[] = [];
    for (let i = 0; i < this.data_last.length; i++) {
      let tipoOperacion: string = (this.data_last[i][7 as number] as any) ? (this.data_last[i][7 as number] as any).toString() : '';         
      let textoValidar = (tipoOperacion !== '') ? (this.funciones.eliminarEspaciosEnBlanco(tipoOperacion.toUpperCase())) : '';
      if (!(textoValidar.substring(0, 5) === 'SALDO') || textoValidar === '') {
        let item = new FileNiubiz();      
        item.fila = (i + 1).toString();      
        item.a = (this.data_last[i][0 as number] as any || (this.data_last[i][0 as number] as any) === 0) ? (this.data_last[i][0 as number] as any).toString() : '';
        item.b = (this.data_last[i][1 as number] as any || (this.data_last[i][1 as number] as any) === 0) ? (this.data_last[i][1 as number] as any).toString() : '';
        item.c = (this.data_last[i][2 as number] as any || (this.data_last[i][2 as number] as any) === 0) ? (this.data_last[i][2 as number] as any).toString() : '';
        item.d = (this.data_last[i][3 as number] as any || (this.data_last[i][3 as number] as any) === 0) ? (this.data_last[i][3 as number] as any).toString() : '';
        item.e = (this.data_last[i][4 as number] as any) ? this.funciones.formatearFechaLargaDMY((this.data_last[i][4 as number] as any).toString()) : '';
        item.f = (this.data_last[i][5 as number] as any) ? this.funciones.formatearFechaCortaDMY((this.data_last[i][5 as number] as any).toString()) : '';
        item.g = (this.data_last[i][6 as number] as any || (this.data_last[i][6 as number] as any) === 0) ? (this.data_last[i][6 as number] as any).toString() : '';
        item.h = (this.data_last[i][7 as number] as any || (this.data_last[i][7 as number] as any) === 0) ? (this.data_last[i][7 as number] as any).toString() : '';
        item.i = (this.data_last[i][8 as number] as any || (this.data_last[i][8 as number] as any) === 0) ? (this.data_last[i][8 as number] as any).toString() : '';
        item.j = (this.data_last[i][9 as number] as any || (this.data_last[i][9 as number] as any) === 0) ? (this.data_last[i][9 as number] as any).toString() : '';
        item.k = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';
        item.l = (this.data_last[i][11 as number] as any || (this.data_last[i][11 as number] as any) === 0) ? (this.data_last[i][11 as number] as any).toString() : '';
        item.m = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
        item.n = (this.data_last[i][13 as number] as any || (this.data_last[i][13 as number] as any) === 0) ? (this.data_last[i][13 as number] as any).toString() : '';
        item.o = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';
        item.p = (this.data_last[i][15 as number] as any || (this.data_last[i][15 as number] as any) === 0) ? (this.data_last[i][15 as number] as any).toString() : '';
        item.q = (this.data_last[i][16 as number] as any || (this.data_last[i][16 as number] as any) === 0) ? (this.data_last[i][16 as number] as any).toString() : '';
        item.r = (this.data_last[i][17 as number] as any || (this.data_last[i][17 as number] as any) === 0) ? (this.data_last[i][17 as number] as any).toString() : '';
        item.s = (this.data_last[i][18 as number] as any || (this.data_last[i][18 as number] as any) === 0) ? (this.data_last[i][18 as number] as any).toString() : '';
        item.t = (this.data_last[i][19 as number] as any || (this.data_last[i][19 as number] as any) === 0) ? (this.data_last[i][19 as number] as any).toString() : '';
        item.u = (this.data_last[i][20 as number] as any || (this.data_last[i][20 as number] as any) === 0) ? (this.data_last[i][20 as number] as any).toString() : '';
        item.v = (this.data_last[i][21 as number] as any || (this.data_last[i][21 as number] as any) === 0) ? (this.data_last[i][21 as number] as any).toString() : '';
        item.w = (this.data_last[i][22 as number] as any || (this.data_last[i][22 as number] as any) === 0) ? (this.data_last[i][22 as number] as any).toString() : '';
        item.x = (this.data_last[i][23 as number] as any || (this.data_last[i][23 as number] as any) === 0) ? (this.data_last[i][23 as number] as any).toString() : '';
        recordList.push(item);
      }
    }
    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }
    this.eventDataFileExcel.emit(dataFileExcel);    
  }

  private getListFiles_BANCARIA_APP_QR_SID() {
    let recordList: IFileNiubiz[] = [];  
    for (let i = 0; i < this.data_last.length; i++) {

      if((this.data_last[i][2 as number] as any)) {
        let item = new FileNiubiz();      
        item.fila = (i + 1).toString();      
        item.a = (this.data_last[i][2 as number] as any) ? (this.data_last[i][2 as number] as any).toString() : '';
        item.b = (this.data_last[i][5 as number] as any || (this.data_last[i][5 as number] as any) === 0) ? (this.data_last[i][5 as number] as any).toString() : '';
        item.c = (this.data_last[i][10 as number] as any || (this.data_last[i][10 as number] as any) === 0) ? (this.data_last[i][10 as number] as any).toString() : '';      
        item.d = (this.data_last[i][12 as number] as any || (this.data_last[i][12 as number] as any) === 0) ? (this.data_last[i][12 as number] as any).toString() : '';
        item.e = (this.data_last[i][14 as number] as any || (this.data_last[i][14 as number] as any) === 0) ? (this.data_last[i][14 as number] as any).toString() : '';

        recordList.push(item);
      }  
	  }

    let dataFileExcel: IDataFileExcel = {
      servicioDePago: this.servicioDePago,
      nameFile: this.nameFile,
      idGuid: this.idGuid,
      lista: recordList
    }

	  console.log ("lista = " + recordList.length);
    this.eventDataFileExcel.emit(dataFileExcel); 
  }
  
  private getFileExtension(nombreArchivo: string): string {    
    let lista: number[] = [];   
    for (let i = 0; i < nombreArchivo.length; i++) {
      if (nombreArchivo.substring(i, i + 1) === '.') {
        lista.push(i);          
      }         
    }
    return nombreArchivo.substring(lista[lista.length - 1], nombreArchivo.length)   
  }    

  private getDataByTypeOfService(service: string) {   
    this.posInitialData = null;
    if (service === SERVICIO_DE_PAGO.NIUBIZ) {
      this.numberFields = CANTIDAD_CAMPOS.NIUBIZ
    } else if (service === SERVICIO_DE_PAGO.DINERS) {
      this.numberFields = CANTIDAD_CAMPOS.DINERS
    } else if (service === SERVICIO_DE_PAGO.AMEX) {
      this.numberFields = CANTIDAD_CAMPOS.AMEX
    } else if (service === SERVICIO_DE_PAGO.NIUBIZ_SPRL) {
      this.numberFields = CANTIDAD_CAMPOS.NIUBIZ_SPRL
    } else if (service === SERVICIO_DE_PAGO.HERMES) {
      this.numberFields = CANTIDAD_CAMPOS.HERMES
    } else if (service === SERVICIO_DE_PAGO.BANCARIA_POS) {
      this.numberFields = CANTIDAD_CAMPOS.BANCARIA_POS
    } else if (service === SERVICIO_DE_PAGO.BANCARIA_POS_DINERS) {
      this.numberFields = CANTIDAD_CAMPOS.BANCARIA_POS
    } else if (service === SERVICIO_DE_PAGO.PAGALO_PE_BN) {
      this.numberFields = CANTIDAD_CAMPOS.PAGALO_PE_BN
    } else if (service === SERVICIO_DE_PAGO.MACMYPE_BN) {
      this.numberFields = CANTIDAD_CAMPOS.MACMYPE_BN
    } else if (service === SERVICIO_DE_PAGO.SIAF_MEF) {
      this.numberFields = CANTIDAD_CAMPOS.SIAF_MEF
    } else if (service === SERVICIO_DE_PAGO.SIAF_MEF_BN) {
      this.numberFields = CANTIDAD_CAMPOS.SIAF_MEF_BN
    } else if (service === SERVICIO_DE_PAGO.CCI_SPRL) {
      this.numberFields = CANTIDAD_CAMPOS.CCI_SPRL
    } else if (service === SERVICIO_DE_PAGO.HERMES_SIAF) {
      this.numberFields = CANTIDAD_CAMPOS.HERMES_SIAF
    } else if (service === SERVICIO_DE_PAGO.HERMES_BN) {
      this.numberFields = CANTIDAD_CAMPOS.HERMES_BN
    } else if (service === SERVICIO_DE_PAGO.NIUBIZ_APP_QR_SID) {
      this.numberFields = CANTIDAD_CAMPOS.NIUBIZ_APP_QR_SID
    } else if (service === SERVICIO_DE_PAGO.BANCARIA_APP_QR_SID) {
      this.numberFields = CANTIDAD_CAMPOS.BANCARIA_APP_QR_SID
    } else{
      this.numberFields = 0;
    }  
	
    if (service === SERVICIO_DE_PAGO.HERMES) {
      this.posInitialData = 11;
    }
    else if (service === SERVICIO_DE_PAGO.NIUBIZ_APP_QR_SID) {
      this.posInitialData = 7;
    }
    else if (service === SERVICIO_DE_PAGO.BANCARIA_POS || service === SERVICIO_DE_PAGO.PAGALO_PE_BN || service === SERVICIO_DE_PAGO.MACMYPE_BN 
              || service === SERVICIO_DE_PAGO.CCI_SPRL || service === SERVICIO_DE_PAGO.HERMES_BN || service === SERVICIO_DE_PAGO.SIAF_MEF_BN 
              || service === SERVICIO_DE_PAGO.BANCARIA_APP_QR_SID || service === SERVICIO_DE_PAGO.BANCARIA_POS_DINERS) {
      this.posInitialData = 15;
    }
    else {
      console.log('data_source2', this.data_source.length);
      for (let i = 0; i < this.data_source.length; i++) {     
        if (this.data_source[i].length === this.numberFields) {
          this.posInitialData = i;      
          break;
        }
      }
    }

    if (this.posInitialData >= 0) {
      console.log('posInitialData', this.posInitialData);
      console.log('data_source3', this.data_source.length);
      if (service === SERVICIO_DE_PAGO.HERMES) {
        for (let i = this.posInitialData + 1; i < this.data_source.length; i++) {

          if(!(this.data_source[i].length === 0)) {
            this.data_last.push(this.data_source[i]);  
          }
        }
      } else if (service === SERVICIO_DE_PAGO.BANCARIA_POS || service === SERVICIO_DE_PAGO.PAGALO_PE_BN || service === SERVICIO_DE_PAGO.MACMYPE_BN  
                || service === SERVICIO_DE_PAGO.SIAF_MEF_BN || service === SERVICIO_DE_PAGO.CCI_SPRL || service === SERVICIO_DE_PAGO.HERMES_SIAF 
                || service === SERVICIO_DE_PAGO.HERMES_BN || service === SERVICIO_DE_PAGO.BANCARIA_APP_QR_SID || service === SERVICIO_DE_PAGO.BANCARIA_POS_DINERS) {
        for (let i = this.posInitialData + 1; i < this.data_source.length; i++) {       
        
          if(!(this.data_source[i].length === 0)) {
            this.data_last.push(this.data_source[i]);  
          }		
        }  
      } else {
        console.log('data_source4', this.data_source);
        for (let i = this.posInitialData + 1; i < this.data_source.length; i++) {
        
          if(this.data_source[i].length === 0) {
            break;
          }   
          this.data_last.push(this.data_source[i]);
        } 		
      }
    }     
  }

  private validateFieldConfiguration(service: string) {
    
    let response = {
      isValid: true,
      message: ''
    }    

    for (let i = 0; i < this.data_last.length; i++) {      

      if (service === SERVICIO_DE_PAGO.NIUBIZ) {
      
        // validar el formato del campo 'RUC'     
        let ruc: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ.RUC as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ.RUC as number] as any).toString() : '';    
        if (ruc && ruc.length !== 11) {
          response.isValid = false;
          response.message = `El campo "RUC" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 11 dígitos`;
          break;   
        }

        // validar que el campo 'RUC' contenga solo números
        if (ruc && !this.funciones.validarSoloNumeros(ruc)) {
          response.isValid = false;
          response.message = `El campo "RUC" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
          break;   
        }

        // // Validar formato del campo 'FECHA Y HORA OPERACIÓN'
        let fechaHoraOperacion: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ.FECHA_HORA_OPERACION as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ.FECHA_HORA_OPERACION as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(fechaHoraOperacion);       
        if (fechaHoraOperacion && fecha1.length !== 12 && fechaHoraOperacion && fecha1.length !== 14) {
          response.isValid = false;
          response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (14 dígitos): los cuatro primeros dígitos son del año, seguido 
          de los dos dígitos del mes, seguido de los dos dígitos del día, seguido de los dos dígitos de la hora, seguido de los dos dígitos del mínuto, seguido de los dos dígitos del segundo.`;
          break; 
        }

        // validar que el campo 'FECHA Y HORA OPERACIÓN' contenga solo números
        let fechaHoraOperacion2: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ.FECHA_HORA_OPERACION as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ.FECHA_HORA_OPERACION as number] as any).toString() : '';    
        if (fechaHoraOperacion2 && !this.funciones.validarSoloNumeros(fechaHoraOperacion2)) {
          response.isValid = false;
          response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // Validar formato del campo 'FECHA DE DEPÓSITO'
        let fechaDeposito: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ.FECHA_DEPOSITO as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ.FECHA_DEPOSITO as number] as any).toString() : '';        
        let fecha2 = this.funciones.eliminarEspaciosEnBlanco(fechaDeposito);
        if (fechaDeposito && fecha2.length !== 8) {
          response.isValid = false;
          response.message = `El campo "FECHA DE DEPÓSITO" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha corta (8 dígitos): los cuatro primeros dígitos son del año, seguido de los dos dígitos del mes, seguido de los dos dígitos del día.`;
          break; 
        }

        // validar que el campo 'FECHA DE DEPÓSITO' contenga solo números
        let fechaDeposito2: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ.FECHA_DEPOSITO as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ.FECHA_DEPOSITO as number] as any).toString() : '';    
        if (fechaDeposito2 && !this.funciones.validarSoloNumeros(fechaDeposito2)) {
          response.isValid = false;
          response.message = `El campo "FECHA DE DEPÓSITO" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'Importe de Operación' contenga solo números
        let importeOperacion: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ.IMPORTE_OPERACION as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ.IMPORTE_OPERACION as number] as any).toString() : '';    
        if (importeOperacion && !this.funciones.validarSoloNumeros(importeOperacion)) {
          response.isValid = false;
          response.message = `El campo "IMPORTE DE OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'Monto DCC' contenga solo números
        let montoDCC: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ.MONTO_DCC as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ.MONTO_DCC as number] as any).toString() : '';    
        if (montoDCC && !this.funciones.validarSoloNumeros(montoDCC)) {
          response.isValid = false;
          response.message = `El campo "MONTO DCC" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'Comisión Total' contenga solo números
        let comisionTotal: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ.COMISION_TOTAL as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ.COMISION_TOTAL as number] as any).toString() : '';    
        if (comisionTotal && !this.funciones.validarSoloNumeros(comisionTotal)) {
          response.isValid = false;
          response.message = `El campo "COMISIÓN TOTAL" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'Comisión Niubiz' contenga solo números
        let comisionNiubiz: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ.COMISION_NIUBIZ as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ.COMISION_NIUBIZ as number] as any).toString() : '';    
        if (comisionNiubiz && !this.funciones.validarSoloNumeros(comisionNiubiz)) {
          response.isValid = false;
          response.message = `El campo "COMISIÓN NIUBIZ" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'IGV' contenga solo números
        let igv: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ.IGV as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ.IGV as number] as any).toString() : '';    
        if (igv && !this.funciones.validarSoloNumeros(igv)) {
          response.isValid = false;
          response.message = `El campo "IGV" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'SUMA DEPOSITADA' contenga solo números
        let sumaDepositada: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ.SUMA_DEPOSITADA as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ.SUMA_DEPOSITADA as number] as any).toString() : '';    
        if (sumaDepositada && !this.funciones.validarSoloNumeros(sumaDepositada)) {
          response.isValid = false;
          response.message = `El campo "SUMA DEPOSITADA" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

      } else if (service === SERVICIO_DE_PAGO.DINERS) {               

        // validar que el campo 'FECHA DE PAGO' contenga solo números
        let fechaPago: string = (this.data_last[i][POSITION_FIELDS_DINERS.FECHA_PAGO as number] as any) ? (this.data_last[i][POSITION_FIELDS_DINERS.FECHA_PAGO as number] as any).toString() : '';    
        if (fechaPago && !this.funciones.validarSoloNumeros(fechaPago)) {
          response.isValid = false;
          response.message = `El campo "FECHA PAGO" del registro de la fila ${i + this.posInitialData + 2} debe de contener el formato FECHA.`;
          break;   
        }

        // validar que el campo 'FECHA DE PAGO' contenga solo números mayores a 0 (es decir fechas del 01/01/1900 en adelante)       
        if (Number(fechaPago) < 1) {
          response.isValid = false;
          response.message = `El campo "FECHA PAGO" del registro de la fila ${i + this.posInitialData + 2} debe de contener el formato FECHA (1/1/1900 en adelante)`;
          break;   
        }

        // validar que el campo 'CONSUMO' contenga solo números
        let consumo: string = (this.data_last[i][POSITION_FIELDS_DINERS.CONSUMO as number] as any) ? (this.data_last[i][POSITION_FIELDS_DINERS.CONSUMO as number] as any).toString() : '';    
        if (consumo && !this.funciones.validarSoloNumeros(consumo)) {
          response.isValid = false;
          response.message = `El campo "CONSUMO" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'COMISIÓN' contenga solo números
        let comision: string = (this.data_last[i][POSITION_FIELDS_DINERS.COMISION as number] as any) ? (this.data_last[i][POSITION_FIELDS_DINERS.COMISION as number] as any).toString() : '';    
        if (comision && !this.funciones.validarSoloNumeros(comision)) {
          response.isValid = false;
          response.message = `El campo "COMISIÓN" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'COMISIÓN + MANEJO' contenga solo números
        let comisionManejo: string = (this.data_last[i][POSITION_FIELDS_DINERS.COMISION_MANEJO as number] as any) ? (this.data_last[i][POSITION_FIELDS_DINERS.COMISION_MANEJO as number] as any).toString() : '';    
        if (comisionManejo && !this.funciones.validarSoloNumeros(comisionManejo)) {
          response.isValid = false;
          response.message = `El campo "COMISIÓN + MANEJO" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'IGV' contenga solo números
        let igv: string = (this.data_last[i][POSITION_FIELDS_DINERS.IGV as number] as any) ? (this.data_last[i][POSITION_FIELDS_DINERS.IGV as number] as any).toString() : '';    
        if (igv && !this.funciones.validarSoloNumeros(igv)) {
          response.isValid = false;
          response.message = `El campo "IGV" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'CARGO' contenga solo números
        let cargo: string = (this.data_last[i][POSITION_FIELDS_DINERS.CARGO as number] as any) ? (this.data_last[i][POSITION_FIELDS_DINERS.CARGO as number] as any).toString() : '';    
        if (cargo && !this.funciones.validarSoloNumeros(cargo)) {
          response.isValid = false;
          response.message = `El campo "CARGO" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'NETO A PAGAR' contenga solo números
        let netoPagar: string = (this.data_last[i][POSITION_FIELDS_DINERS.NETO_PAGAR as number] as any) ? (this.data_last[i][POSITION_FIELDS_DINERS.NETO_PAGAR as number] as any).toString() : '';    
        if (netoPagar && !this.funciones.validarSoloNumeros(netoPagar)) {
          response.isValid = false;
          response.message = `El campo "NETO A PAGAR" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

      } else if (service === SERVICIO_DE_PAGO.AMEX) {        

        // validar eñ campo 'FECHA DEL DEPÓSITO'  que tenga el formato 'dd/mm/aaaa' o que la fecha sea correcta en los días y meses
        let fechaDeposito: string = (this.data_last[i][POSITION_FIELDS_AMEX.FECHA_DEPOSITO as number] as any) ? (this.data_last[i][POSITION_FIELDS_AMEX.FECHA_DEPOSITO as number] as any).toString() : '';            
        if (fechaDeposito && !this.funciones.validarFormatoFecha(this.funciones.eliminarEspaciosEnBlanco(fechaDeposito))) {
          response.isValid = false;
          response.message = `El campo "FECHA DEL DEPÓSITO" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato "dd/mm/aaaa" o la fecha no es correcta.`;
          break; 
        }

        // validar que el campo 'MONTO TOTAL' contenga solo números
        let montoTotal: string = (this.data_last[i][POSITION_FIELDS_AMEX.MONTO_TOTAL as number] as any) ? (this.data_last[i][POSITION_FIELDS_AMEX.MONTO_TOTAL as number] as any).toString() : '';    
        if (montoTotal && !this.funciones.validarSoloNumeros(montoTotal)) {
          response.isValid = false;
          response.message = `El campo "MONTO TOTAL" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'COMISIÓN TOTAL' contenga solo números
        let comisionTotal: string = (this.data_last[i][POSITION_FIELDS_AMEX.COMISION_TOTAL as number] as any) ? (this.data_last[i][POSITION_FIELDS_AMEX.COMISION_TOTAL as number] as any).toString() : '';    
        if (comisionTotal && !this.funciones.validarSoloNumeros(comisionTotal)) {
          response.isValid = false;
          response.message = `El campo "COMISIÓN TOTAL" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'COMISIÓN MERCHANT' contenga solo números
        let comisionMerchant: string = (this.data_last[i][POSITION_FIELDS_AMEX.COMISION_MERCHANT as number] as any) ? (this.data_last[i][POSITION_FIELDS_AMEX.COMISION_MERCHANT as number] as any).toString() : '';    
        if (comisionMerchant && !this.funciones.validarSoloNumeros(comisionMerchant)) {
          response.isValid = false;
          response.message = `El campo "COMISIÓN MERCHANT" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'IGV (COMISIÓN MERCHANT)' contenga solo números
        let igvComisionMerchant: string = (this.data_last[i][POSITION_FIELDS_AMEX.IGV_COMISION_MERCHANT as number] as any) ? (this.data_last[i][POSITION_FIELDS_AMEX.IGV_COMISION_MERCHANT as number] as any).toString() : '';    
        if (igvComisionMerchant && !this.funciones.validarSoloNumeros(igvComisionMerchant)) {
          response.isValid = false;
          response.message = `El campo "IGV (COMISIÓN MERCHANT)" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'NETO A DEPOSITAR' contenga solo números
        let netoDepositar: string = (this.data_last[i][POSITION_FIELDS_AMEX.NETO_DEPOSITAR as number] as any) ? (this.data_last[i][POSITION_FIELDS_AMEX.NETO_DEPOSITAR as number] as any).toString() : '';    
        if (netoDepositar && !this.funciones.validarSoloNumeros(netoDepositar)) {
          response.isValid = false;
          response.message = `El campo "NETO A DEPOSITAR" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'DESCUENTOS POR SERVICIOS' contenga solo números
        let descuentoPorServicio: string = (this.data_last[i][POSITION_FIELDS_AMEX.DESCUENTOS_POR_SERVICIOS as number] as any) ? (this.data_last[i][POSITION_FIELDS_AMEX.DESCUENTOS_POR_SERVICIOS as number] as any).toString() : '';    
        if (descuentoPorServicio && !this.funciones.validarSoloNumeros(descuentoPorServicio)) {
          response.isValid = false;
          response.message = `El campo "DESCUENTOS POR SERVICIOS" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }

        // validar que el campo 'MONTO DEPOSITADO' contenga solo números
        let montoDepositado: string = (this.data_last[i][POSITION_FIELDS_AMEX.MONTO_DEPOSITADO as number] as any) ? (this.data_last[i][POSITION_FIELDS_AMEX.MONTO_DEPOSITADO as number] as any).toString() : '';    
        if (montoDepositado && !this.funciones.validarSoloNumeros(montoDepositado)) {
          response.isValid = false;
          response.message = `El campo "MONTO DEPOSITADO" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
          break;   
        }
      }  else if (service === SERVICIO_DE_PAGO.HERMES) {        
		/*
        // // Validar formato del campo 'FECHA Y HORA OPERACIÓN'
        let fechaHoraOperacion: string = (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(fechaHoraOperacion);    
		let fecha: number = parseInt(fecha1);
		let fecha2 = this.funciones.convertirFechaLargaDateToString(this.ExcelDateToJSDate1(fecha));
		
		console.log("fechaHoraOperacion = " + fecha2);
        if (fecha2 && fecha2.length !== 19) {
          response.isValid = false;
          response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (19 dígitos): los cuatro primeros dígitos son del año, seguido 
          de los dos dígitos del mes, seguido de los dos dígitos del día, seguido de los dos dígitos de la hora, seguido de los dos dígitos del mínuto, seguido de los dos dígitos del segundo.`;
          break; 
        }
		*/
      }  else if (service === SERVICIO_DE_PAGO.BANCARIA_POS) {        
		/*
        // // Validar formato del campo 'FECHA Y HORA OPERACIÓN'
        let fechaHoraOperacion: string = (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(fechaHoraOperacion);    
		let fecha: number = parseInt(fecha1);
		let fecha2 = this.funciones.convertirFechaLargaDateToString(this.ExcelDateToJSDate1(fecha));
		
		console.log("fechaHoraOperacion = " + fecha2);
        if (fecha2 && fecha2.length !== 19) {
          response.isValid = false;
          response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (19 dígitos): los cuatro primeros dígitos son del año, seguido 
          de los dos dígitos del mes, seguido de los dos dígitos del día, seguido de los dos dígitos de la hora, seguido de los dos dígitos del mínuto, seguido de los dos dígitos del segundo.`;
          break; 
        }
		*/
      }  else if (service === SERVICIO_DE_PAGO.PAGALO_PE_BN) {        
		/*
        // // Validar formato del campo 'FECHA Y HORA OPERACIÓN'
        let fechaHoraOperacion: string = (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(fechaHoraOperacion);    
		let fecha: number = parseInt(fecha1);
		let fecha2 = this.funciones.convertirFechaLargaDateToString(this.ExcelDateToJSDate1(fecha));
		
		console.log("fechaHoraOperacion = " + fecha2);
        if (fecha2 && fecha2.length !== 19) {
          response.isValid = false;
          response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (19 dígitos): los cuatro primeros dígitos son del año, seguido 
          de los dos dígitos del mes, seguido de los dos dígitos del día, seguido de los dos dígitos de la hora, seguido de los dos dígitos del mínuto, seguido de los dos dígitos del segundo.`;
          break; 
        }
		*/
      }  else if (service === SERVICIO_DE_PAGO.MACMYPE_BN) {        
		/*
        // // Validar formato del campo 'FECHA Y HORA OPERACIÓN'
        let fechaHoraOperacion: string = (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(fechaHoraOperacion);    
		let fecha: number = parseInt(fecha1);
		let fecha2 = this.funciones.convertirFechaLargaDateToString(this.ExcelDateToJSDate1(fecha));
		
		console.log("fechaHoraOperacion = " + fecha2);
        if (fecha2 && fecha2.length !== 19) {
          response.isValid = false;
          response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (19 dígitos): los cuatro primeros dígitos son del año, seguido 
          de los dos dígitos del mes, seguido de los dos dígitos del día, seguido de los dos dígitos de la hora, seguido de los dos dígitos del mínuto, seguido de los dos dígitos del segundo.`;
          break; 
        }
		*/
      }  else if (service === SERVICIO_DE_PAGO.SIAF_MEF) {        
		/*
        // // Validar formato del campo 'FECHA Y HORA OPERACIÓN'
        let fechaHoraOperacion: string = (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(fechaHoraOperacion);    
		let fecha: number = parseInt(fecha1);
		let fecha2 = this.funciones.convertirFechaLargaDateToString(this.ExcelDateToJSDate1(fecha));
		
		console.log("fechaHoraOperacion = " + fecha2);
        if (fecha2 && fecha2.length !== 19) {
          response.isValid = false;
          response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (19 dígitos): los cuatro primeros dígitos son del año, seguido 
          de los dos dígitos del mes, seguido de los dos dígitos del día, seguido de los dos dígitos de la hora, seguido de los dos dígitos del mínuto, seguido de los dos dígitos del segundo.`;
          break; 
        }
		*/
	}  else if (service === SERVICIO_DE_PAGO.SIAF_MEF_BN) {        
		/*
        // // Validar formato del campo 'FECHA Y HORA OPERACIÓN'
        let fechaHoraOperacion: string = (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(fechaHoraOperacion);    
		let fecha: number = parseInt(fecha1);
		let fecha2 = this.funciones.convertirFechaLargaDateToString(this.ExcelDateToJSDate1(fecha));
		
		console.log("fechaHoraOperacion = " + fecha2);
        if (fecha2 && fecha2.length !== 19) {
          response.isValid = false;
          response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (19 dígitos): los cuatro primeros dígitos son del año, seguido 
          de los dos dígitos del mes, seguido de los dos dígitos del día, seguido de los dos dígitos de la hora, seguido de los dos dígitos del mínuto, seguido de los dos dígitos del segundo.`;
          break; 
        }
		*/
      }  else if (service === SERVICIO_DE_PAGO.CCI_SPRL) {        
		/*
        // // Validar formato del campo 'FECHA Y HORA OPERACIÓN'
        let fechaHoraOperacion: string = (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(fechaHoraOperacion);    
		let fecha: number = parseInt(fecha1);
		let fecha2 = this.funciones.convertirFechaLargaDateToString(this.ExcelDateToJSDate1(fecha));
		
		console.log("fechaHoraOperacion = " + fecha2);
        if (fecha2 && fecha2.length !== 19) {
          response.isValid = false;
          response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (19 dígitos): los cuatro primeros dígitos son del año, seguido 
          de los dos dígitos del mes, seguido de los dos dígitos del día, seguido de los dos dígitos de la hora, seguido de los dos dígitos del mínuto, seguido de los dos dígitos del segundo.`;
          break; 
        }
		*/		
      }  else if (service === SERVICIO_DE_PAGO.HERMES_SIAF) {        
		/*
        // // Validar formato del campo 'FECHA Y HORA OPERACIÓN'
        let fechaHoraOperacion: string = (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(fechaHoraOperacion);    
		let fecha: number = parseInt(fecha1);
		let fecha2 = this.funciones.convertirFechaLargaDateToString(this.ExcelDateToJSDate1(fecha));
		
		console.log("fechaHoraOperacion = " + fecha2);
        if (fecha2 && fecha2.length !== 19) {
          response.isValid = false;
          response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (19 dígitos): los cuatro primeros dígitos son del año, seguido 
          de los dos dígitos del mes, seguido de los dos dígitos del día, seguido de los dos dígitos de la hora, seguido de los dos dígitos del mínuto, seguido de los dos dígitos del segundo.`;
          break; 
        }
		*/		
      }  else if (service === SERVICIO_DE_PAGO.HERMES_BN) {        
		/*
        // // Validar formato del campo 'FECHA Y HORA OPERACIÓN'
        let fechaHoraOperacion: string = (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any) ? (this.data_last[i][POSITION_FIELDS_HERMES.FE_OPER as number] as any).toString() : '';        
        let fecha1 = this.funciones.eliminarEspaciosEnBlanco(fechaHoraOperacion);    
		let fecha: number = parseInt(fecha1);
		let fecha2 = this.funciones.convertirFechaLargaDateToString(this.ExcelDateToJSDate1(fecha));
		
		console.log("fechaHoraOperacion = " + fecha2);
        if (fecha2 && fecha2.length !== 19) {
          response.isValid = false;
          response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (19 dígitos): los cuatro primeros dígitos son del año, seguido 
          de los dos dígitos del mes, seguido de los dos dígitos del día, seguido de los dos dígitos de la hora, seguido de los dos dígitos del mínuto, seguido de los dos dígitos del segundo.`;
          break; 
        }
		*/		

		} else if (service === SERVICIO_DE_PAGO.NIUBIZ_APP_QR_SID) {
		  
			// validar el formato del campo 'RUC'     
			let ruc: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.RUC as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.RUC as number] as any).toString() : '';    
			if (ruc && ruc.length !== 11) {
			  response.isValid = false;
			  response.message = `El campo "RUC" del registro de la fila ${i + this.posInitialData + 2} no cuenta con 11 dígitos`;
			  break;   
			}

			// validar que el campo 'RUC' contenga solo números
			if (ruc && !this.funciones.validarSoloNumeros(ruc)) {
			  response.isValid = false;
			  response.message = `El campo "RUC" del registro de la fila ${i + this.posInitialData + 2} cuenta con una letra en su valor`;
			  break;   
			}

			// // Validar formato del campo 'FECHA Y HORA OPERACIÓN'
			let fechaHoraOperacion: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.FECHA_HORA_OPERACION as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.FECHA_HORA_OPERACION as number] as any).toString() : '';        
			let fecha1 = this.funciones.eliminarEspaciosEnBlanco(fechaHoraOperacion);       
			if (fechaHoraOperacion && fecha1.length !== 19) {
			  response.isValid = false;
			  response.message = `El campo "FECHA Y HORA OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha larga (14 dígitos): los cuatro primeros dígitos son del año, seguido 
			  de los dos dígitos del mes, seguido de los dos dígitos del día, seguido de los dos dígitos de la hora, seguido de los dos dígitos del mínuto, seguido de los dos dígitos del segundo.`;
			  break; 
			}

			// Validar formato del campo 'FECHA DE DEPÓSITO'
			let fechaDeposito: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.FECHA_DEPOSITO as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.FECHA_DEPOSITO as number] as any).toString() : '';        
			let fecha2 = this.funciones.eliminarEspaciosEnBlanco(fechaDeposito);
			if (fechaDeposito && fecha2.length !== 10) {
			  response.isValid = false;
			  response.message = `El campo "FECHA DE DEPÓSITO" del registro de la fila ${i + this.posInitialData + 2} no cuenta con el formato correcto para la fecha corta (8 dígitos): los cuatro primeros dígitos son del año, seguido de los dos dígitos del mes, seguido de los dos dígitos del día.`;
			  break; 
			}

			// validar que el campo 'Importe de Operación' contenga solo números
			let importeOperacion: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.IMPORTE_OPERACION as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.IMPORTE_OPERACION as number] as any).toString() : '';    
			if (importeOperacion && !this.funciones.validarSoloNumeros(importeOperacion)) {
			  response.isValid = false;
			  response.message = `El campo "IMPORTE DE OPERACIÓN" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
			  break;   
			}

			// validar que el campo 'Monto DCC' contenga solo números
			let montoDCC: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.MONTO_DCC as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.MONTO_DCC as number] as any).toString() : '';    
			if (montoDCC && !this.funciones.validarSoloNumeros(montoDCC)) {
			  response.isValid = false;
			  response.message = `El campo "MONTO DCC" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
			  break;   
			}

			// validar que el campo 'Comisión Total' contenga solo números
			let comisionTotal: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.COMISION_TOTAL as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.COMISION_TOTAL as number] as any).toString() : '';    
			if (comisionTotal && !this.funciones.validarSoloNumeros(comisionTotal)) {
			  response.isValid = false;
			  response.message = `El campo "COMISIÓN TOTAL" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
			  break;   
			}

			// validar que el campo 'Comisión Niubiz' contenga solo números
			let comisionNiubiz: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.COMISION_NIUBIZ as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.COMISION_NIUBIZ as number] as any).toString() : '';    
			if (comisionNiubiz && !this.funciones.validarSoloNumeros(comisionNiubiz)) {
			  response.isValid = false;
			  response.message = `El campo "COMISIÓN NIUBIZ" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
			  break;   
			}

			// validar que el campo 'IGV' contenga solo números
			let igv: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.IGV as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.IGV as number] as any).toString() : '';    
			if (igv && !this.funciones.validarSoloNumeros(igv)) {
			  response.isValid = false;
			  response.message = `El campo "IGV" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
			  break;   
			}

			// validar que el campo 'SUMA DEPOSITADA' contenga solo números
			let sumaDepositada: string = (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.SUMA_DEPOSITADA as number] as any) ? (this.data_last[i][POSITION_FIELDS_NIUBIZ_APP_QR_SID.SUMA_DEPOSITADA as number] as any).toString() : '';    
			if (sumaDepositada && !this.funciones.validarSoloNumeros(sumaDepositada)) {
			  response.isValid = false;
			  response.message = `El campo "SUMA DEPOSITADA" del registro de la fila ${i + this.posInitialData + 2} debe de contener solo números`;
			  break;   
			}

		}
    }  
    return response;  
  }

  ngOnChanges() {     
    this.nameFile = '';
    this.data_source = [];
    this.data_last = [];      
  }

}
