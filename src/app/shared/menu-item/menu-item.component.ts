import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { SidenavService } from '../../services/sidenav.service';
import { UsuariosService } from '../../services/seguridad/usuarios.service';

import { IUsuarioPerfil } from '../../interfaces/seguridad/usuario.interface';

import { BaseMenuUser } from '../../base/base-menu.abstract';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent extends BaseMenuUser implements OnInit {

  codeUser: number = environment.codeUsuario;

  @Input() collapsed = false;
  @Input() screenWidth = 0;

  constructor(
    override router: Router,
    override sidenavService: SidenavService,
    override usuarioService: UsuariosService,
  ) { super() }

  ngOnInit(): void {

    this.usuarioService.getUsuarioPerfilList( this.codeUser ).subscribe({
      next: ( data: IUsuarioPerfil[] ) => {
          this.perfilList = data;
      }
    });

    if ( this.roles.length == 1 ) {
      this.showPerfilMenu( this.roles[0] );
    }
  }

  getBodyClass(): string {
    let styleClass = '';

    if ( this.collapsed && this.screenWidth > 768 ) {
      styleClass = 'body-trimmed';
    }
    else if ( this.collapsed && this.screenWidth <= 768 && this.screenWidth > 0 ) {
      styleClass = 'body-md-screen';
    }

    return styleClass;
  }

}
