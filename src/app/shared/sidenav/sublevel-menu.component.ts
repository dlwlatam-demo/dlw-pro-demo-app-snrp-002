import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { INavbarData, fadeInOut } from '../../interfaces/sidenav.interface';

@Component({
  selector: 'app-sublevel-menu',
  template: `
    <ul *ngIf="collapsed && data.items && data.items.length > 0"
        [@submenu]="expanded
        ? {value: 'visible', 
            params: {transitionParams: '400ms cubic-bezier(0.86, 0, 0.07, 1)', height: '*'}}
        : {value: 'hidden',
            params: {transitionParams: '400ms cubic-bezier(0.86, 0, 0.07, 1)', height: '0'}}"
        class="sublevel-nav">
        <li *ngFor="let item of data.items" class="sublevel-nav-item">
          <a class="sublevel-nav-link" *ngIf="showSubMenu( item )"
              (click)="handleClick( item )">
            <span class="sublevel-link-text" @fadeInOut *ngIf="collapsed">{{ item.label }}</span>
            <mat-icon *ngIf="item.items && collapsed" class="menu-collapse-icon">
                {{ ( !item.expanded ) ? 'arrow_right' : 'arrow_drop_down' }}
            </mat-icon>
          </a>
          <a [ngClass]="getClassSub( item )" *ngIf="showSubMenuOpci( item )"
              [routerLink]="[item.routerLink]" routerLinkActive="active-sublevel" 
              [routerLinkActiveOptions]="{exact: true}">
              <span class="sublevel-link-text" @fadeInOut *ngIf="collapsed">{{ item.label }}</span>
          </a>
          <div *ngIf="item.items && item.items.length > 0">
            <app-sublevel-menu
              [data]="item"
              [collapsed]="collapsed"
              [multiple]="multiple"
              [expanded]="item.expanded"
              [menuOpci]="menuOpci">
            </app-sublevel-menu>
          </div>
        </li>
    </ul>
  `,
  styleUrls: ['./sidenav.component.scss'],
  animations: [
    fadeInOut,
    trigger('submenu', [
      state('hidden', style({
        height: '0',
        overflow: 'hidden'
      })),
      state('visible', style({
        height: '*'
      })),
      transition('visible <=> hidden', [style({ overflow: 'hidden' }),
        animate('{{ transitionParams }}')]),
      transition('void => *', animate(0))
    ])
  ]
})
export class SublevelMenuComponent implements OnInit {

  @Input() data: INavbarData = {
    routerLink: '',
    icon: '',
    label: '',
    items: []
  }

  @Input() collapsed: boolean = false;
  @Input() animating: boolean | undefined;
  @Input() expanded: boolean | undefined;
  @Input() multiple: boolean = false;
  @Input() menuSub: number[] = [];
  @Input() menuOpci: number[] = [];

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  handleClick( item: any ): void {
    console.log("item", item);
    
    if ( !this.multiple ) {
      if ( this.data.items && this.data.items.length > 0 ) {
        for ( let modelItem of this.data.items ) {
          if ( item !== modelItem && modelItem.expanded ) {
            modelItem.expanded = false;
          }
        }
      }
    }

    item.expanded = !item.expanded;
  }

  showSubMenu( item: INavbarData ): boolean {
    return ( item.items && item.items.length > 0 && this.menuSub.includes( item.codigo! ) )!;
  }

  showSubMenuOpci( item: INavbarData ): boolean {
    return ( (!item.items || ( item.items && item.items.length === 0 )) && 
             ( this.menuSub.includes( item.codigo! ) || this.menuOpci.includes( item.codigo! ) ) )!;
  }

  getClassSub( item: INavbarData ): string {
    let styleClass: string;

    if ( this.menuSub.includes( item.codigo! ) )
      styleClass = 'sublevel-nav-link';
    else
      styleClass = 'sublevel-nav-link--inside'

    return styleClass
  }

}
