import { Component, OnInit, Output, EventEmitter, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, timer, of } from 'rxjs';
import { switchMap, catchError, filter } from 'rxjs/operators';

import Swal from 'sweetalert2';

import { SideNavToggle, INavbarData, fadeInOut } from '../../interfaces/sidenav.interface';

import { AuthService } from '../../services/auth/auth.service';
import { GeneralService } from '../../services/general.service';
import { SidenavService } from '../../services/sidenav.service';

import { BaseMenuUser } from '../../base/base-menu.abstract';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  animations: [
    fadeInOut
  ]
})
export class SidenavComponent extends BaseMenuUser implements OnInit {

  @Output() onToggleSideNav: EventEmitter<SideNavToggle> = new EventEmitter();

  collapsed: boolean = true;
  screenWidth: number = 0;
  multiple: boolean = false;

  menuItems: INavbarData[] = [];

  viewMenu: boolean = true;

  timerSubscription: Subscription = new Subscription();

  constructor(
    override router: Router,
    override sidenavService: SidenavService,
    private authService: AuthService,
    private generalService: GeneralService
  ) { super() }

  ngOnInit(): void {
    this.initMenuOptions();
    this.screenWidth = window.innerWidth;
    this.perfil = localStorage.getItem('perfil_sarf')!;

    if ( this.router.url == '/menu' ) { 
      this.viewMenu = false;
      this.collapsed = false; 
    }

    if ( this.perfil == null ) {
      return;
    }
    else {
      this.showMenuOptions( this.perfil );
    }

    this.menuItems = this.sidenavService.menu;
    this.toggleSideNav(this.collapsed, this.screenWidth);
    
    this.timerSession();
  }

  @HostListener('window:resize', ['$event'])
  onResize( event: any ) {
    this.screenWidth = window.innerWidth;

    if ( this.screenWidth <= 950 ) {
      this.collapsed = false;
      this.toggleSideNav( this.collapsed, this.screenWidth );
    }
    else {
      this.collapsed = true;
      this.toggleSideNav( this.collapsed, this.screenWidth );
    }
  }

  toggleCollapse(): void {
    this.collapsed = !this.collapsed;
    this.toggleSideNav( this.collapsed, this.screenWidth );
  }

  closeSidenav(): void {
    this.collapsed = false;

    for ( let menu of this.menuItems ) {
      menu.expanded = false;
    }
    
    this.toggleSideNav( this.collapsed, this.screenWidth );
  }

  handleClick( item: INavbarData ): void {
    this.shrinkItems( item );
    item.expanded = !item.expanded
  }

  getActiveClass( data: INavbarData ): string {
    return data.expanded ? 'active' : '';
  }

  shrinkItems( item: INavbarData ): void {
    if ( !this.multiple ) {
      for ( let modelItem of this.menuItems ) {
        if ( item !== modelItem && modelItem.expanded ) {
          modelItem.expanded = false;
        }
      }
    }
  }

  toggleSideNav( collapsed: boolean, screeWidth: number ) {
    this.onToggleSideNav.emit({ collapsed, screeWidth });
  }

  showMenu( data: INavbarData ): boolean {
    return ( data.items && data.items.length > 0 && this.menuPadrPadr.includes( data.codigo! ) )!;
  }

  timerSession() {
    this.timerSubscription = timer(0, 1000 * 60).pipe(
      switchMap(() => {
        return this.generalService.getCbo_TipoDocumentosVarios().pipe(
          catchError( err => {
            // Handle errors
            console.error(err);
			      this.logout();

			      Swal.fire('Su sesión a expirado. ', '', 'warning');
            
            return of(undefined);
          }));
        }),
        filter( data => data !== undefined)
      )
      .subscribe( data => {
        console.log("timer: " + data);
      });
  }

  ngOnDestroy() {
    this.timerSubscription.unsubscribe(); 
  }

  logout(): void{
    this.authService.logout();
  }

}
