import { Component, OnInit } from '@angular/core';

import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-firma-digital',
  templateUrl: './firma-digital.component.html',
  styleUrls: ['./firma-digital.component.scss']
})
export class FirmaDigitalComponent implements OnInit {

  guid!: string;
  fileName!: string;
  roles?: string[];
  perfil?: string;

  constructor(
    private generalService: GeneralService
  ) { }

  ngOnInit(): void {
  }

  addUploadedFile(file: any) {
    this.guid = file.fileId
    this.fileName = file.fileName
  }

  postAdjunto() {
    
    const data = {
      idGuidDocu: this.guid,
      deDocuAdju: this.fileName,
      coTipoAdju: "0001",
      coZonaRegi: "01",
      coOficRegi: "01"
    }

    this.generalService.grabarAdjunto( data ).subscribe({
      next: ( d ) => {
        console.log('Ok', d);
      },
      error: ( e ) => {
        console.log( e );
      }
    });
  }

  sendFirma() {
    this.generalService.subeRedis( this.guid ).subscribe({
      next: ( data ) => {
        console.log( data )
		let guid = data.idCarga;
		
		// se continua solo si hay exito en carga archivo a redis:
		let token = localStorage.getItem("t_sarf") || '';
		let jti = localStorage.getItem("t_jti") || '';
		  
		let mapFormView = document.createElement("form");
		  
		mapFormView.method = "POST";
		mapFormView.action = `https://sarf-digital-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe/sarfsunarp/api/v1/firmar?guid=${guid}&token=${token}&jti=${jti}`;
		document.body.appendChild(mapFormView);
		mapFormView.submit();
		
		
		
      }
    });

  }
}
