import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../services/auth/auth-gaurd.service';

const routes: Routes = [
  {
    path: 'mantenimientos',
    loadChildren: () => import('./mantenimientos/mantenimientos.module').then( m => m.MantenimientosModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'procesos',
    loadChildren: () => import('./procesos/procesos.module').then( m => m.ProcesosModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'reportes',
    loadChildren: () => import('./reportes/reportes.module').then( m => m.ReportesModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'seguridad',
    loadChildren: () => import('./seguridad/seguridad.module').then( m => m.SeguridadModule ),
    canActivate: [ AuthGaurdService ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
