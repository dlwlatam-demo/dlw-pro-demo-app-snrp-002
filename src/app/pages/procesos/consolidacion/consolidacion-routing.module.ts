import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { BandejaRecaudComponent } from './recaudacion-medios-electronicos/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionComponent } from './recaudacion-medios-electronicos/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionComponent } from './recaudacion-medios-electronicos/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionMediosElectronicosGuard } from 'src/app/core/guards/procesos/consolidacion/refresh-recaudacion-medios-electronicos.guard';

import { BandejaRecaudSprlComponent } from './recaudacion-sprl-virtual/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionSprlComponent } from './recaudacion-sprl-virtual/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionSprlComponent } from './recaudacion-sprl-virtual/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionSprlGuard } from 'src/app/core/guards/procesos/consolidacion/refresh-recaudacion-sprl-virtual.guard';

import { BandejaRecaudPagaloPeComponent } from './recaudacion-pagalo-pe/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionPagaloPeComponent } from './recaudacion-pagalo-pe/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionPagaloPeComponent } from './recaudacion-pagalo-pe/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionPagaloPeGuard } from 'src/app/core/guards/procesos/consolidacion/refresh-recaudacion-pagalo-pe.guard';

import { BandejaRecaudMacmypeComponent } from './recaudacion-macmype/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionMacmypeComponent } from './recaudacion-macmype/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionMacmypeComponent } from './recaudacion-macmype/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionMacmypeGuard } from 'src/app/core/guards/procesos/consolidacion/refresh-recaudacion-macmype.guard';

import { BandejaRecaudScunacRemesasComponent } from './recaudacion-scunac-remesas/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionScunacRemesasComponent } from './recaudacion-scunac-remesas/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionScunacRemesasComponent } from './recaudacion-scunac-remesas/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionScunacRemesasGuard } from 'src/app/core/guards/procesos/consolidacion/refresh-recaudacion-scunac-remesas.guard';

import { BandejaRecaudAppQrSidComponent } from './recaudacion-app-qr-sid/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionAppQrSidComponent } from './recaudacion-app-qr-sid/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionAppQrSidComponent } from './recaudacion-app-qr-sid/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionAppQrSidGuard } from 'src/app/core/guards/procesos/consolidacion/refresh-recaudacion-app-qr-sid.guard';


const routes: Routes = [  
  { path: 'recaudacion_medios_electronicos',
    children: [
      { path: 'bandeja', component: BandejaRecaudComponent },
      { path: 'nuevo', component: NuevaRecaudacionComponent, canActivate: [RefreshRecaudacionMediosElectronicosGuard] },
      { path: 'editar', component: EditarRecaudacionComponent, canActivate: [RefreshRecaudacionMediosElectronicosGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionComponent, canActivate: [RefreshRecaudacionMediosElectronicosGuard] }, 
      { path: 'consultar', component: EditarRecaudacionComponent, canActivate: [RefreshRecaudacionMediosElectronicosGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'recaudacion_app_qr_sid',
    children: [
      { path: 'bandeja', component: BandejaRecaudAppQrSidComponent },
      { path: 'nuevo', component: NuevaRecaudacionAppQrSidComponent, canActivate: [RefreshRecaudacionAppQrSidGuard] },
      { path: 'editar', component: EditarRecaudacionAppQrSidComponent, canActivate: [RefreshRecaudacionAppQrSidGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionAppQrSidComponent, canActivate: [RefreshRecaudacionAppQrSidGuard] }, 
      { path: 'consultar', component: EditarRecaudacionAppQrSidComponent, canActivate: [RefreshRecaudacionAppQrSidGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'recaudacion_sprl_virtual',
    children: [
      { path: 'bandeja', component: BandejaRecaudSprlComponent },
      { path: 'nuevo', component: NuevaRecaudacionSprlComponent, canActivate: [RefreshRecaudacionSprlGuard] },
      { path: 'editar', component: EditarRecaudacionSprlComponent, canActivate: [RefreshRecaudacionSprlGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionSprlComponent, canActivate: [RefreshRecaudacionSprlGuard] }, 
      { path: 'consultar', component: EditarRecaudacionSprlComponent, canActivate: [RefreshRecaudacionSprlGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'recaudacion_pagalo_pe',
    children: [
      { path: 'bandeja', component: BandejaRecaudPagaloPeComponent },
      { path: 'nuevo', component: NuevaRecaudacionPagaloPeComponent, canActivate: [RefreshRecaudacionPagaloPeGuard] },
      { path: 'editar', component: EditarRecaudacionPagaloPeComponent, canActivate: [RefreshRecaudacionPagaloPeGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionPagaloPeComponent, canActivate: [RefreshRecaudacionPagaloPeGuard] }, 
      { path: 'consultar', component: EditarRecaudacionPagaloPeComponent, canActivate: [RefreshRecaudacionPagaloPeGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },  
  { path: 'recaudacion_macmype',
    children: [
      { path: 'bandeja', component: BandejaRecaudMacmypeComponent },
      { path: 'nuevo', component: NuevaRecaudacionMacmypeComponent, canActivate: [RefreshRecaudacionMacmypeGuard] },
      { path: 'editar', component: EditarRecaudacionMacmypeComponent, canActivate: [RefreshRecaudacionMacmypeGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionMacmypeComponent, canActivate: [RefreshRecaudacionMacmypeGuard] }, 
      { path: 'consultar', component: EditarRecaudacionMacmypeComponent, canActivate: [RefreshRecaudacionMacmypeGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },   
  { path: 'recaudacion_scunac_remesas',
    children: [
      { path: 'bandeja', component: BandejaRecaudScunacRemesasComponent },
      { path: 'nuevo', component: NuevaRecaudacionScunacRemesasComponent, canActivate: [RefreshRecaudacionScunacRemesasGuard] },
      { path: 'editar', component: EditarRecaudacionScunacRemesasComponent, canActivate: [RefreshRecaudacionScunacRemesasGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionScunacRemesasComponent, canActivate: [RefreshRecaudacionScunacRemesasGuard] }, 
      { path: 'consultar', component: EditarRecaudacionScunacRemesasComponent, canActivate: [RefreshRecaudacionScunacRemesasGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsolidacionRoutingModule { }
