import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Subject } from 'rxjs';
import { concatMap, filter, first, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Dropdown } from 'primeng/dropdown';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { GeneralService } from 'src/app/services/general.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { IDataFileSftpText, IDataFiltrosRecaudacionMacmype, IRecaudacionMacmype } from 'src/app/interfaces/consolidacion-macmype';
import { RecaudacionMacmype } from 'src/app/models/procesos/consolidacion/recaudacion-macmype.model';
import { RecaudacionMacmypeService } from 'src/app/services/procesos/consolidacion/recaudacion-macmype.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { UtilService } from 'src/app/services/util.service';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { IResponse } from 'src/app/interfaces/general.interface';
import { ArchivoAdjunto } from '../../../../../models/archivo-adjunto.model';
import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';

@Component({
  selector: 'app-nueva-recaudacion-macmype',
  templateUrl: './nueva-recaudacion.component.html',
  styleUrls: ['./nueva-recaudacion.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class NuevaRecaudacionMacmypeComponent implements OnInit, OnDestroy  {
  
  formRecaudacionMacmype: FormGroup;
  private unsubscribe$ = new Subject<void>();
  public titulo: string;
  public listaZonaRegistral: ZonaRegistral[];
  public listaOficinaRegistral: OficinaRegistral[];
  public listaLocal: Local[]; 
  public fechaMaxima: Date;
  private userCode: any;
  public loadingOficinaRegistral: boolean = false;
  public loadingLocal: boolean = false;
  public SERVICIO_MACMYPE = SERVICIO_DE_PAGO.MACMYPE;
  public nameTxt: string = '';
  public servicioOperador: string = '';
  public files: ArchivoAdjunto[] = [];
  public sizeFiles: number = 0;
  private recaudacionMacmype: RecaudacionMacmype;
  public reprocesarRecaudacionMacmype: IRecaudacionMacmype; 

  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;

  constructor(    
    private formBuilder: FormBuilder,
    private generalService: GeneralService,
    private router: Router,
    private recaudacionMacmypeService: RecaudacionMacmypeService,
    private funciones: Funciones,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService
  ) {   
  }

  async ngOnInit() {
    this.fechaMaxima = new Date();
    this.construirFormulario();
    this.reprocesarRecaudacionMacmype = await this.sharingInformationService.compartirRecaudacionMacmypeObservable.pipe(first()).toPromise();   
    if (!this.reprocesarRecaudacionMacmype) {
      this.titulo = 'Nuevo';
      this.userCode = localStorage.getItem('user_code') || ''; 
      this.inicilializarListas();
      this.obtenerZonasRegistrales();
    } else {
      this.titulo = 'Reprocesar';
    }                
    this.recaudacionMacmype = new RecaudacionMacmype();        
  }

  private construirFormulario() {
    this.formRecaudacionMacmype = this.formBuilder.group({
      zona: [''],
      oficina: [''],
      local: [''],        
      fecha: [this.fechaMaxima],
      docs: this.formBuilder.array([])  
    });
  }

  private inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];     
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
  }

  private obtenerZonasRegistrales() {
    this.utilService.onShowProcessLoading("Cargando la data");
    this.generalService.getCbo_Zonas_Usuario( this.userCode).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (res: ZonaRegistral[]) => {  
      if (res.length === 1) {   
        this.listaZonaRegistral.push({ coZonaRegi: res[0].coZonaRegi, deZonaRegi: res[0].deZonaRegi });    
        this.formRecaudacionMacmype.patchValue({ "zona": res[0].coZonaRegi});
        this.buscarOficinaRegistral();
      } else {    
        this.listaZonaRegistral.push(...res);
      }       
      this.utilService.onCloseLoading();
    }, (err: HttpErrorResponse) => {   
      this.utilService.onCloseLoading();                                  
    });
  }

  public buscarOficinaRegistral() { 
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formRecaudacionMacmype.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.formRecaudacionMacmype.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (res: OficinaRegistral[]) => {  
        if (res.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: res[0].coOficRegi, deOficRegi: res[0].deOficRegi });    
          this.formRecaudacionMacmype.patchValue({ "oficina": res[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...res);
        }
        this.loadingOficinaRegistral = false;     
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formRecaudacionMacmype.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.formRecaudacionMacmype.get('zona')?.value,this.formRecaudacionMacmype.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formRecaudacionMacmype.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err: HttpErrorResponse) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  public obtenerDataFileSftpText(event: IDataFileSftpText) {
    if (event.servicioDePago === this.SERVICIO_MACMYPE) {
		console.log("nameFile = " + event.nameFile);
      this.nameTxt = event.nameFile;
      this.recaudacionMacmype.noAdju0001 = event.nameFile;
      this.recaudacionMacmype.idGuidDocu01 = event.idGuid;
      this.recaudacionMacmype.trama01 = event.lista;
	  
    } 
  }

  public async procesar() {
    let resultado: boolean = await this.validarFormularioNuevo();
    if (!resultado) {      
      return;
    }     
    this.utilService.onShowProcessSaveEdit(); 
    this.asignarValoresNuevo();
    this.recaudacionMacmype.tiProc = '0'; // '0': es validar data, '1': es insertar
    this.recaudacionMacmypeService.procesarRecaudacionMacmype(this.recaudacionMacmype).pipe(      
      takeUntil(this.unsubscribe$), 
      filter((res1: IResponse) => { 
        if (res1.codResult < 0 ) {        
          this.utilService.onShowAlert("warning", "Atención", res1.msgResult);        
          return false;
        } else {          
          return true 
        } 
      }),     
      concatMap((res1: IResponse) => {   
        this.recaudacionMacmype.tiProc = '1'; // '0': es validar data, '1': es insertar     
        return this.recaudacionMacmypeService.procesarRecaudacionMacmype(this.recaudacionMacmype) 
      })
    ).subscribe((res: IResponse) => {     
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {  
        this.parse();       
        this.respuestaExitosaGrabar('PROCESÓ');
      }         
    }, (err: HttpErrorResponse) => {
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("error", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();        
      }
    });
  }

  private async respuestaExitosaGrabar(texto: string) {    
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Se ${texto} satisfactoriamente el/los registro(s)`);
    if (respuesta.isConfirmed) {
      let dataFiltrosRecaudacionMacmype: IDataFiltrosRecaudacionMacmype = await this.sharingInformationService.compartirDataFiltrosRecaudacionMacmypeObservable.pipe(first()).toPromise(); 
      if (dataFiltrosRecaudacionMacmype.esBusqueda) {
        dataFiltrosRecaudacionMacmype.esCancelar = true;
        this.sharingInformationService.compartirDataFiltrosRecaudacionMacmypeObservableData = dataFiltrosRecaudacionMacmype;
      }      
      this.router.navigate(['SARF/procesos/consolidacion/recaudacion_macmype/bandeja']);
    }
  }

  private async validarFormularioNuevo(): Promise<boolean> { 
    let registro = this.formRecaudacionMacmype.getRawValue();
    if (!registro.zona || registro.zona === "*") {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una zona registral.");
      return false;      
    } else if (!registro.oficina || registro.oficina === "*") {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una oficina registral.");
      return false;      
    } else if (!registro.local || registro.local === "*") {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un local.");
      return false;      
    } else if (!registro.fecha) {
      let respuesta = await this.utilService.onShowAlertPromise("warning", "Atención", `Seleccione la fecha de emisión.`);
      if (respuesta.isConfirmed) {
        document.getElementById('idFecha').focus();
        return false; 
      } else {
        return true;
      }    
    } else if (this.recaudacionMacmype.noAdju0001 === '') {
      this.utilService.onShowAlert("warning", "Atención", "Debe de adjuntar al menos 01 archivo de excel.");
      return false;  
    } else {
      return true;
    }
  }

  private asignarValoresNuevo() {    
    this.recaudacionMacmype.idCncl = 0;
    this.recaudacionMacmype.idTipoCncl = 7;
    this.recaudacionMacmype.coZonaRegi = this.formRecaudacionMacmype.get('zona').value;
    this.recaudacionMacmype.coOficRegi = this.formRecaudacionMacmype.get('oficina').value;
    this.recaudacionMacmype.coLocaAten = this.formRecaudacionMacmype.get('local').value;
    this.recaudacionMacmype.idOperPos = 1;    
    this.recaudacionMacmype.feCncl = this.funciones.convertirFechaDateToString(this.formRecaudacionMacmype.get('fecha').value);
  }

  public async reprocesar() {
    let resultado: boolean = await this.validarFormularioReprocesar();
    if (!resultado) {      
      return;
    }     
    this.utilService.onShowProcessSaveEdit(); 
    this.asignarValoresReprocesar();
    this.recaudacionMacmype.tiProc = '0'; // '0': es validar data, '1': es insertar
    this.recaudacionMacmypeService.reprocesaRecaudacionMacmype(this.recaudacionMacmype).pipe(      
      takeUntil(this.unsubscribe$), 
      filter((res1: IResponse) => { 
        if (res1.codResult < 0 ) {        
          this.utilService.onShowAlert("warning", "Atención", res1.msgResult);        
          return false;
        } else {          
          return true 
        } 
      }),     
      concatMap((res1: IResponse) => {   
        this.recaudacionMacmype.tiProc = '1'; // '0': es validar data, '1': es insertar     
        return this.recaudacionMacmypeService.reprocesaRecaudacionMacmype(this.recaudacionMacmype) 
      })
    ).subscribe((res: IResponse) => {     
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) { 
        this.parse();        
        this.respuestaExitosaGrabar('REPROCESÓ');

      }         
    }, (err: HttpErrorResponse) => {
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("error", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();        
      }
    });
  }

  private async validarFormularioReprocesar(): Promise<boolean> { 
    if (this.recaudacionMacmype.noAdju0001 === '') {
      this.utilService.onShowAlert("warning", "Atención", "Debe de adjuntar al menos 01 archivo de excel.");
      return false;  
    } else {
      return true;
    }
  }

  private asignarValoresReprocesar() {    
    this.recaudacionMacmype.idCncl = this.reprocesarRecaudacionMacmype.idCncl;
    this.recaudacionMacmype.idTipoCncl = 7;
    this.recaudacionMacmype.coZonaRegi = this.reprocesarRecaudacionMacmype.coZonaRegi;
    this.recaudacionMacmype.coOficRegi = this.reprocesarRecaudacionMacmype.coOficRegi;
    this.recaudacionMacmype.coLocaAten = this.reprocesarRecaudacionMacmype.coLocaAten;
    this.recaudacionMacmype.idOperPos = 1;       
    this.recaudacionMacmype.feCncl = (this.reprocesarRecaudacionMacmype.feCncl) ? this.funciones.convertirFechaDateToString(new Date(this.reprocesarRecaudacionMacmype.feCncl)) : '';
  }  

  public async cancelar() {
    let dataFiltrosRecaudacionMacmype: IDataFiltrosRecaudacionMacmype = await this.sharingInformationService.compartirDataFiltrosRecaudacionMacmypeObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionMacmype.esBusqueda) {
      dataFiltrosRecaudacionMacmype.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionMacmypeObservableData = dataFiltrosRecaudacionMacmype;
    }
    this.router.navigate(['SARF/procesos/consolidacion/recaudacion_macmype/bandeja']);
  }

  public obtenerServicioSeleccionado(file: any) {
    // console.log("servicio = " + servicio);
    //   this.servicioOperador = servicio;   
    (this.formRecaudacionMacmype.get('docs') as FormArray).push(
      this.formBuilder.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  public parse() {
    const docs = (this.formRecaudacionMacmype.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto ) {
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: '',
          coZonaRegi: this.formRecaudacionMacmype.get('zona').value,
          coOficRegi: this.formRecaudacionMacmype.get('oficina').value
        })
      }
    });

    this.saveAdjuntoInFileServer();
  }

  public saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.utilService.onShowAlert("error", "Atención", data.msgResult);
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            // if ( this.sizeFiles == 0 ) {
            //   if ( value == 'P') {
            //     this.enviarDataProcesar();
            //   }
            //   else {
            //     this.enviarDataReprocesar();
            //   }
            // }
          }
        },
        error: ( e ) => {
          console.log( e );
          this.utilService.onShowAlert("error", "Atención", e.error.description);
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirRecaudacionMacmypeObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();    
  }

}

