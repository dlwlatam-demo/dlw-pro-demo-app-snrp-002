import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, first, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MessageService } from 'primeng/api';
import { SortEvent } from 'primeng/api';
import { 
  IConciliacionManual, 
  IDataFiltrosRecaudacionPagaloPe, 
  IRecaudacionPagaloPe, 
  IConciliacionManual2, 
  ITotalesNiubizPagaloPe 
} from 'src/app/interfaces/consolidacion-pagalo-pe';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { RecaudacionPagaloPeService } from 'src/app/services/procesos/consolidacion/recaudacion-pagalo-pe.service';
import { UtilService } from 'src/app/services/util.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { environment } from 'src/environments/environment';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { DialogService } from 'primeng/dynamicdialog';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { GeneralService } from '../../../../../services/general.service';
@Component({
  selector: 'app-editar-recaudacion-pagalo-pe',
  templateUrl: './editar-recaudacion.component.html',
  styleUrls: ['./editar-recaudacion.component.scss'],
  providers: [ DialogService, MessageService ],
  encapsulation:ViewEncapsulation.None 
})
export class EditarRecaudacionPagaloPeComponent extends BaseBandeja implements OnInit, OnDestroy {
 
  private unsubscribe$ = new Subject<void>(); 
  public titulo: string;
  public recaudacionPagaloPe: IRecaudacionPagaloPe;
  public esConsultarRegistro: boolean = false;
  public fecha: string = '';
  public nombreArchivoExcel: string = '';
  public idGuidExcel: string = '';
  public servicioOperador: string = '';
  public mostrarBloque: boolean = true;
  private tiMoviVarios: string = 'M';
  private tiMoviObservaciones: string = 'O';
  private reqs: Observable<any>[] = [];
  public listaTotalesNiubizPagaloPe: ITotalesNiubizPagaloPe[] = [];
  public currentPage: string = environment.currentPage;
  public loadingTotalesNiubizPagaloPe: boolean = false;
  public onResetDivConciliar: boolean = false;

  public item: any;
 
  reporteExcel!: any;
  nFile!: any;
  
  constructor(
    private dialogService: DialogService,
    public funciones: Funciones,
    public recaudacionPagaloPeService: RecaudacionPagaloPeService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService ,
    private messageService: MessageService,
    private generalService: GeneralService
  ){ super() }

  async ngOnInit() {
    this.codBandeja = MenuOpciones.CD_Recaudacion_Pagalo_pe;
    this.btnConsolidacionPagaloPe();
    
    this.esConsultarRegistro = await this.sharingInformationService.compartirEsConsultarPagaloPeObservable.pipe(first()).toPromise()
    this.recaudacionPagaloPe = await this.sharingInformationService.compartirRecaudacionPagaloPeObservable.pipe(first()).toPromise();      
    this.titulo = (!this.esConsultarRegistro) ? 'Editar' : 'Consultar';
    this.fecha = this.funciones.convertirFechaDateToString(new Date (this.recaudacionPagaloPe.feCncl));
    this.servicioOperador = SERVICIO_DE_PAGO.NIUBIZ;
    this.nombreArchivoExcel = this.recaudacionPagaloPe.noAdju0001;
    this.idGuidExcel = this.recaudacionPagaloPe.idGuidDocu01;
  }


  public downloadFile(tipo: number) {
	switch(tipo) {
		case 0: this.nFile = "Recaudación_Pagalope_SFTP_NoConciliados";
		break;
		case 1: this.nFile = "Recaudación_Pagalope_SFTP_Conciliados";
		break;
		default: this.nFile = "Recaudación_Pagalope_SFTP_NoDefinido";
	}
	
	this.obtenerArchivoTxSftpBN(this.recaudacionPagaloPe.idCncl, tipo);
  }
  
  
  obtenerArchivoTxSftpBN(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionPagaloPeService.getRecaudacionTxSftpBN( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		this.exportExcel();
        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  
  public downloadFileTx(tipo: number) {
	switch(tipo) {
		case 1: this.nFile = "Recaudación_Pagalope_SPRL_Conciliados";
		break;
		case 0: this.nFile = "Recaudación_Pagalope_SPRL_NoConciliados";
		break;
		default: this.nFile = "Recaudación_Pagalope_SPRL_NoDefinido";
	}
	this.obtenerArchivoSPRL(this.recaudacionPagaloPe.idCncl, tipo);
  }
  
  

  obtenerArchivoSPRL(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionPagaloPeService.getTransaccionesSPRL( idCncl, inCncl ).subscribe({
      next: ( data ) => {
	  
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		this.exportExcel();

        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  
  public handleChange(e: any) {  
      this.servicioOperador = SERVICIO_DE_PAGO.PAGALO_PE;
      this.nombreArchivoExcel = this.recaudacionPagaloPe.noAdju0001;
      this.idGuidExcel = this.recaudacionPagaloPe.idGuidDocu01;
      this.mostrarBloque = true;
  }

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  public async cancelar() {
    let dataFiltrosRecaudacionPagaloPe: IDataFiltrosRecaudacionPagaloPe = await this.sharingInformationService.compartirDataFiltrosRecaudacionPagaloPeObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionPagaloPe.esBusqueda) {
      dataFiltrosRecaudacionPagaloPe.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionPagaloPeObservableData = dataFiltrosRecaudacionPagaloPe;
    }
    this.router.navigate(['SARF/procesos/consolidacion/recaudacion_pagalo_pe/bandeja']);
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirEsConsultarPagaloPeObservableData = false;
    this.sharingInformationService.compartirRecaudacionPagaloPeObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();      
  }


  exportExcel() {
    console.log('Exportar Excel');

    const doc = this.reporteExcel;
	const nFile = this.nFile;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', nFile);
    document.body.appendChild( download );
    download.click();
  }


}
