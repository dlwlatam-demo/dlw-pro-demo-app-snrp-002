import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, first, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MessageService } from 'primeng/api';
import { SortEvent } from 'primeng/api';
import { 
  IConciliacionManual, 
  IDataFiltrosRecaudacionAppQrSid, 
  IRecaudacionAppQrSid, 
  IConciliacionManual2, 
  ITotalesNiubizAppQrSid ,
  IVentasConciliadas
} from 'src/app/interfaces/consolidacion-app-qr-sid';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { RecaudacionAppQrSidService } from 'src/app/services/procesos/consolidacion/recaudacion-app-qr-sid.service';
import { UtilService } from 'src/app/services/util.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { environment } from 'src/environments/environment';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { DialogService } from 'primeng/dynamicdialog';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { GeneralService } from '../../../../../services/general.service';
@Component({
  selector: 'app-editar-recaudacion',
  templateUrl: './editar-recaudacion.component.html',
  styleUrls: ['./editar-recaudacion.component.scss'],
  providers: [ DialogService, MessageService ],
  encapsulation:ViewEncapsulation.None 
})
export class EditarRecaudacionAppQrSidComponent extends BaseBandeja implements OnInit, OnDestroy {
 
  private unsubscribe$ = new Subject<void>(); 
  public titulo: string;
  public recaudacionAppQrSid: IRecaudacionAppQrSid;
  public esConsultarRegistro: boolean = false;
  public fecha: string = '';
  public nombreArchivoExcel: string = '';
  public idGuidExcel: string = '';
  public servicioOperador: string = '';
  public mostrarBloque: boolean = true;
  private tiMoviVarios: string = 'M';
  private tiMoviObservaciones: string = 'O';
  private reqs: Observable<any>[] = [];
  private reqs1: Observable<any>[] = [];
  private reqs2: Observable<any>[] = [];
  public listaTotalesNiubizAppQrSid: ITotalesNiubizAppQrSid[] = [];
  public listaVentasConciliadas: IVentasConciliadas[] = [];
  public listaVentasNoConciliadas: IVentasConciliadas[] = [];
  public currentPage: string = environment.currentPage;
  public loadingTotalesNiubizAppQrSid: boolean = false;
  public loadingVentasConciliadas: boolean = false;
  public loadingVentasNoConciliadas: boolean = false;
  public onResetDivConciliar: boolean = false;

  public item: any;
 
  reporteExcel!: any;
  nFile!: any;
    
	
  tipoApp: boolean = false;
  tipoQr: boolean = false;
  tipoSid: boolean = false;
  tipoConciliacion: string = '';
    
  constructor(
    private dialogService: DialogService,
    public funciones: Funciones,
    public recaudacionAppQrSidService: RecaudacionAppQrSidService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService ,
    private messageService: MessageService,
    private generalService: GeneralService
  ){ super() }

  async ngOnInit() {
    this.codBandeja = MenuOpciones.CD_Recaudacion_APP_SUNARP_QR_SID;
    this.btnConsolidacionAPPSunarp();
    
    this.esConsultarRegistro = await this.sharingInformationService.compartirEsConsultarAppQrSidObservable.pipe(first()).toPromise()
    this.recaudacionAppQrSid = await this.sharingInformationService.compartirRecaudacionAppQrSidObservable.pipe(first()).toPromise();      
    console.log( this.recaudacionAppQrSid );
    this.titulo = (!this.esConsultarRegistro) ? 'Editar' : 'Consultar';
    this.fecha = this.funciones.convertirFechaDateToString(new Date (this.recaudacionAppQrSid.feCncl));
    this.servicioOperador = this.recaudacionAppQrSid.noOperPos;
    this.nombreArchivoExcel = this.recaudacionAppQrSid.noAdju0001;
    this.idGuidExcel = this.recaudacionAppQrSid.idGuidDocu01;

	switch(this.recaudacionAppQrSid.idTipoCncl) {
		case 2: 
				this.tipoConciliacion = 'APP SUNARP';
				this.tipoApp = true;
				break;
		case 3: 
				this.tipoConciliacion = 'CÓDIGO QR';
				this.tipoQr = true;
				break;
		case 4: 
				this.tipoConciliacion = 'SID';
				this.tipoSid = true;
				break;
		default:
				this.tipoConciliacion = '';
	}
    this.obtenerListados();  
  }

  private obtenerListados() { 
    this.utilService.onShowProcessLoading("Cargando la data");
    this.reqs.push(
      this.recaudacionAppQrSidService.getOperacionesTotalNiubiz(this.recaudacionAppQrSid.idCncl).pipe(
        catchError((err) => {        
          if (err.error.category === "NOT_FOUND") {
            return of([])        
          } else {
            return of(undefined)
          } 
        })          
      )
    );
    forkJoin(this.reqs).pipe(takeUntil(this.unsubscribe$)).subscribe((result: any[]) => {     
      this.listaTotalesNiubizAppQrSid = result[0]; 
      this.utilService.onCloseLoading();
    });
	
	
	
  }

  public downloadFilePOS(tipo: number) {
    let tipoCon: string;

    if ( this.tipoApp ) {
      tipoCon = 'APP'
    }
    else if ( this.tipoQr ) {
      tipoCon = 'QR'
    }
    else if ( this.tipoSid ) {
      tipoCon = 'SID'
    }

    switch(tipo) {
      case 0: this.nFile = `Recaudación_${ tipoCon }_POS_NoConciliados`;
      break;
      case 1: this.nFile = `Recaudación_${ tipoCon }_POS_Conciliados`;
      break;
      case 2: this.nFile = `Recaudación_${ tipoCon }_POS_Anulaciones`;
      break;
      case 3: this.nFile = `Recaudación_${ tipoCon }_POS_Contracargos`;
      break;
      default: this.nFile = `Recaudación_${ tipoCon }_POS_NoDefinido`;
    }
	
	  this.obtenerArchivoNiubiz(this.recaudacionAppQrSid.idCncl, tipo);
  }
  
  
  obtenerArchivoNiubiz(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionAppQrSidService.getRecaudacionNiubiz( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        let data1: any = data;		
        this.reporteExcel = data1.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		    this.exportExcel();
        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      },
      complete: () => {
        Swal.close();
      }
    });
  }
  
  
  public downloadFileTx(tipo: number) {
    let tipoConc = '';
    if(this.tipoApp) {
      tipoConc = 'APP';
    }
    if(this.tipoQr) {
      tipoConc = 'QR';
    }
    if(this.tipoSid) {
      tipoConc = 'SID';
    }
	
    switch(tipo) {
      case 0: this.nFile = "Recaudación_" + tipoConc + "_SPRL_NoConciliados";
      break;
      case 1: this.nFile = "Recaudación_" + tipoConc + "_SPRL_Conciliados";
      break;
      default: this.nFile = "Recaudación_" + tipoConc + "_SPRL_NoDefinido";
    }
    this.obtenerArchivoTx(this.recaudacionAppQrSid.idCncl, tipo);
  }

  obtenerArchivoTx(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
	
	if(this.tipoApp) {
		
		this.recaudacionAppQrSidService.getVentasConciliadasAPP( idCncl, inCncl ).subscribe({
		  next: ( data ) => {
		  
			this.reporteExcel = data.body.reporteExcel;

			this.messageService.add({
			  severity: 'success', 
			  summary: 'Reporte generado exitósamente', 
			  detail: 'Puede proceder con la descarga de su reporte.'
			});
			
			this.exportExcel();

			Swal.close();
			//this.disableButton = false;
		  },
		  error: ( e ) => {
			console.log( e );
			Swal.close();

			if ( e.error.category == 'NOT_FOUND' ) {
			  this.messageService.add({
				severity: 'info', 
				summary: e.error.description,
				detail: 'Por favor, modifique los parámetros seleccionados.'
			  });

			  return;
			}

			this.messageService.add({
			  severity: 'error', 
			  summary: 'Falla en generar el Reporte', 
			  detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
			});
		  }
		});
		
	}
	else if(this.tipoQr) {
	
		this.recaudacionAppQrSidService.getVentasConciliadasQR( idCncl, inCncl ).subscribe({
		  next: ( data ) => {
		  
			this.reporteExcel = data.body.reporteExcel;

			this.messageService.add({
			  severity: 'success', 
			  summary: 'Reporte generado exitósamente', 
			  detail: 'Puede proceder con la descarga de su reporte.'
			});
			
			this.exportExcel();

			Swal.close();
			//this.disableButton = false;
		  },
		  error: ( e ) => {
			console.log( e );
			Swal.close();

			if ( e.error.category == 'NOT_FOUND' ) {
			  this.messageService.add({
				severity: 'info', 
				summary: e.error.description,
				detail: 'Por favor, modifique los parámetros seleccionados.'
			  });

			  return;
			}

			this.messageService.add({
			  severity: 'error', 
			  summary: 'Falla en generar el Reporte', 
			  detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
			});
		  }
		});
		
	}
	else if(this.tipoSid) {
	
		this.recaudacionAppQrSidService.getVentasConciliadasSID( idCncl, inCncl ).subscribe({
		  next: ( data ) => {
		  
			this.reporteExcel = data.body.reporteExcel;

			this.messageService.add({
			  severity: 'success', 
			  summary: 'Reporte generado exitósamente', 
			  detail: 'Puede proceder con la descarga de su reporte.'
			});
			
			this.exportExcel();

			Swal.close();
			//this.disableButton = false;
		  },
		  error: ( e ) => {
			console.log( e );
			Swal.close();

			if ( e.error.category == 'NOT_FOUND' ) {
			  this.messageService.add({
				severity: 'info', 
				summary: e.error.description,
				detail: 'Por favor, modifique los parámetros seleccionados.'
			  });

			  return;
			}

			this.messageService.add({
			  severity: 'error', 
			  summary: 'Falla en generar el Reporte', 
			  detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
			});
		  }
		});
		
	}
  }
  
  
  public handleChange(e: any) {  
    if (e.index === 0) {
      this.servicioOperador = SERVICIO_DE_PAGO.NIUBIZ;
      this.nombreArchivoExcel = this.recaudacionAppQrSid.noAdju0001;
      this.idGuidExcel = this.recaudacionAppQrSid.idGuidDocu01;
      this.mostrarBloque = true;
    } else {
      this.mostrarBloque = false;
    }
  }

  numberTransform( value: string ): string {
    return Number( value ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  public async cancelar() {
    let dataFiltrosRecaudacionAppQrSid: IDataFiltrosRecaudacionAppQrSid = await this.sharingInformationService.compartirDataFiltrosRecaudacionAppQrSidObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionAppQrSid.esBusqueda) {
      dataFiltrosRecaudacionAppQrSid.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionAppQrSidObservableData = dataFiltrosRecaudacionAppQrSid;
    }
    this.router.navigate(['SARF/procesos/consolidacion/recaudacion_app_qr_sid/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirEsConsultarAppQrSidObservableData = false;
    this.sharingInformationService.compartirRecaudacionAppQrSidObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();      
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

  exportExcel() {
    console.log('Exportar Excel');

    const doc = this.reporteExcel;
	const nFile = this.nFile;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', nFile);
    document.body.appendChild( download );
    download.click();
  }


}
