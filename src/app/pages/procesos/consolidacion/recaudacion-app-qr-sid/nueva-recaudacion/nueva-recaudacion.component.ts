import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { forkJoin, Subject } from 'rxjs';
import { concatMap, filter, first, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Dropdown } from 'primeng/dropdown';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { GeneralService } from 'src/app/services/general.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { IDataFileExcel, IDataFiltrosRecaudacionAppQrSid, IRecaudacionAppQrSid } from 'src/app/interfaces/consolidacion-app-qr-sid';
import { RecaudacionAppQrSid } from 'src/app/models/procesos/consolidacion/recaudacion-app-qr-sid.model';
import { RecaudacionAppQrSidService } from 'src/app/services/procesos/consolidacion/recaudacion-app-qr-sid.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { UtilService } from 'src/app/services/util.service';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { IResponse, IResponse2 } from 'src/app/interfaces/general.interface';
import { ArchivoAdjunto } from '../../../../../models/archivo-adjunto.model';
import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';

@Component({
  selector: 'app-nueva-recaudacion',
  templateUrl: './nueva-recaudacion.component.html',
  styleUrls: ['./nueva-recaudacion.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class NuevaRecaudacionAppQrSidComponent implements OnInit, OnDestroy  {
  
  formRecaudacionAppQrSid: FormGroup;
  private unsubscribe$ = new Subject<void>();
  public titulo: string;
  public listaZonaRegistral: ZonaRegistral[];
  public listaOficinaRegistral: OficinaRegistral[];
  public listaLocal: Local[]; 
  public fechaMaxima: Date;
  private userCode: any;
  public loadingOficinaRegistral: boolean = false;
  public loadingLocal: boolean = false;
  public SERVICIO_NIUBIZ = SERVICIO_DE_PAGO.NIUBIZ;
  public nameNiubiz: string = '';
  public servicioOperador: string = '';
  public tipoConciliacion: string = '';
  public files: ArchivoAdjunto[] = [];
  public sizeFiles: number = 0;
  private recaudacionAppQrSid: RecaudacionAppQrSid;
  public reprocesarRecaudacionAppQrSid: IRecaudacionAppQrSid; 
  listaOperador: IResponse2[];
  listaTipoConciliacion: IResponse2[];

  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;

  constructor(    
    private formBuilder: FormBuilder,
    private generalService: GeneralService,
    private router: Router,
    private recaudacionAppQrSidService: RecaudacionAppQrSidService,
    private funciones: Funciones,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService
  ) {   
  }

  async ngOnInit() {
    this.fechaMaxima = new Date();
    this.listaTipoConciliacion = [];
    this.listaOperador = [];
    this.construirFormulario();
    this.reprocesarRecaudacionAppQrSid = await this.sharingInformationService.compartirRecaudacionAppQrSidObservable.pipe(first()).toPromise();   
    console.log('repro', this.reprocesarRecaudacionAppQrSid )
    if (!this.reprocesarRecaudacionAppQrSid) {
      this.titulo = 'Nuevo';
      this.userCode = localStorage.getItem('user_code') || ''; 
      this.inicilializarListas();
	    this.obtenerListados() ;
      this.obtenerZonasRegistrales();
    } else {
      this.titulo = 'Reprocesar';

      switch ( this.reprocesarRecaudacionAppQrSid.idTipoCncl ) {
        case 2:
          this.tipoConciliacion = 'APP SUNARP';
		  this.SERVICIO_NIUBIZ = SERVICIO_DE_PAGO.NIUBIZ;
           break;
        case 3:
          this.tipoConciliacion = 'CÓDIGO QR';
		  this.SERVICIO_NIUBIZ = SERVICIO_DE_PAGO.NIUBIZ_APP_QR_SID;
           break;
        case 4:
          this.tipoConciliacion = 'SID';
		  this.SERVICIO_NIUBIZ = SERVICIO_DE_PAGO.NIUBIZ_APP_QR_SID;
           break;
        default:
          this.tipoConciliacion = '';
      }
	  
    }

    this.recaudacionAppQrSid = new RecaudacionAppQrSid();        
  }

  private construirFormulario() {
    this.formRecaudacionAppQrSid = this.formBuilder.group({
      zona: [''],
      oficina: [''],
      local: [''],        
      tipoConciliacion: [''],
      operador: [''],
      fecha: [this.fechaMaxima],
      docs: this.formBuilder.array([])            
    });
  }

  private inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaTipoConciliacion = [];
    this.listaOperador = [];
    this.listaLocal = [];     
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    this.listaTipoConciliacion.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' }); 
    this.listaOperador.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' }); 
  }

  private obtenerZonasRegistrales() {
    this.utilService.onShowProcessLoading("Cargando la data");
    this.generalService.getCbo_Zonas_Usuario( this.userCode).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (res: ZonaRegistral[]) => {  
      if (res.length === 1) {   
        this.listaZonaRegistral.push({ coZonaRegi: res[0].coZonaRegi, deZonaRegi: res[0].deZonaRegi });    
        this.formRecaudacionAppQrSid.patchValue({ "zona": res[0].coZonaRegi});
        this.buscarOficinaRegistral();
      } else {    
        this.listaZonaRegistral.push(...res);
      }       
      this.utilService.onCloseLoading();
    }, (err: HttpErrorResponse) => {   
      this.utilService.onCloseLoading();                                  
    });
  }

  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_OperadoresAppQrSid()   ,
      this.generalService.getCbo_TipoConciliacion()   
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resOperador, resTipoConciliacion]) => { 
      if ( resOperador.length === 1 ) {
        this.listaOperador.push({ ccodigoHijo: resOperador[0].ccodigoHijo, cdescri: resOperador[0].cdescri });
        this.formRecaudacionAppQrSid.patchValue({ 'operador': resOperador[0].ccodigoHijo });
      } else {
        this.listaOperador.push(...resOperador);
      }

      if ( resTipoConciliacion.length === 1 ) {
        this.listaTipoConciliacion.push({ ccodigoHijo: resTipoConciliacion[0].ccodigoHijo, cdescri: resTipoConciliacion[0].cdescri });
        this.formRecaudacionAppQrSid.patchValue({ 'tipoConciliacion': resTipoConciliacion[0].ccodigoHijo });
      } else {
        this.listaTipoConciliacion.push(...resTipoConciliacion);
		this.formRecaudacionAppQrSid.patchValue({ 'tipoConciliacion': resTipoConciliacion[0].ccodigoHijo });
      }
	  
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }
  public buscarOficinaRegistral() { 
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formRecaudacionAppQrSid.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.formRecaudacionAppQrSid.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (res: OficinaRegistral[]) => {  
        if (res.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: res[0].coOficRegi, deOficRegi: res[0].deOficRegi });    
          this.formRecaudacionAppQrSid.patchValue({ "oficina": res[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...res);
        }
        this.loadingOficinaRegistral = false;     
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formRecaudacionAppQrSid.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.formRecaudacionAppQrSid.get('zona')?.value,this.formRecaudacionAppQrSid.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formRecaudacionAppQrSid.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err: HttpErrorResponse) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  public obtenerDataFileExcel(event: IDataFileExcel) {
    if (event.servicioDePago === this.SERVICIO_NIUBIZ) {
      this.nameNiubiz = event.nameFile;
      this.recaudacionAppQrSid.noAdju0001 = event.nameFile;
      this.recaudacionAppQrSid.idGuidDocu01 = event.idGuid;
      this.recaudacionAppQrSid.trama01 = event.lista;
	  
    } 
  }

  public changeTipoConciliacion(event: any) {
  	let registro = this.formRecaudacionAppQrSid.getRawValue();	
	if(registro.tipoConciliacion === '2') {
		this.SERVICIO_NIUBIZ = SERVICIO_DE_PAGO.NIUBIZ;
	} else if(registro.tipoConciliacion === '3') {
		this.SERVICIO_NIUBIZ = SERVICIO_DE_PAGO.NIUBIZ_APP_QR_SID;
	} else if(registro.tipoConciliacion === '4') {
		this.SERVICIO_NIUBIZ = SERVICIO_DE_PAGO.NIUBIZ_APP_QR_SID;
	}
  }
  
  public async procesar() {
    let resultado: boolean = await this.validarFormularioNuevo();
    if (!resultado) {      
      return;
    }     
    this.utilService.onShowProcessSaveEdit(); 
    this.asignarValoresNuevo();
    this.recaudacionAppQrSid.tiProc = '0'; // '0': es validar data, '1': es insertar
	let registro = this.formRecaudacionAppQrSid.getRawValue();
	
	this.recaudacionAppQrSid.idTipoCncl = registro.tipoConciliacion;

    this.recaudacionAppQrSidService.procesarRecaudacionAppQrSid(this.recaudacionAppQrSid).pipe(      
      takeUntil(this.unsubscribe$), 
      filter((res1: IResponse) => { 
        if (res1.codResult < 0 ) {        
          this.utilService.onShowAlert("warning", "Atención", res1.msgResult);        
          return false;
        } else {          
          return true 
        } 
      }),     
      concatMap((res1: IResponse) => {   
        this.recaudacionAppQrSid.tiProc = '1'; // '0': es validar data, '1': es insertar     
        return this.recaudacionAppQrSidService.procesarRecaudacionAppQrSid(this.recaudacionAppQrSid) 
      })
    ).subscribe((res: IResponse) => {     
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {  
        this.parse();       
        this.respuestaExitosaGrabar('PROCESÓ');
      }         
    }, (err: HttpErrorResponse) => {
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("error", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();        
      }
    });
  }

  private async respuestaExitosaGrabar(texto: string) {    
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Se ${texto} satisfactoriamente el/los registro(s)`);
    if (respuesta.isConfirmed) {
      let dataFiltrosRecaudacionAppQrSid: IDataFiltrosRecaudacionAppQrSid = await this.sharingInformationService.compartirDataFiltrosRecaudacionAppQrSidObservable.pipe(first()).toPromise(); 
      if (dataFiltrosRecaudacionAppQrSid.esBusqueda) {
        dataFiltrosRecaudacionAppQrSid.esCancelar = true;
        this.sharingInformationService.compartirDataFiltrosRecaudacionAppQrSidObservableData = dataFiltrosRecaudacionAppQrSid;
      }      
      this.router.navigate(['SARF/procesos/consolidacion/recaudacion_app_qr_sid/bandeja']);
    }
  }

  private async validarFormularioNuevo(): Promise<boolean> { 
    let registro = this.formRecaudacionAppQrSid.getRawValue();
    if (!registro.zona || registro.zona === "*") {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una zona registral.");
      return false;      
    } else if (!registro.oficina || registro.oficina === "*") {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una oficina registral.");
      return false;      
    } else if (!registro.local || registro.local === "*") {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un local.");
      return false;      
    } else if (!registro.fecha) {
      let respuesta = await this.utilService.onShowAlertPromise("warning", "Atención", `Seleccione la fecha de emisión.`);
      if (respuesta.isConfirmed) {
        document.getElementById('idFecha').focus();
        return false; 
      } else {
        return true;
      }    
    } else if (this.recaudacionAppQrSid.noAdju0001 === '') {
      this.utilService.onShowAlert("warning", "Atención", "Debe de adjuntar al menos 01 archivo de excel.");
      return false;  
    } else {
      return true;
    }
  }

  private asignarValoresNuevo() {    
    this.recaudacionAppQrSid.idCncl = 0;
    this.recaudacionAppQrSid.idTipoCncl = 5;
    this.recaudacionAppQrSid.coZonaRegi = this.formRecaudacionAppQrSid.get('zona').value;
    this.recaudacionAppQrSid.coOficRegi = this.formRecaudacionAppQrSid.get('oficina').value;
    this.recaudacionAppQrSid.coLocaAten = this.formRecaudacionAppQrSid.get('local').value;
    this.recaudacionAppQrSid.idOperPos = 1;    
    this.recaudacionAppQrSid.feCncl = this.funciones.convertirFechaDateToString(this.formRecaudacionAppQrSid.get('fecha').value);
  }

  public async reprocesar() {
    let resultado: boolean = await this.validarFormularioReprocesar();
    if (!resultado) {      
      return;
    }     
    this.utilService.onShowProcessSaveEdit(); 
    this.asignarValoresReprocesar();
    this.recaudacionAppQrSid.tiProc = '0'; // '0': es validar data, '1': es insertar
    this.recaudacionAppQrSidService.reprocesaRecaudacionAppQrSid(this.recaudacionAppQrSid).pipe(      
      takeUntil(this.unsubscribe$), 
      filter((res1: IResponse) => { 
        if (res1.codResult < 0 ) {        
          this.utilService.onShowAlert("warning", "Atención", res1.msgResult);        
          return false;
        } else {          
          return true 
        } 
      }),     
      concatMap((res1: IResponse) => {   
        this.recaudacionAppQrSid.tiProc = '1'; // '0': es validar data, '1': es insertar     
        return this.recaudacionAppQrSidService.reprocesaRecaudacionAppQrSid(this.recaudacionAppQrSid) 
      })
    ).subscribe((res: IResponse) => {     
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {   
        this.parse();      
        this.respuestaExitosaGrabar('REPROCESÓ');

      }         
    }, (err: HttpErrorResponse) => {
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("error", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();        
      }
    });
  }

  private async validarFormularioReprocesar(): Promise<boolean> { 
    if (this.recaudacionAppQrSid.noAdju0001 === '') {
      this.utilService.onShowAlert("warning", "Atención", "Debe de adjuntar al menos 01 archivo de excel.");
      return false;  
    } else {
      return true;
    }
  }

  private asignarValoresReprocesar() {    
    this.recaudacionAppQrSid.idCncl = this.reprocesarRecaudacionAppQrSid.idCncl;
    this.recaudacionAppQrSid.idTipoCncl = this.reprocesarRecaudacionAppQrSid.idTipoCncl;
    this.recaudacionAppQrSid.coZonaRegi = this.reprocesarRecaudacionAppQrSid.coZonaRegi;
    this.recaudacionAppQrSid.coOficRegi = this.reprocesarRecaudacionAppQrSid.coOficRegi;
    this.recaudacionAppQrSid.coLocaAten = this.reprocesarRecaudacionAppQrSid.coLocaAten;
    this.recaudacionAppQrSid.idOperPos = 1;       
    this.recaudacionAppQrSid.feCncl = (this.reprocesarRecaudacionAppQrSid.feCncl) ? this.funciones.convertirFechaDateToString(new Date(this.reprocesarRecaudacionAppQrSid.feCncl)) : '';
  }  

  public async cancelar() {
    let dataFiltrosRecaudacionAppQrSid: IDataFiltrosRecaudacionAppQrSid = await this.sharingInformationService.compartirDataFiltrosRecaudacionAppQrSidObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionAppQrSid.esBusqueda) {
      dataFiltrosRecaudacionAppQrSid.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionAppQrSidObservableData = dataFiltrosRecaudacionAppQrSid;
    }
    this.router.navigate(['SARF/procesos/consolidacion/recaudacion_app_qr_sid/bandeja']);
  }

  public obtenerServicioSeleccionado(file: any) {
    // this.servicioOperador = servicio;  
    (this.formRecaudacionAppQrSid.get('docs') as FormArray).push(
      this.formBuilder.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    ) 
  }

  public parse() {
    const docs = (this.formRecaudacionAppQrSid.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto ) {
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.formRecaudacionAppQrSid.get('tipoConciliacion').value,
          coZonaRegi: this.formRecaudacionAppQrSid.get('zona').value,
          coOficRegi: this.formRecaudacionAppQrSid.get('oficina').value
        })
      }
    });

    this.saveAdjuntoInFileServer();
  }

  public saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.utilService.onShowAlert("error", "Atención", data.msgResult);
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            // if ( this.sizeFiles == 0 ) {
            //   if ( value == 'P') {
            //     this.enviarDataProcesar();
            //   }
            //   else {
            //     this.enviarDataReprocesar();
            //   }
            // }
          }
        },
        error: ( e ) => {
          console.log( e );
          this.utilService.onShowAlert("error", "Atención", e.error.description);
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirRecaudacionAppQrSidObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();    
  }

}

