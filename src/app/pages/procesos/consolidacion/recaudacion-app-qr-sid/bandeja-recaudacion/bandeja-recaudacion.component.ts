import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent } from 'primeng/api';
import { forkJoin, Subject } from 'rxjs';
import { concatMap, filter, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { UtilService } from 'src/app/services/util.service';
import { GeneralService } from 'src/app/services/general.service';
import { RecaudacionAppQrSidService } from 'src/app/services/procesos/consolidacion/recaudacion-app-qr-sid.service';
import { FiltroRecaudacionAppQrSid, RecaudacionAppQrSid2 } from 'src/app/models/procesos/consolidacion/recaudacion-app-qr-sid.model';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { IDataFiltrosRecaudacionAppQrSid, IRecaudacionAppQrSid, IOperacionScunac, IPostOperacionScunac, IDetalleOperacionScunac } from 'src/app/interfaces/consolidacion-app-qr-sid';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { ESTADO_REGISTRO_2 } from 'src/app/models/enum/parameters';
import { IResponse, IResponse2 } from 'src/app/interfaces/general.interface';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';

@Component({
  selector: 'app-bandeja-recaudacion',
  templateUrl: './bandeja-recaudacion.component.html',
  styleUrls: ['./bandeja-recaudacion.component.scss'],
  providers: [
    DialogService  
  ]
})
export class BandejaRecaudAppQrSidComponent extends BaseBandeja implements OnInit, OnDestroy { 
  
  unsubscribe$ = new Subject<void>();
  userCode: any;
  currentPage: string = environment.currentPage;
  formRecaudacionAppQrSid: FormGroup;
  fechaMinima: Date;
  fechaMaxima: Date; 
  listaZonaRegistral: ZonaRegistral[];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaOperador: IResponse2[];
  listaTipoConciliacion: IResponse2[];
  listaEstado: IResponse2[];
  listaRecaudacion: IRecaudacionAppQrSid[];
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  selectedValuesRecaudacion: IRecaudacionAppQrSid[] = [];
  loadingRecaudacion: boolean = false;
  filtroRecaudacionAppQrSid: FiltroRecaudacionAppQrSid;
  estadoRegistro = ESTADO_REGISTRO_2;  

  //*************** listados para el filtro ***************/
  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaTipoConciliacionFiltro: IResponse2[]; 
  listaOperadorFiltro: IResponse2[]; 
  listaEstadoFiltro: IResponse2[];
  esBusqueda: boolean = false;  

  constructor( 
    private dialogService: DialogService,
    private formBuilder: FormBuilder,  
    private generalService: GeneralService,
    private funciones: Funciones, 
    private recaudacionAppQrSidService: RecaudacionAppQrSidService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService,    
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.CD_Recaudacion_APP_SUNARP_QR_SID;
    this.btnConsolidacionAPPSunarp();

    this.fechaMaxima = new Date();
    this.fechaMinima = new Date(this.fechaMaxima.getFullYear(), this.fechaMaxima.getMonth(), 1);  
    this.userCode = localStorage.getItem('user_code')||'';   
    this.construirFormulario();

    this.sharingInformationService.compartirDataFiltrosRecaudacionAppQrSidObservable.pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: IDataFiltrosRecaudacionAppQrSid) => {      
      if (data.esCancelar && data.bodyRecaudacionAppQrSid !== null) {    
        this.utilService.onShowProcessLoading("Cargando la data");
        this.setearCamposFiltro(data);
        this.buscarRecaudacion(false);
      } else {
        this.inicilializarListas();
        this.obtenerListados();
      }
    });
  }

  private construirFormulario() {
    this.formRecaudacionAppQrSid = this.formBuilder.group({
      zona: [''],
      oficina: [''],
      local: [''],
      tipoConciliacion: [''],  
      operador: [''],    
      estado: [''],   
      fechaDesde: [this.fechaMinima],
      fechaHasta: [this.fechaMaxima]              
    });
  }

  private setearCamposFiltro(dataFiltro: IDataFiltrosRecaudacionAppQrSid) {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoConciliacion = [];
    this.listaOperador = [];
    this.listaEstado = [];    
    this.listaZonaRegistral = dataFiltro.listaZonaRegistral; 
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaTipoConciliacion = dataFiltro.listaTipoConciliacion;
    this.listaOperador = dataFiltro.listaOperador;
    this.listaEstado = dataFiltro.listaEstado;    
    this.filtroRecaudacionAppQrSid = dataFiltro.bodyRecaudacionAppQrSid;
    this.formRecaudacionAppQrSid.patchValue({ "zona": this.filtroRecaudacionAppQrSid.coZonaRegi});
    this.formRecaudacionAppQrSid.patchValue({ "oficina": this.filtroRecaudacionAppQrSid.coOficRegi});
    this.formRecaudacionAppQrSid.patchValue({ "local": this.filtroRecaudacionAppQrSid.coLocaAten});
    this.formRecaudacionAppQrSid.patchValue({ "tipoConciliacion": this.filtroRecaudacionAppQrSid.idTipoCncl});
    this.formRecaudacionAppQrSid.patchValue({ "operador": this.filtroRecaudacionAppQrSid.idOperPos});
    this.formRecaudacionAppQrSid.patchValue({ "estado": this.filtroRecaudacionAppQrSid.esDocu});
    this.formRecaudacionAppQrSid.patchValue({ "fechaDesde": (this.filtroRecaudacionAppQrSid.feDesd !== '' && this.filtroRecaudacionAppQrSid.feDesd !== null) ? this.funciones.convertirFechaStringToDate(this.filtroRecaudacionAppQrSid.feDesd) : this.filtroRecaudacionAppQrSid.feDesd});
    this.formRecaudacionAppQrSid.patchValue({ "fechaHasta": (this.filtroRecaudacionAppQrSid.feHast !== '' && this.filtroRecaudacionAppQrSid.feHast !== null) ? this.funciones.convertirFechaStringToDate(this.filtroRecaudacionAppQrSid.feHast) : this.filtroRecaudacionAppQrSid.feHast});    
  }

  private inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoConciliacion = [];
    this.listaOperador = [];
    this.listaEstado = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' });
    this.listaOperador.push({ ccodigoHijo: '0', cdescri: '(TODOS)' });  
    this.listaEstado.push({ ccodigoHijo: '', cdescri: '(TODOS)' });   
  }

  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_Zonas_Usuario( this.userCode),     
      this.generalService.getCbo_EstadoRecaudacion(),
      this.generalService.getCbo_OperadoresAppQrSid()   ,
      this.generalService.getCbo_TipoConciliacion()   
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resZonaRegistral, resEstado, resOperador, resTipoConciliacion]) => { 
      if (resZonaRegistral.length === 1) {   
        this.listaZonaRegistral.push({ coZonaRegi: resZonaRegistral[0].coZonaRegi, deZonaRegi: resZonaRegistral[0].deZonaRegi });    
        this.formRecaudacionAppQrSid.patchValue({ "zona": resZonaRegistral[0].coZonaRegi});
        this.buscarOficinaRegistral();
      } else {    
        this.listaZonaRegistral.push(...resZonaRegistral);
      }

      if ( resOperador.length === 1 ) {
        this.listaOperador.push({ ccodigoHijo: resOperador[0].ccodigoHijo, cdescri: resOperador[0].cdescri });
        this.formRecaudacionAppQrSid.patchValue({ 'operador': resOperador[0].ccodigoHijo });
      } else {
        this.listaOperador.push(...resOperador);
      }

      if ( resTipoConciliacion.length === 1 ) {
        this.listaTipoConciliacion.push({ ccodigoHijo: resTipoConciliacion[0].ccodigoHijo, cdescri: resTipoConciliacion[0].cdescri });
        this.formRecaudacionAppQrSid.patchValue({ 'tipoConciliacion': resTipoConciliacion[0].ccodigoHijo });
      } else {
        this.listaTipoConciliacion.push(...resTipoConciliacion);
		this.formRecaudacionAppQrSid.patchValue({ 'tipoConciliacion': resTipoConciliacion[0].ccodigoHijo });
      }
	  
      this.listaEstado.push(...resEstado);        
      this.buscarRecaudacion(true);    
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }

  private establecerBodyRecaudacionAppQrSid() {       
    this.filtroRecaudacionAppQrSid = new FiltroRecaudacionAppQrSid();
    this.filtroRecaudacionAppQrSid.coZonaRegi = (this.formRecaudacionAppQrSid.get('zona')?.value === '*') ? '' : this.formRecaudacionAppQrSid.get('zona')?.value;    
    this.filtroRecaudacionAppQrSid.coOficRegi = (this.formRecaudacionAppQrSid.get('oficina')?.value === '*') ? '' : this.formRecaudacionAppQrSid.get('oficina')?.value;    
    this.filtroRecaudacionAppQrSid.coLocaAten = (this.formRecaudacionAppQrSid.get('local')?.value === '*') ? '' : this.formRecaudacionAppQrSid.get('local')?.value;        
    this.filtroRecaudacionAppQrSid.idTipoCncl = (this.formRecaudacionAppQrSid.get('tipoConciliacion')?.value === '0') ? 0 : this.formRecaudacionAppQrSid.get('tipoConciliacion')?.value;
    this.filtroRecaudacionAppQrSid.idOperPos = (this.formRecaudacionAppQrSid.get('operador')?.value === '0') ? 0 : this.formRecaudacionAppQrSid.get('operador')?.value;
    this.filtroRecaudacionAppQrSid.esDocu = (this.formRecaudacionAppQrSid.get('estado')?.value === '*') ? '' : this.formRecaudacionAppQrSid.get('estado')?.value;
    this.filtroRecaudacionAppQrSid.feDesd = (this.formRecaudacionAppQrSid.get('fechaDesde')?.value !== "" && this.formRecaudacionAppQrSid.get('fechaDesde')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formRecaudacionAppQrSid.get('fechaDesde')?.value) : ""; 
    this.filtroRecaudacionAppQrSid.feHast = (this.formRecaudacionAppQrSid.get('fechaHasta')?.value !== "" && this.formRecaudacionAppQrSid.get('fechaHasta')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formRecaudacionAppQrSid.get('fechaHasta')?.value): "";       
  }

  public buscarRecaudacion(esCargaInicial: boolean) {
    let resultado: boolean = this.validarCamporFiltros();
    if (!resultado) {
      return;
    }
    this.listaRecaudacion = [];
    this.selectedValuesRecaudacion = [];    
    this.loadingRecaudacion = (esCargaInicial) ? false : true;
    this.guardarListadosParaFiltro();     
    this.establecerBodyRecaudacionAppQrSid();
    this.esBusqueda = (esCargaInicial) ? false : true; 
    this.recaudacionAppQrSidService.getBandejaConciliacion(this.filtroRecaudacionAppQrSid).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (data: any) => {  
      this.listaRecaudacion = data.lista;
      this.loadingRecaudacion = false;
      this.utilService.onCloseLoading();       
    }, (err: HttpErrorResponse) => {   
      this.loadingRecaudacion = false;
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("warning", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();
      }                             
    });
  }

  private validarCamporFiltros(): boolean {
    if (
      (this.formRecaudacionAppQrSid.get('fechaDesde')?.value !== "" && this.formRecaudacionAppQrSid.get('fechaDesde')?.value !== null) &&
      (this.formRecaudacionAppQrSid.get('fechaHasta')?.value === "" || this.formRecaudacionAppQrSid.get('fechaHasta')?.value === null)
    ) {
      document.getElementById('idFechaHasta').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de fin.");
      return false; 
    } else if (
      (this.formRecaudacionAppQrSid.get('fechaDesde')?.value === "" || this.formRecaudacionAppQrSid.get('fechaDesde')?.value === null) &&
      (this.formRecaudacionAppQrSid.get('fechaHasta')?.value !== "" && this.formRecaudacionAppQrSid.get('fechaHasta')?.value !== null)
    ) {
      document.getElementById('idFechaDesde').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de inicio.");
      return false; 
    } else {
      return true;
    }   
  }

  public buscarOficinaRegistral() { 
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formRecaudacionAppQrSid.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.formRecaudacionAppQrSid.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.formRecaudacionAppQrSid.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formRecaudacionAppQrSid.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.formRecaudacionAppQrSid.get('zona')?.value,this.formRecaudacionAppQrSid.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formRecaudacionAppQrSid.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err: HttpErrorResponse) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  public cambioFechaInicio() {   
    if (this.formRecaudacionAppQrSid.get('fechaHasta')?.value !== '' && this.formRecaudacionAppQrSid.get('fechaHasta')?.value !== null) {
      if (this.formRecaudacionAppQrSid.get('fechaDesde')?.value > this.formRecaudacionAppQrSid.get('fechaHasta')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.formRecaudacionAppQrSid.controls['fechaDesde'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.formRecaudacionAppQrSid.get('fechaDesde')?.value !== '' && this.formRecaudacionAppQrSid.get('fechaDesde')?.value !== null) {
      if (this.formRecaudacionAppQrSid.get('fechaHasta')?.value < this.formRecaudacionAppQrSid.get('fechaDesde')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.formRecaudacionAppQrSid.controls['fechaHasta'].reset();    
      }      
    }
  }

  public nuevo() {  
    this.router.navigate(['SARF/procesos/consolidacion/recaudacion_app_qr_sid/nuevo']);
    this.sharingInformationService.irRutaBandejaRecaudacionAppQrSidObservableData = false;
  }

  public editar() {   
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      if (!(this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.REPROCESADO)) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede EDITAR un registro en estado ABIERTO o REPROCESADO");
      } else {
        this.sharingInformationService.compartirRecaudacionAppQrSidObservableData = this.selectedValuesRecaudacion[0]; 
        this.router.navigate(['SARF/procesos/consolidacion/recaudacion_app_qr_sid/editar']);
        this.sharingInformationService.irRutaBandejaRecaudacionAppQrSidObservableData = false;    
      }
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }             
  }

  public anular() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosAnulados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ANULAR el registro en estado ABIERTO o REPROCESADO.");
    } else {
      let textoAnular = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ANULAR ${textoAnular}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('anular');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyAnularRecaudacion = new RecaudacionAppQrSid2();
          bodyAnularRecaudacion.trama = lista;
          this.recaudacionAppQrSidService.anulaRecaudacionAppQrSid(bodyAnularRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosAnulados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.REPROCESADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public activar() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosActivados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ACTIVAR el registro en estado ANULADO.");
    } else {
      let textoActivar = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ACTIVAR ${textoActivar}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('activar');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyActivarrRecaudacion = new RecaudacionAppQrSid2();
          bodyActivarrRecaudacion.trama = lista;
          this.recaudacionAppQrSidService.activateRecaudacionAppQrSid(bodyActivarrRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosActivados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ANULADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public abrirConciliacion() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosAbiertos()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ABRIR el registro en estado CERRADO.");
    } else {
      let textoAbrir = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ABRIR ${textoAbrir}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('abrir');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyAbrirRecaudacion = new RecaudacionAppQrSid2();
          bodyAbrirRecaudacion.trama = lista;
          this.recaudacionAppQrSidService.openRecaudacionAppQrSid(bodyAbrirRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosAbiertos(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.CERRADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public cerrarConciliacion() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosCerrados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede CERRAR el registro en estado ABIERTO o REPROCESADO.");
    } else {
      let textoAbrir = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea CERRAR ${textoAbrir}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('cerrar');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyCerrarRecaudacion = new RecaudacionAppQrSid2();
          bodyCerrarRecaudacion.trama = lista;
          this.recaudacionAppQrSidService.closeRecaudacionAppQrSid(bodyCerrarRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {                 
              const listado = lista.map(item => this.operacionesScunac(parseInt(item)).catch((error) => { }));
              Promise.all(lista).then( respuesta => {             
                this.buscarRecaudacion(false);
                this.selectedValuesRecaudacion = [];          
              });
            }             
          }, (err: HttpErrorResponse) => {      
          });
        }
      });
    }
  }

  private operacionesScunac(idCncl: number) {
    return new Promise<any>(
      (resolve, reject) => {               
        this.recaudacionAppQrSidService.getOperacionesScunac(idCncl, 2)
          .pipe(            
            takeUntil(this.unsubscribe$),
            filter((data: any) => {           
              if (data.body.lstOperacionScunac.length > 0) {
                return true;
              } else {               
                return false;
              }
            }),
            concatMap((res: any) => {           
              let listaOperacionScunac: IOperacionScunac[] = res.body.lstOperacionScunac;      
              return this.recaudacionAppQrSidService.actualizarIdOperacionScunac(this.obtenerOperacionScunac(listaOperacionScunac))              
            })
          ).subscribe( (res: IResponse) => {
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
              reject(new Error("Error de ejecución . . . :( "));
            } else if (res.codResult === 0) { 
              resolve(idCncl);                     
            }                                                                     
          }, () => {
            reject(new Error("Error de ejecución . . . :( ")); 
          }
        ) 
      }
    );
  }

  private obtenerOperacionScunac(lista: IOperacionScunac[]): IPostOperacionScunac {
    let data: IPostOperacionScunac;
    let listaDetalle: IDetalleOperacionScunac[] = [];
    for (let i = 0; i < lista.length; i++) {
      let item: IDetalleOperacionScunac = {
        aaMvto: lista[i].anioMovimiento,
        coZonaRegi: lista[i].coZonaRegi,
        nuMvto: lista[i].numeroMovimiento,
        nuSecuDeta: lista[i].nuSecuDeta,
        idTransPos: lista[i].id        
      }
      listaDetalle.push(item);      
    }
    data = {
      trama: listaDetalle
    } 
    return data;
  }

  private validarRegistrosCerrados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.REPROCESADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public reProcesar() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      if (!(this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.REPROCESADO)) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede REPROCESAR un registro en estado ABIERTO o REPROCESADO");
      } else {
        this.sharingInformationService.compartirRecaudacionAppQrSidObservableData = this.selectedValuesRecaudacion[0]; 
        this.router.navigate(['SARF/procesos/consolidacion/recaudacion_app_qr_sid/reprocesar']);
        this.sharingInformationService.irRutaBandejaRecaudacionAppQrSidObservableData = false; 
      }           
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }
  }

  public consultarRegistro() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      this.sharingInformationService.compartirEsConsultarAppQrSidObservableData = true; 
      this.sharingInformationService.compartirRecaudacionAppQrSidObservableData = this.selectedValuesRecaudacion[0];
      this.router.navigate(['SARF/procesos/consolidacion/recaudacion_app_qr_sid/consultar']);
      this.sharingInformationService.irRutaBandejaRecaudacionAppQrSidObservableData = false;      
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }
  }

  customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;
        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
        return (event.order! * result);
    });
  }

  private guardarListadosParaFiltro() {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaTipoConciliacionFiltro = [...this.listaTipoConciliacion];
    this.listaOperadorFiltro = [...this.listaOperador];
    this.listaEstadoFiltro = [...this.listaEstado];    
  }

  private establecerDataFiltrosRecaudacionME() : IDataFiltrosRecaudacionAppQrSid {
    let dataFiltrosRecaudacionME: IDataFiltrosRecaudacionAppQrSid = {
      listaZonaRegistral: this.listaZonaRegistralFiltro, 
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaTipoConciliacion: this.listaTipoConciliacionFiltro,     
      listaOperador: this.listaOperadorFiltro,     
      listaEstado: this.listaEstadoFiltro,
      bodyRecaudacionAppQrSid: this.filtroRecaudacionAppQrSid,   
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosRecaudacionME;
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();   
    this.sharingInformationService.compartirDataFiltrosRecaudacionAppQrSidObservableData = this.establecerDataFiltrosRecaudacionME();                    
  }

}
