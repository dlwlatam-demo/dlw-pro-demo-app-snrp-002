import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent } from 'primeng/api';

import { Estado, Documento } from '../../../../../interfaces/combos.interface';

import { ConfigurarDocumento } from '../../../../../models/mantenimientos/configuracion/config-documento.model';

import { ConfigDocumentoService } from '../../../../../services/mantenimientos/configuracion/config-documento.service';
import { GeneralService } from '../../../../../services/general.service';

import { environment } from '../../../../../../environments/environment';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-bandeja-recaud',
  templateUrl: './bandeja-recaud.component.html',
  styleUrls: ['./bandeja-recaud.component.scss'],
  providers: [
    DialogService
  ]
})
export class BandejaRecaudComponent implements OnInit {

  $zonas!: Observable<any>;
  $oficinas!: Observable<any>;
  $locales!: Observable<any>;

  msg!: string;
  isLoading!: boolean;
  regActivo: string[] = [];
  regInactivo: string[] = [];
  
  documentos!: FormGroup;

  $tipoDocumento!: Documento[];
  $estado!: Estado[];
  
  enviadosList!: ConfigurarDocumento[];
  selectedValues!: ConfigurarDocumento[];

  currentPage: string = environment.currentPage;

  constructor( private fb: FormBuilder,
               private dialogService: DialogService,
               private generalService: GeneralService,
               private configuracionService: ConfigDocumentoService ) { }

  ngOnInit(): void {

    this.$zonas = this.generalService.getCbo_Zonas();
    this.$oficinas = this.generalService.getCbo_Oficinas();
    this.$locales = this.generalService.getCbo_Locales();

    let estadoItem: Estado = { deEstado: 'Todos', coEstado: '*' };

    this.$tipoDocumento = this.generalService.getCbo_TipoDocumento();;
    this.$estado = this.generalService.getCbo_Estado();
    this.$estado.push( estadoItem );

    this.documentos = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoDocu: ['', [ Validators.required ]],
      descripcion: ['', [ Validators.required ]],
      estado: ['', [ Validators.required ]]
    });

    this.buscaBandeja();
  }

  buscaBandeja() {
    let tipoDocu = this.documentos.get('tipoDocu')?.value;
    tipoDocu = tipoDocu == '' ? '1' : tipoDocu;

    const descripcion = this.documentos.get('descripcion')?.value;

    let estado = this.documentos.get('estado')?.value;
    if ( estado == '' ) {
      estado = 'A';
    } else if ( estado == '*' ) {
      estado = '';
    }

    this.buscarBandeja( tipoDocu, descripcion, estado );
  }

  buscarBandeja( tipo: string, des: string, est: string ) {
    Swal.showLoading();
    this.configuracionService.getBandejaConfiguracion( tipo, des, est ).subscribe({
      next: ( data ) => {
        console.log( data )

        for ( let d of data ) {
          if ( d.inRegi == 'A' )
            d.inRegi = 'Activo';
          else
            d.inRegi = 'Inactivo';
        }

        this.enviadosList = data;
      },
      error: () => {
        Swal.close()
        Swal.fire(
          'No se encuentran registros con los datos ingresados',
          'Configurar Documentos',
          'info'
        );

        this.enviadosList = [];
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  nuevaConfiguracion() {
    
  }

  editOrView( value: string ) {
    
  }

  verConcepto( conf: ConfigurarDocumento ) {
    
  }

  anular() {
    if ( !this.validar('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Inactivo' ) {
        Swal.fire('No puede anular un registro ya inactivo', '', 'warning');
        return;
      }

      this.regActivo.push( String( registro.idTipo ) );
    }
    
    Swal.fire({
      title: 'Configurar Documentos',
      text: '¿Está Ud. seguro que desea anular el registro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
    })
    .then( ( result ) => {
      if ( result.isConfirmed ) {
        Swal.showLoading();
        this.isLoading = true;

        let tipoDocu = this.documentos.get('tipoDocu')?.value;
        tipoDocu = tipoDocu == '' ? '1' : tipoDocu;

        const data = {
          tiDocu: tipoDocu,
          idTipoDocumentos: this.regActivo
        }

        this.configuracionService.anularDocumentos( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            Swal.fire('Registros anulados correctamente', '', 'success');
          },
          error: ( e ) => {
            console.log( e );
            Swal.fire('Error al anular los registros', '', 'error');
          },
          complete: () => {
            Swal.close();
            this.isLoading = false;
            this.regActivo = [];
            this.selectedValues = [];
            this.buscaBandeja();
          }
        });
      }
    });
  }

  activar() {
    if ( !this.validar('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Activo' ) {
        Swal.fire('No puede habilitar un registro ya activo', '', 'warning');
        return;
      }

      this.regInactivo.push( String( registro.idTipo ) );
    }

    Swal.fire({
      title: 'Configurar Documentos',
      text: `¿Está Ud. seguro que desea activar los registros?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
    })
    .then( ( result ) => {
      if ( result.isConfirmed ) {
        this.isLoading = true;

        let tipoDocu = this.documentos.get('tipoDocu')?.value;
        tipoDocu = tipoDocu == '' ? '1' : tipoDocu;

        const data = {
          tiDocu: tipoDocu,
          idTipoDocumentos: this.regInactivo
        }

        this.configuracionService.activarDocumentos( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            Swal.fire('Registros activados correctamente', '', 'success');
          },
          error: ( e ) => {
            console.log( e );
            Swal.fire('Error al activar los registros', '', 'error');
          },
          complete: () => {
            Swal.close();
            this.isLoading = false;
            this.regInactivo = [];
            this.selectedValues = [];
            this.buscaBandeja();
          }
        });
      }
    });
  }

  validar( valor: string ): boolean {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Debe seleccionar un registro para editarlo', '', 'error');
      this.selectedValues = [];
      return false;
    }

    if ( valor == 'e' )
      if ( this.selectedValues.length != 1 ) {
        Swal.fire( 'Solo puede editar un registro a la vez', '', 'error');
        this.selectedValues = [];
        return false;
      }
    else
      if ( this.selectedValues.length > environment.fileLimit ) {
        Swal.fire( 'No puede seleccionar más de 20 registros', '', 'error');
        this.selectedValues = [];
        return false;
      }

    return true;
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
