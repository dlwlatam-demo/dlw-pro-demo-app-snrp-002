import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ITotalPorOficina } from 'src/app/interfaces/consolidacion';
import { RecaudacionMediosElectronicosService } from 'src/app/services/procesos/consolidacion/recaudacion-medios-electronicos.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-total-por-oficina',
  templateUrl: './total-por-oficina.component.html',
  styleUrls: ['./total-por-oficina.component.scss']
})
export class TotalPorOficinaComponent implements OnInit {

  private unsubscribe$ = new Subject<void>();
  private idCncl: number; 
  public listaTotal: ITotalPorOficina[];
  public total: number = 0;
  public message: string = '';

  constructor(
    public config: DynamicDialogConfig,
    private recaudacionMediosElectronicosService: RecaudacionMediosElectronicosService,
    private utilService: UtilService
  ) { }

  ngOnInit(): void {
    this.idCncl = this.config.data.idCncl;
    this.obtenerListado();    
  }

  private obtenerListado() {
    this.utilService.onShowProcessLoading("Cargando la data");
    this.recaudacionMediosElectronicosService.totalPerOfficeRecaudacionME(this.idCncl).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (data: ITotalPorOficina[]) => {              
      this.listaTotal = data;
      this.total = this.calcularTotal();
      this.utilService.onCloseLoading();
    }, (err: HttpErrorResponse) => {      
      if (err.error.category === "NOT_FOUND") {
        this.listaTotal = [];    
        this.utilService.onCloseLoading();           
      } else {
        this.utilService.onShowMessageErrorSystem();
      }                             
    });
  }

  private calcularTotal(): number {
    let cantidad: number = 0;
    for (let i = 0; i < this.listaTotal.length; i++) {
      cantidad = cantidad + this.listaTotal[i].imSumaDepo;
    } 
    return cantidad;   
  }

}
