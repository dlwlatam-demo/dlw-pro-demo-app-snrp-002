import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { ConfirmationService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { GeneralService } from '../../../../../services/general.service';
import { RecaudMedElectService } from '../../../../../services/procesos/consolidacion/recaud-medios-electronicos.service';
import { Observable } from 'rxjs';

import { RecaudacionMediosElectronicosTrama01 } from '../../../../../models/procesos/consolidacion/recaud-medios-electronicos-trama01.model';
import { RecaudacionMediosElectronicosTrama02 } from '../../../../../models/procesos/consolidacion/recaud-medios-electronicos-trama02.model';
import { RecaudacionMediosElectronicosTrama03 } from '../../../../../models/procesos/consolidacion/recaud-medios-electronicos-trama03.model';
import { RecaudacionMediosElectronicos } from '../../../../../models/procesos/consolidacion/recaud-medios-electronicos.model';

import { ZonaRegistral, OficinaRegistral, Local } from '../../../../../interfaces/combos.interface';

import { NgxSpinnerService } from 'ngx-spinner';
import {formatDate} from '@angular/common'

@Component({
  selector: 'app-reprocesar-recaudacion',
  templateUrl: './reprocesar-recaudacion.component.html',
  styleUrls: ['./reprocesar-recaudacion.component.scss'],
  providers: [
    ConfirmationService
  ]
})
export class ReprocesarRecaudacionComponent implements OnInit {

  $zonasFinal: ZonaRegistral[]=[];
  $oficinasFinal: OficinaRegistral[]=[];
  $localesFinal: Local[]=[];

  coZonaRegiIni: string='';
  coOficRegiIni: string='';
  coLocaAtenIni: string='';

  coUsua: string;

  isLoadingProcesar!: boolean;
  isLoadingCancelar!: boolean;
  reprocesar!: FormGroup;

  dataNIUBIZ: [][] = [];
  dataDINERS: [][] = [];
  dataAMEX: [][] = [];
  nameNIUBIZ:string="";
  nameDINERS:string="";
  nameAMEX:string="";

  recaudacion!: RecaudacionMediosElectronicos[];
  messClose: string='reject';
  
  constructor( private fb: FormBuilder,
               private ref: DynamicDialogRef,
               private config: DynamicDialogConfig,
               private generalService: GeneralService,
               private recaudacionService: RecaudMedElectService,
               private spinner: NgxSpinnerService ) { 
                this.coUsua = localStorage.getItem("user_code")!;
               }

  ngOnInit(): void {
    console.log("this.config.data", this.config.data);

    this.recaudacion = this.config.data.recaudacion as RecaudacionMediosElectronicos[];

    this.reprocesar = this.defConfiguracionForm();

    let anio = Number(this.recaudacion['0'].feCncl?.substring(0,4));
    let mes = Number(this.recaudacion['0'].feCncl?.substring(5,7)) - 1;
    let dia = Number(this.recaudacion['0'].feCncl?.substring(8,10));
    const cDateH = new Date(anio, mes, dia);
    this.reprocesar.get('fecha')?.setValue( cDateH );

    this.$zonasFinal.splice(0);
    let zonaItem: ZonaRegistral = { coZonaRegi: this.recaudacion['0'].coZonaRegi, deRazSoc: this.recaudacion['0'].deZonaRegi, 
                  abZonaRegi: this.recaudacion['0'].deZonaRegi, deRuc: '', deZonaRegi: this.recaudacion['0'].deZonaRegi };
    this.$zonasFinal.push(zonaItem);
    this.coZonaRegiIni = this.recaudacion['0'].coZonaRegi!;
    this.reprocesar.get('zona')?.setValue( this.coZonaRegiIni );

    this.$oficinasFinal.splice(0);
    let oficinaItem: OficinaRegistral = { coZonaRegi: this.recaudacion['0'].coZonaRegi, 
      coOficRegi: this.recaudacion['0'].coOficRegi, deOficRegi: this.recaudacion['0'].deOficRegi };
    this.$oficinasFinal.push(oficinaItem);
    this.coOficRegiIni = this.recaudacion['0'].coOficRegi!;
    this.reprocesar.get('oficina')?.setValue( this.coOficRegiIni );

    this.$localesFinal.splice(0);
    let localItem: Local = { coZonaRegi: this.recaudacion['0'].coZonaRegi, 
          coOficRegi: this.recaudacion['0'].coOficRegi, coLocaAten: this.recaudacion['0'].coLocaAten, 
          deLocaAten: this.recaudacion['0'].deLocaAten, locaDefe: '' };
    this.$localesFinal.push(localItem);
    this.coLocaAtenIni = this.recaudacion['0'].coLocaAten!;
    this.reprocesar.get('local')?.setValue( this.coLocaAtenIni );
  }

  onChangeZona (zona: any){
  }

  onChangeOficina (oficina: any){
  }

  onChangeLocal (local: any){
    let localLoc: string;
    if (local == ''){
      localLoc = this.coOficRegiIni;
    } else {
      localLoc = local.value;
      this.coLocaAtenIni = localLoc;
    }
  }

  defConfiguracionForm() {
    return this.fb.group({
      zona: [{value: '', disabled: true}, [ Validators.required ]],
      oficina: [{value: '', disabled: true}, [ Validators.required ]],
      local: [{value: '', disabled: true}, [ Validators.required ]],
      fecha: [{value: '', disabled: true}, [ Validators.required ]]
    });
  }

  procesar() {
    let lzona = this.reprocesar.get('zona')?.value;
    let loficina = this.reprocesar.get('oficina')?.value;
    let llocal = this.reprocesar.get('local')?.value;
    let fecha = this.reprocesar.get('fecha')?.value;

    if (lzona == '*' || lzona == null || lzona == ''){
      Swal.fire( 'Seleccione la zona', '', 'error');
      return;
    }
    if (loficina == '*' || loficina == null || loficina == ''){
      Swal.fire( 'Seleccione la oficina', '', 'error');
      return;
    }
    if (llocal == '*' || llocal == null || llocal == ''){
      Swal.fire( 'Seleccione el local', '', 'error');
      return;
    }
    if(fecha == "" || fecha == null){
      Swal.fire('Debe ingresar la fecha', '', 'error');
      return;
    }/*
    if(this.nameNIUBIZ == "" || this.nameNIUBIZ == null){
      Swal.fire('Debe ingresar el archivo NIUBIZ', '', 'error');
      return;
    }
    if(this.nameDINERS == "" || this.nameDINERS == null){
      Swal.fire('Debe ingresar el archivo DINERS', '', 'error');
      return;
    }
    if(this.nameAMEX == "" || this.nameAMEX == null){
      Swal.fire('Debe ingresar el archivo AMEX', '', 'error');
      return;
    }*/

    Swal.fire({
      title: 'Recaudación Medios Electrónicos',
      text: '¿Está Ud. seguro que desea procesar la conciliación?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
    })
    .then( result => {
      if ( result.isConfirmed ) {
        if ( fecha != null && fecha != undefined && fecha != '') {
            /*let mesP = fecha.getMonth() + 1;
            if( mesP < 10 ) { mesP = '0' + mesP.toString() }
            const añoP = fecha.getFullYear();
            const diaP = fecha.getDate();        
            fecha = `${diaP}/${mesP}/${añoP}`;
            */
            fecha = formatDate(fecha,'dd/MM/yyyy','en-US');
        }
        // Trama 01
        let listTrama01: RecaudacionMediosElectronicosTrama01[]=[];
        this.dataNIUBIZ.forEach((e, index) => {
          if(index > 7){
            let trama01: RecaudacionMediosElectronicosTrama01[]=[];
            let dataTrama01: RecaudacionMediosElectronicosTrama01={};
            trama01 = e;
            dataTrama01.fila = String(index+1-8);
            dataTrama01.a = String(trama01[0]);
            dataTrama01.b = String(trama01[1]);
            dataTrama01.c = String(trama01[2]);
            dataTrama01.d = String(trama01[3]);
            dataTrama01.e = String(trama01[4]);
            dataTrama01.f = String(trama01[5]);
            dataTrama01.g = String(trama01[6]);
            dataTrama01.h = String(trama01[7]);
            dataTrama01.i = String(trama01[8]);
            dataTrama01.j = String(trama01[9]);
            dataTrama01.k = String(trama01[10]);
            dataTrama01.l = String(trama01[11]);
            dataTrama01.m = String(trama01[12]);
            dataTrama01.n = String(trama01[13]);
            dataTrama01.o = String(trama01[14]);
            dataTrama01.p = String(trama01[15]);
            dataTrama01.q = String(trama01[16]);
            dataTrama01.r = String(trama01[17]);
            dataTrama01.s = String(trama01[18]);
            dataTrama01.t = String(trama01[19]);
            dataTrama01.u = String(trama01[20]);
            dataTrama01.v = String(trama01[21]);
            dataTrama01.w = String(trama01[22]);
            dataTrama01.x = String(trama01[23]);
            dataTrama01.y = "";
            dataTrama01.z = "";
            listTrama01.push(dataTrama01);
          }
         }
        );
        // Trama 02
        let listTrama02: RecaudacionMediosElectronicosTrama02[]=[];
        this.dataDINERS.forEach((e, index) => {
          if(index > 2){
            let trama02: RecaudacionMediosElectronicosTrama02[]=[];
            let dataTrama02: RecaudacionMediosElectronicosTrama02={};
            trama02 = e;
            dataTrama02.fila = String(index+1-3);
            dataTrama02.a = String(trama02[0]);
            dataTrama02.b = String(trama02[1]);
            dataTrama02.c = String(trama02[2]);
            dataTrama02.d = String(trama02[3]);
            dataTrama02.e = String(trama02[4]);
            dataTrama02.f = String(trama02[5]);
            dataTrama02.g = String(trama02[6]);
            dataTrama02.h = String(trama02[7]);
            dataTrama02.i = String(trama02[8]);
            dataTrama02.j = String(trama02[9]);
            dataTrama02.k = String(trama02[10]);
            dataTrama02.l = String(trama02[11]);
            listTrama02.push(dataTrama02);
          }
         }
        );
        // Trama 03
        let listTrama03: RecaudacionMediosElectronicosTrama03[]=[];
        this.dataAMEX.forEach((e, index) => {
          if(index > 0){
            let trama03: RecaudacionMediosElectronicosTrama03[]=[];
            let dataTrama03: RecaudacionMediosElectronicosTrama03={};
            trama03 = e;
            dataTrama03.fila = String(index+1-1);
            dataTrama03.a = String(trama03[0]);
            dataTrama03.b = String(trama03[1]);
            dataTrama03.c = String(trama03[2]);
            dataTrama03.d = String(trama03[3]);
            dataTrama03.e = String(trama03[4]);
            dataTrama03.f = String(trama03[5]);
            dataTrama03.g = String(trama03[6]);
            dataTrama03.h = String(trama03[7]);
            dataTrama03.i = String(trama03[8]);
            dataTrama03.j = String(trama03[9]);
            dataTrama03.k = String(trama03[10]);
            dataTrama03.l = String(trama03[11]);
            dataTrama03.m = String(trama03[12]);
            dataTrama03.n = String(trama03[13]);
            if(dataTrama03.b.length > 0 && dataTrama03.b != undefined && dataTrama03.b != null && dataTrama03.b !="undefined"){
              listTrama03.push(dataTrama03);
            }
          }
         }
        );
        const data = {
          tiProc: "0",
          idCncl: this.recaudacion['0'].idCncl,
          idTipoCncl: 1,
          coZonaRegi: lzona,
          coOficRegi: loficina,
          coLocaAten: llocal,
          idOperPos: 1,
          feCncl: fecha,
          noAdju0001: this.nameNIUBIZ,
          noAdju0002: this.nameDINERS,
          noAdju0003: this.nameAMEX,
          trama01: listTrama01,
          trama02: listTrama02,
          trama03: listTrama03
        }
        console.log("datadatadata",data);
        this.isLoadingProcesar = true;
        this.isLoadingCancelar = true;
        this.reprocesar.disable();
        this.recaudacionService.createRecaudacion( data  ).subscribe({
          next: ( d ) => {
            this.isLoadingProcesar = false;
            this.isLoadingCancelar = false;
            if(d != null){
              if( d.codResult == 0){
                data.tiProc = "1";
                this.isLoadingProcesar = true;
                this.isLoadingCancelar = true;
                this.recaudacionService.createRecaudacion( data  ).subscribe({
                  next: ( d ) => {
                    if(d != null && d.codResult == 0){
                      Swal.fire( 'Recaudación Medios Electrónicos', 'Se realizó exitosamente el reproceso de conciliación', 'success' );
                      this.isLoadingProcesar = false;
                      this.isLoadingCancelar = false;
                    } else{
                      Swal.fire( 'Recaudación Medios Electrónicos', 'Ha ocurrido un error, no Se realizó el proceso de conciliación', 'error' );
                      this.isLoadingProcesar = false;
                      this.isLoadingCancelar = false;
                    }
                    this.messClose = 'accept';
                    //this.ref.close('accept');
                  },
                  error: ( e ) => {
                    this.isLoadingProcesar = false;
                    this.isLoadingCancelar = false;
                    Swal.fire( 'Error al grabar los datos', '', 'error' );
                  },
                  complete: () => {
                    this.isLoadingCancelar = false;
                    //this.ref.close('accept');
                  }
                });
              } else {
                Swal.fire( 'Recaudación Medios Electrónicos', d.msgResult, 'error' );  
                this.isLoadingProcesar = false;
                this.isLoadingCancelar = false;
              }
            } else{
              Swal.fire( 'Recaudación Medios Electrónicos', 'Los datos no pudieron ser validados', 'error' );
              this.isLoadingProcesar = false;
              this.isLoadingCancelar = false;
            }
            //this.ref.close();
          },
          error: ( e ) => {
            this.isLoadingProcesar = false;
            this.isLoadingCancelar = false;
            Swal.fire( 'Error al grabar los datos', '', 'error' );
          },
          complete: () => {
            this.isLoadingCancelar = false;
          }
        });
      }
    }); 
  }

  cancelar() {
    this.ref.close(this.messClose);
  }

  addUploadedFileNIUBIZ( file: any) {
    this.dataNIUBIZ = file;
  }
  deleteUploadedNIUBIZ() {
  }
  nameFileNIUBIZ(nameFile:string){
    this.nameNIUBIZ=nameFile;
  }

  addUploadedFileDINERS( file: any) {
    this.dataDINERS = file;
  }
  deleteUploadedDINERS() {
  }
  nameFileDINERS(nameFile:string){
    this.nameDINERS=nameFile;
  }

  addUploadedFileAMEX( file: any) {
    this.dataAMEX = file;
  }
  deleteUploadedAMEX() {
  }
  nameFileAMEX(nameFile:string){
    this.nameAMEX=nameFile;
  }



}
