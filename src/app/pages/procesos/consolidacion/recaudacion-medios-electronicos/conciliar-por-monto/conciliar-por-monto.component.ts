import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { SortEvent } from 'primeng/api';
import { IConciliacionPorMonto, IConciliacionPorMonto2, IConciliacionPorMonto3 } from 'src/app/interfaces/consolidacion';
import { RecaudacionMediosElectronicosService } from 'src/app/services/procesos/consolidacion/recaudacion-medios-electronicos.service';
import { UtilService } from 'src/app/services/util.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { ConciliacionPorMontoDetalle, TramaConciliacionPorMonto } from 'src/app/models/procesos/consolidacion/recaudacion-medios-electronicos.model';
import { ConciliacionPorMonto } from 'src/app/models/procesos/consolidacion/recaudacion-medios-electronicos.model';
import { IResponse } from 'src/app/interfaces/general.interface';

@Component({
  selector: 'app-conciliar-por-monto',
  templateUrl: './conciliar-por-monto.component.html',
  styleUrls: ['./conciliar-por-monto.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class ConciliarPorMontoComponent implements OnInit, OnDestroy {

  unsubscribe$ = new Subject<void>(); 
  conciliacionPorMonto: IConciliacionPorMonto;
  conciliacionPorMontoDetalle: ConciliacionPorMontoDetalle;
  conciliacionPorMontoBandeja: IConciliacionPorMonto3[];
  bodyConciliacionPorMonto: ConciliacionPorMonto;
  currentPage: string = environment.currentPage;
  loadingConciliacion: boolean = false;
  selectedValuesConciliacion: IConciliacionPorMonto3[] = [];

  constructor(
    public config: DynamicDialogConfig, 
    public funciones: Funciones,
    private dialogRef: DynamicDialogRef,
    private recaudacionMediosElectronicosService: RecaudacionMediosElectronicosService,
    private utilService: UtilService
  ) { }

  ngOnInit(): void { 
    this.conciliacionPorMontoDetalle = new ConciliacionPorMontoDetalle();    
    this.conciliacionPorMonto = this.config.data.conciliacionPorMonto;
    this.setearConciliacionPorMontoDetalle(this.config.data.conciliacionPorMontoDetalle); 
    this.conciliacionPorMontoBandeja = this.config.data.conciliacionPorMontoBandeja;  
  } 

  private setearConciliacionPorMontoDetalle(data: IConciliacionPorMonto2) {  
    console.log('conciMonto', data )  
    this.conciliacionPorMontoDetalle.nuFila = data.nuFila;   
    this.conciliacionPorMontoDetalle.servicioDePago = this.conciliacionPorMonto.servicioDePago;
    this.conciliacionPorMontoDetalle.idOper = data.idOper; 
    this.conciliacionPorMontoDetalle.feOper = data.feOper;
    this.conciliacionPorMontoDetalle.imSumaDepo = (data.imSumaDepo);
    this.conciliacionPorMontoDetalle.imOper = data.imOper;
    this.conciliacionPorMontoDetalle.deEstaCncl = data.deEstaCncl;
  }

  public grabar() {
    if (this.selectedValuesConciliacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos una transacción SCUNAC.");
      return;
    } 
    this.asignarValores();
    this.recaudacionMediosElectronicosService.saveConciliarPorMonto(this.bodyConciliacionPorMonto).pipe(takeUntil(this.unsubscribe$))
    .subscribe((res: IResponse) => {    
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {         
        this.utilService.onShowAlert("success", "Atención", `Se grabó satisfactoriamente el/los registro(s).`);
        this.dialogRef.close();       
      }         
    }, (err: HttpErrorResponse) => {
      this.utilService.onShowMessageErrorSystem();
    });
  }

  private asignarValores() {
    let trama: TramaConciliacionPorMonto[] = [];   
    for (let i = 0; i < this.selectedValuesConciliacion.length; i++) {
      let item = new TramaConciliacionPorMonto();
      item.nuSecu = this.selectedValuesConciliacion[i].nuSecu.toString();
      item.moPago = this.selectedValuesConciliacion[i].moPago;
      trama.push(item);
    }

    this.bodyConciliacionPorMonto = new ConciliacionPorMonto();
    this.bodyConciliacionPorMonto.idCncl = this.conciliacionPorMonto.idCncl;
    this.bodyConciliacionPorMonto.nuFila = this.conciliacionPorMonto.item.toString();
    this.bodyConciliacionPorMonto.trama = trama;  
  }

  public cancelar() {
    this.dialogRef.close();
  }  

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  public formatearMontoImporte( monto: string ): string {
    return Number( monto ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();                      
  }

}
