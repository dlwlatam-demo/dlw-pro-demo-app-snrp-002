import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, first, takeUntil } from 'rxjs/operators';
import { SortEvent, MessageService } from 'primeng/api';
import { 
  IConciliacionPorMonto, 
  IConciliacionPorMonto3, 
  IDataFiltrosRecaudacionME, 
  IRecaudacionMediosElectronicos, 
  IResConciliacionPorMonto2, 
  IVariosEfectivoAnulacion 
} from 'src/app/interfaces/consolidacion';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { RecaudacionMediosElectronicosService } from 'src/app/services/procesos/consolidacion/recaudacion-medios-electronicos.service';
import { UtilService } from 'src/app/services/util.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { ConciliarPorMontoComponent } from '../conciliar-por-monto/conciliar-por-monto.component';
import { environment } from 'src/environments/environment';
import { VariosEfectivoAnulacionesComponent } from './varios-efectivo-anulaciones/varios-efectivo-anulaciones.component';
import { ObservacionesComponent } from './observaciones/observaciones.component';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { DialogService } from 'primeng/dynamicdialog';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import Swal from 'sweetalert2';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { GeneralService } from '../../../../../services/general.service';
@Component({
  selector: 'app-editar-recaudacion',
  templateUrl: './editar-recaudacion.component.html',
  styleUrls: ['./editar-recaudacion.component.scss'],
  providers: [ DialogService, MessageService ],
  encapsulation:ViewEncapsulation.None  
})
export class EditarRecaudacionComponent extends BaseBandeja implements OnInit, OnDestroy {
 
  private unsubscribe$ = new Subject<void>(); 
  public titulo: string;
  public recaudacionMediosElectronicos: IRecaudacionMediosElectronicos;
  public esConsultarRegistro: boolean = false;
  public fecha: string = '';
  public nombreArchivoExcel: string = '';
  public idGuidExcel: string = '';
  public servicioOperador: string = '';
  public mostrarBloque: boolean = true;
  private tiMoviVarios: string = 'M';
  private tiMoviObservaciones: string = 'O';
  private reqs: Observable<any>[] = [];
  public listaVariosEfectivoAnulaciones: IVariosEfectivoAnulacion[] = [];
  public listaObservaciones: IVariosEfectivoAnulacion[] = [];
  public currentPage: string = environment.currentPage;
  public selectedValuesVarios: IVariosEfectivoAnulacion[] = [];
  public selectedValuesObservaciones: IVariosEfectivoAnulacion[] = [];
  public loadingVariosEfectivoAnulaciones: boolean = false;
  public loadingObservaciones: boolean = false;
  public onResetDivConciliar: boolean = false;

  public nameFile!: string;
  public reporteExcel!: string;

  constructor(
    private dialogService: DialogService,
    public funciones: Funciones,
    public recaudacionMediosElectronicosService: RecaudacionMediosElectronicosService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService,
    private generalService: GeneralService,
    private messageService: MessageService
  ){ super() }

  async ngOnInit() {
    this.codBandeja = MenuOpciones.CD_Recaudacion_Medios_Electronicos;
    this.btnConsolidacionMediosElectronicos();
    
    this.esConsultarRegistro = await this.sharingInformationService.compartirEsConsultarMediosElectronicosObservable.pipe(first()).toPromise()
    this.recaudacionMediosElectronicos = await this.sharingInformationService.compartirRecaudacionMediosElectronicosObservable.pipe(first()).toPromise();      
    console.log('this.recaudacionMediosElectronicos', this.recaudacionMediosElectronicos)
    this.titulo = (!this.esConsultarRegistro) ? 'Editar' : 'Consultar';
    this.fecha = this.funciones.convertirFechaDateToString(new Date (this.recaudacionMediosElectronicos.feCncl));
    this.servicioOperador = SERVICIO_DE_PAGO.NIUBIZ;
    this.nombreArchivoExcel = this.recaudacionMediosElectronicos.noAdju0001;
    this.idGuidExcel = this.recaudacionMediosElectronicos.idGuidDocu01;
    this.obtenerListados();  
  }

  private obtenerListados() { 
    this.utilService.onShowProcessLoading("Cargando la data");
    this.reqs.push(
      this.recaudacionMediosElectronicosService.getBandejaConciliacionesMovimiento(this.recaudacionMediosElectronicos.idCncl, this.tiMoviVarios).pipe(
        catchError((err) => {
          if (err.error.category === "NOT_FOUND") {
            return of([])        
          } else {
            return of(undefined)
          } 
        })          
      )
    );
    this.reqs.push(
      this.recaudacionMediosElectronicosService.getBandejaConciliacionesMovimiento(this.recaudacionMediosElectronicos.idCncl, this.tiMoviObservaciones).pipe(
        catchError((err) => {
          if (err.error.category === "NOT_FOUND") {
            return of([])        
          } else {
            return of(undefined)
          } 
        })          
      )
    );
    forkJoin(this.reqs).pipe(takeUntil(this.unsubscribe$)).subscribe((result: any[]) => {     
      this.listaVariosEfectivoAnulaciones = result[0]; 
      this.listaObservaciones = result[1];
      this.utilService.onCloseLoading();
    });
  }

  public async obtenerDataConciliarMonto(conciliacionPorMonto: IConciliacionPorMonto) {
    
    this.utilService.onShowProcessLoading("Cargando la data"); 
    conciliacionPorMonto.idCncl = this.recaudacionMediosElectronicos.idCncl;
    let resConciliacionPorMonto2: IResConciliacionPorMonto2;
    await this.recaudacionMediosElectronicosService.getDetalleConciliarPorMonto(conciliacionPorMonto.idCncl, conciliacionPorMonto.item).pipe(first()).toPromise()
    .then((res: IResConciliacionPorMonto2) => {
      resConciliacionPorMonto2 = res;         
    }).catch(() => { 
      this.utilService.onShowMessageErrorSystem();
      return;   
    }); 

    if (resConciliacionPorMonto2.codResult !== 0) { 
      this.utilService.onShowAlert("warning", "Atención", resConciliacionPorMonto2.msgResult);          
      return;
    } 

    let listaConciliacionPorMonto: IConciliacionPorMonto3[];
    await this.recaudacionMediosElectronicosService.getBandejaConciliarPorMonto(conciliacionPorMonto.idCncl, conciliacionPorMonto.item, conciliacionPorMonto.opcionOperacion).pipe(first()).toPromise()
    .then((res: IConciliacionPorMonto3[]) => {
      listaConciliacionPorMonto = res;
      this.utilService.onCloseLoading();          
    }).catch((err: HttpErrorResponse) => { 
      if (err.error.category === "NOT_FOUND") {
        listaConciliacionPorMonto = [];  
        this.utilService.onCloseLoading();              
      } else {
        this.utilService.onShowMessageErrorSystem();
        return;        
      }   
    });

    const dialog = this.dialogService.open(ConciliarPorMontoComponent, {  
      data: {
        conciliacionPorMonto,
        conciliacionPorMontoDetalle: resConciliacionPorMonto2.operacionNiubiz,
        conciliacionPorMontoBandeja: listaConciliacionPorMonto         
      },  
      header: 'Conciliar por Monto',   
      width: '1200px'         
    });
    dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {      
      // this.onResetDivConciliar = true;      
    });     
  }

  public downloadFileNiubiz( tipo: number ) {
    switch ( tipo ) {
      case 0:
        this.nameFile = 'Recaudacion_MediosElectronicos_NIUBIZ_NoConciliados';
          break;
      case 1:
        this.nameFile = 'Recaudacion_MediosElectronicos_NIUBIZ_Conciliados';
          break;
      case 2:
        this.nameFile = 'Recaudacion_MediosElectronicos_NIUBIZ_Anulaciones';
          break;
      case 3:
        this.nameFile = 'Recaudacion_MediosElectronicos_NIUBIZ_Contracargos';
          break;
      default:
        this.nameFile = 'Recaudacion_MediosElectronicos_NIUBIZ_NoDefinido';
    }

    this.obtenerArchivoNiubiz( this.recaudacionMediosElectronicos.idCncl, tipo );
  }

  obtenerArchivoNiubiz( idCncl: number, inCncl: number ) {
    Swal.showLoading();
    this.recaudacionMediosElectronicosService.getRecaudacionNiubiz( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        console.log('Niubiz', data );
        this.reporteExcel = data.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });

        this.exportExcel();
        Swal.close();
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  public downloadFileDiners( tipo: number ) {
    switch ( tipo ) {
      case 0:
        this.nameFile = 'Recaudacion_MediosElectronicos_DINERS_NoConciliados';
          break;
      case 1:
        this.nameFile = 'Recaudacion_MediosElectronicos_DINERS_Conciliados';
          break;
      default:
        this.nameFile = 'Recaudacion_MediosElectronicos_DINERS_NoDefinido';
    }

    this.obtenerArchivoDiners( this.recaudacionMediosElectronicos.idCncl, tipo );
  }

  obtenerArchivoDiners( idCncl: number, inCncl: number ) {
    Swal.showLoading();
    this.recaudacionMediosElectronicosService.getRecaudacionDiners( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        console.log('Diners', data );
        this.reporteExcel = data.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });

        this.exportExcel();
        Swal.close();
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  public downloadFileAmex( tipo: number ) {
    switch ( tipo ) {
      case 0:
        this.nameFile = 'Recaudacion_MediosElectronicos_AMEX_NoConciliados';
          break;
      case 1:
        this.nameFile = 'Recaudacion_MediosElectronicos_AMEX_Conciliados';
          break;
      default:
        this.nameFile = 'Recaudacion_MediosElectronicos_AMEX_NoDefinido';
    }

    this.obtenerArchivoAmex( this.recaudacionMediosElectronicos.idCncl, tipo );
  }

  obtenerArchivoAmex( idCncl: number, inCncl: number ) {
    Swal.showLoading();
    this.recaudacionMediosElectronicosService.getRecaudacionAmex( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        console.log('Amex', data );
        this.reporteExcel = data.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });

        this.exportExcel();
        Swal.close();
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  public downloadFileScunac( tipo: number ) {
    switch ( tipo ) {
      case 0:
        this.nameFile = 'Recaudacion_MediosElectronicos_SCUNAC_NoConciliados';
          break;
      case 1:
        this.nameFile = 'Recaudacion_MediosElectronicos_SCUNAC_Conciliados';
          break;
      default:
        this.nameFile = 'Recaudacion_MediosElectronicos_SCUNAC_NoDefinido';
    }

    this.obtenerArchivoScunac( this.recaudacionMediosElectronicos.idCncl, tipo );
  }

  obtenerArchivoScunac( idCncl: number, inCncl: number ) {
    Swal.showLoading();
    this.recaudacionMediosElectronicosService.getOperacionesScunac( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        console.log('Scunac', data );
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });

        this.exportExcel();
        Swal.close();
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      },
      complete: () => {
        Swal.close();
      }
    });
  }
  
  public handleChange(e: any) {  
    if (e.index === 0) {
      this.servicioOperador = SERVICIO_DE_PAGO.NIUBIZ;
      this.nombreArchivoExcel = this.recaudacionMediosElectronicos.noAdju0001;
      this.idGuidExcel = this.recaudacionMediosElectronicos.idGuidDocu01;
      this.mostrarBloque = true;
    } else if (e.index === 1) {
      this.servicioOperador = SERVICIO_DE_PAGO.DINERS;
      this.nombreArchivoExcel = this.recaudacionMediosElectronicos.noAdju0002;
      this.idGuidExcel = this.recaudacionMediosElectronicos.idGuidDocu02;
      this.mostrarBloque = true;
    } else if (e.index === 2) {
      this.servicioOperador = SERVICIO_DE_PAGO.AMEX;
      this.nombreArchivoExcel = this.recaudacionMediosElectronicos.noAdju0003;
      this.idGuidExcel = this.recaudacionMediosElectronicos.idGuidDocu03;
      this.mostrarBloque = true;
    } else {
      this.mostrarBloque = false;
    }
  }

  public nuevoVarios() {
    const dialog = this.dialogService.open(VariosEfectivoAnulacionesComponent, {  
      data: {
        idCncl: this.recaudacionMediosElectronicos.idCncl,
        registroEditar: null
      },  
      header: 'Nuevo - Varios/Efectivo/Anulaciones',   
      width: '700px'         
    });
    dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe((result: boolean) => {
      if (result) {
        this.obtenerBandejaConciliacionesMovimiento1();
      }
    }); 
  }  

  public editarVarios() {
    if (this.selectedValuesVarios.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
    } if (this.selectedValuesVarios.length === 1) {
      const dialog = this.dialogService.open(VariosEfectivoAnulacionesComponent, {  
        data: {
          idCncl: this.recaudacionMediosElectronicos.idCncl,
          registroEditar: this.selectedValuesVarios[0]
        },  
        header: 'Editar - Varios/Efectivo/Anulaciones',   
        width: '700px'         
      });
      dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe((result: boolean) => {
        this.selectedValuesVarios = [];
        if (result) {
          this.obtenerBandejaConciliacionesMovimiento1();
        }
      });            
    } else if (this.selectedValuesVarios.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    } 
  }

  private obtenerBandejaConciliacionesMovimiento1() {
    this.loadingVariosEfectivoAnulaciones = true;
    this.recaudacionMediosElectronicosService.getBandejaConciliacionesMovimiento(this.recaudacionMediosElectronicos.idCncl, this.tiMoviVarios)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe((res: IVariosEfectivoAnulacion[]) => {
      this.listaVariosEfectivoAnulaciones = res;    
      this.loadingVariosEfectivoAnulaciones = false; 
    }, (err: HttpErrorResponse) => {   
      this.loadingVariosEfectivoAnulaciones = false;
      if (err.error.category === "NOT_FOUND") {         
        this.utilService.onShowAlert("error", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();        
      }                                 
    });
  }

  public nuevaObservacion() {
    const dialog = this.dialogService.open(ObservacionesComponent, {  
      data: {
        idCncl: this.recaudacionMediosElectronicos.idCncl,
        registroEditar: null
      },  
      header: 'Nueva Observación',   
      width: '700px'         
    });
    dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe((result: boolean) => {
      if (result) {
        this.obtenerBandejaConciliacionesMovimiento2();
      }
    }); 
  }

  public editarObservacion() {
    if (this.selectedValuesObservaciones.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
    } else if (this.selectedValuesObservaciones.length === 1) {
      const dialog = this.dialogService.open(ObservacionesComponent, {  
        data: {
          idCncl: this.recaudacionMediosElectronicos.idCncl,
          registroEditar: this.selectedValuesObservaciones[0]
        },  
        header: 'Editar - Varios/Efectivo/Anulaciones',   
        width: '700px'         
      });
      dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe((result: boolean) => {
        this.selectedValuesObservaciones = [];
        if (result) {
          this.obtenerBandejaConciliacionesMovimiento2();
        }
      });            
    } else if (this.selectedValuesObservaciones.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    } 
  } 

  private obtenerBandejaConciliacionesMovimiento2() {
    this.loadingObservaciones = true;
    this.recaudacionMediosElectronicosService.getBandejaConciliacionesMovimiento(this.recaudacionMediosElectronicos.idCncl, this.tiMoviObservaciones)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe((res: IVariosEfectivoAnulacion[]) => {
      this.listaObservaciones = res;    
      this.loadingObservaciones = false; 
    }, (err: HttpErrorResponse) => {   
      this.loadingObservaciones = false;
      if (err.error.category === "NOT_FOUND") {         
        this.utilService.onShowAlert("error", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();        
      }                                 
    });
  }

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  public async cancelar() {
    let dataFiltrosRecaudacionME: IDataFiltrosRecaudacionME = await this.sharingInformationService.compartirDataFiltrosRecaudacionMediosElectronicosObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionME.esBusqueda) {
      dataFiltrosRecaudacionME.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionMediosElectronicosObservableData = dataFiltrosRecaudacionME;
    }
    this.router.navigate(['SARF/procesos/consolidacion/recaudacion_medios_electronicos/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirEsConsultarMediosElectronicoObservableData = false;
    this.sharingInformationService.compartirRecaudacionMediosElectronicosObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();      
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

  exportExcel() {
    console.log('Exportar Excel');

    const doc = this.reporteExcel;
	  const nFile = this.nameFile;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', nFile);
    document.body.appendChild( download );
    download.click();
  }

}
