import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { Observable, of, forkJoin, Subject } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';
import { GeneralService } from 'src/app/services/general.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Dropdown } from 'primeng/dropdown';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { IResponse, IResponse2 } from 'src/app/interfaces/general.interface';
import { Estado, Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { UtilService } from 'src/app/services/util.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { ConciliacionMovimiento } from 'src/app/models/procesos/consolidacion/recaudacion-medios-electronicos.model';
import { IVariosEfectivoAnulacion } from 'src/app/interfaces/consolidacion';
import { RecaudacionMediosElectronicosService } from 'src/app/services/procesos/consolidacion/recaudacion-medios-electronicos.service';
import { IZonaRegistral, IOficinaRegistral, ILocalAtencion } from '../../../../../../interfaces/combosV2.interface';

@Component({
  selector: 'app-varios-efectivo-anulaciones',
  templateUrl: './varios-efectivo-anulaciones.component.html',
  styleUrls: ['./varios-efectivo-anulaciones.component.scss'],
  encapsulation:ViewEncapsulation.None,
})
export class VariosEfectivoAnulacionesComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();
  public formVariosEfectivoAnulaciones: FormGroup;
  private userCode: any;
  private reqs: Observable<any>[] = [];
  public listaTipos: IResponse2[] = [];
  public listaZonaRegistral: IZonaRegistral[];
  public listaOficinaRegistral: IOficinaRegistral[];
  public listaLocal: ILocalAtencion[];
  public listaEstados: Estado[];
  public loadingOficinaRegistral: boolean = false;
  public loadingLocal: boolean = false;
  private totalCantidad: string;
  private idCncl: number; 
  public registroEditar: IVariosEfectivoAnulacion;
  private conciliacionMovimiento: ConciliacionMovimiento;

  @ViewChild('ddlTipoMovimiento') ddlTipoMovimiento: Dropdown;
  @ViewChild('ddlZonaRegistral') ddlZonaRegistral: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocalAtencion') ddlLocalAtencion: Dropdown;
  @ViewChild('ddlEstado') ddlEstado: Dropdown;

  constructor(
    public config: DynamicDialogConfig,
    private dialogRef: DynamicDialogRef,
    private formBuilder: FormBuilder,
    public funciones: Funciones,
    public generalService: GeneralService,
    private recaudacionMediosElectronicosService: RecaudacionMediosElectronicosService,
    private utilService: UtilService
  ) {     
  }  

  ngOnInit(): void {
    this.idCncl = this.config.data.idCncl;
    this.registroEditar = this.config.data.registroEditar;
    console.log('registroEditar', this.registroEditar);
    this.construirFormulario();
    this.inicilializarListas();
    if (this.registroEditar === null) {
      this.obtenerListadosNuevo();
    } else {
      this.obtenerListadosEditar();
    }    
  }

  private construirFormulario() {
    this.formVariosEfectivoAnulaciones = this.formBuilder.group({
      tipo: [''],
      descripcion: [''],
      zonaRegistral: [''],        
      oficinaRegistral: [''],
      localAtencion: [''],
      recaudacion: [''],
      comision: [''],
      igv: [''],
      alqMantEquipos: [''],
      deudaDiaAnterior: [''],
      deudaSgteDia: [''],
      total: [''],
      estado: ['']         
    });
  }

  private inicilializarListas() {
    this.listaTipos = []; 
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];  
    this.listaEstados = [];
    this.listaTipos.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' });  
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    this.listaEstados.push({ coEstado: '*', deEstado: '(SELECCIONAR)' });    
  }  

  private obtenerListadosNuevo() {    
    this.utilService.onShowProcessLoading("Cargando la data");    
    this.reqs.push(
      this.generalService.getCbo_TipoMovimiento().pipe(
        catchError((err) => {        
          if (err.error.category === "NOT_FOUND") {
            return of([])        
          } else {
            return of(undefined)
          } 
        })          
      )
    );
    this.reqs.push(
      this.generalService.getCbo_ZonasRegistrales().pipe(
        catchError((err) => {        
          if (err.error.category === "NOT_FOUND") {
            return of([])        
          } else {
            return of(undefined)
          } 
        })          
      )
    );
    forkJoin(this.reqs).pipe(takeUntil(this.unsubscribe$)).subscribe((result: any[]) => {      
      this.listaTipos.push(...result[0]); 
      this.listaZonaRegistral.push(...result[1]);                
      this.utilService.onCloseLoading();
    });
  }

  private obtenerListadosEditar() {
    this.utilService.onShowProcessLoading("Cargando la data");
    this.reqs.push(
      this.generalService.getCbo_TipoMovimiento().pipe(
        catchError((err) => {        
          if (err.error.category === "NOT_FOUND") {
            return of([])        
          } else {
            return of(undefined)
          } 
        })          
      )
    );
    this.reqs.push(
      this.generalService.getCbo_ZonasRegistrales().pipe(
        catchError((err) => {        
          if (err.error.category === "NOT_FOUND") {
            return of([])        
          } else {
            return of(undefined)
          } 
        })          
      )
    );
    this.reqs.push(
      this.generalService.getCbo_OficinasRegistrales( this.registroEditar.coZonaRegi ).pipe(
        catchError((err) => {        
          if (err.error.category === "NOT_FOUND") {
            return of([])        
          } else {
            return of(undefined)
          } 
        })          
      )
    );
    this.reqs.push(
      this.generalService.getCbo_LocalesAtencion( this.registroEditar.coZonaRegi, this.registroEditar.coOficRegi ).pipe(
        catchError((err) => {        
          if (err.error.category === "NOT_FOUND") {
            return of([])        
          } else {
            return of(undefined)
          } 
        })          
      )
    );
    forkJoin(this.reqs).pipe(takeUntil(this.unsubscribe$)).subscribe((result: any[]) => {      
      this.listaTipos.push(...result[0]); 
      this.listaZonaRegistral.push(...result[1]);  
      this.listaOficinaRegistral.push(...result[2]);      
      this.listaLocal.push(...result[3]);  
      this.listaEstados.push(...this.generalService.getCbo_Estado());
      for (let i = 0; i < this.listaEstados.length; i++) {
        this.listaEstados[i].deEstado = this.listaEstados[i].deEstado.toUpperCase();      
      }
      this.setearCampos(); 
      this.utilService.onCloseLoading();      
    });
  }

  private setearCampos() {  
    this.formVariosEfectivoAnulaciones.patchValue({ "tipo": this.registroEditar.tiMovi });
    this.formVariosEfectivoAnulaciones.patchValue({ "descripcion": this.registroEditar.obMovi });
    this.formVariosEfectivoAnulaciones.patchValue({ "zonaRegistral": this.registroEditar.coZonaRegi });
    this.formVariosEfectivoAnulaciones.patchValue({ "oficinaRegistral": this.registroEditar.coOficRegi });
    this.formVariosEfectivoAnulaciones.patchValue({ "localAtencion": this.registroEditar.coLocaAten });
    this.formVariosEfectivoAnulaciones.patchValue({ "recaudacion": this.registroEditar.imReca.toFixed(2) });
    this.formVariosEfectivoAnulaciones.patchValue({ "comision": this.registroEditar.imComi.toFixed(2) });
    this.formVariosEfectivoAnulaciones.patchValue({ "igv": this.registroEditar.imIgv.toFixed(2) });
    this.formVariosEfectivoAnulaciones.patchValue({ "alqMantEquipos": this.registroEditar.imAlquEqui.toFixed(2) });
    this.formVariosEfectivoAnulaciones.patchValue({ "deudaDiaAnterior": this.registroEditar.imDeudAnte.toFixed(2) });
    this.formVariosEfectivoAnulaciones.patchValue({ "deudaSgteDia": this.registroEditar.imDeudSigu.toFixed(2) });   
    this.calcularTotal();
    this.formVariosEfectivoAnulaciones.patchValue({ "estado": this.registroEditar.inRegi });
  }

  public buscarOficinaRegistral() {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formVariosEfectivoAnulaciones.get('zonaRegistral')?.value !== "*") {
      console.log('zonaRegistral', this.formVariosEfectivoAnulaciones.get('zonaRegistral')?.value)
      this.generalService.getCbo_OficinasRegistrales(this.formVariosEfectivoAnulaciones.get('zonaRegistral')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: IOficinaRegistral[]) => {
        console.log('dataOfic', data)
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.formVariosEfectivoAnulaciones.patchValue({ "oficinaRegistral": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }        
        this.loadingOficinaRegistral = false;
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    }
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formVariosEfectivoAnulaciones.get('oficinaRegistral')?.value !== "*") {
      console.log('oficinaRegistral', this.formVariosEfectivoAnulaciones.get('oficinaRegistral')?.value)
      this.generalService.getCbo_LocalesAtencion(this.formVariosEfectivoAnulaciones.get('zonaRegistral')?.value, this.formVariosEfectivoAnulaciones.get('oficinaRegistral')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: ILocalAtencion[]) => {
        console.log('dataLoc', data);
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formVariosEfectivoAnulaciones.patchValue({ "localAtencion": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        }        
        this.loadingLocal = false;      
      }, err => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  private calcularTotal() {
    let recaudacion: number = (this.formVariosEfectivoAnulaciones.get('recaudacion').value === '') ? 0 : Number(this.formVariosEfectivoAnulaciones.get('recaudacion').value);
    let comision: number = (this.formVariosEfectivoAnulaciones.get('comision').value === '') ? 0 : Number(this.formVariosEfectivoAnulaciones.get('comision').value);
    let igv: number = (this.formVariosEfectivoAnulaciones.get('igv').value === '') ? 0 : Number(this.formVariosEfectivoAnulaciones.get('igv').value);
    let alqMantEquipos: number = (this.formVariosEfectivoAnulaciones.get('alqMantEquipos').value === '') ? 0 : Number(this.formVariosEfectivoAnulaciones.get('alqMantEquipos').value);
    let deudaDiaAnterior: number = (this.formVariosEfectivoAnulaciones.get('deudaDiaAnterior').value === '') ? 0 : Number(this.formVariosEfectivoAnulaciones.get('deudaDiaAnterior').value);
    let deudaSgteDia: number = (this.formVariosEfectivoAnulaciones.get('deudaSgteDia').value === '') ? 0 : Number(this.formVariosEfectivoAnulaciones.get('deudaSgteDia').value);  
    let total = recaudacion + deudaSgteDia - (comision + igv + alqMantEquipos + deudaDiaAnterior);
    this.totalCantidad = total.toFixed(2);
    this.formVariosEfectivoAnulaciones.patchValue({ "total": this.totalCantidad});
  }

  public formatearRecaudacion(event:any) {  
    if (event.target.value && this.funciones.validarSoloNumeros(event.target.value)) {
      this.formVariosEfectivoAnulaciones.get('recaudacion')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
      this.calcularTotal();
    } else if (!event.target.value) {
      this.calcularTotal();
    }
  }

  public formatearComision(event:any) {
    if (event.target.value && this.funciones.validarSoloNumeros(event.target.value)) {
      this.formVariosEfectivoAnulaciones.get('comision')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
      this.calcularTotal();
    } else if (!event.target.value) {
      this.calcularTotal();
    }
  }

  public formatearIgv(event:any) {
    if (event.target.value && this.funciones.validarSoloNumeros(event.target.value)) {
      this.formVariosEfectivoAnulaciones.get('igv')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
      this.calcularTotal();
    } else if (!event.target.value) {
      this.calcularTotal();
    }
  }
  
  public formatearAlqMantEquipos(event:any) {
    if (event.target.value && this.funciones.validarSoloNumeros(event.target.value)) {
      this.formVariosEfectivoAnulaciones.get('alqMantEquipos')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
      this.calcularTotal();
    } else if (!event.target.value) {
      this.calcularTotal();
    }
  }

  public formatearDeudaDiaAnterior(event:any) {
    if (event.target.value && this.funciones.validarSoloNumeros(event.target.value)) {
      this.formVariosEfectivoAnulaciones.get('deudaDiaAnterior')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
      this.calcularTotal();
    } else if (!event.target.value) {
      this.calcularTotal();
    }
  }

  public formatearDeudaSgteDia(event:any) {
    if (event.target.value && this.funciones.validarSoloNumeros(event.target.value)) {
      this.formVariosEfectivoAnulaciones.get('deudaSgteDia')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
      this.calcularTotal();
    } else if (!event.target.value) {
      this.calcularTotal();
    }
  }

  public grabar() {
    let resultado: boolean = this.validarFormulario();
    if (!resultado) {
      return;
    } 

    this.utilService.onShowProcessSaveEdit(); 
    this.asignarValores(this.formVariosEfectivoAnulaciones.getRawValue());

    let obs: Observable<any>;
    obs = (this.registroEditar === null) ? 
    this.recaudacionMediosElectronicosService.createConciliacionMovimiento(this.conciliacionMovimiento) : 
    this.recaudacionMediosElectronicosService.editConciliacionMovimiento(this.conciliacionMovimiento);
    
    obs.pipe(takeUntil(this.unsubscribe$))
    .subscribe( (res: IResponse) => {     
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {         
        this.respuestaExitosaGrabar();
      }         
    }, (err: HttpErrorResponse) => {
      this.utilService.onShowMessageErrorSystem();  
    })
  }

  private async respuestaExitosaGrabar() {
    const verbo = (this.registroEditar === null) ? 'creó' : 'editó'
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Se ${verbo} satisfactoriamente el registro.`);
    if (respuesta.isConfirmed) {
      this.dialogRef.close(true); 
    }
  }

  private validarFormulario(): boolean {
    const registro = this.formVariosEfectivoAnulaciones.getRawValue();  
    if (registro.tipo === "" || registro.tipo === "*" || registro.tipo === null) {      
      this.ddlTipoMovimiento.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un tipo de movimiento.");
      return false;      
    } else if (this.funciones.eliminarEspaciosEnBlanco(registro.descripcion) === "" || registro.descripcion === null) {                    
      document.getElementById('idDescripcion').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese una descripción.");
      return false;      
    } else if (registro.zonaRegistral === "" || registro.zonaRegistral === "*" || registro.zonaRegistral === null) {      
      this.ddlZonaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una zona registral.");
      return false;      
    } else if (registro.oficinaRegistral === "" || registro.oficinaRegistral === "*" || registro.oficinaRegistral === null) {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una oficina registral.");
      return false;      
    } else if (registro.localAtencion === "" || registro.localAtencion === "*" || registro.localAtencion === null) {      
      this.ddlLocalAtencion.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un local de atención.");
      return false;      
    } else if (registro.recaudacion === "" || registro.recaudacion === null) {
      document.getElementById('idRecaudacion').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese la recaudación.");
    } else if (!this.funciones.validarSoloNumeros(registro.recaudacion)) {
      document.getElementById('idRecaudacion').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese un número válido para la recaudación.");
    } else if (registro.comision === "" || registro.comision === null) {
      document.getElementById('idComision').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese la comisión.");
    } else if (!this.funciones.validarSoloNumeros(registro.comision)) {
      document.getElementById('idComision').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese un número válido para la comisión.");
    } else if (registro.igv === "" || registro.igv === null) {
      document.getElementById('idIgv').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese el I.G.V.");
    } else if (!this.funciones.validarSoloNumeros(registro.igv)) {
      document.getElementById('idIgv').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese un número válido para el I.G.V.");
    } else if (registro.alqMantEquipos === "" || registro.alqMantEquipos === null) {
      document.getElementById('idAlqMantEquipos').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese el alquiler de mantenimiento de equipos.");
    } else if (!this.funciones.validarSoloNumeros(registro.alqMantEquipos)) {
      document.getElementById('idAlqMantEquipos').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese un número válido para el alquiler de mantenimiento de equipos.");
    } else if (registro.deudaDiaAnterior === "" || registro.deudaDiaAnterior === null) {
      document.getElementById('idDeudaDiaAnterior').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese la deuda del día anterior.");
    } else if (!this.funciones.validarSoloNumeros(registro.deudaDiaAnterior)) {
      document.getElementById('idDeudaDiaAnterior').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese un número válido para la deuda del día anterior.");
    } else if (registro.deudaSgteDia === "" || registro.deudaSgteDia === null) {
      document.getElementById('idDeudaSgteDia').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese la deuda del siguiente día.");
    } else if (!this.funciones.validarSoloNumeros(registro.deudaSgteDia)) {
      document.getElementById('idDeudaSgteDia').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese un número válido para la deuda del siguiente día.");
    } else if (this.registroEditar !== null && (registro.estado === "" || registro.estado === "*" || registro.estado === null)) {      
      this.ddlEstado.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un estado.");
      return false;      
    }
    return true;
  }

  private asignarValores(registro: any) {
    this.conciliacionMovimiento = new ConciliacionMovimiento();  
    this.conciliacionMovimiento.idCnclMovi = (this.registroEditar === null) ? null : this.registroEditar.idCnclMovi;    
    this.conciliacionMovimiento.idCncl = this.idCncl;
    this.conciliacionMovimiento.tiMovi = registro.tipo;
    this.conciliacionMovimiento.coZonaRegi = registro.zonaRegistral;
    this.conciliacionMovimiento.coOficRegi = registro.oficinaRegistral;
    this.conciliacionMovimiento.coLocaAten = registro.localAtencion;
    this.conciliacionMovimiento.obMovi = registro.descripcion;
    this.conciliacionMovimiento.imReca = registro.recaudacion;
    this.conciliacionMovimiento.imComi = registro.comision;
    this.conciliacionMovimiento.imIgv = registro.igv;
    this.conciliacionMovimiento.imAlquEqui = registro.alqMantEquipos;
    this.conciliacionMovimiento.imDeudAnte = registro.deudaDiaAnterior;
    this.conciliacionMovimiento.imDeudSigu = registro.deudaSgteDia;
    this.conciliacionMovimiento.imTotal = this.totalCantidad;
    this.conciliacionMovimiento.inRegi = (this.registroEditar === null) ? 'A' : registro.estado;    
  }

  public cancelar() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();                      
  }

}
