import { Component, Input, Output, OnInit, OnChanges, ViewEncapsulation, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { IConciliacionPorMonto } from 'src/app/interfaces/consolidacion';
import { OPERACION_SCUNAC_NO_CONCILAIDAS } from 'src/app/models/enum/parameters';
import { UtilService } from 'src/app/services/util.service';
import { BaseBandeja } from '../../../../../../base/base-bandeja.abstract';
@Component({
  selector: 'app-div-conciliar-por-monto',
  templateUrl: './div-conciliar-por-monto.component.html',
  styleUrls: ['./div-conciliar-por-monto.component.scss'],
  encapsulation:ViewEncapsulation.None 
})
export class DivConciliarPorMontoComponent extends BaseBandeja implements OnInit, OnChanges {  

  @Input() bandeja!: number;
  @Input() btnConciliar!: number;
  @Input() servicioDePago: string = '';
  @Input() onReset: boolean = false;
  @Output() eventDataConciliarMonto = new EventEmitter<IConciliacionPorMonto>(); 

  public form: FormGroup;
  public item: number = null;
  public opcionOperacion: number = null;
  public igualMonto: number = OPERACION_SCUNAC_NO_CONCILAIDAS.CON_IGUAL_MONTO;
  public menorIgualMonto: number  = OPERACION_SCUNAC_NO_CONCILAIDAS.CON_MENOR_O_IGUAL_MONTO; 
  public ConciliacionPorMonto: IConciliacionPorMonto;

  constructor(
    private formBuilder: FormBuilder,
    public funciones: Funciones,   
    private utilService: UtilService
  ) { super() }

  ngOnInit(): void {
    this.construirFormulario();
  }

  private construirFormulario() {
    this.form = this.formBuilder.group({
      item: [''],
      opcionOperacion: [this.igualMonto],         
    });
  }

  public conciliarPorMonto() {    
    if (this.form.get('item').value === null || this.form.get('item').value === '') {
      document.getElementById('inputItem').focus(); 
      this.utilService.onShowAlert("warning", "Atención", "Ingrese el item.");
      return;
    } else if (this.form.get('opcionOperacion').value === null || this.form.get('opcionOperacion').value === '') {
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una opción de las operaciones SCUNAC.");
      return;
    }
    this.ConciliacionPorMonto = {  
      idCncl: null,
      servicioDePago: this.servicioDePago,
      item: this.form.get('item').value,
      opcionOperacion: this.form.get('opcionOperacion').value
    }   
    this.eventDataConciliarMonto.emit(this.ConciliacionPorMonto);   
  }

  ngOnChanges() {
    if (this.onReset) {
      this.form.reset();
    }
  }

}
