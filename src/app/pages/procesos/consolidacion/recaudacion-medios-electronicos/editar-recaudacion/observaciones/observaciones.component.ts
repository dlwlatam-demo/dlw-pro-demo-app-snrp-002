import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Dropdown } from 'primeng/dropdown';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { Estado } from 'src/app/interfaces/combos.interface';
import { IVariosEfectivoAnulacion } from 'src/app/interfaces/consolidacion';
import { GeneralService } from 'src/app/services/general.service';
import { UtilService } from 'src/app/services/util.service';
import { ConciliacionMovimiento } from 'src/app/models/procesos/consolidacion/recaudacion-medios-electronicos.model';
import { RecaudacionMediosElectronicosService } from 'src/app/services/procesos/consolidacion/recaudacion-medios-electronicos.service';
import { IResponse } from 'src/app/interfaces/general.interface';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-observaciones',
  templateUrl: './observaciones.component.html',
  styleUrls: ['./observaciones.component.scss']
})
export class ObservacionesComponent implements OnInit, OnDestroy {

  formObservaciones: FormGroup;
  private unsubscribe$ = new Subject<void>();
  public listaEstados: Estado[];
  private idCncl: number; 
  public registroEditar: IVariosEfectivoAnulacion;
  private conciliacionObservacion: ConciliacionMovimiento;

  @ViewChild('ddlEstado') ddlEstado: Dropdown;

  constructor(
    public config: DynamicDialogConfig,
    private dialogRef: DynamicDialogRef,
    private formBuilder: FormBuilder,
    public funciones: Funciones,
    public generalService: GeneralService,
    private recaudacionMediosElectronicosService: RecaudacionMediosElectronicosService,
    private utilService: UtilService
  ) { }

  ngOnInit(): void {
    this.idCncl = this.config.data.idCncl;
    this.registroEditar = this.config.data.registroEditar; 
    this.construirFormulario();
    if (this.registroEditar !== null) {
      this.listaEstados = [];
      this.listaEstados.push({ coEstado: '*', deEstado: '(SELECCIONAR)' }); 
      this.listaEstados.push(...this.generalService.getCbo_Estado());
      for (let i = 0; i < this.listaEstados.length; i++) {
        this.listaEstados[i].deEstado = this.listaEstados[i].deEstado.toUpperCase();      
      }
      this.setearCampos(); 
    }     
  }

  private setearCampos() {  
    this.formObservaciones.patchValue({ "observacion": this.registroEditar.obMovi });   
    this.formObservaciones.patchValue({ "estado": this.registroEditar.inRegi });
  }

  private construirFormulario() {
    this.formObservaciones = this.formBuilder.group({
      observacion: [''],
      estado: ['']           
    });
  }

  public grabar() {
    const registro = this.formObservaciones.getRawValue();
    if (this.funciones.eliminarEspaciosEnBlanco(registro.observacion) === "" || registro.observacion === null) {                    
      document.getElementById('idObservacion').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese una observación.");
      return;      
    } else if (this.registroEditar !== null && (registro.estado === "" || registro.estado === "*" || registro.estado === null)) {      
      this.ddlEstado.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un estado.");
      return;      
    } 
    this.utilService.onShowProcessSaveEdit();
    this.conciliacionObservacion = new ConciliacionMovimiento();
    this.conciliacionObservacion.idCnclMovi = (this.registroEditar === null) ? null : this.registroEditar.idCnclMovi;    
    this.conciliacionObservacion.idCncl = this.idCncl;
    this.conciliacionObservacion.tiMovi = '4';    
    this.conciliacionObservacion.obMovi = registro.observacion;
    this.conciliacionObservacion.inRegi = (this.registroEditar === null) ? '' : registro.estado;
    
    let obs: Observable<any>;
    obs = (this.registroEditar === null) ? 
    this.recaudacionMediosElectronicosService.createConciliacionMovimiento(this.conciliacionObservacion) : 
    this.recaudacionMediosElectronicosService.editConciliacionMovimiento(this.conciliacionObservacion);

    obs.pipe(takeUntil(this.unsubscribe$))
    .subscribe( (res: IResponse) => {     
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {         
        this.respuestaExitosaGrabar();
      }         
    }, (err: HttpErrorResponse) => {
      this.utilService.onShowMessageErrorSystem();  
    })
  }

  private async respuestaExitosaGrabar() {
    const verbo = (this.registroEditar === null) ? 'creó' : 'editó'
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Se ${verbo} satisfactoriamente el registro.`);
    if (respuesta.isConfirmed) {
      this.dialogRef.close(true); 
    }
  }

  public cancelar() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();                      
  }

}
