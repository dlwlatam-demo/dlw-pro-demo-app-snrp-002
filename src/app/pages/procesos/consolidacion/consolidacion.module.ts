import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsolidacionRoutingModule } from './consolidacion-routing.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';
import { DigitOnlyModule } from '@uiowa/digit-only';
import { SharedModule } from 'src/app/shared/shared.module';
import { BandejaRecaudComponent } from './recaudacion-medios-electronicos/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionComponent } from './recaudacion-medios-electronicos/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionComponent } from './recaudacion-medios-electronicos/editar-recaudacion/editar-recaudacion.component';
import { DivConciliarPorMontoComponent } from './recaudacion-medios-electronicos/editar-recaudacion/div-conciliar-por-monto/div-conciliar-por-monto.component';
import { ConciliarPorMontoComponent } from './recaudacion-medios-electronicos/conciliar-por-monto/conciliar-por-monto.component';
import { VariosEfectivoAnulacionesComponent } from './recaudacion-medios-electronicos/editar-recaudacion/varios-efectivo-anulaciones/varios-efectivo-anulaciones.component';
import { ObservacionesComponent } from './recaudacion-medios-electronicos/editar-recaudacion/observaciones/observaciones.component';
import { TotalPorOficinaComponent } from './recaudacion-medios-electronicos/total-por-oficina/total-por-oficina.component';

import { BandejaRecaudSprlComponent } from './recaudacion-sprl-virtual/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionSprlComponent } from './recaudacion-sprl-virtual/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionSprlComponent } from './recaudacion-sprl-virtual/editar-recaudacion/editar-recaudacion.component';
import { DivConciliarPorMontoSprlComponent } from './recaudacion-sprl-virtual/editar-recaudacion/div-conciliar-por-monto/div-conciliar-por-monto-sprl.component';
import { DivConciliarPorMontoNiubizComponent } from './recaudacion-sprl-virtual/editar-recaudacion/div-conciliar-por-monto/div-conciliar-por-monto-niubiz.component';
import { ConciliarPorMontoSprlComponent } from './recaudacion-sprl-virtual/conciliar-por-monto/conciliar-por-monto-sprl.component';

import { BandejaRecaudPagaloPeComponent } from './recaudacion-pagalo-pe/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionPagaloPeComponent } from './recaudacion-pagalo-pe/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionPagaloPeComponent } from './recaudacion-pagalo-pe/editar-recaudacion/editar-recaudacion.component';

import { BandejaRecaudMacmypeComponent } from './recaudacion-macmype/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionMacmypeComponent } from './recaudacion-macmype/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionMacmypeComponent } from './recaudacion-macmype/editar-recaudacion/editar-recaudacion.component';

import { BandejaRecaudScunacRemesasComponent } from './recaudacion-scunac-remesas/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionScunacRemesasComponent } from './recaudacion-scunac-remesas/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionScunacRemesasComponent } from './recaudacion-scunac-remesas/editar-recaudacion/editar-recaudacion.component';

import { BandejaRecaudAppQrSidComponent } from './recaudacion-app-qr-sid/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionAppQrSidComponent } from './recaudacion-app-qr-sid/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionAppQrSidComponent } from './recaudacion-app-qr-sid/editar-recaudacion/editar-recaudacion.component';
import { ConciliarPorNiubizComponent } from './recaudacion-sprl-virtual/conciliar-por-niubiz/conciliar-por-niubiz.component';
import { OtrasOperacionesHermesComponent } from './recaudacion-scunac-remesas/otras-operaciones-hermes/otras-operaciones-hermes.component';


@NgModule({
  declarations: [
    BandejaRecaudComponent,
    NuevaRecaudacionComponent,
    EditarRecaudacionComponent,    
    DivConciliarPorMontoComponent,
    ConciliarPorMontoComponent,
    VariosEfectivoAnulacionesComponent,
    ObservacionesComponent,
    TotalPorOficinaComponent,
	BandejaRecaudSprlComponent,
    NuevaRecaudacionSprlComponent,
    EditarRecaudacionSprlComponent,    
    DivConciliarPorMontoSprlComponent,
    ConciliarPorMontoSprlComponent,
	BandejaRecaudPagaloPeComponent,
    NuevaRecaudacionPagaloPeComponent,
    EditarRecaudacionPagaloPeComponent,  
	BandejaRecaudMacmypeComponent,
    NuevaRecaudacionMacmypeComponent,
    EditarRecaudacionMacmypeComponent,  
	BandejaRecaudScunacRemesasComponent,
    NuevaRecaudacionScunacRemesasComponent,
    EditarRecaudacionScunacRemesasComponent, 
	BandejaRecaudAppQrSidComponent,
    NuevaRecaudacionAppQrSidComponent,
    EditarRecaudacionAppQrSidComponent,
    DivConciliarPorMontoNiubizComponent,
    ConciliarPorNiubizComponent,
    OtrasOperacionesHermesComponent, 
	],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ConsolidacionRoutingModule,
    NgPrimeModule,
    SharedModule,
    DigitOnlyModule
  ]
})
export class ConsolidacionModule { }
