import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import { ConciliacionNIUBIZ } from '../../../../../models/procesos/consolidacion/recaudacion-sprl-virtual.model';

import { IResponse2, IResponse } from '../../../../../interfaces/general.interface';
import { IResConciliacionNIUBIZ, IConciliacionNIUBIZ } from '../../../../../interfaces/consolidacion-sprl-virtual';

import { UtilService } from '../../../../../services/util.service';
import { GeneralService } from '../../../../../services/general.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { RecaudacionSprlService } from 'src/app/services/procesos/consolidacion/recaudacion-sprl-virtual.service';

@Component({
  selector: 'app-conciliar-por-niubiz',
  templateUrl: './conciliar-por-niubiz.component.html',
  styleUrls: ['./conciliar-por-niubiz.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class ConciliarPorNiubizComponent implements OnInit {

  imOper: string = '';
  imDcc: string = '';
  imComiTota: string = '';
  imComiNiub: string = '';
  im_igv: string = '';
  imSumaDepo: string = '';

  formRecaudacionNiubiz!: FormGroup;

  conciliacionManual: IConciliacionNIUBIZ;
  bodyConciliacionManual: ConciliacionNIUBIZ;

  listaEstado: IResponse2[] = [];
  listaEstadoFiltro!: IResponse2[];

  unsubscribe$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private utilService: UtilService,
    private generalService: GeneralService,
    private validateService: ValidatorsService,
    private recaudacionSPRLService: RecaudacionSprlService
  ) { }

  ngOnInit(): void {
    this.listaEstado = [];
    this.conciliacionManual = new ConciliacionNIUBIZ();

    this.formRecaudacionNiubiz = this.fb.group({
      estado: [''],   
      obCnclNiub: ['']              
    });

    this.obtenerListados();
    this.setearConciliacionNIUBIZ( this.config.data.niubiz )
  }

  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_EstadoSprl()
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resEstado]) => {
      this.listaEstado.push(...resEstado);        
	    this.listaEstadoFiltro = [...this.listaEstado];    
      
      this.utilService.onCloseLoading();

      let estado = this.listaEstado.findIndex( x => x.ccodigoHijo == this.conciliacionManual.inCncl );
      this.formRecaudacionNiubiz.patchValue({ "estado": resEstado[estado].ccodigoHijo });

	  
    }, (err: HttpErrorResponse) => {                                  
    });
  }

  private setearConciliacionNIUBIZ( data: IResConciliacionNIUBIZ ) {
    console.log('niubiz', data);
    const niubiz = data.operacionNiubiz;

    this.conciliacionManual.idCncl = this.config.data.id;

    this.conciliacionManual.nuRuc = niubiz.nuRuc.trim();
    this.conciliacionManual.noRazoSoci = niubiz.noRazoSoci.trim();

    this.conciliacionManual.coCome = niubiz.coCome.trim();
    this.conciliacionManual.noCome = niubiz.noCome.trim();
    
    this.conciliacionManual.feOper = this.validateService.reFormateaFechaConHoras( niubiz.feOper.trim() );
    this.conciliacionManual.feDepo = this.validateService.reFormateaFecha( niubiz.feDepo.trim() );
    
    this.conciliacionManual.noTipoOper = niubiz.noTipoOper.trim();
    
    this.conciliacionManual.nuTarj = niubiz.nuTarj.trim();
    this.conciliacionManual.inOrigTarj = niubiz.inOrigTarj.trim();
    this.conciliacionManual.deTipoTarj = niubiz.deTipoTarj.trim();
    this.conciliacionManual.noMarcTarj = niubiz.noMarcTarj.trim();
    
    this.conciliacionManual.noMone = niubiz.noMone.trim();
    this.conciliacionManual.imOper = niubiz.imOper;
    
    this.conciliacionManual.deEsDcc = niubiz.deEsDcc;
    this.conciliacionManual.imDcc = niubiz.imDcc;

    this.conciliacionManual.imComiTota = niubiz.imComiTota;
    this.conciliacionManual.imComiNiub = niubiz.imComiNiub;
    this.conciliacionManual.im_igv = niubiz.im_igv;
    this.conciliacionManual.imSumaDepo = niubiz.imSumaDepo;
    
    this.conciliacionManual.deEsta = niubiz.deEsta.trim();
    this.conciliacionManual.idOper = niubiz.idOper.trim();
    
    this.conciliacionManual.coCuenBancPaga = niubiz.coCuenBancPaga.trim();
    this.conciliacionManual.noBancPaga = niubiz.noBancPaga.trim();
    
    this.conciliacionManual.nuVouc = niubiz.nuVouc.trim();
    this.conciliacionManual.nuAuth = niubiz.nuAuth.trim();
    
    this.conciliacionManual.inCncl = niubiz.inCncl;
    this.conciliacionManual.noProd = niubiz.noProd;
    this.conciliacionManual.nuFila = niubiz.nuFila;
    this.conciliacionManual.deEstaCncl = niubiz.deEstaCncl;

    this.formRecaudacionNiubiz.patchValue({ 'obCnclNiub': niubiz.obCnclNiub.trim() });

    this.imOper = niubiz.imOper.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });

    this.imDcc = niubiz.imDcc.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });

    this.imComiTota = niubiz.imComiTota.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });

    this.imComiNiub = niubiz.imComiNiub.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });

    this.im_igv = niubiz.im_igv.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });

    this.imSumaDepo = niubiz.imSumaDepo.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

  public grabar() {
    this.utilService.onShowProcessLoading("Procesando la data");  
    this.asignarValores();
    console.log('this.bodyConciliacionManual', this.bodyConciliacionManual);
    this.recaudacionSPRLService.saveConciliarNIUBIZ(this.bodyConciliacionManual).pipe(takeUntil(this.unsubscribe$))
    .subscribe((res: IResponse) => {
      if (res.codResult < 0 ) {
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {
        this.utilService.onShowAlert("success", "Atención", `Se grabó satisfactoriamente el/los registro(s).`);
        this.ref.close();       
      }         
    }, (err: HttpErrorResponse) => {
      this.utilService.onShowMessageErrorSystem();
    });
  }

  private asignarValores() {
    this.bodyConciliacionManual = new ConciliacionNIUBIZ();
    this.bodyConciliacionManual.idCncl = this.conciliacionManual.idCncl;
    this.bodyConciliacionManual.nuFila = this.conciliacionManual.nuFila;
    this.bodyConciliacionManual.inCncl = this.formRecaudacionNiubiz.get('estado')?.value;
    this.bodyConciliacionManual.obCnclNiub = this.formRecaudacionNiubiz.get('obCnclNiub')?.value;
  }

  public cancelar() {
    this.ref.close('');
  }

}
