import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Subject } from 'rxjs';
import { concatMap, filter, first, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Dropdown } from 'primeng/dropdown';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { GeneralService } from 'src/app/services/general.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { IDataFileExcel, IDataFiltrosRecaudacionSprl, IRecaudacionSprl } from 'src/app/interfaces/consolidacion-sprl-virtual';
import { RecaudacionSprl } from 'src/app/models/procesos/consolidacion/recaudacion-sprl-virtual.model';
import { RecaudacionSprlService } from 'src/app/services/procesos/consolidacion/recaudacion-sprl-virtual.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { UtilService } from 'src/app/services/util.service';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { IResponse } from 'src/app/interfaces/general.interface';
import { ArchivoAdjunto } from '../../../../../models/archivo-adjunto.model';
import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';

@Component({
  selector: 'app-nueva-recaudacion',
  templateUrl: './nueva-recaudacion.component.html',
  styleUrls: ['./nueva-recaudacion.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class NuevaRecaudacionSprlComponent implements OnInit, OnDestroy  {
  
  formRecaudacionSprl: FormGroup;
  private unsubscribe$ = new Subject<void>();
  public titulo: string;
  public listaZonaRegistral: ZonaRegistral[];
  public listaOficinaRegistral: OficinaRegistral[];
  public listaLocal: Local[]; 
  public fechaMaxima: Date;
  private userCode: any;
  public loadingOficinaRegistral: boolean = false;
  public loadingLocal: boolean = false;
  public SERVICIO_NIUBIZ = SERVICIO_DE_PAGO.NIUBIZ_SPRL;
  public nameNiubiz: string = '';
  public servicioOperador: string = '';
  public files: ArchivoAdjunto[] = [];
  public sizeFiles: number = 0;
  private recaudacionSprl: RecaudacionSprl;
  public reprocesarRecaudacionSprl: IRecaudacionSprl; 

  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;

  constructor(    
    private formBuilder: FormBuilder,
    private generalService: GeneralService,
    private router: Router,
    private recaudacionSprlService: RecaudacionSprlService,
    private funciones: Funciones,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService
  ) {   
  }

  async ngOnInit() {
    this.fechaMaxima = new Date();
    this.construirFormulario();
    this.reprocesarRecaudacionSprl = await this.sharingInformationService.compartirRecaudacionSprlObservable.pipe(first()).toPromise();   
    if (!this.reprocesarRecaudacionSprl) {
      this.titulo = 'Nuevo';
      this.userCode = localStorage.getItem('user_code') || ''; 
      this.inicilializarListas();
      this.obtenerZonasRegistrales();
    } else {
      this.titulo = 'Reprocesar';
    }                
    this.recaudacionSprl = new RecaudacionSprl();        
  }

  private construirFormulario() {
    this.formRecaudacionSprl = this.formBuilder.group({
      zona: [''],
      oficina: [''],
      local: [''],        
      fecha: [this.fechaMaxima],
      docs: this.formBuilder.array([])
    });
  }

  private inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];     
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
  }

  private obtenerZonasRegistrales() {
    this.utilService.onShowProcessLoading("Cargando la data");
    this.generalService.getCbo_Zonas_Usuario( this.userCode).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (res: ZonaRegistral[]) => {  
      if (res.length === 1) {   
        this.listaZonaRegistral.push({ coZonaRegi: res[0].coZonaRegi, deZonaRegi: res[0].deZonaRegi });    
        this.formRecaudacionSprl.patchValue({ "zona": res[0].coZonaRegi});
        this.buscarOficinaRegistral();
      } else {    
        this.listaZonaRegistral.push(...res);
      }       
      this.utilService.onCloseLoading();
    }, (err: HttpErrorResponse) => {   
      this.utilService.onCloseLoading();                                  
    });
  }

  public buscarOficinaRegistral() { 
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formRecaudacionSprl.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.formRecaudacionSprl.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (res: OficinaRegistral[]) => {  
        if (res.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: res[0].coOficRegi, deOficRegi: res[0].deOficRegi });    
          this.formRecaudacionSprl.patchValue({ "oficina": res[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...res);
        }
        this.loadingOficinaRegistral = false;     
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formRecaudacionSprl.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.formRecaudacionSprl.get('zona')?.value,this.formRecaudacionSprl.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formRecaudacionSprl.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err: HttpErrorResponse) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  public obtenerDataFileExcel(event: IDataFileExcel) {
    console.log('file', event)
    if (event.servicioDePago === this.SERVICIO_NIUBIZ) {
      this.nameNiubiz = event.nameFile;
      this.recaudacionSprl.noAdju0001 = event.nameFile;
      this.recaudacionSprl.idGuidDocu01 = event.idGuid;
      this.recaudacionSprl.trama01 = event.lista;
	  
    } 
  }

  public async procesar() {
    let resultado: boolean = await this.validarFormularioNuevo();
    if (!resultado) {      
      return;
    }     
    this.utilService.onShowProcessSaveEdit(); 
    this.asignarValoresNuevo();
    this.recaudacionSprl.tiProc = '0'; // '0': es validar data, '1': es insertar
    this.recaudacionSprlService.procesarRecaudacionSprl(this.recaudacionSprl).pipe(      
      takeUntil(this.unsubscribe$), 
      filter((res1: IResponse) => { 
        if (res1.codResult < 0 ) {        
          this.utilService.onShowAlert("warning", "Atención", res1.msgResult);        
          return false;
        } else {          
          return true 
        } 
      }),     
      concatMap((res1: IResponse) => {   
        this.recaudacionSprl.tiProc = '1'; // '0': es validar data, '1': es insertar     
        return this.recaudacionSprlService.procesarRecaudacionSprl(this.recaudacionSprl) 
      })
    ).subscribe((res: IResponse) => {     
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {   
        this.parse();      
        this.respuestaExitosaGrabar('PROCESÓ');
      }         
    }, (err: HttpErrorResponse) => {
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("error", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();        
      }
    });
  }

  private async respuestaExitosaGrabar(texto: string) {    
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Se ${texto} satisfactoriamente el/los registro(s)`);
    if (respuesta.isConfirmed) {
      let dataFiltrosRecaudacionSprl: IDataFiltrosRecaudacionSprl = await this.sharingInformationService.compartirDataFiltrosRecaudacionSprlObservable.pipe(first()).toPromise(); 
      if (dataFiltrosRecaudacionSprl.esBusqueda) {
        dataFiltrosRecaudacionSprl.esCancelar = true;
        this.sharingInformationService.compartirDataFiltrosRecaudacionSprlObservableData = dataFiltrosRecaudacionSprl;
      }      
      this.router.navigate(['SARF/procesos/consolidacion/recaudacion_sprl_virtual/bandeja']);
    }
  }

  private async validarFormularioNuevo(): Promise<boolean> { 
    let registro = this.formRecaudacionSprl.getRawValue();
    if (!registro.zona || registro.zona === "*") {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una zona registral.");
      return false;      
    } else if (!registro.oficina || registro.oficina === "*") {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una oficina registral.");
      return false;      
    } else if (!registro.local || registro.local === "*") {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un local.");
      return false;      
    } else if (!registro.fecha) {
      let respuesta = await this.utilService.onShowAlertPromise("warning", "Atención", `Seleccione la fecha de emisión.`);
      if (respuesta.isConfirmed) {
        document.getElementById('idFecha').focus();
        return false; 
      } else {
        return true;
      }    
    } else if (this.recaudacionSprl.noAdju0001 === '') {
      this.utilService.onShowAlert("warning", "Atención", "Debe de adjuntar al menos 01 archivo de excel.");
      return false;  
    } else {
      return true;
    }
  }

  private asignarValoresNuevo() {    
    this.recaudacionSprl.idCncl = 0;
    this.recaudacionSprl.idTipoCncl = 5;
    this.recaudacionSprl.coZonaRegi = this.formRecaudacionSprl.get('zona').value;
    this.recaudacionSprl.coOficRegi = this.formRecaudacionSprl.get('oficina').value;
    this.recaudacionSprl.coLocaAten = this.formRecaudacionSprl.get('local').value;
    this.recaudacionSprl.idOperPos = 1;    
    this.recaudacionSprl.feCncl = this.funciones.convertirFechaDateToString(this.formRecaudacionSprl.get('fecha').value);
  }

  public async reprocesar() {
    let resultado: boolean = await this.validarFormularioReprocesar();
    if (!resultado) {      
      return;
    }     
    this.utilService.onShowProcessSaveEdit(); 
    this.asignarValoresReprocesar();
    this.recaudacionSprl.tiProc = '0'; // '0': es validar data, '1': es insertar
    this.recaudacionSprlService.reprocesaRecaudacionSprl(this.recaudacionSprl).pipe(      
      takeUntil(this.unsubscribe$), 
      filter((res1: IResponse) => { 
        if (res1.codResult < 0 ) {        
          this.utilService.onShowAlert("warning", "Atención", res1.msgResult);        
          return false;
        } else {          
          return true 
        } 
      }),     
      concatMap((res1: IResponse) => {   
        this.recaudacionSprl.tiProc = '1'; // '0': es validar data, '1': es insertar     
        return this.recaudacionSprlService.reprocesaRecaudacionSprl(this.recaudacionSprl) 
      })
    ).subscribe((res: IResponse) => {     
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {     
        this.parse();    
        this.respuestaExitosaGrabar('REPROCESÓ');

      }         
    }, (err: HttpErrorResponse) => {
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("error", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();        
      }
    });
  }

  private async validarFormularioReprocesar(): Promise<boolean> { 
    if (this.recaudacionSprl.noAdju0001 === '') {
      this.utilService.onShowAlert("warning", "Atención", "Debe de adjuntar al menos 01 archivo de excel.");
      return false;  
    } else {
      return true;
    }
  }

  private asignarValoresReprocesar() {    
    this.recaudacionSprl.idCncl = this.reprocesarRecaudacionSprl.idCncl;
    this.recaudacionSprl.idTipoCncl = 5;
    this.recaudacionSprl.coZonaRegi = this.reprocesarRecaudacionSprl.coZonaRegi;
    this.recaudacionSprl.coOficRegi = this.reprocesarRecaudacionSprl.coOficRegi;
    this.recaudacionSprl.coLocaAten = this.reprocesarRecaudacionSprl.coLocaAten;
    this.recaudacionSprl.idOperPos = 1;       
    this.recaudacionSprl.feCncl = (this.reprocesarRecaudacionSprl.feCncl) ? this.funciones.convertirFechaDateToString(new Date(this.reprocesarRecaudacionSprl.feCncl)) : '';
  }  

  public async cancelar() {
    let dataFiltrosRecaudacionSprl: IDataFiltrosRecaudacionSprl = await this.sharingInformationService.compartirDataFiltrosRecaudacionSprlObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionSprl.esBusqueda) {
      dataFiltrosRecaudacionSprl.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionSprlObservableData = dataFiltrosRecaudacionSprl;
    }
    this.router.navigate(['SARF/procesos/consolidacion/recaudacion_sprl_virtual/bandeja']);
  }

  public obtenerServicioSeleccionado(file: any) {
    // this.servicioOperador = servicio;   
    (this.formRecaudacionSprl.get('docs') as FormArray).push(
      this.formBuilder.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    ) 
  }

  public parse() {
    const docs = (this.formRecaudacionSprl.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto ) {
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: '',
          coZonaRegi: this.formRecaudacionSprl.get('zona').value,
          coOficRegi: this.formRecaudacionSprl.get('oficina').value
        })
      }
    });

    this.saveAdjuntoInFileServer();
  }

  public saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      console.log('PreFile', file);
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.utilService.onShowAlert("error", "Atención", data.msgResult);
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            // if ( this.sizeFiles == 0 ) {
            //   if ( value == 'P') {
            //     this.enviarDataProcesar();
            //   }
            //   else {
            //     this.enviarDataReprocesar();
            //   }
            // }
          }
        },
        error: ( e ) => {
          console.log( e );
          this.utilService.onShowAlert("error", "Atención", e.error.description);
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirRecaudacionSprlObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();    
  }

}

