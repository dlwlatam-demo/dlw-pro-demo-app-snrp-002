import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, first, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MessageService } from 'primeng/api';
import { SortEvent } from 'primeng/api';
import { 
  IConciliacionManual, 
  IDataFiltrosRecaudacionSprl, 
  IRecaudacionSprl, 
  IConciliacionManual2, 
  ITotalesNiubizSprl 
} from 'src/app/interfaces/consolidacion-sprl-virtual';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { RecaudacionSprlService } from 'src/app/services/procesos/consolidacion/recaudacion-sprl-virtual.service';
import { UtilService } from 'src/app/services/util.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { ConciliarPorMontoSprlComponent } from '../conciliar-por-monto/conciliar-por-monto-sprl.component';
import { environment } from 'src/environments/environment';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { DialogService } from 'primeng/dynamicdialog';
import { Router } from '@angular/router';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { GeneralService } from '../../../../../services/general.service';
import { ConciliarPorNiubizComponent } from '../conciliar-por-niubiz/conciliar-por-niubiz.component';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { IResConciliacionNIUBIZ, IConciliacionNIUBIZ } from '../../../../../interfaces/consolidacion-sprl-virtual';

@Component({
  selector: 'app-editar-recaudacion',
  templateUrl: './editar-recaudacion.component.html',
  styleUrls: ['./editar-recaudacion.component.scss'],
  providers: [ DialogService, MessageService ],
  encapsulation:ViewEncapsulation.None 
})
export class EditarRecaudacionSprlComponent extends BaseBandeja implements OnInit, OnDestroy {
 
  private unsubscribe$ = new Subject<void>(); 
  public titulo: string;
  public recaudacionSprl: IRecaudacionSprl;
  public esConsultarRegistro: boolean = false;
  public fecha: string = '';
  public nombreArchivoExcel: string = '';
  public idGuidExcel: string = '';
  public servicioOperador: string = '';
  public mostrarBloque: boolean = true;
  private tiMoviVarios: string = 'M';
  private tiMoviObservaciones: string = 'O';
  private reqs: Observable<any>[] = [];
  public listaTotalesNiubizSprl: ITotalesNiubizSprl[] = [];
  public listarConceptos!: IResponse2[];
  public currentPage: string = environment.currentPage;
  public loadingTotalesNiubizSprl: boolean = false;
  public onResetDivConciliar: boolean = false;
  public loadingConceptos: boolean = true;
  public validarConcepto!: boolean;
  public concepto: string = '';

  public item: any;
 
  reporteExcel!: any;
  nFile!: any;
  
  constructor(
    private fb: FormBuilder,
    private dialogService: DialogService,
    public funciones: Funciones,
    public recaudacionSprlService: RecaudacionSprlService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService ,
    private messageService: MessageService,
    private generalService: GeneralService
  ){ super() }

  async ngOnInit() {
    this.codBandeja = MenuOpciones.CD_Recaudacion_SPRL_Virtual;
    this.btnConsolidacionSPRLVirtual();
    
    this.esConsultarRegistro = await this.sharingInformationService.compartirEsConsultarSprlObservable.pipe(first()).toPromise()
    this.recaudacionSprl = await this.sharingInformationService.compartirRecaudacionSprlObservable.pipe(first()).toPromise();
    this.titulo = (!this.esConsultarRegistro) ? 'Editar' : 'Consultar';
    this.fecha = this.funciones.convertirFechaDateToString(new Date (this.recaudacionSprl.feCncl));
    this.servicioOperador = SERVICIO_DE_PAGO.NIUBIZ;
    this.nombreArchivoExcel = this.recaudacionSprl.noAdju0001;
    this.idGuidExcel = this.recaudacionSprl.idGuidDocu01;

    this.listarConceptos = [];
    this.listarConceptos.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' });

    this.obtenerListados();  
    this._obtenerConcepto();
  }

  private obtenerListados() { 
    this.utilService.onShowProcessLoading("Cargando la data");
    this.reqs.push(
      this.recaudacionSprlService.getOperacionesTotalNiubiz(this.recaudacionSprl.idCncl).pipe(
        catchError((err) => {        
          if (err.error.category === "NOT_FOUND") {
            return of([])        
          } else {
            return of(undefined)
          } 
        })          
      )
    );
    forkJoin(this.reqs).pipe(takeUntil(this.unsubscribe$)).subscribe((result: any[]) => {     
      this.listaTotalesNiubizSprl = result[0]; 
      this.utilService.onCloseLoading();
    });
  }

  private _obtenerConcepto() {
    this.generalService.getCbo_DescargarConceptos().pipe( takeUntil( this.unsubscribe$ ) )
      .subscribe({
        next: ( data ) => {
          this.loadingConceptos = false;

          if ( data.length === 1 ) {
            this.listarConceptos.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });
            this.concepto = data[0].ccodigoHijo;
          }
          else {
            this.listarConceptos.push( ...data );
          }
        },
        error: ( _err ) => {
          this.loadingConceptos = false;
        }
      });
  }

  public async obtenerDataConciliarMonto(conciliacionManual1: IConciliacionManual) {
    
    this.utilService.onShowProcessLoading("Cargando la data"); 
    conciliacionManual1.idCncl = this.recaudacionSprl.idCncl;
    let conciliacionManual: IConciliacionManual2;
    await this.recaudacionSprlService.getConciliacionesSPRL(conciliacionManual1.idCncl, conciliacionManual1.nuSecu).pipe(first()).toPromise()
    .then((res: IConciliacionManual2) => {
      conciliacionManual = res;         
    }).catch(() => { 
      this.utilService.onShowMessageErrorSystem();
      return;   
    }); 

    if (conciliacionManual == null) { 
      this.utilService.onShowAlert("warning", "Atención", "Registro no encontrado");          
      return;
    } 
	  this.utilService.onCloseLoading();
    const dialog = this.dialogService.open(ConciliarPorMontoSprlComponent, {
      closable: false,
      data: {
        conciliacionManual      
      },  
      header: 'SPRL Virtual - Conciliación Manual',   
      width: '50%'        
    });
    dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {      
      // this.onResetDivConciliar = true;      
    });     
  }

  public async obtenerDataConciliarNiubiz( niubiz: IConciliacionNIUBIZ ) {
    this.utilService.onShowProcessLoading("Cargando la data"); 
    niubiz.idCncl = this.recaudacionSprl.idCncl;
    
    let conciliacionNIUBIZ: IResConciliacionNIUBIZ;
    
    await this.recaudacionSprlService.getConciliacionesNIUBIZ(niubiz.idCncl, niubiz.nuFila).pipe(first()).toPromise()
    .then((res: IResConciliacionNIUBIZ) => {

      conciliacionNIUBIZ = res;
    }).catch(() => { 
      this.utilService.onShowMessageErrorSystem();
      return;   
    });

    if (conciliacionNIUBIZ.codResult < 0) { 
      this.utilService.onShowAlert("warning", "Atención", conciliacionNIUBIZ.msgResult);          
      return;
    }

    this.utilService.onCloseLoading();

    const dialog = this.dialogService.open( ConciliarPorNiubizComponent, {
      header: 'Recaudación SPRL Virtual - Conciliación Manual',
      width: '50%',
      closable: false,
      data: {
        niubiz: conciliacionNIUBIZ,
        id: this.recaudacionSprl.idCncl
      }
    });

    dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {      
      // this.onResetDivConciliar = true;      
    });
  }

  public onChangeConcepto() {
    this.validarConcepto = false;
  }

  public downloadFile() {
    console.log( this.concepto )
    if ( this.concepto == '*' ) {
      this.validarConcepto = true;
      return;
    }

    const index = this.listarConceptos.findIndex( ( i: IResponse2 ) => i.ccodigoHijo == this.concepto );
    const name = this.listarConceptos[index].cdescri;
    
    this.nFile = `Recaudación_SPRL_NIUBIZ_${ name.replace(/ /g, '').trim() }`
    // switch( this.concepto ) {
    // 	case '0': this.nFile = "Recaudación_SPRL_NIUBIZ_NoConciliados";
    // 	break;
    // 	case '1': this.nFile = "Recaudación_SPRL_NIUBIZ_Conciliados";
    // 	break;
    // 	case '2': this.nFile = "Recaudación_SPRL_NIUBIZ_Anulaciones";
    // 	break;
    // 	case '3': this.nFile = "Recaudación_SPRL_NIUBIZ_Contracargos";
    // 	break;
    // 	default: this.nFile = "Recaudación_SPRL_NIUBIZ_NoDefinido";
    // }
    
    this.obtenerArchivoNiubiz(this.recaudacionSprl.idCncl, Number( this.concepto ));
  }
  
  
  obtenerArchivoNiubiz(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionSprlService.getRecaudacionNiubiz( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        let data1: any = data;		
        this.reporteExcel = data1.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		    this.exportExcel();
        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      },
      complete: () => {
        Swal.close();
      }
    });
  }
  
  
  public downloadFileTx(tipo: number) {
	switch(tipo) {
		case 0: this.nFile = "Recaudación_SPRL_NoConciliados";
		break;
		case 1: this.nFile = "Recaudación_SPRL_Conciliados";
		break;
		case 2: this.nFile = "Recaudación_SPRL_ConciliadosManualmente";
		break;
		default: this.nFile = "Recaudación_SPRL_NoDefinido";
	}
	this.obtenerArchivoSPRL(this.recaudacionSprl.idCncl, tipo);
  }
  
  

  obtenerArchivoSPRL(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionSprlService.getTransaccionesSPRL( idCncl, inCncl ).subscribe({
      next: ( data ) => {
	  
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		this.exportExcel();

        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  
  public handleChange(e: any) {  
    if (e.index === 0) {
      this.servicioOperador = SERVICIO_DE_PAGO.NIUBIZ;
      this.nombreArchivoExcel = this.recaudacionSprl.noAdju0001;
      this.idGuidExcel = this.recaudacionSprl.idGuidDocu01;
      this.mostrarBloque = true;
    } else {
      this.mostrarBloque = false;
    }
  }

  numberTransform( value: string ): string {
    return Number( value ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  public async cancelar() {
    let dataFiltrosRecaudacionSprl: IDataFiltrosRecaudacionSprl = await this.sharingInformationService.compartirDataFiltrosRecaudacionSprlObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionSprl.esBusqueda) {
      dataFiltrosRecaudacionSprl.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionSprlObservableData = dataFiltrosRecaudacionSprl;
    }
    this.router.navigate(['SARF/procesos/consolidacion/recaudacion_sprl_virtual/bandeja']);
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirEsConsultarSprlObservableData = false;
    this.sharingInformationService.compartirRecaudacionSprlObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();      
  }


  exportExcel() {
    console.log('Exportar Excel');

    const doc = this.reporteExcel;
	const nFile = this.nFile;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', nFile);
    document.body.appendChild( download );
    download.click();
  }


}
