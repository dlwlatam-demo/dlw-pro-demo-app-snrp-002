import { Component, Input, Output, OnInit, OnChanges, ViewEncapsulation, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { IConciliacionManual } from 'src/app/interfaces/consolidacion-sprl-virtual';
import { OPERACION_SCUNAC_NO_CONCILAIDAS } from 'src/app/models/enum/parameters';
import { UtilService } from 'src/app/services/util.service';
import { BaseBandeja } from '../../../../../../base/base-bandeja.abstract';
@Component({
  selector: 'app-div-conciliar-por-monto-sprl',
  templateUrl: './div-conciliar-por-monto-sprl.component.html',
  styleUrls: ['./div-conciliar-por-monto-sprl.component.scss'],
  encapsulation:ViewEncapsulation.None 
})
export class DivConciliarPorMontoSprlComponent extends BaseBandeja implements OnInit, OnChanges {  

  @Input() bandeja!: number;
  @Input() btnSPRL!: number;
  @Input() onReset: boolean = false;
  @Output() eventDataConciliarMonto = new EventEmitter<IConciliacionManual>(); 
  
  public form: FormGroup;
  public nuSecu: number = null;
  public igualMonto: number = OPERACION_SCUNAC_NO_CONCILAIDAS.CON_IGUAL_MONTO;
  public menorIgualMonto: number  = OPERACION_SCUNAC_NO_CONCILAIDAS.CON_MENOR_O_IGUAL_MONTO; 
  public ConciliacionManual: IConciliacionManual;

  constructor(
    private formBuilder: FormBuilder,
    public funciones: Funciones,   
    private utilService: UtilService
  ) { super() }

  ngOnInit(): void {
    this.construirFormulario();
  }

  private construirFormulario() {
    this.form = this.formBuilder.group({
      nuSecu: [''],
      opcionOperacion: [this.igualMonto],         
    });
  }

  public conciliarPorMonto() {    
    if (this.form.get('nuSecu').value === null || this.form.get('nuSecu').value === '') {
      document.getElementById('inputItem').focus(); 
      this.utilService.onShowAlert("warning", "Atención", "Ingrese el nuSecu.");
      return;
    } 
    this.ConciliacionManual = {  
		idCncl: null,
		nuSecu: this.form.get('nuSecu').value,
		feCncl: null,
		idUsua: null,
		razSoc: null,
		primerApellido: null,
		segundoApellido: null,
		nombres: null,	
		monto: null,
		idPagoLinea: null,
		eTicket: null,
		inCncl: null,
		obCnclSprl: null
    }   
    this.eventDataConciliarMonto.emit(this.ConciliacionManual);   
  }

  ngOnChanges() {
    if (this.onReset) {
      this.form.reset();
    }
  }

}
