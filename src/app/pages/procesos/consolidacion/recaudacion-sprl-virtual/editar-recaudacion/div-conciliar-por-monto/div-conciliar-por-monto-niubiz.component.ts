import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Funciones } from '../../../../../../core/helpers/funciones/funciones';

import { UtilService } from '../../../../../../services/util.service';
import { IConciliacionNIUBIZ } from '../../../../../../interfaces/consolidacion-sprl-virtual';
import { BaseBandeja } from '../../../../../../base/base-bandeja.abstract';

@Component({
  selector: 'app-div-conciliar-por-monto-niubiz',
  templateUrl: './div-conciliar-por-monto-niubiz.component.html',
  styleUrls: ['./div-conciliar-por-monto-niubiz.component.scss']
})
export class DivConciliarPorMontoNiubizComponent extends BaseBandeja implements OnInit {

  @Input() bandeja!: number;
  @Input() btnNIUBIZ!: number;
  @Input() onReset: boolean = false;
  @Output() eventDataConciliarMonto = new EventEmitter<IConciliacionNIUBIZ>();

  public form: FormGroup;
  public ConciliacionManual: IConciliacionNIUBIZ;

  constructor(
    private fb: FormBuilder,
    public funciones: Funciones,
    private utilService: UtilService
  ) { super() }

  ngOnInit(): void {
    this.form = this.fb.group({
      nuFila: [''],
      opcionOperacion: [0],         
    });
  }

  public conciliarPorMonto() {
    if (this.form.get('nuFila').value === null || this.form.get('nuFila').value === '') {
      document.getElementById('inputItem').focus(); 
      this.utilService.onShowAlert("warning", "Atención", "Ingrese el número de fila.");
      return;
    }

    this.ConciliacionManual = {
      idCncl: null,
      nuFila: this.form.get('nuFila')?.value,
      nuRuc: null,
      noRazoSoci: null,
      coCome: null,
      noCome: null,
      feOper: null,
      feDepo: null,
      noProd: null,
      noTipoOper: null,
      nuTarj: null,
      inOrigTarj: null,
      deTipoTarj: null,
      noMarcTarj: null,
      noMone: null,
      imOper: null,
      deEsDcc: null,
      imDcc: null,
      imComiTota: null,
      imComiNiub: null,
      im_igv: null,
      imSumaDepo: null,
      deEsta: null,
      idOper: null,
      coCuenBancPaga: null,
      noBancPaga: null,
      nuVouc: null,
      nuAuth: null,
      inCncl: null,
      deEstaCncl: null,
      obCnclNiub: null
  }
    this.eventDataConciliarMonto.emit(this.ConciliacionManual);
  }

  ngOnChanges() {
    if (this.onReset) {
      this.form.reset();
    }
  }

}
