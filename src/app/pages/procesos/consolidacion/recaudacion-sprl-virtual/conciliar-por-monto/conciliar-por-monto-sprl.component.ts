import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { SortEvent } from 'primeng/api';
import { IConciliacionManual, IConciliacionManual2 } from 'src/app/interfaces/consolidacion-sprl-virtual';
import { RecaudacionSprlService } from 'src/app/services/procesos/consolidacion/recaudacion-sprl-virtual.service';
import { UtilService } from 'src/app/services/util.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { GeneralService } from 'src/app/services/general.service';
import { ConciliacionManual } from 'src/app/models/procesos/consolidacion/recaudacion-sprl-virtual.model';
import { IResponse, IResponse2 } from 'src/app/interfaces/general.interface';

@Component({
  selector: 'app-conciliar-por-monto-sprl',
  templateUrl: './conciliar-por-monto-sprl.component.html',
  styleUrls: ['./conciliar-por-monto-sprl.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class ConciliarPorMontoSprlComponent implements OnInit, OnDestroy {

  unsubscribe$ = new Subject<void>(); 
  conciliacionManual: IConciliacionManual;
  bodyConciliacionManual: ConciliacionManual;
  currentPage: string = environment.currentPage;
  loadingConciliacion: boolean = false;
  readonly: boolean = false;
  listaEstado: IResponse2[];
  listaEstadoFiltro: IResponse2[];
  formRecaudacionSprl: FormGroup;

  razSocNombre: string = "";

  constructor(
    public config: DynamicDialogConfig, 
    public funciones: Funciones,
    private formBuilder: FormBuilder,  
    private generalService: GeneralService,
    private dialogRef: DynamicDialogRef,
    private recaudacionSprlService: RecaudacionSprlService,
    private utilService: UtilService
  ) { }

  ngOnInit(): void { 
	  this.listaEstado = []; 
	  this.construirFormulario();	
	  this.obtenerListados();
    this.conciliacionManual = new ConciliacionManual();    
    this.setearConciliacionManual(this.config.data.conciliacionManual);
  } 

  private construirFormulario() {
    this.formRecaudacionSprl = this.formBuilder.group({
      estado: [''],   
      obCnclSprl: ['']              
    });
  }
  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_EstadoSprl()      
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resEstado]) => {
      this.listaEstado.push(...resEstado);        
	    this.listaEstadoFiltro = [...this.listaEstado];    

      if ( this.conciliacionManual.inCncl != '2' )
        this.readonly = true;
      else
        this.readonly = false;
      
      this.utilService.onCloseLoading();
      let estado = this.listaEstado.findIndex(x => x.ccodigoHijo == this.conciliacionManual.inCncl);
      this.formRecaudacionSprl.patchValue({ "estado": resEstado[estado].ccodigoHijo });

	  
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }
  
  private setearConciliacionManual(data: IConciliacionManual2) {
    console.log('conci', data );   
    this.conciliacionManual.idCncl = data.idCncl;
    this.conciliacionManual.nuSecu = data.nuSecu;
    this.conciliacionManual.feCncl = data.feCncl;

    this.conciliacionManual.idUsua = data.idUsua;
    this.conciliacionManual.razSoc = data.razSoc;
    this.conciliacionManual.primerApellido = data.primerApellido;
    this.conciliacionManual.segundoApellido = data.segundoApellido;
    this.conciliacionManual.nombres = data.nombres;
    this.conciliacionManual.monto = (data.monto);
    this.conciliacionManual.idPagoLinea = data.idPagoLinea;
    this.conciliacionManual.eTicket = data.eticket;	
    this.conciliacionManual.inCncl = data.inCncl;
    this.conciliacionManual.obCnclSprl = data.obCnclSprl;
	  this.formRecaudacionSprl.patchValue({ "obCnclSprl": data.obCnclSprl});

    if(this.conciliacionManual.razSoc !== null && this.conciliacionManual.razSoc.trim() !== "") {
      this.razSocNombre = this.conciliacionManual.razSoc;
    }
    else {
      this.razSocNombre = this.conciliacionManual.primerApellido + ' ' + this.conciliacionManual.segundoApellido + ', ' + this.conciliacionManual.nombres;
    }
	
  }

  public seleccionarEstado(event: any) {
    if ( event.value != '2' )
      this.readonly = true;
    else
      this.readonly = false;
  }

  public grabar() {
    this.utilService.onShowProcessLoading("Procesando la data");  
    this.asignarValores();
    this.recaudacionSprlService.saveConciliarSPRL(this.bodyConciliacionManual).pipe(takeUntil(this.unsubscribe$))
    .subscribe((res: IResponse) => {    
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {         
        this.utilService.onShowAlert("success", "Atención", `Se grabó satisfactoriamente el/los registro(s).`);
        this.dialogRef.close();       
      }         
    }, (err: HttpErrorResponse) => {
      this.utilService.onShowMessageErrorSystem();
    });
  }

  private asignarValores() {
    this.bodyConciliacionManual = new ConciliacionManual();
    this.bodyConciliacionManual.idCncl = this.conciliacionManual.idCncl;
    this.bodyConciliacionManual.nuSecu = this.conciliacionManual.nuSecu;
    this.bodyConciliacionManual.inCncl = this.formRecaudacionSprl.get('estado')?.value;
    this.bodyConciliacionManual.obCnclSprl = this.formRecaudacionSprl.get('obCnclSprl')?.value;
  }

  public cancelar() {
    this.dialogRef.close();
  }  

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();                      
  }

}
