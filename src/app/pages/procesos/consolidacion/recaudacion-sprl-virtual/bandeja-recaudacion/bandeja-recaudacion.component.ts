import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent } from 'primeng/api';
import { forkJoin, Subject } from 'rxjs';
import { concatMap, filter, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { UtilService } from 'src/app/services/util.service';
import { GeneralService } from 'src/app/services/general.service';
import { RecaudacionSprlService } from 'src/app/services/procesos/consolidacion/recaudacion-sprl-virtual.service';
import { FiltroRecaudacionSprl, RecaudacionSprl2 } from 'src/app/models/procesos/consolidacion/recaudacion-sprl-virtual.model';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { IDataFiltrosRecaudacionSprl, IRecaudacionSprl, IOperacionScunac, IPostOperacionScunac, IDetalleOperacionScunac } from 'src/app/interfaces/consolidacion-sprl-virtual';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { ESTADO_REGISTRO_2 } from 'src/app/models/enum/parameters';
import { IResponse, IResponse2 } from 'src/app/interfaces/general.interface';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';

@Component({
  selector: 'app-bandeja-recaudacion',
  templateUrl: './bandeja-recaudacion.component.html',
  styleUrls: ['./bandeja-recaudacion.component.scss'],
  providers: [
    DialogService  
  ]
})
export class BandejaRecaudSprlComponent extends BaseBandeja implements OnInit, OnDestroy { 
  
  unsubscribe$ = new Subject<void>();
  userCode: any;
  currentPage: string = environment.currentPage;
  formRecaudacionSprl: FormGroup;
  fechaMinima: Date;
  fechaMaxima: Date; 
  listaZonaRegistral: ZonaRegistral[];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaOperador: IResponse2[];
  listaEstado: IResponse2[];
  listaRecaudacion: IRecaudacionSprl[];
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  selectedValuesRecaudacion: IRecaudacionSprl[] = [];
  loadingRecaudacion: boolean = false;
  filtroRecaudacionSprl: FiltroRecaudacionSprl;
  estadoRegistro = ESTADO_REGISTRO_2;  

  //*************** listados para el filtro ***************/
  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaOperadorFiltro: IResponse2[]; 
  listaEstadoFiltro: IResponse2[];
  esBusqueda: boolean = false;  

  constructor( 
    private dialogService: DialogService,
    private formBuilder: FormBuilder,  
    private generalService: GeneralService,
    private funciones: Funciones, 
    private recaudacionSprlService: RecaudacionSprlService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService,    
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.CD_Recaudacion_SPRL_Virtual;
    this.btnConsolidacionSPRLVirtual();

    this.fechaMaxima = new Date();
    this.fechaMinima = new Date(this.fechaMaxima.getFullYear(), this.fechaMaxima.getMonth(), 1);  
    this.userCode = localStorage.getItem('user_code')||'';   
    this.construirFormulario();

    this.sharingInformationService.compartirDataFiltrosRecaudacionSprlObservable.pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: IDataFiltrosRecaudacionSprl) => {      
      if (data.esCancelar && data.bodyRecaudacionSprl !== null) {    
        this.utilService.onShowProcessLoading("Cargando la data");
        this.setearCamposFiltro(data);
        this.buscarRecaudacion(false);
      } else {
        this.inicilializarListas();
        this.obtenerListados();
      }
    });
  }

  private construirFormulario() {
    this.formRecaudacionSprl = this.formBuilder.group({
      zona: [''],
      oficina: [''],
      local: [''],
      operador: [''],    
      estado: [''],   
      fechaDesde: [this.fechaMinima],
      fechaHasta: [this.fechaMaxima]              
    });
  }

  private setearCamposFiltro(dataFiltro: IDataFiltrosRecaudacionSprl) {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOperador = [];
    this.listaEstado = [];    
    this.listaZonaRegistral = dataFiltro.listaZonaRegistral; 
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaOperador = dataFiltro.listaOperador;
    this.listaEstado = dataFiltro.listaEstado;    
    this.filtroRecaudacionSprl = dataFiltro.bodyRecaudacionSprl;
    this.formRecaudacionSprl.patchValue({ "zona": this.filtroRecaudacionSprl.coZonaRegi});
    this.formRecaudacionSprl.patchValue({ "oficina": this.filtroRecaudacionSprl.coOficRegi});
    this.formRecaudacionSprl.patchValue({ "local": this.filtroRecaudacionSprl.coLocaAten});
    this.formRecaudacionSprl.patchValue({ "operador": this.filtroRecaudacionSprl.idOperPos});
    this.formRecaudacionSprl.patchValue({ "estado": this.filtroRecaudacionSprl.esDocu});
    this.formRecaudacionSprl.patchValue({ "fechaDesde": (this.filtroRecaudacionSprl.feDesd !== '' && this.filtroRecaudacionSprl.feDesd !== null) ? this.funciones.convertirFechaStringToDate(this.filtroRecaudacionSprl.feDesd) : this.filtroRecaudacionSprl.feDesd});
    this.formRecaudacionSprl.patchValue({ "fechaHasta": (this.filtroRecaudacionSprl.feHast !== '' && this.filtroRecaudacionSprl.feHast !== null) ? this.funciones.convertirFechaStringToDate(this.filtroRecaudacionSprl.feHast) : this.filtroRecaudacionSprl.feHast});    
  }

  private inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOperador = [];
    this.listaEstado = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' });
    this.listaOperador.push({ ccodigoHijo: '0', cdescri: '(TODOS)' });  
    this.listaEstado.push({ ccodigoHijo: '', cdescri: '(TODOS)' });   
  }

  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_Zonas_Usuario( this.userCode),     
      this.generalService.getCbo_EstadoRecaudacion(),
      this.generalService.getCbo_OperadorSPRL()   
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resZonaRegistral, resEstado, resOperador]) => { 
      if (resZonaRegistral.length === 1) {   
        this.listaZonaRegistral.push({ coZonaRegi: resZonaRegistral[0].coZonaRegi, deZonaRegi: resZonaRegistral[0].deZonaRegi });    
        this.formRecaudacionSprl.patchValue({ "zona": resZonaRegistral[0].coZonaRegi});
        this.buscarOficinaRegistral();
      } else {    
        this.listaZonaRegistral.push(...resZonaRegistral);
      }

      if ( resOperador.length === 1 ) {
        this.listaOperador.push({ ccodigoHijo: resOperador[0].ccodigoHijo, cdescri: resOperador[0].cdescri });
        this.formRecaudacionSprl.patchValue({ 'operador': resOperador[0].ccodigoHijo });
      } else {
        this.listaOperador.push(...resOperador);
      }

      this.listaEstado.push(...resEstado);        
      this.buscarRecaudacion(true);    
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }

  private establecerBodyRecaudacionSprl() {       
    this.filtroRecaudacionSprl = new FiltroRecaudacionSprl();
    this.filtroRecaudacionSprl.coZonaRegi = (this.formRecaudacionSprl.get('zona')?.value === '*') ? '' : this.formRecaudacionSprl.get('zona')?.value;    
    this.filtroRecaudacionSprl.coOficRegi = (this.formRecaudacionSprl.get('oficina')?.value === '*') ? '' : this.formRecaudacionSprl.get('oficina')?.value;    
    this.filtroRecaudacionSprl.coLocaAten = (this.formRecaudacionSprl.get('local')?.value === '*') ? '' : this.formRecaudacionSprl.get('local')?.value;        
    this.filtroRecaudacionSprl.idOperPos = (this.formRecaudacionSprl.get('operador')?.value === '0') ? 0 : this.formRecaudacionSprl.get('operador')?.value;
    this.filtroRecaudacionSprl.esDocu = (this.formRecaudacionSprl.get('estado')?.value === '*') ? '' : this.formRecaudacionSprl.get('estado')?.value;
    this.filtroRecaudacionSprl.feDesd = (this.formRecaudacionSprl.get('fechaDesde')?.value !== "" && this.formRecaudacionSprl.get('fechaDesde')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formRecaudacionSprl.get('fechaDesde')?.value) : ""; 
    this.filtroRecaudacionSprl.feHast = (this.formRecaudacionSprl.get('fechaHasta')?.value !== "" && this.formRecaudacionSprl.get('fechaHasta')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formRecaudacionSprl.get('fechaHasta')?.value): "";       
  }

  public buscarRecaudacion(esCargaInicial: boolean) {
    let resultado: boolean = this.validarCamporFiltros();
    if (!resultado) {
      return;
    }
    this.listaRecaudacion = [];
    this.selectedValuesRecaudacion = [];    
    this.loadingRecaudacion = (esCargaInicial) ? false : true;
    this.guardarListadosParaFiltro();     
    this.establecerBodyRecaudacionSprl();
    this.esBusqueda = (esCargaInicial) ? false : true; 
    this.recaudacionSprlService.getBandejaConciliacion(this.filtroRecaudacionSprl).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (data: any) => {
      this.listaRecaudacion = data.lista;
      this.loadingRecaudacion = false;
      this.utilService.onCloseLoading();       
    }, (err: HttpErrorResponse) => {   
      this.loadingRecaudacion = false;
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("warning", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();
      }                             
    });
  }

  private validarCamporFiltros(): boolean {
    if (
      (this.formRecaudacionSprl.get('fechaDesde')?.value !== "" && this.formRecaudacionSprl.get('fechaDesde')?.value !== null) &&
      (this.formRecaudacionSprl.get('fechaHasta')?.value === "" || this.formRecaudacionSprl.get('fechaHasta')?.value === null)
    ) {
      document.getElementById('idFechaHasta').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de fin.");
      return false; 
    } else if (
      (this.formRecaudacionSprl.get('fechaDesde')?.value === "" || this.formRecaudacionSprl.get('fechaDesde')?.value === null) &&
      (this.formRecaudacionSprl.get('fechaHasta')?.value !== "" && this.formRecaudacionSprl.get('fechaHasta')?.value !== null)
    ) {
      document.getElementById('idFechaDesde').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de inicio.");
      return false; 
    } else {
      return true;
    }   
  }

  public buscarOficinaRegistral() { 
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formRecaudacionSprl.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.formRecaudacionSprl.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.formRecaudacionSprl.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formRecaudacionSprl.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.formRecaudacionSprl.get('zona')?.value,this.formRecaudacionSprl.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formRecaudacionSprl.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err: HttpErrorResponse) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  public cambioFechaInicio() {   
    if (this.formRecaudacionSprl.get('fechaHasta')?.value !== '' && this.formRecaudacionSprl.get('fechaHasta')?.value !== null) {
      if (this.formRecaudacionSprl.get('fechaDesde')?.value > this.formRecaudacionSprl.get('fechaHasta')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.formRecaudacionSprl.controls['fechaDesde'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.formRecaudacionSprl.get('fechaDesde')?.value !== '' && this.formRecaudacionSprl.get('fechaDesde')?.value !== null) {
      if (this.formRecaudacionSprl.get('fechaHasta')?.value < this.formRecaudacionSprl.get('fechaDesde')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.formRecaudacionSprl.controls['fechaHasta'].reset();    
      }      
    }
  }

  public nuevo() {  
    this.router.navigate(['SARF/procesos/consolidacion/recaudacion_sprl_virtual/nuevo']);
    this.sharingInformationService.irRutaBandejaRecaudacionSprlObservableData = false;
  }

  public editar() {   
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      if (!(this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.REPROCESADO)) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede EDITAR un registro en estado ABIERTO o REPROCESADO");
      } else {
        this.sharingInformationService.compartirRecaudacionSprlObservableData = this.selectedValuesRecaudacion[0]; 
        this.router.navigate(['SARF/procesos/consolidacion/recaudacion_sprl_virtual/editar']);
        this.sharingInformationService.irRutaBandejaRecaudacionSprlObservableData = false;    
      }
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }             
  }

  public anular() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosAnulados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ANULAR el registro en estado ABIERTO o REPROCESADO.");
    } else {
      let textoAnular = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ANULAR ${textoAnular}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('anular');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyAnularRecaudacion = new RecaudacionSprl2();
          bodyAnularRecaudacion.trama = lista;
          this.recaudacionSprlService.anulaRecaudacionSprl(bodyAnularRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosAnulados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.REPROCESADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public activar() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosActivados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ACTIVAR el registro en estado ANULADO.");
    } else {
      let textoActivar = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ACTIVAR ${textoActivar}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('activar');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyActivarrRecaudacion = new RecaudacionSprl2();
          bodyActivarrRecaudacion.trama = lista;
          this.recaudacionSprlService.activateRecaudacionSprl(bodyActivarrRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosActivados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ANULADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public abrirConciliacion() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosAbiertos()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ABRIR el registro en estado CERRADO.");
    } else {
      let textoAbrir = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ABRIR ${textoAbrir}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('abrir');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyAbrirRecaudacion = new RecaudacionSprl2();
          bodyAbrirRecaudacion.trama = lista;
          this.recaudacionSprlService.openRecaudacionSprl(bodyAbrirRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosAbiertos(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.CERRADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public cerrarConciliacion() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosCerrados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede CERRAR el registro en estado ABIERTO o REPROCESADO.");
    } else {
      let textoAbrir = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea CERRAR ${textoAbrir}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('cerrar');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyCerrarRecaudacion = new RecaudacionSprl2();
          bodyCerrarRecaudacion.trama = lista;
          this.recaudacionSprlService.closeRecaudacionSprl(bodyCerrarRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {                 
              const listado = lista.map(item => this.operacionesScunac(parseInt(item)).catch((error) => { }));
              Promise.all(lista).then( respuesta => {             
                this.buscarRecaudacion(false);
                this.selectedValuesRecaudacion = [];          
              });
            }             
          }, (err: HttpErrorResponse) => {      
          });
        }
      });
    }
  }

  private operacionesScunac(idCncl: number) {
    return new Promise<any>(
      (resolve, reject) => {               
        this.recaudacionSprlService.getOperacionesScunac(idCncl, 2)
          .pipe(            
            takeUntil(this.unsubscribe$),
            filter((data: any) => {           
              if (data.body.lstOperacionScunac.length > 0) {
                return true;
              } else {               
                return false;
              }
            }),
            concatMap((res: any) => {           
              let listaOperacionScunac: IOperacionScunac[] = res.body.lstOperacionScunac;      
              return this.recaudacionSprlService.actualizarIdOperacionScunac(this.obtenerOperacionScunac(listaOperacionScunac))              
            })
          ).subscribe( (res: IResponse) => {
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
              reject(new Error("Error de ejecución . . . :( "));
            } else if (res.codResult === 0) { 
              resolve(idCncl);                     
            }                                                                     
          }, () => {
            reject(new Error("Error de ejecución . . . :( ")); 
          }
        ) 
      }
    );
  }

  private obtenerOperacionScunac(lista: IOperacionScunac[]): IPostOperacionScunac {
    let data: IPostOperacionScunac;
    let listaDetalle: IDetalleOperacionScunac[] = [];
    for (let i = 0; i < lista.length; i++) {
      let item: IDetalleOperacionScunac = {
        aaMvto: lista[i].anioMovimiento,
        coZonaRegi: lista[i].coZonaRegi,
        nuMvto: lista[i].numeroMovimiento,
        nuSecuDeta: lista[i].nuSecuDeta,
        idTransPos: lista[i].id        
      }
      listaDetalle.push(item);      
    }
    data = {
      trama: listaDetalle
    } 
    return data;
  }

  private validarRegistrosCerrados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.REPROCESADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public reProcesar() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      if (!(this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.REPROCESADO)) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede REPROCESAR un registro en estado ABIERTO o REPROCESADO");
      } else {
        this.sharingInformationService.compartirRecaudacionSprlObservableData = this.selectedValuesRecaudacion[0]; 
        this.router.navigate(['SARF/procesos/consolidacion/recaudacion_sprl_virtual/reprocesar']);
        this.sharingInformationService.irRutaBandejaRecaudacionSprlObservableData = false; 
      }           
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }
  }

  public consultarRegistro() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      this.sharingInformationService.compartirEsConsultarSprlObservableData = true; 
      this.sharingInformationService.compartirRecaudacionSprlObservableData = this.selectedValuesRecaudacion[0];
      this.router.navigate(['SARF/procesos/consolidacion/recaudacion_sprl_virtual/consultar']);
      this.sharingInformationService.irRutaBandejaRecaudacionSprlObservableData = false;      
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }
  }

  customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;
        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
        return (event.order! * result);
    });
  }

  private guardarListadosParaFiltro() {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaOperadorFiltro = [...this.listaOperador];
    this.listaEstadoFiltro = [...this.listaEstado];    
  }

  private establecerDataFiltrosRecaudacionME() : IDataFiltrosRecaudacionSprl {
    let dataFiltrosRecaudacionME: IDataFiltrosRecaudacionSprl = {
      listaZonaRegistral: this.listaZonaRegistralFiltro, 
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaOperador: this.listaOperadorFiltro,     
      listaEstado: this.listaEstadoFiltro,
      bodyRecaudacionSprl: this.filtroRecaudacionSprl,   
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosRecaudacionME;
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();   
    this.sharingInformationService.compartirDataFiltrosRecaudacionSprlObservableData = this.establecerDataFiltrosRecaudacionME();                    
  }

}
