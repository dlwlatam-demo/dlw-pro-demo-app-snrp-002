import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MessageService } from 'primeng/api';
import { SortEvent } from 'primeng/api';
import { HttpErrorResponse } from '@angular/common/http';
import { 
  IDataFiltrosRecaudacionScunacRemesas, 
  IRecaudacionScunacRemesas, 
  ITotalesNiubizScunacRemesas 
} from 'src/app/interfaces/consolidacion-scunac-remesas';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { RecaudacionScunacRemesasService } from 'src/app/services/procesos/consolidacion/recaudacion-scunac-remesas.service';
import { UtilService } from 'src/app/services/util.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { environment } from 'src/environments/environment';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { DialogService } from 'primeng/dynamicdialog';
import { Router } from '@angular/router';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { GeneralService } from '../../../../../services/general.service';
import { OtrasOperacionesHermesComponent } from '../otras-operaciones-hermes/otras-operaciones-hermes.component';
import { IOtrosOperHermes } from '../../../../../interfaces/consolidacion-scunac-remesas';
import { BodyAnularActivarOtrosHermes } from '../../../../../models/procesos/consolidacion/recaudacion-scunac-remesas.model';

@Component({
  selector: 'app-editar-recaudacion',
  templateUrl: './editar-recaudacion.component.html',
  styleUrls: ['./editar-recaudacion.component.scss'],
  providers: [ DialogService, MessageService, DialogService ],
  encapsulation:ViewEncapsulation.None
})
export class EditarRecaudacionScunacRemesasComponent extends BaseBandeja implements OnInit, OnDestroy {
 
  private unsubscribe$ = new Subject<void>(); 
  public titulo: string;
  public recaudacionScunacRemesas: IRecaudacionScunacRemesas;
  public esConsultarRegistro: boolean = false;
  public fecha: string = '';
  public nombreArchivoExcel1: string = '';
  public nombreArchivoExcel2: string = '';
  public nombreArchivoExcel3: string = '';
  public idGuidExcel1: string = '';
  public idGuidExcel2: string = '';
  public idGuidExcel3: string = '';
  public servicioOperador: string = '';
  public mostrarBloque: boolean = true;
  private tiMoviVarios: string = 'M';
  private tiMoviObservaciones: string = 'O';
  private reqs: Observable<any>[] = [];
  public listaTotalesNiubizScunacRemesas: ITotalesNiubizScunacRemesas[] = [];
  public currentPage: string = environment.currentPage;
  public loadingTotalesNiubizScunacRemesas: boolean = false;
  public onResetDivConciliar: boolean = false;
  public listarOtrosHermes: IOtrosOperHermes[];
  public selectOtrosHermes: IOtrosOperHermes[] = []
  public loadingOtrosHermes: boolean = false;

  public item: any;
 
  reporteExcel!: any;
  nFile!: any;
  
  constructor(
    private dialogService: DialogService,
    public funciones: Funciones,
    public recaudacionScunacRemesasService: RecaudacionScunacRemesasService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService ,
    private messageService: MessageService,
    private generalService: GeneralService
  ){ super() }

  async ngOnInit() {
    this.codBandeja = MenuOpciones.CD_Recaudacion_SCUNAC_vs_Informes_Hermes;
    this.btnConsolidacionSCUNAC_Hermes();

    this.esConsultarRegistro = await this.sharingInformationService.compartirEsConsultarScunacRemesasObservable.pipe(first()).toPromise()
    this.recaudacionScunacRemesas = await this.sharingInformationService.compartirRecaudacionScunacRemesasObservable.pipe(first()).toPromise();      
    this.titulo = (!this.esConsultarRegistro) ? 'Editar' : 'Consultar';
    this.fecha = this.funciones.convertirFechaDateToString(new Date (this.recaudacionScunacRemesas.feCncl));
    this.servicioOperador = SERVICIO_DE_PAGO.HERMES;
    this.nombreArchivoExcel1 = this.recaudacionScunacRemesas.noAdju0001;
    this.nombreArchivoExcel2 = this.recaudacionScunacRemesas.noAdju0002;
    this.nombreArchivoExcel3 = this.recaudacionScunacRemesas.noAdju0003;
    this.idGuidExcel1 = this.recaudacionScunacRemesas.idGuidDocu01;
    this.idGuidExcel2 = this.recaudacionScunacRemesas.idGuidDocu02;
    this.idGuidExcel3 = this.recaudacionScunacRemesas.idGuidDocu03;
    this._obtenerOtrasOperacionesHermes();
  }


  public downloadFile(tipo: number) {
	switch(tipo) {
		case 0: this.nFile = "Recaudación_SCUNAC_NoConciliados";
		break;
		case 1: this.nFile = "Recaudación_SCUNAC_Conciliados";
		break;
		default: this.nFile = "Recaudación_SCUNAC_NoDefinido";
	}
	
	this.obtenerArchivoScunac(this.recaudacionScunacRemesas.idCncl, tipo);
  }
  
  
  obtenerArchivoScunac(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionScunacRemesasService.getRecaudacionScunac( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		this.exportExcel();
        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  
  public downloadFileTx(tipo: number) {
	switch(tipo) {
		case 1: this.nFile = "Recaudación_SCUNAC_Hermes_Conciliados";
		break;
		case 0: this.nFile = "Recaudación_SCUNAC_Hermes_NoConciliados";
		break;
    default: this.nFile = "Recaudación_SCUNAC_Hermes_NoDefinido";
	}
	this.obtenerArchivoSPRL(this.recaudacionScunacRemesas.idCncl, tipo);
  }
  
  

  obtenerArchivoSPRL(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionScunacRemesasService.getTransaccionesHermes( idCncl, inCncl ).subscribe({
      next: ( data ) => {
	  
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		this.exportExcel();

        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  public handleChange(e: any) {}

  private _obtenerOtrasOperacionesHermes() {
    this.loadingOtrosHermes = true;
    this.recaudacionScunacRemesasService.getOtrasOperacionesHERMES( this.recaudacionScunacRemesas.idCncl )
        .pipe( takeUntil( this.unsubscribe$) ).subscribe({
          next: ( data ) => {
            this.listarOtrosHermes = data.lista;
            this.loadingOtrosHermes = false;
          },
          error: () => {
            this.listarOtrosHermes = [];
            this.loadingOtrosHermes = false;
          }
        });
  }

  public nuevoOtrasHermes() {
    const dialog = this.dialogService.open( OtrasOperacionesHermesComponent, {
      header: 'Otras Operaciones HERMES',
      width: '40%',
      closable: false,
      data: {
        idCncl: this.recaudacionScunacRemesas.idCncl,
        registros: null
      }
    });

    dialog.onClose.pipe( takeUntil( this.unsubscribe$) ).subscribe((result: boolean) => {
      if ( result ) {
        this.messageService.add({ severity: 'success', summary: 'Se creo satisfactoriamente el registro.' });
        this._obtenerOtrasOperacionesHermes();
      }
    });
  }

  public editarOtrasHermes() {
    if ( this.selectOtrosHermes.length === 0 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro.');
      return;
    }

    if ( this.selectOtrosHermes.length !== 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede seleccionar más de un registro.');
      return;
    }

    const dialog = this.dialogService.open( OtrasOperacionesHermesComponent, {
      header: 'Otras Operaciones HERMES',
      width: '40%',
      closable: false,
      data: {
        idCncl: this.recaudacionScunacRemesas.idCncl,
        registros: this.selectOtrosHermes[0]
      }
    });

    dialog.onClose.pipe( takeUntil(this.unsubscribe$) ).subscribe((result: boolean) => {
      this.selectOtrosHermes = [];

      if ( result ) {
        this.messageService.add({ severity: 'success', summary: 'Se editó satisfactoriamente el registro.' });
        this._obtenerOtrasOperacionesHermes();
      }
    });
  }

  public anularOtrasHermes() {
    if ( this.selectOtrosHermes.length === 0 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar al menos un registro.');
      return;
    }

    if ( !this._validarRegistrosAnulados() ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Solo puede ANULAR registros en estado ACTIVO.');
      return;
    }

    const bodyActivarAnular = new BodyAnularActivarOtrosHermes();
    const textoAnular = (this.selectOtrosHermes.length === 1) ? 'el registro' : 'los registros';
    const textoActivar = (this.selectOtrosHermes.length === 1) ? 'anulo el registro' : 'anularon los registros';

    this.utilService.onShowConfirm(`¿Estás seguro que desea ANULAR ${textoAnular}?`).then( (result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('anular');           
        
        bodyActivarAnular.tramaOtro = this.selectOtrosHermes.map( so => { return so.idOperHermOtro } );

        this.recaudacionScunacRemesasService.anularOtrasOperacionesHERMES(bodyActivarAnular)
            .pipe(takeUntil(this.unsubscribe$)).subscribe( ( res ) => {
              if (res.codResult < 0 ) {
                this.utilService.onCloseLoading();
                this.utilService.onShowAlert("info", "Atención", res.msgResult);
              } else if (res.codResult === 0) {
                this._obtenerOtrasOperacionesHermes();
                this.utilService.onCloseLoading();
                this.utilService.onShowAlert('success', 'Atención', `Se ${ textoActivar } correctamente.`);
                this.selectOtrosHermes = [];
              }
            }, (err: HttpErrorResponse) => {   
              this.utilService.onShowMessageErrorSystem();                                
            });          
      }
    });
  }

  public activarOtrasHermes() {
    if ( this.selectOtrosHermes.length === 0 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar al menos un registro.');
      return;
    }

    if ( !this._validarRegistrosActivados() ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Solo puede ACTIVAR registros en estado INACTIVO.');
      return;
    }

    const bodyActivarAnular = new BodyAnularActivarOtrosHermes();
    const textoAnular = (this.selectOtrosHermes.length === 1) ? 'el registro' : 'los registros';
    const textoActivar = (this.selectOtrosHermes.length === 1) ? 'activo el registro' : 'activaron los registros';

    this.utilService.onShowConfirm(`¿Estás seguro que desea ACTIVAR ${textoAnular}?`).then( (result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('activar');
        
        bodyActivarAnular.tramaOtro = this.selectOtrosHermes.map( so => { return so.idOperHermOtro } );

        this.recaudacionScunacRemesasService.activarOtrasOperacionesHERMES(bodyActivarAnular)
            .pipe(takeUntil(this.unsubscribe$)).subscribe( ( res ) => {
              if (res.codResult < 0 ) {        
                this.utilService.onCloseLoading();
                this.utilService.onShowAlert("info", "Atención", res.msgResult);
              } else if (res.codResult === 0) {         
                this._obtenerOtrasOperacionesHermes();
                this.utilService.onCloseLoading();
                this.utilService.onShowAlert('success', 'Atención', `Se ${ textoActivar } correctamente.`);
                this.selectOtrosHermes = [];
              }
            }, (err: HttpErrorResponse) => {   
              this.utilService.onShowMessageErrorSystem();                                
            });          
      }
    });
  }

  public reprocesarOtrosHermes() {
    if ( this.listarOtrosHermes.length === 0 ) {
      this.utilService.onShowAlert('info', 'Atención', 'No existen registros en la tabla.');
      return;
    }

    const textoReprocesar: string = (this.listarOtrosHermes.length === 1) ? 'el registro' : 'los registros';

    this.utilService.onShowConfirm(`¿Está Ud. seguro que desea REPROCESAR ${ textoReprocesar }?`).then( (result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcessLoading('Reprocesando la información');

        this.recaudacionScunacRemesasService.reprocesarOtrasOperacionesHERMES( this.recaudacionScunacRemesas.idCncl )
            .pipe(takeUntil(this.unsubscribe$)).subscribe( ( res ) => {
              if (res.codResult < 0 ) {
                this.utilService.onCloseLoading();
                this.utilService.onShowAlert("info", "Atención", res.msgResult);
              } else if (res.codResult === 0) {
                this._obtenerOtrasOperacionesHermes();
                this.utilService.onCloseLoading();
                this.utilService.onShowAlert('success', 'Atención', 'Se reproceso correctamente.');
                this.selectOtrosHermes = [];
              }
            }, (err: HttpErrorResponse) => {   
              this.utilService.onShowMessageErrorSystem();
            });          
      }
    });
  }

  private _validarRegistrosAnulados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectOtrosHermes.length; i++) { 
      if (!(this.selectOtrosHermes[i].deEsta === 'Activo')) {
        resultado = false;
      }     
    }
    return resultado;
  }

  private _validarRegistrosActivados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectOtrosHermes.length; i++) {
      if (!(this.selectOtrosHermes[i].deEsta === 'Inactivo')) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  public async cancelar() {
    let dataFiltrosRecaudacionScunacRemesas: IDataFiltrosRecaudacionScunacRemesas = await this.sharingInformationService.compartirDataFiltrosRecaudacionScunacRemesasObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionScunacRemesas.esBusqueda) {
      dataFiltrosRecaudacionScunacRemesas.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionScunacRemesasObservableData = dataFiltrosRecaudacionScunacRemesas;
    }
    this.router.navigate(['SARF/procesos/consolidacion/recaudacion_scunac_remesas/bandeja']);
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirEsConsultarScunacRemesasObservableData = false;
    this.sharingInformationService.compartirRecaudacionScunacRemesasObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();      
  }


  exportExcel() {
    console.log('Exportar Excel...');

    const doc = this.reporteExcel;
	  const nFile = this.nFile;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', nFile);
    document.body.appendChild( download );
    download.click();
  }


}
