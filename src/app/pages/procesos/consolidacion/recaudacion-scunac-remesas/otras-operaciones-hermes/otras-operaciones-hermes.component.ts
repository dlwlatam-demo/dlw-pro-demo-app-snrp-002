import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject, takeUntil, Observable } from 'rxjs';

import { Calendar } from 'primeng/calendar';
import { Dropdown } from 'primeng/dropdown';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';

import { Funciones } from '../../../../../core/helpers/funciones/funciones';

import { IOficinaRegistral, IZonaRegistral, ILocalAtencion } from '../../../../../interfaces/combosV2.interface';
import { IResponse } from '../../../../../interfaces/general.interface';

import { BodyOtrosHermes } from '../../../../../models/procesos/consolidacion/recaudacion-scunac-remesas.model';

import { GeneralService } from '../../../../../services/general.service';
import { RecaudacionScunacRemesasService } from '../../../../../services/procesos/consolidacion/recaudacion-scunac-remesas.service';
import { IOtrosOperHermes } from '../../../../../interfaces/consolidacion-scunac-remesas';

@Component({
  selector: 'app-otras-operaciones-hermes',
  templateUrl: './otras-operaciones-hermes.component.html',
  styleUrls: ['./otras-operaciones-hermes.component.scss'],
  providers: [
    MessageService
  ]
})
export class OtrasOperacionesHermesComponent implements OnInit, OnDestroy {

  private idCncl!: number;
  public isLoading: boolean = false;

  public fecha: Date = new Date();

  public loadingZonaRegistral: boolean = false;
  public loadingOficinaRegistral: boolean = false;
  public loadingLocalAtencion: boolean = false;

  public listaZonaRegistral: IZonaRegistral[];
  public listaOficinaRegistral: IOficinaRegistral[];
  public listaLocalAtencion: ILocalAtencion[];

  private registroEditar: IOtrosOperHermes;

  public formOtrosHermes!: FormGroup;

  private bodyOtrosHermes!: BodyOtrosHermes;

  private unsubscribe$ = new Subject<void>();

  @ViewChild('ddlZonaRegistral') ddlZonaRegistral: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocalAtencion') ddlLocalAtencion: Dropdown;
  @ViewChild('imMontoHermes') montoHermes: ElementRef;
  @ViewChild('fechaHermes') fechaHermes: Calendar;

  constructor(
    private fb: FormBuilder,
    private funciones: Funciones,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private messageService: MessageService,
    private generalService: GeneralService,
    private recaudacionScunacService: RecaudacionScunacRemesasService
  ) { }

  ngOnInit(): void {
    this.bodyOtrosHermes = new BodyOtrosHermes();

    this.formOtrosHermes = this._defFormOtrosHermes();

    this.idCncl = this.config.data.idCncl;
    this.registroEditar = this.config.data.registros;

    this._inicilializarListas();

    if ( this.registroEditar === null ) {
      this._buscarZonaRegistral();
    } else {
      console.log('registroEditar', this.registroEditar);
      this._buscarZonaRegistral();
      this._setearCampos();
    }
  }

  private _defFormOtrosHermes() {
    return this.fb.group({
      fecha: [this.fecha, [ Validators.required ]],
      zonaRegistral: ['', [ Validators.required ]],
      oficinaRegistral: ['', [ Validators.required ]],
      localAtencion: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]],
      observaciones: ['', [ Validators.required ]]
    });
  }

  private _inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocalAtencion = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocalAtencion.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
  }

  private _setearCampos() {
    this.formOtrosHermes.patchValue({ "fecha": new Date(this.registroEditar.feOper) });
    this.formOtrosHermes.patchValue({ "monto": this.registroEditar.imOper.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    }) });
    this.formOtrosHermes.patchValue({ "observaciones": this.registroEditar.obHerm });
  }

  private _buscarZonaRegistral(): void {
    this.loadingZonaRegistral = true;
    this.generalService.getCbo_ZonasRegistrales().pipe( takeUntil(this.unsubscribe$) )
      .subscribe({
      next: ( data ) => {
        this.loadingZonaRegistral = false;    
        if (data.length === 1) {   
          this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });    
          this.formOtrosHermes.patchValue({ "zonaRegistral": data[0].coZonaRegi });
          this.buscarOficinaRegistral();
        } else {    
          this.listaZonaRegistral.push(...data);
        }

        if ( this.registroEditar ) {
          this.formOtrosHermes.patchValue({ "zonaRegistral": this.registroEditar.coZonaRegi });
          this.buscarOficinaRegistral();
        }
      },
      error:( _err )=>{
        this.loadingZonaRegistral = false;    
      }
    });
  }

  public buscarOficinaRegistral(): void {
    this.loadingOficinaRegistral = true;
    const zonaRegistral: string = this.formOtrosHermes.get('zonaRegistral')?.value;
    
    this.listaOficinaRegistral = [];
    this.listaLocalAtencion = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocalAtencion.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    
    if ( zonaRegistral !== "*" ) {
      this.generalService.getCbo_OficinasRegistrales( zonaRegistral ).pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data ) => {
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.formOtrosHermes.patchValue({ "oficinaRegistral": data[0].coOficRegi });
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;

        if ( this.registroEditar ) {
          this.formOtrosHermes.patchValue({ "oficinaRegistral": this.registroEditar.coOficRegi });
          this.buscarLocal();
        }
      }, ( _err ) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal(): void {
    this.loadingLocalAtencion = true;
    const zonaRegistral: string = this.formOtrosHermes.get('zonaRegistral')?.value;
    const oficinaRegistral: string = this.formOtrosHermes.get('oficinaRegistral')?.value;

    this.listaLocalAtencion = [];
    this.listaLocalAtencion.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    
    if ( oficinaRegistral !== "*" ) {
      this.generalService.getCbo_LocalesAtencion( zonaRegistral, oficinaRegistral ).pipe( takeUntil(this.unsubscribe$) )
      .subscribe( (data ) => {
        if (data.length === 1) {
          this.listaLocalAtencion.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formOtrosHermes.patchValue({ "local": data[0].coLocaAten});         
        } else {
          this.listaLocalAtencion.push(...data);          
        } 
        this.loadingLocalAtencion = false;

        if ( this.registroEditar ) {
          this.formOtrosHermes.patchValue({ "localAtencion": this.registroEditar.coLocaAten });
        }
      }, ( _err ) => {
        this.loadingLocalAtencion = false;
      });
    } else {
      this.loadingLocalAtencion = false;
    }
  }

  public validarMonto(event:any){
    const pattern = /^(\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public formatearRecaudacion( event: any ) {  
    if (event.target.value) {
      const nuevoValor = this.funciones.convertirTextoANumero( event.target.value );
      const number = Number(nuevoValor).toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });

      this.formOtrosHermes.get('monto')?.setValue( number );
    }
  }

  public grabar() {

    if ( !this._validarFormulario() ) {
      return;
    }

    this.isLoading = true;
    this._asignarValores( this.formOtrosHermes.getRawValue() );

    let obs: Observable< IResponse >;
    obs = ( this.registroEditar === null ) ?
    this.recaudacionScunacService.saveOrUpdateOperacionesHERMES( this.bodyOtrosHermes, 0 ) :
    this.recaudacionScunacService.saveOrUpdateOperacionesHERMES( this.bodyOtrosHermes, 1 )

    obs.pipe( takeUntil( this.unsubscribe$ ) ).subscribe({
      next: ( data ) => {
        if ( data.codResult < 0 ) {
          this.messageService.add({ severity: 'warn', summary: 'Atención', detail: data.msgResult, sticky: true });
          this.isLoading = false;
          return;
        }

        this.isLoading = false;
        this.ref.close( true );
      },
      error: ( err ) => {
        this.isLoading = false;
        if (err.error.category === "NOT_FOUND") {
          this.messageService.add({ severity: 'warn', summary: 'Atención', detail: err.error.description, sticky: true });
        } else {
          this.messageService.add({ severity: 'warn', summary: 'El servicio no esta disponible en estos momentos, inténtelo más tarde', sticky: true });
        }
      }
    });
  }

  public cancelar() {
    this.ref.close( false );
  }

  private _asignarValores(form: any) {
    const montoHermes: string = this.funciones.convertirTextoANumero( form.monto );

    this.bodyOtrosHermes.idOperHermOtro = (this.registroEditar === null) ? null : this.registroEditar.idOperHermOtro;
    this.bodyOtrosHermes.idCncl = this.idCncl;
    this.bodyOtrosHermes.feOper = (form.fecha !== "" && form.fecha !== null) ? this.funciones.convertirFechaDateToString(form.fecha) : '';
    this.bodyOtrosHermes.coZonaRegi = form.zonaRegistral === '*' ? '' : form.zonaRegistral;
    this.bodyOtrosHermes.coOficRegi = form.oficinaRegistral === '*' ? '' : form.oficinaRegistral;
    this.bodyOtrosHermes.coLocaAten = form.localAtencion === '*' ? '' : form.localAtencion;
    this.bodyOtrosHermes.imOper = Number( montoHermes );
    this.bodyOtrosHermes.obHerm = form.observaciones;
  }

  private _validarFormulario(): boolean {
    const registro = this.formOtrosHermes.getRawValue();  
    if ( registro.fecha === '' || registro.fecha === null ) {
      this.fechaHermes.focus;
      this.messageService.add({ severity: 'warn', summary: 'Seleccione una Fecha', sticky: true });
      return false;
    } else if (registro.zonaRegistral === "" || registro.zonaRegistral === "*" || registro.zonaRegistral === null) {      
      this.ddlZonaRegistral.focus();
      this.messageService.add({ severity: 'warn', summary: 'Seleccione una Zona Registral.', sticky: true });
      return false;
    } else if (registro.oficinaRegistral === "" || registro.oficinaRegistral === "*" || registro.oficinaRegistral === null) {      
      this.ddlOficinaRegistral.focus();
      this.messageService.add({ severity: 'warn', summary: 'Seleccione una Oficina Registral.', sticky: true });
      return false;
    } else if (registro.localAtencion === "" || registro.localAtencion === "*" || registro.localAtencion === null) {      
      this.ddlLocalAtencion.focus();
      this.messageService.add({ severity: 'warn', summary: 'Seleccione un Local de Atención.', sticky: true });
      return false;
    } else if (registro.monto === "" || registro.monto === null) {
      this.montoHermes.nativeElement.focus();
      this.messageService.add({ severity: 'warn', summary: 'Ingrese el Monto.', sticky: true });
    }
    return true;
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();                      
  }

}
