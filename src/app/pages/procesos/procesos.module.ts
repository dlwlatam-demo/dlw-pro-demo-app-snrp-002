import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ProcesosRoutingModule } from './procesos-routing.module';
import { NgPrimeModule } from '../../ng-prime/ng-prime.module';

import { CerrarAbrirMesComponent } from './cerrar-abrir-mes/cerrar-abrir-mes.component';


@NgModule({
  declarations: [
    CerrarAbrirMesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgPrimeModule,
    ProcesosRoutingModule
  ]
})
export class ProcesosModule { }
