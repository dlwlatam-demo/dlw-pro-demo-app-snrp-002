import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { SortEvent, ConfirmationService, MessageService } from 'primeng/api';

import { MesAnio } from '../../../interfaces/combos.interface';

import { GeneralService } from '../../../services/general.service';
import { CerrarAbrirMesService } from '../../../services/procesos/cerrar-abrir-mes.service';

import { BaseBandeja } from '../../../base/base-bandeja.abstract';

import { environment } from '../../../../environments/environment';
import { ValidatorsService } from '../../../core/services/validators.service';
import { MenuOpciones } from '../../../models/auth/Menu.model';

@Component({
  selector: 'app-cerrar-abrir-mes',
  templateUrl: './cerrar-abrir-mes.component.html',
  styleUrls: ['./cerrar-abrir-mes.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class CerrarAbrirMesComponent extends BaseBandeja implements OnInit {

  registros: string[] = [];
  msg!: string;

  form!: FormGroup;

  $mesAnio!: MesAnio[];
  $anio!: any[];
  enviadosList!: any[];

  today: Date = new Date();

  currentPage: string = environment.currentPage;

  constructor(
    private fb: FormBuilder,
    private confirmService: ConfirmationService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private validarService: ValidatorsService,
    private mesService: CerrarAbrirMesService,
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.CERRAR_ABRIR_MES;
    this.btnCerrarAbrirMes();
    
    this.$mesAnio = this.generalService.getCbo_MesesAnio();
    this.$anio = this.mesService.getCbo_Anios( this.today.getFullYear() );

    const mesInit: string = String(this.today.getMonth() + 1);
    const anioInit: number = this.today.getFullYear();

    this.form = this.fb.group({
      mes: [mesInit, [ Validators.required ]],
      anio: [anioInit, [ Validators.required ]]
    });

    this.buscaBandeja();
  }

  buscaBandeja() {
    let nuMes = this.form.get('mes')?.value;
    let nuAno = this.form.get('anio')?.value;

    this.buscarBandeja( nuAno, nuMes );
  }

  buscarBandeja( nuAno: number, nuMes: string ) {
    Swal.showLoading();
    this.mesService.getRegistrosPorMes( String( nuAno ), nuMes ).subscribe({
      next: ( data ) => {
        console.log( data );
        this.enviadosList = data;
      },
      error: () => {
        Swal.close()
        Swal.fire(
          'No se encuentran registros con los datos ingresados',
          '',
          'info'
        );

        this.enviadosList = [];
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  openOrCloseMes( value: string ) {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.deEsta == 'CERRADO' && value == 'close' ) {
        Swal.fire('No puede cerrar un registro ya cerrado', '', 'warning');
        return;
      }
      else if ( registro.deEsta == 'ABIERTO' && value == 'open' ) {
        Swal.fire('No puede abrir un registro ya abierto', '', 'warning');
        return;
      }

      this.registros.push( registro.idCierProc );
      this.msg = value == 'close' ? 'cerrar' : 'abrir';
    }

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea ${ this.msg } los registros?`,
      accept: () => {
        Swal.showLoading();

        const data = {
          nuAno: this.form.get('anio')?.value,
          nuMes: this.form.get('mes')?.value,
          coEsta: value == 'open' ? '001' : '002',
          trama: this.registros
        }

        this.mesService.cerrarAbrirMes( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({
              severity: 'success',
              summary: 'Se actualizaron los registros correctamente'
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({
              severity: 'error',
              summary: 'Ocurrió un error al actualizar los registros'
            });
          },
          complete: () => {
            Swal.close();
            this.registros = [];
            this.selectedValues = [];
            this.buscaBandeja();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  formatearFechas( fecha: string ): string {
    return this.validarService.reFormateaFechaConHoras( fecha );
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
