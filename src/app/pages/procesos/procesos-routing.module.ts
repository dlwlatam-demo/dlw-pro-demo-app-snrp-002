import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../services/auth/auth-gaurd.service';

import { CerrarAbrirMesComponent } from './cerrar-abrir-mes/cerrar-abrir-mes.component';

const routes: Routes = [
  {
    path: 'cerrar_abrir_mes',
    component: CerrarAbrirMesComponent,
    canActivate: [ AuthGaurdService ]
  },
  { 
    path: 'consolidacion',
    loadChildren: () => import('./consolidacion/consolidacion.module').then( m => m.ConsolidacionModule ),
    canActivate: [ AuthGaurdService ]
  },
  { 
    path: 'bancaria',
    loadChildren: () => import('./bancaria/consolidacion.module').then( m => m.ConsolidacionModule ),
    canActivate: [ AuthGaurdService ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcesosRoutingModule { }
