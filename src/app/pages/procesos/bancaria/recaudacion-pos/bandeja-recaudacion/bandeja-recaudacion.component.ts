import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent } from 'primeng/api';
import { forkJoin, Subject } from 'rxjs';
import { concatMap, filter, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { UtilService } from 'src/app/services/util.service';
import { GeneralService } from 'src/app/services/general.service';
import { RecaudacionPosService } from 'src/app/services/procesos/bancaria/recaudacion-pos.service';
import { FiltroRecaudacionPos, RecaudacionPos2 } from 'src/app/models/procesos/bancaria/recaudacion-pos.model';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { IDataFiltrosRecaudacionPos, IRecaudacionPos, IOperacionScunac, IPostOperacionScunac, IDetalleOperacionScunac } from 'src/app/interfaces/consolidacion-pos';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { ESTADO_REGISTRO_2 } from 'src/app/models/enum/parameters';
import { IResponse, IResponse2 } from 'src/app/interfaces/general.interface';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';

@Component({
  selector: 'app-bandeja-recaudacion',
  templateUrl: './bandeja-recaudacion.component.html',
  styleUrls: ['./bandeja-recaudacion.component.scss'],
  providers: [
    DialogService  
  ]
})
export class BandejaRecaudPosComponent extends BaseBandeja implements OnInit, OnDestroy { 
  
  unsubscribe$ = new Subject<void>();
  userCode: any;
  currentPage: string = environment.currentPage;
  formRecaudacionPos: FormGroup;
  fechaMinima: Date;
  fechaMaxima: Date; 
  listaZonaRegistral: ZonaRegistral[];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaOperador: IResponse2[];
  listaEstado: IResponse2[];
  listaRecaudacion: IRecaudacionPos[];
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  selectedValuesRecaudacion: IRecaudacionPos[] = [];
  loadingRecaudacion: boolean = false;
  filtroRecaudacionPos: FiltroRecaudacionPos;
  estadoRegistro = ESTADO_REGISTRO_2;  

  //*************** listados para el filtro ***************/
  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaOperadorFiltro: IResponse2[]; 
  listaEstadoFiltro: IResponse2[];
  esBusqueda: boolean = false;  

  constructor( 
    private dialogService: DialogService,
    private formBuilder: FormBuilder,  
    private generalService: GeneralService,
    private funciones: Funciones, 
    private recaudacionPosService: RecaudacionPosService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService,    
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.CB_Recaudacion_Medios_Electronicos;
    this.btnConsolidacionBancariaMediosElectronicos();
    
    this.fechaMaxima = new Date();
    this.fechaMinima = new Date(this.fechaMaxima.getFullYear(), this.fechaMaxima.getMonth(), 1);  
    this.userCode = localStorage.getItem('user_code')||'';   
    this.construirFormulario();

    this.sharingInformationService.compartirDataFiltrosRecaudacionPosObservable.pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: IDataFiltrosRecaudacionPos) => {      
      if (data.esCancelar && data.bodyRecaudacionPos !== null) {    
        this.utilService.onShowProcessLoading("Cargando la data");
        this.setearCamposFiltro(data);
        this.buscarRecaudacion(false);
      } else {
        this.inicilializarListas();
        this.obtenerListados();
      }
    });
  }

  private construirFormulario() {
    this.formRecaudacionPos = this.formBuilder.group({
      zona: [''],
      oficina: [''],
      local: [''],
      // operador: [''],    
      estado: [''],   
      fechaDesde: [this.fechaMinima],
      fechaHasta: [this.fechaMaxima]              
    });
  }

  private setearCamposFiltro(dataFiltro: IDataFiltrosRecaudacionPos) {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOperador = [];
    this.listaEstado = [];    
    this.listaZonaRegistral = dataFiltro.listaZonaRegistral; 
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaOperador = dataFiltro.listaOperador;
    this.listaEstado = dataFiltro.listaEstado;    
    this.filtroRecaudacionPos = dataFiltro.bodyRecaudacionPos;
    this.formRecaudacionPos.patchValue({ "zona": this.filtroRecaudacionPos.coZonaRegi});
    this.formRecaudacionPos.patchValue({ "oficina": this.filtroRecaudacionPos.coOficRegi});
    this.formRecaudacionPos.patchValue({ "local": this.filtroRecaudacionPos.coLocaAten});
    //this.formRecaudacionPos.patchValue({ "operador": this.filtroRecaudacionPos.idOperPos});
    this.formRecaudacionPos.patchValue({ "estado": this.filtroRecaudacionPos.esDocu});
    this.formRecaudacionPos.patchValue({ "fechaDesde": (this.filtroRecaudacionPos.feDesd !== '' && this.filtroRecaudacionPos.feDesd !== null) ? this.funciones.convertirFechaStringToDate(this.filtroRecaudacionPos.feDesd) : this.filtroRecaudacionPos.feDesd});
    this.formRecaudacionPos.patchValue({ "fechaHasta": (this.filtroRecaudacionPos.feHast !== '' && this.filtroRecaudacionPos.feHast !== null) ? this.funciones.convertirFechaStringToDate(this.filtroRecaudacionPos.feHast) : this.filtroRecaudacionPos.feHast});    
  }

  private inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOperador = [];
    this.listaEstado = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' });
    this.listaOperador.push({ ccodigoHijo: '0', cdescri: '(TODOS)' });  
    this.listaEstado.push({ ccodigoHijo: '', cdescri: '(TODOS)' });   
  }

  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_Zonas_Usuario( this.userCode),     
      this.generalService.getCbo_EstadoRecaudacion()
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resZonaRegistral, resEstado]) => { 
      if (resZonaRegistral.length === 1) {   
        this.listaZonaRegistral.push({ coZonaRegi: resZonaRegistral[0].coZonaRegi, deZonaRegi: resZonaRegistral[0].deZonaRegi });    
        this.formRecaudacionPos.patchValue({ "zona": resZonaRegistral[0].coZonaRegi});
        this.buscarOficinaRegistral();
      } else {    
        this.listaZonaRegistral.push(...resZonaRegistral);
      }
      this.listaEstado.push(...resEstado);        
      this.buscarRecaudacion(true);    
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }

  private establecerBodyRecaudacionPos() {       
    this.filtroRecaudacionPos = new FiltroRecaudacionPos();
    this.filtroRecaudacionPos.coZonaRegi = (this.formRecaudacionPos.get('zona')?.value === '*') ? '' : this.formRecaudacionPos.get('zona')?.value;    
    this.filtroRecaudacionPos.coOficRegi = (this.formRecaudacionPos.get('oficina')?.value === '*') ? '' : this.formRecaudacionPos.get('oficina')?.value;    
    this.filtroRecaudacionPos.coLocaAten = (this.formRecaudacionPos.get('local')?.value === '*') ? '' : this.formRecaudacionPos.get('local')?.value;        
    this.filtroRecaudacionPos.idOperPos = 0; // (this.formRecaudacionPos.get('operador')?.value === '0') ? 0 : this.formRecaudacionPos.get('operador')?.value;
    this.filtroRecaudacionPos.esDocu = (this.formRecaudacionPos.get('estado')?.value === '*') ? '' : this.formRecaudacionPos.get('estado')?.value;
    this.filtroRecaudacionPos.feDesd = (this.formRecaudacionPos.get('fechaDesde')?.value !== "" && this.formRecaudacionPos.get('fechaDesde')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formRecaudacionPos.get('fechaDesde')?.value) : ""; 
    this.filtroRecaudacionPos.feHast = (this.formRecaudacionPos.get('fechaHasta')?.value !== "" && this.formRecaudacionPos.get('fechaHasta')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formRecaudacionPos.get('fechaHasta')?.value): "";       
  }

  public buscarRecaudacion(esCargaInicial: boolean) {
    let resultado: boolean = this.validarCamporFiltros();
    if (!resultado) {
      return;
    }
    this.listaRecaudacion = [];
    this.selectedValuesRecaudacion = [];    
    this.loadingRecaudacion = (esCargaInicial) ? false : true;
    this.guardarListadosParaFiltro();     
    this.establecerBodyRecaudacionPos();
    this.esBusqueda = (esCargaInicial) ? false : true; 
    this.recaudacionPosService.getBandejaConciliacion(this.filtroRecaudacionPos).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (data: any) => {
      this.listaRecaudacion = data.lista;
      this.loadingRecaudacion = false;
      this.utilService.onCloseLoading();       
    }, (err: HttpErrorResponse) => {   
      this.loadingRecaudacion = false;
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("warning", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();
      }                             
    });
  }

  private validarCamporFiltros(): boolean {
    if (
      (this.formRecaudacionPos.get('fechaDesde')?.value !== "" && this.formRecaudacionPos.get('fechaDesde')?.value !== null) &&
      (this.formRecaudacionPos.get('fechaHasta')?.value === "" || this.formRecaudacionPos.get('fechaHasta')?.value === null)
    ) {
      document.getElementById('idFechaHasta').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de fin.");
      return false; 
    } else if (
      (this.formRecaudacionPos.get('fechaDesde')?.value === "" || this.formRecaudacionPos.get('fechaDesde')?.value === null) &&
      (this.formRecaudacionPos.get('fechaHasta')?.value !== "" && this.formRecaudacionPos.get('fechaHasta')?.value !== null)
    ) {
      document.getElementById('idFechaDesde').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de inicio.");
      return false; 
    } else {
      return true;
    }   
  }

  public buscarOficinaRegistral() { 
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formRecaudacionPos.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.formRecaudacionPos.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.formRecaudacionPos.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formRecaudacionPos.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.formRecaudacionPos.get('zona')?.value,this.formRecaudacionPos.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formRecaudacionPos.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err: HttpErrorResponse) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  public cambioFechaInicio() {   
    if (this.formRecaudacionPos.get('fechaHasta')?.value !== '' && this.formRecaudacionPos.get('fechaHasta')?.value !== null) {
      if (this.formRecaudacionPos.get('fechaDesde')?.value > this.formRecaudacionPos.get('fechaHasta')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.formRecaudacionPos.controls['fechaDesde'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.formRecaudacionPos.get('fechaDesde')?.value !== '' && this.formRecaudacionPos.get('fechaDesde')?.value !== null) {
      if (this.formRecaudacionPos.get('fechaHasta')?.value < this.formRecaudacionPos.get('fechaDesde')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.formRecaudacionPos.controls['fechaHasta'].reset();    
      }      
    }
  }

  public nuevo() {  
    this.router.navigate(['SARF/procesos/bancaria/recaudacion_pos/nuevo']);
    this.sharingInformationService.irRutaBandejaRecaudacionPosObservableData = false;
  }

  public editar() {   
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      if (!(this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.REPROCESADO)) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede EDITAR un registro en estado ABIERTO o REPROCESADO");
      } else {
        this.sharingInformationService.compartirRecaudacionPosObservableData = this.selectedValuesRecaudacion[0]; 
        this.router.navigate(['SARF/procesos/bancaria/recaudacion_pos/editar']);
        this.sharingInformationService.irRutaBandejaRecaudacionPosObservableData = false;    
      }
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }             
  }

  public anular() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosAnulados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ANULAR el registro en estado ABIERTO o REPROCESADO.");
    } else {
      let textoAnular = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ANULAR ${textoAnular}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('anular');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyAnularRecaudacion = new RecaudacionPos2();
          bodyAnularRecaudacion.trama = lista;
          this.recaudacionPosService.anulaRecaudacionPos(bodyAnularRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosAnulados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.REPROCESADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public activar() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosActivados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ACTIVAR el registro en estado ANULADO.");
    } else {
      let textoActivar = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ACTIVAR ${textoActivar}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('activar');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyActivarrRecaudacion = new RecaudacionPos2();
          bodyActivarrRecaudacion.trama = lista;
          this.recaudacionPosService.activateRecaudacionPos(bodyActivarrRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosActivados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ANULADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public abrirConciliacion() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosAbiertos()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ABRIR el registro en estado CERRADO.");
    } else {
      let textoAbrir = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ABRIR ${textoAbrir}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('abrir');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyAbrirRecaudacion = new RecaudacionPos2();
          bodyAbrirRecaudacion.trama = lista;
          this.recaudacionPosService.openRecaudacionPos(bodyAbrirRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosAbiertos(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.CERRADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public cerrarConciliacion() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosCerrados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede CERRAR el registro en estado ABIERTO o REPROCESADO.");
    } else {
      let textoAbrir = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea CERRAR ${textoAbrir}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('cerrar');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyCerrarRecaudacion = new RecaudacionPos2();
          bodyCerrarRecaudacion.trama = lista;
          this.recaudacionPosService.closeRecaudacionPos(bodyCerrarRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {                 
              const listado = lista.map(item => this.operacionesScunac(parseInt(item)).catch((error) => { }));
              Promise.all(lista).then( respuesta => {             
                this.buscarRecaudacion(false);
                this.selectedValuesRecaudacion = [];          
              });
            }             
          }, (err: HttpErrorResponse) => {      
          });
        }
      });
    }
  }

  private operacionesScunac(idCncl: number) {
    return new Promise<any>(
      (resolve, reject) => {               
        this.recaudacionPosService.getOperacionesScunac(idCncl, 2)
          .pipe(            
            takeUntil(this.unsubscribe$),
            filter((data: any) => {           
              if (data.body.lstOperacionScunac.length > 0) {
                return true;
              } else {               
                return false;
              }
            }),
            concatMap((res: any) => {           
              let listaOperacionScunac: IOperacionScunac[] = res.body.lstOperacionScunac;      
              return this.recaudacionPosService.actualizarIdOperacionScunac(this.obtenerOperacionScunac(listaOperacionScunac))              
            })
          ).subscribe( (res: IResponse) => {
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
              reject(new Error("Error de ejecución . . . :( "));
            } else if (res.codResult === 0) { 
              resolve(idCncl);                     
            }                                                                     
          }, () => {
            reject(new Error("Error de ejecución . . . :( ")); 
          }
        ) 
      }
    );
  }

  private obtenerOperacionScunac(lista: IOperacionScunac[]): IPostOperacionScunac {
    let data: IPostOperacionScunac;
    let listaDetalle: IDetalleOperacionScunac[] = [];
    for (let i = 0; i < lista.length; i++) {
      let item: IDetalleOperacionScunac = {
        aaMvto: lista[i].anioMovimiento,
        coZonaRegi: lista[i].coZonaRegi,
        nuMvto: lista[i].numeroMovimiento,
        nuSecuDeta: lista[i].nuSecuDeta,
        idTransPos: lista[i].id        
      }
      listaDetalle.push(item);      
    }
    data = {
      trama: listaDetalle
    } 
    return data;
  }

  private validarRegistrosCerrados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.REPROCESADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public reProcesar() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      if (!(this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.REPROCESADO)) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede REPROCESAR un registro en estado ABIERTO o REPROCESADO");
      } else {
        this.sharingInformationService.compartirRecaudacionPosObservableData = this.selectedValuesRecaudacion[0]; 
        this.router.navigate(['SARF/procesos/bancaria/recaudacion_pos/reprocesar']);
        this.sharingInformationService.irRutaBandejaRecaudacionPosObservableData = false; 
      }           
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }
  }

  public consultarRegistro() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      this.sharingInformationService.compartirEsConsultarPosObservableData = true; 
      this.sharingInformationService.compartirRecaudacionPosObservableData = this.selectedValuesRecaudacion[0];
      this.router.navigate(['SARF/procesos/bancaria/recaudacion_pos/consultar']);
      this.sharingInformationService.irRutaBandejaRecaudacionPosObservableData = false;      
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }
  }

  customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;
        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
        return (event.order! * result);
    });
  }

  private guardarListadosParaFiltro() {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaOperadorFiltro = [...this.listaOperador];
    this.listaEstadoFiltro = [...this.listaEstado];    
  }

  private establecerDataFiltrosRecaudacionME() : IDataFiltrosRecaudacionPos {
    let dataFiltrosRecaudacionME: IDataFiltrosRecaudacionPos = {
      listaZonaRegistral: this.listaZonaRegistralFiltro, 
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaOperador: this.listaOperadorFiltro,     
      listaEstado: this.listaEstadoFiltro,
      bodyRecaudacionPos: this.filtroRecaudacionPos,   
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosRecaudacionME;
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();   
    this.sharingInformationService.compartirDataFiltrosRecaudacionPosObservableData = this.establecerDataFiltrosRecaudacionME();                    
  }

}
