import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, first, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MessageService } from 'primeng/api';
import { SortEvent } from 'primeng/api';
import { 
  IConciliacionManual, 
  IDataFiltrosRecaudacionPos, 
  IRecaudacionPos, 
  IConciliacionManual2, 
  ITotalesNiubizPos 
} from 'src/app/interfaces/consolidacion-pos';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { RecaudacionPosService } from 'src/app/services/procesos/bancaria/recaudacion-pos.service';
import { UtilService } from 'src/app/services/util.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { environment } from 'src/environments/environment';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { DialogService } from 'primeng/dynamicdialog';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { GeneralService } from '../../../../../services/general.service';
import { RecaudacionMediosElectronicosService } from '../../../../../services/procesos/consolidacion/recaudacion-medios-electronicos.service';
@Component({
  selector: 'app-editar-recaudacion',
  templateUrl: './editar-recaudacion.component.html',
  styleUrls: ['./editar-recaudacion.component.scss'],
  providers: [ DialogService, MessageService ],
  encapsulation:ViewEncapsulation.None 
})
export class EditarRecaudacionPosComponent extends BaseBandeja implements OnInit, OnDestroy {
 
  private unsubscribe$ = new Subject<void>(); 
  public titulo: string;
  public recaudacionPos: IRecaudacionPos;
  public esConsultarRegistro: boolean = false;
  public fecha: string = '';
  public nombreDocumento: string = '';
  public nombreDocumentoDiners: string = '';
  public nombreArchivoExcel: string = '';
  public nombreArchivoExcelDiners: string = '';
  public idGuidExcel: string = '';
  public idGuidExcelDiners: string = '';
  public servicioOperador: string = '';
  public mostrarBloque: boolean = false;
  private tiMoviVarios: string = 'M';
  private tiMoviObservaciones: string = 'O';
  private reqs: Observable<any>[] = [];
  public listaTotalesNiubizPos: ITotalesNiubizPos[] = [];
  public currentPage: string = environment.currentPage;
  public loadingTotalesNiubizPos: boolean = false;
  public onResetDivConciliar: boolean = false;

  public item: any;
 
  reporteExcel!: any;
  nFile!: any;
  
  constructor(
    private dialogService: DialogService,
    public funciones: Funciones,
    public recaudacionPosService: RecaudacionPosService,
    public recaudacionMediosElectronicosService: RecaudacionMediosElectronicosService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService ,
    private messageService: MessageService,
    private generalService: GeneralService
  ){ super() }

  async ngOnInit() {
    this.codBandeja = MenuOpciones.CB_Recaudacion_Medios_Electronicos;
    this.btnConsolidacionBancariaMediosElectronicos();
    
    this.esConsultarRegistro = await this.sharingInformationService.compartirEsConsultarPosObservable.pipe(first()).toPromise()
    this.recaudacionPos = await this.sharingInformationService.compartirRecaudacionPosObservable.pipe(first()).toPromise();      
    console.log('recaudacionPOS', this.recaudacionPos);
    this.titulo = (!this.esConsultarRegistro) ? 'Editar' : 'Consultar';
    this.fecha = this.funciones.convertirFechaDateToString(new Date (this.recaudacionPos.feCncl));
    this.servicioOperador = SERVICIO_DE_PAGO.NIUBIZ;
    this.nombreDocumento = 'Nombre de archivo Estado Bancario NIUBIZ y AMEX';
    this.nombreDocumentoDiners = 'Nombre de archivo Estado Bancario DINERS';
    this.nombreArchivoExcel = this.recaudacionPos.noAdju0001;
    this.nombreArchivoExcelDiners = this.recaudacionPos.noAdju0002;
    this.idGuidExcel = this.recaudacionPos.idGuidDocu01;
    this.idGuidExcelDiners = this.recaudacionPos.idGuidDocu02;
  }

  public downloadFile(tipo: number) {
	switch(tipo) {
		case 0: this.nFile = "RecaudacionBancaria_MediosElectronicos_EEBB_NoConciliados";
		break;
		case 1: this.nFile = "RecaudacionBancaria_MediosElectronicos_EEBB_Conciliados";
		break;
		default: this.nFile = "RecaudacionBancaria_MediosElectronicos_EEBB_NoDefinido";
	}
	
	this.obtenerArchivoMEF(this.recaudacionPos.idCncl, tipo);
  }
  
  
  obtenerArchivoMEF(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionPosService.getRecaudacionMEF( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		this.exportExcel();
        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }

  public downloadFileNiubiz( tipo: number ) {
    switch ( tipo ) {
      case 0:
        this.nFile = 'Recaudacion_MediosElectronicos_NIUBIZ_NoConciliados';
          break;
      case 1:
        this.nFile = 'Recaudacion_MediosElectronicos_NIUBIZ_Conciliados';
          break;
      case 2:
        this.nFile = 'Recaudacion_MediosElectronicos_NIUBIZ_Anulaciones';
          break;
      case 3:
        this.nFile = 'Recaudacion_MediosElectronicos_NIUBIZ_Contracargos';
          break;
      default:
        this.nFile = 'Recaudacion_MediosElectronicos_NIUBIZ_NoDefinido';
    }

    this.obtenerArchivoNiubiz( this.recaudacionPos.idCncl, tipo );
  }

  obtenerArchivoNiubiz( idCncl: number, inCncl: number ) {
    Swal.showLoading();
    this.recaudacionMediosElectronicosService.getRecaudacionNiubiz( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        console.log('Niubiz', data );
        this.reporteExcel = data.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });

        this.exportExcel();
        Swal.close();
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  public downloadFileDiners( tipo: number ) {
    switch ( tipo ) {
      case 0:
        this.nFile = 'Recaudacion_MediosElectronicos_DINERS_NoConciliados';
          break;
      case 1:
        this.nFile = 'Recaudacion_MediosElectronicos_DINERS_Conciliados';
          break;
      default:
        this.nFile = 'Recaudacion_MediosElectronicos_DINERS_NoDefinido';
    }

    this.obtenerArchivoDiners( this.recaudacionPos.idCncl, tipo );
  }

  obtenerArchivoDiners( idCncl: number, inCncl: number ) {
    Swal.showLoading();
    this.recaudacionMediosElectronicosService.getRecaudacionDiners( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        console.log('Diners', data );
        this.reporteExcel = data.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });

        this.exportExcel();
        Swal.close();
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  public downloadFileAmex( tipo: number ) {
    switch ( tipo ) {
      case 0:
        this.nFile = 'Recaudacion_MediosElectronicos_AMEX_NoConciliados';
          break;
      case 1:
        this.nFile = 'Recaudacion_MediosElectronicos_AMEX_Conciliados';
          break;
      default:
        this.nFile = 'Recaudacion_MediosElectronicos_AMEX_NoDefinido';
    }

    this.obtenerArchivoAmex( this.recaudacionPos.idCncl, tipo );
  }

  obtenerArchivoAmex( idCncl: number, inCncl: number ) {
    Swal.showLoading();
    this.recaudacionMediosElectronicosService.getRecaudacionAmex( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        console.log('Amex', data );
        this.reporteExcel = data.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });

        this.exportExcel();
        Swal.close();
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      },
      complete: () => {
        Swal.close();
      }
    });
  }
  
  
  // public downloadFileSCUNAC(tipo: number) {
	// switch(tipo) {
	// 	case 1: this.nFile = "RecaudacionBancaria_MediosElectronicos_SCUNAC_Conciliados";
	// 	break;
	// 	case 0: this.nFile = "RecaudacionBancaria_MediosElectronicos_SCUNAC_NoConciliados";
	// 	break;
	// 	default: this.nFile = "RecaudacionBancaria_MediosElectronicos_SCUNAC_NoDefinido";
	// }
	// this.obtenerArchivoSCUNAC(this.recaudacionPos.idCncl, tipo);
  // }
  
  

  // obtenerArchivoSCUNAC(idCncl: number, inCncl: number) {
    
  //   Swal.showLoading();
  //   this.recaudacionPosService.getRecaudacionSCUNAC( idCncl, inCncl ).subscribe({
  //     next: ( data ) => {
	  
  //       this.reporteExcel = data.body.reporteExcel;

  //       this.messageService.add({
  //         severity: 'success', 
  //         summary: 'Reporte generado exitósamente', 
  //         detail: 'Puede proceder con la descarga de su reporte.'
  //       });
		
	// 	this.exportExcel();

  //       Swal.close();
  //       //this.disableButton = false;
  //     },
  //     error: ( e ) => {
  //       console.log( e );
  //       Swal.close();

  //       if ( e.error.category == 'NOT_FOUND' ) {
  //         this.messageService.add({
  //           severity: 'info', 
  //           summary: e.error.description,
  //           detail: 'Por favor, modifique los parámetros seleccionados.'
  //         });

  //         return;
  //       }

  //       this.messageService.add({
  //         severity: 'error', 
  //         summary: 'Falla en generar el Reporte', 
  //         detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
  //       });
  //     }
  //   });
  // }
  
  
  public handleChange(e: any) {
    if ( e.index === 0 ) {
      this.nombreDocumento = 'Nombre de archivo Estado Bancario NIUBIZ y AMEX';
      this.nombreArchivoExcel = this.recaudacionPos.noAdju0001;
      this.idGuidExcel = this.recaudacionPos.idGuidDocu01;

      this.nombreDocumentoDiners = 'Nombre de archivo Estado Bancario DINERS';
      this.nombreArchivoExcelDiners = this.recaudacionPos.noAdju0002;
      this.idGuidExcelDiners = this.recaudacionPos.idGuidDocu02;
      this.mostrarBloque = false;
    } else if ( e.index === 1 ) {
      this.servicioOperador = SERVICIO_DE_PAGO.NIUBIZ;
      this.nombreDocumento = 'Nombre de archivo excel';
      this.nombreArchivoExcel = this.recaudacionPos.noAdju0003;
      this.idGuidExcel = this.recaudacionPos.idGuidDocu03;
      this.mostrarBloque = true;
    } else if ( e.index === 2 ) {
      this.servicioOperador = SERVICIO_DE_PAGO.DINERS;
      this.nombreArchivoExcel = this.recaudacionPos.noAdju0004;
      this.idGuidExcel = this.recaudacionPos.idGuidDocu04;
      this.mostrarBloque = true;
    } else if ( e.index === 3 ) {
      this.servicioOperador = SERVICIO_DE_PAGO.AMEX;
      this.nombreArchivoExcel = this.recaudacionPos.noAdju0005;
      this.idGuidExcel = this.recaudacionPos.idGuidDocu05;
      this.mostrarBloque = true;
    }
  }

  numberTransform( value: string ): string {
    return Number( value ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  public async cancelar() {
    let dataFiltrosRecaudacionPos: IDataFiltrosRecaudacionPos = await this.sharingInformationService.compartirDataFiltrosRecaudacionPosObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionPos.esBusqueda) {
      dataFiltrosRecaudacionPos.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionPosObservableData = dataFiltrosRecaudacionPos;
    }
    this.router.navigate(['SARF/procesos/bancaria/recaudacion_pos/bandeja']);
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirEsConsultarPosObservableData = false;
    this.sharingInformationService.compartirRecaudacionPosObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();      
  }


  exportExcel() {
    console.log('Exportar Excel');

    const doc = this.reporteExcel;
	const nFile = this.nFile;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', nFile);
    document.body.appendChild( download );
    download.click();
  }


}
