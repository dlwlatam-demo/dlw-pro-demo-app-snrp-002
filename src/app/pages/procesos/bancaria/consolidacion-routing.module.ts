import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { BandejaRecaudPosComponent } from './recaudacion-pos/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionPosComponent } from './recaudacion-pos/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionPosComponent } from './recaudacion-pos/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionPosGuard } from 'src/app/core/guards/procesos/bancaria/refresh-recaudacion-pos.guard';


import { BandejaRecaudPagaloPeComponent } from './recaudacion-pagalo-pe/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionPagaloPeComponent } from './recaudacion-pagalo-pe/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionPagaloPeComponent } from './recaudacion-pagalo-pe/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionPagaloPeGuard } from 'src/app/core/guards/procesos/bancaria/refresh-recaudacion-pagalo-pe.guard';

import { BandejaRecaudMacmypeComponent } from './recaudacion-macmype/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionMacmypeComponent } from './recaudacion-macmype/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionMacmypeComponent } from './recaudacion-macmype/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionMacmypeGuard } from 'src/app/core/guards/procesos/bancaria/refresh-recaudacion-macmype.guard';

import { BandejaRecaudSiafMefComponent } from './recaudacion-siaf-mef/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionSiafMefComponent } from './recaudacion-siaf-mef/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionSiafMefComponent } from './recaudacion-siaf-mef/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionSiafMefGuard } from 'src/app/core/guards/procesos/bancaria/refresh-recaudacion-siaf-mef.guard';

import { BandejaRecaudCciSprlComponent } from './recaudacion-cci-sprl/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionCciSprlComponent } from './recaudacion-cci-sprl/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionCciSprlComponent } from './recaudacion-cci-sprl/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionCciSprlGuard } from 'src/app/core/guards/procesos/bancaria/refresh-recaudacion-cci-sprl.guard';

import { BandejaRecaudHermesComponent } from './recaudacion-hermes/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionHermesComponent } from './recaudacion-hermes/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionHermesComponent } from './recaudacion-hermes/editar-recaudacion/editar-recaudacion.component';
import { RefreshRecaudacionHermesGuard } from 'src/app/core/guards/procesos/bancaria/refresh-recaudacion-hermes.guard';

import { BandejaBancariaAppQrSid } from './recaudacion-app-qr-sid/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaBancariaAppQrSid } from './recaudacion-app-qr-sid/nueva-recaudacion/nueva-recaudacion.component';
import { EditarBancariaAppQrSid } from './recaudacion-app-qr-sid/editar-recaudacion/editar-recaudacion.component';



const routes: Routes = [  
  { path: 'recaudacion_pos',
    children: [
      { path: 'bandeja', component: BandejaRecaudPosComponent },
      { path: 'nuevo', component: NuevaRecaudacionPosComponent, canActivate: [RefreshRecaudacionPosGuard] },
      { path: 'editar', component: EditarRecaudacionPosComponent, canActivate: [RefreshRecaudacionPosGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionPosComponent, canActivate: [RefreshRecaudacionPosGuard] }, 
      { path: 'consultar', component: EditarRecaudacionPosComponent, canActivate: [RefreshRecaudacionPosGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'recaudacion_bancaria_app_qr_sid',
    children: [
      { path: 'bandeja', component: BandejaBancariaAppQrSid },
      { path: 'nuevo', component: NuevaBancariaAppQrSid },
      { path: 'editar', component: EditarBancariaAppQrSid },
      { path: 'reprocesar', component: NuevaBancariaAppQrSid },
      { path: 'consultar', component: EditarBancariaAppQrSid }
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'recaudacion_pagalo_pe',
    children: [
      { path: 'bandeja', component: BandejaRecaudPagaloPeComponent },
      { path: 'nuevo', component: NuevaRecaudacionPagaloPeComponent, canActivate: [RefreshRecaudacionPagaloPeGuard] },
      { path: 'editar', component: EditarRecaudacionPagaloPeComponent, canActivate: [RefreshRecaudacionPagaloPeGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionPagaloPeComponent, canActivate: [RefreshRecaudacionPagaloPeGuard] }, 
      { path: 'consultar', component: EditarRecaudacionPagaloPeComponent, canActivate: [RefreshRecaudacionPagaloPeGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'recaudacion_macmype',
    children: [
      { path: 'bandeja', component: BandejaRecaudMacmypeComponent },
      { path: 'nuevo', component: NuevaRecaudacionMacmypeComponent, canActivate: [RefreshRecaudacionMacmypeGuard] },
      { path: 'editar', component: EditarRecaudacionMacmypeComponent, canActivate: [RefreshRecaudacionMacmypeGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionMacmypeComponent, canActivate: [RefreshRecaudacionMacmypeGuard] }, 
      { path: 'consultar', component: EditarRecaudacionMacmypeComponent, canActivate: [RefreshRecaudacionMacmypeGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'recaudacion_siaf_mef',
    children: [
      { path: 'bandeja', component: BandejaRecaudSiafMefComponent },
      { path: 'nuevo', component: NuevaRecaudacionSiafMefComponent, canActivate: [RefreshRecaudacionSiafMefGuard] },
      { path: 'editar', component: EditarRecaudacionSiafMefComponent, canActivate: [RefreshRecaudacionSiafMefGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionSiafMefComponent, canActivate: [RefreshRecaudacionSiafMefGuard] }, 
      { path: 'consultar', component: EditarRecaudacionSiafMefComponent, canActivate: [RefreshRecaudacionSiafMefGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'recaudacion_cci_sprl',
    children: [
      { path: 'bandeja', component: BandejaRecaudCciSprlComponent },
      { path: 'nuevo', component: NuevaRecaudacionCciSprlComponent, canActivate: [RefreshRecaudacionCciSprlGuard] },
      { path: 'editar', component: EditarRecaudacionCciSprlComponent, canActivate: [RefreshRecaudacionCciSprlGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionCciSprlComponent, canActivate: [RefreshRecaudacionCciSprlGuard] }, 
      { path: 'consultar', component: EditarRecaudacionCciSprlComponent, canActivate: [RefreshRecaudacionCciSprlGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'recaudacion_hermes',
    children: [
      { path: 'bandeja', component: BandejaRecaudHermesComponent },
      { path: 'nuevo', component: NuevaRecaudacionHermesComponent, canActivate: [RefreshRecaudacionHermesGuard] },
      { path: 'editar', component: EditarRecaudacionHermesComponent, canActivate: [RefreshRecaudacionHermesGuard] },  
      { path: 'reprocesar', component: NuevaRecaudacionHermesComponent, canActivate: [RefreshRecaudacionHermesGuard] }, 
      { path: 'consultar', component: EditarRecaudacionHermesComponent, canActivate: [RefreshRecaudacionHermesGuard] }     
    ],
    canActivate: [ AuthGaurdService ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsolidacionRoutingModule { }
