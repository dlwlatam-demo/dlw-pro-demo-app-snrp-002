import { Component, OnInit, OnDestroy, ViewEncapsulation, Inject, LOCALE_ID } from '@angular/core';
import { formatNumber } from '@angular/common';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, first, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MessageService } from 'primeng/api';
import { SortEvent } from 'primeng/api';
import { 
  IConciliacionManual, 
  IDataFiltrosRecaudacionHermes, 
  IRecaudacionHermes, 
  ITotales, 
  ITotalesNiubizHermes 
} from 'src/app/interfaces/consolidacion-hermes';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { RecaudacionHermesService } from 'src/app/services/procesos/bancaria/recaudacion-hermes.service';
import { UtilService } from 'src/app/services/util.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { environment } from 'src/environments/environment';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { DialogService } from 'primeng/dynamicdialog';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { GeneralService } from '../../../../../services/general.service';
@Component({
  selector: 'app-editar-recaudacion-hermes',
  templateUrl: './editar-recaudacion.component.html',
  styleUrls: ['./editar-recaudacion.component.scss'],
  providers: [ DialogService, MessageService ],
  encapsulation:ViewEncapsulation.None 
})
export class EditarRecaudacionHermesComponent extends BaseBandeja implements OnInit, OnDestroy {
 
  private unsubscribe$ = new Subject<void>(); 
  public titulo: string;
  public recaudacionHermes: IRecaudacionHermes;
  public esConsultarRegistro: boolean = false;
  public fecha: string = '';
  public nombreArchivoExcel1: string = '';
  public nombreArchivoExcel2: string = '';
  public idGuidExcel1: string = '';
  public idGuidExcel2: string = '';
  public mostrarBloque: boolean = true;
  private tiMoviVarios: string = 'M';
  private tiMoviObservaciones: string = 'O';
  private reqs: Observable<any>[] = [];
  public listaTotalesNiubizHermes: ITotalesNiubizHermes[] = [];
  public currentPage: string = environment.currentPage;
  public loadingTotalesNiubizHermes: boolean = false;
  public onResetDivConciliar: boolean = false;
  
  public sftpTotalRegis: string = '0.00';
  public sftpTotalAbonos: string = '0.00';
  public bancarioTotalRegis: string = '0.00';
  public bancarioTotalCargos: string = '0.00';
  public bancarioTotalAbonos: string = '0.00';
  public diferencia: string = '0.00';

  public item: any;
 
  reporteExcel!: any;
  nFile!: any;
  
  constructor(
    private dialogService: DialogService,
    public funciones: Funciones,
    public recaudacionHermesService: RecaudacionHermesService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService ,
    private messageService: MessageService,
    private generalService: GeneralService,
	@Inject(LOCALE_ID) public locale: string
  ){ super() }

  async ngOnInit() {
    this.codBandeja = MenuOpciones.CB_Depositos_Hermes;
    this.btnConsolidacionBancariaDepositoHermes();
    
    this.esConsultarRegistro = await this.sharingInformationService.compartirEsConsultarHermesObservable.pipe(first()).toPromise()
    this.recaudacionHermes = await this.sharingInformationService.compartirRecaudacionHermesObservable.pipe(first()).toPromise();      
    this.titulo = (!this.esConsultarRegistro) ? 'Editar' : 'Consultar';
    this.fecha = this.funciones.convertirFechaDateToString(new Date (this.recaudacionHermes.feCnclDesd));
	  this.nombreArchivoExcel1 = this.recaudacionHermes.noAdju0001;
    this.idGuidExcel1 = this.recaudacionHermes.idGuidDocu01;
	  this.nombreArchivoExcel2 = this.recaudacionHermes.noAdju0002;
    this.idGuidExcel2 = this.recaudacionHermes.idGuidDocu02;
  }


  public downloadFile(tipo: number) {
	switch(tipo) {
		case 0: this.nFile = "Consolidación_DepositoHermes_NoConciliados";
		break;
		case 1: this.nFile = "Consolidación_DepositoHermes_Conciliados";
		break;
		default: this.nFile = "Consolidación_DepositoHermes_NoDefinido";
	}
	
	this.obtenerArchivoHermes(this.recaudacionHermes.idCnclSiaf, tipo);
  }
  
  
  obtenerArchivoHermes(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionHermesService.getRecaudacionHermes( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		this.exportExcel();
        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  
  public downloadFileTx(tipo: number) {
	switch(tipo) {
		case 1: this.nFile = "Consolidación_DepositoHermes_EEBB_Conciliados";
		break;
		case 0: this.nFile = "Consolidación_DepositoHermes_EEBB_NoConciliados";
		break;
		default: this.nFile = "Consolidación_DepositoHermes_EEBB_NoDefinido";
	}
	this.obtenerArchivoMef(this.recaudacionHermes.idCnclSiaf, tipo);
  }
  
  

  obtenerArchivoMef(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionHermesService.getArchivosMef( idCncl, inCncl ).subscribe({
      next: ( data ) => {
	  
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		this.exportExcel();

        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  
   
  public handleChange(e: any) {  
      this.mostrarBloque = true;
  }

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

  public async cancelar() {
    let dataFiltrosRecaudacionHermes: IDataFiltrosRecaudacionHermes = await this.sharingInformationService.compartirDataFiltrosRecaudacionHermesObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionHermes.esBusqueda) {
      dataFiltrosRecaudacionHermes.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionHermesObservableData = dataFiltrosRecaudacionHermes;
    }
    this.router.navigate(['SARF/procesos/bancaria/recaudacion_hermes/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirEsConsultarHermesObservableData = false;
    this.sharingInformationService.compartirRecaudacionHermesObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();      
  }


  exportExcel() {
    console.log('Exportar Excel');

    const doc = this.reporteExcel;
	const nFile = this.nFile;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', nFile);
    document.body.appendChild( download );
    download.click();
  }


}
