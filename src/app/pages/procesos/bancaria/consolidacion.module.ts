import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsolidacionRoutingModule } from './consolidacion-routing.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';
import { DigitOnlyModule } from '@uiowa/digit-only';
import { SharedModule } from 'src/app/shared/shared.module';

import { BandejaRecaudPosComponent } from './recaudacion-pos/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionPosComponent } from './recaudacion-pos/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionPosComponent } from './recaudacion-pos/editar-recaudacion/editar-recaudacion.component';


import { BandejaRecaudPagaloPeComponent } from './recaudacion-pagalo-pe/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionPagaloPeComponent } from './recaudacion-pagalo-pe/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionPagaloPeComponent } from './recaudacion-pagalo-pe/editar-recaudacion/editar-recaudacion.component';

import { BandejaRecaudMacmypeComponent } from './recaudacion-macmype/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionMacmypeComponent } from './recaudacion-macmype/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionMacmypeComponent } from './recaudacion-macmype/editar-recaudacion/editar-recaudacion.component';

import { BandejaRecaudSiafMefComponent } from './recaudacion-siaf-mef/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionSiafMefComponent } from './recaudacion-siaf-mef/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionSiafMefComponent } from './recaudacion-siaf-mef/editar-recaudacion/editar-recaudacion.component';
import { DivConciliarPorMontoSiafMefComponent } from './recaudacion-siaf-mef/editar-recaudacion/div-conciliar-por-monto/div-conciliar-por-monto-siaf.component';
import { ConciliarPorMontoSiafMefComponent } from './recaudacion-siaf-mef/conciliar-por-monto/conciliar-por-monto-siaf.component';
import { DivConciliarPorMontoMefComponent } from './recaudacion-siaf-mef/editar-recaudacion/div-conciliar-por-monto/div-conciliar-por-monto-mef.component';
import { ConciliarPorMontoMefComponent } from './recaudacion-siaf-mef/conciliar-por-monto/conciliar-por-monto-mef.component';

import { BandejaRecaudCciSprlComponent } from './recaudacion-cci-sprl/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionCciSprlComponent } from './recaudacion-cci-sprl/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionCciSprlComponent } from './recaudacion-cci-sprl/editar-recaudacion/editar-recaudacion.component';
import { ConciliarPorMontoCciMefComponent } from './recaudacion-cci-sprl/conciliar-por-monto/conciliar-por-monto-cci-mef.component';
import { ConciliarPorMontoCciSprlComponent } from './recaudacion-cci-sprl/conciliar-por-monto/conciliar-por-monto-cci-sprl.component';
import { DivConciliarPorMontoCciSprlComponent } from './recaudacion-cci-sprl/editar-recaudacion/div-conciliar-por-monto/div-conciliar-por-monto-cci-sprl.component';
import { DivConciliarPorMontoCciMefComponent } from './recaudacion-cci-sprl/editar-recaudacion/div-conciliar-por-monto/div-conciliar-por-monto-cci-mef.component';

import { BandejaRecaudHermesComponent } from './recaudacion-hermes/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaRecaudacionHermesComponent } from './recaudacion-hermes/nueva-recaudacion/nueva-recaudacion.component';
import { EditarRecaudacionHermesComponent } from './recaudacion-hermes/editar-recaudacion/editar-recaudacion.component';

import { BandejaBancariaAppQrSid } from './recaudacion-app-qr-sid/bandeja-recaudacion/bandeja-recaudacion.component';
import { NuevaBancariaAppQrSid } from './recaudacion-app-qr-sid/nueva-recaudacion/nueva-recaudacion.component';
import { EditarBancariaAppQrSid } from './recaudacion-app-qr-sid/editar-recaudacion/editar-recaudacion.component';


@NgModule({
  declarations: [
	BandejaRecaudPosComponent,
    NuevaRecaudacionPosComponent,
    EditarRecaudacionPosComponent, 
	BandejaRecaudPagaloPeComponent,
    NuevaRecaudacionPagaloPeComponent,
    EditarRecaudacionPagaloPeComponent,  
	BandejaRecaudMacmypeComponent,
    NuevaRecaudacionMacmypeComponent,
    EditarRecaudacionMacmypeComponent, 
	BandejaRecaudSiafMefComponent,
    NuevaRecaudacionSiafMefComponent,
    EditarRecaudacionSiafMefComponent,    
    DivConciliarPorMontoSiafMefComponent,
    ConciliarPorMontoSiafMefComponent, 
	DivConciliarPorMontoMefComponent,
	ConciliarPorMontoMefComponent,
	BandejaRecaudCciSprlComponent,
    NuevaRecaudacionCciSprlComponent,
    EditarRecaudacionCciSprlComponent,  
	BandejaRecaudHermesComponent,
    NuevaRecaudacionHermesComponent,
    EditarRecaudacionHermesComponent,
    ConciliarPorMontoCciMefComponent,
    ConciliarPorMontoCciSprlComponent,
    DivConciliarPorMontoCciSprlComponent,
    DivConciliarPorMontoCciMefComponent,
    BandejaBancariaAppQrSid,
    NuevaBancariaAppQrSid,
    EditarBancariaAppQrSid,
	],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ConsolidacionRoutingModule,
    NgPrimeModule,
    SharedModule,
    DigitOnlyModule
  ]
})
export class ConsolidacionModule { }
