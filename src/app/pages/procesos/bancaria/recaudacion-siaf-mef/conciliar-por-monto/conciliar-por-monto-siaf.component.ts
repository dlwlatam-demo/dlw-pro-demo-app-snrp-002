import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { SortEvent } from 'primeng/api';
import { IConciliacionManual } from 'src/app/interfaces/consolidacion-siaf-mef';
import { RecaudacionSiafMefService } from 'src/app/services/procesos/bancaria/recaudacion-siaf-mef.service';
import { UtilService } from 'src/app/services/util.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { GeneralService } from 'src/app/services/general.service';
import { ConciliacionManual } from 'src/app/models/procesos/bancaria/recaudacion-siaf-mef.model';
import { IResponse, IResponse2 } from 'src/app/interfaces/general.interface';

@Component({
  selector: 'app-conciliar-por-monto-siaf',
  templateUrl: './conciliar-por-monto-siaf.component.html',
  styleUrls: ['./conciliar-por-monto-siaf.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class ConciliarPorMontoSiafMefComponent implements OnInit, OnDestroy {

  unsubscribe$ = new Subject<void>(); 
  conciliacionManual: IConciliacionManual;
  bodyConciliacionManual: ConciliacionManual;
  currentPage: string = environment.currentPage;
  loadingConciliacion: boolean = false;
  readonly: boolean = false;
  listaEstado: IResponse2[];
  listaEstadoFiltro: IResponse2[];
  formRecaudacionSiafMef: FormGroup;

  razSocNombre: string = "";

  constructor(
    public config: DynamicDialogConfig, 
    public funciones: Funciones,
    private formBuilder: FormBuilder,  
    private generalService: GeneralService,
    private dialogRef: DynamicDialogRef,
    private recaudacionSiafMefService: RecaudacionSiafMefService,
    private utilService: UtilService
  ) { }

  ngOnInit(): void { 
	  this.listaEstado = []; 
	  this.construirFormulario();	
	  this.obtenerListados();
    this.conciliacionManual = new ConciliacionManual();    
    this.setearConciliacionManual(this.config.data.conciliacionManual);
  } 

  private construirFormulario() {
    this.formRecaudacionSiafMef = this.formBuilder.group({
      estado: [''],
      observacion: ['']
    });
  }
  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_EstadoSiafMefLibro()      
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resEstado]) => {
      this.listaEstado.push(...resEstado);        
	    this.listaEstadoFiltro = [...this.listaEstado];    
		
      if ( this.conciliacionManual.inCncl != '2' )
        this.readonly = true;
      else
        this.readonly = false;
      console.log("listaEstado = " + this.listaEstado.length);
      this.utilService.onCloseLoading();
	    this.formRecaudacionSiafMef.controls['estado'].setValue(this.conciliacionManual.inCncl, {onlySelf: true});
	  
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }
  
  private setearConciliacionManual(data: IConciliacionManual) {
    console.log('conci', data );   
    this.conciliacionManual.idCnclSiaf = data.idCnclSiaf;
    this.conciliacionManual.nuSecu = data.nuSecu;
    this.conciliacionManual.coClav = data.coClav;

    this.conciliacionManual.nuExpe = data.nuExpe;
    this.conciliacionManual.coCiclo = data.coCiclo;
    this.conciliacionManual.feDocu = data.feDocu;
    this.conciliacionManual.coDocu = data.coDocu;
    this.conciliacionManual.nuDocu = data.nuDocu;
    this.conciliacionManual.noRazo = data.noRazo;
    this.conciliacionManual.imDebe = (data.imDebe);
    this.conciliacionManual.imHabe = (data.imHabe);
    this.conciliacionManual.imSaldo = (data.imSaldo);

    this.conciliacionManual.deMone = data.deMone;
    this.conciliacionManual.coEsta = data.coEsta;	
    this.conciliacionManual.inCncl = data.inCncl;	
    this.conciliacionManual.nuAnoEnto = data.nuAnoEnto;
    this.conciliacionManual.nuEjecSec = data.nuEjecSec;
	
    this.conciliacionManual.tiOper = data.tiOper;
    this.conciliacionManual.nuOrdeCicl = data.nuOrdeCicl;
		
	  this.formRecaudacionSiafMef.patchValue({ "observacion": data.obCncl.trim() });

	
  }
  
  
  public seleccionarEstado(event: any) {
    if ( event.value != '2' )
      this.readonly = true;
    else
      this.readonly = false;
  }

  public grabar() {
    this.utilService.onShowProcessLoading("Procesando la data");  
    this.asignarValores();
    this.recaudacionSiafMefService.saveLibroBancosSiaf(this.bodyConciliacionManual).pipe(takeUntil(this.unsubscribe$))
    .subscribe((res: IResponse) => {    
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {         
        this.utilService.onShowAlert("success", "Atención", `Se grabó satisfactoriamente el/los registro(s).`);
        this.dialogRef.close();       
      }         
    }, (err: HttpErrorResponse) => {
      this.utilService.onShowMessageErrorSystem();
    });
  }

  private asignarValores() {
	const date = new Date(this.conciliacionManual.feDocu);
    this.bodyConciliacionManual = new ConciliacionManual();
    this.bodyConciliacionManual.idCnclSiaf = this.conciliacionManual.idCnclSiaf;
    this.bodyConciliacionManual.nuSecu = this.conciliacionManual.nuSecu;	
    this.bodyConciliacionManual.nuExpe = this.conciliacionManual.nuExpe;
    this.bodyConciliacionManual.feDocu = this.date_TO_String(date);
    this.bodyConciliacionManual.coDocu = this.conciliacionManual.coDocu;
    this.bodyConciliacionManual.nuDocu = this.conciliacionManual.nuDocu;
    this.bodyConciliacionManual.imDebe = this.conciliacionManual.imDebe;
    this.bodyConciliacionManual.inCncl = this.formRecaudacionSiafMef.get('estado')?.value;
    this.bodyConciliacionManual.obCncl = this.formRecaudacionSiafMef.get('observacion')?.value;
	
  }

  private date_TO_String(date_Object: Date): string {
  // get the year, month, date, hours, and minutes seprately and append to the string.
  let date_String: string =
    date_Object.getDate() +
    "/" +
    (date_Object.getMonth() + 1) +
    "/" +
    + date_Object.getFullYear();
  return date_String;
}


  public cancelar() {
    this.dialogRef.close();
  }  

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();                      
  }

  formatearMontoImporte( monto: number ): string {
    return monto.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

}
