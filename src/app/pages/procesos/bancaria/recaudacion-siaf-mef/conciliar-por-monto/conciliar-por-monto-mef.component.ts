import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { SortEvent } from 'primeng/api';
import { IConciliacionManual2 } from 'src/app/interfaces/consolidacion-siaf-mef';
import { RecaudacionSiafMefService } from 'src/app/services/procesos/bancaria/recaudacion-siaf-mef.service';
import { UtilService } from 'src/app/services/util.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { GeneralService } from 'src/app/services/general.service';
import { ConciliacionManual2 } from 'src/app/models/procesos/bancaria/recaudacion-siaf-mef.model';
import { IResponse, IResponse2 } from 'src/app/interfaces/general.interface';
import { Dropdown } from 'primeng/dropdown';
import { ValidatorsService } from '../../../../../core/services/validators.service';

@Component({
  selector: 'app-conciliar-por-monto-mef',
  templateUrl: './conciliar-por-monto-mef.component.html',
  styleUrls: ['./conciliar-por-monto-mef.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class ConciliarPorMontoMefComponent implements OnInit, OnDestroy {

  unsubscribe$ = new Subject<void>(); 
  conciliacionManual: IConciliacionManual2;
  bodyConciliacionManual: ConciliacionManual2;
  currentPage: string = environment.currentPage;
  loadingConciliacion: boolean = false;
  readonly: boolean = false;
  listaEstado: IResponse2[];
  listaEstadoFiltro: IResponse2[];
  formRecaudacionMef: FormGroup;

  razSocNombre: string = "";
  
  isEditar: boolean = false;

  @ViewChild('ddlEstado') ddlEstado: Dropdown;

  constructor(
    public config: DynamicDialogConfig, 
    public funciones: Funciones,
    private formBuilder: FormBuilder,  
    private generalService: GeneralService,
    private dialogRef: DynamicDialogRef,
    private recaudacionSiafMefService: RecaudacionSiafMefService,
    private utilService: UtilService
  ) { }

  ngOnInit(): void { 
	  this.listaEstado = []; 
	  this.construirFormulario();	
	  this.obtenerListados();
    this.conciliacionManual = new ConciliacionManual2();    
    this.setearConciliacionManual(this.config.data.conciliacionManual);
  } 

  private construirFormulario() {
    this.formRecaudacionMef = this.formBuilder.group({
      feBanc: [''],   
      deTran: [''],
      coTran: [''],
      imCarg: [''],
      imAbon: [''],   
      estado: [''],
      observacion: ['']
    });
  }
  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_EstadoSiafMefBanco()      
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resEstado]) => {
      this.listaEstado.push(...resEstado);        
	    this.listaEstadoFiltro = [...this.listaEstado];    

      if ( this.conciliacionManual.inCncl != '0' )
        this.readonly = true;
      else
        this.readonly = false;
      
      this.utilService.onCloseLoading();
	  this.formRecaudacionMef.controls['estado'].setValue(this.conciliacionManual.inCncl, {onlySelf: true});

	  
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }
  
  
  public seleccionarEstado(event: any) {
    if ( event.value != '2' )
      this.readonly = true;
    else
      this.readonly = false;
  }

  
  private setearConciliacionManual(data: IConciliacionManual2) {
    console.log('conci', data );   
	this.conciliacionManual.idCnclSiaf = data.idCnclSiaf;
	if(data.nuFila !== 0) {
		this.conciliacionManual.nuFila = data.nuFila;
		this.conciliacionManual.coTran = data.coTran;

		this.conciliacionManual.deTran = data.deTran;
		this.conciliacionManual.feBanc = data.feBanc;
		this.conciliacionManual.imCarg = (data.imCarg);
		this.conciliacionManual.imAbon = (data.imAbon);
		
		this.conciliacionManual.inCncl = data.inCncl;
    this.formRecaudacionMef.patchValue({ "observacion": data.obCncl.trim() });
	
		this.isEditar = true;
	
	}
	
	
  }

  public grabar() {
    this.utilService.onShowProcessLoading("Procesando la data");  
      this.asignarValores();
      this.recaudacionSiafMefService.saveEstadoBancarioMef(this.bodyConciliacionManual).pipe(takeUntil(this.unsubscribe$))
      .subscribe((res: IResponse) => {    
        if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
        } else if (res.codResult === 0) {
        this.utilService.onShowAlert("success", "Atención", `Se grabó satisfactoriamente el/los registro(s).`);
        this.dialogRef.close();       
        }         
      }, (err: HttpErrorResponse) => {
        this.utilService.onShowMessageErrorSystem();
      });
  }


  public async grabarNuevo() {
    let resultado: boolean = await this.validarFormularioValoresNuevo();
    if (!resultado) {
      return;
    }

    this.utilService.onShowProcessLoading("Procesando la data");
    this.asignarValoresNuevo();
    this.recaudacionSiafMefService.saveNewEstadoBancarioMef(this.bodyConciliacionManual).pipe(takeUntil(this.unsubscribe$))
    .subscribe((res: IResponse) => {    
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {         
        this.utilService.onShowAlert("success", "Atención", `Se grabó satisfactoriamente el/los registro(s).`);
        this.dialogRef.close();       
      }         
    }, (err: HttpErrorResponse) => {
      this.utilService.onShowMessageErrorSystem();
    });
  }
  
  private asignarValores() {
    const date = new Date(this.conciliacionManual.feBanc);
    this.bodyConciliacionManual = new ConciliacionManual2();
    this.bodyConciliacionManual.idCnclSiaf = this.conciliacionManual.idCnclSiaf;
    this.bodyConciliacionManual.nuFila = this.conciliacionManual.nuFila;
	
    this.bodyConciliacionManual.feBanc = this.date_TO_String(date);
    this.bodyConciliacionManual.deTran = this.conciliacionManual.deTran;
    this.bodyConciliacionManual.coTran = this.conciliacionManual.coTran;
    this.bodyConciliacionManual.imCarg = this.conciliacionManual.imCarg;
    this.bodyConciliacionManual.imAbon = this.conciliacionManual.imAbon;
		
    this.bodyConciliacionManual.inCncl = this.formRecaudacionMef.get('estado')?.value;
    this.bodyConciliacionManual.obCncl = this.formRecaudacionMef.get('observacion')?.value;
						
  }


  private asignarValoresNuevo() {
    const date = new Date(this.formRecaudacionMef.get('feBanc')?.value);
    this.bodyConciliacionManual = new ConciliacionManual2();
	  console.log('this.conciliacionManual.idCnclSiaf = ' + this.conciliacionManual.idCnclSiaf);
    this.bodyConciliacionManual.idCnclSiaf = this.conciliacionManual.idCnclSiaf;
    this.bodyConciliacionManual.nuFila = 0;
	
    this.bodyConciliacionManual.feBanc = this.date_TO_String(date);
    this.bodyConciliacionManual.deTran = this.formRecaudacionMef.get('deTran')?.value;
    this.bodyConciliacionManual.coTran = this.formRecaudacionMef.get('coTran')?.value;
    this.bodyConciliacionManual.imCarg = this.formRecaudacionMef.get('imCarg')?.value;
    this.bodyConciliacionManual.imAbon = this.formRecaudacionMef.get('imAbon')?.value;
		
    this.bodyConciliacionManual.inCncl = this.formRecaudacionMef.get('estado')?.value;						
						
  }

  private async validarFormularioValoresNuevo(): Promise<boolean> { 
    let registro = this.formRecaudacionMef.getRawValue();
    
    if (!registro.feBanc) {
      let respuesta = await this.utilService.onShowAlertPromise("warning", "Atención", `Seleccione una fecha.`);
      if (respuesta.isConfirmed) {
        document.getElementById('idFeBanc').focus();
        return false; 
      } else {
        return true;
      }    
    } else if (!registro.deTran) {      
      document.getElementById('deTran').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese nombre de la Transacción.");
      return false;      
    } else if (!registro.coTran) {      
      document.getElementById('coTran').focus();
      this.utilService.onShowAlert("warning", "Atención", "Ingrese Codificación/Cheque.");
      return false;      
    } else if (!registro.imCarg && !registro.imAbon) {      
      if ( !registro.imCarg ) {
        document.getElementById('imCarg').focus();
      }

      if (!registro.imAbon) {
        document.getElementById('imAbon').focus();
      }

      this.utilService.onShowAlert("warning", "Atención", "Ingrese monto de cargo o monto de abono.");
      return false;      
    } else if (!registro.estado || registro.estado === "*") {      
      this.ddlEstado.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Estado para el registro.");
      return false;      
    } else {
      return true;
    }
  }

  public ingresaCargo() {
    if ( this.formRecaudacionMef.get('imCarg')?.value ) {
      this.formRecaudacionMef.get('imAbon')?.disable();
    }
    else {
      this.formRecaudacionMef.get('imAbon')?.enable();
    }
  }

  public ingresaAbono() {
    if ( this.formRecaudacionMef.get('imAbon')?.value ) {
      this.formRecaudacionMef.get('imCarg')?.disable();
    }
    else {
      this.formRecaudacionMef.get('imCarg')?.enable();
    }
  }
  
  private date_TO_String(date_Object: Date): string {
  // get the year, month, date, hours, and minutes seprately and append to the string.
  let date_String: string =
    date_Object.getDate() +
    "/" +
    (date_Object.getMonth() + 1) +
    "/" +
    + date_Object.getFullYear();
  return date_String;
}


  public cancelar() {
    this.dialogRef.close();
  }  

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();                      
  }

  formatearMontoImporte( monto: number ): string {
    return monto.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

}
