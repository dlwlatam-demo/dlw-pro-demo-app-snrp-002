import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, first, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MessageService } from 'primeng/api';
import { SortEvent } from 'primeng/api';
import { 
  IConciliacionManual, 
  IDataFiltrosRecaudacionSiafMef, 
  IRecaudacionSiafMef, 
  IConciliacionManual2, 
  ITotalesNiubizSiafMef 
} from 'src/app/interfaces/consolidacion-siaf-mef';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { RecaudacionSiafMefService } from 'src/app/services/procesos/bancaria/recaudacion-siaf-mef.service';
import { UtilService } from 'src/app/services/util.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { ConciliarPorMontoSiafMefComponent } from '../conciliar-por-monto/conciliar-por-monto-siaf.component';
import { ConciliarPorMontoMefComponent } from '../conciliar-por-monto/conciliar-por-monto-mef.component';
import { environment } from 'src/environments/environment';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { DialogService } from 'primeng/dynamicdialog';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { GeneralService } from '../../../../../services/general.service';
@Component({
  selector: 'app-editar-recaudacion',
  templateUrl: './editar-recaudacion.component.html',
  styleUrls: ['./editar-recaudacion.component.scss'],
  providers: [ DialogService, MessageService ],
  encapsulation:ViewEncapsulation.None 
})
export class EditarRecaudacionSiafMefComponent extends BaseBandeja implements OnInit, OnDestroy {
 
  private unsubscribe$ = new Subject<void>(); 
  public index: number = 0;
  public titulo: string;
  public recaudacionSiafMef: IRecaudacionSiafMef;
  public esConsultarRegistro: boolean = false;
  public feCnclDesd: string = '';
  public feCnclHast: string = '';  
  public nombreArchivoExcel: string = '';
  public nombreArchivoExcelMef: string = '';
  public idGuidExcel1: string = '';
  public idGuidExcel2: string = '';
  public servicioOperador: string = '';
  public mostrarBloque: boolean = true;
  private tiMoviVarios: string = 'M';
  private tiMoviObservaciones: string = 'O';
  private reqs: Observable<any>[] = [];
  public listaTotalesNiubizSiafMef: ITotalesNiubizSiafMef[] = [];
  public currentPage: string = environment.currentPage;
  public loadingTotalesNiubizSiafMef: boolean = false;
  public onResetDivConciliar: boolean = false;

  public item: any;
 
  reporteExcel!: any;
  nFile!: any;
  
  constructor(
    private dialogService: DialogService,
    public funciones: Funciones,
    public recaudacionSiafMefService: RecaudacionSiafMefService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService ,
    private messageService: MessageService,
    private generalService: GeneralService
  ){ super() }

  async ngOnInit() {
    this.codBandeja = MenuOpciones.CB_Libro_Banco_SIAF;
    this.btnConsolidacionBancariaLibroBanco();
    
    this.esConsultarRegistro = await this.sharingInformationService.compartirEsConsultarSiafMefObservable.pipe(first()).toPromise()
    this.recaudacionSiafMef = await this.sharingInformationService.compartirRecaudacionSiafMefObservable.pipe(first()).toPromise();      
    this.titulo = (!this.esConsultarRegistro) ? 'Editar' : 'Consultar';
    this.feCnclDesd = this.funciones.convertirFechaDateToString(new Date (this.recaudacionSiafMef.feCnclDesd));
    this.feCnclHast = this.funciones.convertirFechaDateToString(new Date (this.recaudacionSiafMef.feCnclHast));	
    this.servicioOperador = SERVICIO_DE_PAGO.SIAF_MEF;
    this.nombreArchivoExcel = this.recaudacionSiafMef.noAdju0001;
    this.idGuidExcel1 = this.recaudacionSiafMef.idGuidDocu01;
    this.nombreArchivoExcelMef = this.recaudacionSiafMef.noAdju0002;
    this.idGuidExcel2 = this.recaudacionSiafMef.idGuidDocu02;
    this.obtenerListados();  
  }
    
  
  obtenerListados() {
    
    Swal.showLoading();
    this.recaudacionSiafMefService.getConciliacionesSiafTotal( this.recaudacionSiafMef.idCnclSiaf).subscribe({
      next: ( data ) => {
	    let data1: any[] = data;
        this.listaTotalesNiubizSiafMef = data1;
		console.log("size = " + this.listaTotalesNiubizSiafMef.length);
        this.messageService.add({
          severity: 'success', 
          summary: 'Totales cargados exitósamente', 
          detail: ''
        });
		
        Swal.close();
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'No se cuenta con totales para visualizar.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en obtener totales', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  public async obtenerDataConciliarMonto(conciliacionManual1: IConciliacionManual) {
    
    this.utilService.onShowProcessLoading("Cargando la data"); 
    conciliacionManual1.idCnclSiaf = this.recaudacionSiafMef.idCnclSiaf;
    let conciliacionManual: IConciliacionManual2;
    await this.recaudacionSiafMefService.getEditarLibroBancosSiaf(conciliacionManual1.idCnclSiaf, conciliacionManual1.nuSecu).pipe(first()).toPromise()
    .then((res: IConciliacionManual2) => {
      conciliacionManual = res;  
		this.utilService.onCloseLoading();	  
    }).catch(() => { 
      this.utilService.onShowMessageErrorSystem();
      return;   
    }); 

    if (conciliacionManual == null) { 
      this.utilService.onShowAlert("warning", "Atención", "Registro no encontrado");          
      return;
    } 
	  this.utilService.onCloseLoading();
    const dialog = this.dialogService.open(ConciliarPorMontoSiafMefComponent, {
      closable: false,
      data: {
        conciliacionManual      
      },  
      header: 'Libro Bancos - Editar',   
      width: '50%'        
    });
    dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {    
      if ( this.index === 1 ) {
        this.index = 0;
      }
      this.obtenerListados();  
      // this.onResetDivConciliar = true;      
    });     
  }

  public async obtenerDataConciliarMonto2(conciliacionManual1: IConciliacionManual) {
    
    this.utilService.onShowProcessLoading("Cargando la data"); 
    conciliacionManual1.idCnclSiaf = this.recaudacionSiafMef.idCnclSiaf;

	let conciliacionManual: IConciliacionManual2;
	let tipoMant = "Nuevo";
	if(conciliacionManual1.nuSecu !== 0) {
		tipoMant = "Editar";
		await this.recaudacionSiafMefService.getEditarEBMef(conciliacionManual1.idCnclSiaf, conciliacionManual1.nuSecu).pipe(first()).toPromise()
		.then((res: IConciliacionManual2) => {
		  conciliacionManual = res;         
		  this.utilService.onCloseLoading();
		}).catch(() => { 
		  this.utilService.onShowMessageErrorSystem();
		  return;   
		}); 

		if (conciliacionManual == null) { 
		  this.utilService.onShowAlert("warning", "Atención", "Registro no encontrado");          
		  return;
		} 
	}
	else {
		conciliacionManual = {idCnclSiaf: conciliacionManual1.idCnclSiaf, nuFila: 0, feBanc:"", deTran:"", coTran:"", imCarg: null, imAbon: null, inCncl:"", inRegi:"", obCncl: "", coUsuaCrea: null, feCrea: "", coUsuaModi: null, feModi: ""}
		
	}
	
	  this.utilService.onCloseLoading();
    const dialog = this.dialogService.open(ConciliarPorMontoMefComponent, {
      closable: false,
      data: {
        conciliacionManual      
      },  
      header: 'Estado Bancario MEF - ' + tipoMant,   
      width: '50%'        
    });
    dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      if ( this.index === 2 ) {
        this.index = 0;
      }
      this.obtenerListados();      
      // this.onResetDivConciliar = true;      
    });     
  }

  public downloadFile(tipo: number) {
	switch(tipo) {
		case 0: this.nFile = "Consolidación_LibroSIAF_NoConciliados";
		break;
		case 1: this.nFile = "Consolidación_LibroSIAF_Conciliados";
		break;
		case 3: this.nFile = "Consolidación_LibroSIAF_Anulaciones";
		break;
		default: this.nFile = "Consolidación_LibroSIAF_NoDefinido";
	}
	
	this.obtenerArchivoLibroBancos(this.recaudacionSiafMef.idCnclSiaf, tipo);
  }
  
  
  obtenerArchivoLibroBancos(idCnclSiaf: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionSiafMefService.getLibroBancosSiaf( idCnclSiaf, inCncl ).subscribe({
      next: ( data ) => {
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		this.exportExcel();
        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  
  public downloadFileTx(tipo: number) {
	switch(tipo) {
		case 0: this.nFile = "Consolidación_LibroSIAF_EEBB_NoConciliados";
		break;
		case 1: this.nFile = "Consolidación_LibroSIAF_EEBB_Conciliados";
		break;
		case 4: this.nFile = "Consolidación_LibroSIAF_EEBB_Extornos";
		break;
		case 5: this.nFile = "Consolidación_LibroSIAF_EEBB_DepositosNoIdentificados";
		break;
		case 6: this.nFile = "Consolidación_LibroSIAF_EEBB_IngresosMesesAnteriores";
		break;
		default: this.nFile = "Consolidación_LibroSIAF_EEBB_NoDefinido";
	}
	this.obtenerArchivoEB_MEF(this.recaudacionSiafMef.idCnclSiaf, tipo);
  }
  
  

  obtenerArchivoEB_MEF(idCnclSiaf: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionSiafMefService.getEstadoBancarioSiaf( idCnclSiaf, inCncl ).subscribe({
      next: ( data ) => {
	  
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		this.exportExcel();

        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  
  public handleChange(e: any) {  
      this.servicioOperador = SERVICIO_DE_PAGO.SIAF_MEF;
      this.nombreArchivoExcel = this.recaudacionSiafMef.noAdju0001;
      this.idGuidExcel1 = this.recaudacionSiafMef.idGuidDocu01;
      this.nombreArchivoExcelMef = this.recaudacionSiafMef.noAdju0002;
      this.idGuidExcel2 = this.recaudacionSiafMef.idGuidDocu02;
      this.mostrarBloque = true;
  }

  numberTransform( value: string ): string {
    return Number( value ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

  public async cancelar() {
    let dataFiltrosRecaudacionSiafMef: IDataFiltrosRecaudacionSiafMef = await this.sharingInformationService.compartirDataFiltrosRecaudacionSiafMefObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionSiafMef.esBusqueda) {
      dataFiltrosRecaudacionSiafMef.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionSiafMefObservableData = dataFiltrosRecaudacionSiafMef;
    }
    this.router.navigate(['SARF/procesos/bancaria/recaudacion_siaf_mef/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirEsConsultarSiafMefObservableData = false;
    this.sharingInformationService.compartirRecaudacionSiafMefObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();      
  }


  exportExcel() {
    console.log('Exportar Excel');

    const doc = this.reporteExcel;
	const nFile = this.nFile;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', nFile);
    document.body.appendChild( download );
    download.click();
  }


}
