import { Component, Input, Output, OnInit, OnChanges, ViewEncapsulation, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { IConciliacionManual } from 'src/app/interfaces/consolidacion-siaf-mef';
import { OPERACION_SCUNAC_NO_CONCILAIDAS } from 'src/app/models/enum/parameters';
import { UtilService } from 'src/app/services/util.service';
import { BaseBandeja } from '../../../../../../base/base-bandeja.abstract';
@Component({
  selector: 'app-div-conciliar-por-monto-mef',
  templateUrl: './div-conciliar-por-monto-mef.component.html',
  styleUrls: ['./div-conciliar-por-monto-mef.component.scss'],
  encapsulation:ViewEncapsulation.None 
})
export class DivConciliarPorMontoMefComponent implements OnInit, OnChanges {  

  @Input() onReset: boolean = false;
  @Input() showButton!: boolean;
  @Output() eventDataConciliarMonto = new EventEmitter<IConciliacionManual>(); 

  public form: FormGroup;
  public nuSecuMef: number = null;
  public igualMonto: number = OPERACION_SCUNAC_NO_CONCILAIDAS.CON_IGUAL_MONTO;
  public menorIgualMonto: number  = OPERACION_SCUNAC_NO_CONCILAIDAS.CON_MENOR_O_IGUAL_MONTO; 
  public conciliacionManual: IConciliacionManual;

  constructor(
    private formBuilder: FormBuilder,
    public funciones: Funciones,   
    private utilService: UtilService
  ) { }

  ngOnInit(): void {
    console.log('Mef', this.showButton)
    this.construirFormulario();
  }

  private construirFormulario() {
    this.form = this.formBuilder.group({
      nuSecuMef: [''],
      opcionOperacion: [this.igualMonto],         
    });
  }

  public conciliarPorMontoMef() {    
    if (this.form.get('nuSecuMef').value === null || this.form.get('nuSecuMef').value === '') {
      document.getElementById('inputItemMef').focus(); 
      this.utilService.onShowAlert("warning", "Atención", "Ingrese el nuSecu.");
      return;
    } 
    this.conciliacionManual = {  
		idCnclSiaf: null,
		nuSecu: this.form.get('nuSecuMef').value,
		coClav: null,
		nuExpe: null,
		coCiclo: null,
		feDocu: null,
		coDocu: null,
		nuDocu: null,
		noRazo: null,
		imDebe: null,
		imHabe: null,
		imSaldo: null,	
		deMone: null,
		coEsta: null,
		inCncl: null,
		nuAnoEnto: null,	
		nuEjecSec: null,
		tiOper: null,
		nuOrdeCicl: null,		
		inRegi: null,
		obCncl: null,
		coUsuaCrea: null,
		feCrea: null,
		coUsuaModi: null,
		feModi: null
    }   
    this.eventDataConciliarMonto.emit(this.conciliacionManual);   
  }
  
  
  public nuevoConciliarPorMontoMef() {    

    this.conciliacionManual = {  
		idCnclSiaf: null,
		nuSecu: 0,
		coClav: null,
		nuExpe: null,
		coCiclo: null,
		feDocu: null,
		coDocu: null,
		nuDocu: null,
		noRazo: null,
		imDebe: null,
		imHabe: null,
		imSaldo: null,	
		deMone: null,
		coEsta: null,
		inCncl: null,
		nuAnoEnto: null,	
		nuEjecSec: null,
		tiOper: null,
		nuOrdeCicl: null,		
		inRegi: null,
		obCncl: null,
		coUsuaCrea: null,
		feCrea: null,
		coUsuaModi: null,
		feModi: null
    }   
    this.eventDataConciliarMonto.emit(this.conciliacionManual);   
  }
  
   
  
  

  ngOnChanges() {
    if (this.onReset) {
      this.form.reset();
    }
  }

}
