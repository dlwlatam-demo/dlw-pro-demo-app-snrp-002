import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent } from 'primeng/api';
import { forkJoin, Subject } from 'rxjs';
import { concatMap, filter, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { UtilService } from 'src/app/services/util.service';
import { GeneralService } from 'src/app/services/general.service';
import { RecaudacionPagaloPeService } from 'src/app/services/procesos/bancaria/recaudacion-pagalo-pe.service';
import { FiltroRecaudacionPagaloPe, RecaudacionPagaloPe2 } from 'src/app/models/procesos/bancaria/recaudacion-pagalo-pe.model';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { IDataFiltrosRecaudacionPagaloPe, IRecaudacionPagaloPe, IOperacionScunac, IPostOperacionScunac, IDetalleOperacionScunac } from 'src/app/interfaces/consolidacion-pagalo-pe';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { ESTADO_REGISTRO_2 } from 'src/app/models/enum/parameters';
import { IResponse, IResponse2 } from 'src/app/interfaces/general.interface';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
@Component({
  selector: 'app-bandeja-recaudacion-pagalo-pe',
  templateUrl: './bandeja-recaudacion.component.html',
  styleUrls: ['./bandeja-recaudacion.component.scss'],
  providers: [
    DialogService  
  ]
})
export class BandejaRecaudPagaloPeComponent extends BaseBandeja implements OnInit, OnDestroy { 
  
  unsubscribe$ = new Subject<void>();
  userCode: any;
  currentPage: string = environment.currentPage;
  formRecaudacionPagaloPe: FormGroup;
  fechaMinima: Date;
  fechaMaxima: Date; 
  listaZonaRegistral: ZonaRegistral[];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[]; 
  listaEstado: IResponse2[];
  listaRecaudacion: IRecaudacionPagaloPe[];
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  selectedValuesRecaudacion: IRecaudacionPagaloPe[] = [];
  loadingRecaudacion: boolean = false;
  filtroRecaudacionPagaloPe: FiltroRecaudacionPagaloPe;
  estadoRegistro = ESTADO_REGISTRO_2;  

  //*************** listados para el filtro ***************/
  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];    
  listaEstadoFiltro: IResponse2[];
  esBusqueda: boolean = false;  

  constructor( 
    private dialogService: DialogService,
    private formBuilder: FormBuilder,  
    private generalService: GeneralService,
    private funciones: Funciones, 
    private recaudacionPagaloPeService: RecaudacionPagaloPeService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService,    
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.CB_Recaudacion_Pagalo_pe;
    this.btnConsolidacionBancariaPagaloPe();
    
    this.fechaMaxima = new Date();
    this.fechaMinima = new Date(this.fechaMaxima.getFullYear(), this.fechaMaxima.getMonth(), 1);  
    this.userCode = localStorage.getItem('user_code')||'';   
    this.construirFormulario();

    this.sharingInformationService.compartirDataFiltrosRecaudacionPagaloPeObservable.pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: IDataFiltrosRecaudacionPagaloPe) => {      
      if (data.esCancelar && data.bodyRecaudacionPagaloPe !== null) {    
        this.utilService.onShowProcessLoading("Cargando la data");
        this.setearCamposFiltro(data);
        this.buscarRecaudacion(false);
      } else {
        this.inicilializarListas();
        this.obtenerListados();
      }
    });
  }

  private construirFormulario() {
    this.formRecaudacionPagaloPe = this.formBuilder.group({
      zona: [''],
      oficina: [''],
      local: [''],      
      estado: [''],   
      fechaDesde: [this.fechaMinima],
      fechaHasta: [this.fechaMaxima]              
    });
  }

  private setearCamposFiltro(dataFiltro: IDataFiltrosRecaudacionPagaloPe) {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaEstado = [];    
    this.listaZonaRegistral = dataFiltro.listaZonaRegistral; 
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaEstado = dataFiltro.listaEstado;    
    this.filtroRecaudacionPagaloPe = dataFiltro.bodyRecaudacionPagaloPe;
    this.formRecaudacionPagaloPe.patchValue({ "zona": this.filtroRecaudacionPagaloPe.coZonaRegi});
    this.formRecaudacionPagaloPe.patchValue({ "oficina": this.filtroRecaudacionPagaloPe.coOficRegi});
    this.formRecaudacionPagaloPe.patchValue({ "local": this.filtroRecaudacionPagaloPe.coLocaAten});
    this.formRecaudacionPagaloPe.patchValue({ "estado": this.filtroRecaudacionPagaloPe.esDocu});
    this.formRecaudacionPagaloPe.patchValue({ "fechaDesde": (this.filtroRecaudacionPagaloPe.feDesd !== '' && this.filtroRecaudacionPagaloPe.feDesd !== null) ? this.funciones.convertirFechaStringToDate(this.filtroRecaudacionPagaloPe.feDesd) : this.filtroRecaudacionPagaloPe.feDesd});
    this.formRecaudacionPagaloPe.patchValue({ "fechaHasta": (this.filtroRecaudacionPagaloPe.feHast !== '' && this.filtroRecaudacionPagaloPe.feHast !== null) ? this.funciones.convertirFechaStringToDate(this.filtroRecaudacionPagaloPe.feHast) : this.filtroRecaudacionPagaloPe.feHast});    
  }

  private inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaEstado = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' });   
    this.listaEstado.push({ ccodigoHijo: '', cdescri: '(TODOS)' });   
  }

  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_Zonas_Usuario( this.userCode),     
      this.generalService.getCbo_EstadoRecaudacion()      
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resZonaRegistral, resEstado]) => { 
      if (resZonaRegistral.length === 1) {   
        this.listaZonaRegistral.push({ coZonaRegi: resZonaRegistral[0].coZonaRegi, deZonaRegi: resZonaRegistral[0].deZonaRegi });    
        this.formRecaudacionPagaloPe.patchValue({ "zona": resZonaRegistral[0].coZonaRegi});
        this.buscarOficinaRegistral();
      } else {    
        this.listaZonaRegistral.push(...resZonaRegistral);
      }     
      this.listaEstado.push(...resEstado);        
      this.buscarRecaudacion(true);    
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }

  private establecerBodyRecaudacionPagaloPe() {       
    this.filtroRecaudacionPagaloPe = new FiltroRecaudacionPagaloPe();
    this.filtroRecaudacionPagaloPe.coZonaRegi = (this.formRecaudacionPagaloPe.get('zona')?.value === '*') ? '' : this.formRecaudacionPagaloPe.get('zona')?.value;    
    this.filtroRecaudacionPagaloPe.coOficRegi = (this.formRecaudacionPagaloPe.get('oficina')?.value === '*') ? '' : this.formRecaudacionPagaloPe.get('oficina')?.value;    
    this.filtroRecaudacionPagaloPe.coLocaAten = (this.formRecaudacionPagaloPe.get('local')?.value === '*') ? '' : this.formRecaudacionPagaloPe.get('local')?.value;        
    this.filtroRecaudacionPagaloPe.esDocu = (this.formRecaudacionPagaloPe.get('estado')?.value === '*') ? '' : this.formRecaudacionPagaloPe.get('estado')?.value;
    this.filtroRecaudacionPagaloPe.feDesd = (this.formRecaudacionPagaloPe.get('fechaDesde')?.value !== "" && this.formRecaudacionPagaloPe.get('fechaDesde')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formRecaudacionPagaloPe.get('fechaDesde')?.value) : ""; 
    this.filtroRecaudacionPagaloPe.feHast = (this.formRecaudacionPagaloPe.get('fechaHasta')?.value !== "" && this.formRecaudacionPagaloPe.get('fechaHasta')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formRecaudacionPagaloPe.get('fechaHasta')?.value): "";       
  }

  public buscarRecaudacion(esCargaInicial: boolean) {
    let resultado: boolean = this.validarCamporFiltros();
    if (!resultado) {
      return;
    }
    this.listaRecaudacion = [];
    this.selectedValuesRecaudacion = [];    
    this.loadingRecaudacion = (esCargaInicial) ? false : true;
    this.guardarListadosParaFiltro();     
    this.establecerBodyRecaudacionPagaloPe();
    this.esBusqueda = (esCargaInicial) ? false : true; 
    this.recaudacionPagaloPeService.getBandejaConciliacion(this.filtroRecaudacionPagaloPe).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (data: any) => {             
      this.listaRecaudacion = data.lista;
      this.loadingRecaudacion = false;
      this.utilService.onCloseLoading();       
    }, (err: HttpErrorResponse) => {   
      this.loadingRecaudacion = false;
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("warning", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();
      }                             
    });
  }

  private validarCamporFiltros(): boolean {
    if (
      (this.formRecaudacionPagaloPe.get('fechaDesde')?.value !== "" && this.formRecaudacionPagaloPe.get('fechaDesde')?.value !== null) &&
      (this.formRecaudacionPagaloPe.get('fechaHasta')?.value === "" || this.formRecaudacionPagaloPe.get('fechaHasta')?.value === null)
    ) {
      document.getElementById('idFechaHasta').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de fin.");
      return false; 
    } else if (
      (this.formRecaudacionPagaloPe.get('fechaDesde')?.value === "" || this.formRecaudacionPagaloPe.get('fechaDesde')?.value === null) &&
      (this.formRecaudacionPagaloPe.get('fechaHasta')?.value !== "" && this.formRecaudacionPagaloPe.get('fechaHasta')?.value !== null)
    ) {
      document.getElementById('idFechaDesde').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de inicio.");
      return false; 
    } else {
      return true;
    }   
  }

  public buscarOficinaRegistral() { 
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formRecaudacionPagaloPe.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.formRecaudacionPagaloPe.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.formRecaudacionPagaloPe.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formRecaudacionPagaloPe.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.formRecaudacionPagaloPe.get('zona')?.value,this.formRecaudacionPagaloPe.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formRecaudacionPagaloPe.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err: HttpErrorResponse) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  public cambioFechaInicio() {   
    if (this.formRecaudacionPagaloPe.get('fechaHasta')?.value !== '' && this.formRecaudacionPagaloPe.get('fechaHasta')?.value !== null) {
      if (this.formRecaudacionPagaloPe.get('fechaDesde')?.value > this.formRecaudacionPagaloPe.get('fechaHasta')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.formRecaudacionPagaloPe.controls['fechaDesde'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.formRecaudacionPagaloPe.get('fechaDesde')?.value !== '' && this.formRecaudacionPagaloPe.get('fechaDesde')?.value !== null) {
      if (this.formRecaudacionPagaloPe.get('fechaHasta')?.value < this.formRecaudacionPagaloPe.get('fechaDesde')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.formRecaudacionPagaloPe.controls['fechaHasta'].reset();    
      }      
    }
  }

  public nuevo() {  
    this.router.navigate(['SARF/procesos/bancaria/recaudacion_pagalo_pe/nuevo']);
    this.sharingInformationService.irRutaBandejaRecaudacionPagaloPeObservableData = false;
  }

  public editar() {   
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      if (!(this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.REPROCESADO)) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede EDITAR un registro en estado ABIERTO o REPROCESADO");
      } else {
        this.sharingInformationService.compartirRecaudacionPagaloPeObservableData = this.selectedValuesRecaudacion[0]; 
        this.router.navigate(['SARF/procesos/bancaria/recaudacion_pagalo_pe/editar']);
        this.sharingInformationService.irRutaBandejaRecaudacionPagaloPeObservableData = false;    
      }
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }             
  }

  public anular() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosAnulados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ANULAR el registro en estado ABIERTO o REPROCESADO.");
    } else {
      let textoAnular = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ANULAR ${textoAnular}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('anular');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyAnularRecaudacion = new RecaudacionPagaloPe2();
          bodyAnularRecaudacion.trama = lista;
          this.recaudacionPagaloPeService.anulaRecaudacionPagaloPe(bodyAnularRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosAnulados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.REPROCESADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public activar() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosActivados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ACTIVAR el registro en estado ANULADO.");
    } else {
      let textoActivar = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ACTIVAR ${textoActivar}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('activar');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyActivarrRecaudacion = new RecaudacionPagaloPe2();
          bodyActivarrRecaudacion.trama = lista;
          this.recaudacionPagaloPeService.activateRecaudacionPagaloPe(bodyActivarrRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosActivados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ANULADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public abrirConciliacion() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosAbiertos()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ABRIR el registro en estado CERRADO.");
    } else {
      let textoAbrir = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ABRIR ${textoAbrir}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('abrir');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyAbrirRecaudacion = new RecaudacionPagaloPe2();
          bodyAbrirRecaudacion.trama = lista;
          this.recaudacionPagaloPeService.openRecaudacionPagaloPe(bodyAbrirRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesRecaudacion = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  private validarRegistrosAbiertos(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.CERRADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public cerrarConciliacion() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosCerrados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede CERRAR el registro en estado ABIERTO o REPROCESADO.");
    } else {
      let textoAbrir = (this.selectedValuesRecaudacion.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea CERRAR ${textoAbrir}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('cerrar');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) {
            lista.push(this.selectedValuesRecaudacion[i].idCncl.toString());           
          }      
          let bodyCerrarRecaudacion = new RecaudacionPagaloPe2();
          bodyCerrarRecaudacion.trama = lista;
          this.recaudacionPagaloPeService.closeRecaudacionPagaloPe(bodyCerrarRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {                 
              const listado = lista.map(item => this.operacionesScunac(parseInt(item)).catch((error) => { }));
              Promise.all(lista).then( respuesta => {             
                this.buscarRecaudacion(false);
                this.selectedValuesRecaudacion = [];          
              });
            }             
          }, (err: HttpErrorResponse) => {      
          });
        }
      });
    }
  }

  private operacionesScunac(idCncl: number) {
    return new Promise<any>(
      (resolve, reject) => {               
        this.recaudacionPagaloPeService.getOperacionesScunac(idCncl, 2)
          .pipe(            
            takeUntil(this.unsubscribe$),
            filter((data: any) => {           
              if (data.body.lstOperacionScunac.length > 0) {
                return true;
              } else {               
                return false;
              }
            }),
            concatMap((res: any) => {           
              let listaOperacionScunac: IOperacionScunac[] = res.body.lstOperacionScunac;      
              return this.recaudacionPagaloPeService.actualizarIdOperacionScunac(this.obtenerOperacionScunac(listaOperacionScunac))              
            })
          ).subscribe( (res: IResponse) => {
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
              reject(new Error("Error de ejecución . . . :( "));
            } else if (res.codResult === 0) { 
              resolve(idCncl);                     
            }                                                                     
          }, () => {
            reject(new Error("Error de ejecución . . . :( ")); 
          }
        ) 
      }
    );
  }

  private obtenerOperacionScunac(lista: IOperacionScunac[]): IPostOperacionScunac {
    let data: IPostOperacionScunac;
    let listaDetalle: IDetalleOperacionScunac[] = [];
    for (let i = 0; i < lista.length; i++) {
      let item: IDetalleOperacionScunac = {
        aaMvto: lista[i].anioMovimiento,
        coZonaRegi: lista[i].coZonaRegi,
        nuMvto: lista[i].numeroMovimiento,
        nuSecuDeta: lista[i].nuSecuDeta,
        idTransPos: lista[i].id        
      }
      listaDetalle.push(item);      
    }
    data = {
      trama: listaDetalle
    } 
    return data;
  }

  private validarRegistrosCerrados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesRecaudacion.length; i++) { 
      if (!(this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[i].esDocu === this.estadoRegistro.REPROCESADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public reProcesar() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      if (!(this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesRecaudacion[0].esDocu === this.estadoRegistro.REPROCESADO)) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede REPROCESAR un registro en estado ABIERTO o REPROCESADO");
      } else {
        this.sharingInformationService.compartirRecaudacionPagaloPeObservableData = this.selectedValuesRecaudacion[0]; 
        this.router.navigate(['SARF/procesos/bancaria/recaudacion_pagalo_pe/reprocesar']);
        this.sharingInformationService.irRutaBandejaRecaudacionPagaloPeObservableData = false; 
      }           
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }
  }

  public consultarRegistro() {
    if (this.selectedValuesRecaudacion.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (this.selectedValuesRecaudacion.length === 1) {
      this.sharingInformationService.compartirEsConsultarPagaloPeObservableData = true; 
      this.sharingInformationService.compartirRecaudacionPagaloPeObservableData = this.selectedValuesRecaudacion[0];
      this.router.navigate(['SARF/procesos/bancaria/recaudacion_pagalo_pe/consultar']);
      this.sharingInformationService.irRutaBandejaRecaudacionPagaloPeObservableData = false;      
    } else if (this.selectedValuesRecaudacion.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }
  }

  customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;
        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
        return (event.order! * result);
    });
  }

  private guardarListadosParaFiltro() {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaEstadoFiltro = [...this.listaEstado];    
  }

  private establecerDataFiltrosRecaudacionME() : IDataFiltrosRecaudacionPagaloPe {
    let dataFiltrosRecaudacionME: IDataFiltrosRecaudacionPagaloPe = {
      listaZonaRegistral: this.listaZonaRegistralFiltro, 
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,      
      listaEstado: this.listaEstadoFiltro,
      bodyRecaudacionPagaloPe: this.filtroRecaudacionPagaloPe,   
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosRecaudacionME;
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();   
    this.sharingInformationService.compartirDataFiltrosRecaudacionPagaloPeObservableData = this.establecerDataFiltrosRecaudacionME();                    
  }

}
