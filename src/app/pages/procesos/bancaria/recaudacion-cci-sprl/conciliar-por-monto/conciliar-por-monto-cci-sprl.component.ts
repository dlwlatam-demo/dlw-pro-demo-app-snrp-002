import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject, forkJoin } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Dropdown } from 'primeng/dropdown';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import { IConciliacionCCISprl, IConciliacionListaSPRL } from '../../../../../interfaces/consolidacion-cci-sprl';
import { IResponse2, IResponse } from '../../../../../interfaces/general.interface';

import { ConciliacionManualSprlCci } from '../../../../../models/procesos/bancaria/recaudacion-cci-sprl.model';

import { Funciones } from '../../../../../core/helpers/funciones/funciones';

import { UtilService } from '../../../../../services/util.service';
import { GeneralService } from '../../../../../services/general.service';
import { RecaudacionCciSprlService } from '../../../../../services/procesos/bancaria/recaudacion-cci-sprl.service';

@Component({
  selector: 'app-conciliar-por-monto-cci-sprl',
  templateUrl: './conciliar-por-monto-cci-sprl.component.html',
  styleUrls: ['./conciliar-por-monto-cci-sprl.component.scss']
})
export class ConciliarPorMontoCciSprlComponent implements OnInit {

  isEditar: boolean = false;
  readonly: boolean = false;
  razSocNombre: string = '';
  loadingConciliacion: boolean = false;

  formRecaudacionCCI!: FormGroup;

  conciliacionManual!: IConciliacionCCISprl;
  bodyConciliacionManual!: ConciliacionManualSprlCci;

  listaEstado!: IResponse2[];
  listaEstadoFiltro!: IResponse2[];

  unsubscribe$ = new Subject<void>();

  @ViewChild('ddlEstado') ddlEstado: Dropdown;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    public funciones: Funciones,
    private utilService: UtilService,
    private generalService: GeneralService,
    private recaudacionViaCCIService: RecaudacionCciSprlService
  ) { }

  ngOnInit(): void {
    this.listaEstado = [];
    this.construirFormulario();
    this.obtenerListados();
    this.conciliacionManual = new ConciliacionManualSprlCci();
    this.setearConciliacionManual( this.config.data.conciliacionManual2 );
  }

  private construirFormulario() {
    this.formRecaudacionCCI = this.fb.group({
      estado: [''],
      observacion: ['']
    });
  }

  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_EstadoSiafMefBanco()      
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resEstado]) => {
      this.listaEstado.push(...resEstado);        
	    this.listaEstadoFiltro = [...this.listaEstado];    

      if ( this.conciliacionManual.inCncl != '0' )
        this.readonly = true;
      else
        this.readonly = false;
      
      this.utilService.onCloseLoading();
	    this.formRecaudacionCCI.controls['estado'].setValue(this.conciliacionManual.inCncl, {onlySelf: true});

	  
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }

  private setearConciliacionManual( data: IConciliacionListaSPRL ) {
    this.conciliacionManual.idCncl = data.lista[0].idCncl;
    if ( data.lista[0].nuSecu !== 0 ) {
      this.conciliacionManual.nuSecu = data.lista[0].nuSecu;

      this.conciliacionManual.usrId = data.lista[0].usrId;
      this.conciliacionManual.fecOperacionBan = data.lista[0].fecOperacionBan;
      this.conciliacionManual.tsCrea = data.lista[0].tsCrea;

      this.conciliacionManual.usrCaja = data.lista[0].usrCaja;
      this.conciliacionManual.personaId = data.lista[0].personaId;
      this.conciliacionManual.tpoPers = data.lista[0].tpoPers;
      this.conciliacionManual.noRazoSoci = data.lista[0].noRazoSoci;

      this.conciliacionManual.monto = (data.lista[0].monto);
      this.conciliacionManual.numOperacionBan = data.lista[0].numOperacionBan;

      this.conciliacionManual.inCncl = data.lista[0].inCncl;
      this.formRecaudacionCCI.patchValue({ 'observacion': data.lista[0].obCncl.trim() });

      this.isEditar = true;
    }
  }

  public grabar() {
    this.utilService.onShowProcessLoading('Procesando la data');
    this.asignarValores();
    this.recaudacionViaCCIService.saveEstadoBancarioCCISprl( this.bodyConciliacionManual ).pipe( takeUntil( this.unsubscribe$ ))
      .subscribe( ( res: IResponse ) => {
        if ( res.codResult < 0 ) {
          this.utilService.onShowAlert('info', 'Atención', res.msgResult);
        } else if ( res.codResult === 0 ) {
          this.utilService.onShowAlert('success', 'Atención', 'Se grabó satisfactoriamente el/los registro(s).');
          this.ref.close();
        }
      }, ( err: HttpErrorResponse ) => {
        this.utilService.onShowMessageErrorSystem();
      });
  }

  private asignarValores() {
    this.bodyConciliacionManual = new ConciliacionManualSprlCci();
    this.bodyConciliacionManual.idCncl = this.conciliacionManual.idCncl;
    this.bodyConciliacionManual.nuSecu = this.conciliacionManual.nuSecu;

    this.bodyConciliacionManual.inCncl = this.formRecaudacionCCI.get('estado')?.value;
    this.bodyConciliacionManual.obCncl = this.formRecaudacionCCI.get('observacion')?.value;
  }

  public cancelar() {
    this.ref.close();
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();                      
  }

  formatearMontoImporte( monto: number ): string {
    return monto.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }
}
