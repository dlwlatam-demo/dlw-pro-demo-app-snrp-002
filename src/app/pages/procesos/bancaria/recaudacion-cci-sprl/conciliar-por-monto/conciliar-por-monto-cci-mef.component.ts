import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject, forkJoin } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Dropdown } from 'primeng/dropdown';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import { IConciliacionCCIMef, IConciliacionListaMEF } from '../../../../../interfaces/consolidacion-cci-sprl';
import { IResponse2, IResponse } from '../../../../../interfaces/general.interface';

import { ConciliacionManualMefCci } from '../../../../../models/procesos/bancaria/recaudacion-cci-sprl.model';

import { Funciones } from '../../../../../core/helpers/funciones/funciones';

import { UtilService } from '../../../../../services/util.service';
import { GeneralService } from '../../../../../services/general.service';
import { RecaudacionCciSprlService } from '../../../../../services/procesos/bancaria/recaudacion-cci-sprl.service';

@Component({
  selector: 'app-conciliar-por-monto-cci-mef',
  templateUrl: './conciliar-por-monto-cci-mef.component.html',
  styleUrls: ['./conciliar-por-monto-cci-mef.component.scss']
})
export class ConciliarPorMontoCciMefComponent implements OnInit {

  isEditar: boolean = false;
  readonly: boolean = false;
  razSocNombre: string = '';
  loadingConciliacion: boolean = false;
  
  formRecaudacionCCI!: FormGroup;

  conciliacionManual!: IConciliacionCCIMef;
  bodyConciliacionManual!: ConciliacionManualMefCci;
  
  listaEstado!: IResponse2[];
  listaEstadoFiltro!: IResponse2[];
  
  unsubscribe$ = new Subject<void>();

  @ViewChild('ddlEstado') ddlEstado: Dropdown;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    public funciones: Funciones,
    private utilService: UtilService,
    private generalService: GeneralService,
    private recaudacionViaCCIService: RecaudacionCciSprlService
  ) { }

  ngOnInit(): void {
    this.listaEstado = [];
    this.construirFormulario();
    this.obtenerListados();
    this.conciliacionManual = new ConciliacionManualMefCci();
    this.setearConciliacionManual( this.config.data.conciliacionManual2 );
  }

  private construirFormulario() {
    this.formRecaudacionCCI = this.fb.group({
      feBanc: [''],   
      deTran: [''],
      coTran: [''],
      imCarg: [''],
      imAbon: [''],   
      estado: [''],
      observacion: ['']
    });
  }

  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_EstadoSiafMefBanco()      
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resEstado]) => {
      this.listaEstado.push(...resEstado);        
	    this.listaEstadoFiltro = [...this.listaEstado];    

      if ( this.conciliacionManual.inCncl != '0' )
        this.readonly = true;
      else
        this.readonly = false;
      
      this.utilService.onCloseLoading();
	    this.formRecaudacionCCI.controls['estado'].setValue(this.conciliacionManual.inCncl, {onlySelf: true});

	  
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }

  private setearConciliacionManual(data: IConciliacionListaMEF) {
    this.conciliacionManual.idCncl = data.lista[0].idCncl;
    if(data.lista[0].nuFila !== 0) {
      this.conciliacionManual.nuFila = data.lista[0].nuFila;
      this.conciliacionManual.coTran = data.lista[0].coTran;

      this.conciliacionManual.deTran = data.lista[0].deTran;
      this.conciliacionManual.feBanc = data.lista[0].feBanc;
      this.conciliacionManual.imCarg = (data.lista[0].imCarg);
      this.conciliacionManual.imAbon = (data.lista[0].imAbon);
      
      this.conciliacionManual.inCncl = data.lista[0].inCncl;
      this.formRecaudacionCCI.patchValue({ "observacion": data.lista[0].obCncl.trim() });

      this.isEditar = true;
    }
  }

  public grabar() {
    this.utilService.onShowProcessLoading('Procesando la data');
    this.asignarValores();
    this.recaudacionViaCCIService.saveEstadoBancarioCCIMef( this.bodyConciliacionManual ).pipe( takeUntil( this.unsubscribe$ ))
      .subscribe( ( res: IResponse ) => {
        if ( res.codResult < 0 ) {
          this.utilService.onShowAlert('info', 'Atención', res.msgResult);
        } else if ( res.codResult === 0 ) {
          this.utilService.onShowAlert('success', 'Atención', 'Se grabó satisfactoriamente el/los registro(s).');
          this.ref.close();
        }
      }, ( err: HttpErrorResponse ) => {
        this.utilService.onShowMessageErrorSystem();
      });
  }

  private asignarValores() {
    this.bodyConciliacionManual = new ConciliacionManualMefCci();
    this.bodyConciliacionManual.idCncl = this.conciliacionManual.idCncl;
    this.bodyConciliacionManual.nuFila = this.conciliacionManual.nuFila;

    this.bodyConciliacionManual.inCncl = this.formRecaudacionCCI.get('estado')?.value;
    this.bodyConciliacionManual.obCncl = this.formRecaudacionCCI.get('observacion')?.value;
  }

  public cancelar() {
    this.ref.close();
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();                      
  }

  formatearMontoImporte( monto: number ): string {
    return monto.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }
}
