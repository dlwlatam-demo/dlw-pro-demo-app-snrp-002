import { Component, OnInit, OnDestroy, ViewEncapsulation, Inject, LOCALE_ID } from '@angular/core';
import { formatNumber } from '@angular/common';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { catchError, first, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MessageService } from 'primeng/api';
import { SortEvent } from 'primeng/api';
import { 
  IConciliacionListaMEF,
  IConciliacionManual, 
  IDataFiltrosRecaudacionCciSprl, 
  IRecaudacionCciSprl, 
  ITotales, 
  ITotalesNiubizCciSprl 
} from 'src/app/interfaces/consolidacion-cci-sprl';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { RecaudacionCciSprlService } from 'src/app/services/procesos/bancaria/recaudacion-cci-sprl.service';
import { UtilService } from 'src/app/services/util.service';
import { SERVICIO_DE_PAGO } from 'src/app/models/enum/parameters';
import { environment } from 'src/environments/environment';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { DialogService } from 'primeng/dynamicdialog';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { GeneralService } from '../../../../../services/general.service';
import { ConciliarPorMontoCciMefComponent } from '../conciliar-por-monto/conciliar-por-monto-cci-mef.component';
import { IConciliacionListaSPRL } from '../../../../../interfaces/consolidacion-cci-sprl';
import { ConciliarPorMontoCciSprlComponent } from '../conciliar-por-monto/conciliar-por-monto-cci-sprl.component';
@Component({
  selector: 'app-editar-recaudacion-cci-sprl',
  templateUrl: './editar-recaudacion.component.html',
  styleUrls: ['./editar-recaudacion.component.scss'],
  providers: [ DialogService, MessageService ],
  encapsulation:ViewEncapsulation.None 
})
export class EditarRecaudacionCciSprlComponent extends BaseBandeja implements OnInit, OnDestroy {
 
  private unsubscribe$ = new Subject<void>(); 
  public titulo: string;
  public recaudacionCciSprl: IRecaudacionCciSprl;
  public esConsultarRegistro: boolean = false;
  public fecha: string = '';
  public nombreArchivoExcel: string = '';
  public idGuidExcel: string = '';
  public servicioOperador: string = '';
  public mostrarBloque: boolean = true;
  private tiMoviVarios: string = 'M';
  private tiMoviObservaciones: string = 'O';
  private reqs: Observable<any>[] = [];
  public listaTotalesNiubizCciSprl: ITotalesNiubizCciSprl[] = [];
  public currentPage: string = environment.currentPage;
  public loadingTotalesNiubizCciSprl: boolean = false;
  public onResetDivConciliar: boolean = false;
  
  public sprlTotalRegis: string = '0.00';
  public sprlTotalRecaudacion: string = '0.00';
  public bancarioTotalRegis: string = '0.00';
  public bancarioTotalCargos: string = '0.00';
  public bancarioTotalAbonos: string = '0.00';
  public diferencia: string = '0.00';

  public item: any;
 
  reporteExcel!: any;
  nFile!: any;
  
  constructor(
    private dialogService: DialogService,
    public funciones: Funciones,
    public recaudacionCciSprlService: RecaudacionCciSprlService,
    private router: Router,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService ,
    private messageService: MessageService,
    private generalService: GeneralService,
	@Inject(LOCALE_ID) public locale: string
  ){ super() }

  async ngOnInit() {
    this.codBandeja = MenuOpciones.CB_Recaudacion_Transferencia_via_CCI;
    this.btnConsolidacionBancariaTransferenciaCCI();
    
    this.esConsultarRegistro = await this.sharingInformationService.compartirEsConsultarCciSprlObservable.pipe(first()).toPromise()
    this.recaudacionCciSprl = await this.sharingInformationService.compartirRecaudacionCciSprlObservable.pipe(first()).toPromise();      
    this.titulo = (!this.esConsultarRegistro) ? 'Editar' : 'Consultar';
    this.fecha = this.funciones.convertirFechaDateToString(new Date (this.recaudacionCciSprl.feCncl));
	
	
	this.getBancariaTotalesCciSprl(this.recaudacionCciSprl.idCncl);
  this.handleChange('');
	
  }


  public downloadFile(tipo: number) {
	switch(tipo) {
		case 0: this.nFile = "Consolidación_TransferenciaViaCCI_EEBB_NoConciliados";
		break;
		case 1: this.nFile = "Consolidación_TransferenciaViaCCI_EEBB_Conciliados";
		break;
		default: this.nFile = "Consolidación_TransferenciaViaCCI_EEBB_NoDefinido";
	}
	
	this.obtenerArchivoEBMEF(this.recaudacionCciSprl.idCncl, tipo);
  }
  
  
  obtenerArchivoEBMEF(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionCciSprlService.getRecaudacionEBMEF( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		this.exportExcel();
        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }

  public async obtenerDataConciliarMonto( conciliacionManual: IConciliacionManual ) {
    this.utilService.onShowProcessLoading('Cargando la data');
    conciliacionManual.idCncl = this.recaudacionCciSprl.idCncl;

    let conciliacionManual2: IConciliacionListaMEF;

    if ( conciliacionManual.nuSecu !== 0 ) {
      await this.recaudacionCciSprlService.getEditarBancariaMEF( conciliacionManual.idCncl, conciliacionManual.nuSecu ).pipe( first() ).toPromise()
        .then(( res: IConciliacionListaMEF ) => {
          conciliacionManual2 = res;
          this.utilService.onCloseLoading();
        }).catch( () => {
          this.utilService.onShowMessageErrorSystem();
          return;
        });

      if ( conciliacionManual2 == null ) {
        this.utilService.onShowAlert("warning", "Atención", "Registro no encontrado");          
		    return;
      }
    }

    this.utilService.onCloseLoading();
    const dialog = this.dialogService.open( ConciliarPorMontoCciMefComponent, {
      header: 'Transferencia vía CCI - Conciliación Manual',
      width: '52%',
      closable: false,
      data: {
        conciliacionManual2
      }
    });

    dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      // this.onResetDivConciliar = true;      
    });
  }

  public async obtenerDataConciliarMonto2( conciliacionManual: IConciliacionManual ) {
    this.utilService.onShowProcessLoading('Cargando la data');
    conciliacionManual.idCncl = this.recaudacionCciSprl.idCncl;

    let conciliacionManual2: IConciliacionListaSPRL;

    if ( conciliacionManual.nuSecu !== 0 ) {
      await this.recaudacionCciSprlService.getEditarBancariaSPRL( conciliacionManual.idCncl, conciliacionManual.nuSecu ).pipe( first() ).toPromise()
        .then(( res: IConciliacionListaSPRL ) => {
          conciliacionManual2 = res;
          this.utilService.onCloseLoading();
        }).catch( () => {
          this.utilService.onShowMessageErrorSystem();
          return;
        });

      if ( conciliacionManual2 == null ) {
        this.utilService.onShowAlert("warning", "Atención", "Registro no encontrado");          
		    return;
      }
    }

    this.utilService.onCloseLoading();
    const dialog = this.dialogService.open( ConciliarPorMontoCciSprlComponent, {
      header: 'Transferencia vía CCI - Conciliación Manual',
      width: '52%',
      closable: false,
      data: {
        conciliacionManual2
      }
    });

    dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      // this.onResetDivConciliar = true;      
    });
  }
  
  
  public downloadFileTx(tipo: number) {
	switch(tipo) {
		case 1: this.nFile = "Consolidación_TransferenciaViaCCI_SPRL_Conciliados";
		break;
		case 0: this.nFile = "Consolidación_TransferenciaViaCCI_SPRL_NoConciliados";
		break;
		default: this.nFile = "Consolidación_TransferenciaViaCCI_SPRL_NoDefinido";
	}
	this.obtenerArchivoTxSprl(this.recaudacionCciSprl.idCncl, tipo);
  }
  
  

  obtenerArchivoTxSprl(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.recaudacionCciSprlService.getArchivosTxSprl( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        console.log('dataSPRL', data)
	  
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		this.exportExcel();

        Swal.close();
        //this.disableButton = false;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  
  
  getBancariaTotalesCciSprl(idCncl: number) {
    
    Swal.showLoading();
    this.recaudacionCciSprlService.getBancariaTotalesCciSprl( idCncl ).subscribe({
      next: ( data ) => {
		this.setTotalesCciSprl(data);
        Swal.close();
		
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en obtener Totales', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }
  
  setTotalesCciSprl(data: ITotales[]) {
    console.log("totales = " + data.length);
    if(data.length == 3) {
		let sprlTotales:ITotales = data.find(x => x.coSecc == "S.1");
		let bancarioTotales:ITotales = data.find(x => x.coSecc == "S.2");
		let diferencias:ITotales = data.find(x => x.coSecc == "S.3");
		
		this.sprlTotalRegis = formatNumber(sprlTotales.nuRegi,this.locale);
		this.sprlTotalRecaudacion = formatNumber(sprlTotales.imTota,this.locale, '3.2-2');
		this.bancarioTotalRegis = formatNumber(bancarioTotales.nuRegi,this.locale);
		this.bancarioTotalCargos = this.formatearMontoImporte(String(bancarioTotales.imCarg)) 
    //formatNumber(bancarioTotales.imCarg,this.locale, '3.2-2');
		this.bancarioTotalAbonos = formatNumber(bancarioTotales.imAbon,this.locale, '3.2-2');
		
		this.diferencia = formatNumber(diferencias.imTota,this.locale, '3.2-2');
		
	}
  
  }
  
  public handleChange(e: any) {  
      this.servicioOperador = SERVICIO_DE_PAGO.PAGALO_PE;
      this.nombreArchivoExcel = this.recaudacionCciSprl.noAdju0001;
      this.idGuidExcel = this.recaudacionCciSprl.idGuidDocu01;
      this.mostrarBloque = true;
  }

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
      let value1 = data1[event.field!];
      let value2 = data2[event.field!];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order! * result);
    });
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

  public async cancelar() {
    let dataFiltrosRecaudacionCciSprl: IDataFiltrosRecaudacionCciSprl = await this.sharingInformationService.compartirDataFiltrosRecaudacionCciSprlObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecaudacionCciSprl.esBusqueda) {
      dataFiltrosRecaudacionCciSprl.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosRecaudacionCciSprlObservableData = dataFiltrosRecaudacionCciSprl;
    }
    this.router.navigate(['SARF/procesos/bancaria/recaudacion_cci_sprl/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirEsConsultarCciSprlObservableData = false;
    this.sharingInformationService.compartirRecaudacionCciSprlObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();      
  }


  exportExcel() {
    console.log('Exportar Excel');

    const doc = this.reporteExcel;
	const nFile = this.nFile;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', nFile);
    document.body.appendChild( download );
    download.click();
  }


}
