import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { IConciliacionManual } from '../../../../../../interfaces/consolidacion-cci-sprl';

import { OPERACION_SCUNAC_NO_CONCILAIDAS } from '../../../../../../models/enum/parameters';

import { Funciones } from '../../../../../../core/helpers/funciones/funciones';
import { UtilService } from '../../../../../../services/util.service';

@Component({
  selector: 'app-div-conciliar-por-monto-cci-sprl',
  templateUrl: './div-conciliar-por-monto-cci-sprl.component.html',
  styleUrls: ['./div-conciliar-por-monto-cci-sprl.component.scss']
})
export class DivConciliarPorMontoCciSprlComponent implements OnInit {

  @Input() onReset: boolean = false;
  @Input() showButton!: boolean;
  @Output() eventDataConciliarMonto = new EventEmitter<IConciliacionManual>();

  public form: FormGroup;
  public nuSecuMef: number = null;
  public igualMonto: number = OPERACION_SCUNAC_NO_CONCILAIDAS.CON_IGUAL_MONTO;
  public menorIgualMonto: number = OPERACION_SCUNAC_NO_CONCILAIDAS.CON_MENOR_O_IGUAL_MONTO;
  public conciliacionManual: IConciliacionManual;

  constructor(
    private fb: FormBuilder,
    public funciones: Funciones,
    private utilService: UtilService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      nuSecuMef: [''],
      opcionOperacion: [this.igualMonto],         
    });
  }

  public conciliarPorMontoMef() {
    if ( this.form.get('nuSecuMef').value === null || this.form.get('nuSecuMef').value === '' ) {
      document.getElementById('inputItemMef').focus(); 
      this.utilService.onShowAlert("warning", "Atención", "Ingrese el nuSecu.");
      return;
    }

    this.conciliacionManual = {    
      idCncl: null,
      nuSecu: this.form.get('nuSecuMef').value,
      feCncl: null,
      idUsua: null,
      razSoc: null,
      primerApellido: null,
      segundoApellido: null,
      nombres: null,	
      monto: null,
      idPagoLinea: null,
      eTicket: null,
      inCncl: null,
      obCnclCciSprl: null,
    }

    this.eventDataConciliarMonto.emit( this.conciliacionManual );
  }

}
