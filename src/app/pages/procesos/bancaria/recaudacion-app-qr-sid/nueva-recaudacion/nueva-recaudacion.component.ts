import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Subject, first, forkJoin, takeUntil } from 'rxjs';
import { ZonaRegistral, OficinaRegistral, Local } from '../../../../../interfaces/combos.interface';
import { SERVICIO_DE_PAGO } from '../../../../../models/enum/parameters';
import { ArchivoAdjunto } from '../../../../../models/archivo-adjunto.model';
import { BancariaAppQrSid } from '../../../../../models/procesos/bancaria/recaudacion-bancaria-app-qr-sid.model';
import { IBancariaAppQrSid, IDataFileExcel, IDataFiltrosBancariaAppQrSid } from '../../../../../interfaces/consolidacion-bancaria-app-qr-sid.interface';
import { IResponse2, IResponse } from '../../../../../interfaces/general.interface';
import { Dropdown } from 'primeng/dropdown';
import { GeneralService } from '../../../../../services/general.service';
import { Router } from '@angular/router';
import { Funciones } from '../../../../../core/helpers/funciones/funciones';
import { UtilService } from '../../../../../services/util.service';
import { RecaudacionBancariaAppQrSidService } from '../../../../../services/procesos/bancaria/recaudacion-bancaria-app-qr-sid.service';
import { SharingInformationService } from '../../../../../core/services/sharing-services.service';
import { HttpErrorResponse } from '@angular/common/http';
import { filter, concatMap } from 'rxjs/operators';
import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';

@Component({
  selector: 'app-nueva-recaudacion',
  templateUrl: './nueva-recaudacion.component.html',
  styleUrls: ['./nueva-recaudacion.component.scss']
})
export class NuevaBancariaAppQrSid implements OnInit, OnDestroy {

  public titulo!: string;
  private userCode!: any;
  public sizeFiles: number = 0;
  public nameBancario: string = '';
  public servicioOperador: string = '';
  public tipoConciliacion: string = '';
  public loadingLocal: boolean = false;
  public loadingOficinaRegistral: boolean = false;

  public SERVICIO_BANCARIA = SERVICIO_DE_PAGO.BANCARIA_APP_QR_SID;

  private bancariaAppQrSid!: BancariaAppQrSid;
  public reprocesarBancariaAppQrSid!: IBancariaAppQrSid;

  public files: ArchivoAdjunto[] = [];

  public formBancariaAppQrSid!: FormGroup;

  public fechaMaxima!: Date;

  public listaLocal!: Local[];
  public listaZonaRegistral!: ZonaRegistral[];
  public listaOficinaRegistral!: OficinaRegistral[];
  public listaTipoConciliacion!: IResponse2[];
  
  private unsubscribe$ = new Subject<void>();

  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private funciones: Funciones,
    private utilService: UtilService,
    private generalService: GeneralService,
    private sharingInformationService: SharingInformationService,
    private bancariaAppQrSidService: RecaudacionBancariaAppQrSidService
  ) { }

  async ngOnInit() {
    this.fechaMaxima = new Date();
    this.listaTipoConciliacion = [];

    this.construirFormulario();
    this.reprocesarBancariaAppQrSid = await this.sharingInformationService.compartirBancariaAppQrSidObservable.pipe( first() ).toPromise();

    if ( !this.reprocesarBancariaAppQrSid ) {
      this.titulo = 'Nuevo';
      this.userCode = localStorage.getItem('user_code') || '';
      this.inicilializarListas();
      this.obtenerListados();
      this.obtenerZonasRegistrales();
    } else {
      this.titulo = 'Reprocesar';

      switch( this.reprocesarBancariaAppQrSid.idTipoCncl ) {
        case 15:
          this.tipoConciliacion = 'APP SUNARP';
           break;
        case 16:
          this.tipoConciliacion = 'CÓDIGO QR';
           break;
        case 17:
          this.tipoConciliacion = 'SID';
           break;
        default:
          this.tipoConciliacion = '';
      }
    }

    this.bancariaAppQrSid = new BancariaAppQrSid();
  }

  private construirFormulario() {
    this.formBancariaAppQrSid = this.fb.group({
      zona: [''],
      oficina: [''],
      local: [''],
      tipoConciliacion: [''],
      fecha: [this.fechaMaxima],
      docs: this.fb.array([])
    });
  }

  private inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaTipoConciliacion = [];
    this.listaLocal = [];     
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    this.listaTipoConciliacion.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' });
  }

  private obtenerListados() {
    this.utilService.onShowProcessLoading("Cargando la data");
    forkJoin(
      this.generalService.getCbo_TipoConciliacionBancaria()   
    ).pipe( takeUntil(this.unsubscribe$) )
     .subscribe(([ resTipoConciliacion ]) => {
      if ( resTipoConciliacion.length === 1 ) {
        this.listaTipoConciliacion.push({ ccodigoHijo: resTipoConciliacion[0].ccodigoHijo, cdescri: resTipoConciliacion[0].cdescri });
        this.formBancariaAppQrSid.patchValue({ 'tipoConciliacion': resTipoConciliacion[0].ccodigoHijo });
      } else {
        this.listaTipoConciliacion.push(...resTipoConciliacion);
		    this.formBancariaAppQrSid.patchValue({ 'tipoConciliacion': resTipoConciliacion[0].ccodigoHijo });
      }
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }

  private obtenerZonasRegistrales() {
    this.utilService.onShowProcessLoading("Cargando la data");
    this.generalService.getCbo_Zonas_Usuario( this.userCode).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (res: ZonaRegistral[]) => {  
      if (res.length === 1) {   
        this.listaZonaRegistral.push({ coZonaRegi: res[0].coZonaRegi, deZonaRegi: res[0].deZonaRegi });    
        this.formBancariaAppQrSid.patchValue({ "zona": res[0].coZonaRegi});
        this.buscarOficinaRegistral();
      } else {    
        this.listaZonaRegistral.push(...res);
      }       
      this.utilService.onCloseLoading();
    }, (err: HttpErrorResponse) => {   
      this.utilService.onCloseLoading();                                  
    });
  }

  public buscarOficinaRegistral() { 
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formBancariaAppQrSid.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.formBancariaAppQrSid.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (res: OficinaRegistral[]) => {  
        if (res.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: res[0].coOficRegi, deOficRegi: res[0].deOficRegi });    
          this.formBancariaAppQrSid.patchValue({ "oficina": res[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...res);
        }
        this.loadingOficinaRegistral = false;     
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formBancariaAppQrSid.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.formBancariaAppQrSid.get('zona')?.value,this.formBancariaAppQrSid.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formBancariaAppQrSid.patchValue({ "local": data[0].coLocaAten});         
        } else {
          this.listaLocal.push(...data);          
        }
        this.loadingLocal = false;       
      }, (err: HttpErrorResponse) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  public obtenerDataFileExcel(event: IDataFileExcel) {
    console.log('eventObtener', event);
    if (event.servicioDePago === this.SERVICIO_BANCARIA) {
      this.nameBancario = event.nameFile;
      this.bancariaAppQrSid.noAdju0001 = event.nameFile;
      this.bancariaAppQrSid.idGuidDocu01 = event.idGuid;
      this.bancariaAppQrSid.trama01 = event.lista;
    } 
  }

  public async procesar() {
    let resultado: boolean = await this.validarFormularioNuevo();
    if (!resultado) {      
      return;
    }     
    this.utilService.onShowProcessSaveEdit(); 
    this.asignarValoresNuevo();
    this.bancariaAppQrSid.tiProc = '0'; // '0': es validar data, '1': es insertar

    this.bancariaAppQrSidService.procesarBancariaAppQrSid(this.bancariaAppQrSid).pipe(      
      takeUntil(this.unsubscribe$), 
      filter((res1: IResponse) => { 
        if (res1.codResult < 0 ) {        
          this.utilService.onShowAlert("warning", "Atención", res1.msgResult);        
          return false;
        } else {          
          return true 
        } 
      }),     
      concatMap((res1: IResponse) => {   
        this.bancariaAppQrSid.tiProc = '1'; // '0': es validar data, '1': es insertar     
        return this.bancariaAppQrSidService.procesarBancariaAppQrSid(this.bancariaAppQrSid) 
      })
    ).subscribe((res: IResponse) => {     
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {  
        this.parse();       
        this.respuestaExitosaGrabar('PROCESÓ');
      }         
    }, (err: HttpErrorResponse) => {
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("error", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();        
      }
    });
  }

  public async reprocesar() {
    let resultado: boolean = await this.validarFormularioReprocesar();
    if (!resultado) {      
      return;
    }     
    this.utilService.onShowProcessSaveEdit(); 
    this.asignarValoresReprocesar();
    this.bancariaAppQrSid.tiProc = '0'; // '0': es validar data, '1': es insertar
    this.bancariaAppQrSidService.reprocesaBancariaAppQrSid(this.bancariaAppQrSid).pipe(      
      takeUntil(this.unsubscribe$), 
      filter((res1: IResponse) => { 
        if (res1.codResult < 0 ) {        
          this.utilService.onShowAlert("warning", "Atención", res1.msgResult);        
          return false;
        } else {          
          return true 
        } 
      }),     
      concatMap((res1: IResponse) => {   
        this.bancariaAppQrSid.tiProc = '1'; // '0': es validar data, '1': es insertar     
        return this.bancariaAppQrSidService.reprocesaBancariaAppQrSid(this.bancariaAppQrSid) 
      })
    ).subscribe((res: IResponse) => {     
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {   
        this.parse();      
        this.respuestaExitosaGrabar('REPROCESÓ');

      }         
    }, (err: HttpErrorResponse) => {
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("error", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();        
      }
    });
  }

  private async validarFormularioNuevo(): Promise<boolean> { 
    let registro = this.formBancariaAppQrSid.getRawValue();
    if (!registro.zona || registro.zona === "*") {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una zona registral.");
      return false;      
    } else if (!registro.oficina || registro.oficina === "*") {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una oficina registral.");
      return false;      
    } else if (!registro.local || registro.local === "*") {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un local.");
      return false;      
    } else if (!registro.fecha) {
      let respuesta = await this.utilService.onShowAlertPromise("warning", "Atención", `Seleccione la fecha de emisión.`);
      if (respuesta.isConfirmed) {
        document.getElementById('idFecha').focus();
        return false; 
      } else {
        return true;
      }    
    } else if (this.bancariaAppQrSid.noAdju0001 === '') {
      this.utilService.onShowAlert("warning", "Atención", "Debe de adjuntar al menos 01 archivo de excel.");
      return false;  
    } else {
      return true;
    }
  }

  private async validarFormularioReprocesar(): Promise<boolean> { 
    if (this.bancariaAppQrSid.noAdju0001 === '') {
      this.utilService.onShowAlert("warning", "Atención", "Debe de adjuntar al menos 01 archivo de excel.");
      return false;  
    } else {
      return true;
    }
  }

  private asignarValoresNuevo() {    
    this.bancariaAppQrSid.idCncl = 0;
    this.bancariaAppQrSid.idTipoCncl = this.formBancariaAppQrSid.get('tipoConciliacion')?.value;
    this.bancariaAppQrSid.coZonaRegi = this.formBancariaAppQrSid.get('zona').value;
    this.bancariaAppQrSid.coOficRegi = this.formBancariaAppQrSid.get('oficina').value;
    this.bancariaAppQrSid.coLocaAten = this.formBancariaAppQrSid.get('local').value;
    this.bancariaAppQrSid.idOperPos = 1;    
    this.bancariaAppQrSid.feCncl = this.funciones.convertirFechaDateToString(this.formBancariaAppQrSid.get('fecha').value);
  }

  private asignarValoresReprocesar() {    
    this.bancariaAppQrSid.idCncl = this.reprocesarBancariaAppQrSid.idCncl;
    this.bancariaAppQrSid.idTipoCncl = this.reprocesarBancariaAppQrSid.idTipoCncl;
    this.bancariaAppQrSid.coZonaRegi = this.reprocesarBancariaAppQrSid.coZonaRegi;
    this.bancariaAppQrSid.coOficRegi = this.reprocesarBancariaAppQrSid.coOficRegi;
    this.bancariaAppQrSid.coLocaAten = this.reprocesarBancariaAppQrSid.coLocaAten;
    this.bancariaAppQrSid.idOperPos = 1;       
    this.bancariaAppQrSid.feCncl = (this.reprocesarBancariaAppQrSid.feCncl) ? this.funciones.convertirFechaDateToString(new Date(this.reprocesarBancariaAppQrSid.feCncl)) : '';
  }

  private async respuestaExitosaGrabar(texto: string) {    
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Se ${texto} satisfactoriamente el/los registro(s)`);
    if (respuesta.isConfirmed) {
      let dataFiltrosBancariaAppQrSid: IDataFiltrosBancariaAppQrSid = await this.sharingInformationService.compartirDataFiltrosBancariaAppQrSidObservable.pipe(first()).toPromise(); 
      if (dataFiltrosBancariaAppQrSid.esBusqueda) {
        dataFiltrosBancariaAppQrSid.esCancelar = true;
        this.sharingInformationService.compartirDataFiltrosBancariaAppQrSidObservableData = dataFiltrosBancariaAppQrSid;
      }      
      this.router.navigate(['SARF/procesos/bancaria/recaudacion_bancaria_app_qr_sid/bandeja']);
    }
  }

  public async cancelar() {
    let dataFiltrosBancariaAppQrSid: IDataFiltrosBancariaAppQrSid = await this.sharingInformationService.compartirDataFiltrosBancariaAppQrSidObservable.pipe(first()).toPromise(); 
    if (dataFiltrosBancariaAppQrSid.esBusqueda) {
      dataFiltrosBancariaAppQrSid.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosBancariaAppQrSidObservableData = dataFiltrosBancariaAppQrSid;
    }
    this.router.navigate(['SARF/procesos/bancaria/recaudacion_bancaria_app_qr_sid/bandeja']);
  }

  public obtenerServicioSeleccionado(file: any) {
    // this.servicioOperador = servicio;
    (this.formBancariaAppQrSid.get('docs') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  public parse() {
    const docs = (this.formBancariaAppQrSid.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto ) {
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: '',
          coZonaRegi: this.formBancariaAppQrSid.get('zona').value,
          coOficRegi: this.formBancariaAppQrSid.get('oficina').value
        })
      }
    });

    this.saveAdjuntoInFileServer();
  }

  public saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.utilService.onShowAlert("error", "Atención", data.msgResult);
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            // if ( this.sizeFiles == 0 ) {
            //   if ( value == 'P') {
            //     this.enviarDataProcesar();
            //   }
            //   else {
            //     this.enviarDataReprocesar();
            //   }
            // }
          }
        },
        error: ( e ) => {
          console.log( e );
          this.utilService.onShowAlert("error", "Atención", e.error.description);
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirBancariaAppQrSidObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();    
  }

}
