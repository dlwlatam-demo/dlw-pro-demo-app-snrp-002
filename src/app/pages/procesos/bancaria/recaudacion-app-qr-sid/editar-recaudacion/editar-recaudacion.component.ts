import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Subject, first } from 'rxjs';
import { IBancariaAppQrSid, IDataFiltrosBancariaAppQrSid } from '../../../../../interfaces/consolidacion-bancaria-app-qr-sid.interface';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { Funciones } from '../../../../../core/helpers/funciones/funciones';
import { UtilService } from '../../../../../services/util.service';
import { GeneralService } from '../../../../../services/general.service';
import { RecaudacionBancariaAppQrSidService } from '../../../../../services/procesos/bancaria/recaudacion-bancaria-app-qr-sid.service';
import { SharingInformationService } from '../../../../../core/services/sharing-services.service';
import Swal from 'sweetalert2';
import { SERVICIO_DE_PAGO } from '../../../../../models/enum/parameters';

@Component({
  selector: 'app-editar-recaudacion',
  templateUrl: './editar-recaudacion.component.html',
  styleUrls: ['./editar-recaudacion.component.scss'],
  providers: [
    MessageService
  ],
  encapsulation: ViewEncapsulation.None
})
export class EditarBancariaAppQrSid extends BaseBandeja implements OnInit, OnDestroy {

  private nameFile!: string;
  private reporteExcel!: any;

  public titulo!: string;
  public fecha: string = '';
  public idGuidExcel: string = '';
  public nombreDocumento: string = '';
  public servicioOperador: string = '';
  public tipoConciliacion: string = '';
  public nombreArchivoExcel: string = '';

  public mostrarBloque: boolean = false;
  public esConsultarRegistro: boolean = false;

  public bancariaAppQrSid!: IBancariaAppQrSid;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private router: Router,
    private funciones: Funciones,
    private utilService: UtilService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private sharingInformationService: SharingInformationService,
    private bancariaAppQrSidService: RecaudacionBancariaAppQrSidService
  ) { super() }

  async ngOnInit() {
    this.esConsultarRegistro = await this.sharingInformationService.compartirEsConsultarBancariaAppQrSidObservable.pipe( first() ).toPromise();
    this.bancariaAppQrSid = await this.sharingInformationService.compartirBancariaAppQrSidObservable.pipe( first() ).toPromise();
    console.log('bancariaAppQrSid', this.bancariaAppQrSid);
    this.titulo = ( !this.esConsultarRegistro ) ? 'Editar' : 'Consultar';
    this.fecha = this.funciones.convertirFechaDateToString( new Date( this.bancariaAppQrSid.feCncl ) );
    this.servicioOperador = this.bancariaAppQrSid.noOperPos;
    this.nombreDocumento = 'Nombre de archivo Estado Bancario';
    this.nombreArchivoExcel = this.bancariaAppQrSid.noAdju0001;
    this.idGuidExcel = this.bancariaAppQrSid.idGuidDocu01;

    switch ( this.bancariaAppQrSid.idTipoCncl ) {
      case 15:
        this.tipoConciliacion = 'APP SUNARP';
         break;
      case 16:
        this.tipoConciliacion = 'CÓDIGO QR';
         break;
      case 17:
        this.tipoConciliacion = 'SID TARJETA';
         break;
      default:
        this.tipoConciliacion = '';
    }
  }

  public downloadFile(tipo: number) {
    switch(tipo) {
      case 0:
        this.nameFile = "RecaudacionBancaria_MediosElectronicos_EEBB_NoConciliados";
         break;
      case 1:
        this.nameFile = "RecaudacionBancaria_MediosElectronicos_EEBB_Conciliados";
         break;
      default:
        this.nameFile = "RecaudacionBancaria_MediosElectronicos_EEBB_NoDefinido";
    }
    
    this.obtenerArchivoMEF( this.bancariaAppQrSid.idCncl, tipo );
  }

  private obtenerArchivoMEF(idCncl: number, inCncl: number) {
    
    Swal.showLoading();
    this.bancariaAppQrSidService.getRecaudacionMEF( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        this.reporteExcel = data.body.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });
		
		    this.exportExcel();
        Swal.close();
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      }
    });
  }

  public downloadFileNiubiz( tipo: number ) {
    switch ( tipo ) {
      case 0:
        this.nameFile = 'Recaudacion_MediosElectronicos_NIUBIZ_NoConciliados';
          break;
      case 1:
        this.nameFile = 'Recaudacion_MediosElectronicos_NIUBIZ_Conciliados';
          break;
      case 2:
        this.nameFile = 'Recaudacion_MediosElectronicos_NIUBIZ_Anulaciones';
          break;
      case 3:
        this.nameFile = 'Recaudacion_MediosElectronicos_NIUBIZ_Contracargos';
          break;
      default:
        this.nameFile = 'Recaudacion_MediosElectronicos_NIUBIZ_NoDefinido';
    }

    this.obtenerArchivoNiubiz( this.bancariaAppQrSid.idCncl, tipo );
  }

  private obtenerArchivoNiubiz( idCncl: number, inCncl: number ) {
    Swal.showLoading();
    this.bancariaAppQrSidService.getRecaudacionNiubiz( idCncl, inCncl ).subscribe({
      next: ( data ) => {
        console.log('Niubiz', data );
        this.reporteExcel = data.reporteExcel;

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.'
        });

        this.exportExcel();
        Swal.close();
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.'
          });

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.'
        });
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  public handleChange(e: any) {
    if ( e.index === 0 ) {
      this.nombreDocumento = 'Nombre de archivo Estado Bancario';
      this.nombreArchivoExcel = this.bancariaAppQrSid.noAdju0001;
      this.idGuidExcel = this.bancariaAppQrSid.idGuidDocu01;
      console.log('idGuidExcel1', this.idGuidExcel);
      this.mostrarBloque = false;
    } else if ( e.index === 1 ) {
      this.servicioOperador = SERVICIO_DE_PAGO.NIUBIZ;
      this.nombreDocumento = 'Nombre de archivo excel';
      this.nombreArchivoExcel = this.bancariaAppQrSid.noAdju0002;
      this.idGuidExcel = this.bancariaAppQrSid.idGuidDocu02;
      console.log('idGuidExcel2', this.idGuidExcel);
      this.mostrarBloque = true;
    }
  }

  public async cancelar() {
    let dataFiltrosBancariaAppQrSid: IDataFiltrosBancariaAppQrSid = await this.sharingInformationService.compartirDataFiltrosBancariaAppQrSidObservable.pipe(first()).toPromise(); 
    if (dataFiltrosBancariaAppQrSid.esBusqueda) {
      dataFiltrosBancariaAppQrSid.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosBancariaAppQrSidObservableData = dataFiltrosBancariaAppQrSid;
    }
    this.router.navigate(['SARF/procesos/bancaria/recaudacion_bancaria_app_qr_sid/bandeja']);
  }

  public visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

  private exportExcel() {
    console.log('Exportar Excel');

    const doc = this.reporteExcel;
	  const nFile = this.nameFile;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', nFile);
    document.body.appendChild( download );
    download.click();
  }

  ngOnDestroy(): void {
    this.sharingInformationService.compartirEsConsultarBancariaAppQrSidObservableData = false;
    this.sharingInformationService.compartirBancariaAppQrSidObservableData = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();      
  }

}
