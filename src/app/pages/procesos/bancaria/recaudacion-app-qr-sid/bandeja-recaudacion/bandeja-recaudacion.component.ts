import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, forkJoin } from 'rxjs';
import { takeUntil, filter, concatMap } from 'rxjs/operators';

import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent } from 'primeng/api';

import { IResponse2, IResponse } from '../../../../../interfaces/general.interface';
import { IDataFiltrosBancariaAppQrSid, IBancariaAppQrSid } from '../../../../../interfaces/consolidacion-bancaria-app-qr-sid.interface';
import { ZonaRegistral, OficinaRegistral, Local } from '../../../../../interfaces/combos.interface';

import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { ESTADO_REGISTRO_2 } from '../../../../../models/enum/parameters';
import { FiltroBancariaAppQrSid, TramaBancariaAppQrSid } from '../../../../../models/procesos/bancaria/recaudacion-bancaria-app-qr-sid.model';

import { Funciones } from '../../../../../core/helpers/funciones/funciones';

import { GeneralService } from '../../../../../services/general.service';
import { UtilService } from '../../../../../services/util.service';
import { SharingInformationService } from '../../../../../core/services/sharing-services.service';
import { RecaudacionBancariaAppQrSidService } from '../../../../../services/procesos/bancaria/recaudacion-bancaria-app-qr-sid.service';

import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';

import { environment } from '../../../../../../environments/environment';
import { IOperacionScunac, IPostOperacionScunac, IDetalleOperacionScunac } from '../../../../../interfaces/consolidacion-app-qr-sid';

@Component({
  selector: 'app-bandeja-recaudacion',
  templateUrl: './bandeja-recaudacion.component.html',
  styleUrls: ['./bandeja-recaudacion.component.scss'],
  providers: [
    DialogService
  ]
})
export class BandejaBancariaAppQrSid extends BaseBandeja implements OnInit {

  userCode!: any;
  esBusqueda: boolean = false;
  loadingLocal: boolean = false;
  loadingBancaria: boolean = false;
  loadingOficinaRegistral: boolean = false;

  fechaMinima!: Date;
  fechaMaxima!: Date;

  formBancariaAppQrSid!: FormGroup;
  
  filtroBancariaAppQrSid!: FiltroBancariaAppQrSid;

  listaLocal: Local[]
  listaEstado: IResponse2[];
  listaOperador: IResponse2[];
  listaZonaRegistral: ZonaRegistral[];
  listaTipoConciliacion: IResponse2[];
  listaOficinaRegistral: OficinaRegistral[];

  listaLocalFiltro: Local[];
  listaEstadoFiltro: IResponse2[];
  listaOperadorFiltro: IResponse2[];
  listaZonaRegistralFiltro: ZonaRegistral[];
  listaTipoConciliacionFiltro: IResponse2[];
  listaOficinaRegistralFiltro: OficinaRegistral[];

  listaBancaria!: IBancariaAppQrSid[];
  selectedValuesBancaria: IBancariaAppQrSid[] = [];

  estadoRegistro = ESTADO_REGISTRO_2;

  unsubscribe$ = new Subject<void>();

  currentPage: string = environment.currentPage;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private funciones: Funciones,
    private utilService: UtilService,
    private dialogService: DialogService,
    private generalService: GeneralService,
    private sharingInformationService: SharingInformationService,
    private bancariaAppQrSidService: RecaudacionBancariaAppQrSidService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.CD_Recaudacion_APP_SUNARP_QR_SID;
    this.btnConsolidacionAPPSunarp();

    this.fechaMaxima = new Date();
    this.fechaMinima = new Date(this.fechaMaxima.getFullYear(), this.fechaMaxima.getMonth(), 1);
    this.userCode = localStorage.getItem('user_code') || '';
    this.construirFormulario();

    this.sharingInformationService.compartirDataFiltrosBancariaAppQrSidObservable.pipe( takeUntil( this.unsubscribe$ ) )
    .subscribe( ( data: IDataFiltrosBancariaAppQrSid ) => {
      if ( data.esCancelar && data.bodyBancariaAppQrSid !== null ) {
        this.utilService.onShowProcessLoading('Cargando la data');
        this.setearCamposFiltro( data );
        this.buscarRecaudacion( false );
      } else {
        this.inicilializarListas();
        this.obtenerListados();
      }
    });
  }

  private construirFormulario() {
    this.formBancariaAppQrSid = this.fb.group({
      zona: [''],
      oficina: [''],
      local: [''],
      tipoConciliacion: [''],  
      operador: [''],    
      estado: [''],   
      fechaDesde: [this.fechaMinima],
      fechaHasta: [this.fechaMaxima]              
    });
  }

  private inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoConciliacion = [];
    this.listaOperador = [];
    this.listaEstado = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' });
    this.listaOperador.push({ ccodigoHijo: '0', cdescri: '(TODOS)' });  
    this.listaEstado.push({ ccodigoHijo: '', cdescri: '(TODOS)' });   
  }

  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data");     
    forkJoin(      
      this.generalService.getCbo_Zonas_Usuario( this.userCode),     
      this.generalService.getCbo_EstadoRecaudacion(),
      this.generalService.getCbo_OperadoresAppQrSid(),
      this.generalService.getCbo_TipoConciliacionBancaria()
    ).pipe( takeUntil( this.unsubscribe$ ) )
     .subscribe(([resZonaRegistral, resEstado, resOperador, resTipoConciliacion]) => {
      if (resZonaRegistral.length === 1) {
        this.listaZonaRegistral.push({ coZonaRegi: resZonaRegistral[0].coZonaRegi, deZonaRegi: resZonaRegistral[0].deZonaRegi });    
        this.formBancariaAppQrSid.patchValue({ "zona": resZonaRegistral[0].coZonaRegi});
        this.buscarOficinaRegistral();
      } else {
        this.listaZonaRegistral.push(...resZonaRegistral);
      }

      if ( resOperador.length === 1 ) {
        this.listaOperador.push({ ccodigoHijo: resOperador[0].ccodigoHijo, cdescri: resOperador[0].cdescri });
        this.formBancariaAppQrSid.patchValue({ 'operador': resOperador[0].ccodigoHijo });
      } else {
        this.listaOperador.push(...resOperador);
      }

      if ( resTipoConciliacion.length === 1 ) {
        this.listaTipoConciliacion.push({ ccodigoHijo: resTipoConciliacion[0].ccodigoHijo, cdescri: resTipoConciliacion[0].cdescri });
        this.formBancariaAppQrSid.patchValue({ 'tipoConciliacion': resTipoConciliacion[0].ccodigoHijo });
      } else {
        this.listaTipoConciliacion.push(...resTipoConciliacion);
		    this.formBancariaAppQrSid.patchValue({ 'tipoConciliacion': resTipoConciliacion[0].ccodigoHijo });
      }
      this.listaEstado.push(...resEstado);        
      this.buscarRecaudacion(true);    
    }, (err: HttpErrorResponse) => {
    }) 
  }

  private setearCamposFiltro( dataFiltro: IDataFiltrosBancariaAppQrSid ) {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoConciliacion = [];
    this.listaOperador = [];
    this.listaEstado = [];    
    this.listaZonaRegistral = dataFiltro.listaZonaRegistral; 
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaTipoConciliacion = dataFiltro.listaTipoConciliacion;
    this.listaOperador = dataFiltro.listaOperador;
    this.listaEstado = dataFiltro.listaEstado;    
    this.filtroBancariaAppQrSid = dataFiltro.bodyBancariaAppQrSid;
    this.formBancariaAppQrSid.patchValue({ "zona": this.filtroBancariaAppQrSid.coZonaRegi});
    this.formBancariaAppQrSid.patchValue({ "oficina": this.filtroBancariaAppQrSid.coOficRegi});
    this.formBancariaAppQrSid.patchValue({ "local": this.filtroBancariaAppQrSid.coLocaAten});
    this.formBancariaAppQrSid.patchValue({ "tipoConciliacion": this.filtroBancariaAppQrSid.idTipoCncl});
    this.formBancariaAppQrSid.patchValue({ "operador": this.filtroBancariaAppQrSid.idOperPos});
    this.formBancariaAppQrSid.patchValue({ "estado": this.filtroBancariaAppQrSid.esDocu});
    this.formBancariaAppQrSid.patchValue({ "fechaDesde": (this.filtroBancariaAppQrSid.feDesd !== '' && this.filtroBancariaAppQrSid.feDesd !== null) ? this.funciones.convertirFechaStringToDate(this.filtroBancariaAppQrSid.feDesd) : this.filtroBancariaAppQrSid.feDesd});
    this.formBancariaAppQrSid.patchValue({ "fechaHasta": (this.filtroBancariaAppQrSid.feHast !== '' && this.filtroBancariaAppQrSid.feHast !== null) ? this.funciones.convertirFechaStringToDate(this.filtroBancariaAppQrSid.feHast) : this.filtroBancariaAppQrSid.feHast});
  }

  public buscarRecaudacion( esCargaInicial: boolean ) {
    let resultado: boolean = this.validarCamporFiltros();
    if ( !resultado ) {
      return;
    }
    this.listaBancaria = [];
    this.selectedValuesBancaria = [];
    this.loadingBancaria = ( esCargaInicial ) ? false : true;
    this.guardarListadosParaFiltro();
    this.establecerBodyBancariaAppQrSid();
    this.esBusqueda = ( esCargaInicial ) ? false : true;
    this.bancariaAppQrSidService.getBandejaConciliacion( this.filtroBancariaAppQrSid ).pipe( takeUntil( this.unsubscribe$ ) )
        .subscribe( ( data: any ) => {
          this.listaBancaria = data.lista;
          this.loadingBancaria = false
          this.utilService.onCloseLoading();
        }, ( err: HttpErrorResponse ) => {
          this.loadingBancaria = false;
          if ( err.error.category === 'NOT_FOUND' ) {
            this.utilService.onShowAlert('warning', 'Atención', err.error.description);
          } else {
            this.utilService.onShowMessageErrorSystem();
          }
        });
  }

  private validarCamporFiltros(): boolean {
    if (
      (this.formBancariaAppQrSid.get('fechaDesde')?.value !== "" && this.formBancariaAppQrSid.get('fechaDesde')?.value !== null) &&
      (this.formBancariaAppQrSid.get('fechaHasta')?.value === "" || this.formBancariaAppQrSid.get('fechaHasta')?.value === null)
    ) {
      document.getElementById('idFechaHasta').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de fin.");
      return false; 
    } else if (
      (this.formBancariaAppQrSid.get('fechaDesde')?.value === "" || this.formBancariaAppQrSid.get('fechaDesde')?.value === null) &&
      (this.formBancariaAppQrSid.get('fechaHasta')?.value !== "" && this.formBancariaAppQrSid.get('fechaHasta')?.value !== null)
    ) {
      document.getElementById('idFechaDesde').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de inicio.");
      return false; 
    } else {
      return true;
    }   
  }

  public buscarOficinaRegistral() { 
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formBancariaAppQrSid.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.formBancariaAppQrSid.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.formBancariaAppQrSid.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formBancariaAppQrSid.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.formBancariaAppQrSid.get('zona')?.value,this.formBancariaAppQrSid.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formBancariaAppQrSid.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err: HttpErrorResponse) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  public cambioFechaInicio() {   
    if (this.formBancariaAppQrSid.get('fechaHasta')?.value !== '' && this.formBancariaAppQrSid.get('fechaHasta')?.value !== null) {
      if (this.formBancariaAppQrSid.get('fechaDesde')?.value > this.formBancariaAppQrSid.get('fechaHasta')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.formBancariaAppQrSid.controls['fechaDesde'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.formBancariaAppQrSid.get('fechaDesde')?.value !== '' && this.formBancariaAppQrSid.get('fechaDesde')?.value !== null) {
      if (this.formBancariaAppQrSid.get('fechaHasta')?.value < this.formBancariaAppQrSid.get('fechaDesde')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.formBancariaAppQrSid.controls['fechaHasta'].reset();    
      }      
    }
  }

  public nuevo() {  
    this.router.navigate(['SARF/procesos/bancaria/recaudacion_bancaria_app_qr_sid/nuevo']);
    this.sharingInformationService.irRutaBandejaBancariaAppQrSidObservableData = false;
  }

  public editar() {   
    if (this.selectedValuesBancaria.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
    } else if (this.selectedValuesBancaria.length === 1) {
      if (!(this.selectedValuesBancaria[0].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesBancaria[0].esDocu === this.estadoRegistro.REPROCESADO)) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede EDITAR un registro en estado ABIERTO o REPROCESADO");
      } else {
        this.sharingInformationService.compartirBancariaAppQrSidObservableData = this.selectedValuesBancaria[0]; 
        this.router.navigate(['SARF/procesos/bancaria/recaudacion_bancaria_app_qr_sid/editar']);
        this.sharingInformationService.irRutaBandejaBancariaAppQrSidObservableData = false;    
      }
    } else if (this.selectedValuesBancaria.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }
  }

  public anular() {
    if (this.selectedValuesBancaria.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosAnulados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ANULAR el registro en estado ABIERTO o REPROCESADO.");
    } else {
      let textoAnular = (this.selectedValuesBancaria.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ANULAR ${textoAnular}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('anular');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesBancaria.length; i++) {
            lista.push(this.selectedValuesBancaria[i].idCncl.toString());           
          }      
          let bodyAnularRecaudacion = new TramaBancariaAppQrSid();
          bodyAnularRecaudacion.trama = lista;
          this.bancariaAppQrSidService.anulaBancariaAppQrSid(bodyAnularRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesBancaria = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  public activar() {
    if (this.selectedValuesBancaria.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosActivados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ACTIVAR el registro en estado ANULADO.");
    } else {
      let textoActivar = (this.selectedValuesBancaria.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ACTIVAR ${textoActivar}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('activar');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesBancaria.length; i++) {
            lista.push(this.selectedValuesBancaria[i].idCncl.toString());           
          }      
          let bodyActivarrRecaudacion = new TramaBancariaAppQrSid();
          bodyActivarrRecaudacion.trama = lista;
          this.bancariaAppQrSidService.activateBancariaAppQrSid(bodyActivarrRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarRecaudacion(false);
              this.selectedValuesBancaria = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  public abrirConciliacion() {
    if (this.selectedValuesBancaria.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosAbiertos()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ABRIR el registro en estado CERRADO.");
    } else {
      let textoAbrir = (this.selectedValuesBancaria.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ABRIR ${textoAbrir}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('abrir');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesBancaria.length; i++) {
            lista.push(this.selectedValuesBancaria[i].idCncl.toString());           
          }      
          let bodyAbrirRecaudacion = new TramaBancariaAppQrSid();
          bodyAbrirRecaudacion.trama = lista;
          this.bancariaAppQrSidService.openBancariaAppQrSid(bodyAbrirRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {
            if (res.codResult < 0 ) {
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) { 
              this.buscarRecaudacion(false);
              this.selectedValuesBancaria = [];
            }             
          }, (err: HttpErrorResponse) => {   
            this.utilService.onShowMessageErrorSystem();                                
          });          
        }
      });
    }
  }

  public cerrarConciliacion() {
    if (this.selectedValuesBancaria.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosCerrados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede CERRAR el registro en estado ABIERTO o REPROCESADO.");
    } else {
      let textoAbrir = (this.selectedValuesBancaria.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea CERRAR ${textoAbrir}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('cerrar');           
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesBancaria.length; i++) {
            lista.push(this.selectedValuesBancaria[i].idCncl.toString());           
          }      
          let bodyCerrarRecaudacion = new TramaBancariaAppQrSid();
          bodyCerrarRecaudacion.trama = lista;
          this.bancariaAppQrSidService.closeBancariaAppQrSid(bodyCerrarRecaudacion).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {
              const listado = lista.map(item => this.operacionesScunac(parseInt(item)).catch((error) => { }));
              Promise.all(lista).then( respuesta => {
                this.buscarRecaudacion(false);
                this.selectedValuesBancaria = [];          
              });
            }
          }, (err: HttpErrorResponse) => {      
          });
        }
      });
    }
  }

  public reProcesar() {
    if (this.selectedValuesBancaria.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (this.selectedValuesBancaria.length === 1) {
      if (!(this.selectedValuesBancaria[0].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesBancaria[0].esDocu === this.estadoRegistro.REPROCESADO)) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede REPROCESAR un registro en estado ABIERTO o REPROCESADO");
      } else {
        this.sharingInformationService.compartirBancariaAppQrSidObservableData = this.selectedValuesBancaria[0]; 
        this.router.navigate(['SARF/procesos/bancaria/recaudacion_bancaria_app_qr_sid/reprocesar']);
        this.sharingInformationService.irRutaBandejaBancariaAppQrSidObservableData = false; 
      }           
    } else if (this.selectedValuesBancaria.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }
  }

  public consultarRegistro() {
    if (this.selectedValuesBancaria.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (this.selectedValuesBancaria.length === 1) {
      this.sharingInformationService.compartirEsConsultarBancariaAppQrSidObservableData = true; 
      this.sharingInformationService.compartirBancariaAppQrSidObservableData = this.selectedValuesBancaria[0];
      this.router.navigate(['SARF/procesos/bancaria/recaudacion_bancaria_app_qr_sid/consultar']);
      this.sharingInformationService.irRutaBandejaBancariaAppQrSidObservableData = false;      
    } else if (this.selectedValuesBancaria.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }
  }

  private guardarListadosParaFiltro() {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaTipoConciliacionFiltro = [...this.listaTipoConciliacion];
    this.listaOperadorFiltro = [...this.listaOperador];
    this.listaEstadoFiltro = [...this.listaEstado];    
  }

  private establecerBodyBancariaAppQrSid() {       
    this.filtroBancariaAppQrSid = new FiltroBancariaAppQrSid();
    this.filtroBancariaAppQrSid.coZonaRegi = (this.formBancariaAppQrSid.get('zona')?.value === '*') ? '' : this.formBancariaAppQrSid.get('zona')?.value;    
    this.filtroBancariaAppQrSid.coOficRegi = (this.formBancariaAppQrSid.get('oficina')?.value === '*') ? '' : this.formBancariaAppQrSid.get('oficina')?.value;    
    this.filtroBancariaAppQrSid.coLocaAten = (this.formBancariaAppQrSid.get('local')?.value === '*') ? '' : this.formBancariaAppQrSid.get('local')?.value;        
    this.filtroBancariaAppQrSid.idTipoCncl = (this.formBancariaAppQrSid.get('tipoConciliacion')?.value === '0') ? 0 : this.formBancariaAppQrSid.get('tipoConciliacion')?.value;
    this.filtroBancariaAppQrSid.idOperPos = (this.formBancariaAppQrSid.get('operador')?.value === '0') ? 0 : this.formBancariaAppQrSid.get('operador')?.value;
    this.filtroBancariaAppQrSid.esDocu = (this.formBancariaAppQrSid.get('estado')?.value === '*') ? '' : this.formBancariaAppQrSid.get('estado')?.value;
    this.filtroBancariaAppQrSid.feDesd = (this.formBancariaAppQrSid.get('fechaDesde')?.value !== "" && this.formBancariaAppQrSid.get('fechaDesde')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formBancariaAppQrSid.get('fechaDesde')?.value) : ""; 
    this.filtroBancariaAppQrSid.feHast = (this.formBancariaAppQrSid.get('fechaHasta')?.value !== "" && this.formBancariaAppQrSid.get('fechaHasta')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formBancariaAppQrSid.get('fechaHasta')?.value): "";       
  }

  private establecerDataFiltrosRecaudacionME() : IDataFiltrosBancariaAppQrSid {
    let dataFiltrosRecaudacionME: IDataFiltrosBancariaAppQrSid = {
      listaZonaRegistral: this.listaZonaRegistralFiltro, 
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaTipoConciliacion: this.listaTipoConciliacionFiltro,     
      listaOperador: this.listaOperadorFiltro,     
      listaEstado: this.listaEstadoFiltro,
      bodyBancariaAppQrSid: this.filtroBancariaAppQrSid,   
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosRecaudacionME;
  }

  private operacionesScunac(idCncl: number) {
    return new Promise<any>(
      (resolve, reject) => {
        this.bancariaAppQrSidService.getOperacionesScunac(idCncl, 2)
          .pipe(            
            takeUntil(this.unsubscribe$),
            filter((data: any) => {
              if (data.body.lstOperacionScunac.length > 0) {
                return true;
              } else {
                return false;
              }
            }),
            concatMap((res: any) => {
              let listaOperacionScunac: IOperacionScunac[] = res.body.lstOperacionScunac;      
              return this.bancariaAppQrSidService.actualizarIdOperacionScunac(this.obtenerOperacionScunac(listaOperacionScunac))              
            })
          ).subscribe( (res: IResponse) => {
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
              reject(new Error("Error de ejecución . . . :( "));
            } else if (res.codResult === 0) { 
              resolve(idCncl);                     
            }                                                                     
          }, () => {
            reject(new Error("Error de ejecución . . . :( ")); 
          }
        ) 
      }
    );
  }

  private obtenerOperacionScunac(lista: IOperacionScunac[]): IPostOperacionScunac {
    let data: IPostOperacionScunac;
    let listaDetalle: IDetalleOperacionScunac[] = [];
    for (let i = 0; i < lista.length; i++) {
      let item: IDetalleOperacionScunac = {
        aaMvto: lista[i].anioMovimiento,
        coZonaRegi: lista[i].coZonaRegi,
        nuMvto: lista[i].numeroMovimiento,
        nuSecuDeta: lista[i].nuSecuDeta,
        idTransPos: lista[i].id        
      }
      listaDetalle.push(item);      
    }
    data = {
      trama: listaDetalle
    } 
    return data;
  }

  private validarRegistrosAnulados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesBancaria.length; i++) { 
      if (!(this.selectedValuesBancaria[i].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesBancaria[i].esDocu === this.estadoRegistro.REPROCESADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  private validarRegistrosActivados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesBancaria.length; i++) { 
      if (!(this.selectedValuesBancaria[i].esDocu === this.estadoRegistro.ANULADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  private validarRegistrosAbiertos(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesBancaria.length; i++) { 
      if (!(this.selectedValuesBancaria[i].esDocu === this.estadoRegistro.CERRADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  private validarRegistrosCerrados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesBancaria.length; i++) { 
      if (!(this.selectedValuesBancaria[i].esDocu === this.estadoRegistro.ABIERTO || this.selectedValuesBancaria[i].esDocu === this.estadoRegistro.REPROCESADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;
        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
        return (event.order! * result);
    });
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();   
    this.sharingInformationService.compartirDataFiltrosBancariaAppQrSidObservableData = this.establecerDataFiltrosRecaudacionME();                    
  }
}
