import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { DiarioPosComponent } from './diario-pos/diario-pos.component';
import { ConsolidadoPosComponent } from './consolidado-pos/consolidado-pos.component';

const routes: Routes = [
  { path: 'recaudacion_diario_pos', component: DiarioPosComponent, canActivate: [ AuthGaurdService ] },
  { path: 'consolidado_recaudacion_pos', component: ConsolidadoPosComponent, canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecaudacionMediosElectronicosRoutingModule { }
