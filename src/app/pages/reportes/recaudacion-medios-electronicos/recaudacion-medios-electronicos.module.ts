import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecaudacionMediosElectronicosRoutingModule } from './recaudacion-medios-electronicos-routing.module';
import { ComponentsModule } from '../components/components.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';

import { DiarioPosComponent } from './diario-pos/diario-pos.component';
import { ConsolidadoPosComponent } from './consolidado-pos/consolidado-pos.component';


@NgModule({
  declarations: [
    DiarioPosComponent,
    ConsolidadoPosComponent
  ],
  imports: [
    CommonModule,
    RecaudacionMediosElectronicosRoutingModule,
    NgPrimeModule,
    ComponentsModule
  ]
})
export class RecaudacionMediosElectronicosModule { }
