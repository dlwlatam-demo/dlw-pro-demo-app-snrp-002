import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { MessageService } from 'primeng/api';

import { MenuOpciones } from '../../../../models/auth/Menu.model';

import { RecaudacionMediosService } from '../../../../services/reportes/recaudacion-medios/recaudacion-medios.service';
import { UtilService } from '../../../../services/util.service';
import { ValidatorsService } from '../../../../core/services/validators.service';

import { BaseBandeja } from '../../../../base/base-bandeja.abstract';

@Component({
  selector: 'app-consolidado-pos',
  templateUrl: './consolidado-pos.component.html',
  styleUrls: ['./consolidado-pos.component.scss'],
  providers: [
    MessageService
  ]
})
export class ConsolidadoPosComponent extends BaseBandeja implements OnInit {

  alertMsg!: boolean;
  today: Date = new Date();
  disableButton: boolean = true;
  bandeja: string = 'obligatorio';

  filtros!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private messageService: MessageService,
    private validarService: ValidatorsService,
    private recaudacionMedioService: RecaudacionMediosService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Reporte_Consolidado_de_Recaudacion
    this.btnReporteConsolidadoPOS();

    sessionStorage.removeItem('reportePDF');
    sessionStorage.removeItem('reporteExcel');

    const fechaDesde = new Date( this.today.getFullYear(), this.today.getMonth(), 1 );

    this.filtros = this.fb.group({
      zonas: ['', [ Validators.required ]],
      oficinas: ['', [ Validators.required ]],
      operador: ['', [ Validators.required ]],
      fecDesde: [fechaDesde, [ Validators.required ]],
      fecHasta: [this.today, [ Validators.required ]]
    });
  }

  generarReporte() {
    this.disableButton = true;
    
    let zona = this.filtros.get('zonas')?.value;
    if ( zona == '*' ) {
      this.alertMsg = true;
      return;
    }

    let oficina = this.filtros.get('oficinas')?.value;
    oficina = oficina == '*' ? '' : oficina;

    const data = {
      coZonaRegi: zona,
      coOficRegi: oficina,
      idOperPos: this.filtros.get('operador')?.value,
      feDesd: this.validarService.formatDate( this.filtros.get('fecDesde')?.value ),
      feHast: this.validarService.formatDate( this.filtros.get('fecHasta')?.value ),
    }

    this.obtenerReporte( data );
  }

  obtenerReporte( data: any ) {
    Swal.showLoading();
    this.recaudacionMedioService.reporteConsolidadoRecaudacionPOS( data ).subscribe({
      next: ( data ) => {

        if (data.codResult < 0 ) {
          this.utilService.onShowAlert("info", "Atención", data.msgResult);
          return;
        }

        this.visualizarReporte( data.reportePdf );
        
        sessionStorage.setItem('reportePDF', data.reportePdf);
        sessionStorage.setItem('reporteExcel', data.reporteExcel);

        this.messageService.add({
          severity: 'success', 
          summary: 'Reporte generado exitósamente', 
          detail: 'Puede proceder con la descarga de su reporte.',
          life: 5000
        });

        Swal.close();
        this.disableButton = false;
      },
      error: ( e ) => {
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.',
            life: 5000
          });

          sessionStorage.removeItem('reportePDF');
          sessionStorage.removeItem('reporteExcel');

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.',
          life: 5000
        });

        sessionStorage.removeItem('reportePDF');
        sessionStorage.removeItem('reporteExcel');
      }
    });
  }

  exportPdf() {
    const reportePDF = sessionStorage.getItem('reportePDF');

    if ( reportePDF === null || reportePDF === 'null' ) {
      return;
    }

    const fileName = 'Reporte_Consolidado_Recaudacion_POS';

    const byteCharacters = atob(reportePDF);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/pdf' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', fileName);
    document.body.appendChild( download );
    download.click();
  }

  exportExcel() {
    const reporteExcel = sessionStorage.getItem('reporteExcel');

    if ( reporteExcel === null || reporteExcel === 'null' ) {
      return;
    }

    const fileName = 'Reporte_Consolidado_Recaudacion_POS';

    const byteCharacters = atob(reporteExcel);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', fileName);
    document.body.appendChild( download );
    download.click();
  }

}
