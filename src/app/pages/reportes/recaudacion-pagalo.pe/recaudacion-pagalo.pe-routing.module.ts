import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { IngresosCuentaComponent } from './ingresos-cuenta/ingresos-cuenta.component';
import { RecaudacionRegistradaComponent } from './recaudacion-registrada/recaudacion-registrada.component';

const routes: Routes = [
  { path: 'ingresos_cuenta_pagalo_pe', component: IngresosCuentaComponent, canActivate: [ AuthGaurdService ] },
  { path: 'recaudacion_registrada_pagalo_pe', component: RecaudacionRegistradaComponent, canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecaudacionPagaloPeRoutingModule { }
