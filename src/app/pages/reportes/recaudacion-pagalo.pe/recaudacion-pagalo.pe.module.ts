import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecaudacionPagaloPeRoutingModule } from './recaudacion-pagalo.pe-routing.module';
import { ComponentsModule } from '../components/components.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';

import { IngresosCuentaComponent } from './ingresos-cuenta/ingresos-cuenta.component';
import { RecaudacionRegistradaComponent } from './recaudacion-registrada/recaudacion-registrada.component';


@NgModule({
  declarations: [
    IngresosCuentaComponent,
    RecaudacionRegistradaComponent
  ],
  imports: [
    CommonModule,
    RecaudacionPagaloPeRoutingModule,
    NgPrimeModule,
    ComponentsModule
  ]
})
export class RecaudacionPagaloPeModule { }
