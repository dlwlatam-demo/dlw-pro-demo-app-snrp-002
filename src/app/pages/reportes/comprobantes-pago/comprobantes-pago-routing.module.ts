import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { ReciboHonorarioComponent } from './recibo-honorario/recibo-honorario.component';
import { PorCuentaContableComponent } from './por-cuenta-contable/por-cuenta-contable.component';

const routes: Routes = [
  { path: 'recibo_por_honorarios', component: ReciboHonorarioComponent, canActivate: [ AuthGaurdService ] },
  { path: 'por_cuenta_contable', component: PorCuentaContableComponent, canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComprobantesPagoRoutingModule { }
