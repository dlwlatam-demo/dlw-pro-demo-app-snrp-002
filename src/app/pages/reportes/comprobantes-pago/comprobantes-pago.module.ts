import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComprobantesPagoRoutingModule } from './comprobantes-pago-routing.module';
import { ComponentsModule } from '../components/components.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';

import { ReciboHonorarioComponent } from './recibo-honorario/recibo-honorario.component';
import { PorCuentaContableComponent } from './por-cuenta-contable/por-cuenta-contable.component';


@NgModule({
  declarations: [
    ReciboHonorarioComponent,
    PorCuentaContableComponent
  ],
  imports: [
    CommonModule,
    ComprobantesPagoRoutingModule,
    NgPrimeModule,
    ComponentsModule
  ]
})
export class ComprobantesPagoModule { }
