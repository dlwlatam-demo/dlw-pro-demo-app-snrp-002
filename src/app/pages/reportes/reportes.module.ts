import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportesRoutingModule } from './reportes-routing.module';
import { NgPrimeModule } from '../../ng-prime/ng-prime.module';
import { ComponentsModule } from './components/components.module';

import { TasasRegistralesComponent } from './tasas-registrales/tasas-registrales.component';
import { LibroBancoSiafComponent } from './libro-banco-siaf/libro-banco-siaf.component';

@NgModule({
  declarations: [
    TasasRegistralesComponent,
    LibroBancoSiafComponent
  ],
  imports: [
    CommonModule,
    ReportesRoutingModule,
    NgPrimeModule,
    ComponentsModule
  ]
})
export class ReportesModule { }
