import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { RecaudacionSprlRoutingModule } from './recaudacion-sprl-routing.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';
import { ComponentsModule } from '../components/components.module';

import { ReporteSprlComponent } from './reporte-sprl/reporte-sprl.component';
import { PublicidadSprlComponent } from './publicidad-sprl/publicidad-sprl.component';
import { PublicidadSinIdentComponent } from './publicidad-sin-ident/publicidad-sin-ident.component';
import { ServicioInscripcionComponent } from './servicio-inscripcion/servicio-inscripcion.component';
import { DistribucionSprlComponent } from './distribucion-sprl/distribucion-sprl.component';


@NgModule({
  declarations: [
    ReporteSprlComponent,
    PublicidadSprlComponent,
    PublicidadSinIdentComponent,
    ServicioInscripcionComponent,
    DistribucionSprlComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RecaudacionSprlRoutingModule,
    NgPrimeModule,
    ComponentsModule
  ]
})
export class RecaudacionSprlModule { }
