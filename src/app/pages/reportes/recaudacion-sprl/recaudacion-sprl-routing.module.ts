import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { ReporteSprlComponent } from './reporte-sprl/reporte-sprl.component';
import { PublicidadSprlComponent } from './publicidad-sprl/publicidad-sprl.component';
import { PublicidadSinIdentComponent } from './publicidad-sin-ident/publicidad-sin-ident.component';
import { ServicioInscripcionComponent } from './servicio-inscripcion/servicio-inscripcion.component';
import { DistribucionSprlComponent } from './distribucion-sprl/distribucion-sprl.component';

const routes: Routes = [
  { path: 'reporte_de_recaudacion', component: ReporteSprlComponent, canActivate: [ AuthGaurdService ] },
  { path: 'consumo_publicidad_sprl', component: PublicidadSprlComponent, canActivate: [ AuthGaurdService ] },
  { path: 'consumo_publicidad_sin_identificacion', component: PublicidadSinIdentComponent, canActivate: [ AuthGaurdService ] },
  { path: 'consumo_servicio_inscripcion', component: ServicioInscripcionComponent, canActivate: [ AuthGaurdService ] },
  { path: 'distribucion_consumos_sprl', component: DistribucionSprlComponent, canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecaudacionSprlRoutingModule { }
