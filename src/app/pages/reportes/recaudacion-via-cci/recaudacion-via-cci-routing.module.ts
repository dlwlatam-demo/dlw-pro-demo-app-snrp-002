import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { TransferenciaViaCciComponent } from './transferencia-via-cci/transferencia-via-cci.component';
import { AbonoCuentaSprlComponent } from './abono-cuenta-sprl/abono-cuenta-sprl.component';

const routes: Routes = [
  { path: 'recaudacion_transferencia_via_cci', component: TransferenciaViaCciComponent, canActivate: [ AuthGaurdService ] },
  { path: 'abono_cuenta_id_sprl', component: AbonoCuentaSprlComponent, canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecaudacionViaCciRoutingModule { }
