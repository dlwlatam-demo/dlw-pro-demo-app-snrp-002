import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecaudacionViaCciRoutingModule } from './recaudacion-via-cci-routing.module';
import { ComponentsModule } from '../components/components.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';

import { TransferenciaViaCciComponent } from './transferencia-via-cci/transferencia-via-cci.component';
import { AbonoCuentaSprlComponent } from './abono-cuenta-sprl/abono-cuenta-sprl.component';


@NgModule({
  declarations: [
    TransferenciaViaCciComponent,
    AbonoCuentaSprlComponent
  ],
  imports: [
    CommonModule,
    RecaudacionViaCciRoutingModule,
    NgPrimeModule,
    ComponentsModule
  ]
})
export class RecaudacionViaCciModule { }
