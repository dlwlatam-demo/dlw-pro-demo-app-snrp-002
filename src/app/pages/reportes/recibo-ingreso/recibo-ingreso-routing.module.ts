import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { ClasificadorComponent } from './clasificador/clasificador.component';

const routes: Routes = [
  { path: 'clasificadores', component: ClasificadorComponent, canActivate: [AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReciboIngresoRoutingModule { }
