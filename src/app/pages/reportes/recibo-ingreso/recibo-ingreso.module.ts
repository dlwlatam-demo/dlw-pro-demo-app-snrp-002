import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReciboIngresoRoutingModule } from './recibo-ingreso-routing.module';
import { ComponentsModule } from '../components/components.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';

import { ClasificadorComponent } from './clasificador/clasificador.component';


@NgModule({
  declarations: [
    ClasificadorComponent
  ],
  imports: [
    CommonModule,
    ReciboIngresoRoutingModule,
    NgPrimeModule,
    ComponentsModule
  ]
})
export class ReciboIngresoModule { }
