import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecaudacionMacmypeRoutingModule } from './recaudacion-macmype-routing.module';
import { ComponentsModule } from '../components/components.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';

import { CuadroMacmypeComponent } from './cuadro-macmype/cuadro-macmype.component';
import { GastosMacmypeComponent } from './gastos-macmype/gastos-macmype.component';
import { ConciliacionMacmypeComponent } from './conciliacion-macmype/conciliacion-macmype.component';


@NgModule({
  declarations: [
    CuadroMacmypeComponent,
    GastosMacmypeComponent,
    ConciliacionMacmypeComponent
  ],
  imports: [
    CommonModule,
    RecaudacionMacmypeRoutingModule,
    NgPrimeModule,
    ComponentsModule
  ]
})
export class RecaudacionMacmypeModule { }
