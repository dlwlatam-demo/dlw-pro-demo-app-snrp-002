import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { CuadroMacmypeComponent } from './cuadro-macmype/cuadro-macmype.component';
import { GastosMacmypeComponent } from './gastos-macmype/gastos-macmype.component';
import { ConciliacionMacmypeComponent } from './conciliacion-macmype/conciliacion-macmype.component';

const routes: Routes = [
  { path: 'cuadro_recaudacion_macmype', component: CuadroMacmypeComponent, canActivate: [ AuthGaurdService ] },
  { path: 'gastos_macmype', component: GastosMacmypeComponent, canActivate: [ AuthGaurdService ] },
  { path: 'conciliacion_recaudacion_macmype', component: ConciliacionMacmypeComponent, canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecaudacionMacmypeRoutingModule { }
