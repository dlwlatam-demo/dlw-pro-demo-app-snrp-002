import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../services/auth/auth-gaurd.service';

import { TasasRegistralesComponent } from './tasas-registrales/tasas-registrales.component';
import { LibroBancoSiafComponent } from './libro-banco-siaf/libro-banco-siaf.component';

const routes: Routes = [
  {
    path: 'comprobantes_pago',
    loadChildren: () => import('./comprobantes-pago/comprobantes-pago.module').then( m => m.ComprobantesPagoModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'recibo_ingreso',
    loadChildren: () => import('./recibo-ingreso/recibo-ingreso.module').then( m => m.ReciboIngresoModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'recaudacion_medios_electronicos',
    loadChildren: () => import('./recaudacion-medios-electronicos/recaudacion-medios-electronicos.module').then( m => m.RecaudacionMediosElectronicosModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'recaudacion_app_qr_sid',
    loadChildren: () => import('./recaudacion-app-qr-sid/recaudacion-app-qr-sid.module').then( m => m.RecaudacionAppQrSidModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'recaudacion_sprl',
    loadChildren: () => import('./recaudacion-sprl/recaudacion-sprl.module').then( m => m.RecaudacionSprlModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'recaudacion_pagalo_pe',
    loadChildren: () => import('./recaudacion-pagalo.pe/recaudacion-pagalo.pe.module').then( m => m.RecaudacionPagaloPeModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'recaudacion_macmype',
    loadChildren: () => import('./recaudacion-macmype/recaudacion-macmype.module').then( m => m.RecaudacionMacmypeModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'recaudacion_via_cci',
    loadChildren: () => import('./recaudacion-via-cci/recaudacion-via-cci.module').then( m => m.RecaudacionViaCciModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'recaudacion_scunac_cut',
    loadChildren: () => import('./recaudacion-scunac-cut/recaudacion-scunac-cut.module').then( m => m.RecaudacionScunacCutModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'tasas_registrales',
    component: TasasRegistralesComponent,
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'conciliacion_libro_banco',
    component: LibroBancoSiafComponent,
    canActivate: [ AuthGaurdService ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportesRoutingModule { }
