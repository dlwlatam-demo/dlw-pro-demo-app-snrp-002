import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecaudacionAppQrSidRoutingModule } from './recaudacion-app-qr-sid-routing.module';
import { ComponentsModule } from '../components/components.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';

import { RecaudacionAppComponent } from './recaudacion-app/recaudacion-app.component';
import { RecaudacionQrComponent } from './recaudacion-qr/recaudacion-qr.component';
import { RecaudacionSidComponent } from './recaudacion-sid/recaudacion-sid.component';
import { DistribucionAppComponent } from './distribucion-app/distribucion-app.component';
import { DistribucionQrComponent } from './distribucion-qr/distribucion-qr.component';
import { DistribucionSidComponent } from './distribucion-sid/distribucion-sid.component';


@NgModule({
  declarations: [
    RecaudacionAppComponent,
    RecaudacionQrComponent,
    RecaudacionSidComponent,
    DistribucionAppComponent,
    DistribucionQrComponent,
    DistribucionSidComponent
  ],
  imports: [
    CommonModule,
    RecaudacionAppQrSidRoutingModule,
    NgPrimeModule,
    ComponentsModule
  ]
})
export class RecaudacionAppQrSidModule { }
