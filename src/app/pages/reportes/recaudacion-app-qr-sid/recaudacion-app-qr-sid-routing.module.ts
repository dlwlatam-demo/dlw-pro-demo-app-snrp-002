import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { RecaudacionAppComponent } from './recaudacion-app/recaudacion-app.component';
import { RecaudacionQrComponent } from './recaudacion-qr/recaudacion-qr.component';
import { RecaudacionSidComponent } from './recaudacion-sid/recaudacion-sid.component';
import { DistribucionAppComponent } from './distribucion-app/distribucion-app.component';
import { DistribucionQrComponent } from './distribucion-qr/distribucion-qr.component';
import { DistribucionSidComponent } from './distribucion-sid/distribucion-sid.component';

const routes: Routes = [
  { path: 'recaudacion_app_sunarp', component: RecaudacionAppComponent, canActivate: [ AuthGaurdService ] },
  { path: 'recaudacion_qr', component: RecaudacionQrComponent, canActivate: [ AuthGaurdService ] },
  { path: 'recaudacion_sid', component: RecaudacionSidComponent, canActivate: [ AuthGaurdService ] },
  { path: 'cuadro_distribucion_app_sunarp', component: DistribucionAppComponent, canActivate: [ AuthGaurdService ] },
  { path: 'cuadro_distribucion_qr', component: DistribucionQrComponent, canActivate: [ AuthGaurdService ] },
  { path: 'cuadro_distribucion_sid', component: DistribucionSidComponent, canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecaudacionAppQrSidRoutingModule { }
