import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { RecaudacionDiarioComponent } from './recaudacion-diario/recaudacion-diario.component';
import { ConsolidadoMensualComponent } from './consolidado-mensual/consolidado-mensual.component';

const routes: Routes = [
  { path: 'recaudacion_diario', component: RecaudacionDiarioComponent, canActivate: [ AuthGaurdService ] },
  { path: 'consolidado_mensual', component: ConsolidadoMensualComponent, canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecaudacionScunacCutRoutingModule { }
