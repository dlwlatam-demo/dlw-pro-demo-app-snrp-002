import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, map, switchMap, timer } from 'rxjs';

import Swal from 'sweetalert2';
import { MessageService } from 'primeng/api';

import { MenuOpciones } from '../../../../models/auth/Menu.model';

import { UtilService } from '../../../../services/util.service';
import { ValidatorsService } from '../../../../core/services/validators.service';
import { RecaudacionScunacService } from '../../../../services/reportes/recaudacion-scunac/recaudacion-scunac.service';

import { BaseBandeja } from '../../../../base/base-bandeja.abstract';

@Component({
  selector: 'app-consolidado-mensual',
  templateUrl: './consolidado-mensual.component.html',
  styleUrls: ['./consolidado-mensual.component.scss'],
  providers: [
    MessageService
  ]
})
export class ConsolidadoMensualComponent extends BaseBandeja implements OnInit {

  today: Date = new Date();
  disableButton: boolean = true;

  filtros!: FormGroup;

  timerSubscription: Subscription = new Subscription();

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private messageService: MessageService,
    private validateService: ValidatorsService,
    private scunacService: RecaudacionScunacService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Reporte_Consolidado_Mensual;
    this.btnReporteConsolidadoMensual();

    sessionStorage.removeItem('reportePDF');
    sessionStorage.removeItem('reporteExcel');

    const fechaDesde = new Date( this.today.getFullYear(), this.today.getMonth(), 1 );

    this.filtros = this.fb.group({
      zonas: ['', [ Validators.required ]],
      oficinas: ['', [ Validators.required ]],
      fecDesde: [fechaDesde, [ Validators.required ]],
      fecHasta: [this.today, [ Validators.required ]]
    });
  }

  generarReporte() {
    this.disableButton = true;
    
    let zona = this.filtros.get('zonas')?.value;
    zona = zona == '*' ? '' : zona;

    let oficina = this.filtros.get('oficinas')?.value;
    oficina = oficina == '*' ? '' : oficina;

    const data = {
      coZonaRegi: zona,
      coOficRegi: oficina,
      feDesd: this.validateService.formatDate( this.filtros.get('fecDesde')?.value ),
      feHast: this.validateService.formatDate( this.filtros.get('fecHasta')?.value )
    }

    this.obtenerReporte( data );
  }

  obtenerReporte( data: any ) {
    Swal.showLoading();
    this.scunacService.reporteConsolidadoMensual( data ).subscribe({
      next: ( data ) => {
        console.log('data', data);

        this.timerSubscription = timer(0, 10000).pipe(
          switchMap(() => {
            return this.scunacService.timeOutConsolidadoMensual( data.guid ).pipe(
              map( res => {
                console.log('res', res);
                if ( res.status === '1' ) {
                  this.timerSubscription.unsubscribe();

                  const response = res.response;
                  if (response.codResult < 0 ) {
                    this.utilService.onShowAlert("info", "Atención", response.msgResult);
                    return;
                  }
          
                  this.visualizarReporte( response.reportePdf );
                  
                  sessionStorage.setItem('reportePDF', response.reportePdf);
                  sessionStorage.setItem('reporteExcel', response.reporteExcel);
          
                  this.messageService.add({
                    severity: 'success', 
                    summary: 'Reporte generado exitósamente', 
                    detail: 'Puede proceder con la descarga de su reporte.',
                    life: 5000
                  });
          
                  Swal.close();
                  this.disableButton = false;
                }
              }));
            })
          )
          .subscribe( data => {
            console.log("timer report: " + data);
          });
      },
      error: ( e ) => {
        console.log( e );
        Swal.close();

        if ( e.error.category == 'NOT_FOUND' ) {
          this.messageService.add({
            severity: 'info', 
            summary: e.error.description,
            detail: 'Por favor, modifique los parámetros seleccionados.',
            life: 5000
          });

          sessionStorage.removeItem('reportePDF');
          sessionStorage.removeItem('reporteExcel');

          return;
        }

        this.messageService.add({
          severity: 'error', 
          summary: 'Falla en generar el Reporte', 
          detail: 'Ocurrió un error al momento de procesar la información. Por favor, inténtelo nuevamente.',
          life: 5000
        });

        sessionStorage.removeItem('reportePDF');
        sessionStorage.removeItem('reporteExcel');
      }
    });
  }

  exportPdf() {
    const reportePDF = sessionStorage.getItem('reportePDF');

    if ( reportePDF === null || reportePDF === 'null' ) {
      return;
    }

    const fileName = 'Reporte_Consolidado_Mensual_SCUNAC';

    const byteCharacters = atob(reportePDF);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/pdf' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', fileName);
    document.body.appendChild( download );
    download.click();
  }

  exportExcel() {
    const reporteExcel = sessionStorage.getItem('reporteExcel');

    if ( reporteExcel === null || reporteExcel === 'null' ) {
      return;
    }

    const fileName = 'Reporte_Consolidado_Mensual_SCUNAC';

    const byteCharacters = atob(reporteExcel);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', fileName);
    document.body.appendChild( download );
    download.click();
  }

}
