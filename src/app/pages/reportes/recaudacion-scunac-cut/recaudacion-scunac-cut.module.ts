import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecaudacionScunacCutRoutingModule } from './recaudacion-scunac-cut-routing.module';
import { ComponentsModule } from '../components/components.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';

import { RecaudacionDiarioComponent } from './recaudacion-diario/recaudacion-diario.component';
import { ConsolidadoMensualComponent } from './consolidado-mensual/consolidado-mensual.component';


@NgModule({
  declarations: [
    RecaudacionDiarioComponent,
    ConsolidadoMensualComponent
  ],
  imports: [
    CommonModule,
    RecaudacionScunacCutRoutingModule,
    NgPrimeModule,
    ComponentsModule
  ]
})
export class RecaudacionScunacCutModule { }
