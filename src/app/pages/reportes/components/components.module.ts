import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';

import { ReportesFiltrosComponent } from './reportes-filtros/reportes-filtros.component';



@NgModule({
  declarations: [
    ReportesFiltrosComponent
  ],
  exports: [
    ReportesFiltrosComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgPrimeModule
  ]
})
export class ComponentsModule { }
