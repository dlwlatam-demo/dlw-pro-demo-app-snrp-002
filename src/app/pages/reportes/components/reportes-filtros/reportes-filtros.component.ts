import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable, of, map } from 'rxjs';

import { IResponse3 } from '../../../../interfaces/general.interface';
import { MesAnio } from '../../../../interfaces/combos.interface';

import { GeneralService } from '../../../../services/general.service';
import { CuentaContableService } from '../../../../services/mantenimientos/maestras/cuenta-contable.service';
import { ClasificadorService } from '../../../../services/mantenimientos/maestras/clasificador.service';

@Component({
  selector: 'app-reportes-filtros',
  templateUrl: './reportes-filtros.component.html',
  styleUrls: ['./reportes-filtros.component.scss']
})
export class ReportesFiltrosComponent implements OnInit {

  alertFecDesde!: boolean;
  alertFecHasta!: boolean;
  listaOperadores!: IResponse3[];

  $zonas!: Observable< any >;
  $oficinas: Observable< any > = of([{ coOficRegi: '*', deOficRegi: '(TODOS)' }]);
  $locales: Observable< any > = of([{ coLocaAten: '*', deLocaAten: '(TODOS)' }]);
  $operador!: Observable< any >;
  $conciliacion!: Observable< any >;
  $contables!: Observable< any >
  $clasificador!: Observable< any >
  $estado!: Observable< any >;
  $tramite!: Observable< any >;
  $pago!: Observable< any >;
  $canal!: Observable< any >;
  $mes!: MesAnio[];

  @Input()
  form!: FormGroup

  @Input()
  value!: string;

  @Input()
  alertZona!: boolean;

  @Input()
  alertOper!: boolean;

  @Input()
  alertMes!: boolean;

  @Input()
  zonas: boolean = true;

  @Input()
  oficinas: boolean = true;
  
  @Input()
  locales: boolean = false;
  
  @Input()
  fechas: boolean = false;

  @Input()
  operador: boolean = false;

  @Input()
  conciliacion: boolean = false;

  @Input()
  mesAnio: boolean = false;

  @Input()
  cuenta: boolean = false;

  @Input()
  check: boolean = false;

  @Input()
  contable: boolean = false

  @Input()
  estado: boolean = false;

  @Input()
  listConci: boolean = false;
  
  @Input()
  clsf: boolean = false;

  constructor(
    private generalService: GeneralService,
    private contableService: CuentaContableService,
    private clasificadorService: ClasificadorService
  ) { }

  ngOnInit(): void {
    let zonaItem: any;

    if ( this.value == 'obligatorio' )
      zonaItem = { coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' };
    else
      zonaItem = { coZonaRegi: '*', deZonaRegi: '(TODOS)' };

    if ( this.value == 'sprl' ) {
      this.$zonas = this.generalService.getCbo_ZonasRegistrales().pipe(
        map( z => {
          z.unshift( zonaItem );
          return z;
        })
      );  
    } else {
      this.$zonas = this.generalService.getCbo_Zonas_Usuario('').pipe(
        map( z => {
          z.unshift( zonaItem );
          return z;
        })
      );
    }

    if ( this.operador )
      this.CboOperador();

    if ( this.conciliacion )
      this.Cbo_Conciliacion();

    if ( this.mesAnio )
      this.Cbo_Mes();
    
    if ( this.contable )
      this.Cbo_Contable();

    if ( this.estado )
      this.Cbo_Estado();

    if ( this.clsf )
      this.Cbo_Clasificador();
  }

  buscarOficinaPorZona( event: any ) {
    const oficinaItem: any = { coOficRegi: '*', deOficRegi: '(TODOS)' };
    this.alertZona = false;

    if ( this.value == 'sprl' ) {
      if ( event.value == '*' )
      this.$oficinas = of([ oficinaItem ]);
      else
        this.$oficinas = this.generalService.getCbo_OficinasRegistrales(event.value).pipe(
          map( o => {
            o.unshift( oficinaItem );
            return o;
          })
        );  
    } else {
      if ( event.value == '*' )
        this.$oficinas = of([ oficinaItem ]);
      else
        this.$oficinas = this.generalService.getCbo_Oficinas_Zonas('', event.value).pipe(
          map( o => {
            o.unshift( oficinaItem );
            return o;
          })
        );
    }
  }

  buscarLocalAtencion( event: any ) {
    const localItem: any = { coLocaAten  : '*', deLocaAten: '(TODOS)' };
    const zonas = this.form.get('zonas')?.value;

    if ( event.value === '*') {
      this.$locales = of([ localItem ]);
    } else {
      this.$locales = this.generalService.getCbo_Locales_Ofic('', zonas, event.value).pipe(
        map( l => {
          l.unshift( localItem );
          return l;
        })
      );
    }
  }

  CboOperador() {
    let operadorItem: any;

    if ( this.value == 'electronico' || this.value == 'sprl' ) 
      this.listaOperadores = [{ ccodigoHijo: 0, cdescri: '(SELECCIONAR)' }];
    else
      this.listaOperadores = [{ ccodigoHijo: 0, cdescri: '(TODOS)' }];

    switch ( this.value ) {
      case 'sprl':
        this.generalService.getCbo_OperadorSPRL().subscribe({
          next: ( data: any ) => {
            if (data.length === 1) {   
              this.listaOperadores.push({ ccodigoHijo: data[0].cccodigoHijo, cdescri: data[0].cdescri });    
              this.form.patchValue({ 'operador': data[0].cccodigoHijo });
            } else {    
              this.listaOperadores.push(...data);
            }  
          }
        });
         break;

      case 'app':
        this.generalService.getCbo_OperadoresAppQrSid().subscribe({
          next: ( data: any ) => {
            if (data.length === 1) {   
              this.listaOperadores.push({ ccodigoHijo: data[0].cccodigoHijo, cdescri: data[0].cdescri });    
              this.form.patchValue({ 'operador': data[0].cccodigoHijo });
            } else {    
              this.listaOperadores.push(...data);
            }  
          }
        });
         break;
      
      default:
        this.generalService.getCbo_Operadores().subscribe({
          next: ( data: any ) => {
            if (data.length === 1) {   
              this.listaOperadores.push({ ccodigoHijo: data[0].cccodigoHijo, cdescri: data[0].cdescri });    
              this.form.patchValue({ 'operador': data[0].cccodigoHijo });
            } else {    
              this.listaOperadores.push(...data);
            }  
          }
        });
    }
  }

  selectOperador() {
    this.alertOper = false;
  }

  selectMes() {
    this.alertMes = false;
  }

  Cbo_Mes() {
    const mesItem: MesAnio = { coMes: '*', deMes: '(SELECCIONAR)' };

    this.$mes = this.generalService.getCbo_MesesAnio();
    this.$mes.unshift( mesItem );
  }

  Cbo_Conciliacion() {
    const conciliacionItem: any = { ccodigoHijo: '*', cdescri: '(SELECCIONAR)' }

    this.$conciliacion = this.generalService.getCbo_TipoConciliacion().pipe(
      map( c => {
        c.unshift( conciliacionItem );
        return c;
      })
    )
  }

  Cbo_Contable() {
    const contableItem: any = { coCuenCont: '(TODOS)' }

    this.$contables = this.contableService.getBandejaCuentaContable('', '', 'A').pipe(
      map( c => {
        c.unshift( contableItem );
        return c;
      })
    );
  }

  cuentaContableDescripcion( event: any ) {
    this.$contables.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( event.value == c.coCuenCont ) {
          this.form.get('descripcion')?.setValue( c.noCuenCont )
        }
      }
    })
  }

  Cbo_Clasificador() {
    const clasificadorItem: any = { coClsf: '(TODOS)' }

    this.$clasificador = this.clasificadorService.getBandejaClasificadores(null, null, 'A').pipe(
      map( c => {
        c.unshift( clasificadorItem );
        return c;
      })
    );
  }

  clasificadorDescripcion( event: any ) {
    this.$clasificador.forEach( clsf => {
      for ( let c of clsf ) {
        if ( event.value == c.coClsf ) {
          this.form.get('desClsf')?.setValue( c.deClsf )
        }
      }
    })
  }

  Cbo_Estado() {
    const estadoItem: any = { ccodigoHijo: '*', cdescri: '(TODOS)' };

    if ( this.value === 'clsf' ) {
      this.$estado = this.generalService.getCbo_EstadoRecibo().pipe(
        map( e => {
          e.unshift( estadoItem );
          return e;
        })
      )
    } else {
      this.$estado = this.generalService.getCbo_EstadoComprobante().pipe(
        map( e => {
          e.unshift( estadoItem );
          return e;
        })
      )
    }
  }

  cambioFechaInicio() {
    this.alertFecDesde = false;

    if ( this.form.get('fecHasta')?.value !== '' && this.form.get('fecHasta')?.value !== null ) {
      if ( this.form.get('fecDesde')?.value > this.form.get('fecHasta')?.value ) {
        this.alertFecDesde = true;
        this.form.controls['fecDesde'].reset();      
      }
      else {
        this.alertFecDesde = false;
      }
    }
  }

  cambioFechaDesde() {
    this.alertFecDesde = false;

    if ( this.form.get('fecHasta')?.value !== '' && this.form.get('fecHasta')?.value !== null ) {
      if ( this.form.get('fecDesde')?.value > this.form.get('fecHasta')?.value ) {
        this.alertFecDesde = true;
      }
      else {
        this.alertFecDesde = false;
      }
    }
  }

  cambioFechaFin() {
    this.alertFecHasta = false;

    if ( this.form.get('fecDesde')?.value !== '' && this.form.get('fecDesde')?.value !== null ) {
      if ( this.form.get('fecHasta')?.value < this.form.get('fecDesde')?.value ) {
        this.alertFecHasta = true;
        this.form.controls['fecHasta'].reset();    
      }
      else {
        this.alertFecHasta = false;
      } 
    }
  }

  cambioFechaHasta() {
    this.alertFecHasta = false;

    if ( this.form.get('fecDesde')?.value !== '' && this.form.get('fecDesde')?.value !== null ) {
      if ( this.form.get('fecHasta')?.value < this.form.get('fecDesde')?.value ) {
        this.alertFecHasta = true;   
      }
      else {
        this.alertFecHasta = false;
      } 
    }
  }


  validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  get disabled() {
    return this.form.get('anio')?.value == '';
  }

  onChangeChecked1( event: any ) {
    if ( event.checked && this.form.get('conClsf')?.value === true ) {
      this.form.get('clasificador')?.enable();
    }
    else if ( !event.checked && this.form.get('sinClsf')?.value === true ) {
      this.form.get('clasificador')?.setValue('');
      this.form.get('clasificador')?.disable();
    } 
    else {
      this.form.get('clasificador')?.enable();
    }
  }

  onChangeChecked2( event: any ) {
    if ( event.checked && this.form.get('conClsf')?.value === true ) {
      this.form.get('clasificador')?.enable();
    }
    else if ( event.checked ) {
      this.form.get('clasificador')?.setValue('');
      this.form.get('clasificador')?.disable();
    } 
    else {
      this.form.get('clasificador')?.enable();
    }
  }

}
