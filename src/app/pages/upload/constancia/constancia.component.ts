import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import Swal from 'sweetalert2';

import { TramaSustentoComp } from '../../../models/archivo-adjunto.model';

import { GeneralService } from '../../../services/general.service';

import { environment } from '../../../../environments/environment';

import { BaseBandeja } from '../../../base/base-bandeja.abstract';

@Component({
  selector: 'app-constancia',
  templateUrl: './constancia.component.html',
  styleUrls: ['./constancia.component.scss']
})
export class ConstanciaComponent extends BaseBandeja implements OnInit {

  maxFileSize!: number;
  fileLimit!: number;
  prog = 0;
  showprogress = false;
  uploadedFileConst: any[] = [];
  invalidFileSizeSummary: string = `${ environment.invalidFileSizeSummary }`
  invalidFileSizeDetail: string = `${ environment.invalidFileSizeDetail }`
  invalidFileTypeSummary: string = `${ environment.invalidFileTypeSummary }`
  invalidFileTypeDetail: string = `${ environment.invalidFileTypeDetail }`
  invalidFileLimitSummary: string = `${ environment.invalidFileLimitSummary }`
  invalidFileLimitDetail: string = `${ environment.invalidFileLimitDetail }`

  sustentoConst?: TramaSustentoComp[];
  
  @Input()
  disabled!: boolean;

  @Input()
  disabledDelete!: boolean;

  @Input()
  nroBandeja!: number;

  @Input()
  nroAdd!: number;

  @Input()
  nroDel!: number;

  @Input()
  acceptFiles: string = '.pdf, .xls, .xlsx, .doc, .docx, .png, .jpg, .jpge';
  
  @Output()
  uploadedConst = new EventEmitter<any>();

  @Output()
  onDeleteConst = new EventEmitter<string>();

  uploadingEndpoint!: string;

  constructor(
    private generalService: GeneralService
  ) { super() }

  goEdit(): void {
    if ( this.sustentoConst ) {
      this.sustentoConst.forEach( susConst => {
        const f = { idAdjunto: susConst.id, fileName: susConst.noDocuSust, fileId: susConst.id, guidVisor: susConst.idGuidDocu }
        this.uploadedConst.emit(f);
        this.uploadedFileConst.push(f);
      })
    }
  }

  ngOnInit(): void {

    this.uploadingEndpoint = this.generalService.getUploadingEndpoint()
    this.maxFileSize = environment.maxFileSize
    this.fileLimit = environment.fileLimitAdju
    this.uploadedFileConst = []
    // this.goEdit()
  }

  reset() { this.uploadedFileConst = [] }

  get isDisabled() {
    return (this.uploadedFileConst.length >= this.fileLimit) || this.disabled
  }

  onProgress(event: any) {
    this.prog = event.progress
    this.showprogress = true

  }

  onUpload(event: any) {
    this.showprogress = false;
    for (let file of event.files) {
      const f = { fileId: event.originalEvent.body.idCarga, fileName: file.name }
      this.uploadedConst.emit(f)
      this.uploadedFileConst.push(f);
    }
  }

  deleteUploaded(fileId: string) {
    
    const index = this.uploadedFileConst.findIndex(el => { return el.fileId == fileId })

    if (index > -1) {
      this.uploadedFileConst.splice(index, 1)
      this.onDeleteConst.emit(fileId)
    }


  }

  onError(ul: any) {
     
    Swal.fire(
      'Ha ocurrido un error al subir la imagen',
      '',
      'warning'
    );

    this.showprogress = false
    ul.clear()
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

}
