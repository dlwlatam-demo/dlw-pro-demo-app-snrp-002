import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { MessageService } from 'primeng/api';

import { IOnSelect, IRowReorder } from '../../../interfaces/archivo-adjunto.interface';

import { TramaSustentoComp } from '../../../models/archivo-adjunto.model';

import { GeneralService } from '../../../services/general.service';

import { environment } from '../../../../environments/environment';

import { BaseBandeja } from '../../../base/base-bandeja.abstract';

@Component({
  selector: 'app-otros',
  templateUrl: './otros.component.html',
  styleUrls: ['./otros.component.scss']
})
export class OtrosComponent extends BaseBandeja implements OnInit {

  maxFileSize!: number;
  fileLimit!: number;
  prog = 0;
  showprogress = false;
  uploadedFilesOtros: any[] = [];
  invalidFileSizeSummary: string = `${ environment.invalidFileSizeSummary }`
  invalidFileSizeDetail: string = `${ environment.invalidFileSizeDetail }`
  invalidFileTypeSummary: string = `${ environment.invalidFileTypeSummary }`
  invalidFileTypeDetail: string = `${ environment.invalidFileTypeDetail }`
  invalidFileLimitSummary: string = `${ environment.invalidFileLimitSummary }`
  invalidFileLimitDetail: string = `${ environment.invalidFileLimitDetail }`

  sustentosOtros?: TramaSustentoComp[];
  
  @Input()
  disabled!: boolean;

  @Input()
  disabledDelete!: boolean;

  @Input()
  nroBandeja!: number;

  @Input()
  nroAdd!: number;

  @Input()
  nroDel!: number;

  @Input()
  acceptFiles: string = '.pdf, .xls, .xlsx, .doc, .docx, .png, .jpg, .jpge';
  
  @Output()
  uploadedOtros = new EventEmitter<any>();

  @Output()
  onDeleteOtros = new EventEmitter<string>();

  @Output()
  onSelectOtros = new EventEmitter<IOnSelect>();

  uploadingEndpoint!: string;

  constructor(
    private messageService: MessageService,
    private generalService: GeneralService,
  ) { super() }

  goEdit(): void {
    if ( this.sustentosOtros ) {
      this.sustentosOtros.forEach( s => {
        const f = { idAdjunto: s.id, fileName: s.noDocuSust, fileId: s.id, guidVisor: s.idGuidDocu }
        this.uploadedOtros.emit(f)
        this.uploadedFilesOtros.push(f);
      })
    }
  }

  ngOnInit(): void {

    this.uploadingEndpoint = this.generalService.getUploadingEndpoint();
    this.maxFileSize = environment.maxFileSize;
    this.fileLimit = environment.fileLimit;
    this.uploadedFilesOtros = [];
    // this.goEdit()
  }

  reset() { this.uploadedFilesOtros = [] }

  get isDisabled() {
    return (this.uploadedFilesOtros.length >= this.fileLimit) || this.disabled
  }

  onProgress(event: any) {
    this.prog = event.progress
    this.showprogress = true

  }

  onSelectFile(event: IOnSelect, fileInput: any) {
    fileInput.clear();
    this.showprogress = true;
    let sizeCurrentFiles = event.currentFiles.length;

    for ( let file of event.currentFiles ) {
      this.generalService.getUploadingEndpointObservable( file ).subscribe({
        next: ( data ) => {

          if ( data.codResult === 0 ) {
            sizeCurrentFiles = sizeCurrentFiles - 1;

            const f = { fileId: data.idCarga, fileName: file.name }
            this.uploadedOtros.emit(f);
            this.uploadedFilesOtros.push(f);

            if ( sizeCurrentFiles === 0 ) {
              this.showprogress = false;
            }
          }

        },
        error: ( error ) => {
          console.log('err', error.error.description );
          this.messageService.add({
            severity: 'warn',
            summary: `${ error.error.description }`,
            sticky: true
          });
        }
      });
    }
  }

  onUpload(event: any) {
    this.showprogress = false;
    for (let file of event.files) {
      const f = { fileId: event.originalEvent.body.idCarga, fileName: file.name }
      this.uploadedOtros.emit(f)
      this.uploadedFilesOtros.push(f);
    }
  }

  deleteUploaded(fileId: string) {

    const index = this.uploadedFilesOtros.findIndex(el => { return el.fileId == fileId })

    if (index > -1) {
      this.uploadedFilesOtros.splice(index, 1)
      this.onDeleteOtros.emit(fileId)
    }


  }

  onError(ul: any) {

    this.messageService.add({
      severity: 'warn',
      summary: 'Ha ocurrido un error al subir la imagen',
      sticky: true
    });

    this.showprogress = false
    ul.clear()
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

}
