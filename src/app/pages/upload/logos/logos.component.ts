import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { IDataLogo } from '../../../interfaces/configuracion/configurar-logos';

import { GeneralService } from '../../../services/general.service';
import { UtilService } from '../../../services/util.service';

import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-logos',
  templateUrl: './logos.component.html',
  styleUrls: ['./logos.component.scss']
})
export class LogosComponent implements OnInit {

  public idGuid: string = '';
  public nameFile: string = '';

  public maxFileSize!: number;
  public uploadingEndpoint!: string;

  public invalidFileSizeSummary: string = `${ environment.invalidFileSizeSummary }`
  public invalidFileSizeDetail: string = `${ environment.invalidFileSizeDetail }`
  public invalidFileTypeSummary: string = `${ environment.invalidFileTypeSummary }`
  public invalidFileTypeDetail: string = `${ environment.invalidFileTypeDetail }`
  public invalidFileLimitSummary: string = `${ environment.invalidFileLimitSummary }`
  public invalidFileLimitDetail: string = `${ environment.invalidFileLimitDetail }`

  @Input() posicionDeLogo: string = '';
  @Input() disableUpload: boolean = false;
  @Output() eventDataLogo = new EventEmitter<IDataLogo>();
  @Output() uploaded = new EventEmitter<any>();

  constructor(
    private utilService: UtilService,
    private generalService: GeneralService,
  ) { }

  ngOnInit(): void {
    this.maxFileSize = environment.maxFileSize
    this.uploadingEndpoint = `${ this.generalService.getUploadingEndpoint() }?tipo=IMG`;
  }

  public uploadFiles( event: any ) {
    for ( let file of event.files ) {
      const f = { fileId: event.originalEvent.body.idCarga, fileName: file.name }
      this.uploaded.emit(f);
    }
  }

  public uploadImage( event: any ) {
    this.uploadFiles( event );

    const reader = new FileReader();
    reader.readAsDataURL( event.files[0] );

    reader.onload = () => {
      const dataFileLogo: IDataLogo = {
        posicionDeLogo: this.posicionDeLogo,
        nameFile: event.files[0].name,
        idGuid: event.originalEvent.body.idCarga,
        imgReader: reader.result.toString().replace(/^data:(.*,)?/, '')
      }
  
      this.eventDataLogo.emit( dataFileLogo );
    }
  }

  public onError(ul: any) {
    
    this.utilService.onShowAlert('warning', 'Ha ocurrido un error al subir la imagen', '');

    // this.showprogress = false
    ul.clear()
  }

}
