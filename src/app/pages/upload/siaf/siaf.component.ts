import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import Swal from 'sweetalert2';

import { TramaSustentoComp } from '../../../models/archivo-adjunto.model';

import { GeneralService } from '../../../services/general.service';

import { environment } from '../../../../environments/environment';

import { BaseBandeja } from '../../../base/base-bandeja.abstract';

@Component({
  selector: 'app-siaf',
  templateUrl: './siaf.component.html',
  styleUrls: ['./siaf.component.scss']
})
export class SiafComponent extends BaseBandeja implements OnInit {

  maxFileSize!: number;
  fileLimit!: number;
  prog = 0;
  showprogress = false;
  uploadedFilesSiaf: any[] = [];
  invalidFileSizeSummary: string = `${ environment.invalidFileSizeSummary }`
  invalidFileSizeDetail: string = `${ environment.invalidFileSizeDetail }`
  invalidFileTypeSummary: string = `${ environment.invalidFileTypeSummary }`
  invalidFileTypeDetail: string = `${ environment.invalidFileTypeDetail }`
  invalidFileLimitSummary: string = `${ environment.invalidFileLimitSummary }`
  invalidFileLimitDetail: string = `${ environment.invalidFileLimitDetail }`

  sustentosSiaf?: TramaSustentoComp[];
  
  @Input()
  disabled!: boolean;

  @Input()
  disabledDelete!: boolean;

  @Input()
  nroBandeja!: number;

  @Input()
  nroAdd!: number;

  @Input()
  nroDel!: number;

  @Input()
  acceptFiles: string = '.pdf, .xls, .xlsx, .doc, .docx, .png, .jpg, .jpge';
  
  @Output()
  uploadedSiaf = new EventEmitter<any>();

  @Output()
  onDeleteSiaf = new EventEmitter<string>();

  uploadingEndpoint!: string;

  constructor(
    private generalService: GeneralService
  ) { super() }

  goEdit(): void {
    if ( this.sustentosSiaf ) {
      this.sustentosSiaf.forEach( siaf => {
        const f = { idAdjunto: siaf.id, fileName: siaf.noDocuSust, fileId: siaf.id, guidVisor: siaf.idGuidDocu }
        this.uploadedSiaf.emit(f);
        this.uploadedFilesSiaf.push(f);
      })
    }
  }

  ngOnInit(): void {

    this.uploadingEndpoint = this.generalService.getUploadingEndpoint()
    this.maxFileSize = environment.maxFileSize
    this.fileLimit = environment.fileLimitAdju
    this.uploadedFilesSiaf = []
    // this.goEdit()
  }

  reset() { this.uploadedFilesSiaf = [] }

  get isDisabled() {
    return (this.uploadedFilesSiaf.length >= this.fileLimit) || this.disabled
  }

  onProgress(event: any) {
    this.prog = event.progress
    this.showprogress = true

  }

  onUpload(event: any) {
    this.showprogress = false;
    for (let file of event.files) {
      const f = { fileId: event.originalEvent.body.idCarga, fileName: file.name }
      this.uploadedSiaf.emit(f)
      this.uploadedFilesSiaf.push(f);
    }
  }

  deleteUploaded(fileId: string) {
    
    const index = this.uploadedFilesSiaf.findIndex(el => { return el.fileId == fileId })

    if (index > -1) {
      this.uploadedFilesSiaf.splice(index, 1)
      this.onDeleteSiaf.emit(fileId)
    }


  }

  onError(ul: any) {
     
    Swal.fire(
      'Ha ocurrido un error al subir la imagen',
      '',
      'warning'
    );

    this.showprogress = false
    ul.clear()
  }

  visorEndpoint(guid: string): string {
    return this.generalService.downloadManager(guid)
  }

}
