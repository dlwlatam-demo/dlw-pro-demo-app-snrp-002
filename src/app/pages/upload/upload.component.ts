import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { MessageService } from 'primeng/api';

import { IDocuVariosSustento } from '../../interfaces/documentos-varios.interface';
import { IDocuEnviarSustento } from '../../interfaces/documentos-enviar.interface';
import { IRowReorder, IOnSelect } from '../../interfaces/archivo-adjunto.interface';

import { TramaSustento, TramaAnexo, TramaSustentoComp } from '../../models/archivo-adjunto.model';

import { GeneralService } from '../../services/general.service';
import { IdService } from '../../services/id.service';

import { environment } from '../../../environments/environment';

import { BaseBandeja } from '../../base/base-bandeja.abstract';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
  providers: [
    MessageService
  ]
})
export class UploadComponent extends BaseBandeja implements OnInit {

  maxFileSize!: number;
  fileLimit!: number;
  prog = 0;
  showprogress = false;
  uploadedFiles: any[] = [];
  uploadedFilesTmp: any[] = [];
  invalidFileSizeSummary: string = `${ environment.invalidFileSizeSummary }`
  invalidFileSizeDetail: string = `${ environment.invalidFileSizeDetail }`
  invalidFileTypeSummary: string = `${ environment.invalidFileTypeSummary }`
  invalidFileTypeDetail: string = `${ environment.invalidFileTypeDetail }`
  invalidFileLimitSummary: string = `${ environment.invalidFileLimitSummary }`
  invalidFileLimitDetail: string = `${ environment.invalidFileLimitDetail }`

  anexos?: TramaAnexo[];
  sustentos?: TramaSustento[];
  sustentosComp?: TramaSustentoComp[];
  sustDocuVari?: IDocuVariosSustento[];
  sustDocuEnvi?: IDocuEnviarSustento[];
  
  @Input()
  disabled!: boolean;

  @Input()
  disabledDelete!: boolean;

  @Input()
  reordenar: boolean = false;

  @Input()
  nroBandeja!: number;

  @Input()
  nroAdd!: number;

  @Input()
  nroDel!: number;

  @Input()
  acceptFiles: string = '.pdf, .xls, .xlsx, .doc, .docx, .png, .jpg, .jpge';

  @Input()
  multiple: boolean = false;
  
  @Output()
  uploaded = new EventEmitter<any>();

  @Output()
  onDelete = new EventEmitter<string>();

  @Output()
  onReorder = new EventEmitter<IRowReorder>();

  @Output()
  onSelect = new EventEmitter<IOnSelect>();

  uploadingEndpoint!: string;

  constructor(
    private idService: IdService,
    private messageService: MessageService,
    private generalService: GeneralService,
  ) { super() }

  goEdit(): void {
    if ( this.sustentos ) {
      this.sustentos.forEach( s => {
        const f = { idAdjunto: s.id, fileName: s.noDocuSust, fileId: s.id, guidVisor: s.idGuidDocu }
        this.uploaded.emit(f)
        this.uploadedFiles.push(f);
      })
    }

    if ( this.sustentosComp ) {
      this.sustentosComp.forEach( s => {
        const f = { idAdjunto: s.id, fileName: s.noDocuSust, fileId: s.id, guidVisor: s.idGuidDocu }
        this.uploaded.emit(f)
        this.uploadedFiles.push(f);
      })
    }

    if ( this.sustDocuVari ) {
      this.sustDocuVari.forEach( s => {
        const f = { idAdjunto: s.idDocuVariSust, fileName: s.noDocuSust, fileId: s.idDocuVariSust, guidVisor: s.idGuidDocu }
        this.uploaded.emit(f)
        this.uploadedFiles.push(f);
      })
    }

    if ( this.sustDocuEnvi ) {
      this.sustDocuEnvi.forEach( s => {
        const f = { idAdjunto: s.idDocuEnviSust, fileName: s.noDocuSust, fileId: s.idDocuEnviSust, guidVisor: s.idGuidDocu }
        this.uploaded.emit(f)
        this.uploadedFiles.push(f);
      })
    }

    if ( this.anexos ) {
      this.anexos.forEach( a => {
        const f = { idAdjunto: a.id, fileName: a.noDocuAnex, fileId: a.id, guidVisor: a.idGuidDocu }
        this.uploaded.emit(f)
        this.uploadedFiles.push(f);
      })
    }
  }

  ngOnInit(): void {

    this.uploadingEndpoint = this.generalService.getUploadingEndpoint();
    this.maxFileSize = environment.maxFileSize;
    this.fileLimit = environment.fileLimit;
    this.uploadedFiles = [];
    // this.goEdit()
  }

  reset() { this.uploadedFiles = [] }

  get isDisabled() {
    return (this.uploadedFiles.length >= this.fileLimit) || this.disabled
  }

  onProgress(event: any) {
    this.prog = event.progress
    this.showprogress = true

  }

  onSelectFile(event: IOnSelect, fileInput: any) {
    console.log('event.currentFiles', event.currentFiles);
    fileInput.clear();
    this.showprogress = true;

    const currentFiles = event.currentFiles.entries();
    let sizeCurrentFiles = event.currentFiles.length;
    const currentFiles1 = event.currentFiles.entries();

	let idxBase = this.uploadedFiles.length;
	console.log("idxBase = " + idxBase);
    for ( const [idx1, file1] of currentFiles1 ) {
		console.log(idx1 + ") file: " + file1.name);
		const f = { fileId: "", fileName: file1.name, id: idx1 };
		this.uploadedFiles.push(f);
	}
	console.log("idxBase2 = " + this.uploadedFiles.length);

    for ( const [idx, file] of currentFiles ) {
      this.generalService.getUploadingEndpointObservable( file ).subscribe({
        next: ( data ) => {
          
          if ( data.codResult === 0 ) {
            sizeCurrentFiles = sizeCurrentFiles - 1;
			
			this.uploadedFiles[idx + idxBase].fileId = data.idCarga;
			console.log('idx = ' + idx + " === " + this.uploadedFiles[idx + idxBase].id + " ) " + this.uploadedFiles[idx + idxBase].fileName);

            // const f = { fileId: data.idCarga, fileName: file.name, id: idx + idxBase };
            // console.log('f', f);
			
			/*

            if ( this.uploadedFiles.length === 0 ) {
              this.uploadedFiles.splice(idx, 0, f);
            } else if ( this.uploadedFiles.length !== 0 && sizeCurrentFiles > 1 ) {
              this.uploadedFiles.splice(idx + sizeCurrentFiles, 0, f);
            } else {
              this.uploadedFiles.push(f);
            }
			*/

            if ( sizeCurrentFiles === 0 ) {
              this.showprogress = false;
              for ( let f of this.uploadedFiles ) {
                this.uploaded.emit(f);
              }
              console.log('uploadedFiles', this.uploadedFiles)
            }
          }

        },
        error: ( error ) => {
          this.messageService.add({
            severity: 'warn',
            summary: `${ error.error.description }`,
            sticky: true
          });

          this.showprogress = false;
        }
      });
    }
  }

  onUpload(event: any) {
    this.showprogress = false;
	/*
    for (let file of event.files) {
      const f = { fileId: event.originalEvent.body.idCarga, fileName: file.name }
      this.uploaded.emit(f);
      this.uploadedFiles.push(f);
    }
	*/
  }

  deleteUploaded(fileId: string) {

    const index = this.uploadedFiles.findIndex(el => { return el.fileId == fileId })

    if (index > -1) {
      this.uploadedFiles.splice(index, 1)
      this.onDelete.emit(fileId)
    }


  }

  onError(ul: any) {

    this.messageService.add({
      severity: 'warn',
      summary: 'Ha ocurrido un error al subir la imagen',
      sticky: true
    });

    this.showprogress = false
    ul.clear()
  }

  onRowReorder( event: IRowReorder ) {
	 this.onReorder.emit( event );
    
    for ( let file of this.uploadedFiles ) {
      this.uploaded.emit( file );
    }
	
  }

  visorEndpoint(guid: string): string {


    return this.generalService.downloadManager(guid)
  }

}
