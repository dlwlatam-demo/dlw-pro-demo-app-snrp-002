import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { IUsuario, IUsuarioPerfil } from '../../../../interfaces/seguridad/usuario.interface';

import { UsuariosService } from '../../../../services/seguridad/usuarios.service';

@Component({
  selector: 'app-asignar-perfiles',
  templateUrl: './asignar-perfiles.component.html',
  styleUrls: ['./asignar-perfiles.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class AsignarPerfilesComponent implements OnInit {

  isLoading!: boolean;
  registros: any[] = [];

  form!: FormGroup;
  
  usuario!: IUsuario[];
  perfilList!: IUsuarioPerfil[];
  selectedPerfil: IUsuarioPerfil[] = [];

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private messageService: MessageService,
    private usuarioService: UsuariosService
  ) { }

  ngOnInit(): void {
    this.usuario = this.config.data.user as IUsuario[];

    this.form = this.fb.group({
      coUsua: [{value: '', disabled: true}, [ Validators.required ]],
      nomUsua: [{value: '', disabled: true}, [ Validators.required ]]
    });

    this.hidrateForm();
    this.listaDePerfiles();
  }

  get isDisabled() {
    return this.usuario['0'].inEstd == 'Inactivo';
  }

  hidrateForm() {
    const nomUsua: string = `${ this.usuario['0'].noEmpl } ${ this.usuario['0'].apPateEmpl } ${ this.usuario['0'].apMateEmpl }`;
    
    this.form.get('coUsua')?.setValue( this.usuario['0'].idUsua );
    this.form.get('nomUsua')?.setValue( nomUsua );
  }

  listaDePerfiles() {
    this.usuarioService.getUsuarioPerfilList( this.usuario['0'].coUsua! ).subscribe({
      next: ( data ) => {
        console.log( data );
        this.perfilList = data;
        this.selectedPerfil = this.perfilList.filter( p => p.inCheck == '1' );
      }
    });
  }

  grabarPerfiles() {
    if ( this.selectedPerfil.length < 1 ) {
      this.messageService.add({ severity: 'warn', summary: 'Debe seleccionar por lo menos un perfil', sticky: true });
      return;
    }

    for ( let registro of this.selectedPerfil ) {
      this.registros.push({
        nuSecu: String( registro.nuSecu ),
        coPerf: String( registro.coPerf )
      });

      console.log('registros', this.registros );
    }

    this.confirmService.confirm({
      message: `¿Está Ud. seguro de grabar los perfiles seleccionados para el usuario ${ this.usuario['0'].idUsua! }?`,
      accept: () => {
        this.isLoading = true;

        const data = {
          coUsua: this.usuario['0'].coUsua!,
          coSist: 20,
          trama: this.registros
        }
        console.log('dataPerfil', data)

        this.usuarioService.updateUsuarioPerfil( data ).subscribe({
          next: ( d ) => {
            console.log( d );

            if ( d.codResult < 0 ) {
              this.isLoading = false;
              this.ref.close('reject');
              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close();
  }

}
