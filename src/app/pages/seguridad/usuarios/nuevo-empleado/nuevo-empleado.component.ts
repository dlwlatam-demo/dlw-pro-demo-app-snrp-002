import { Component, OnInit, HostListener } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, map } from 'rxjs';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';

import { GeneralService } from '../../../../services/general.service';
import { UsuariosService } from '../../../../services/seguridad/usuarios.service';
import { IResponse2 } from '../../../../interfaces/general.interface';

@Component({
  selector: 'app-nuevo-empleado',
  templateUrl: './nuevo-empleado.component.html',
  styleUrls: ['./nuevo-empleado.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class NuevoEmpleadoComponent implements OnInit {

  isLoading!: boolean;
  blocked!: boolean;
  msgTipDocu!: boolean;
  maxLength: number = 8;

  empleado!: FormGroup;

  $cargos!: Observable<any>;
  $tipoDocumentos!: Observable< any >;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private generalService: GeneralService,
    private usuarioService: UsuariosService
  ) { }

  ngOnInit(): void {
    const cargoItem: any = { cdescri: '(NINGUNO)', ccodigoHijo: '*' };
    const tipoDocuItem: IResponse2 = { cdescri: '(SELECCIONAR)', ccodigoHijo: '*' };

    this.$cargos = this.generalService.getCbo_CargosUsuario().pipe(
      map( c => {
        c.unshift( cargoItem );
        return c;
      })
    );

    this.$tipoDocumentos = this.generalService.getCbo_TipoDocumentoIdentidad().pipe(
      map( t => {
        t.unshift( tipoDocuItem );
        return t;
      })
    );

    this.empleado = this.fb.group({
      tipoDocu: ['', [ Validators.required ]],
      nroDocu: ['', [ Validators.required ]],
      nomEmpl: ['', [ Validators.required ]],
      apePate: ['', [ Validators.required ]],
      apeMate: ['', []],
      cargo: ['', []]
    });
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent): void {
    if ( event.key === 'Tab' ) {
      document.getElementById('input2').focus()
    }
  }

  // saltarTab(event: KeyboardEvent, id: string) {
  //   if ( event.key === 'Tab' ) {
  //     console.log(id)
  //   }
  // }

  selectDocumento( event: any ) {
    this.msgTipDocu = false;

    switch ( event.value ) {
      case '004': 
        this.maxLength = 12;
        break;
      case '003':
        this.maxLength = 12;
        break;
      case '002':
        this.maxLength = 11;
        break;
      default:
        this.maxLength = 8;
    }
  }

  campoEsValido( campo: string ) {
    return this.empleado.controls[campo].errors && 
              this.empleado.controls[campo].touched
  }

  grabarEmpleado() {
    if ( this.empleado.invalid ) {
      this.empleado.markAllAsTouched();
      return;
    }

    let tipoDocu = this.empleado.get('tipoDocu')?.value;
    if ( tipoDocu == '*' ) {
      this.msgTipDocu = true;
      return;
    }

    const data = {
      tiDocuIden: tipoDocu,
      nuDocuIden: this.empleado.get('nroDocu')?.value,
      apPateEmpl: this.empleado.get('apePate')?.value,
      apMateEmpl: this.empleado.get('apeMate')?.value,
      noEmpl: this.empleado.get('nomEmpl')?.value,
      coCarg: this.empleado.get('cargo')?.value == '*' ? '' : this.empleado.get('cargo')?.value,
      coCondEmpl: ''
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de grabar los siguientes datos?',
      accept: () => {
        this.isLoading = true;
        this.blocked = true;
        this.empleado.disable();

        this.usuarioService.createEmpleado( data ).subscribe({
          next: ( d ) => {
            console.log('msgEmp', d );

            if ( d.codResult < 0 ) {
              this.messageService.add({severity: 'error', summary: `${ d.msgResult }`});
              this.empleado.enable();
              this.blocked = false;
              return;
            }

            data.coCondEmpl = d.coEmpl
            this.ref.close( data );
          },
          error: ( e ) => {
            console.log( e );

            this.ref.close();
          },
          complete: () => {
            this.isLoading = false;
            this.blocked = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
