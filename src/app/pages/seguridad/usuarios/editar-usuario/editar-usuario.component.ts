import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import { IUsuario } from '../../../../interfaces/seguridad/usuario.interface';

import { Funciones } from '../../../../core/helpers/funciones/funciones';
import { ValidatorsService } from '../../../../core/services/validators.service';

import { GeneralService } from '../../../../services/general.service';
import { UsuariosService } from '../../../../services/seguridad/usuarios.service';
import { UtilService } from '../../../../services/util.service';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class EditarUsuarioComponent implements OnInit {

  loadingUser!: boolean;
  isLoading!: boolean;
  consultar!: boolean;
  validarUsuario!: boolean;
  maxLength: number = 8;
  btnGrabar: boolean = false;

  usuario!: FormGroup;

  user!: IUsuario[];
  $tipoDocumentos!: Observable< any >;

  constructor(
    private fb: FormBuilder,
    private funciones: Funciones,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private utilService: UtilService,
    private validateService: ValidatorsService,
    private usuarioService: UsuariosService
  ) { }

  ngOnInit(): void {
    this.user = this.config.data.usuario as IUsuario[];
    console.log('user', this.user);
    this.consultar = this.config.data.vista;

    this.$tipoDocumentos = this.generalService.getCbo_TipoDocumentoIdentidad();

    this.usuario = this.fb.group({
      tipoDocu: [{value: '', disabled: true}, [ Validators.required ]],
      nroDocu: [{value: '', disabled: true}, [ Validators.required ]],
      coUsua: ['', [ Validators.required ]],
      coUsuaDomi: ['', [ Validators.required ]],
      nomUsua: ['', [ Validators.required ]],
      apePate: ['', [ Validators.required ]],
      apeMate: ['', []],
      fecAlta: ['', [ Validators.required ]],
      fecBaja: ['', []],
      email: ['', [ Validators.required, Validators.pattern( this.funciones.emailPattern ) ]]
    });

    this.hidrateForm();
  }

  hidrateForm() {
    if ( this.consultar ) {
      this.validarUsuario = true;
      this.usuario.disable();
    }
    
    const fecAlta = this.validateService.reFormateaFecha( this.user['0'].feIngr );
    const fecBaja = this.validateService.reFormateaFecha( this.user['0'].feBaja );

    this.usuario.get('tipoDocu').setValue( this.user['0'].tiDocuIden );
    this.usuario.get('nroDocu').setValue( this.user['0'].nuDocuIden );
    this.usuario.get('coUsua').setValue( this.user['0'].idUsua.trim() );
    this.usuario.get('coUsuaDomi').setValue( this.user[0].deUsuaDomi.trim() );
    this.usuario.get('nomUsua').setValue( this.user['0'].noEmpl.trim() );
    this.usuario.get('apePate').setValue( this.user['0'].apPateEmpl.trim() );
    this.usuario.get('apeMate').setValue( this.user['0'].apMateEmpl.trim() );
    this.usuario.get('fecAlta').setValue( fecAlta );
    this.usuario.get('fecBaja').setValue( fecBaja );
    this.usuario.get('email').setValue( this.user['0'].correoUsua.trim() );
  }

  selectDocumento( event: any ) {
    switch ( event.value ) {
      case '004': 
        this.maxLength = 12;
        break;
      case '003':
        this.maxLength = 12;
        break;
      case '002':
        this.maxLength = 11;
        break;
      default:
        this.maxLength = 8;
    }
  }

  getEmpleadoDisable(): boolean {
    return true //!this.usuario.get('nroDocu')?.value;
  }

  buscarUsuarioAD() {
    this.loadingUser = true;
    const coUsuaDomi = this.usuario.get('coUsuaDomi')?.value;

    if ( coUsuaDomi == '' ) {
      this.usuario.get('coUsuaDomi')?.markAsTouched();

      this.messageService.add({
        severity: 'warn',
        summary: 'Debe ingresar un Usuario de Red para validarlo',
        sticky: true
      });

      this.loadingUser = false;

      return;
    }

    this.usuarioService.getUsuarioAD( coUsuaDomi ).subscribe({
      next: ( data ) => {
        if ( data.codResult === 1 ) {
          this.messageService.add({ severity: 'success', summary: 'Usuario de Red si existe en el sistema', sticky: true });
          this.btnGrabar = false;
        }
        else {
          this.messageService.add({ severity: 'error', summary: 'Usuario de Red no existe en el sistema', sticky: true });
          this.btnGrabar = true;
        }

        this.loadingUser = false;

      },
      error: ( _err ) => {
        this.utilService.onShowMessageErrorSystem();

        this.loadingUser = false;
      }
    });
  }

  updateUsuario() {
    if ( this.usuario.invalid ) {
      this.messageService.add({ severity: 'warn', summary: 'Por favor, complete todos los campos obligatorios', sticky: true });
      this.usuario.markAllAsTouched();
      return;
    }

    const fecAlta = this.usuario.get('fecAlta')?.value;
    const fecBaja = this.usuario.get('fecBaja')?.value;
    let fecBajaFormat: string;

    if (fecBaja !== "" && fecBaja !== null) {
      if ( typeof fecBaja === 'string' )
        fecBajaFormat = fecBaja;
      else
        fecBajaFormat = this.validateService.formatDate( fecBaja );
    }
    else {
      fecBajaFormat = '';
    }

    const data = {
      coUsua: this.user['0'].coUsua,
      idUsua: this.usuario.get('coUsua')?.value,
      coEmpl: this.user['0'].coEmpl,
      coUsuaDomi: this.usuario.get('coUsuaDomi')?.value,
      correoUsua: this.usuario.get('email')?.value,
      feIngr: typeof fecAlta === 'string' ? fecAlta : this.validateService.formatDate( fecAlta ),
      feBaja: fecBajaFormat
    }
    console.log('dataUsuario', data);

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de actualizar los datos?',
      accept: () => {
        this.isLoading = true;

        this.usuarioService.createOrUpdateUsuario( 1, data ).subscribe({
          next: ( d ) => {
            console.log( d );

            if ( d.codResult < 0 ) {
              this.isLoading = false;
              this.messageService.add({ severity: 'warn', summary: 'Atención', detail: d.msgResult, sticky: true });
              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
