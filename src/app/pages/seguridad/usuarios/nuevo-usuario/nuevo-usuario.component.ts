import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';

import Swal from 'sweetalert2';

import { IEmpleado } from '../../../../interfaces/seguridad/usuario.interface';
import { IResponse2 } from '../../../../interfaces/general.interface';

import { Funciones } from '../../../../core/helpers/funciones/funciones';
import { ValidatorsService } from '../../../../core/services/validators.service';

import { GeneralService } from '../../../../services/general.service';
import { UsuariosService } from '../../../../services/seguridad/usuarios.service';

import { NuevoEmpleadoComponent } from '../nuevo-empleado/nuevo-empleado.component';
import { UtilService } from '../../../../services/util.service';

@Component({
  selector: 'app-nuevo-usuario',
  templateUrl: './nuevo-usuario.component.html',
  styleUrls: ['./nuevo-usuario.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class NuevoUsuarioComponent implements OnInit {

  loadingEmp!: boolean;
  isLoading!: boolean;
  tipoDocumento!: boolean;
  coEmpl!: string;
  maxLength: number = 12;
  btnGrabar: boolean = true;
  today: Date = new Date();

  usuario!: FormGroup;

  empleado!: IEmpleado;
  $tipoDocumentos!: Observable< any >;

  constructor(
    private fb: FormBuilder,
    private funciones: Funciones,
    private ref: DynamicDialogRef,
    private dialogService: DialogService,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private generalService: GeneralService,
    private utilService: UtilService,
    private validateService: ValidatorsService,
    private usuarioService: UsuariosService
  ) { }

  ngOnInit(): void {
    const tipoDocuItem: IResponse2 = { cdescri: '(SELECCIONAR)', ccodigoHijo: '*' };
    this.$tipoDocumentos = this.generalService.getCbo_TipoDocumentoIdentidad().pipe(
      map( td => {
        td.unshift( tipoDocuItem );
        return td;
      })
    );

    this.usuario = this.fb.group({
      tipoDocu: ['', []],
      nroDocu: ['', [ Validators.required, Validators.minLength(8) ]],
      coUsua: ['', [ Validators.required ]],
      coUsuaDomi: ['', [ Validators.required ]],
      nomUsua: ['', [ Validators.required ]],
      apePate: ['', [ Validators.required ]],
      apeMate: ['', []],
      coEmpl: ['', []],
      fecAlta: [this.today, [ Validators.required ]],
      fecBaja: ['', []],
      email: ['', [ Validators.required, Validators.pattern( this.funciones.emailPattern ) ]]
    });

  }

  campoEsValido( campo: string ) {
    return this.usuario.controls[campo].errors && 
              this.usuario.controls[campo].touched
  }

  registrarEmpleado() {
    document.getElementById('btnNuevo').blur();
    
    const dialog = this.dialogService.open( NuevoEmpleadoComponent, {
      header: 'Empleado - Nuevo',
      width: '50%',
      closable: false
    });

    dialog.onClose.subscribe( ( data: IEmpleado | '' ) => {
      console.log('IEmp', data );
      if ( data ) {
        this.messageService.add({
          severity: 'success',
          summary: 'Su registro se ha grabado correctamente'
        });

        this.usuario.get('tipoDocu')?.setValue( data.tiDocuIden );
        this.usuario.get('nroDocu')?.setValue( data.nuDocuIden );
        this.usuario.get('nomUsua')?.setValue( data.noEmpl );
        this.usuario.get('apePate')?.setValue( data.apPateEmpl );
        this.usuario.get('apeMate')?.setValue( data.apMateEmpl );
        this.coEmpl = data.coCondEmpl;
      }
      else if ( data == '' ) {
        return;
      }
      else {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al crear su registro'
        });
      }
    });
  }

  getEmpleadoDisable(): boolean {
    return !this.usuario.get('nroDocu')?.value;
  }

  selectTipoDocu() {
    this.tipoDocumento = false;
  }

  buscarEmpleadoPorDocumento() {
    this.loadingEmp = true;

    const tipoDocu = this.usuario.get('tipoDocu')?.value;
    if ( tipoDocu == '*' ) {
      this.tipoDocumento = true;
      this.loadingEmp = false;
      return;
    }

    const nroDocu = this.usuario.get('nroDocu')?.value

    this.usuarioService.getEmpleadoByDocumento(tipoDocu, nroDocu).subscribe({
      next: ( data ) => {
        console.log('empleadoDoc', data )
        this.empleado = data;

        this.usuario.get('nomUsua')?.setValue( this.empleado.noEmpl );
        this.usuario.get('apePate')?.setValue( this.empleado.apPateEmpl );
        this.usuario.get('apeMate')?.setValue( this.empleado.apMateEmpl );
      },
      error: ( e ) => {
        console.log( e );
        this.loadingEmp = false;

        this.usuario.get('nomUsua')?.setValue('');
        this.usuario.get('apePate')?.setValue('');
        this.usuario.get('apeMate')?.setValue('');

        this.messageService.add({
          severity: 'error',
          summary: 'No se encontró el empleado',
          detail: 'Por favor, regístrelo como nuevo o busque con un documento de identidad diferente.',
          sticky: true
        });
      },
      complete: () => {
        this.loadingEmp = false;
      }
    });
  }

  buscarUsuarioAD() {
    const coUsuaDomi = this.usuario.get('coUsuaDomi')?.value;

    if ( coUsuaDomi == '' ) {
      this.usuario.get('coUsuaDomi')?.markAsTouched();

      this.messageService.add({
        severity: 'warn',
        summary: 'Debe ingresar un Usuario de Red para validarlo',
        sticky: true
      });

      return;
    }

    this.usuarioService.getUsuarioAD( coUsuaDomi ).subscribe({
      next: ( data ) => {
        if ( data.codResult === 1 ) {
          this.messageService.add({ severity: 'success', summary: 'Usuario de Red si existe en el sistema', sticky: true });
          this.btnGrabar = false;
        }
        else {
          this.messageService.add({ severity: 'error', summary: 'Usuario de Red no existe en el sistema', sticky: true });
          this.btnGrabar = true;
        }

      },
      error: ( _err ) => {
        this.utilService.onShowMessageErrorSystem();
      }
    });
  }

  grabarUsuario() {
    this.messageService.clear();
    
    if ( this.usuario.invalid ) {
      this.messageService.add({ severity: 'warn', summary: 'Por favor, complete todos los campos obligatorios', sticky: true });
      this.usuario.markAllAsTouched();
      return;
    }

    const fechaBaja = this.usuario.get('fecBaja')?.value;

    const data = {
      idUsua: this.usuario.get('coUsua')?.value,
      coEmpl: !this.empleado ? this.coEmpl : this.empleado.coEmpl,
      correoUsua: this.usuario.get('email')?.value,
      coUsuaDomi: this.usuario.get('coUsuaDomi')?.value,
      feIngr: this.validateService.formatDate( this.usuario.get('fecAlta')?.value ),
      feBaja: (fechaBaja !== "" && fechaBaja !== null) ? this.validateService.formatDate( fechaBaja ) : ''
    }
    console.log('dataUsuario', data)

    this.confirmService.confirm({
      message: `¿Está Ud. seguro de registrar el siguiente usuario: ${ data.idUsua }?`,
      accept: () => {
        this.isLoading = true;

        this.usuarioService.createOrUpdateUsuario( 0, data ).subscribe({
          next: ( d ) => {
            console.log( d );

            if ( d.codResult < 0 ) {
              this.isLoading = false;
              this.messageService.add({ severity: 'warn', summary: 'Atención', detail: d.msgResult, sticky: true });
              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
