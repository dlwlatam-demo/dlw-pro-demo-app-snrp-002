import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BandejaUsuarioComponent } from './bandeja-usuario/bandeja-usuario.component';
import { AsignarLocalesComponent } from './asignar-locales/asignar-locales.component';
import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

const routes: Routes = [
  { path: 'bandeja', component: BandejaUsuarioComponent, canActivate: [ AuthGaurdService ] },
  { path: 'asignar_local/bandeja', component: AsignarLocalesComponent, canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
