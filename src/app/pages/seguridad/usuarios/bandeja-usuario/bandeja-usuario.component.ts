import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, map, of } from 'rxjs';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService, SortEvent } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';

import { Estado } from '../../../../interfaces/combos.interface';
import { IUsuario } from '../../../../interfaces/seguridad/usuario.interface';

import { GeneralService } from '../../../../services/general.service';
import { PerfilesService } from '../../../../services/seguridad/perfiles.service';
import { UsuariosService } from '../../../../services/seguridad/usuarios.service';

import { AsignarPerfilesComponent } from '../asignar-perfiles/asignar-perfiles.component';
import { NuevoUsuarioComponent } from '../nuevo-usuario/nuevo-usuario.component';
import { EditarUsuarioComponent } from '../editar-usuario/editar-usuario.component';

import { BaseBandeja } from '../../../../base/base-bandeja.abstract';

import { environment } from '../../../../../environments/environment';
import { MenuOpciones } from '../../../../models/auth/Menu.model';

@Component({
  selector: 'app-bandeja-usuario',
  templateUrl: './bandeja-usuario.component.html',
  styleUrls: ['./bandeja-usuario.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class BandejaUsuarioComponent extends BaseBandeja implements OnInit {

  disableEdit!: boolean;
  registros: string[] = [];

  searchBox!: FormGroup;

  $zonas!: Observable<any>;
  $oficinas: Observable<any> = of([{ coOficRegi: '*', deOficRegi: '(TODOS)' }]);
  $local: Observable<any> = of([{ coLocaAten  : '*', deLocaAten: '(TODOS)' }]);
  $perfiles!: Observable<any>;
  $estado!: Estado[];

  enviadosList!: IUsuario[];

  currentPage: string = `${ environment.currentPage }`;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private confirmService: ConfirmationService,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private perfilService: PerfilesService,
    private usuarioService: UsuariosService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.USUARIOS;
    this.btnUsuarios();
    
    this.initCombos();

    this.searchBox = this.fb.group({
      zonas: ['', [ Validators.required ]],
      oficinas: [{value: '', disabled: true}, [ Validators.required ]],
      locales: [{value: '', disabled: true}, [ Validators.required ]],
      perfiles: ['', [ Validators.required ]],
      usuario: ['', []],
      dni: ['', []],
      nomUsua: ['', []],
      apeUsua: ['', []],
      estado: ['A', [ Validators.required ]],
    });

    this.buscaBandeja();
  }

  initCombos() {
    const zonaItem: any = { coZonaRegi: '*', deZonaRegi: '(TODOS)' };
    const perfilItem: any = { coPerf: '*', noPerf: '(TODOS)' };
    const estadoItem: Estado = { coEstado: '*', deEstado: '(TODOS)' };

    this.$zonas = this.generalService.getCbo_Zonas_Usuario('').pipe(
      map( z => { 
        z.unshift( zonaItem ); 
        return z;
      } 
    ));

    this.$perfiles = this.perfilService.getPerfilesBandeja('', 'A').pipe(
      map( p => {
        p.unshift( perfilItem );
        return p;
      })
    );

    this.$estado = this.generalService.getCbo_Estado()
    this.$estado.unshift( estadoItem );
  }

  buscarOficinaPorZona( event: any ) {
    const oficinaItem: any = { coOficRegi: '*', deOficRegi: '(TODOS)' };

    if ( event.value == '*' ) {
      this.$oficinas = of([ oficinaItem ]);
      this.searchBox.get('oficinas')?.disable();
    }
    else
      this.$oficinas = this.generalService.getCbo_Oficinas_Zonas('', event.value).pipe(
        map( o => {
          o.unshift( oficinaItem );
          this.searchBox.get('oficinas')?.enable();
          return o;
        })
      );
  }

  buscarLocalPorOficina( event: any ) {
    const localItem: any = { coLocaAten  : '*', deLocaAten: '(TODOS)' };
    const coZonaRegi = this.searchBox.get('zonas')?.value;

    if ( event.value == '*' ) {
      this.$local = of([ localItem ]);
      this.searchBox.get('locales')?.disable();
    }
    else
      this.$local = this.generalService.getCbo_Locales_Ofic('', coZonaRegi, event.value).pipe(
        map( l => {
          l.unshift( localItem );
          this.searchBox.get('locales')?.enable();
          return l;
        })
      );
  }

  buscaBandeja() {
    let zona = this.searchBox.get('zonas')?.value
    zona = zona == '*' ? '' : zona;

    let oficina = this.searchBox.get('oficinas')?.value
    oficina = oficina == '*' ? '' : oficina;

    let local = this.searchBox.get('locales')?.value
    local = local == '*' ? '' : local;

    let perfil = this.searchBox.get('perfiles')?.value
    if ( perfil == '' || perfil == '*' ) {
      perfil = '0'
    }

    const usuario = this.searchBox.get('usuario')?.value
    const dni = this.searchBox.get('dni')?.value
    const nomUsua = this.searchBox.get('nomUsua')?.value
    const apeUsua = this.searchBox.get('apeUsua')?.value
    
    let estado = this.searchBox.get('estado')?.value
    if ( estado == '' ) {
      estado = 'A';
    } else if ( estado == '*' ) {
      estado = '';
    }

    const pathBandeja = `${ zona }/${ oficina }/${ local }/${ perfil }/${ usuario }/${ dni }/${ nomUsua }/${ apeUsua }/${ estado }`;
    this.buscarBandejaUsuario( pathBandeja );
  }

  buscarBandejaUsuario( pathBandeja: string ) {
    Swal.showLoading();
    this.usuarioService.getUsuariosBandeja( pathBandeja ).subscribe({
      next: ( data ) => {
        
        for ( let d of data ) {
          if ( d.inEstd == 'A' )
            d.inEstd = 'Activo';
          else
            d.inEstd = 'Inactivo';
        }

        this.enviadosList = data;
      },
      error: () => {
        Swal.close()
        Swal.fire(
          'No se encuentran registros con los datos ingresados',
          '',
          'info'
        );

        this.enviadosList = [];
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  nuevoUsuario() {
    const dialog = this.dialogService.open( NuevoUsuarioComponent, {
      header: 'Nuevo Usuario',
      width: '70%',
      closable: false
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({
          severity: 'success',
          summary: 'Su registro se ha grabado correctamente'
        });

        this.buscaBandeja();
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al crear su registro'
        });
      }
    });
  }

  editOrView( value: string ) {
    if ( !this.validateSelect( value ) )
      return;

    if ( value == 'edit' && this.selectedValues[0].inEstd == 'Inactivo' ) {
      Swal.fire('No puede editar un registro en estado INACTIVO.', '', 'warning');
      return;
    }

    const dataDialog = {
      vista: value == 'edit' ? false : true,
      usuario: this.selectedValues
    }

    const dialog = this.dialogService.open( EditarUsuarioComponent, {
      header: value == 'edit' ? 'Editar Usuario' : 'Consultar Usuario',
      width: '70%',
      closable: false,
      data: dataDialog
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha actualizado correctamente'
        });

        this.buscaBandeja();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al actualizar su registro'
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  anularUsuario() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inEstd == 'Inactivo' ) {
        Swal.fire('No puede anular un registro en estado INACTIVO.', '', 'warning');
        return;
      }

      this.registros.push( registro.coUsua );
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro que desea anular el registro?',
      accept: () => {
        Swal.showLoading();
        this.loadingAnu = true;

        const data = {
          trama: this.registros
        }

        this.usuarioService.anularUsuarios( data ).subscribe({
          next: ( d ) => {

            if ( d.codResult < 0 ) {
              this.messageService.add({
                severity: 'warn',
                summary: 'Atención',
                detail: d.msgResult,
                sticky: true
              });

              return;
            }

            this.messageService.add({
              severity: 'success',
              summary: 'Se anularon los registros correctamente',
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al anular los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAnu = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscaBandeja();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  activarUsuario() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inEstd == 'Activo' ) {
        Swal.fire('No puede anular un registro ya inactivo', '', 'warning');
        return;
      }

      this.registros.push( registro.coUsua );
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro que desea activar los registros?',
      accept: () => {
        Swal.showLoading();
        this.loadingAct = true;

        const data = {
          trama: this.registros
        }

        this.usuarioService.activarUsuarios( data ).subscribe({
          next: ( d ) => {

            if ( d.codResult < 0 ) {
              this.messageService.add({
                severity: 'warn',
                summary: 'Atención',
                detail: d.msgResult,
                sticky: true
              });

              return;
            }

            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se activaron los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al activar los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAct = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscaBandeja();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  asignarPerfil( value: string ) {
    if ( !this.validateSelect( value ) )
      return;
    
    const dialog = this.dialogService.open( AsignarPerfilesComponent, {
      header: 'Usuario - Asignar Perfiles',
      width: '40%',
      closable: false,
      data: {
        user: this.selectedValues
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({
          severity: 'success',
          summary: 'Se grabaron correctamente los perfiles seleccionados'
        });

        this.buscaBandeja();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al grabar la información'
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  asignarLocal( value: string ) {
    if ( !this.validateSelect( value ) )
      return;
    
    sessionStorage.setItem('user_sarf', JSON.stringify( this.selectedValues ));
    this.router.navigate(['/SARF/seguridad/usuarios/asignar_local/bandeja']);
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
