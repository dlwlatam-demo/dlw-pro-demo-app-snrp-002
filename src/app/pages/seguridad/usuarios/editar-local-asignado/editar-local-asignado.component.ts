import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IUsuario, IUsuarioLocal } from '../../../../interfaces/seguridad/usuario.interface';
import { Observable, of, map } from 'rxjs';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ConfirmationService, SortEvent } from 'primeng/api';
import { GeneralService } from '../../../../services/general.service';
import { UsuariosService } from '../../../../services/seguridad/usuarios.service';
import Swal from 'sweetalert2';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-editar-local-asignado',
  templateUrl: './editar-local-asignado.component.html',
  styleUrls: ['./editar-local-asignado.component.scss']
})
export class EditarLocalAsignadoComponent implements OnInit {

  isLoading!: boolean;
  registros!: any[];

  form!: FormGroup;

  user!: IUsuario[];
  userLocal!: IUsuarioLocal[];

  listLocal!: IUsuarioLocal[];

  localDefault: IUsuarioLocal[] = [];
  localSelected: IUsuarioLocal[] = [];
  localSelectedFiltro: IUsuarioLocal[] = [];

  $zonas!: Observable<any>;
  $oficinas: Observable<any> = of([{ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' }]);

  currentPage: string = `${ environment.currentPage }`;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private generalService: GeneralService,
    private usuarioService: UsuariosService
  ) { }

  ngOnInit(): void {
    this.user = this.config.data.usuario as IUsuario[];
    this.userLocal = this.config.data.local as IUsuarioLocal[];

    const zonaItem: any = { coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' };

    this.$zonas = this.generalService.getCbo_Zonas_Usuario('').pipe(
      map( z => {
        z.unshift( zonaItem );
        return z;
      })
    );

    this.form = this.fb.group({
      coUsua: [{value: '', disabled: true}, [ Validators.required ]],
      nomUsua: [{value: '', disabled: true}, [ Validators.required ]],
      zonas: ['', [ Validators.required ]],
      oficinas: [{value: '', disabled: true}, [ Validators.required ]]
    });

    this.hidrate();
  }

  hidrate() {
    const oficinaItem: any = { coOficRegi: '*', deOficRegi: '(SELECCIONAR)' };
    const nomUsua: string = `${ this.user['0'].noEmpl } ${ this.user['0'].apPateEmpl } ${ this.user['0'].apMateEmpl }`;

    this.form.get('coUsua')?.setValue( this.user['0'].idUsua );
    this.form.get('nomUsua')?.setValue( nomUsua );
    this.form.get('zonas')?.setValue( this.userLocal[0].coZonaRegi );

    this.$oficinas = this.generalService.getCbo_Oficinas_Zonas('', this.userLocal[0].coZonaRegi).pipe(
      map( o => {
        o.unshift( oficinaItem );
        this.form.get('oficinas')?.enable();
        return o;
      })
    );

    this.form.get('oficinas')?.setValue( this.userLocal[0].coOficRegi );

    this.listaDeLocales( this.userLocal[0].coZonaRegi, this.userLocal[0].coOficRegi );
  }

  buscarOficinaPorZona( event: any ) {
    this.listLocal = [];
    const oficinaItem: any = { coOficRegi: '*', deOficRegi: '(SELECCIONAR)' }

    this.$oficinas = this.generalService.getCbo_Oficinas_Zonas('', event.value).pipe(
      map( o => {
        o.unshift( oficinaItem );
        this.form.get('oficinas')?.enable();
        return o;
      })
    );
  }

  bandejaLocalesPorOficina( event: any ) {
    const coZonaRegi = this.form.get('zonas')?.value;

    this.listaDeLocales( coZonaRegi, event.value );
  }

  listaDeLocales( zona: string, oficina: string ) {
    Swal.showLoading();
    this.usuarioService.getUsuarioPorLocales(this.user[0].coUsua, zona, oficina).subscribe({
      next: ( data ) => {
        console.log( data );
        this.listLocal = data;

        this.localDefault = this.listLocal.filter( l => l.inCheckDefa == '1');
        this.localSelected = this.listLocal.filter(l => l.inCheckSele == '1');
      },
      error: ( e ) => {
        console.log( e );

        Swal.close();
        Swal.fire('No se encontraron Locales con los datos seleccionados', '', 'info');
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  selectLocalDefault( event: any ) {
    this.localSelectedFiltro = [...this.localSelected];
    
    if ( event.checked.length > 1 ) {
      event.checked.shift();
    }

    if ( this.localSelectedFiltro.includes( event.checked[0] ) ) {
      const index: number = this.localSelectedFiltro.findIndex( local => local == event.checked[0] );
      this.localSelectedFiltro.splice( index, 1 );
      this.localSelectedFiltro.unshift( event.checked[0] )
    }
    else {
      this.localSelectedFiltro.unshift( event.checked[0] );
    }

    this.localSelected = this.localSelectedFiltro;
  }

  deselectLocalDefault( l: IUsuarioLocal ) {
    if ( this.localDefault.includes( l ) ) {
      this.localDefault = [];
    }
  }

  grabarLocales() {

    if ( this.localSelected.includes( this.localDefault[0] ) ) {
      const index: number = this.localSelected.findIndex( l => l == this.localDefault[0] );
      this.localSelected.splice( index, 1 );
    }

    this.registros = this.localDefault.map( d => { return { coLoca: d.coLocaAten, inDefa: '1' } }).concat(
      this.localSelected.map( s => { return { coLoca: s.coLocaAten, inDefa: '0' } })
    );

    const data = {
      coUsua: this.user['0'].coUsua,
      coZonaRegi: this.form.get('zonas')?.value,
      coOficRegi: this.form.get('oficinas')?.value,
      trama: this.registros
    }
    console.log('dataEdit', data);
    
    this.confirmService.confirm({
      message: `¿Está Ud. seguro de registrar los locales al siguiente usuario: ${ this.user['0'].idUsua }?`,
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        this.usuarioService.updateUsuarioPorLocal( data ).subscribe({
          next: ( d ) => {
            console.log( d );

            if ( d.codResult < 0 ) {
              Swal.close();
              Swal.fire(`${ d.msgResult }`, '', 'error');
              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            Swal.close();
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
