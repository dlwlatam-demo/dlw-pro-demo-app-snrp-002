import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';

import { BandejaUsuarioComponent } from './bandeja-usuario/bandeja-usuario.component';
import { NuevoUsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';
import { NuevoEmpleadoComponent } from './nuevo-empleado/nuevo-empleado.component';
import { AsignarPerfilesComponent } from './asignar-perfiles/asignar-perfiles.component';
import { AsignarLocalesComponent } from './asignar-locales/asignar-locales.component';
import { AsignarNuevoLocalComponent } from './asignar-nuevo-local/asignar-nuevo-local.component';
import { EditarUsuarioComponent } from './editar-usuario/editar-usuario.component';
import { EditarLocalAsignadoComponent } from './editar-local-asignado/editar-local-asignado.component';


@NgModule({
  declarations: [
    BandejaUsuarioComponent,
    NuevoUsuarioComponent,
    NuevoEmpleadoComponent,
    AsignarPerfilesComponent,
    AsignarLocalesComponent,
    AsignarNuevoLocalComponent,
    EditarUsuarioComponent,
    EditarLocalAsignadoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UsuariosRoutingModule,
    NgPrimeModule
  ]
})
export class UsuariosModule { }
