import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, ConfirmationService, MessageService } from 'primeng/api';

import { IUsuario, IUsuarioLocal } from '../../../../interfaces/seguridad/usuario.interface';

import { UsuariosService } from '../../../../services/seguridad/usuarios.service';

import { environment } from '../../../../../environments/environment';
import { AsignarNuevoLocalComponent } from '../asignar-nuevo-local/asignar-nuevo-local.component';
import { EditarLocalAsignadoComponent } from '../editar-local-asignado/editar-local-asignado.component';
import { UtilService } from '../../../../services/util.service';

@Component({
  selector: 'app-asignar-locales',
  templateUrl: './asignar-locales.component.html',
  styleUrls: ['./asignar-locales.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class AsignarLocalesComponent implements OnInit {

  form!: FormGroup;

  user!: IUsuario[];
  userLocalList!: IUsuarioLocal[];
  selectLocal: IUsuarioLocal[] = [];

  currentPage: string = `${ environment.currentPage }`

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private utilService: UtilService,
    private dialogService: DialogService,
    private messageService: MessageService,
    private usuarioService: UsuariosService
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse( sessionStorage.getItem('user_sarf') );
    if ( this.user == null ) { this.regresar(); }

    this.form = this.fb.group({
      usuario: [{value: '', disabled: true}, [ Validators.required ]],
      nomUsua: [{value: '', disabled: true}, [ Validators.required ]]
    });

    this.hidrateForm();
    this.bandejaLocalesPorUsuario();
  }

  get isDisabled() {
    return this.user['0'].inEstd == 'Inactivo';
  }

  hidrateForm() {
    for ( let u of this.user ) {
      const nomUsua: string = `${ u.noEmpl } ${ u.apPateEmpl } ${ u.apMateEmpl }`;

      this.form.get('usuario')?.setValue( u.idUsua );
      this.form.get('nomUsua')?.setValue( nomUsua );
    }
  }

  bandejaLocalesPorUsuario() {
    this.utilService.onShowProcessLoading('Cargando información');
    this.usuarioService.getBandejaUsuarioLocales( this.user['0'].coUsua! ).subscribe({
      next: ( data ) => {

        for ( let d of data ) {
          if ( d.inCheckDefa == '0' ) {
            d.inCheckDefa = 'No'
          }
          else {
            d.inCheckDefa = 'Si'
          }
        }

        this.userLocalList = data;
      },
      error: ( e ) => {
        console.log( e );

        this.utilService.onCloseLoading();
        this.utilService.onShowAlert('warning', 'Atención', 'No se encontraron registros para este usuario');

        this.userLocalList = [];
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  asignarNuevoLocal() {
    const dialog = this.dialogService.open( AsignarNuevoLocalComponent, {
      header: 'Asignar Nuevo Local',
      width: '50%',
      closable: false,
      data: {
        usuario: this.user
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({
          severity: 'success',
          summary: 'Se han asignado y grabados los locales correctamente'
        });

        this.bandejaLocalesPorUsuario();
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al grabar los locales seleccionados'
        });
      }
    });
  }

  editarLocalAsignado() {
    if ( this.selectLocal.length == 0 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro para editarlo');
      return;
    }

    if ( this.selectLocal.length > 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede editar más de un registro a la vez');
      this.selectLocal = [];
      return;
    }

    const dialog = this.dialogService.open( EditarLocalAsignadoComponent, {
      header: 'Editar Local Asignado',
      width: '50%',
      closable: false,
      data: {
        usuario: this.user,
        local: this.selectLocal
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({
          severity: 'success',
          summary: 'Se han asignado y grabados los locales correctamente'
        });

        this.bandejaLocalesPorUsuario();
        this.selectLocal = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al grabar los locales seleccionados'
        });

        this.selectLocal = [];
      }

      this.selectLocal = [];
    });
  }

  regresar() {
    sessionStorage.removeItem('user_sarf');
    this.router.navigate(['/SARF/seguridad/usuarios/bandeja']);
  }

  paginatorClean() {
    this.selectLocal = [];
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
