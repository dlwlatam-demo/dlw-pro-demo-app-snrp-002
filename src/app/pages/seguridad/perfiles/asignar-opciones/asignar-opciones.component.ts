import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { ConfirmationService, TreeNode } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { IPerfil, IPerfilOption } from '../../../../interfaces/seguridad/perfil.interface';

import { PerfilesService } from '../../../../services/seguridad/perfiles.service';

export interface Options {
  label: string,
  value: number,
  select: number,
  items?: IPerfilOption[]
}

@Component({
  selector: 'app-asignar-opciones',
  templateUrl: './asignar-opciones.component.html',
  styleUrls: ['./asignar-opciones.component.scss'],
  providers: [
    ConfirmationService
  ]
})
export class AsignarOpcionesComponent implements OnInit {

  isLoading!: boolean;
  registros: string[] = [];

  form!: FormGroup;

  perfil!: IPerfil[];

  options: TreeNode[] = [];
  selectedOptions: TreeNode[] = [];

  registrosN1: IPerfilOption[] = [];
  registrosN2: IPerfilOption[] = [];
  registrosN3: IPerfilOption[] = [];
  registrosN4: IPerfilOption[] = [];

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private perfilService: PerfilesService
  ) { }

  ngOnInit(): void {
    this.perfil = this.config.data.perfil as IPerfil[];
    const nombre = this.perfil['0'].noPerf!

    this.form = this.fb.group({
      noPerf: [{value: nombre, disabled: true}, [ Validators.required ]]
    });

    this.getOptionsSARF();
  }

  getOptionsSARF() {
    this.perfilService.getPerfilByOption( this.perfil['0'].coPerf! ).subscribe({
      next: ( options ) => {
    
        this.registrosN1 = options.filter( (o: any) => o.coPadr == 0 );

        this.registrosN2 = options.filter( (o: any) => this.registrosN1.find( (r: any) => r.coOpci == o.coPadr ));

        this.registrosN3 = options.filter( (o: any) => this.registrosN2.find( (r: any) => r.coOpci == o.coPadr && o.tiOpci != 'B' )).concat(
          options.filter( (o: any) => this.registrosN2.find( (r: any) => r.coOpci == o.coPadr && o.tiOpci == 'B' ))
        );

        this.registrosN4 = options.filter( (o: any) => this.registrosN3.find( (r: any) => r.coOpci == o.coPadr && o.tiOpci == 'B' ));

        this.setNivel2();
      }
    });
  }

  setNivel2() {
    for ( let n1 of this.registrosN1 ) {
      let file = {} as TreeNode;

      file.label = n1.deOpci;
      file.data = n1.coSel;
      file.key = String( n1.coOpci )
      file.expanded = false;
	  
      let children1: TreeNode[] = [];
      
      for ( let n2 of this.registrosN2 ) {
        let file2 = {} as TreeNode;

        if ( n2.coPadr == n1.coOpci ) {
          file2.label = n2.deOpci;
          file2.data = n2.coSel;
          file2.key = String( n2.coOpci )
          file2.expanded = false;
          file2.parent = file;
          
          let children2: TreeNode[] = [];
          
          for ( let n3 of this.registrosN3 ) {
            let file3 = {} as TreeNode;

            if ( n3.coPadr == n2.coOpci ) { // && n3.tiOpci != 'B'
              file3.label = n3.deOpci;
              file3.data = n3.coSel;
              file3.key = String( n3.coOpci );
              file3.expanded = false;
              file3.parent = file2;
            
              let children3: TreeNode[] = [];
              
              for ( let n4 of this.registrosN4 ) {
                let file4 = {} as TreeNode;

                if ( n4.coPadr == n3.coOpci && n4.tiOpci == 'B' ) {
                  file4.label = n4.deOpci;
                  file4.data = n4.coSel;
                  file4.key = String( n4.coOpci );
                  file4.children =  null;
                  file4.expanded = false;
                  file4.parent = file3;
                  children3.push(file4);
                }
              }
            
              file3.children =  children3;			  
              children2.push(file3);
            }
          }

          file2.children =  children2; 
          children1.push(file2);
        }
      }

      file.children =  children1; 
    
      this.options.push( file );
    }

    this.habilitarOpciones()
  }

  habilitarOpciones() {
    const registros2: TreeNode[] = [];

    // NIVEL 1
    for ( let option1 of this.options ) {
      registros2.push( option1 );

      // NIVEL 2
      for ( let option2 of option1.children ) {
        registros2.push( option2 );

        if ( option2.children.length == 0 ) {
          this.selectedOptions = registros2.filter( r2 => r2.data == 1 );
        }

        // NIVEL 3
        for ( let option3 of option2.children ) {
          registros2.push( option3 );

          if ( option3.children.length == 0 ) {
            this.selectedOptions = registros2.filter( r2 => r2.data == 1 );
          }
          
          // NIVEL 4
          for ( let option4 of option3.children ) {
            registros2.push( option4 );
            this.selectedOptions = registros2.filter( r2 => r2.data == 1 );
          }
        }
      }
    }
  }
  
  grabar() {

    for ( let so of this.selectedOptions ) {
      if ( !this.registros.includes( so.key ) ) {
        this.registros.push( so.key );

        if ( so.parent ) {
          if ( !this.registros.includes( so.parent.key ) ) {
            this.registros.push( so.parent.key );
          }
  
          if ( so.parent.parent ) {
            if ( !this.registros.includes( so.parent.parent.key ) ) {
              this.registros.push( so.parent.parent.key );
            }
  
            if ( so.parent.parent.parent ) {
              if ( !this.registros.includes( so.parent.parent.parent.key ) ) {
                this.registros.push( so.parent.parent.parent.key );
              }
            }
          }
        }
      }
    }
    
    const data = {
      coPerf: this.perfil[0].coPerf,
      trama: this.registros
    }
    
    this.confirmService.confirm({
      message: `¿Está Ud. seguro de asignar las siguientes opciones al perfil: ${ this.perfil[0].noPerf }?`,
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        this.perfilService.asignarOpcionesPerfil( data ).subscribe({
          next: ( d ) => {
            console.log( d );

            if ( d.codResult < 0 ) {
              Swal.close();
              Swal.fire(d.msgResult, '', 'error');
              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            Swal.close();
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
