import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { PerfilesRoutingModule } from './perfiles-routing.module';
import { MaterialModule } from '../../../material/material.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';

import { AsignarOpcionesComponent } from './asignar-opciones/asignar-opciones.component';
import { NuevoPerfilComponent } from './nuevo-perfil/nuevo-perfil.component';
import { BandejaPerfilComponent } from './bandeja-perfil/bandeja-perfil.component';
import { EditarPerfilComponent } from './editar-perfil/editar-perfil.component';
import { AsignarEstadosComponent } from './asignar-estados/asignar-estados.component';




@NgModule({
  declarations: [
    BandejaPerfilComponent,
    NuevoPerfilComponent,
    AsignarOpcionesComponent,
    EditarPerfilComponent,
    AsignarEstadosComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfilesRoutingModule,
    MaterialModule,
    NgPrimeModule
  ]
})
export class PerfilesModule { }
