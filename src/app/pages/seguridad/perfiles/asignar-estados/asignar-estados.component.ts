import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ConfirmationService, MessageService, TreeNode } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import { IPerfil, IDocumentoEstado } from '../../../../interfaces/perfiles.interface';

import { BodyAsignarEstados, ITramaCodigos } from '../../../../models/seguridad/perfil.model';

import { PerfilesService } from '../../../../services/seguridad/perfiles.service';

@Component({
  selector: 'app-asignar-estados',
  templateUrl: './asignar-estados.component.html',
  styleUrls: ['./asignar-estados.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class AsignarEstadosComponent implements OnInit {

  isLoading!: boolean;

  form!: FormGroup;

  perfil!: IPerfil[];

  estados: TreeNode[] = [];
  selectedEstados: TreeNode[] = [];

  registrosN1: IDocumentoEstado[] = [];
  registrosN2: IDocumentoEstado[] = [];


  bodyAsignarEstados!: BodyAsignarEstados;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private perfilService: PerfilesService,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
  ) { }

  ngOnInit(): void {
    this.bodyAsignarEstados = new BodyAsignarEstados();

    this.perfil = this.config.data.perfil as IPerfil[];
    const nombre = this.perfil[0].noPerf;

    this.form = this.fb.group({
      noPerf: [nombre, [ Validators.required ]]
    });

    this.getEstadosDocumento();
  }

  getEstadosDocumento() {
    const coUsuaLogi = localStorage.getItem('perfil_sarf');
    const coPerf = this.perfil[0].coPerf;

    this.perfilService.getEstadosPorPerfil( coPerf, Number(coUsuaLogi) ).subscribe({
      next: ( estados ) => {

        this.registrosN1 = estados.filter( ( e ) => e.ccodigoHijo == '000' );

        this.registrosN2 = estados.filter( ( e ) => e.ccodigoHijo != '000' );

        this.setNiveles();
      }
    });
  }

  setNiveles() {
    for ( let n1 of this.registrosN1 ) {
      let file = {} as TreeNode;

      file.label = n1.cdescri;
      file.data = n1.coSel;
      file.key = `${n1.ccodigoMaster}${n1.ccodigoHijo}`;
      file.type = n1.ccodigoMaster;
      file.expanded = false;

      let children1: TreeNode[] = [];

      for ( let n2 of this.registrosN2 ) {
        let file2 = {} as TreeNode;

        if ( n2.ccodigoMaster == n1.ccodigoMaster ) {
          file2.label = n2.cdescri;
          file2.data = n2.coSel;
          file2.key = `${n2.ccodigoMaster}${n2.ccodigoHijo}`;
          file2.type = n2.ccodigoMaster;
          file2.children = null;
          file2.expanded = false;
          file2.parent = file;

          children1.push( file2 );
        }
      }

      file.children = children1;

      this.estados.push( file );
    }

    this.habilitarOpciones();
  }

  habilitarOpciones() {
    const registros: TreeNode[] = [];

    // NIVEL 1
    for ( let option1 of this.estados ) {
      registros.push( option1 );

      // NIVEL 2
      for ( let option2 of option1.children ) {
        registros.push( option2 );

        if ( option2.children == null ) {
          this.selectedEstados = registros.filter( r => r.data == '1' )
        }
      }
    }
  }

  grabar() {

    const codigoRegistros: ITramaCodigos[] = [];

    for ( let se of this.selectedEstados ) {
      const coMaster = se.key.slice(0, 3);
      const coHijo = se.key.slice(3);

      if ( coHijo != '000' ) {
        codigoRegistros.push({
          ccodigoMaster: coMaster,
          ccodigoHijo: coHijo
        });
      }
    }

    this.bodyAsignarEstados.coPerf = this.perfil[0].coPerf;
    this.bodyAsignarEstados.tramaEstDocumentos = codigoRegistros;

    console.log('bodyAsignarEstados', this.bodyAsignarEstados);

    this.confirmService.confirm({
      message: `¿Está Ud. seguro de asignar los siguientes estados del documento al perfil: ${ this.perfil[0].noPerf }?`,
      accept: () => {
        this.isLoading = true;

        this.perfilService.asignarEstadosPerfil( this.bodyAsignarEstados ).subscribe({
          next: ( d ) => {

            if ( d.codResult < 0 ) {
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención!', 
                detail: d.msgResult,
                sticky: true
              });

              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
