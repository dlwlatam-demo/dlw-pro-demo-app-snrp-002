import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { BandejaPerfilComponent } from './bandeja-perfil/bandeja-perfil.component';

const routes: Routes = [
  { path: 'bandeja', component: BandejaPerfilComponent, canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfilesRoutingModule { }
