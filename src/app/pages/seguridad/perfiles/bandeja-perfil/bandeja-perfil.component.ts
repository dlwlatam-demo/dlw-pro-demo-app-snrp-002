import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService, SortEvent } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';

import { Estado } from '../../../../interfaces/combos.interface';
import { IPerfil } from '../../../../interfaces/seguridad/perfil.interface';

import { MenuOpciones } from '../../../../models/auth/Menu.model';

import { GeneralService } from '../../../../services/general.service';
import { PerfilesService } from '../../../../services/seguridad/perfiles.service';
import { UtilService } from '../../../../services/util.service';

import { AsignarOpcionesComponent } from '../asignar-opciones/asignar-opciones.component';
import { EditarPerfilComponent } from '../editar-perfil/editar-perfil.component';
import { NuevoPerfilComponent } from '../nuevo-perfil/nuevo-perfil.component';

import { BaseBandeja } from '../../../../base/base-bandeja.abstract';

import { environment } from '../../../../../environments/environment';
import { AsignarEstadosComponent } from '../asignar-estados/asignar-estados.component';

@Component({
  selector: 'app-bandeja-perfil',
  templateUrl: './bandeja-perfil.component.html',
  styleUrls: ['./bandeja-perfil.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class BandejaPerfilComponent extends BaseBandeja implements OnInit {

  disableEdit!: boolean;
  registros: string[] = [];

  loadingPerfiles: boolean = false;

  form!: FormGroup;

  $estado!: Estado[];
  enviadosList!: IPerfil[];

  currentPage: string = environment.currentPage;

  constructor(
    private fb: FormBuilder,
    private confirmService: ConfirmationService,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private utilService: UtilService,
    private perfilService: PerfilesService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.PERFILES;
    this.btnPerfiles();
    
    let estadoItem: Estado = { deEstado: '(TODOS)', coEstado: '*' }

    this.$estado = this.generalService.getCbo_Estado();
    this.$estado.unshift( estadoItem );

    this.form = this.fb.group({
      noPerf: ['', []],
      estado: ['A', [ Validators.required ]]
    });

    this.buscaBandeja();
  }

  buscaBandeja() {
    let noPerf = this.form.get('noPerf')?.value;

    let estado = this.form.get('estado')?.value;
    if ( estado == '' ) {
      estado = 'A';
    } else if ( estado == '*' ) {
      estado = '';
    }

    this.buscarBandeja( noPerf, estado );
  }

  buscarBandeja( noPerf: string, estado: string ) {
    Swal.showLoading();
    this.loadingPerfiles = true;
    this.perfilService.getPerfilesBandeja( noPerf, estado ).subscribe({
      next: ( data ) => {
        this.enviadosList = data;
      },
      error: () => {
        Swal.close()
        Swal.fire(
          'No se encuentran registros con los datos ingresados',
          '',
          'info'
        );

        this.enviadosList = [];
        this.loadingPerfiles = false;
      },
      complete: () => {
        Swal.close();
        this.loadingPerfiles = false;
      }
    });
  }

  nuevoPerfil() {
    const dialog = this.dialogService.open( NuevoPerfilComponent, {
      header: 'Registrar Nuevo Perfil',
      width: '40%',
      closable: false
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({
          severity: 'success',
          summary: 'Su registro se ha grabado correctamente'
        });

        this.buscaBandeja();
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al crear su registro'
        });
      }
    });
  }

  editOrView( value: string ) {
    if ( !this.validateSelect( value ) )
      return;

    if ( value == 'edit' && this.selectedValues[0].deEstd == 'Inactivo' ) {
      Swal.fire('No puede editar un registro en estado INACTIVO.', '', 'warning');
      return;
    }

    const dataDialog = {
      vista: value == 'edit' ? false : true,
      perfil: this.selectedValues
    }

    const dialog = this.dialogService.open( EditarPerfilComponent, {
      header: value == 'edit' ? 'Editar Perfil' : 'Consultar Perfil',
      width: '40%',
      closable: false,
      data: dataDialog
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha actualizado correctamente'
        });

        this.buscaBandeja();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al actualizar su registro'
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  asignarEstadosDocumento() {
    if ( this.selectedValues.length === 0 ) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe seleccionar un registro');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", 'No puede seleccionar más de un registro a la vez');
      return;
    }

    if ( this.selectedValues[0].inEstd === 'I' ) {
      this.utilService.onShowAlert("warning", "Atención", 'No puede seleccionar un registro con estado INACTIVO');
      return;
    }

    const dialog = this.dialogService.open( AsignarEstadosComponent, {
      header: 'Asignar Estados de Documentos',
      width: '40%',
      closable: false,
      data: {
        perfil: this.selectedValues
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({
          severity: 'success',
          summary: 'Se asignaron los estados al perfil correctamente'
        });

        this.buscaBandeja();
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al registrar los estados seleccionadas'
        });
      }
    });
  }

  asignarOpciones( value: string ) {
    if ( !this.validateSelect( value ) ) {
      return;
    }

    const dataDialog = {
      perfil: this.selectedValues
    }

    const dialog = this.dialogService.open( AsignarOpcionesComponent, {
      header: 'Asignar Opciones',
      width: '40%',
      closable: false,
      data: dataDialog
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({
          severity: 'success',
          summary: 'Se asignaron las opciones al perfil correctamente'
        });

        this.buscaBandeja();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al registrar las opciones seleccionadas'
        });
      }
    });
  }

  anularPerfiles() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.deEstd == 'Inactivo' ) {
        Swal.fire('No puede anular un registro en estado INACTIVO.', '', 'warning');
        return;
      }

      this.registros.push( String( registro.coPerf ) );
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro que desea anular el registro?',
      accept: () => {
        Swal.showLoading();
        this.loadingAnu = true;

        const data = {
          trama: this.registros
        }

        this.perfilService.anularPerfiles( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({
              severity: 'success',
              summary: 'Se anularon los registros correctamente',
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al anular los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAnu = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscaBandeja();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  activarPerfiles() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.deEstd == 'Activo' ) {
        Swal.fire('No puede habilitar un registro ya activo', '', 'warning');
        return;
      }

      this.registros.push( String( registro.coPerf ) );
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro que desea activar los registros?',
      accept: () => {
        Swal.showLoading();
        this.loadingAct = true;

        const data = {
          trama: this.registros
        }

        this.perfilService.activarPerfiles( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se activaron los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al activar los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAct = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscaBandeja();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
