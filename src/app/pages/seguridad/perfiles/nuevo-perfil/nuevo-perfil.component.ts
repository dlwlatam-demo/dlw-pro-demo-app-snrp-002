import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { PerfilesService } from '../../../../services/seguridad/perfiles.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-nuevo-perfil',
  templateUrl: './nuevo-perfil.component.html',
  styleUrls: ['./nuevo-perfil.component.scss'],
  providers: [
    ConfirmationService
  ]
})
export class NuevoPerfilComponent implements OnInit {

  isLoading!: boolean;

  nuevoPerfil!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private confirmService: ConfirmationService,
    private perfilService: PerfilesService
  ) { }

  ngOnInit(): void {
    this.nuevoPerfil = this.fb.group({
      noPerf: ['', [ Validators.required ]],
      coPerf: [{value: '', disabled: true}, []]
    });
  }

  campoEsValido( campo: string ) {
    return this.nuevoPerfil.controls[campo].errors && 
              this.nuevoPerfil.controls[campo].touched
  }

  grabar() {
    if ( this.nuevoPerfil.invalid ) {
      Swal.fire('Por favor llenar los campos requeridos', '', 'warning');
      this.nuevoPerfil.markAllAsTouched();
      return;
    }

    const data = {
      noPerf: this.nuevoPerfil.get('noPerf')?.value,
      inTipoPerf: 'U',
      coSist: 20
    }
    console.log('newData', data);

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de crear el registro?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        this.perfilService.createOrUpdatePerfil( 0, data ).subscribe({
          next: ( d ) => {
            console.log( d );

            if ( d.codResult < 0 ) {
              Swal.close();
              Swal.fire(d.msgResult, '', 'error');
              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            Swal.close();
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
