import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { ConfirmationService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { IPerfil } from '../../../../interfaces/seguridad/perfil.interface';

import { PerfilesService } from '../../../../services/seguridad/perfiles.service';

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.component.html',
  styleUrls: ['./editar-perfil.component.scss'],
  providers: [
    ConfirmationService
  ]
})
export class EditarPerfilComponent implements OnInit {

  isLoading!: boolean;
  consultar!: boolean;

  editarPerfil!: FormGroup;
  classPerfil!: IPerfil[];

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private perfilService: PerfilesService
  ) { }

  ngOnInit(): void {
    this.classPerfil = this.config.data.perfil as IPerfil[];
    this.consultar = this.config.data.vista;

    this.editarPerfil = this.fb.group({
      coPerf: [{value: '', disabled: true}, [ Validators.required ]],
      noPerf: ['', [ Validators.required ]]
    });

    this.hidrate( this.classPerfil[0] );
  }

  hidrate( d: IPerfil ) {
    if ( this.consultar )
      this.editarPerfil.disable();

    this.editarPerfil.get('coPerf')?.setValue( d.coPerf );
    this.editarPerfil.get('noPerf')?.setValue( d.noPerf );
  }

  campoEsValido( campo: string ) {
    return this.editarPerfil.controls[campo].errors && 
              this.editarPerfil.controls[campo].touched
  }

  update() {
    
    if ( this.editarPerfil.invalid ) {
      Swal.fire('Por favor llenar los campos requeridos', '', 'warning');
      this.editarPerfil.markAllAsTouched();
      return;
    }

    const data = {
      coPerf: this.editarPerfil.get('coPerf')?.value,
      noPerf: this.editarPerfil.get('noPerf')?.value,
      inTipoPerf: 'U',
      coSist: 20
    }
    console.log('editData', data);

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de actualizar el registro?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        this.perfilService.createOrUpdatePerfil( 1, data ).subscribe({
          next: ( d ) => {
            console.log( d );

            if ( d.codResult < 0 ) {
              Swal.close();
              Swal.fire(d.msgResult, '', 'error');
              return;
            }

            Swal.close();
            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            Swal.close();
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close();
  }

}
