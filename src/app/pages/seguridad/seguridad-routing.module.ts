import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../services/auth/auth-gaurd.service';

const routes: Routes = [
  {
    path: 'perfiles',
    loadChildren: () => import('./perfiles/perfiles.module').then( m => m.PerfilesModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'usuarios',
    loadChildren: () => import('./usuarios/usuarios.module').then( m => m.UsuariosModule ),
    canActivate: [ AuthGaurdService ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeguridadRoutingModule { }
