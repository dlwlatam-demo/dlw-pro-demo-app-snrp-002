import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DigitOnlyModule } from '@uiowa/digit-only';

import { PagesRoutingModule } from './pages-routing.module';
import { NgPrimeModule } from '../ng-prime/ng-prime.module';

import { SiafComponent } from './upload/siaf/siaf.component';
import { UploadComponent } from './upload/upload.component';
import { ConstanciaComponent } from './upload/constancia/constancia.component';
import { LogosComponent } from './upload/logos/logos.component';
import { OtrosComponent } from './upload/otros/otros.component';


@NgModule({
  declarations: [
    UploadComponent,
    SiafComponent,
    ConstanciaComponent,
    LogosComponent,
    OtrosComponent
  ],
  exports: [
    UploadComponent,
    SiafComponent,
    ConstanciaComponent,
    LogosComponent,
    OtrosComponent,
    DigitOnlyModule
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PagesRoutingModule,
    NgPrimeModule,
    DigitOnlyModule
  ]
})
export class PagesModule { }
