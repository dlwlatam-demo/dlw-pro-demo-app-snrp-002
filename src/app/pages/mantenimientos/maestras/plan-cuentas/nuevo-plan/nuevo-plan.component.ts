import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { ConfirmationService, MessageService } from 'primeng/api';

import { CuentaContableService } from '../../../../../services/mantenimientos/maestras/cuenta-contable.service';

@Component({
  selector: 'app-nuevo-plan',
  templateUrl: './nuevo-plan.component.html',
  styleUrls: ['./nuevo-plan.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class NuevoPlanComponent implements OnInit {

  isLoading!: boolean;
  codigo!: boolean;

  nuevaCuenta!: FormGroup;

  constructor( private fb: FormBuilder,
               private ref: DynamicDialogRef,
               private messageService: MessageService,
               private confirmService: ConfirmationService,
               private cuentaService: CuentaContableService ) { }

  ngOnInit(): void {
    this.nuevaCuenta = this.fb.group({
      cuenta: ['', [ Validators.required ]],
      nombre: ['', [ Validators.required ]]
    });
  }

  campoEsValido( campo: string ) {
    return this.nuevaCuenta.controls[campo].errors && 
              this.nuevaCuenta.controls[campo].touched
  }

  grabar() {

    if ( this.nuevaCuenta.invalid ) {
      this.nuevaCuenta.markAllAsTouched();
      return;
    }

    const data = {
      coCuenCont: this.nuevaCuenta.get('cuenta')?.value,
      noCuenCont: this.nuevaCuenta.get('nombre')?.value
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de crear el registro?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        this.cuentaService.createOrUpdate( 0, data ).subscribe({
          next: ( d ) => {

            if ( d.codResult < 0 ) {
              Swal.close();
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención!', 
                detail: d.msgResult,
                sticky: true
              });

              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            Swal.close();
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
