import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService, ConfirmationService } from 'primeng/api';

import { Estado } from '../../../../../interfaces/combos.interface';

import { PlanCuentaContable } from '../../../../../models/mantenimientos/maestras/plan-cuenta.model';

import { CuentaContableService } from '../../../../../services/mantenimientos/maestras/cuenta-contable.service';
import { GeneralService } from '../../../../../services/general.service';

import { EditarPlanComponent } from '../editar-plan/editar-plan.component';
import { NuevoPlanComponent } from '../nuevo-plan/nuevo-plan.component';

import { environment } from '../../../../../../environments/environment';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';

@Component({
  selector: 'app-bandeja-plan',
  templateUrl: './bandeja-plan.component.html',
  styleUrls: ['./bandeja-plan.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class BandejaPlanComponent extends BaseBandeja implements OnInit {

  disabledEdit!: boolean;
  registros: string[] = [];

  cuentas!: FormGroup;

  $estado!: Estado[];
  enviadosList!: PlanCuentaContable[];

  currentPage: string = environment.currentPage;

  constructor( 
    private fb: FormBuilder,
    private confirmService: ConfirmationService,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private cuentaService: CuentaContableService 
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Plan_de_Cuentas;
    this.btnPlanDeCuenta();

    let estadoItem: Estado = { deEstado: '(TODOS)', coEstado: '*' };

    this.$estado = this.generalService.getCbo_Estado();
    this.$estado.unshift( estadoItem );

    this.cuentas = this.fb.group({
      contable: ['', [ Validators.required ]],
      nombre: ['', [ Validators.required ]],
      estado: ['A', [ Validators.required ]]
    });

    this.buscarCuenta();
  }

  buscarCuenta() {
    const contable = this.cuentas.get('contable')?.value;
    const nombre = this.cuentas.get('nombre')?.value

    let estado = this.cuentas.get('estado')?.value;
    if ( estado == '' ) {
      estado = 'A';
    } else if ( estado == '*' ) {
      estado = '';
    }

    this.buscarBandeja( contable, nombre, estado );
  }

  buscarBandeja( cont: string, nomb: string, est: string ) {
    Swal.showLoading();
    this.cuentaService.getBandejaCuentaContable( cont, nomb, est ).subscribe({
      next: ( data ) => {
        this.disabledEdit = est == 'I' ? true : false;

        for ( let d of data ) {
          if ( d.inRegi == 'A' )
            d.inRegi = 'Activo'
          else
            d.inRegi = 'Inactivo'
        }

        this.enviadosList = data;
      },
      error: () => {
        Swal.close()
        Swal.fire(
          'No se encuentran registros con los datos ingresados',
          'Plan de Cuentas Contables',
          'info'
        );

        this.enviadosList = [];
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  nuevoPlan() {
    const dialog = this.dialogService.open( NuevoPlanComponent, {
      header: 'Registrar Nueva Cuenta',
      width: '50%',
      closable: false
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha creado correctamente'
        });

        this.buscarCuenta();
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al crear su registro'
        });
      }
    });
  }

  editOrView( value: string ) {
    if ( !this.validateSelect( value ) )
      return;

    let dataDialog = {
      cuenta: this.selectedValues,
      vista: value == 'edit' ? false : true
    };

    const title: string = value == 'edit' ? 'Editar' : 'Consultar';

    const dialog = this.dialogService.open( EditarPlanComponent, {
      header: `${ title } Cuenta Contable`,
      width: '50%',
      closable: false,
      data: dataDialog
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha actualizado correctamente'
        });

        this.buscarCuenta();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al actualizar su registro'
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  anular() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Inactivo' ) {
        Swal.fire('No puede anular un registro ya inactivo', '', 'warning');
        return;
      }

      this.registros.push( registro.coCuenCont );
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea anular ${ msg1 } ${ msg2 }?`,
      accept: () => {
        Swal.showLoading();
        this.loadingAnu = true;

        const data = {
          planesCuenta: this.registros
        }

        this.cuentaService.anularCuentaContable( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se anularon los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al anular los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAnu = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscarCuenta();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  activar() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Activo' ) {
        Swal.fire('No puede habilitar un registro ya activo', '', 'warning');
        return;
      }

      this.registros.push( registro.coCuenCont! );
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';
    
    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea activar ${ msg1 } ${ msg2 }?`,
      accept: () => {
        Swal.showLoading();
        this.loadingAct = true;

        const data = {
          planesCuenta: this.registros
        }

        this.cuentaService.activarCuentaContable( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se activaron los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al activar los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAct = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscarCuenta();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
