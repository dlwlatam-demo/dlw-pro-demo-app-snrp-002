import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ConfirmationService, MessageService } from 'primeng/api';

import { PlanCuentaContable } from '../../../../../models/mantenimientos/maestras/plan-cuenta.model';

import { CuentaContableService } from '../../../../../services/mantenimientos/maestras/cuenta-contable.service';

@Component({
  selector: 'app-editar-plan',
  templateUrl: './editar-plan.component.html',
  styleUrls: ['./editar-plan.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class EditarPlanComponent implements OnInit {

  isLoading!: boolean;
  codigo!: boolean;
  consultar!: boolean;

  editarCuenta!: FormGroup;
  cuentaContable!: PlanCuentaContable[];

  constructor( private fb: FormBuilder,
               private ref: DynamicDialogRef, 
               private config: DynamicDialogConfig,
               private confirmService: ConfirmationService,
               private messageService: MessageService,
               private cuentaService: CuentaContableService ) { }

  ngOnInit(): void {

    this.cuentaContable = this.config.data.cuenta as PlanCuentaContable[];
    this.consultar = this.config.data.vista;

    this.editarCuenta = this.fb.group({
      cuenta: [{value: '', disabled: true}, [ Validators.required ]],
      nombre: ['', [ Validators.required ]]
    });

    this.cuentaService.getCuentaContableById( this.cuentaContable['0'].coCuenCont! ).subscribe({
      next: ( data ) => {
        this.hidrate( data as PlanCuentaContable );
      },
      error: ( e ) => {
        console.log( e );
      }
    });
  }

  hidrate( d: PlanCuentaContable ) {
    if ( this.consultar )
      this.editarCuenta.disable();
      
    this.editarCuenta.get('cuenta')?.setValue( d.coCuenCont );
    this.editarCuenta.get('nombre')?.setValue( d.noCuenCont );
  }

  campoEsValido( campo: string ) {
    return this.editarCuenta.controls[campo].errors && 
              this.editarCuenta.controls[campo].touched
  }

  update() {

    if ( this.editarCuenta.invalid ) {
      this.editarCuenta.markAllAsTouched();
      return;
    }

    const data = {
      coCuenCont: this.editarCuenta.get('cuenta')?.value,
      noCuenCont: this.editarCuenta.get('nombre')?.value
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de actualizar el registro?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        this.cuentaService.createOrUpdate( 1, data ).subscribe({
          next: ( d ) => {
            console.log( d );

            if ( d.codResult < 0 ) {
              Swal.close();
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención!', 
                detail: d.msgResult,
                sticky: true
              });

              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            Swal.close();
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
