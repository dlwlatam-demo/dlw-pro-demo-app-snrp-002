import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MaestrasRoutingModule } from './maestras-routing.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';
import { PagesModule } from '../../pages.module';

import { BandejaCuentasComponent } from './cuentas-bancarias/bandeja-cuentas/bandeja-cuentas.component';
import { NuevaCuentaComponent } from './cuentas-bancarias/nueva-cuenta/nueva-cuenta.component';
import { BandejaInfoComponent } from './informacion-distribucion/bandeja-info/bandeja-info.component';
import { ImportarComponent } from './informacion-distribucion/importar/importar.component';
import { BandejaPlanComponent } from './plan-cuentas/bandeja-plan/bandeja-plan.component';
import { NuevoPlanComponent } from './plan-cuentas/nuevo-plan/nuevo-plan.component';
import { EditarPlanComponent } from './plan-cuentas/editar-plan/editar-plan.component';
import { BandejaTasasComponent } from './tasas-distribucion/bandeja-tasas/bandeja-tasas.component';
import { NuevoTasasComponent } from './tasas-distribucion/nuevo-tasas/nuevo-tasas.component';
import { EditarTasasComponent } from './tasas-distribucion/editar-tasas/editar-tasas.component';
import { BandejaClasificadorComponent } from './clasificadores/bandeja-clasificador/bandeja-clasificador.component';
import { NuevoClasificadorComponent } from './clasificadores/nuevo-clasificador/nuevo-clasificador.component';
import { EditarClasificadorComponent } from './clasificadores/editar-clasificador/editar-clasificador.component';


@NgModule({
  declarations: [
    BandejaCuentasComponent,
    NuevaCuentaComponent,
    BandejaInfoComponent,
    ImportarComponent,
    BandejaPlanComponent,
    NuevoPlanComponent,
    EditarPlanComponent,
    BandejaTasasComponent,
    NuevoTasasComponent,
    EditarTasasComponent,
    BandejaClasificadorComponent,
    NuevoClasificadorComponent,
    EditarClasificadorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaestrasRoutingModule,
    NgPrimeModule,
    PagesModule
  ]
})
export class MaestrasModule { }
