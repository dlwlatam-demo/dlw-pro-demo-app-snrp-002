import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, ConfirmationService, MessageService } from 'primeng/api';

import { IClasificador } from '../../../../../interfaces/configuracion/clasificador.interface';
import { Estado } from '../../../../../interfaces/combos.interface';

import { BodyEliminateActivateClsf } from '../../../../../models/mantenimientos/maestras/clasificador.model';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';

import { GeneralService } from '../../../../../services/general.service';
import { UtilService } from '../../../../../services/util.service';
import { ClasificadorService } from '../../../../../services/mantenimientos/maestras/clasificador.service';

import { NuevoClasificadorComponent } from '../nuevo-clasificador/nuevo-clasificador.component';
import { EditarClasificadorComponent } from '../editar-clasificador/editar-clasificador.component';

import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-bandeja-clasificador',
  templateUrl: './bandeja-clasificador.component.html',
  styleUrls: ['./bandeja-clasificador.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class BandejaClasificadorComponent extends BaseBandeja implements OnInit {

  form!: FormGroup;

  $estado!: Estado[];

  enviadosList!: IClasificador[];
  selectedClasificador: IClasificador[] = [];

  bodyAnularActivarClsf!: BodyEliminateActivateClsf;

  currentPage: string = environment.currentPage;

  constructor(
    private fb: FormBuilder,
    private dialogService: DialogService,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private generalService: GeneralService,
    private utilService: UtilService,
    private clasificadorService: ClasificadorService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Clasificadores;
    this.btnClasificadores();

    this.bodyAnularActivarClsf = new BodyEliminateActivateClsf();

    let estadoItem: Estado = { deEstado: '(TODOS)', coEstado: '*' };

    this.$estado = this.generalService.getCbo_Estado();
    this.$estado.unshift( estadoItem );

    this.form = this.fb.group({
      clasificador: ['', []],
      descripcion: ['', []],
      estado: ['A', []]
    });

    this.buscarClasificador();
  }

  buscarClasificador() {
    let clasificador = this.form.get('clasificador')?.value;
    clasificador = clasificador == '' ? null : clasificador;

    let descripcion = this.form.get('descripcion')?.value;
    descripcion = descripcion == '' ? null : descripcion;

    let estado = this.form.get('estado')?.value;
    estado = estado == '*' ? null : estado;

    this.buscarBandeja( clasificador, descripcion, estado );
  }

  buscarBandeja( coClsf: string, deClsf: string, inRegi: string ) {
    this.utilService.onShowProcessLoading('Cargando datos...');

    this.clasificadorService.getBandejaClasificadores( coClsf, deClsf, inRegi ).subscribe({
      next: ( data ) => {
        for ( let d of data ) {
          if ( d.inRegi == 'A' )
            d.inRegi = 'Activo';
          else
            d.inRegi = 'Inactivo';
        }

        this.enviadosList = data;
      },
      error: ( e ) => {
        console.log( e );
        this.utilService.onShowAlert('info', 'Atención', 'No se encontrarón registros con los datos ingresados');

        this.enviadosList = [];
      },
      complete: () => {
        this.utilService.onCloseLoading();
      }
    });
  }

  nuevoClasificador() {
    const dialog = this.dialogService.open( NuevoClasificadorComponent, {
      header: 'Clasificador - Nuevo',
      width: '50%',
      closable: false
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha creado correctamente'
        });

        this.buscarClasificador();
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al crear su registro'
        });
      }
    });
  }

  editOrView( value: string ) {

    if ( this.selectedClasificador.length === 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro.");
      return;
    }

    if ( this.selectedClasificador.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede editar un registro a la vez.");
      return;
    }

    if ( this.selectedClasificador[0].inRegi === 'Inactivo' ) {
      this.utilService.onShowAlert("warning", "Atención", "No puede editar un registro con estado INACTIVO.");
      return;
    }

    let dataDialog = {
      clsf: this.selectedClasificador[0],
      vista: value == 'edit' ? false : true
    };

    const title: string = value == 'edit' ? 'Editar' : 'Consultar'

    const dialog = this.dialogService.open( EditarClasificadorComponent, {
      header: `Clasificador - ${ title }`,
      width: '50%',
      closable: false,
      data: dataDialog
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha actualizado correctamente'
        });

        this.buscarClasificador();
        this.selectedClasificador = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al actualizar su registro'
        });

        this.selectedClasificador = [];
      }

      this.selectedClasificador = [];
    });
  }

  anular() {

    if ( this.selectedClasificador.length === 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro.");
      return;
    }

    for ( let clsf of this.selectedClasificador ) {
      if ( clsf.inRegi === 'Inactivo' ) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede anular registros con estado ACTIVO.");
        this.selectedClasificador = [];
        return;
      }
    }

    this.bodyAnularActivarClsf.listaClasificador = this.selectedClasificador.map( clsf => { return clsf.coClsf });

    const msgText = (this.selectedClasificador?.length === 1) ? 'el registro' : 'los registros';

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea anular ${ msgText }?`,
      accept: () => {
        this.utilService.onShowProcess('anular');

        this.clasificadorService.anularClasificador( this.bodyAnularActivarClsf ).subscribe({
          next: ( data ) => {
            if ( data.codResult < 0 ) {
              this.utilService.onShowAlert("info", "Atención", data.msgResult);
            }
            else {
              this.messageService.add({ 
                severity: 'success', 
                summary: 'Se anularon los registros correctamente', 
              });

              this.selectedClasificador = [];
              this.buscarClasificador();
            }
          },
          error: () => {
            this.messageService.add({ 
              severity: 'error', 
              summary: 'No se anularon los registros correctamente', 
            });
          }
        });
      }
    });
  }

  activar() {

    if ( this.selectedClasificador.length === 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro.");
      return;
    }

    for ( let clsf of this.selectedClasificador ) {
      if ( clsf.inRegi === 'Activo' ) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede anular registros con estado INACTIVO.");
        this.selectedClasificador = [];
        return;
      }
    }

    this.bodyAnularActivarClsf.listaClasificador = this.selectedClasificador.map( clsf => { return clsf.coClsf });

    const msgText = (this.selectedClasificador?.length === 1) ? 'el registro' : 'los registros';

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea activar ${ msgText }?`,
      accept: () => {
        this.utilService.onShowProcess('activar');

        this.clasificadorService.activarClasificador( this.bodyAnularActivarClsf ).subscribe({
          next: ( data ) => {
            if ( data.codResult < 0 ) {
              this.utilService.onShowAlert("info", "Atención", data.msgResult);
            }
            else {
              this.messageService.add({ 
                severity: 'success', 
                summary: 'Se activaron los registros correctamente', 
              });

              this.selectedClasificador = [];
              this.buscarClasificador();
            }
          },
          error: () => {
            this.messageService.add({ 
              severity: 'error', 
              summary: 'No se activaron los registros correctamente', 
            });
          }
        });
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
