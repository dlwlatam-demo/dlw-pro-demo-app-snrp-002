import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { MessageService, ConfirmationService } from 'primeng/api';

import { IClasificador } from '../../../../../interfaces/configuracion/clasificador.interface';

import { BodyClasificador } from '../../../../../models/mantenimientos/maestras/clasificador.model';

import { ClasificadorService } from '../../../../../services/mantenimientos/maestras/clasificador.service';

@Component({
  selector: 'app-editar-clasificador',
  templateUrl: './editar-clasificador.component.html',
  styleUrls: ['./editar-clasificador.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class EditarClasificadorComponent implements OnInit {

  isLoading!: boolean;
  consultar!: boolean;

  editarClasificador!: FormGroup;

  clasificador!: IClasificador;
  bodyClasificador!: BodyClasificador;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private clasificadorService: ClasificadorService
  ) { }

  ngOnInit(): void {
    this.bodyClasificador = new BodyClasificador();

    this.clasificador = this.config.data.clsf as IClasificador;
    this.consultar = this.config.data.vista;

    this.editarClasificador = this.fb.group({
      clasificador: [{value: '', disabled: true}, [ Validators.required ]],
      descripcion: ['', [ Validators.required ]],
      desDetallada: ['', []]
    });

    this.clasificadorService.getClasificadorById( this.clasificador.coClsf ).subscribe({
      next: ( data ) => {
        this.hidrate( data as IClasificador[] );
      },
      error: ( e ) => {
        console.log( e );
      }
    });
  }

  hidrate( clsf: IClasificador[] ) {
    if ( this.consultar )
      this.editarClasificador.disable();
    
    this.editarClasificador.get('clasificador')?.setValue( clsf[0].coClsf );
    this.editarClasificador.get('descripcion')?.setValue( clsf[0].deClsf );
    this.editarClasificador.get('desDetallada')?.setValue( clsf[0].deDetaClsf );
  }

  campoEsValido( campo: string ) {
    return this.editarClasificador.controls[campo].errors && 
              this.editarClasificador.controls[campo].touched
  }

  update() {
    if ( this.editarClasificador.invalid ) {
      this.editarClasificador.markAllAsTouched();
      return;
    }

    this.bodyClasificador.coClsf = this.editarClasificador.get('clasificador')?.value;
    this.bodyClasificador.deClsf = this.editarClasificador.get('descripcion')?.value;
    this.bodyClasificador.deDetaClsf = this.editarClasificador.get('desDetallada')?.value;

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de actualizar el registro?',
      accept: () => {
        this.isLoading = true;

        this.clasificadorService.updateClasificador( this.bodyClasificador ).subscribe({
          next: ( data ) => {

            if ( data.codResult < 0 ) {
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención!', 
                detail: data.msgResult,
                sticky: true
              });

              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
