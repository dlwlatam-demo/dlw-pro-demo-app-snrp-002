import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

import { BodyClasificador } from '../../../../../models/mantenimientos/maestras/clasificador.model';

import { ClasificadorService } from '../../../../../services/mantenimientos/maestras/clasificador.service';

@Component({
  selector: 'app-nuevo-clasificador',
  templateUrl: './nuevo-clasificador.component.html',
  styleUrls: ['./nuevo-clasificador.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class NuevoClasificadorComponent implements OnInit {

  isLoading!: boolean;

  nuevoClasificador!: FormGroup

  bodyClasificador!: BodyClasificador;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private clasificadorService: ClasificadorService
  ) { }

  ngOnInit(): void {
    this.bodyClasificador = new BodyClasificador();

    this.nuevoClasificador = this.fb.group({
      clasificador: ['', [ Validators.required ]],
      descripcion: ['', [ Validators.required ]],
      desDetallada: ['', []]
    });
  }

  campoEsValido( campo: string ) {
    return this.nuevoClasificador.controls[campo].errors && 
              this.nuevoClasificador.controls[campo].touched
  }

  grabar() {
    if ( this.nuevoClasificador.invalid ) {
      this.nuevoClasificador.markAllAsTouched();
      return;
    }

    this.bodyClasificador.coClsf = this.nuevoClasificador.get('clasificador')?.value;
    this.bodyClasificador.deClsf = this.nuevoClasificador.get('descripcion')?.value;
    this.bodyClasificador.deDetaClsf = this.nuevoClasificador.get('desDetallada')?.value;

    console.log('this.bodyClasificador', this.bodyClasificador);
    this.confirmService.confirm({
      message: '¿Está Ud. seguro de crear el registro?',
      accept: () => {
        this.isLoading = true;

        this.clasificadorService.createClasificador( this.bodyClasificador ).subscribe({
          next: ( d ) => {

            if ( d.codResult < 0 ) {
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención!', 
                detail: d.msgResult,
                sticky: true
              });

              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
