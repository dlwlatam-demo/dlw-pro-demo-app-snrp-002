import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, ConfirmationService, MessageService } from 'primeng/api';

import { Estado } from '../../../../../interfaces/combos.interface';
import { TasaDistribucion } from '../../../../../models/mantenimientos/maestras/tasas-distribucion.model';

import { MenuOpciones } from '../../../../../models/auth/Menu.model';

import { GeneralService } from '../../../../../services/general.service';
import { TasasDistribucionService } from '../../../../../services/mantenimientos/maestras/tasas-distribucion.service';

import { EditarTasasComponent } from '../editar-tasas/editar-tasas.component';
import { NuevoTasasComponent } from '../nuevo-tasas/nuevo-tasas.component';

import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-bandeja-tasas',
  templateUrl: './bandeja-tasas.component.html',
  styleUrls: ['./bandeja-tasas.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class BandejaTasasComponent extends BaseBandeja implements OnInit {

  registros: string[] = [];
  
  tasas!: FormGroup;

  $estado!: Estado[];
  enviadosList!: TasaDistribucion[];

  currentPage: string = environment.currentPage;

  constructor( 
    private fb: FormBuilder,
    private confirmService: ConfirmationService,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private tasaService: TasasDistribucionService 
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Tasas_de_Distribucion;
    this.btnTasasDistribucion();

    let estadoItem: Estado = { deEstado: '(TODOS)', coEstado: '' };

    this.$estado = this.generalService.getCbo_Estado();
    this.$estado.push( estadoItem );

    this.tasas = this.fb.group({
      zona: ['', [ Validators.required ]],
      estado: ['', [ Validators.required ]]
    });

    this.buscarTasas();
  }

  buscarTasas() {
    const zona = this.tasas.get('zona')?.value;

    let estado = this.tasas.get('estado')?.value;
    if ( estado == '*' ) {
      estado = '';
    }

    this.buscarBandeja(zona, estado);
  }

  buscarBandeja(zona: string, est: string) {
    Swal.showLoading();
    this.tasaService.getBandejaTasaDistribucion(zona, est).subscribe({
      next: ( data ) => {
        for ( let d of data ) {
          if ( d.inRegi == 'A' )
            d.inRegi = 'Activo'
          else
            d.inRegi = 'Inactivo'
        }

        this.enviadosList = data;
      },
      error: () => {
        Swal.close()
        Swal.fire(
          'No se encuentran registros con los datos ingresados',
          'Tasas Distribución',
          'info'
        );

        this.enviadosList = [];
      },
      complete: () => {
        Swal.close();
      }
    });

  }

  nuevoTasa() {
    const dialog = this.dialogService.open( NuevoTasasComponent, {
      header: 'Registrar Nueva Tasa',
      width: '45%',
      closable: false
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha creado correctamente'
        });

        this.buscarTasas();
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al crear su registro'
        });
      }
    });
  }

  editarTasa( value: string ) {
    if ( !this.validateSelect(value) )
      return;

    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Tasas de Distribución', 'Debe seleccionar un registro para editarlo!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Tasas de Distribución', 'Solo puede editar un registro a la vez!', 'error');
      return;
    } else 

    if (this.selectedValues[0].inRegi == 'Inactivo') {
      Swal.fire( 'No puede editar un registro en estado Inactivo', '', 'error');
      this.selectedValues = [];
      return;
    }

    const title: string = value == 'edit' ? 'Editar' : 'Consultar';

    const dialog = this.dialogService.open( EditarTasasComponent, {
      header: `${ title } Tasa de Distribución`,
      width: '45%',
      closable: false,
      data: {
        op: value,
        td: this.selectedValues
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha actualizado correctamente'
        });

        this.buscarTasas();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al actualizar su registro'
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  anular() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Inactivo' ) {
        Swal.fire('No puede anular un registro ya inactivo', '', 'warning');
        return;
      }

      this.registros.push( registro.coZonaRegi! );
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea anular ${ msg1 } ${ msg2 }?`,
      accept: () => {
        Swal.showLoading();
        this.loadingAnu = true;

        const data = {
          tasasDistribucion: this.registros
        }

        this.tasaService.anularTasaDistribucion( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se anularon los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al anular los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAnu = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscarTasas();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  activar() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Activo' ) {
        Swal.fire('No puede habilitar un registro ya activo', '', 'warning');
        return;
      }

      this.registros.push( registro.coZonaRegi! );
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea activar ${ msg1 } ${ msg2 }?`,
      accept: () => {
        Swal.showLoading();
        this.loadingAct = true;

        const data = {
          tasasDistribucion: this.registros
        }

        this.tasaService.activarTasaDistribucion( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se activaron los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al activar los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAct = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscarTasas();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
