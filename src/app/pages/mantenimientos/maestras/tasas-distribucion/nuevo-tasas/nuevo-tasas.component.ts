import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, map } from 'rxjs';

import Swal from 'sweetalert2';
import { MessageService, ConfirmationService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { GeneralService } from '../../../../../services/general.service';
import { TasasDistribucionService } from '../../../../../services/mantenimientos/maestras/tasas-distribucion.service';

@Component({
  selector: 'app-nuevo-tasas',
  templateUrl: './nuevo-tasas.component.html',
  styleUrls: ['./nuevo-tasas.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class NuevoTasasComponent implements OnInit {

  validarZona!: boolean;
  isLoading!: boolean;

  nuevaTasa!: FormGroup;

  $zonas!: Observable< any >;

  constructor( private fb: FormBuilder,
               private ref: DynamicDialogRef,
               private messageService: MessageService,
               private confirmService: ConfirmationService,
               private generalService: GeneralService,
               private tasaService: TasasDistribucionService ) { }

  loadCbo_ZonaRegistral() {
    return this.generalService.getCbo_Zonas_Usuario('');
  }

  ngOnInit(): void {
    const zonaItem: any = { coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' };

    this.$zonas = this.loadCbo_ZonaRegistral().pipe(
      map( z => {
        z.unshift( zonaItem );
        return z;
      })
    );

    this.nuevaTasa = this.fb.group({
      zona: ['', [ Validators.required ]],
      distribucion: ['', [ Validators.required ]]
    });

    document.getElementById("btnGrabar").focus();
  }

  campoEsValido( campo: string ) {
    return this.nuevaTasa.controls[campo].errors && 
              this.nuevaTasa.controls[campo].touched
  }

  selectZona() {
    this.validarZona = false;
  }

  grabar() {

    if ( this.nuevaTasa.get('zona')?.value == '*' ) {
      this.validarZona = true;
      return;
    }

    if ( this.nuevaTasa.invalid ) {
      this.nuevaTasa.markAllAsTouched();
      return;
    }

    const data = {
      coZonaRegi: this.nuevaTasa.get('zona')?.value,
      poDist: this.nuevaTasa.get('distribucion')?.value
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de crear el registro?',
      acceptButtonStyleClass: 'p-button-buscar p-button-sm ml-2',
      rejectButtonStyleClass: 'p-button-anular p-button-sm',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        this.tasaService.addTasaDistribucion( data ).subscribe({
          next: ( _d ) => {

            if ( _d.codResult < 0 ) {
              Swal.close();
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención!', 
                detail: _d.msgResult,
                sticky: true
              });

              return;
            }

            this.ref.close('accept');
          },
          error: ( _e ) => {

            Swal.close();
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
