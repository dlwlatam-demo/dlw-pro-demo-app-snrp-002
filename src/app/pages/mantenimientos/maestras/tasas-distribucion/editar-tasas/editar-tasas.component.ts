import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, map } from 'rxjs';

import Swal from 'sweetalert2';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ConfirmationService, MessageService } from 'primeng/api';

import { TasaDistribucion } from '../../../../../models/mantenimientos/maestras/tasas-distribucion.model';

import { GeneralService } from '../../../../../services/general.service';
import { TasasDistribucionService } from '../../../../../services/mantenimientos/maestras/tasas-distribucion.service';

@Component({
  selector: 'app-editar-tasas',
  templateUrl: './editar-tasas.component.html',
  styleUrls: ['./editar-tasas.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class EditarTasasComponent implements OnInit {

  isLoading!: boolean;
  consultar!: boolean;

  $zonas!: Observable< any >;

  editarTasa!: FormGroup;

  tasaDistribucion!: TasaDistribucion[];
  tasa!: TasaDistribucion;

  constructor( private fb: FormBuilder,
               private ref: DynamicDialogRef, 
               private config: DynamicDialogConfig,
               private confirmService: ConfirmationService,
               private messageService: MessageService,
               private generalService: GeneralService,
               private tasaService: TasasDistribucionService ) { }

  loadCbo_ZonaRegistral() {
    return this.generalService.getCbo_Zonas_Usuario('');
  }

  ngOnInit(): void {
    const zonaItem: any = { coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' };

    this.$zonas = this.loadCbo_ZonaRegistral().pipe(
      map( z => {
        z.unshift( zonaItem );
        return z;
      })
    );

    this.tasaDistribucion = this.config.data.td as TasaDistribucion[];
    const op = this.config.data.op;

    this.editarTasa = this.fb.group({
      zona: [{value: '', disabled: true}, [ Validators.required ]],
      distribucion: ['', [ Validators.required ]]
    });

    this.tasaService.getTasaDistribucionById( this.tasaDistribucion['0'].coZonaRegi! ).subscribe({
      next: ( data ) => {
        this.tasa = data;
        this.hidrate( data as TasaDistribucion );
      },
      error: ( e ) => {
        console.log( e );
      }
    });

    this.consultar = op == 'edit' ? false : true;
    
    if (op == 'view') {
      this.editarTasa.disable();
    }
  }

  hidrate( d: TasaDistribucion ) {
    this.editarTasa.get('zona')?.setValue( d.coZonaRegi );
    this.editarTasa.get('distribucion')?.setValue( d.poDist );
  }

  campoEsValido( campo: string ) {
    return this.editarTasa.controls[campo].errors && 
              this.editarTasa.controls[campo].touched
  }

  update() {

    if ( this.editarTasa.invalid ) {
      this.editarTasa.markAllAsTouched();
      return;
    }

    const data = {
      coZonaRegi: this.editarTasa.get('zona')?.value,
      poDist: this.editarTasa.get('distribucion')?.value,
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de actualizar el registro?',
      acceptButtonStyleClass: 'p-button-buscar p-button-sm ml-2',
      rejectButtonStyleClass: 'p-button-anular p-button-sm',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        this.tasaService.updateTasaDistribucion( data ).subscribe({
          next: ( _d ) => {
            
            if ( _d.codResult < 0 ) {
              Swal.close();
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención!', 
                detail: _d.msgResult,
                sticky: true
              });

              return;
            }

            this.ref.close('accept');
          },
          error: ( _e ) => {

            Swal.close();
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      } 
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
