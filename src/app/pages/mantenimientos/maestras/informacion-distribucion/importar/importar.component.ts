import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import * as XLSX from 'xlsx';
import Swal from 'sweetalert2';

import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';

import { GeneralService } from '../../../../../services/general.service';
import { InformacionDistribucionService } from '../../../../../services/mantenimientos/maestras/informacion-distribucion.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';

export class trama {
  a: string;
  b: string;
  c: string;
  d: string;
  e: string;
  f: string;
}

@Component({
  selector: 'app-importar',
  templateUrl: './importar.component.html',
  styleUrls: ['./importar.component.scss'],
  providers: [
    MessageService
  ]
})
export class ImportarComponent implements OnInit {

  importar!: FormGroup;
  $userCode:any

  $zonas!: Observable< any >;
  $recaudaciones!: Observable< any >;

  informacionDistribucionTrama: trama[];

  constructor( private fb: FormBuilder,
               private ref: DynamicDialogRef,
               private messageService: MessageService,
               private generalService: GeneralService,
               private validateService: ValidatorsService,
			   private funciones: Funciones,
               private informacionService: InformacionDistribucionService ) { }

  ngOnInit(): void {

    this.$userCode = localStorage.getItem('user_code')||'';
    //this.$zonas = this.generalService.getCbo_Zonas();
    this.$zonas = this.generalService.getCbo_Zonas_Usuario(this.$userCode);
    this.$recaudaciones = this.generalService.getCbo_TipoRecaudacion();

    this.importar = this.fb.group({
      zona: ['', [ Validators.required ]],
      fecDesde: ['', [ Validators.required ]],
      fecHasta: ['', [ Validators.required ]],
      tipoReca: ['', [ Validators.required ]]
    });
  }

  selectFile(e: any) {
    console.log("cargando archivo");
    if ( (e.target.files[0].type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') ) {
      console.log("nombre del archivo: " + e.target.files[0].name);
      console.log("tipo del archivo " + e.target.files[0].type);

      const file = e.target.files[0];
  
      const reader = new FileReader();
      reader.onload = (e: any) => {
        
        let data = new Uint8Array(e.target.result);
        let workbook = XLSX.read(data, { type: "array" });
        let worksheet = workbook.Sheets[workbook.SheetNames[0]];
        //let sheet =    XLSX.utils.sheet_to_json(worksheet, { header: 1 });
        let jsontext = XLSX.utils.sheet_to_json<string>(worksheet,{header: "A", raw:true});
  
        this.informacionDistribucionTrama = JSON.parse(JSON.stringify(jsontext));
        this.informacionDistribucionTrama.splice(0,1);
        this.informacionDistribucionTrama.splice(0,1);
        this.informacionDistribucionTrama.splice(0,1);

        this.informacionDistribucionTrama.map((item:any) => {
          item.a = this.getFechaCorta(item.A);
          item.b = item.B;
          item.c = item.C;
          item.d = item.D;
          item.e = item.E;
          item.f = item.F == 'Activo' ? 'A' : 'I'

          delete item.A;
          delete item.B;
          delete item.C;
          delete item.D;
          delete item.E;
          delete item.F;

        });

        console.log("informacionDistribucionTrama despues: " + JSON.stringify(this.informacionDistribucionTrama));
      };
      reader.readAsArrayBuffer(file);

    } else {
      console.log("Formato de archino no permitido", 'ERROR', { duration: 4000 });
      Swal.fire(
        'Formato de archino no permitido',
        'Solo se permiten los tipos de archivo .xlsx',
        'info'
      );
    }
  }

  addUploadedFile(file: any) {
    console.log(file);
    // (this.sustento.get('docs') as FormArray).push(
    //   this.fb.group(
    //     {
    //       doc: [file.fileId, []],
    //       nombre: [file.fileName, []],
    //       idAdjunto: [file?.idAdjunto, []]
    //     }
    //   )
    // )
  }

  deleteUploaded(fileId: string) {
    // let index: number;

    // const ar = <FormArray>this.sustento.get('docs')
    // index = ar.controls.findIndex((ctl: AbstractControl) => {
    //   return ctl.get('doc')?.value == fileId
    // })

    // ar.removeAt(index)

    // index = this.comprobanteSustento.findIndex( f => f.id == fileId );
    // this.comprobanteSustento.splice( index, 1 );
  }

  get isDisabled() {
    return false //!(this.comprobante['0'].esDocu == '001' || this.comprobante['0'].esDocu == '006' || this.comprobante['0'].esDocu == '007')
  }

  aceptar() {
    console.log(this.importar.value );
    
    const fechaDesde = this.importar.get('fecDesde')?.value;
    const fechaHasta = this.importar.get('fecHasta')?.value;
    let fechaDesdeFormat: string = '';
    let fechaHastaFormat: string = '';

    if ( fechaDesde ) {
      fechaDesdeFormat = this.validateService.formatDate(fechaDesde);
    }

    if ( fechaHasta ) {
      fechaHastaFormat = this.validateService.formatDate(fechaHasta);
    }

    const data = {
      coZonaRegi: this.importar.get('zona')?.value,
      tiReca: this.importar.get('tipoReca')?.value,
      feDesd: fechaDesdeFormat,
      feHast: fechaHastaFormat,
      informacionDistribucionTrama: this.informacionDistribucionTrama
    }

    console.log( data );

    this.informacionService.crearInfoDistribucion(data).subscribe({
      next: ( _d ) => {

        if ( _d.codResult < 0 ) {
          Swal.close();
          this.messageService.add({
            severity: 'warn', 
            summary: 'Atención!', 
            detail: _d.msgResult,
            sticky: true
          });

          return;
        }

        this.ref.close('accept');
      },
      error: ( e ) => {
        console.log( e );
        Swal.fire( 'Error al grabar los datos', '', 'error' );
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }
  
    private getFechaCorta(data: any):string {  
	return data?(!isNaN(Number(data))? this.funciones.convertirFechaDateToString(this.ExcelDateToJSDate1(data.toString())): this.isValidDate(data)?data :'') : '';
  }
  
	private ExcelDateToJSDate1(serial: number) : Date{
	   var utc_days  = Math.floor(serial - 25568);
	   var utc_value = utc_days * 86400;                                        
	   var date_info = new Date(utc_value * 1000);

	   var fractional_day = serial - Math.floor(serial) + 0.0000001;

	   var total_seconds = Math.floor(86400 * fractional_day);

	   var seconds = total_seconds % 60;

	   total_seconds -= seconds;

	   var hours = Math.floor(total_seconds / (60 * 60));
	   var minutes = Math.floor(total_seconds / 60) % 60;

	   return new Date(date_info.getFullYear(), date_info.getMonth(), date_info.getDate(), hours, minutes, seconds);
	}
	
	
private isValidDate(dateString: string): boolean {
  const regex = /^([1-9]|([012][0-9])|(3[01]))\/([0]{0,1}[1-9]|1[012])\/\d\d\d\d$/;
  return regex.test(dateString);
}

}
