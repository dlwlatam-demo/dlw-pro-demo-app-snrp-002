import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, map } from 'rxjs';

import Swal from 'sweetalert2';

import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService } from 'primeng/api';

import { Estado } from '../../../../../interfaces/combos.interface';
import { InformacionDistribucion } from '../../../../../models/mantenimientos/maestras/informacion-distribucion.model';

import { MenuOpciones } from '../../../../../models/auth/Menu.model';

import { GeneralService } from '../../../../../services/general.service';
import { InformacionDistribucionService } from '../../../../../services/mantenimientos/maestras/informacion-distribucion.service';

import { ImportarComponent } from '../importar/importar.component';

import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';

import { environment } from '../../../../../../environments/environment';
import { InformacionDistribucionExport } from 'src/app/models/mantenimientos/maestras/Informacion-distribucion-export.model';
import { ValidatorsService } from '../../../../../core/services/validators.service';

@Component({
  selector: 'app-bandeja-info',
  templateUrl: './bandeja-info.component.html',
  styleUrls: ['./bandeja-info.component.scss'],
  providers: [
    DialogService,
    MessageService
  ]
})
export class BandejaInfoComponent extends BaseBandeja implements OnInit {

  tipoRecaud: string = '';

  distribucion!: FormGroup;

  $zonas!: Observable< any >;
  $recaudaciones!: Observable< any >;
  $estados!: Estado[];

  enviadosList!: InformacionDistribucion[];
  informacionDistribucionExport!: InformacionDistribucionExport;

  currentPage: string = environment.currentPage;

  constructor( 
    private fb: FormBuilder,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private validateService: ValidatorsService,
    private informacionService: InformacionDistribucionService 
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Informacion_para_Distribucion;
    this.btnInformacionDistribucion();

    let estadoItem: Estado = { deEstado: '(TODOS)', coEstado: '*' };
    const zonasItem: any = { coZonaRegi : '*', deZonaRegi: '(SELECCIONE)' };
    const tipoRecaItem: any = { ccodigoHijo : '*', cdescri: '(SELECCIONE)' };
    
    this.$zonas = this.generalService.getCbo_Zonas_Usuario('').pipe(
      map( d => {
        d.unshift( zonasItem );
        return d;
      })
    );

    this.$recaudaciones = this.generalService.getCbo_TipoRecaudacion().pipe(
      map( d => {
        d.unshift( tipoRecaItem );
        return d;
      })
    );

    this.$estados = this.generalService.getCbo_Estado();
    this.$estados.unshift( estadoItem );

    this.distribucion = this.fb.group({
      zona: ['', [ Validators.required ]],
      fecDesde: ['', [ Validators.required ]],
      fecHasta: ['', [ Validators.required ]],
      tipoReca: ['001', [ Validators.required ]],
      estado: ['', [ Validators.required ]],
    });
    const hoy = new Date();
    const cDateD = new Date(hoy.getFullYear(), hoy.getMonth(), 1);
    this.distribucion.get('fecDesde')?.setValue( cDateD );
    const cDateH = new Date(hoy.getFullYear(), hoy.getMonth(), hoy.getDate());
    this.distribucion.get('fecHasta')?.setValue( cDateH );

    //this.buscarDistribucion();
  }

  buscarDistribucion() {
    Swal.showLoading();
    
    const fechaDesde = this.distribucion.get('fecDesde')?.value;
    const fechaHasta = this.distribucion.get('fecHasta')?.value;
    const fechaDesdeFormat:string = this.validateService.formatDate( fechaDesde ) ;
    const fechaHastaFormat:string = this.validateService.formatDate( fechaHasta ) ;

    let zona = this.distribucion.get('zona')?.value;
    zona = zona == '*' ? '' : zona;

    let tipoReca = this.distribucion.get('tipoReca')?.value;
    tipoReca = tipoReca == '*' ? '' : tipoReca;
    this.tipoRecaud = tipoReca;

    let inRegi = this.distribucion.get('estado')?.value;
    inRegi = inRegi == '*' ? '' : inRegi;

    const data = {
      "coZonaRegi": zona,
      "tiReca": tipoReca,
      "feDesd": fechaDesde ? fechaDesdeFormat : '',
      "feHast": fechaHasta ? fechaHastaFormat : '',
      "inRegi": inRegi,
    }

    this.informacionService.getBandejaInfoDistribucion(data).subscribe({
      next: ( data ) => {
        this.informacionDistribucionExport = data;
        this.enviadosList = data.lstInformacionDistribucion? data.lstInformacionDistribucion: [];
        for( let d of this.enviadosList ) {
          if ( d.inRegi == 'A' )
            d.inRegi = 'Activo'
          else
            d.inRegi = 'Inactivo'
        }
      },
      error: ( e ) => {
        console.log( e );
        Swal.close()
        Swal.fire(
          'No se encuentran registros con los datos ingresados',
          'Información Distribución',
          'info'
        );

        this.enviadosList = [];
      },
      complete: () => {
        Swal.close();
      }
    })

  }

  exportExcel() {
    console.log('Exportar Excel');

    const doc = this.informacionDistribucionExport.reporteExcel;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', 'Informacion_Distribucion');
    document.body.appendChild( download );
    download.click();
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
      let EXCEL_EXTENSION = ".xlsx";
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });

      const link = document.createElement("a");
      if (link.download !== undefined) {
          const url = URL.createObjectURL(data);
          link.setAttribute("href", url);
          link.setAttribute("download", fileName);
          link.style.visibility = "hidden";
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
      }
    });
  }

  importExcel() {
    const dialog = this.dialogService.open( ImportarComponent, {
      header: 'Información para la Distribución - Importar',
      width: '45%',
      closable: false
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha cargado correctamente'
        });

        this.buscarDistribucion();
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al cargar su registro'
        });
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
