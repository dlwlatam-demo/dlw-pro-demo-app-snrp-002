import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { BandejaCuentasComponent } from './cuentas-bancarias/bandeja-cuentas/bandeja-cuentas.component';
import { BandejaInfoComponent } from './informacion-distribucion/bandeja-info/bandeja-info.component';
import { BandejaPlanComponent } from './plan-cuentas/bandeja-plan/bandeja-plan.component';
import { BandejaTasasComponent } from './tasas-distribucion/bandeja-tasas/bandeja-tasas.component';
import { BandejaClasificadorComponent } from './clasificadores/bandeja-clasificador/bandeja-clasificador.component';

const routes: Routes = [
  { path: 'cuentas_bancarias', children: [{ path: 'bandeja', component: BandejaCuentasComponent }], canActivate: [ AuthGaurdService ] },
  { path: 'informacion_para_distribucion', children: [{ path: 'bandeja', component: BandejaInfoComponent }], canActivate: [ AuthGaurdService ] },
  { path: 'plan_de_cuentas', children: [{ path: 'bandeja', component: BandejaPlanComponent }], canActivate: [ AuthGaurdService ] },
  { path: 'tasas_de_distribucion', children: [{ path: 'bandeja', component: BandejaTasasComponent }], canActivate: [ AuthGaurdService ] },
  { path: 'clasificadores', children: [{ path: 'bandeja', component: BandejaClasificadorComponent }], canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaestrasRoutingModule { }
