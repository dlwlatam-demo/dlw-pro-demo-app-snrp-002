import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, map } from 'rxjs';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { Banco, Caja } from '../../../../../interfaces/combos.interface';

import { CuentaBancaria } from '../../../../../models/mantenimientos/maestras/cuenta-bancaria.model';

import { CuentaBancariaService } from '../../../../../services/mantenimientos/maestras/cuenta-bancaria.service';
import { CuentaContableService } from '../../../../../services/mantenimientos/maestras/cuenta-contable.service';
import { GeneralService } from '../../../../../services/general.service';

@Component({
  selector: 'app-nueva-cuenta',
  templateUrl: './nueva-cuenta.component.html',
  styleUrls: ['./nueva-cuenta.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class NuevaCuentaComponent implements OnInit {

  msg!: string;
  isLoading!: boolean;
  consultar!: boolean;
  loadingBancos: boolean = true;

  cuenta!: FormGroup;

  listaBancos!: Banco[];
  cuentaBancaria!: CuentaBancaria[];

  $bancos!: Observable< any >;
  $contables!: Observable< any >;
  $cajas!: Caja[];

  constructor( 
    private fb: FormBuilder,
    private router: Router,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private contableService: CuentaContableService,
    private cuentaService: CuentaBancariaService 
  ) { }

  ngOnInit(): void {

    const vista: string = this.config.data.vista;

    this._buscarBancos();
    this.$contables = this.contableService.getBandejaCuentaContable('', '', '');
    this.$cajas = this.generalService.getCbo_Caja();

    this.cuenta = this.fb.group({
      banco: ['', [ Validators.required ]],
      desCuenta: ['', [ Validators.required ]],
      desCorto: ['', [ Validators.required ]],
      nroCuenta: ['', [ Validators.required ]],
      cci: ['', [ Validators.required ]],
      prefijo: ['', [ Validators.required ]],
      nroIni: ['', [ Validators.required ]],
      nroFin: ['', [ Validators.required ]],
    });

    if ( vista == 'edit' ) {
      this.msg = 'actualizar';
      this.consultar = false;
      this.cuentaBancaria = this.config.data.cuenta as CuentaBancaria[];
      this.editCuenta();
    }
    else if ( vista == 'view' ) {
      this.consultar = true;
      this.cuentaBancaria = this.config.data.cuenta as CuentaBancaria[];
      this.editCuenta();
    }
    else {
      this.msg = 'crear';
    }
  }

  private _buscarBancos() {
    this.listaBancos = [];
    this.listaBancos.push({ nidInstitucion: 0, cdeInst: '(SELECCIONAR)' });

    this.generalService.getCbo_Bancos().subscribe({
      next: ( data: Banco[] ) => {
        this.loadingBancos = false;
        this.cuenta.get('banco')?.enable();

        this.listaBancos.push(...data);
      },
      error:( _err )=>{
        this.loadingBancos = false;
        this.cuenta.get('banco')?.enable();
      }
    });
  }

  editCuenta() {
    const idBanc = this.cuentaBancaria[0].idBanc;
    const nroCuenta = this.cuentaBancaria[0].nuCuenBanc;

    this.cuentaService.getCuentaBancariaById( Number( idBanc ), nroCuenta! ).subscribe({
      next: ( data ) => {
        console.log('data', data);
        this.goEdit( data as CuentaBancaria );
      }
    });
  }

  goEdit( d: CuentaBancaria ) {
    if ( !this.consultar ) {
      this.cuenta.get('banco')?.disable(),
      this.cuenta.get('nroCuenta')?.disable();
    }

    this.cuenta.get('banco')?.setValue( d.idBanc );
    this.cuenta.get('desCuenta')?.setValue( d.noCuenBanc );
    this.cuenta.get('desCorto')?.setValue( d.noCuenBancCort );
    this.cuenta.get('nroCuenta')?.setValue( d.nuCuenBanc );
    this.cuenta.get('cci')?.setValue( d.coCuenInte );
    this.cuenta.get('prefijo')?.setValue( d.nuPrefCartOrde );
    this.cuenta.get('nroIni')?.setValue( d.nuCheqInic );
    this.cuenta.get('nroFin')?.setValue( d.nuCheqFina );
  }

  saveOrUpdate() {

    const dataBancaria = {
      idBanc: this.cuenta.get('banco')?.value,
      nuCuenBanc: this.cuenta.get('nroCuenta')?.value,
      noCuenBanc: this.cuenta.get('desCuenta')?.value,
      noCuenBancCort: this.cuenta.get('desCorto')?.value,
      coCuenInte: this.cuenta.get('cci')?.value,
      nuPrefCartOrde: this.cuenta.get('prefijo')?.value,
      nuCheqInic: this.cuenta.get('nroIni')?.value,
      nuCheqFina: this.cuenta.get('nroFin')?.value,
    }

    console.log('dataBancaria', dataBancaria );

    this.confirmService.confirm({
      message: `¿Está Ud. seguro de ${ this.msg } el registro?`,
      accept: () => {
        this.isLoading = true;

        this.cuentaService.createOrUpdateCuenta( this.msg, dataBancaria ).subscribe({
          next: ( d ) => {

            if ( d.codResult < 0 ) {
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención!', 
                detail: d.msgResult,
                sticky: true
              });

              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('')
  }

  public validarNumero(event:KeyboardEvent) {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
