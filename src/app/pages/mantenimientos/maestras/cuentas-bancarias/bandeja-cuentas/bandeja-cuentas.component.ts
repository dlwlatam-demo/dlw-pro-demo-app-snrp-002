import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, map } from 'rxjs';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, ConfirmationService, MessageService } from 'primeng/api';

import { Estado } from '../../../../../interfaces/combos.interface';

import { CuentaBancaria } from '../../../../../models/mantenimientos/maestras/cuenta-bancaria.model';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';

import { CuentaBancariaService } from '../../../../../services/mantenimientos/maestras/cuenta-bancaria.service';
import { GeneralService } from '../../../../../services/general.service';

import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';

import { environment } from '../../../../../../environments/environment';
import { NuevaCuentaComponent } from '../nueva-cuenta/nueva-cuenta.component';

@Component({
  selector: 'app-bandeja-cuentas',
  templateUrl: './bandeja-cuentas.component.html',
  styleUrls: ['./bandeja-cuentas.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class BandejaCuentasComponent extends BaseBandeja implements OnInit {

  registros: any[] = [];
  
  cuentas!: FormGroup;

  $bancos!: Observable< any >;
  $estado!: Estado[];

  enviadosList!: CuentaBancaria[];

  currentPage: string = environment.currentPage;

  constructor( 
    private fb: FormBuilder,
    private router: Router,
    private confirmService: ConfirmationService,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private cuentaService: CuentaBancariaService 
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Cuentas_Bancarias;
    this.btnCuentasBancarias();

    let bancoItem: any = { cdeInst: '(TODOS)', nidInstitucion: 0 };
    let estadoItem: Estado = { deEstado: '(TODOS)', coEstado: '' };

    this.$bancos = this.generalService.getCbo_Bancos().pipe( 
      map( b => {
        b.unshift( bancoItem );
        return b;
      })
    );

    this.$estado = this.generalService.getCbo_Estado();
    this.$estado.push( estadoItem );

    this.cuentas = this.fb.group({
      banco: ['', [ Validators.required ]],
      nroCuenta: ['', [ Validators.required ]],
      estado: ['', [ Validators.required ]]
    });

    this.buscarCuenta();
  }

  buscarCuenta() {
    let banco = this.cuentas.get('banco')?.value;
    banco = banco == '' ? 0 : banco;
    
    const nroCuenta = this.cuentas.get('nroCuenta')?.value;

    let estado = this.cuentas.get('estado')?.value;
    if ( estado == '*' ) {
      estado = '';
    }

    this.buscarBandeja( banco, nroCuenta, estado );
  }

  buscarBandeja( banc: number, nro: string, est: string ) {
    Swal.showLoading();
    this.cuentaService.getBandejaCuentaBancaria( banc, nro, est ).subscribe({
      next: ( data ) => {
        for ( let d of data ) {
          if ( d.inRegi == 'A' )
            d.inRegi = 'Activo';
          else
            d.inRegi = 'Inactivo';
        }

        this.enviadosList = data;
      },
      error: ( e ) => {
        console.log( e );
        Swal.close()
        Swal.fire(
          'No se encuentran registros con los datos ingresados',
          'Cuentas Bancarias',
          'info'
        );

        this.enviadosList = [];
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  nuevaCuenta() {

    const dialog = this.dialogService.open( NuevaCuentaComponent, {
      header: 'Cuenta Bancaria - Nuevo',
      width: '60%',
      closable: false,
      data: {
        vista: '',
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha creado correctamente'
        });

        this.buscarCuenta();
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al crear su registro'
        });
      }
    });
  }

  editOrView( value: string ) {
    if ( !this.validateSelect( value ) )
      return;

    const dataDialog = {
      cuenta: this.selectedValues,
      vista: value
    }

    const title: string = value == 'edit' ? 'Editar' : 'Consultar';

    const dialog = this.dialogService.open( NuevaCuentaComponent, {
      header: `Cuenta Bancaria - ${ title }`,
      width: '60%',
      closable: false,
      data: dataDialog
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha actualizado correctamente'
        });

        this.buscarCuenta();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al actualizar su registro'
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  anular() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Inactivo' ) {
        Swal.fire('No puede anular un registro ya inactivo', '', 'warning');
        return;
      }

      this.registros.push({ 
        idBanc: registro.idBanc,
        nuCuenBanc: registro.nuCuenBanc 
      });
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea anular ${ msg1 } ${ msg2 }?`,
      accept: () => {
        Swal.showLoading();
        this.loadingAnu = true;

        const data = {
          trama: this.registros
        }

        this.cuentaService.anularCuentaBancaria( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se anularon los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al anular los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAnu = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscarCuenta();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  activar() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Activo' ) {
        Swal.fire('No puede habilitar un registro ya activo', '', 'warning');
        return;
      }

      this.registros.push({ 
        idBanc: registro.idBanc,
        nuCuenBanc: registro.nuCuenBanc 
      });
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea activar ${ msg1 } ${ msg2 }?`,
      accept: () => {
        Swal.showLoading();
        this.loadingAct = true;

        const data = {
          trama: this.registros
        }

        this.cuentaService.activarCuentaBancaria( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se activaron los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al activar los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAct = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscarCuenta();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
