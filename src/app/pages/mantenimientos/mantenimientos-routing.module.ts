import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../services/auth/auth-gaurd.service';

const routes: Routes = [
  { 
    path: 'configuracion',
    loadChildren: () => import('./configuracion/configuracion.module').then( m => m.ConfiguracionModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'documentos',
    loadChildren: () => import('./documentos/documentos.module').then( m => m.DocumentosModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'maestras',
    loadChildren: () => import('./maestras/maestras.module').then( m => m.MaestrasModule ),
    canActivate: [ AuthGaurdService ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MantenimientosRoutingModule { }
