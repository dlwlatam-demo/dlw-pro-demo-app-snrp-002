import { IDataFiltrosDocumentosVarios, IDocumentosVarios } from 'src/app/interfaces/documentos-varios.interface';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { UtilService } from 'src/app/services/util.service';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { BodyEditarDocumentosVarios } from 'src/app/models/mantenimientos/documentos/documentos-varios.models';
import { DocumentoVariosService } from 'src/app/services/mantenimientos/documentos/documentos-varios.service';
import { ArchivoAdjunto, TramaSustento } from '../../../../../models/archivo-adjunto.model';
import { UploadComponent } from '../../../../upload/upload.component';
import { environment } from '../../../../../../environments/environment';
import { GeneralService } from '../../../../../services/general.service';
import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { ValidatorsService } from '../../../../../core/services/validators.service';

@Component({
  selector: 'app-editar-documentos-varios',
  templateUrl: './editar-documentos-varios.component.html',
  styleUrls: ['./editar-documentos-varios.component.scss']
})
export class EditarDocumentosVariosComponent extends BaseBandeja implements OnInit, OnDestroy {
  loadingDocumentosVariosData: boolean = false;

  userCode:any;
  fechaActual: Date;
  idEdit!: number;
  sizeFiles: number = 0;
  msg!: string;
  files: ArchivoAdjunto[] = [];
  docuVariosSustento: TramaSustento[] = [];
  editarDocumentosVarios!: FormGroup;
  idBancCargo!: number;
  bodyEditarDocumentosVarios!: BodyEditarDocumentosVarios;
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoDocumento: IResponse2[] = [];
  unsubscribe$ = new Subject<void>();

  documentosVariosData: IDocumentosVarios;

  @ViewChild(UploadComponent) upload: UploadComponent;

  constructor( 
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private generalService: GeneralService,
    private documentoVariosService: DocumentoVariosService,
    public funciones: Funciones,
    private utilService: UtilService,
    private validateService: ValidatorsService,
    private sharingInformationService: SharingInformationService,
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Documentos_Varios_Solicitar_Firma;
    this.btnDocumentosVariosSolicitarFirma();

    this.bodyEditarDocumentosVarios = new BodyEditarDocumentosVarios();
    this.userCode = localStorage.getItem('user_code')||'';
    this._getIdByRoute();
    this.fechaActual = new Date();
    this.editarDocumentosVarios = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoDocumento: ['', [ Validators.required ]],
      fecha: [this.fechaActual, [ Validators.required ]],
      numero: ['', [ Validators.required ]],
      nombreRutaDocu: ['', [ Validators.required ]],
      concepto: ['', [ Validators.required ]],
      docs: this.fb.array([])
    });
  }

  private _getIdByRoute(): void {
    this.activatedRoute.params.pipe(takeUntil(this.unsubscribe$))
      .subscribe( params => this._getDocumentosVariosPagoById(params['id']))
  }

  private _getDocumentosVariosPagoById(id: string): void {
    this.loadingDocumentosVariosData = true;
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.documentoVariosService.getDocumentoVariosById(id).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          console.log('editData', data)
          this.documentosVariosData = data;
          this._inicilializarEditarCartaOrden();
          this.upload.sustentos = this.docuVariosSustento;
          this.upload.goEdit();
          this.loadingDocumentosVariosData = false;
          this.utilService.onCloseLoading(); 
        },
        error: ( err ) => {
          this.loadingDocumentosVariosData = false;  
          this.utilService.onCloseLoading(); 
        }
      })
  }

  private _inicilializarEditarCartaOrden() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaZonaRegistral.push({ coZonaRegi: this.documentosVariosData.coZonaRegi, deZonaRegi: this.documentosVariosData.deZonaRegi });
    this.listaOficinaRegistral.push({ coOficRegi: this.documentosVariosData.coOficRegi, deOficRegi: this.documentosVariosData.deOficRegi });
    this.listaLocal.push({ coLocaAten: this.documentosVariosData.coLocaAten, deLocaAten: this.documentosVariosData.deLocaAten }); 
    this.listaTipoDocumento.push({ ccodigoHijo: this.documentosVariosData.tiDocu, cdescri: this.documentosVariosData.deTipoDocu }); 
    this.editarDocumentosVarios.get('numero')?.setValue( this.documentosVariosData.nuDocu);
    // this.editarDocumentosVarios.get('nombreDocumento')?.setValue( this.documentosVariosData.noDocuPdf);
    this.editarDocumentosVarios.get('concepto')?.setValue( this.documentosVariosData.obDocu);
    this.editarDocumentosVarios.patchValue({ "fecha": new Date(this.documentosVariosData.feDocu)});

    this.docuVariosSustento = [{
      id: 0,
      noDocuSust: this.documentosVariosData.noDocuPdf,
      noRutaSust: this.documentosVariosData.noRutaDocuPdf,
      idGuidDocu: this.documentosVariosData.idGuidDocu
    }];

    this.bodyEditarDocumentosVarios.noDocuPdf = this.documentosVariosData.noDocuPdf;
    this.bodyEditarDocumentosVarios.idGuidDocu = this.documentosVariosData.idGuidDocu;
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  public validarMonto(event:any){
    const pattern = /^(-?\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public formatearNumero(event:any){
    if (event.target.value) {
      this.editarDocumentosVarios.get('numero')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
    }
  }

  public grabar() {
    let resultado: boolean = this.validarFormulario();
    if (!resultado) {
      return;
    }

    this.parse();
    console.log('this.files =', this.files);

    if ( this.files.length != 0 ) {
      this.saveAdjuntoInFileServer();
    }
    else {
      this.guardarDatosEdit();
    }
    
  }

  private async guardarDatosEdit() {
    const dataFiltrosDocumentosVarios: IDataFiltrosDocumentosVarios = await this.sharingInformationService.compartirDataFiltrosDocumentosVariosObservable.pipe(first()).toPromise(); 
    if (dataFiltrosDocumentosVarios.esBusqueda) {
      dataFiltrosDocumentosVarios.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosDocumentosVariosObservableData = dataFiltrosDocumentosVarios;
    }
    this.utilService.onShowProcessLoading("Creando nuevo documento");
    const fecha = this.editarDocumentosVarios.get('fecha')?.value;
    const fechaFormat = this.validateService.formatDate( fecha );
    this.bodyEditarDocumentosVarios.idDocuVari = this.documentosVariosData.idDocuVari;
    this.bodyEditarDocumentosVarios.coZonaRegi = this.documentosVariosData.coZonaRegi;
    this.bodyEditarDocumentosVarios.coOficRegi = this.documentosVariosData.coOficRegi;
    this.bodyEditarDocumentosVarios.coLocaAten = this.documentosVariosData.coLocaAten;
    this.bodyEditarDocumentosVarios.tiDocu = this.documentosVariosData.tiDocu;
    this.bodyEditarDocumentosVarios.feDocu = fechaFormat;
    this.bodyEditarDocumentosVarios.nuDocu = this.editarDocumentosVarios.get('numero')?.value;
    // this.bodyEditarDocumentosVarios.noDocuPdf = this.editarDocumentosVarios.get('nombreDocumento')?.value;
    this.bodyEditarDocumentosVarios.obDocu = this.editarDocumentosVarios.get('concepto')?.value;
    console.log('dataEdit', this.bodyEditarDocumentosVarios);
    this.documentoVariosService.editarDocumentoVarios(this.bodyEditarDocumentosVarios).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( d ) => {
          if (d.codResult < 0 ) {
            this.utilService.onShowAlert("info", "Atención", d.msgResult);
          } else if (d.codResult === 0) {
            this.utilService.onCloseLoading();
            this.router.navigate(['SARF/mantenimientos/documentos/documentos_varios_solicitar_firma/bandeja']);
          }
        },
        error:( d )=>{
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("error", "Atención", d.error.description);
          }else {
            this.utilService.onShowMessageErrorSystem(); 
          }
        }
      });
  }

  private validarFormulario(): boolean {   
    if (this.editarDocumentosVarios.value.fecha === "" || this.editarDocumentosVarios.value.fecha === null) {                    
      document.getElementById('idFecha').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de nuevo documento.");
      return false;      
    } else if ( (this.editarDocumentosVarios.get('docs') as FormArray).length == 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe adjuntar un documento.");
      return false;
    } else {
      return true;
    }       
  }

  public async cancelar() {
    const dataFiltrosDocumentosVarios: IDataFiltrosDocumentosVarios = await this.sharingInformationService.compartirDataFiltrosDocumentosVariosObservable.pipe(first()).toPromise(); 
    if (dataFiltrosDocumentosVarios.esBusqueda) {
      dataFiltrosDocumentosVarios.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosDocumentosVariosObservableData = dataFiltrosDocumentosVarios;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/documentos_varios_solicitar_firma/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaDocumentosVariosObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();   
  }

  disableUpload(): boolean {
    return (this.editarDocumentosVarios.get('docs') as FormArray).length == environment.fileLimitAdju;
  }

  addUploadedFile(file: any) {
    (this.editarDocumentosVarios.get('docs') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.editarDocumentosVarios.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.docuVariosSustento.findIndex( f => f.id == fileId );
    this.docuVariosSustento.splice( index, 1 );
  }

  parse() {
    const docs = (this.editarDocumentosVarios.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( idAdjunto != 0 )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: '999',
          coZonaRegi: this.documentosVariosData.coZonaRegi,
          coOficRegi: this.documentosVariosData.coOficRegi
        })
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.utilService.onShowAlert("info", "Atención", data.msgResult);
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            this.addAdjunto(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.guardarDatosEdit();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          if (e.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("error", "Atención", e.error.description);
          }else {
            this.utilService.onShowMessageErrorSystem(); 
          }
        }
      });
    }
  }

  addAdjunto(file?: any, guid?: string) {
    if ( file != '' && guid != '' ) {
      this.bodyEditarDocumentosVarios.noDocuPdf = file.deDocuAdju;
      this.bodyEditarDocumentosVarios.idGuidDocu = guid;
    }
  }

}
