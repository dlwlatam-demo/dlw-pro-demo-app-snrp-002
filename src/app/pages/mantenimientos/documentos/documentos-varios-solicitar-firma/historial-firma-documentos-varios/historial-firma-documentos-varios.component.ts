import { Component, OnInit, OnDestroy} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { UtilService } from 'src/app/services/util.service';
import { IDocumentosVarios, IHistorialFirmasDocumentosVarios } from 'src/app/interfaces/documentos-varios.interface';
import { DocumentoVariosService } from 'src/app/services/mantenimientos/documentos/documentos-varios.service';

@Component({
  selector: 'app-historial-firma-documentos-varios',
  templateUrl: './historial-firma-documentos-varios.component.html',
  styleUrls: ['./historial-firma-documentos-varios.component.scss']
})
export class HistorialFirmaDocumentosVariosComponent implements OnInit, OnDestroy {
  unsubscribe$ = new Subject<void>();
  datos!: FormGroup;
  dataDocumentosVarios: IDocumentosVarios;
  loadingHistorialFirmas: boolean = false;
  listaHistorialFirmas: IHistorialFirmasDocumentosVarios[] = [];

  constructor( private fb: FormBuilder,
               private config: DynamicDialogConfig,
               private dialogRef: DynamicDialogRef,
               private funciones: Funciones,  
               private utilService: UtilService,
               private documentoVariosService: DocumentoVariosService ) { }

  ngOnInit(): void {
    this.dataDocumentosVarios = this.config.data;
    const formatfecha = this._customFecha();
    this.datos = this.fb.group({
      tipoDocumento: [this.dataDocumentosVarios.deTipoDocu, [ Validators.required ]],
      fecha: [formatfecha, [ Validators.required ]]
    });
    this._getHistoricalFirmas();
  }

  private _customFecha(): string {
    const fecha = new Date(this.dataDocumentosVarios.feDocu);
    const fechaFormat = (fecha !== null) ? this.funciones.convertirFechaDateToString(fecha) : '';
    return fechaFormat;
  }

  private _getHistoricalFirmas(): void {
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.loadingHistorialFirmas = true;
    this.documentoVariosService.historialFirmasDocumentoVarios(this.dataDocumentosVarios.idDocuFirm).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.loadingHistorialFirmas = false;
          this.listaHistorialFirmas = data;
          this.utilService.onCloseLoading();  
        },
        error:( d )=>{
          this.loadingHistorialFirmas = false;
          this.listaHistorialFirmas = [];
          this.utilService.onCloseLoading();
        }
      })
  }

  regresar() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();  
  }

}
