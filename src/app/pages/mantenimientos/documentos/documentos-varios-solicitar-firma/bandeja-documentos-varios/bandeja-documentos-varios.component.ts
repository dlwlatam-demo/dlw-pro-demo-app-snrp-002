import { UtilService } from '../../../../../services/util.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import { GeneralService } from '../../../../../services/general.service';
import { Local, OficinaRegistral, ZonaRegistral } from '../../../../../interfaces/combos.interface';
import { IResponse2 } from '../../../../../interfaces/general.interface';
import { Funciones } from '../../../../../core/helpers/funciones/funciones';
import { SharingInformationService } from '../../../../../core/services/sharing-services.service';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { IBandejaProcess } from '../../../../../interfaces/global.interface';
import { BodyAnularActivarDocumentosVarios, BodyDocumentosVarios } from '../../../../../models/mantenimientos/documentos/documentos-varios.models';
import { DocumentoVariosService } from '../../../../../services/mantenimientos/documentos/documentos-varios.service';
import { IDataFiltrosDocumentosVarios, IDocumentosVarios } from '../../../../../interfaces/documentos-varios.interface';
import { SolicitarFirmasDocumentosVariosComponent } from '../solicitar-firmas-documentos-varios/solicitar-firmas-documentos-varios.component';
import { HistorialFirmaDocumentosVariosComponent } from '../historial-firma-documentos-varios/historial-firma-documentos-varios.component';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { SustentosDocumentosVariosComponent } from '../sustentos-documentos-varios/sustentos-documentos-varios.component';
import { ValidatorsService } from '../../../../../core/services/validators.service';

@Component({
  selector: 'app-bandeja-documentos-varios',
  templateUrl: './bandeja-documentos-varios.component.html',
  styleUrls: ['./bandeja-documentos-varios.component.scss'],
  providers: [
    DialogService,
    MessageService
  ]
})
export class BandejaDocumentosVariosComponent extends BaseBandeja implements OnInit, OnDestroy {
  userCode:any
  unsubscribe$ = new Subject<void>();
  
  loadingZonaRegistral: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  loadingTipoDocumento: boolean = false;
  loadingEstado: boolean = false;

  reporteExcel!: string;

  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoDocumento: IResponse2[] = [];
  listaEstadoRecibo: IResponse2[] = [];

  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaTipoDocumentoFiltro: IResponse2[];
  listaEstadoReciboFiltro: IResponse2[];
  esBusqueda: boolean = false;
  fechaMinima: Date;
  fechaMaxima: Date; 
  documentosVarios!: FormGroup;
  currentPage: string = environment.currentPage;
  bodyDocumentosVarios: BodyDocumentosVarios;
  bodyAnularActivarDocumentosVarios: BodyAnularActivarDocumentosVarios;
  documentosVariosList: IDocumentosVarios[];
  documentosVariosProceso: IBandejaProcess = {
    idMemo: null,
    proceso: ''
  };
  override selectedValues: IDocumentosVarios[] = [];

  constructor( 
    private fb: FormBuilder,
    private router: Router,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private utilService: UtilService,
    private funciones: Funciones,
    private validateService: ValidatorsService,
    private documentoVariosService: DocumentoVariosService,
    private sharingInformationService: SharingInformationService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Documentos_Varios_Solicitar_Firma;
    this.btnDocumentosVariosSolicitarFirma();
    
    this.bodyDocumentosVarios = new BodyDocumentosVarios();
    this.bodyAnularActivarDocumentosVarios = new BodyAnularActivarDocumentosVarios();
    this.userCode = localStorage.getItem('user_code')||'';
    this.fechaMaxima = new Date();
    this.fechaMinima = new Date(this.fechaMaxima.getFullYear(), this.fechaMaxima.getMonth(), 1);
    this.documentosVarios = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoDocu: [''],
      fecDes: [this.fechaMinima],
      fecHas: [this.fechaMaxima],
      numero: [''],
      usuario: [''],
      estado: [''],
    })
    this._dataFiltrosDocumentosVarios();
  }

  private _buscarZonaRegistral(): void {
    this.loadingZonaRegistral = true;
    this.generalService.getCbo_Zonas_Usuario(this.userCode).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingZonaRegistral = false;    
        if (data.length === 1) {
          this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });    
          this.documentosVarios.patchValue({ "zona": data[0].coZonaRegi});
          this.buscarOficinaRegistral();
        } else {
          this.listaZonaRegistral.push(...data);
        }
      },
      error:( err )=>{
        this.loadingZonaRegistral = false;    
      }
    });
  }

  public buscarOficinaRegistral(): void {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.selectedValues = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.documentosVarios.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.documentosVarios.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.documentosVarios.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.documentosVarios.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.documentosVarios.get('zona')?.value,this.documentosVarios.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.documentosVarios.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  private _buscarTipoDocumento() {
    this.loadingTipoDocumento = true;
    this.generalService.getCbo_TipoDocumentosVarios().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        // if (data.length === 1) {
        //   this.listaTipoDocumento.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
        //   this.documentosVarios.patchValue({ "tipoDocu": data[0].ccodigoHijo});        
        // } else {    
        // }
        this.listaTipoDocumento.push(...data);
        this.loadingTipoDocumento = false;
      },
      error:( err )=>{
        this.loadingTipoDocumento = false;    
      }
    });
  }

  public cambioFechaInicio() {   
    if (this.documentosVarios.get('fecHas')?.value !== '' && this.documentosVarios.get('fecHas')?.value !== null) {
      if (this.documentosVarios.get('fecDes')?.value > this.documentosVarios.get('fecHas')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.documentosVarios.controls['fecDes'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.documentosVarios.get('fecDes')?.value !== '' && this.documentosVarios.get('fecDes')?.value !== null) {
      if (this.documentosVarios.get('fecHas')?.value < this.documentosVarios.get('fecDes')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.documentosVarios.controls['fecHas'].reset();    
      }      
    }
  }

  private _buscarEstado(): void {
    this.loadingEstado = true;
    this.generalService.getCbo_EstadoDocumentosVarios().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        if (data.length === 1) {
          this.listaEstadoRecibo.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
          this.documentosVarios.patchValue({ "estado": data[0].ccodigoHijo});         
        } else {    
          this.listaEstadoRecibo.push(...data);
        }
        this.loadingEstado = false;    
      },
      error:( err )=>{
        this.loadingEstado = false;    
      }
    });
  }

  private _dataFiltrosDocumentosVarios(): void {
    this.sharingInformationService.compartirDataFiltrosDocumentosVariosObservable.pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: IDataFiltrosDocumentosVarios) => {
      if (data.esCancelar && data.bodyDocumentosVarios !== null) {      
        this.utilService.onShowProcessLoading("Cargando la data");
        this._setearCamposFiltro(data);
        this.buscarDocumentosVarios(false);
      } else {
        this._inicilializarListas();
        this._buscarZonaRegistral();
        this._buscarTipoDocumento();
        this._buscarEstado();
        this.buscarDocumentosVarios(true);
      }
    });
  }

  private _setearCamposFiltro(dataFiltro: IDataFiltrosDocumentosVarios): void {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaEstadoRecibo = [];
    this.listaZonaRegistral = dataFiltro.listaZonaRegistral; 
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaTipoDocumento = dataFiltro.listaTipoDocumento;
    this.listaEstadoRecibo = dataFiltro.listaEstadoRecibo;
    this.bodyDocumentosVarios = dataFiltro.bodyDocumentosVarios;
    this.documentosVarios.patchValue({ "zona": this.bodyDocumentosVarios.coZonaRegi});
    this.documentosVarios.patchValue({ "oficina": this.bodyDocumentosVarios.coOficRegi});
    this.documentosVarios.patchValue({ "local": this.bodyDocumentosVarios.coLocaAten});
    this.documentosVarios.patchValue({ "tipoDocu": this.bodyDocumentosVarios.tiDocu});
    this.documentosVarios.patchValue({ "fecDes": (this.bodyDocumentosVarios.feDesd !== '' && this.bodyDocumentosVarios.feDesd !== null) ? this.funciones.convertirFechaStringToDate(this.bodyDocumentosVarios.feDesd) : this.bodyDocumentosVarios.feDesd});
    this.documentosVarios.patchValue({ "fecHas": (this.bodyDocumentosVarios.feHast !== '' && this.bodyDocumentosVarios.feHast !== null) ? this.funciones.convertirFechaStringToDate(this.bodyDocumentosVarios.feHast) : this.bodyDocumentosVarios.feHast});
    this.documentosVarios.patchValue({ "numero": this.bodyDocumentosVarios.idDocuVari});
    this.documentosVarios.patchValue({ "usuario": this.bodyDocumentosVarios.noUsuaCrea});
    this.documentosVarios.patchValue({ "estado": this.bodyDocumentosVarios.esDocu});
  }

  private _inicilializarListas(): void {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaEstadoRecibo = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    this.listaTipoDocumento.push({ ccodigoHijo: '', cdescri: '(TODOS)' }); 
    this.listaEstadoRecibo.push({ ccodigoHijo: '', cdescri: '(TODOS)' });   
  }

  public buscarDocumentosVarios(esCargaInicial: boolean): void {
    const fechaDesde = this.documentosVarios.get('fecDes')?.value;
    const fechaHasta = this.documentosVarios.get('fecHas')?.value;
    if (!fechaDesde && fechaHasta) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha DESDE');  
      return;     
    }
    if (!fechaHasta && fechaDesde) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha HASTA');   
      return;     
    }
    this.guardarListadosParaFiltro();
    this.esBusqueda = (esCargaInicial) ? false : true;    
    this.utilService.onShowProcessLoading("Cargando la data"); 
    const zona = this.documentosVarios.get('zona')?.value;
    const oficina = this.documentosVarios.get('oficina')?.value;
    const local = this.documentosVarios.get('local')?.value;
    const fechaDesdeFormat = (fechaDesde !== "" && fechaDesde !== null) ? this.funciones.convertirFechaDateToString(fechaDesde) : '';
    const fechaHastaFormat = (fechaHasta !== "" && fechaHasta !== null) ? this.funciones.convertirFechaDateToString(fechaHasta) : '';
    const tipoDocu = this.documentosVarios.get('tipoDocu')?.value;
    const estadoDocumento = this.documentosVarios.get('estado')?.value;
    this.bodyDocumentosVarios.coZonaRegi = zona === '*' ? '' : zona;
    this.bodyDocumentosVarios.coOficRegi = oficina === '*' ? '' : oficina;
    this.bodyDocumentosVarios.coLocaAten = local === '*' ? '' : local;
    this.bodyDocumentosVarios.tiDocu = tipoDocu === '*' ? '' : tipoDocu;
    this.bodyDocumentosVarios.idDocuVari = this.documentosVarios.get('numero')?.value;
    this.bodyDocumentosVarios.feDesd = fechaDesdeFormat;
    this.bodyDocumentosVarios.feHast = fechaHastaFormat;
    this.bodyDocumentosVarios.esDocu = estadoDocumento === '*' ? '' : estadoDocumento;
    this.bodyDocumentosVarios.noUsuaCrea = this.documentosVarios.get('usuario')?.value;

    this.documentoVariosService.getBandejaDocumentoVarios(this.bodyDocumentosVarios).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.documentosVariosList = data.lstDocumentoVarios;
          this.reporteExcel = data.reporteExcel;
          this.selectedValues = [];
          this.utilService.onCloseLoading();  
        },
        error:( d )=>{
          this.documentosVariosList = [];
          this.selectedValues = [];
          this.utilService.onCloseLoading();  
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("warning", "Atención", d.error.description);        
          }else {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      });
  }

  private guardarListadosParaFiltro(): void {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaTipoDocumentoFiltro = [...this.listaTipoDocumento];
    this.listaEstadoReciboFiltro = [...this.listaEstadoRecibo];
  }

  public nuevoDocumentosVarios(): void {    
    this.documentosVariosProceso.idMemo = null;
    this.documentosVariosProceso.proceso = 'nuevo';    
    this.sharingInformationService.compartirComprobantePagoProcesoObservableData = this.documentosVariosProceso;
    this.router.navigate(['SARF/mantenimientos/documentos/documentos_varios_solicitar_firma/nuevo']);
    this.sharingInformationService.irRutaBandejaDocumentosVariosObservableData = false;
  }

  public editarDocumentosVarios(): void {
    console.log("this.selectedValues", this.selectedValues);
    
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
      return;
    }

    if ( this.selectedValues[0].deEsta === 'ANULADO' || this.selectedValues[0].deEsta === 'RECHAZADO' || this.selectedValues[0].deEsta === 'PENDIENTE DE FIRMA' ) {
      this.utilService.onShowAlert("warning", "Atención", "No puede editar un registro ANULADO, RECHAZADO o PENDIENTE DE FIRMA");
      return;
    }

    this.documentosVariosProceso.idMemo = this.selectedValues[0].idDocuVari;
    this.documentosVariosProceso.proceso  = 'editar'; 
    this.sharingInformationService.compartirComprobantePagoProcesoObservableData = this.documentosVariosProceso
    this.router.navigate([`SARF/mantenimientos/documentos/documentos_varios_solicitar_firma/editar/${ this.selectedValues[0].idDocuVari}`])
    this.sharingInformationService.irRutaBandejaDocumentosVariosObservableData = false;
  }

  public verDocumento() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro.");
      return;
    }

    if ( this.selectedValues?.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ver el documento de un registro a la vez.");
      return;
    }

    const blobUrl = this.generalService.downloadManager( this.selectedValues[0].idGuidDocu );
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute("target", "_blank");
    document.body.appendChild( download );
    download.click();
  }

  public solicitarFirma() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para solicitar firma.");
      return;
    }

    if ( this.selectedValues?.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede solicitar firma de un registro a la vez.");
      return;
    }

    if ( this.selectedValues[0].esDocu !== '001' ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede solicitar firma un registro con estado registrado.");
      return;
    }
    const dialog = this.dialogService.open( SolicitarFirmasDocumentosVariosComponent, {
      header: 'Documentos varios - Solicitar firma',
      width: '40%',
      data: this.selectedValues[0]
    });

    dialog.onClose.subscribe( (data) => {
      data && this.buscarDocumentosVarios(false);
      this.selectedValues = [];
    });
  }

  historialFirmas() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para ver historial de firmas.");
      return;
    }

    if ( this.selectedValues?.length !== 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ver historial de firmas de un registro a la vez.");
      return;
    }

    if ( this.selectedValues[0].deEsta !== 'PENDIENTE DE FIRMA' && this.selectedValues[0].deEsta !== 'RECHAZADO' && this.selectedValues[0].deEsta !== 'FIRMADO') {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ver historial de firmas de un registro en estado PENDIENTE DE FIRMA, RECHAZADO o FIRMADO.");
      return;
    }

    const dialog = this.dialogService.open( HistorialFirmaDocumentosVariosComponent, {
      header: 'Documentos varios - Historial de Firmas',
      width: '50%',
      data: this.selectedValues[0]
    });

    dialog.onClose.subscribe( () => {
      this.selectedValues = [];
    });
  }

  public sustentos(): void {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para ver los sustentos.");
      return;
    }

    if ( this.selectedValues?.length !== 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ver los sustentos de un registro a la vez.");
      this.selectedValues = [];
      return;
    }

    const dialog = this.dialogService.open( SustentosDocumentosVariosComponent, {
      header: 'Documentos Varios Solicitar Firma - Sustentos',
      width: '45%',
      closable: false,
      data: {
        varios: this.selectedValues,
        add: this.BtnAdicionar_Sustento,
        delete: this.BtnEliminar_Sustento,
        bandeja: this.codBandeja
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Los sustentos han sido cargados y grabados correctamente'
        });

        this.buscarDocumentosVarios(false);
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al cargar y grabar los sustentos'
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  public anular(): void {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
      return;
    }
    
    const idCartasOrdenes = this.selectedValues?.map( value => {
      const trama = {
        idDocuVari: value.idDocuVari?.toString()
      }
        return trama
    });
    const textoAnular = (this.selectedValues?.length === 1) ? 'el registro' : 'los registros';
    this.utilService.onShowConfirm(`¿Estás seguro que desea anular ${textoAnular}?`).then((result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('anular'); 
        this.bodyAnularActivarDocumentosVarios.trama = idCartasOrdenes;
        this.documentoVariosService.anularDocumentoVarios(this.bodyAnularActivarDocumentosVarios).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0 ) {
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
            } else if (d.codResult === 0 ) {
              this.selectedValues = [];
              this.buscarDocumentosVarios(false);
            }
          },

          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", d.error.description);        

            }else {
              this.utilService.onShowMessageErrorSystem();        
            }
          }
        });
        
      } else if (result.isDenied) {
        this.utilService.onShowAlert("warning", "Atención", "No se pudieron guardar los cambios.");
      }
    })
  }

  public activar(): void {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
      return;
    }
    const idCartasOrdenes = this.selectedValues?.map( value => {
      const trama = {
        idDocuVari: value.idDocuVari?.toString()
      }
        return trama
    });
    const textoActivar = (this.selectedValues?.length === 1) ? 'el registro' : 'los registros';
    this.utilService.onShowConfirm(`¿Estás seguro que desea activar ${textoActivar}?`).then((result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('activar'); 
        this.bodyAnularActivarDocumentosVarios.trama = idCartasOrdenes;
        this.documentoVariosService.activarDocumentoVarios(this.bodyAnularActivarDocumentosVarios).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0) {
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
            } else if (d.codResult === 0) {
              this.selectedValues = [];
              this.buscarDocumentosVarios(false);
            }
          },
          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              Swal.fire( 'Carta orden', `${d.error.description}`, 'info');
              this.utilService.onShowAlert("error", "Atención", d.error.description);        
            }else {
              this.utilService.onShowMessageErrorSystem();        
            }
          }
        });
        
      } else if (result.isDenied) {
        this.utilService.onShowAlert("warning", "Atención", "No se pudieron guardar los cambios.");
      }
    });
  }

  public consultar(): void {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para consultarlo.");
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede consultar un registro a la vez.");
      return;
    }
    this.documentosVariosProceso.idMemo = this.selectedValues[0].idDocuVari;
    this.documentosVariosProceso.proceso = 'consultar';        
    this.sharingInformationService.compartirComprobantePagoProcesoObservableData = this.documentosVariosProceso;
    this.router.navigate([`SARF/mantenimientos/documentos/documentos_varios_solicitar_firma/consultar/${ this.selectedValues[0].idDocuVari }`])
    this.sharingInformationService.irRutaBandejaDocumentosVariosObservableData = false;
  }

  exportExcel() {
    if ( this.reporteExcel === null || this.reporteExcel === 'null' ) {
      return;
    }

    const fechaHoy = new Date();
    const fileName = this.validateService.formatDateWithHoursForExcel( fechaHoy );;

    const byteCharacters = atob(this.reporteExcel);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', `DocumentosVarios_${ fileName }`);
    document.body.appendChild( download );
    download.click();
  }

  public customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  private _establecerDataFiltrosDocumentosVarios() : IDataFiltrosDocumentosVarios {
    const dataFiltrosRecibos: IDataFiltrosDocumentosVarios = {
      listaZonaRegistral: this.listaZonaRegistralFiltro, 
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaTipoDocumento: this.listaTipoDocumentoFiltro,
      listaEstadoRecibo: this.listaEstadoReciboFiltro,
      bodyDocumentosVarios: this.bodyDocumentosVarios,   
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosRecibos;
  }

  public validarNumero(event:KeyboardEvent): void{
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.sharingInformationService.compartirDataFiltrosDocumentosVariosObservableData = this._establecerDataFiltrosDocumentosVarios(); 
  }

}
