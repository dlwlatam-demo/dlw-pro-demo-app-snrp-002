import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import { DocumentoFirmar } from '../../../../../models/mantenimientos/documentos/documento-firma.model';

import { GeneralService } from '../../../../../services/general.service';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { DocumentoVariosService } from 'src/app/services/mantenimientos/documentos/documentos-varios.service';
import { BodyGrabarFirmasDocumentosVarios, BodySolicitarFirmasDocumentosVarios, ITramaSolicitarFirmas } from 'src/app/models/mantenimientos/documentos/documentos-varios.models';
import { IDocumentosVarios } from 'src/app/interfaces/documentos-varios.interface';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-solicitar-firmas-documentos-varios',
  templateUrl: './solicitar-firmas-documentos-varios.component.html',
  styleUrls: ['./solicitar-firmas-documentos-varios.component.scss']
})
export class SolicitarFirmasDocumentosVariosComponent implements OnInit, OnDestroy {
  unsubscribe$ = new Subject<void>();
  solicitarFirmas!: FormGroup;
  bodySolicitarFirmasDocumentosVarios: BodySolicitarFirmasDocumentosVarios;
  bodyGrabarFirmasDocumentosVarios: BodyGrabarFirmasDocumentosVarios;
  dataDocumentosVarios: IDocumentosVarios;
  loadingFirma: boolean = false;
  loadingSolicitarFirmas: boolean = false;
  loadingGrabarFirmas: boolean = false;

  listaFirma01: IResponse2[] = [];
  listaFirma02: IResponse2[];
  listaFirma03: IResponse2[];
  dataSolicitarFirmas: any;
  dataGrabrarFirmas: any;

  documento!: DocumentoFirmar[];

  constructor( private fb: FormBuilder,
               private config: DynamicDialogConfig,
               private dialogRef: DynamicDialogRef,
               private generalService: GeneralService,
               private utilService: UtilService,
               private documentoVariosService: DocumentoVariosService ) { }

  ngOnInit(): void {
    this.bodySolicitarFirmasDocumentosVarios = new BodySolicitarFirmasDocumentosVarios();
    this.bodyGrabarFirmasDocumentosVarios = new BodyGrabarFirmasDocumentosVarios();
    this.dataDocumentosVarios = this.config.data;
    this.solicitarFirmas = this.fb.group({
      firma01: ['', [ Validators.required ]],
      firma02: [''],
      firma03: [''],
    });
    this._inicilializarListas();
    this._getListaPerfil();
  }

  private _inicilializarListas() {
    this.listaFirma01 = [];
    this.listaFirma02 = [];
    this.listaFirma03 = [];
    this.listaFirma01.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' });
    this.listaFirma02.push({ ccodigoHijo: '*', cdescri: '(NINGUNO)' });
    this.listaFirma03.push({ ccodigoHijo: '*', cdescri: '(NINGUNO)' }); 
    this.solicitarFirmas.get('firma02')?.disable();
    this.solicitarFirmas.get('firma03')?.disable();
  }

  private _getListaPerfil(): void {
    this.loadingFirma = true;
    this.generalService.getCbo_PerfilDocumentosVarios().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.loadingFirma = false;
          this.listaFirma01.push(...data);
          // this.listaFirma02.push(...data);
          // this.listaFirma03.push(...data);
        },
        error:( err )=>{
          this.loadingFirma = false;    
        }
      });
  }

  public cambioFirma01(event: any): void {
    this.listaFirma02 = [];
    this.listaFirma03 = [];
    this.listaFirma02.push({ ccodigoHijo: '*', cdescri: '(NINGUNO)' });
    this.listaFirma03.push({ ccodigoHijo: '*', cdescri: '(NINGUNO)' });
    if (event.value === '*') {
      this.solicitarFirmas.get('firma02')?.disable();
      this.solicitarFirmas.get('firma03')?.disable();
    } else {
      this.solicitarFirmas.get('firma02')?.enable();
      this.listaFirma01.map(value => {
        if (value.ccodigoHijo !== event.value && value.ccodigoHijo !== '*') {
          this.listaFirma02.push(value);
        }
      })
    }
  }

  public cambioFirma02(event: any): void {
    this.listaFirma03 = [];
    this.listaFirma03.push({ ccodigoHijo: '*', cdescri: '(NINGUNO)' });
    if (event.value === '*') {
      this.solicitarFirmas.get('firma03')?.disable();
    } else {
      this.solicitarFirmas.get('firma03')?.enable();
      this.listaFirma02.map(value => {
        if (value.ccodigoHijo !== event.value && value.ccodigoHijo !== '*') {
          this.listaFirma03.push(value);
        }
      })
    }
  }

  public solicitarFirma(): void {
    const firma01 = this.solicitarFirmas.get('firma01')?.value;
    const firma02 = this.solicitarFirmas.get('firma02')?.value;
    const firma03 = this.solicitarFirmas.get('firma03')?.value;

    if (firma01 !== '' && firma01 !== '*') {
      const firma01Obj: ITramaSolicitarFirmas = {
        coPerf: firma01,
        nuSecuFirm: '1'
      };
      this.bodySolicitarFirmasDocumentosVarios.trama = [firma01Obj];
    }

    if (firma02 !== '' && firma02 !== '*') {
      const firma02Obj: ITramaSolicitarFirmas = {
        coPerf: firma02,
        nuSecuFirm: '2'
      };
      this.bodySolicitarFirmasDocumentosVarios.trama.push(firma02Obj);
    }

    if (firma03 !== '' && firma03 !== '*') {
      const firma03Obj: ITramaSolicitarFirmas = {
        coPerf: firma03,
        nuSecuFirm: '3'
      };
      this.bodySolicitarFirmasDocumentosVarios.trama.push(firma03Obj);
    }
    
    this.bodySolicitarFirmasDocumentosVarios.idDocuVari = this.dataDocumentosVarios.idDocuVari;
    this.documentoVariosService.solicitarFirmasDocumentoVarios(this.bodySolicitarFirmasDocumentosVarios).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.loadingSolicitarFirmas = false;
          if (data.codResult < 0 ) {
            this.utilService.onShowAlert("info", "Atención", data.msgResult);
          } else if (data.codResult === 0) {
            this.dataSolicitarFirmas = data;
            this._grabarSolicitarFirmas();
          }
        },
        error:( d )=>{
          this.loadingSolicitarFirmas = false;
          this.utilService.onCloseLoading(); 
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("warning", "Atención", d.error.description);        
          }else {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      })
  }

  private _grabarSolicitarFirmas(): void {
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.bodyGrabarFirmasDocumentosVarios.tiDocu = this.dataDocumentosVarios.tiDocu;
    this.bodyGrabarFirmasDocumentosVarios.idDocuOrig = this.dataDocumentosVarios.idDocuVari;
    this.bodyGrabarFirmasDocumentosVarios.noDocuPdf = this.dataDocumentosVarios.noDocuPdf;
    this.bodyGrabarFirmasDocumentosVarios.noRutaDocuPdf = this.dataDocumentosVarios.noRutaDocuPdf;
    this.bodyGrabarFirmasDocumentosVarios.idGuidDocu = this.dataDocumentosVarios.idGuidDocu;
    this.bodyGrabarFirmasDocumentosVarios.coZonaRegi = this.dataDocumentosVarios.coZonaRegi;
    this.bodyGrabarFirmasDocumentosVarios.coOficRegi = this.dataDocumentosVarios.coOficRegi;
    this.bodyGrabarFirmasDocumentosVarios.coLocaAten = this.dataDocumentosVarios.coLocaAten;
    this.bodyGrabarFirmasDocumentosVarios.esDocu = this.dataDocumentosVarios.esDocu;
    this.documentoVariosService.grabarFirmasDocumentoVarios(this.bodyGrabarFirmasDocumentosVarios).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.loadingGrabarFirmas = false;
          if (data.codResult < 0 ) {
            this.utilService.onShowAlert("info", "Atención", data.msgResult);
          } else if (data.codResult === 0) {
            this.dataGrabrarFirmas = data;
            this.utilService.onCloseLoading();
            this.dialogRef.close(true);
          }
        },
        error:( d )=>{
          this.loadingGrabarFirmas = false;
          this.utilService.onCloseLoading(); 
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("warning", "Atención", d.error.description);        
          }else {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      });
  }

  public cancelar(): void {
    this.dialogRef.close(false);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
