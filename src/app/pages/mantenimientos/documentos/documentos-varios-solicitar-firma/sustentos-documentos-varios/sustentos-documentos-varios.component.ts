import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { ArchivoAdjunto, TramaSustento } from '../../../../../models/archivo-adjunto.model';
import { IDocumentosVarios, IDocuVariosSustento } from '../../../../../interfaces/documentos-varios.interface';
import { BodyDocumentosVariosSustento } from '../../../../../models/mantenimientos/documentos/documentos-varios.models';
import { UploadComponent } from '../../../../upload/upload.component';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ConfirmationService } from 'primeng/api';
import { DocumentoVariosService } from '../../../../../services/mantenimientos/documentos/documentos-varios.service';
import { GeneralService } from '../../../../../services/general.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import Swal from 'sweetalert2';
import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';

@Component({
  selector: 'app-sustentos-documentos-varios',
  templateUrl: './sustentos-documentos-varios.component.html',
  styleUrls: ['./sustentos-documentos-varios.component.scss'],
  providers: [
    ConfirmationService
  ]
})
export class SustentosDocumentosVariosComponent implements OnInit {

  isLoading!: boolean;
  coZonaRegi?: string;
  coOficRegi?: string;
  sizeFiles: number = 0;
  coTipoAdju: string = '999';

  add!: number;
  delete!: number;
  bandeja!: number;

  sustento!: FormGroup;

  files: ArchivoAdjunto[] = [];

  docuVarios!: IDocumentosVarios[];
  docuVariosSustento: IDocuVariosSustento[] = [];

  bodyDocuVariosSustento!: BodyDocumentosVariosSustento;

  @ViewChild(UploadComponent) upload: UploadComponent;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private generalService: GeneralService,
    private validarService: ValidatorsService,
    private documentoVariosService: DocumentoVariosService
  ) { }

  ngOnInit(): void {
    this.bodyDocuVariosSustento = new BodyDocumentosVariosSustento();
    this.docuVarios = this.config.data.varios as IDocumentosVarios[];

    this.add = this.config.data.add;
    this.delete = this.config.data.delete;
    this.bandeja = this.config.data.bandeja;

    this.sustento = this.fb.group({
      tipoDocumento: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      docs: this.fb.array([])
    });

    this.hidrate( this.docuVarios['0'] );
    this.sustentoDocumentoVarios();
  }

  get isDisabled() {
    return this.docuVarios['0'].esDocu != '001'
  }

  addUploadedFile(file: any) {
    (this.sustento.get('docs') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.sustento.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.docuVariosSustento.findIndex( f => f.idDocuVariSust == fileId );
    this.docuVariosSustento.splice( index, 1 );
  }

  hidrate( d: IDocumentosVarios ) {
    console.log('DocuVariData', d);
    const fecha = this.validarService.reFormateaFecha( d.feDocu );

    this.sustento.get('tipoDocumento')?.setValue( d.deTipoDocu );
    this.sustento.get('fecha')?.setValue( fecha );
    this.coZonaRegi = d.coZonaRegi;
    this.coOficRegi = d.coOficRegi;
  }

  sustentoDocumentoVarios() {
    this.documentoVariosService.getDocumentoVariosSustento( this.docuVarios['0'].idDocuVari ).subscribe({
      next: ( sustentos ) => {
        console.log('docuVarioSust', sustentos);
        this.docuVariosSustento = sustentos.map( s => {
          return {
            idDocuVariSust: s.idDocuVariSust,
            noDocuSust: s.noDocuSust,
            noRutaSust: s.noRutaSust,
            idGuidDocu: s.idGuidDocu
          }
        });

        this.upload.sustDocuVari = this.docuVariosSustento;
        this.upload.goEdit();
      }
    });
  }

  parse() {
    const docs = (this.sustento.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  grabar() {
    this.parse();
    console.log('this.files =', this.files);

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de grabar los sustentos cargados?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        if ( this.files.length != 0 ) {
          this.saveAdjuntoInFileServer();
        }
        else {
          this.saveSustentoInBD();
        }
      },
      reject: () => {
        this.files = [];
      }
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            Swal.fire(`${ data.msgResult }`, '', 'error');
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            this.addSustento(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.saveSustentoInBD();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          Swal.close();
          this.files = [];
          this.ref.close('reject');
        }
      });
    }
  }

  addSustento(file?: any, guid?: string) {

    if ( file != '' && guid != '' ) {
      let trama = {} as any

      trama.idDocuVariSust = 0,
      trama.noDocuSust = file.deDocuAdju,
      trama.idGuidDocu = guid

      this.docuVariosSustento.push( trama );
    }
  }

  saveSustentoInBD() {
    this.bodyDocuVariosSustento.idDocuVari = this.docuVarios['0'].idDocuVari!;
    this.bodyDocuVariosSustento.trama = this.docuVariosSustento;
    console.log('dataDocuVar', this.bodyDocuVariosSustento );

    this.documentoVariosService.updateDocumentoVariosSustento( this.bodyDocuVariosSustento ).subscribe({
      next: ( d: any ) => {
        console.log( d );

        if ( d.codResult < 0 ) {
          Swal.fire( `${ d.msgResult }`, '', 'error');
          this.files = [];
          return;
        }

        Swal.close();
        this.ref.close('accept');
      },
      error: ( e ) => {
        console.log( e );
        Swal.fire('Error al guardar sustento.', '', 'error')
        this.files = [];
        this.isLoading = false;
      },
      complete: () => {
        this.files = [];
        this.isLoading = false;
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
