import { IDataFiltrosDocumentosVarios, IDocumentosVarios } from 'src/app/interfaces/documentos-varios.interface';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { UtilService } from 'src/app/services/util.service';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { DocumentoVariosService } from 'src/app/services/mantenimientos/documentos/documentos-varios.service';

@Component({
  selector: 'app-consultar-documentos-varios',
  templateUrl: './consultar-documentos-varios.component.html',
  styleUrls: ['./consultar-documentos-varios.component.scss']
})
export class ConsultarDocumentosVariosComponent implements OnInit, OnDestroy {
  loadingDocumentosVariosData: boolean = false;

  userCode:any;
  fechaActual: Date;
  idEdit!: number;
  msg!: string;
  editarDocumentosVarios!: FormGroup;
  idBancCargo!: number;
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoDocumento: IResponse2[] = [];
  unsubscribe$ = new Subject<void>();

  documentosVariosData: IDocumentosVarios;


  constructor( 
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private documentoVariosService: DocumentoVariosService,
    public funciones: Funciones,
    private utilService: UtilService,
    private sharingInformationService: SharingInformationService,
  ) { }

  ngOnInit(): void {
    this.userCode = localStorage.getItem('user_code')||'';
    this._getIdByRoute();
    this.fechaActual = new Date();
    this.editarDocumentosVarios = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoDocumento: ['', [ Validators.required ]],
      fecha: [this.fechaActual, [ Validators.required ]],
      numero: ['', [ Validators.required ]],
      nombreDocumento: ['', [ Validators.required ]],
      nombreRutaDocu: ['', [ Validators.required ]],
      concepto: ['', [ Validators.required ]],
    });
  }

  private _getIdByRoute(): void {
    this.activatedRoute.params.pipe(takeUntil(this.unsubscribe$))
      .subscribe( params => this._getDocumentosVariosPagoById(params['id']))
  }

  private _getDocumentosVariosPagoById(id: string): void {
    this.loadingDocumentosVariosData = true;
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.documentoVariosService.getDocumentoVariosById(id).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.documentosVariosData = data;
          this._inicilializarEditarCartaOrden();
          this.loadingDocumentosVariosData = false;
          this.utilService.onCloseLoading(); 
        },
        error: ( err ) => {
          this.loadingDocumentosVariosData = false;  
          this.utilService.onCloseLoading(); 
        }
      })
  }

  private _inicilializarEditarCartaOrden() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaZonaRegistral.push({ coZonaRegi: this.documentosVariosData.coZonaRegi, deZonaRegi: this.documentosVariosData.deZonaRegi });
    this.listaOficinaRegistral.push({ coOficRegi: this.documentosVariosData.coOficRegi, deOficRegi: this.documentosVariosData.deOficRegi });
    this.listaLocal.push({ coLocaAten: this.documentosVariosData.coLocaAten, deLocaAten: this.documentosVariosData.deLocaAten }); 
    this.listaTipoDocumento.push({ ccodigoHijo: this.documentosVariosData.tiDocu, cdescri: this.documentosVariosData.deTipoDocu }); 
    this.editarDocumentosVarios.get('numero')?.setValue( this.documentosVariosData.nuDocu);
    this.editarDocumentosVarios.get('nombreDocumento')?.setValue( this.documentosVariosData.noDocuPdf);
    this.editarDocumentosVarios.get('concepto')?.setValue( this.documentosVariosData.obDocu);
    this.editarDocumentosVarios.patchValue({ "fecha": new Date(this.documentosVariosData.feDocu)});
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  public validarMonto(event:any){
    const pattern = /^(-?\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public formatearNumero(event:any){
    if (event.target.value) {
      this.editarDocumentosVarios.get('numero')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
    }
  }

  private validarFormulario(): boolean {   
    if (this.editarDocumentosVarios.value.fecha === "" || this.editarDocumentosVarios.value.fecha === null) {                    
      document.getElementById('idFecha').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de nuevo documento.");
      return false;      
    } else if (this.editarDocumentosVarios.value.numero === '' || this.editarDocumentosVarios.value.numero === null) {
      document.getElementById('numero').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese un número.");
      return false;       
    } else if (this.editarDocumentosVarios.value.concepto === "" || this.editarDocumentosVarios.value.concepto === null) {
      document.getElementById('concepto').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese un concepto.");
      return false;       
    } else {
      return true;
    }       
  }

  public async cancelar() {
    const dataFiltrosDocumentosVarios: IDataFiltrosDocumentosVarios = await this.sharingInformationService.compartirDataFiltrosDocumentosVariosObservable.pipe(first()).toPromise(); 
    if (dataFiltrosDocumentosVarios.esBusqueda) {
      dataFiltrosDocumentosVarios.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosDocumentosVariosObservableData = dataFiltrosDocumentosVarios;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/documentos_varios_solicitar_firma/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaDocumentosVariosObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();   
  }

}
