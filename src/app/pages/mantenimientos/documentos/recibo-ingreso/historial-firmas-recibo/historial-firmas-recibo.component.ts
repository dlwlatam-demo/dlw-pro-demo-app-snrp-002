import { Component, OnInit } from '@angular/core';

import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { SortEvent } from 'primeng/api';

import { environment } from '../../../../../../environments/environment';
import { ReciboIngresoService } from 'src/app/services/mantenimientos/documentos/recibo-ingreso.service';
import { historiaFirmaReciboIngreso, ReciboIngreso } from 'src/app/models/mantenimientos/documentos/recibo-ingreso.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorsService } from '../../../../../core/services/validators.service';

@Component({
  selector: 'app-historial-firmas-recibo',
  templateUrl: './historial-firmas-recibo.component.html',
  styleUrls: ['./historial-firmas-recibo.component.scss']
})
export class HistorialFirmasReciboComponent implements OnInit {

  currentPage: string = environment.currentPage;
  recibo!: ReciboIngreso[];
  historial!: FormGroup;
  enviadosList!: historiaFirmaReciboIngreso[];


  constructor( 
    private ref: DynamicDialogRef, 
    private reciboService: ReciboIngresoService ,
    private fb: FormBuilder,
    private config: DynamicDialogConfig,
    private validateService: ValidatorsService
  ) { }

  ngOnInit(): void {

    this.recibo = this.config.data.csta as ReciboIngreso[];

    this.historial = this.fb.group({
      nroRecibo: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]],
      nroSiaf: ['', [ Validators.required ]]
    });
    this.reciboService.getReciboIngresoById( this.recibo['0'].idReciIngr! ).subscribe({
      next: ( data ) => {
        this.hidrate( data as ReciboIngreso )
      },
      error: ( e ) => {
        console.log( e );
      }
    });


  }


  hidrate( d: ReciboIngreso ) {
    this.historial.disable();
    const feDepo =  d.feReciIngr||'' ;
    const feDepoFormat: string = this.validateService.reFormateaFecha( feDepo );
    const moDepo = Number(d.imReciIngr).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
    
    this.historial.get('nroRecibo')?.setValue( d.nuReciIngr );
    this.historial.get('fecha')?.setValue( feDepoFormat );
    this.historial.get('nroSiaf')?.setValue( d.nuSiaf );
    this.historial.get('monto')?.setValue( moDepo);

     this.reciboService.getDocumentoFirmaHistoricoById(d.idDocuFirm!).subscribe({
      next: ( d ) => {
        this.enviadosList = d;
      },
      error:( d )=>{
        this.enviadosList= [];
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  regresar() {
    this.ref.close();
  }

  formatearFecha( date: string ): string {
    return this.validateService.reFormateaFechaConHoras( date );
  }

}
