import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService } from 'primeng/api';

import { SustentosReciboComponent } from '../sustentos-recibo/sustentos-recibo.component';
import { SolicitarFirmaReciboComponent } from '../solicitar-firma-recibo/solicitar-firma-recibo.component';
import { HistorialFirmasReciboComponent } from '../historial-firmas-recibo/historial-firmas-recibo.component';
import { ReservarReciboComponent } from '../reservar-recibo/reservar-recibo.component';
import { NoUtilizadoReciboComponent } from '../no-utilizado-recibo/no-utilizado-recibo.component';
import { CopiarReciboComponent } from '../copiar-recibo/copiar-recibo.component';

import { environment } from '../../../../../../environments/environment';
import { ReciboIngresoService } from 'src/app/services/mantenimientos/documentos/recibo-ingreso.service';
import { ReciboIngreso } from 'src/app/models/mantenimientos/documentos/recibo-ingreso.model';
import { GeneralService } from 'src/app/services/general.service';
import { Observable, map, of, Subject } from 'rxjs';
import { ReciboIngresoBandeja } from 'src/app/models/mantenimientos/documentos/recibo-ingreso-bandeja.model';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { UtilService } from '../../../../../services/util.service';
import { BodyEstadoReciboIngreso, BodyValidarRecibo } from '../../../../../models/mantenimientos/documentos/recibo-ingreso.model';
import { takeUntil } from 'rxjs/operators';
import { RECIBO_NUM } from '../../../../../models/enum/recibo-numeracion';
import { Funciones } from '../../../../../core/helpers/funciones/funciones';

@Component({
  selector: 'app-bandeja-recibo',
  templateUrl: './bandeja-recibo.component.html',
  styleUrls: ['./bandeja-recibo.component.scss'],
  providers: [
    DialogService,
    MessageService
  ]
})
export class BandejaReciboComponent extends BaseBandeja implements OnInit {

  recibo!: FormGroup;
  enviadosList!: ReciboIngreso[];
  override selectedValues!: ReciboIngreso[];
  currentPage: string = environment.currentPage;
  $userCode:any
  $estadoRecibo!: Observable<any>;
  $tipoRecibo!: Observable<any>;
  $zonas!: Observable<any>;
  $oficinas: Observable<any> = of([ { coOficRegi  : '*', deOficRegi: '(TODOS)' }]);
  $local : Observable<any>= of([  { coLocaAten  : '*', deLocaAten: '(TODOS)' }]);
  $filtro! : any
  reciboIngresoBandeja!: ReciboIngresoBandeja;
  selectSust!: ReciboIngreso[];
  fechaMaxima: Date;

  bodyEstadoRecibo!: BodyEstadoReciboIngreso;
  bodyValidarRecibo!: BodyValidarRecibo;

  sustentoCheck!: boolean;
  guidDocumento: string = '';

  BUSCAR: number = RECIBO_NUM.BUSCAR;
  NUEVO: number = RECIBO_NUM.NUEVO;
  EDITAR: number = RECIBO_NUM.EDITAR;
  ANULAR: number = RECIBO_NUM.ANULAR;
  ACTIVAR: number = RECIBO_NUM.ACTIVAR;
  CONSULTAR: number = RECIBO_NUM.CONSULTAR;
  AUTORIZAR_MODIFICACION: number = RECIBO_NUM.AUTORIZAR_MODIFICACION;
  VER_DOCUMENTO: number = RECIBO_NUM.VER_DOCUMENTO;
  VER_SUSTENTO: number = RECIBO_NUM.VER_SUSTENTO;
  SOLICITAR_FIRMA: number = RECIBO_NUM.SOLICITAR_FIRMA;
  VER_HISTORIAL: number = RECIBO_NUM.VER_HISTORIAL;
  COPIAR: number = RECIBO_NUM.COPIAR;
  RESERVAR: number = RECIBO_NUM.RESERVAR;
  NO_UTILIZADO: number = RECIBO_NUM.NO_UTILIZADO;
  EXPORTAR_EXCEL: number = RECIBO_NUM.EXPORTAR_EXCEL;

  unsubscribe$ = new Subject<void>();
  
  constructor( 
    private fb: FormBuilder,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private reciboService: ReciboIngresoService ,
    private router: Router,
    private funciones: Funciones,
    private utilService: UtilService,
    private validateService: ValidatorsService,
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Recibo_de_Ingresos;
    this.btnReciboDeIngreso();

    this.bodyEstadoRecibo = new BodyEstadoReciboIngreso();
    this.bodyValidarRecibo = new BodyValidarRecibo();

    const zonasItem: any = { coZonaRegi : '*', deZonaRegi: '(TODOS)' };
    const estadoReciboItem: any = { ccodigoHijo  : '*', cdescri: '(TODOS)' };
    const tipoReciboItem: any = { idTipo  : '*', noTipo: '(TODOS)' };

    this.$userCode = localStorage.getItem('user_code')||'';


    this.$tipoRecibo = this.reciboService.getTipoReciboIngreso('1').pipe(
      map( d => {
        d.unshift( tipoReciboItem );
        return d;
      })
    );
    this.$zonas = this.generalService.getCbo_Zonas_Usuario(this.$userCode).pipe(
      map( d => {
        if (d.length === 1) {
          this.recibo.get('zona')?.setValue(d[0].coZonaRegi); 
          this.buscarOficina();
        }
        d.unshift( zonasItem );
        return d;
      })
    );
    this.$estadoRecibo = this.generalService.getCbo_EstadoRecibo().pipe(
      map( d => {
        d.unshift( estadoReciboItem );
        return d;
      })
    );

    this.fechaMaxima = new Date();

    this.recibo = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      coLocaAten: ['', [ Validators.required ]],
      tipoRecibo: ['', [ Validators.required ]],
      fecDes: ['', []],
      fecHas: ['', []],
      nroRecibo: ['', []],
      nroSIAF: ['', []],
      noUsuaCrea: ['', []],
      noUsuaModi: ['', []],
      estado: ['', [ Validators.required ]],
      nombre: ['', []],
      monto: ['', []],
      nroCtaCte: ['', []],
      concepto: ['', []],
      fecCrea: ['', []],
      fecModi: ['', []],
      noUsuaAuto: ['', []],
      fecAuto: ['', []]
    });

    const hoy = new Date();
    const cDateD = new Date(hoy.getFullYear(), hoy.getMonth(), 1);
    this.recibo.get('fecDes')?.setValue( cDateD );
    const cDateH = new Date(hoy.getFullYear(), hoy.getMonth(), hoy.getDate());
    this.recibo.get('fecHas')?.setValue( cDateH );
    this.utilService.onShowProcessLoading('Cargando la información');
    setTimeout(()=>{
      this.$filtro = sessionStorage.getItem('searchRI');
      if (this.$filtro) {
        this.$filtro = JSON.parse(this.$filtro);
        const oficinasItem: any = { coOficRegi  : '*', deOficRegi: '(TODOS)' };
        if (this.$filtro.coZonaRegi != '') {
          this.$oficinas = this.generalService.getCbo_Oficinas_Zonas(this.$userCode,this.$filtro.coZonaRegi).pipe(
            map( d => {
              d.unshift( oficinasItem );
              return d;
            })
          );
        }
        if (this.$filtro.coOficRegi != '') {
          const localItem: any = { coLocaAten  : '*', deLocaAten: '(TODOS)' };
          this.$local = this.generalService.getCbo_Locales_Ofic(this.$userCode,this.$filtro.coZonaRegi,this.$filtro.coOficRegi).pipe(
            map( d => {
              d.unshift( localItem );
              return d;
            })
          );
        }
        const nuSiaf = this.$filtro.nuSiaf === 0 ? '': this.$filtro.nuSiaf;
        setTimeout(() => {
          let fechahasta = this.$filtro.feHast == 'Invalid Date' ? '' : this.$filtro.feHast;
          let fechaDesde = this.$filtro.feDesd == 'Invalid Date' ? '' : this.$filtro.feDesd;

          let cDateDHast;
          let cDateDDesde;

          if ( fechahasta == '' ) {
            cDateDHast = '';
          }
          else {
            fechahasta = fechahasta.split('/').reverse().join('-');
            cDateDHast = new Date(fechahasta);
            cDateDHast.setDate(cDateDHast.getDate() + 1);
          }

          if ( fechaDesde == '' ) {
            cDateDDesde = '';
          }
          else {
            fechaDesde = fechaDesde.split('/').reverse().join('-');
            cDateDDesde = new Date(fechaDesde);
            cDateDDesde.setDate(cDateDDesde.getDate() + 1);
          }


          this.recibo.get('fecHas')?.setValue(cDateDHast);
          this.recibo.get('fecDes')?.setValue(cDateDDesde);
          this.recibo.get('zona')?.setValue(this.$filtro.coZonaRegi); 
          this.recibo.get('oficina')?.setValue(this.$filtro.coOficRegi);
          this.recibo.get('coLocaAten')?.setValue(this.$filtro.coLocaAten);
          this.recibo.get('nroRecibo')?.setValue(this.$filtro.nuReciIngr);
          this.recibo.get('nroSIAF')?.setValue(nuSiaf);
          this.recibo.get('estado')?.setValue(this.$filtro.esDocu);
          this.recibo.get('tipoRecibo')?.setValue(this.$filtro.idTipoReciIngr);
          this.recibo.get('noUsuaCrea')?.setValue(this.$filtro.noUsuaCrea); 
          this.recibo.get('noUsuaModi')?.setValue(this.$filtro.noUsuaModi); 
          this.buscarRecibo();
        }, 1000);
      }else{
        this.buscarRecibo();
      }
    }, 1000);

    const validaRutaInt = setInterval( validarRuta, 2000);
    function validarRuta() {
      if (!window.location.href.includes('recibos_de_ingreso')) {
        clearInterval(validaRutaInt);
        sessionStorage.removeItem('searchRI');
      }
    }
  }

  


  buscarOficina() {
    const oficinasItem: any = { coOficRegi  : '*', deOficRegi: '(TODOS)' };
    const zonaValor = this.recibo.get('zona')?.value; 
    if (zonaValor === '*') {
      this.$oficinas = of([oficinasItem]);
    }else {
      this.$oficinas = this.generalService.getCbo_Oficinas_Zonas(this.$userCode,zonaValor).pipe(
        map( d => {
          if (d.length === 1) {
            this.recibo.get('oficina')?.setValue(d[0].coOficRegi); 
            this.buscarLocal();
          }
          d.unshift( oficinasItem );
          return d;
        })
      );
    }
    this.$local =  of([  { coLocaAten  : '*', deLocaAten: '(TODOS)' }]);
  }

  buscarLocal() {
    const localItem: any = { coLocaAten  : '*', deLocaAten: '(TODOS)' };
    const oficinaValor = this.recibo.get('oficina')?.value; 
    const zonaValor = this.recibo.get('zona')?.value; 
    if (oficinaValor === '*') {
      this.$local = of([localItem]);
    }else{
      this.$local = this.generalService.getCbo_Locales_Ofic(this.$userCode,zonaValor,oficinaValor).pipe(
        map( d => {
          if (d.length === 1) {
            this.recibo.get('coLocaAten')?.setValue(d[0].coLocaAten); 
          }
          d.unshift( localItem );
          return d;
        })
      );
    }
  }


  buscarEnter (event:any){
    if (event.keyCode === 13) {
      this.buscarRecibo();
    }
  }

  editOrView( value: string ) {
    if ( !this.validar(value) )
      return;

    if ( value == 'edit' ) {
      this.router.navigate([`SARF/mantenimientos/documentos/recibos_de_ingreso/editar`]);
    }
    else {
      this.router.navigate([`SARF/mantenimientos/documentos/recibos_de_ingreso/consultar`]);
    }
    
    sessionStorage.setItem( 'idReciIngr', String( this.selectedValues[0].idReciIngr! ) );
  }

  validar( valor: string ): boolean {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Debe seleccionar un registro', '', 'error');
      this.selectedValues = [];
      return false;
    }

    if ( valor === 'edit' )
    {
      if ( this.selectedValues.length != 1 ) {
        Swal.fire( 'Solo se puede editar un registro a la vez', '', 'error');
        this.selectedValues = [];
        return false;
      }
    }
    else
    {
      if ( this.selectedValues.length != 1 ) {
        Swal.fire( 'Solo se puede consultar un registro a la vez', '', 'error');
        this.selectedValues = [];
        return false;
      }
    }

    return true;
  }

  public cambioFechaInicio() {   
    if (this.recibo.get('fecHas')?.value !== '' && this.recibo.get('fecHas')?.value !== null) {
      if (this.recibo.get('fecDes')?.value > this.recibo.get('fecHas')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.recibo.controls['fecDes'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.recibo.get('fecDes')?.value !== '' && this.recibo.get('fecDes')?.value !== null) {
      if (this.recibo.get('fecHas')?.value < this.recibo.get('fecDes')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.recibo.controls['fecHas'].reset();    
      }      
    }
  }

  buscarRecibo() {
    this.selectedValues = [];
    
    const fechaHasta = this.recibo.get('fecHas')?.value;
    const fechaDesde = this.recibo.get('fecDes')?.value;
    const coZonaRegi = this.recibo.get('zona')?.value;  
    const coOficRegi = this.recibo.get('oficina')?.value;
    const coLocaAten = this.recibo.get('coLocaAten')?.value;
    const nuReciIngr = this.recibo.get('nroRecibo')?.value;
    const nuSiaf = this.recibo.get('nroSIAF')?.value;
    const esDocu = this.recibo.get('estado')?.value;
    const fechaCreacion = this.recibo.get('fecCrea')?.value;
    const fechaModificacion = this.recibo.get('fecModi')?.value;
    const fechaAutorizacion = this.recibo.get('fecAuto')?.value;
    const idTipoReciIngr = this.recibo.get('tipoRecibo')?.value;

    const data = {
      coZonaRegi : coZonaRegi === '*'? '': coZonaRegi ,
      coOficRegi : coOficRegi === '*'? '': coOficRegi,
      coLocaAten : coLocaAten === '*'? '': coLocaAten,
      nuConsAbon : this.recibo.get('nroConst')?.value,
      feDesd : this.validateService.formatDate( fechaDesde ),
      feHast : this.validateService.formatDate( fechaHasta ),
      esDocu : esDocu === '*'? '': esDocu,
      noUsuaCrea : this.recibo.get('noUsuaCrea')?.value,
      noUsuaModi : this.recibo.get('noUsuaModi')?.value,
      idTipoReciIngr:idTipoReciIngr === '*'? '': idTipoReciIngr,
      nuReciIngr: nuReciIngr,
      nuSiaf: Number(nuSiaf),
      noBene: this.recibo.get('nombre')?.value,
      imReciIngr: this.recibo.get('monto')?.value,
      nuCuenBanc: this.recibo.get('nroCtaCte')?.value,
      obConc: this.recibo.get('concepto')?.value,
      feCrea: (fechaCreacion === null || fechaCreacion === '') ? '' : this.validateService.formatDate( fechaCreacion ),
      feModi: (fechaModificacion === null || fechaModificacion === '') ? '' : this.validateService.formatDate( fechaModificacion ),
      feDocuAuto: (fechaAutorizacion === null || fechaAutorizacion === '') ? '' : this.validateService.formatDate( fechaAutorizacion ),
      noUsuaAuto: this.recibo.get('noUsuaAuto')?.value,
    }
    if (!Swal.isLoading) {
      Swal.showLoading();
    }
    sessionStorage.setItem('searchRI',JSON.stringify(data));
    this.reciboService.getBandejaReciboIngreso(data).subscribe({
      next: ( d ) => {
        this.reciboIngresoBandeja = d;

        this.enviadosList = d.lstReciboIngreso ? d.lstReciboIngreso : [];
        this.selectSust = this.enviadosList.filter( re => re.inSustRegi == '1' );
      },
      error:( d )=>{
        this.enviadosList= [];
        if (d.error.category === "NOT_FOUND") {
          this.utilService.onShowAlert('warning', 'Atención', d.error.description);
        }else {
          this.utilService.onShowMessageErrorSystem();
        }
      },
      complete:()=>{
        Swal.close();
      }
    });
  }

  nuevoRecibo() {
    this.router.navigate(['SARF/mantenimientos/documentos/recibos_de_ingreso/nuevo']);
  }

  autorizarModificacion() {
    if ( this.selectedValues.length === 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar mínimo un registro registro.");
      return;
    }

    // for ( let reg of this.selectedValues ) {
    //   if ( reg.esDocu != '003' ) {
    //     this.utilService.onShowAlert("warning", "Atención", "Solo puede autorizar la modificación de uno o más registros en estado FIRMADO.");
    //     return;
    //   }
    // }

    this.bodyEstadoRecibo.trama = this.selectedValues.map(( value )=> { 
      return { id: String( value.idReciIngr ) } 
    });

    const textoAnular = (this.selectedValues?.length === 1) ? 'del Recibo' : 'de los Recibos';

    this.utilService.onShowConfirm(`¿Estás seguro que deseas AUTORIZAR la modificación ${textoAnular} de Ingreso?`).then((result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('autorizar'); 
        
        this.reciboService.autorizarModificacionRegistros( this.bodyEstadoRecibo ).subscribe({
          next: ( d: any ) => {
            if (d.codResult < 0 ) {
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
            } else if (d.codResult === 0 ) {
              this.utilService.onShowAlert("success", "Atención", 'Se actualizaron los registros correctamente');
              this.selectedValues = [];
              this.buscarRecibo();
            }
          },

          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", d.error.description);        

            } else {
              this.utilService.onShowMessageErrorSystem();        
            }
          }
        });
        
      } else if (result.isDenied) {
        this.utilService.onShowAlert("warning", "Atención", "No se pudieron guardar los cambios.");
      }
    });
  }

  verDocumento() {
	  console.log('verDocumento');
    if ( !this.selectedValues || !this.selectedValues.length ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro.');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Solo puede visualizar un documento a la vez.');
      return;
    }

	  const recibo  = this.selectedValues;

    if ( recibo[0].esDocu == '001' ) {
      this.reciboService.getReciboIngresoPdfById( recibo['0'].idReciIngr! ).subscribe({
        next: ( data ) => {
          
          this.guidDocumento = data.msgResult;
          console.log('Exportar PDF: ' + this.guidDocumento);
  
          const doc = data.archivoPdf;
          const byteCharacters = atob(doc!);
          const byteNumbers = new Array(byteCharacters.length);
          for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
          }
          const byteArray = new Uint8Array(byteNumbers);
          const blob = new Blob([byteArray], { type: 'application/pdf' });
          const blobUrl = URL.createObjectURL(blob);
          const download = document.createElement('a');
          download.href =  blobUrl;
          //download.setAttribute('download', 'Constacia_Abono');
          download.setAttribute("target", "_blank");
          document.body.appendChild( download );
          download.click();
        },
        error: ( e ) => {
          Swal.fire( 'Atención', e.error.description, 'warning');
        }
      });
    }
    else {
      const blobUrl = this.generalService.downloadManager( recibo[0].idGuidDocu );
      const download = document.createElement('a');
      download.href =  blobUrl;
      //download.setAttribute('download', 'Constacia_Abono');
      download.setAttribute("target", "_blank");
      document.body.appendChild( download );
      download.click();
    }
    
	
  }

  sustentos() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro.');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Solo puede visualizar un registro a la vez.');
      return;
    }

    const dialog = this.dialogService.open( SustentosReciboComponent, {
      header: 'Recibo de Ingreso - Sustentos',
      width: '45%',
      closable: false,
      data: {
        csta: this.selectedValues,
        add: this.BtnAdicionar_Sustento,
        delete: this.BtnEliminar_Sustento,
        docRecibido: this.BtnAdicionar_Sustento_DocRecibidos,
        bandeja: this.codBandeja
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Los sustentos han sido cargados y grabados correctamente',
          life: 5000
        });

        this.buscarRecibo();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al cargar y grabar los sustentos',
          life: 5000
        });

        Swal.close();
        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  solicitarFirma() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro.');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Solo puede solicitar firma de un registro a la vez.');
      return;
    }

    // if ( !(this.selectedValues['0'].esDocu == '001' || this.selectedValues['0'].esDocu == '006' || this.selectedValues['0'].esDocu == '007' || this.selectedValues['0'].esDocu == '004' ) ) {
    //   Swal.fire( 'Solo puede Solicitar Firma a Recibos de Ingreso en estado REGISTRADO, NO UTILIZADO o RESERVADO', '', 'error');
    //   this.selectedValues = [];
    //   return;
    // }
	
	  const recibo  = this.selectedValues;
    this.reciboService.getReciboIngresoPdfById( recibo['0'].idReciIngr! ).subscribe({
      next: ( data ) => {
        console.log('dataFirm', data);
        
		this.guidDocumento = data.msgResult;
		console.log('Exportar PDF: ' + this.guidDocumento);
				
		const dialog = this.dialogService.open( SolicitarFirmaReciboComponent, {
		  header: 'Recibo de Ingreso - Solicitar Firma',
		  width: '45%',
      closable: false,
		  data: {
			csta: this.selectedValues,
			guidDocumento: data.msgResult,
			doc: data.archivoPdf
		  }
		});

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Se solicitó la firma del documento satisfactoriamente.',
          life: 5000
        });

        this.buscarRecibo();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al solicitar la firma del documento.',
          life: 5000
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
		
      },
      error: ( e ) => {
        console.log( e );
      }
    });
  }

  historialFirmas() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro.');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Solo puede visualizar el historial de un registro a la vez.');
      return;
    }

    // if ( !(this.selectedValues['0'].esDocu == '002' || this.selectedValues['0'].esDocu == '003' || this.selectedValues['0'].esDocu == '004') ) {
    //   Swal.fire( 'Solo puede ver el Historial de Firmas a Recibos de Ingreso en estado PENDIENTE DE FIRMA, FIRMADO o RECHAZADO', '', 'error');
    //   this.selectedValues = [];
    //   return;
    // }

    const dialog = this.dialogService.open( HistorialFirmasReciboComponent, {
      header: 'Recibo de Ingreso - Historial de Firmas',
      width: '50%',
      closable: false,
      data: {
        csta: this.selectedValues
      }
    });

    dialog.onClose.subscribe( () => {
      this.selectedValues = [];
    });
  }

  reservar() {
  
    const dialog = this.dialogService.open( ReservarReciboComponent, {
      header: 'Recibo de Ingreso - Reservar',
      width: '50%',
      data: {
        csta: this.selectedValues
      }
    });

    dialog.onClose.subscribe( () => {
      this.selectedValues = [];
      this.buscarRecibo();
    });
  }

  noUtilizado() {

    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro.");
      return;
    }

    // if ( this.selectedValues['0'].esDocu != '006' ) {
    //   Swal.fire( 'Solo puede modificar Recibos de Ingreso en estado RESERVADO', '', 'error');
    //   this.selectedValues = [];
    //   return;
    // }

    const dialog = this.dialogService.open( NoUtilizadoReciboComponent, {
      header: 'Cambiar a estado NO UTILIZADO',
      width: '48%',
      closable: false,
      data: {
        recibos: this.selectedValues
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        
        this.buscarRecibo();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.utilService.onShowAlert('error', 'Atención', 'Hubo un error al realizar el cambio.');

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  anular() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro.');
      return;
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';

    Swal.fire({
      title: 'Recibos de Ingreso',
      text: `¿Está Ud. seguro que desea anular ${ msg1 } ${ msg2 }?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        const data = {
          trama:this.selectedValues.map((value)=> { return {id:value.idReciIngr}})
        }
        this.reciboService.anularReciboIngreso(data ).subscribe({
          next: ( d:any ) => {
            this.selectedValues = [];
            if (d.codResult < 0 ) {
              Swal.fire(d.msgResult, '', 'info');
              return;
            }
            Swal.fire('Recibos de Ingreso','Anulado!',  'success').then(()=>{
              this.buscarRecibo();
            });
          }
        });
      } else if (result.isDenied) {
        this.selectedValues = [];
        Swal.fire('No se pudieron guardar los cambios.', '', 'info')
      }
    });
  }

  activar() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro.');
      return;
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';
  
    Swal.fire({
      title: 'Recibos de Ingreso',
      text: `¿Está Ud. seguro que desea activar ${ msg1 } ${ msg2 }?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        const data = {
          trama:this.selectedValues.map((value)=> { return {id:value.idReciIngr}})
        }
        this.reciboService.activarReciboIngreso(data ).subscribe({
          next: ( d:any ) => {
            this.selectedValues = [];

            if (d.codResult < 0 ) {
              Swal.fire(d.msgResult, '', 'info');
              return;
            }
            Swal.fire('Recibos de Ingreso','Activado!','success').then(()=>{
              this.buscarRecibo();
            });
          }
        });
        
      } else if (result.isDenied) {
        this.selectedValues = [];
        Swal.fire('Recibos de Ingreso','No se pudieron guardar los cambios.', 'info')
      }
    });
  }

  copiar() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro.');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Solo puede copiar un registro a la vez.');
      this.selectedValues = [];
      return;
    }

    const dialog = this.dialogService.open( CopiarReciboComponent, {
      header: 'Recibo de Ingreso - Copiar',
      width: '50%',
      data: {
        csta: this.selectedValues
      }
    });

    dialog.onClose.subscribe( () => {
      this.selectedValues = [];
      this.buscarRecibo();
    });
  }


  exportExcel() {
    console.log('Exportar Excel');
    const fileName = this.validateService.formatDateWithHoursForExcel( new Date() );

    const doc = this.reciboIngresoBandeja.reporteExcel;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', `ReciboIngreso_${ fileName }` );
    document.body.appendChild( download );
    download.click();
  }

  exportPdf() {
    console.log('Exportar PDF');
    const fileName = this.validateService.formatDateWithHoursForExcel( new Date() );

    const doc = this.reciboIngresoBandeja.reportePdf;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/pdf' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', `ReciboIngreso_${ fileName }` );
    document.body.appendChild( download );
    download.click();
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  public validarMonto(event:any){
    const pattern = /^(-?\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public validarNumeroCuenta(event:KeyboardEvent){
    const pattern = /[0-9-]+/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  public formatearNumero(event:any){
    if (event.target.value) {
      const montoIngreso = this.funciones.convertirTextoANumero( event.target.value );
      this.recibo.get('monto')?.setValue( Number(montoIngreso).toFixed(2) );
    }
  }

  public validarBotonRecibo( tipoVali: number ) {
    this.bodyValidarRecibo.tipoVali = tipoVali;
    this.bodyValidarRecibo.tramaReciIngr = this.selectedValues?.map( value => { return value.idReciIngr }) || [];

    this.reciboService.validarAccionReciboIngreso( this.bodyValidarRecibo ).pipe( takeUntil( this.unsubscribe$ ) )
        .subscribe({
          next: ( data ) => {
            if ( data.codResult < 0 ) {
              this.utilService.onShowAlert('warning', 'Atención', data.msgResult);
              return;
            }

            this._realizarAccionBoton( tipoVali );
          },
          error: ( err ) => {
            if ( err.error.category === "NOT_FOUND" ) {
              this.utilService.onShowAlert("info", "Atención", err.error.description);
            } else {
              this.utilService.onShowMessageErrorSystem();
            }
          }
        });
  }

  private _realizarAccionBoton( tipoVali: number ) {
    switch ( tipoVali ) {
      case this.NUEVO:
        this.nuevoRecibo();
          break;
      case this.EDITAR:
        this.editOrView('edit');
          break;
      case this.ANULAR:
        this.anular();
          break;
      case this.ACTIVAR:
        this.activar();
          break;
      case this.CONSULTAR:
        this.editOrView('view');
          break;
      case this.AUTORIZAR_MODIFICACION:
        this.autorizarModificacion();
          break;
      case this.VER_DOCUMENTO:
        this.verDocumento();
          break;
      case this.VER_SUSTENTO:
        this.sustentos();
          break;
      case this.SOLICITAR_FIRMA:
        this.solicitarFirma();
          break;
      case this.VER_HISTORIAL:
        this.historialFirmas();
          break;
      case this.COPIAR:
        this.copiar();
          break;
      case this.RESERVAR:
        this.reservar();
          break;
      case this.NO_UTILIZADO:
        this.noUtilizado();
          break;
      case this.EXPORTAR_EXCEL:
        this.exportExcel();
          break;
      default:
        this.buscarRecibo();
    }
  }
  

}


