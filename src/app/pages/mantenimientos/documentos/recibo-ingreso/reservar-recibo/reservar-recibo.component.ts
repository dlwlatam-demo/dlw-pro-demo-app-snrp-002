import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ReciboIngreso } from 'src/app/models/mantenimientos/documentos/recibo-ingreso.model';
import { ReciboIngresoService } from 'src/app/services/mantenimientos/documentos/recibo-ingreso.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map, Observable, of } from 'rxjs';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-reservar-recibo',
  templateUrl: './reservar-recibo.component.html',
  styleUrls: ['./reservar-recibo.component.scss'],
  styles: [`
    #borde {
      padding: 10px;
      border: black 1px solid;
    }
  `]
})
export class ReservarReciboComponent implements OnInit {
  recibo!: ReciboIngreso[];
  reservaForm!: FormGroup;
  $zonas!: Observable<any>;
  $oficinas: Observable<any> = of([ { coOficRegi  : '*', deOficRegi: '(SELECCIONE)' }]);
  $local: Observable<any> = of([ { coLocaAten  : '*', deLocaAten: '(SELECCIONE)' }]);
   // Observable<any>= of([  { coLocaAten  : '*', deLocaAten: '(SELECCIONE)' }])
  $userCode:any

  constructor( private ref: DynamicDialogRef, 
              private fb: FormBuilder,
              private generalService: GeneralService,
               private reciboService: ReciboIngresoService ,
               private config: DynamicDialogConfig ) { }

  ngOnInit(): void {
    const zonasItem: any = { coZonaRegi : '*', deZonaRegi: '(SELECCIONE)' };



    this.recibo = this.config.data.csta as ReciboIngreso[];
    this.reservaForm = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      coLocaAten: ['', [ Validators.required ]],
      caReciIngr: ['', [ Validators.required,Validators.max(999),Validators.pattern("^[0-9]*$") ]],
    });

    this.$zonas = this.generalService.getCbo_Zonas_Usuario(this.$userCode).pipe(
      map( d => {
        if (d.length === 1) {
          this.reservaForm.get('zona')?.setValue(d[0].coZonaRegi);
          this.buscarOficina();
        }
        d.unshift( zonasItem );
        return d;
      })
    );
  }
  
  async buscarOficina() {
    const oficinasItem: any = { coOficRegi  : '*', deOficRegi: '(SELECCIONE)' };
    const zonaValor = this.reservaForm.get('zona')?.value; 
    if (zonaValor === '*') {
      this.$oficinas = of([oficinasItem]);
    }else {
      this.$oficinas = await this.generalService.getCbo_Oficinas_Zonas(this.$userCode,zonaValor).pipe(
        map( d => {
          if (d.length === 1) {
            this.reservaForm.get('oficina')?.setValue(d[0].coOficRegi);
            this.buscarLocal();
          }
          d.unshift( oficinasItem );
          return d;
        })
      );
    }
  }

  validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  async buscarLocal()  {
    const localItem: any = { coLocaAten  : '*', deLocaAten: '(SELECCIONE)' };
    const oficinaValor = this.reservaForm.get('oficina')?.value; 
    const zonaValor = this.reservaForm.get('zona')?.value; 
    if (oficinaValor === '*') {
      this.$local = of([localItem]);
    }else{

      this.$local = await this.generalService.getCbo_Locales_Ofic(this.$userCode,zonaValor,oficinaValor).pipe(
        map( d => {
          if (d.length === 1) {
            this.reservaForm.get('coLocaAten')?.setValue(d[0].coLocaAten);
          }
          d.unshift( localItem );
          return d;
        })
      );
    }
  }

  grabar() {
    const validarZona = ( !this.reservaForm.get('zona')?.value || !this.reservaForm.get('zona')?.value?.length );
    const validarCantidad = ( !this.reservaForm.get('caReciIngr')?.value || !this.reservaForm.get('caReciIngr')?.value?.length );
    const validarLocal = ( !this.reservaForm.get('coLocaAten')?.value || !this.reservaForm.get('coLocaAten')?.value?.length );
    const validarOficina = ( !this.reservaForm.get('oficina')?.value || !this.reservaForm.get('oficina')?.value?.length );
    if (  validarCantidad || validarLocal  || validarOficina || validarZona ) {
      Swal.fire( 'Recibos de Ingreso - Reservar', 'Todos los campos son requeridos!', 'info');
      return;
    }

  
      const coZonaRegi = this.reservaForm.get('zona')?.value
      const coOficRegi = this.reservaForm.get('oficina')?.value
      const coLocaAten = this.reservaForm.get('coLocaAten')?.value
      const data = {
        caReciIngr: Number(this.reservaForm.get('caReciIngr')?.value),
        coZonaRegi,
        coOficRegi,
        coLocaAten
      }
  
    this.reciboService.resrvarReciboIngreso(data).subscribe({
      next: ( d:any ) => {
        if (d.codResult < 0 ) {
          Swal.fire(d.msgResult, '', 'info');
          return;
        }
        Swal.fire( 'Recibos de Ingreso', `Reserva creada!`, 'success').then((result) => {
          this.ref.close();
        })
      },
      error:()=>{
        Swal.fire('Error al crear la reserva.', '', 'error');

      }
    });
  }

  cancelar() {
    this.ref.close();
  }

}

// map( d => {
//   if (d.length === 1) {
//     this.reservaForm.get('coLocaAten')?.setValue(d[0].coLocaAten);
//   }
//   d.unshift( localItem );
//   return d;
// })
