import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, AbstractControl } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ReciboIngreso } from 'src/app/models/mantenimientos/documentos/recibo-ingreso.model';
import { GeneralService } from 'src/app/services/general.service';
import { ReciboIngresoService } from 'src/app/services/mantenimientos/documentos/recibo-ingreso.service';
import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';
import { UploadComponent } from '../../../../upload/upload.component';
import { TramaSustento, ArchivoAdjunto } from '../../../../../models/archivo-adjunto.model';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';
import { Funciones } from '../../../../../core/helpers/funciones/funciones';
import { BaseBandeja } from 'src/app/base/base-bandeja.abstract';

@Component({
  selector: 'app-sustentos-recibo',
  templateUrl: './sustentos-recibo.component.html',
  styleUrls: ['./sustentos-recibo.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class SustentosReciboComponent extends BaseBandeja implements OnInit {

  isLoading!: boolean;
  isLoadingImg!: boolean;
  coZonaRegi?: string;
  coOficRegi?: string;
  sizeFiles: number = 0;
  coTipoAdju: string = '001';

  add!: number;
  delete!: number;
  bandeja!: number;
  docRecibido!: number;

  sustento!: FormGroup;

  files: ArchivoAdjunto[] = []

  recibo!: ReciboIngreso[];
  reciboSustento: TramaSustento[] = [];
  reciboSustentoTemp: TramaSustento[] = [];

  @ViewChild(UploadComponent) upload: UploadComponent;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    public funciones: Funciones,
    private confirmService: ConfirmationService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private validarService: ValidatorsService,
    private reciboService: ReciboIngresoService ,
  ) { super() }

  ngOnInit(): void {
    
    this.recibo = this.config.data.csta as ReciboIngreso[];

    this.add = this.config.data.add;
    this.delete = this.config.data.delete;
    this.bandeja = this.config.data.bandeja;
    this.docRecibido = this.config.data.docRecibido;

    this.sustento = this.fb.group({
      numRecibo: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      numSiaf: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]],
      idDocuEnvi: ['', []],
      docs: this.fb.array([])
    });

    this.hidrate( this.recibo[0] );
    this.sustentosRecibo();
  }

  addUploadedFile(file: any) {
    const sustento = this.sustento.get('docs').value.find( ( d: any ) => file.fileId === d.doc );
    if ( !sustento ) {
      (this.sustento.get('docs') as FormArray).push(
        this.fb.group(
          {
            doc: [file.fileId, []],
            nombre: [file.fileName, []],
            idAdjunto: [file?.idAdjunto, []]
          }
        )
      )
    }
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.sustento.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.reciboSustentoTemp.findIndex( f => f.id == fileId );
    this.reciboSustentoTemp.splice( index, 1 );
  }

  hidrate( d: ReciboIngreso ) {

    const moDepo = Number( d.imReciIngr ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
    
    const fecRecibo = this.validarService.reFormateaFecha( d.feReciIngr ); 

    this.sustento.get('numRecibo')?.setValue( d.nuReciIngr );
    this.sustento.get('fecha')?.setValue( fecRecibo );
    this.sustento.get('numSiaf')?.setValue( d.nuSiaf );
    this.sustento.get('monto')?.setValue( moDepo);
    this.coZonaRegi = d.coZonaRegi;
    this.coOficRegi = d.coOficRegi;
  }

  sustentosRecibo() {
    this.reciboService.getRecibosSustentos( this.recibo[0].idReciIngr ).subscribe({
      next: ( sustentos ) => {
        this.reciboSustentoTemp = sustentos.map( s => {
          return {
            id: s.idReciIngrSust,
            noDocuSust: s.noDocuSust,
            idGuidDocu: s.idGuidDocu
          }
        });

        this.upload.sustentos = this.reciboSustentoTemp;
        this.upload.goEdit();
      }
    });
  }

  obtenerIdImagenes() {
    this.messageService.clear();
    
    const idDocuEnvi = this.sustento.get('idDocuEnvi')?.value;

    if ( idDocuEnvi === '' ) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Ingrese un número de documento',
        sticky: true
      });

      return;
    }

    if ( idDocuEnvi === 0 || idDocuEnvi === '0' ) {
      this.messageService.add({
        severity: 'warn',
        summary: 'El número de documento debe ser mayor a 0',
        sticky: true
      });

      return;
    }

    this.isLoadingImg = true;

    this.reciboService.getIdImagenesEnviarDocumentos( idDocuEnvi ).subscribe({
      next: ( imagenes ) => {
        this.isLoadingImg = false;
        const enviarDocumentos: TramaSustento[] = imagenes.map( i => {
          return {
            id: i.idDocuEnviSust,
            noDocuSust: i.noDocuSust,
            idGuidDocu: i.idGuidDocu
          }
        });

        this.reciboSustentoTemp = this.reciboSustentoTemp.concat( enviarDocumentos );

        this.sustento.get('idDocuEnvi')?.setValue('');

        this.upload.sustentos = enviarDocumentos;
        this.upload.goEdit();
      },
      error: ( err ) => {
        this.isLoadingImg = false;
        this.messageService.add({ severity: 'warn', summary: err.error.description, sticky: true });
      }
    });
  }

  parse() {
    const docs = (this.sustento.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value;
      const nombre = g.get('nombre')?.value;
      const idAdjunto = g.get('idAdjunto')?.value;

      if ( idAdjunto === null )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  grabar() {
    this.messageService.clear();
    
    this.parse();
    console.log('this.files =', this.files);

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de grabar los sustentos cargados?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        if ( this.files.length != 0 ) {
          this.saveAdjuntoInFileServer();
        }
        else {
          this.saveSustentoInBD();
        }
      },
      reject: () => {
        this.files = [];
      }
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.messageService.add({
              severity: 'warn',
              summary: 'Atención',
              detail: data.msgResult,
              sticky: true
            });

            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            this.addSustento(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.saveSustentoInBD();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          this.files = [];
          this.ref.close('reject');
        }
      });
    }
  }

  addSustento(file?: any, guid?: string) {

    if ( file != '' && guid != '' ) {
      let trama = {} as TramaSustento;

      trama.id = 0,
      trama.noDocuSust = file.deDocuAdju,
      trama.idGuidDocu = guid

      this.reciboSustentoTemp.push( trama );
    }
  }

  saveSustentoInBD() {

    for ( let recibo of this.reciboSustentoTemp ) {
      const index: number = this.sustento.get('docs')?.value.findIndex( ( d: any ) => 
                  (recibo.noDocuSust ===  d.nombre && recibo.id === d.idAdjunto) 
                    || 
                  recibo.idGuidDocu === d.doc );
      this.reciboSustento[index] = recibo;
    }

    this.reciboSustento.forEach( r => {
      r.noDocuSust = r.noDocuSust.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
                      .replace(/"/g, '&quot;').replace(/'/g, '&apos;');
    });

    const dataRecib = {
      idReciIngr: this.recibo['0'].idReciIngr!,
      trama: this.reciboSustento
    }
    console.log('dataRecib', dataRecib );
    
    this.reciboService.sustentoGuardarReciboIngreso( dataRecib ).subscribe({
      next: ( d: any ) => {

        if ( d.codResult < 0 ) {
          this.messageService.add({
            severity: 'warn',
            summary: 'Atención',
            detail: d.msgResult,
            sticky: true
          });

          Swal.close();
          this.files = [];
          return;
        }

        Swal.close();
        this.ref.close('accept');
      },
      error: ( e ) => {
        console.log( e );
        this.messageService.add({
          severity: 'error',
          summary: 'Atención',
          detail: 'Error al guardar sustento.',
          sticky: true
        });

        Swal.close();
        this.files = [];
        this.isLoading = false;
      },
      complete: () => {
        this.files = [];
        this.isLoading = false
      }
    });
  }

  get isDisabled() {
    return !(this.recibo['0'].esDocu == '001' || this.recibo['0'].esDocu == '006' || this.recibo['0'].esDocu == '007'
            || this.recibo['0'].esDocu == '004');
  }

  get disabledSubir() {
    let disable: boolean;
    if ( !(this.recibo['0'].esDocu == '001' || this.recibo['0'].esDocu == '004') ) {
      this.sustento.get('idDocuEnvi')?.disable();
      disable = true;
    }
    return disable;
  }

  cancelar() {
    this.ref.close('');
  }

}
