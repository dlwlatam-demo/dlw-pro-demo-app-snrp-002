import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';

import { MessageService, ConfirmationService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { ReciboIngreso } from 'src/app/models/mantenimientos/documentos/recibo-ingreso.model';
import { GeneralService } from 'src/app/services/general.service';
import { ReciboIngresoService } from 'src/app/services/mantenimientos/documentos/recibo-ingreso.service';
import { IUsuarioSistema } from '../../../../../interfaces/seguridad/usuario.interface';
import { BodyNoUtilizadoRecibo } from '../../../../../models/mantenimientos/documentos/recibo-ingreso.model';
import { UsuariosService } from '../../../../../services/seguridad/usuarios.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-no-utilizado-recibo',
  templateUrl: './no-utilizado-recibo.component.html',
  styleUrls: ['./no-utilizado-recibo.component.scss'],
  styles: [`
    #borde {
      padding: 10px;
      border: 1px solid #ced4da;
      border-radius: 6px;
    }
  `],
  providers: [
    MessageService,
    ConfirmationService
  ]
})
export class NoUtilizadoReciboComponent implements OnInit {
  
  codigoUsuario: number = 0;
  isLoading: boolean = false;
  loadingUsuario: boolean = false;

  recibos!: ReciboIngreso[];
  listaUsuarios: IUsuarioSistema[] = [];

  bodyEstadoRecibo!: BodyNoUtilizadoRecibo;

  unsubscribe$ = new Subject<void>();
  
  constructor( 
    private ref: DynamicDialogRef, 
    private config: DynamicDialogConfig,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private usuarioService: UsuariosService,
    private reciboService: ReciboIngresoService,
  ) { }

  ngOnInit(): void {
    this.bodyEstadoRecibo = new BodyNoUtilizadoRecibo();

    this.recibos = this.config.data.recibos;

    this.loadingUsuario = true;
    this.listaUsuarios.push({ coUsua: 0, noUsua: '(TODOS)' });
    this.usuarioService.getUsuariosPorSistema( 0 ).pipe( takeUntil( this.unsubscribe$ ) )
        .subscribe({
          next: ( data ) => {
            this.listaUsuarios.push(...data);
            this.loadingUsuario = false;
          },
          error: () => {
            this.loadingUsuario = false;
          }
        });
  }

  grabar() {
    this.bodyEstadoRecibo.coUsuaNout = this.codigoUsuario;
    this.bodyEstadoRecibo.trama = this.recibos?.map( value => {
      return { id: value.idReciIngr?.toString() }
    });
    console.log('bodyEstadoRecibo', this.bodyEstadoRecibo);

    const textoAnular = (this.recibos?.length === 1) ? 'el Recibo' : 'los Recibos';

    this.confirmService.confirm({
      message: `¿Estás seguro que deseas cambiar a estado NO UTILIZADO ${textoAnular} de Ingreso?`,
      accept: () => {
        this.isLoading = true;

        this.reciboService.cambiarEstadoReciboIngreso( this.bodyEstadoRecibo ).subscribe({
          next: ( d:any ) => {
            console.log('d', d);
            if ( d.codResult < 0 ) {
              this.messageService.add({
                severity: 'warn',
                summary: 'Atención',
                detail: d.msgResult,
                sticky: true
              });

              this.isLoading = false;
              this.bodyEstadoRecibo.onReset();
              return;

            } else if ( d.codResult === 0 ) {
              this.isLoading = false;
              this.ref.close('accept');
            }
          },

          error:( _d )=>{
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
            this.bodyEstadoRecibo.onReset();
          }
        });
      },
      reject: () => {
        this.bodyEstadoRecibo.onReset();
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }
}
