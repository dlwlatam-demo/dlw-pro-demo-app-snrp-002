import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ReciboIngresoService } from 'src/app/services/mantenimientos/documentos/recibo-ingreso.service';
import { ReciboIngreso } from 'src/app/models/mantenimientos/documentos/recibo-ingreso.model';
import { EnviaFirma } from '../../../../../models/mantenimientos/documentos/envia-firma.model';
import { DocumentoFirmaService } from '../../../../../services/mantenimientos/documentos/documento-firma.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';
import { GeneralService } from '../../../../../services/general.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-solicitar-firma-recibo',
  templateUrl: './solicitar-firma-recibo.component.html',
  styleUrls: ['./solicitar-firma-recibo.component.scss'],
  providers: [
    MessageService
  ]
})
export class SolicitarFirmaReciboComponent implements OnInit {
  firma!: FormGroup;

  isLoading!: boolean;

  documento!: ReciboIngreso[];
  guidDocumento!: string;
  doc: any;
  
  docVisto: boolean = false;
  roles?: string[];
  perfil?: string;
  
  constructor(
    private fb: FormBuilder,
    private config: DynamicDialogConfig,
    private ref: DynamicDialogRef,
    private messageService: MessageService,
    private reciboIngresoService: ReciboIngresoService,
	  private documentoFirmaService: DocumentoFirmaService,
    private validarService: ValidatorsService,
    private utilService: UtilService,
	  private generalService: GeneralService) { }

  ngOnInit(): void {
  
  this.documento = this.config.data.csta as ReciboIngreso[];
	this.guidDocumento = this.config.data.guidDocumento;
	this.doc = this.config.data.doc;

    this.firma = this.fb.group({
      nroRecib: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      nroSiaf: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]]
    });

    this.reciboIngresoService.getReciboIngresoById( this.documento['0'].idReciIngr! ).subscribe({
      next: ( data ) => {
        this.hidrate( data as ReciboIngreso )
      },
      error: ( e ) => {
        console.log( e );
      }
    });
  }


  hidrate( d: ReciboIngreso ) {
    this.firma.disable();

    const moDepo = d.imReciIngr.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
    
    const fecha = this.validarService.reFormateaFecha( d.feReciIngr )

    this.firma.get('nroRecib')?.setValue( d.nuReciIngr );
    this.firma.get('fecha')?.setValue( fecha );
    this.firma.get('nroSiaf')?.setValue( d.nuSiaf );
    this.firma.get('monto')?.setValue( moDepo );
  }

  grabar() {
    console.log('Solicitando Firma');
    
    this.isLoading = true;

    this.postAdjunto();
	
  }
  
   postAdjunto() {
    this.roles = JSON.parse( localStorage.getItem('roles_sarf')! );
    this.roles?.forEach( r => {
      this.perfil = r;
    });

    const token = localStorage.getItem('t_sarf')!;
	
	let nomDocuPdf = this.documento['0'].noDocuPdf!;
	if(nomDocuPdf == null || nomDocuPdf.trim() == "") {
		nomDocuPdf = "ReciboIngreso" + this.documento['0'].idReciIngr! + ".pdf";
	}
    
    const data = {
      idGuidDocu: this.guidDocumento,
      deDocuAdju: nomDocuPdf,
      coTipoAdju: '001', // Recibo de Ingreso
      coZonaRegi: this.documento['0'].coZonaRegi!,
      coOficRegi: this.documento['0'].coOficRegi!
    }

    this.generalService.grabarAdjunto( data ).subscribe({
      next: ( _d ) => {
	  
		let dataFirma: EnviaFirma = new EnviaFirma();
		dataFirma.tiDocu = data.coTipoAdju;
		dataFirma.idDocuOrig = this.documento['0'].idReciIngr!;
		dataFirma.noDocuPdf = data.deDocuAdju;
		dataFirma.noRutaDocuPdf = this.documento['0'].noRutaDocuPdf!;
		dataFirma.idGuidDocu = data.idGuidDocu;
	
       this.documentoFirmaService.setDocumentoFirma(dataFirma).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0 ) {
				this.generalService.eliminarAdjunto( this.guidDocumento, this.perfil!, token! ).subscribe({
					next: ( d1 ) => { 
						this.docVisto = false;
						this.messageService.add({
              severity: 'info',
              summary: 'Atención',
              detail: d.msgResult,
              sticky: true
            });
					}
					});
              
            } else if (d.codResult === 0 ) {
				        this.ref.close('accept');
            }
          },

          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              this.messageService.add({
                severity: 'info',
                summary: 'Atención',
                detail: d.error.description,
                sticky: true
              });

            }else {
              this.messageService.add({
                severity: 'warn',
                summary: 'Atención',
                detail: 'El servicio no esta disponible en estos momentos, inténtelo mas tarde.',
                sticky: true
              });
            }
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      },
      error: ( e ) => {
        console.log( e );
        this.ref.close('reject');
      }
    });
  }
  
  

  cancelar() {
    this.ref.close('');
  }
  
  verDocumento() {
	console.log('verDocumento');

	const byteCharacters = atob(this.doc!);
	const byteNumbers = new Array(byteCharacters.length);
	for (let i = 0; i < byteCharacters.length; i++) {
	  byteNumbers[i] = byteCharacters.charCodeAt(i);
	}
	const byteArray = new Uint8Array(byteNumbers);
	const blob = new Blob([byteArray], { type: 'application/pdf' });
	const blobUrl = URL.createObjectURL(blob);
	const download = document.createElement('a');
	download.href =  blobUrl;
	//download.setAttribute('download', 'Constacia_Abono');
	download.setAttribute("target", "_blank");
	document.body.appendChild( download );
	download.click();
	
	this.docVisto = true;
	
  }
  
}
