import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray  } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, map, of, takeUntil, Subject } from 'rxjs';
import { ReciboIngresoService } from 'src/app/services/mantenimientos/documentos/recibo-ingreso.service';
import { ReciboIngreso } from 'src/app/models/mantenimientos/documentos/recibo-ingreso.model';
import { GeneralService } from 'src/app/services/general.service';
import { environment } from '../../../../../../environments/environment';
import Swal from 'sweetalert2';
import { Dropdown } from 'primeng/dropdown';
import { CuentaContableService } from '../../../../../services/mantenimientos/maestras/cuenta-contable.service';
import { PlanCuentaContable } from 'src/app/models/mantenimientos/maestras/plan-cuenta.model';
import { ConfigDocumentoService } from '../../../../../services/mantenimientos/configuracion/config-documento.service';
import { ClasificadorService } from '../../../../../services/mantenimientos/maestras/clasificador.service';
import { IClasificador } from '../../../../../interfaces/configuracion/clasificador.interface';
import { Funciones } from '../../../../../core/helpers/funciones/funciones';
import { DialogService } from 'primeng/dynamicdialog';
import { VentanaNombresComponent } from '../ventana-nombres/ventana-nombres.component';
import { IParametros } from '../../../../../interfaces/general.interface';
import { UtilService } from '../../../../../services/util.service';
import { IBeneRecibo } from '../../../../../interfaces/documentos/recibo-ingreso.interface';
import { TipoCuentaContable } from '../../../../../interfaces/configuracion/configurar-documento.interface';

@Component({
  selector: 'app-nuevo-recibo',
  templateUrl: './nuevo-recibo.component.html',
  styleUrls: ['./nuevo-recibo.component.scss'],
  providers: [
    DialogService
  ]
})
export class NuevoReciboComponent implements OnInit {
  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;
  @ViewChild('ddlTipoRecibo') ddlTipoRecibo: Dropdown;
  @ViewChild('ddlBancoCargo') ddlBancoCargo: Dropdown;
  @ViewChild('ddlCuentaAbono') ddlCuentaAbono: Dropdown;
  @ViewChild('imReciIngr') imReciIngr: ElementRef;
  @ViewChild('idRazoSoc') idRazoSoc: ElementRef;

  $userCode:any
  $contables: any = [];
  $clasificadores: any = [];
  isDisabled:boolean= true;
  nuevoRecibo!: FormGroup;
  $estadoRecibo!: Observable<any>;
  $tipoRecibo: any = [{ idTipo  : '*', noTipo: '(SELECCIONE)' }]
  $zonas!: Observable<any>;
  $oficinas: Observable<any> = of([ { coOficRegi  : '*', deOficRegi: '(SELECCIONE)' }]);
  $local: Observable<any>= of([  { coLocaAten  : '*', deLocaAten: '(SELECCIONE)' }]);
  $bancos!: Observable<any>;
  tramaCont: any = [];
  tramaClsf: any = [];
  parametros: IParametros;

  dataCuentaContable: TipoCuentaContable[] = [];

  consultar: boolean = false;
  title!: string;
  msg!: string;

  $cuentasBancarias: any = [{  nuCuenBanc: '(SELECCIONE)' }];

  unsubscribe$ = new Subject<void>();

  constructor( 
    private router: Router,
    private fb: FormBuilder,
    private funciones: Funciones,
    private utilService: UtilService,
    private dialogService: DialogService,
    private generalService: GeneralService,
    private reciboService: ReciboIngresoService ,
    private contableService: CuentaContableService,
    private clasificadorService: ClasificadorService,
    private configuracionService: ConfigDocumentoService
  ) { }

  ngOnInit(): void {
    this.contableService.getBandejaCuentaContable('', '', 'A').subscribe({
      next: ( data: PlanCuentaContable[] ) => {
        this.$contables = data
      }
    });

    this.clasificadorService.getBandejaClasificadores(null, null, 'A').subscribe({
      next: ( data: IClasificador[] ) => {
        this.$clasificadores = data;
      }
    });

    const zonasItem: any = { coZonaRegi : '*', deZonaRegi: '(SELECCIONE)' };
    const estadoReciboItem: any = { ccodigoHijo  : '*', cdescri: '(SELECCIONE)' };
    const tipoReciboItem: any = { idTipo  : '*', noTipo: '(SELECCIONE)' };
    const bancosItem: any = { nidInstitucion  : '*', cdeInst: '(SELECCIONE)' };

    this.$userCode = localStorage.getItem('user_code')||'';
    this.$bancos = this.generalService.getCbo_Bancos().pipe(
      map( d => {
        d.unshift( bancosItem );
        return d;
      })
    );

    this.reciboService.getTipoReciboIngreso('1').subscribe({
      next: ( data ) => {
        data.unshift( tipoReciboItem );
        this.$tipoRecibo = data
      },
      error: ( e ) => {
        this.$tipoRecibo =[tipoReciboItem];
      }
    });

    this.nuevoRecibo = this.fb.group({
      zona: [{
          value:  '*',
          disabled: this.router.url.split('/').includes('editar')  ||  this.router.url.split('/').includes('consultar') ? true :false 
        } , [ Validators.required ]],
      oficina: [{
          value:  '*',
          disabled: this.router.url.split('/').includes('editar')  ||  this.router.url.split('/').includes('consultar') ? true :false 
        } , [ Validators.required ]],
      local: [{
          value:  '*',
          disabled: this.router.url.split('/').includes('editar')  ||  this.router.url.split('/').includes('consultar') ? true :false 
        } , [ Validators.required ]],
      tipoRecibo: [{
          value:  '*',
          disabled: this.router.url.split('/').includes('editar') ||  this.router.url.split('/').includes('consultar') ? true :false 
        } , [ Validators.required ]],
      nroRecibo: [  {
        value:  '',
        disabled: true 
    },[ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]],
      banco: ['', [ Validators.required ]],
      nroCuenta: ['', [ Validators.required ]],
      nroSIAF: ['', [ Validators.required, Validators.pattern('^[0-9]+') ]],
      concepto: ['', [ Validators.required ]],
      nombre: ['', [ Validators.required ]],
      detallePresupuestal: this.fb.array([
        this.fb.group({
          clasificador: ['', [ Validators.required ]],
          detClasificador: ['', [ Validators.required ]],
          debe: ['', [ Validators.required , Validators.pattern('^[0-9]+([.][0-9]{1,2})?$')]],
          haber: ['', [ Validators.required , Validators.pattern('^[0-9]+([.][0-9]{1,2})?$')]],
          id:[0, [ ]],
        })
      ]),
      detallePatrimonial: this.fb.array([
        this.fb.group({
          cuenta: ['', [ Validators.required, Validators.pattern('^[0-9]+') ]],
          descripcion: ['', [ Validators.required ]],
          debe: ['', [ Validators.required , Validators.pattern('^[0-9]+([.][0-9]{1,2})?$')]],
          haber: ['', [ Validators.required , Validators.pattern('^[0-9]+([.][0-9]{1,2})?$')]],
          id:[0, [ ]],

        })
      ])
    });

    this.obtenerParametrosDelSistema();

    this.$zonas = this.generalService.getCbo_Zonas_Usuario(this.$userCode).pipe(
      map( d => {
        if (d.length === 1) {
          this.nuevoRecibo.get('zona')?.setValue(d[0].coZonaRegi);
          this.buscarOficina();
        }
        d.unshift( zonasItem );
        return d;
      })
    );
    this.$estadoRecibo = this.generalService.getCbo_EstadoRecibo().pipe(
      map( d => {
        d.unshift( estadoReciboItem );
        return d;
      })
    );

    if ( this.router.url.split('/').includes('editar') ) {
      this.title = 'Editar';
      this.msg = 'actualizar';
      this.editarRecibo();
    } else if ( this.router.url.split('/').includes('consultar') ) {
      this.title = sessionStorage.getItem('nombre')!;
      this.consultar = true;
      this.editarRecibo();
   
    } else {
      this.title = 'Nuevo';
      this.msg = 'crear';
      const hoy = new Date();
      this.nuevoRecibo.get('fecha')?.setValue( hoy );
      this.cuentaPres.clear();
      this.cuentaPatr.clear();
    }
  }

  get detallePresupuestalArray(): FormArray | null { return  this.nuevoRecibo.get('detallePresupuestal') as FormArray }
  get detallePatrimonialArray(): FormArray | null { return  this.nuevoRecibo.get('detallePatrimonial') as FormArray }
  get cuentasPresupuestales(): FormGroup[] {
    return <FormGroup[]>this.detallePresupuestalArray?.controls
  }
  get cuentasPatrimoniales(): FormGroup[] {
    return <FormGroup[]>this.detallePatrimonialArray?.controls
  }

  get cuentaPres(): FormArray { return <FormArray>this.nuevoRecibo.get('detallePresupuestal') }
  get cuentaPatr(): FormArray { return <FormArray>this.nuevoRecibo.get('detallePatrimonial')}

  onChangeCuenta1( event: any ) {
    const cuentasBancariasItem: any = {  nuCuenBanc: '(SELECCIONE)' };
    if (event.value === '*') {
      this.$cuentasBancarias =[cuentasBancariasItem];
    } else {
      this.generalService.getCbo_Cuentas( event.value ).subscribe({
        next: ( data ) => {
          data.unshift( cuentasBancariasItem );
          this.$cuentasBancarias = data;
          // this.cuentaPres.clear();
          // this.cuentaPatr.clear();
        },
        error: ( e ) => {
          // this.cuentaPres.clear();
          // this.cuentaPatr.clear();
          this.$cuentasBancarias =[cuentasBancariasItem];
        }
      });
    }
  }

  buscarOficina() {
    const zonaValor = this.nuevoRecibo.get('zona')?.value;
    const oficinasItem: any = { coOficRegi  : '*', deOficRegi: '(SELECCIONE)' };
    if (zonaValor === '*') {
      this.$oficinas =of([  oficinasItem ]);
    }
    else{
      this.$oficinas = this.generalService.getCbo_Oficinas_Zonas(this.$userCode,this.nuevoRecibo.get('zona')?.value).pipe(
        map( d => {
          if (d.length === 1) {
            this.nuevoRecibo.get('oficina')?.setValue(d[0].coOficRegi);
            this.buscarLocal();
          }
          d.unshift( oficinasItem );
          return d;
        })
      );
    }
    this.$local =  of([  { coLocaAten  : '*', deLocaAten: '(SELECCIONE)' }]);
  }

  buscarLocal() {
    const localItem: any = { coLocaAten  : '*', deLocaAten: '(SELECCIONE)' };
    const zonaValor = this.nuevoRecibo.get('zona')?.value;
    const oficinaValor = this.nuevoRecibo.get('oficina')?.value;
    if (oficinaValor === '*') {
      this.$local =  of([  localItem ]);
    }else {
      this.$local = this.generalService.getCbo_Locales_Ofic(this.$userCode,zonaValor,oficinaValor).pipe(
        map( d => {
          if (d.length === 1) {
            this.nuevoRecibo.get('local')?.setValue(d[0].coLocaAten);
          }
          d.unshift( localItem );
          return d;
        })
      );
    }
  }

  obtenerParametrosDelSistema() {
    this.generalService.getParametrosDelSistema(18).pipe( takeUntil( this.unsubscribe$ ) )
        .subscribe({
          next: ( data ) => {
            this.parametros = data[0];
            this.nuevoRecibo.get('nombre')?.setValue( data[0].noValo1 );
          },
          error: () => {
            this.parametros = null;
          }
        });
  }

  editarRecibo() {
    this.utilService.onShowProcessLoading('Cargando la información');

    const idReciIngr = sessionStorage.getItem('idReciIngr');
    this.reciboService.getReciboIngresoById( Number(idReciIngr)).subscribe({
      next: ( data ) => {
        console.log('dataRec', data);
        this.goEdit( data as ReciboIngreso );
      }
    });
  }
  
  goEdit( d: ReciboIngreso ) {
    const oficinasItem: any = { coOficRegi  : '*', deOficRegi: '(SELECCIONE)' };
    const zonaValor = d.coZonaRegi ||'';
    const oficinaValor =d.coOficRegi||'';
    this.$oficinas = this.generalService.getCbo_Oficinas_Zonas(this.$userCode, zonaValor ).pipe(
      map( d => {
        d.unshift( oficinasItem );
        return d;
      }));
     const localItem: any = { coLocaAten  : '*', deLocaAten: '(SELECCIONE)' };
      this.$local = this.generalService.getCbo_Locales_Ofic(this.$userCode,zonaValor,oficinaValor).pipe(
        map( d => {
          d.unshift( localItem );
          return d;
        })
      );
      const cuentasBancariasItem: any = {  nuCuenBanc: '(SELECCIONE)' };
      this.generalService.getCbo_Cuentas( d.idBanc!).subscribe({
        next: ( data ) => {
          data.unshift( cuentasBancariasItem );
          this.$cuentasBancarias = data
        },
        error: ( _e ) => {
          this.$cuentasBancarias =[cuentasBancariasItem];
        }
      });

      setTimeout(() => {
        const cDateD = new Date(d.feReciIngr||'');
        this.nuevoRecibo.get('zona')?.setValue( d.coZonaRegi );
        this.nuevoRecibo.get('oficina')?.setValue( d.coOficRegi );
        this.nuevoRecibo.get('local')?.setValue( d.coLocaAten );
        this.nuevoRecibo.get('nroRecibo')?.setValue( d.nuReciIngr );
        this.nuevoRecibo.get('fecha')?.setValue( cDateD );
        this.nuevoRecibo.get('tipoRecibo')?.setValue( d.idTipoReciIngr );
        this.nuevoRecibo.get('monto')?.setValue( d.imReciIngr?.toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }) );
        this.nuevoRecibo.get('banco')?.setValue( d.idBanc );
        this.nuevoRecibo.get('nroCuenta')?.setValue( d.nuCuenBanc  );
        this.nuevoRecibo.get('nroSIAF')?.setValue( d.nuSiaf );
        this.nuevoRecibo.get('nombre')?.setValue( d.noBene.trim() );
        this.nuevoRecibo.get('concepto')?.setValue( d.obConc );
  
        if (d.deEsta==='ANULADO') {
          this.nuevoRecibo.disable();
          this.consultar = true;
        }
        if ( ['NO UTILIZADO','RESERVADO'].includes(d.deEsta) ) {
          this.nuevoRecibo.get('tipoRecibo').enable();
        }

        if ( d.detalleClasificadores?.length!  > 0 ) {
          this.cuentaPres.clear();
        }

        if ( d.cuentasContables?.length!  > 0 ) {
          this.cuentaPatr.clear();
        }

        d.detalleClasificadores?.forEach( clsf => {
          const clsfDes = this.$clasificadores.find( (c: any ) => c.coClsf === clsf.coClsf );

          this.cuentaPres.push( 
            this.fb.group({ 
              clasificador: [ clsf.coClsf, [ Validators.required ]],
              detClasificador: [clsfDes?.deClsf, [ Validators.required ]],
              debe: [ Number( clsf.imDebe ).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
              }), [ Validators.required ]],
              haber: [ Number( clsf.imHabe ).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
              }), [ Validators.required ]],
              id:[ clsf.idReciIngrClsf, [ Validators.required]],

            })
          )
        });
    
        d.cuentasContables?.forEach( cuent => {
          const cuentaDes = this.$contables.find((c:any)=> c.coCuenCont === cuent.coCuenCont);
          
          this.cuentaPatr.push( 
            this.fb.group({ 
              cuenta: [ cuent.coCuenCont, [ Validators.required ]],
              descripcion: [cuentaDes?.noCuenCont, [ Validators.required ]],
              debe: [ cuent.imDebe.toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
              }), [ Validators.required ]],
              haber: [ cuent.imHabe.toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
              }), [ Validators.required ]],
              id:[ cuent.idReciIngrCont, [ Validators.required]],
            })
          )
        });
        if ( this.router.url.split('/').includes('consultar') ) {
          this.nuevoRecibo.disable();
        }
        this.utilService.onCloseLoading();
      }, 2000);

  }

  onchangeTipoRecibo (event:any){
    this.cuentaPres.clear();
    this.cuentaPatr.clear();
    
    const montoRecibo = this.nuevoRecibo.get('monto')?.value;
    const tipoReciboSelect = this.$tipoRecibo.find((e:any)=> e.idTipo === event.value);
    console.log("tipoReciboSelect", tipoReciboSelect);

      if (tipoReciboSelect) {

        Swal.showLoading();
        const cuentasBancariasItem: any = {  nuCuenBanc: '(SELECCIONE)' };
        this.generalService.getCbo_Cuentas( tipoReciboSelect.idBanc!).subscribe({
          next: ( data ) => {
            data.unshift( cuentasBancariasItem );
            this.$cuentasBancarias = data;
          },
          error: ( _e ) => {
            this.$cuentasBancarias =[cuentasBancariasItem];
          }
        });

        this.configuracionService.getConfiguracionById('1', tipoReciboSelect.idTipo ).subscribe({
          next: ( data ) => {
            console.log('dataConfig', data );
            const monto = !(montoRecibo === '' || montoRecibo === '0.00') ? montoRecibo : Number('0').toFixed(2);
            
            data.listTipoDocumentoClsf.forEach( clsf => {
              this.cuentaPres.push( 
                this.fb.group({ 
                  clasificador: [ clsf.coClsf, [ Validators.required ]],
                  detClasificador: [clsf.deClsf, [ Validators.required ]],
                  debe: [ monto, [ Validators.required ]],
                  haber: [ monto , [ Validators.required ]],
                  id:[ 0, [ Validators.required]],
  
                })
              )
            });

            data.listTipoDocumentoCont.forEach( cont => {
              const impDebe = cont.tiDebeHabe === 'D' ? monto : Number('0').toFixed(2);
              const impHabe = cont.tiDebeHabe === 'H' ? monto : Number('0').toFixed(2);

              this.cuentaPatr.push( 
                this.fb.group({ 
                  cuenta: [ cont.coCuenCont, [ Validators.required ]],
                  descripcion: [cont.noCuenCont, [ Validators.required ]],
                  debe: [ impDebe, [ Validators.required ]],
                  haber: [ impHabe, [ Validators.required ]],
                  id:[ 0, [ Validators.required]],
                })
              )
            });

            this.dataCuentaContable = data.listTipoDocumentoCont;
          },
          error: ( e ) => {
            this.cuentaPres.clear();
            this.cuentaPatr.clear();
          }
        });
        setTimeout(() => {
          this.nuevoRecibo.get('banco')?.setValue( tipoReciboSelect.idBanc );
          this.nuevoRecibo.get('concepto')?.setValue( tipoReciboSelect.obConce );
          this.nuevoRecibo.get('nroCuenta')?.setValue( tipoReciboSelect.nuCuenBanc );
          Swal.close();

        }, 1000);

        
      }
  }

  addcuentaPresupuestal() {
    const ar = ( this.nuevoRecibo.get('detallePresupuestal') as FormArray )
    if ( environment.cantidadCuentas > ar.length )
      ar.push(
        this.fb.group({
          clasificador: ['', [ Validators.required ]],
          detClasificador: ['', [  Validators.required ,]],
          debe: ['', [ Validators.required ]],
          haber: ['', [ Validators.required ]],
          id:[0, []],
        })
      )
  }

  deletecuentaPresupuestal( index: number ) {
    ( this.nuevoRecibo.get('detallePresupuestal') as FormArray ).removeAt(index)
  }

  addcuentaPatrimonial() {
    const ar = ( this.nuevoRecibo.get('detallePatrimonial') as FormArray )
    if ( environment.cantidadCuentas > ar.length )
      ar.push(
        this.fb.group({
          cuenta: ['', [ Validators.required ]],
          descripcion: ['', [ Validators.required ]],
          debe: ['', [ Validators.required ]],
          haber: ['', [ Validators.required ]],
          id:[0, []],
        })
      )
  }

  deletecuentaPatrimonial( index: number ) {
    ( this.nuevoRecibo.get('detallePatrimonial') as FormArray ).removeAt(index)
  }

  saveOrUpdate() {

    let resultado: boolean = this._validarFormulario();
    if (!resultado) {
      return;
    }

    this.tramaCont = [];
    this.tramaClsf = [];

    Swal.fire({
      title: 'Recibos de Ingreso',
      text: `¿Está Ud. seguro de ${ this.msg } el registro?`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
    })
    .then( result => {
      if ( result.isConfirmed ) {
        for ( let clsf of this.nuevoRecibo.get('detallePresupuestal')?.value ) {
          if ( clsf.clasificador != '' &&  clsf.detClasificador != '') {
            const haber = this.funciones.convertirTextoANumero( clsf.haber );
            const debe = this.funciones.convertirTextoANumero( clsf.debe );

            this.tramaClsf.push({
              coClsf: clsf.clasificador,
              imHabe: Number(haber).toFixed(2),
              imDebe: Number(debe).toFixed(2),
              idReciIngrClsf: clsf.id
            });
          }
        
        }

        for ( let cont of this.nuevoRecibo.get('detallePatrimonial')?.value ) {
          if ( cont.cuenta != '' &&  cont.descripcion != '') {
            const haber = this.funciones.convertirTextoANumero( cont.haber );
            const debe = this.funciones.convertirTextoANumero( cont.debe );

            this.tramaCont.push({
              coCuenCont: cont.cuenta,
              imHabe: Number( haber ).toFixed(2),
              imDebe: Number( debe ).toFixed(2),
              id: cont.id
            });
          }
        
        }

        const zonaRegistral = this.nuevoRecibo.get('zona')?.value; 
        const oficinaRegistral = this.nuevoRecibo.get('oficina')?.value; 
        const localAtencion = this.nuevoRecibo.get('local')?.value;
        const cuentaBancaria =  this.nuevoRecibo.get('nroCuenta')?.value
        const montoReciIngr: string = this.funciones.convertirTextoANumero( this.nuevoRecibo.get('monto')?.value )
        const fecha:string = this.nuevoRecibo.get('fecha')?.value;
        const fechaFormat:string = `${new Date(fecha).toLocaleDateString('es-PE')}`;
        const id = sessionStorage.getItem('idReciIngr') ? sessionStorage.getItem('idReciIngr') : 0;
        const dataBancaria = {
          idReciIngr: Number(id),
          coZonaRegi: zonaRegistral === '*' ? '' : zonaRegistral,
          coOficRegi: oficinaRegistral === '*' ? '': oficinaRegistral,
          coLocaAten: localAtencion === '*' ? '' : localAtencion,
          idTipoReciIngr: this.nuevoRecibo.get('tipoRecibo')?.value,
          feReciIngr: fechaFormat,
          imReciIngr: Number( montoReciIngr ),
          idBanc: this.nuevoRecibo.get('banco')?.value,
          nuCuenBanc: cuentaBancaria === '(SELECCIONE)' ? '' : cuentaBancaria,
          nuSiaf: this.nuevoRecibo.get('nroSIAF')?.value,
          noBene: this.nuevoRecibo.get('nombre')?.value,
          obConc: this.nuevoRecibo.get('concepto')?.value,
          noDocuPdf: '',
          noRutaDocuPdf: '',
          trama: this.tramaCont,
          tramaClsf: this.tramaClsf
        }
        console.log('dataBancaria', dataBancaria);
        let continuar = false;
        this.reciboService.createOrUpdateCuenta( this.msg, dataBancaria ).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0) {
              Swal.fire( `${d.msgResult}`, '', 'info' );
            } else {
              Swal.fire( 'Recibos de Ingreso','Los datos se grabaron correctamente', 'info' ).then(()=>{
                this.router.navigate(['SARF/mantenimientos/documentos/recibos_de_ingreso/bandeja']);
              });
            }
          },
          error: ( e ) => {
            Swal.fire( 'Recibos de Ingreso','Error al grabar los datos',  'error' );

          },
          complete: () => {
            if (continuar) {
              this.router.navigate(['SARF/mantenimientos/documentos/recibos_de_ingreso/bandeja']);
            }
          }
        });
      }
    });
  }

  cancelar() {
    sessionStorage.removeItem('idReciIngr');
    this.router.navigate(['SARF/mantenimientos/documentos/recibos_de_ingreso/bandeja']);
  }

  buscarClasificadores(event:any,$tipoCuenta:string,i:number){
    if (event.target.value !== '') {
      const controlName = ( this.nuevoRecibo.get($tipoCuenta) as FormArray );
      const valorArr = controlName.value;
      const valorArrAux = valorArr.map(( val: any, indx: number ) => {
        const clsfDes = this.$clasificadores.find((clsf: any) => clsf.coClsf === val.clasificador);
        val.detClasificador =  clsfDes ? clsfDes.deClsf : val.deClsf;

        const debe = this.funciones.convertirTextoANumero( val.debe );
        val.debe  = Number(debe).toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });

        const haber = this.funciones.convertirTextoANumero( val.haber );
        val.haber = Number(haber).toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        return val;
      });
      controlName.setValue(valorArrAux);
    }
  }

  buscarCuentabancariaPresupuestal (event:KeyboardEvent,$event1:any,i:number){
    // const pattern = /^[0-9]+([.][0-9]{1,2})?$/ ///[0-9]/;
    // const inputChar = String.fromCharCode(event.charCode);
    // if (!pattern.test(inputChar)) {
    //     event.preventDefault();
    // }
    if (event.keyCode === 13) {
      this.buscarClasificadores($event1,'detallePresupuestal',i);
    }
  }

  buscarCuentabancariaPresupuestalBlur(event:any,i:number){
    this.buscarClasificadores(event,'detallePresupuestal',i);
  }

  buscarCuentaContable(event:any,$tipoCuenta:string,i:number){
    if (event.target.value !== '') {
      const controlName = ( this.nuevoRecibo.get($tipoCuenta) as FormArray );
      const valorArr = controlName.value;
      const valorArrAux= valorArr.map((val:any,indx:number)=>{
        const cuentaDes = this.$contables.find((cuent:any)=>  Number(cuent.coCuenCont) === Number(val.cuenta)) ;
        val.descripcion =  cuentaDes ? cuentaDes.noCuenCont : val.descripcion;
        
        const debe = this.funciones.convertirTextoANumero( val.debe );
        val.debe  = Number(debe).toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        
        const haber = this.funciones.convertirTextoANumero( val.haber );
        val.haber = Number(haber).toLocaleString('en-US' ,{
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        return val;
      });
      controlName.setValue(valorArrAux);
    }
  }

  buscarCuentabancariaPatrimoniales (event:KeyboardEvent,$event1:any,i:number){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
    if (event.keyCode === 13) {
      this.buscarCuentaContable($event1,'detallePatrimonial',i);
    }
  }
  
  buscarCuentabancariaPatrimonialesBlur(event:any,i:number){
    this.buscarCuentaContable(event,'detallePatrimonial',i);
  }

  public abrirVentanaNombre() {
    const dialog = this.dialogService.open( VentanaNombresComponent, {
      header: 'Consulta de Beneficiarios',
      width: '50%',
      closable: false
    });

    dialog.onClose.pipe( takeUntil( this.unsubscribe$ ) )
          .subscribe( ( bene: IBeneRecibo ) => {
            console.log('bene', bene);
            if ( bene !== null ) {
              this.nuevoRecibo.get('nombre')?.setValue( bene.noBene );
            }
          });
  }

  validarMonto(event:any){
    // Original: /^(-?\d*)((\.(\d{0,2})?)?)$/i
    const pattern = /^(-?\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode(event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if ( !pattern.test(inputChar) || count === 2) {
      event.preventDefault();
    }
  }

  validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  formatearNumero(event:any) {
    if (event.target.value) {
      const nuevoValor = this.funciones.convertirTextoANumero( event.target.value );
      const number = Number(nuevoValor).toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });

      this.nuevoRecibo.get('monto')?.setValue( number );

      this.cuentasPresupuestales.forEach( pres => {
        pres.get('debe')?.setValue( number );
        pres.get('haber')?.setValue( number );
      });

      const contablesList = this.dataCuentaContable.entries();
      for ( const [idx, cont] of contablesList ) {
        if ( cont.tiDebeHabe === 'D' ) {
          this.cuentasPatrimoniales[idx].get('debe')?.setValue( number );    
        }

        if ( cont.tiDebeHabe === 'H' ) {
          this.cuentasPatrimoniales[idx].get('haber')?.setValue( number );
        }
      }
    } else {
      this.cuentasPresupuestales.forEach( pres => {
        pres.get('debe')?.setValue( '0.00' );
        pres.get('haber')?.setValue( '0.00' );
      });

      this.cuentasPatrimoniales.forEach( patr => {
        patr.get('debe')?.setValue( '0.00' );
        patr.get('haber')?.setValue( '0.00' );
      });
    }
  }

  formatearNumeroHabe(event:any){
    if (event.target.value) {
      const nuevoValor = this.funciones.convertirTextoANumero( event.target.value );
      event.target.value = Number(nuevoValor).toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });
    } 
  }

  private _validarFormulario(): boolean {
    if (this.nuevoRecibo.value.zona === "" || this.nuevoRecibo.value.zona === "*" || this.nuevoRecibo.value.zona === null) {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una Zona Registral.");
      return false;  
    } else if (this.nuevoRecibo.value.oficina === "" || this.nuevoRecibo.value.oficina === "*" || this.nuevoRecibo.value.oficina === null) {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una Oficina Registral.");
      return false;      
    } else if (this.nuevoRecibo.value.local === "" || this.nuevoRecibo.value.local === "*" || this.nuevoRecibo.value.local === null) {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Local.");
      return false;      
    } else if (this.nuevoRecibo.value.tipoRecibo === "" || this.nuevoRecibo.value.tipoRecibo === "*" || this.nuevoRecibo.value.tipoRecibo === null) {      
      this.ddlTipoRecibo.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Tipo de Recibo.");
      return false;      
    } else if (this.nuevoRecibo.value.fecha === "" || this.nuevoRecibo.value.fecha === "*" || this.nuevoRecibo.value.fecha === null) {      
      document.getElementById('idFecha').focus();            
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una Fecha.");
      return false;      
    } else if (this.nuevoRecibo.value.banco === "" || this.nuevoRecibo.value.banco === "*" || this.nuevoRecibo.value.banco === null) {      
      this.ddlBancoCargo.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Banco.");
      return false;      
    } else if (this.nuevoRecibo.value.nroCuenta === "" || this.nuevoRecibo.value.nroCuenta === "*" || this.nuevoRecibo.value.nroCuenta === null) {      
      this.ddlCuentaAbono.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Número de Cuenta.");
      return false;      
    } else if (this.nuevoRecibo.value.monto === "" || this.nuevoRecibo.value.monto === "*" || this.nuevoRecibo.value.monto === null) {      
      this.imReciIngr.nativeElement.focus();
      this.utilService.onShowAlert("warning", "Atención", "Debe ingresar el Monto.");
      return false;      
    } else if (this.nuevoRecibo.value.nombre === "" || this.nuevoRecibo.value.nombre === "*" || this.nuevoRecibo.value.nombre === null) {      
      this.idRazoSoc.nativeElement.focus();
      this.utilService.onShowAlert("warning", "Atención", "Seleccione o digite un Nombre.");
      return false;      
    } else {
      return true;
    }
  }
}
