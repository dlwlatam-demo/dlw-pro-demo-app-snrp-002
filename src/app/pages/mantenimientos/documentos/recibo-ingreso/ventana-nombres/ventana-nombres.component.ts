import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { takeUntil, Subject } from 'rxjs';

import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';

import { IBeneRecibo } from '../../../../../interfaces/documentos/recibo-ingreso.interface';

import { BodyNombreRecibo } from '../../../../../models/mantenimientos/documentos/recibo-ingreso.model';

import { ReciboIngresoService } from '../../../../../services/mantenimientos/documentos/recibo-ingreso.service';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-ventana-nombres',
  templateUrl: './ventana-nombres.component.html',
  styleUrls: ['./ventana-nombres.component.scss'],
  providers: [
    MessageService
  ]
})
export class VentanaNombresComponent implements OnInit {

  public loadingNombres: boolean = false;

  public form!: FormGroup;

  public bodyNombres!: BodyNombreRecibo;

  public selectNombres: IBeneRecibo[] = [];
  public listNombresBandeja!: IBeneRecibo[];

  public currentPage: string = environment.currentPage;

  public unsubscribe$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private messageService: MessageService,
    private reciboIngresoService: ReciboIngresoService
  ) { }

  ngOnInit(): void {
    this.bodyNombres = new BodyNombreRecibo();

    this.form = this.fb.group({
      noBene: ['', []]
    });
  }

  public buscarNombres() {
    const noBene = this.form.get('noBene')?.value;

    if ( noBene === '' ) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Ingrese el nombre para realizar la búsqueda.',
        sticky: true
      });

      return;
    }

    this.bodyNombres.noBene = noBene;

    this._obtenerNombres( this.bodyNombres );
  }

  private _obtenerNombres( body: BodyNombreRecibo ) {
    this.loadingNombres = true;

    this.reciboIngresoService.getObtenerNombresRecibo( body ).pipe( takeUntil( this.unsubscribe$ ) )
        .subscribe({
          next: ( data ) => {
            this.loadingNombres = false;
            this.listNombresBandeja = data.lstReciboIngresoBene;
          },
          error: ( _err ) => {
            this.listNombresBandeja = [];
            this.loadingNombres = false;
          }
        });
  }

  public aceptar() {
    if ( this.selectNombres.length === 0 ) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Debe seleccionar un registro de la tabla.',
        sticky: true
      });

      return;
    }

    if ( this.selectNombres.length !== 1 ) {
      this.messageService.add({
        severity: 'warn',
        summary: 'No puede seleccionar más de un registro.',
        sticky: true
      });

      return;
    }

    this.ref.close( this.selectNombres[0] );
  }

  public cancelar() {
    this.ref.close( null );
  }

}
