import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { GeneralService } from '../../../../../services/general.service';
import { CartaOrdenService } from '../../../../../services/mantenimientos/documentos/carta-orden.service';
import { BodyEditarCartaOrden, BodyNuevaCartaOrden } from '../../../../../models/mantenimientos/documentos/carta-orden.model';
import { Dropdown } from 'primeng/dropdown';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { ICartaOrdenById, IDataFiltrosCartaOrden } from 'src/app/interfaces/carta-orden.interface';
import { UtilService } from 'src/app/services/util.service';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { ITipoCartaOrden, ITipoCartaVariables } from '../../../../../interfaces/configuracion/tipo-carta-orden.interface';
import { TipoCartaOrdenService } from '../../../../../services/mantenimientos/configuracion/tipo-carta-orden.service';
import Quill from 'quill';

@Component({
  selector: 'app-editar-carta',
  templateUrl: './editar-carta.component.html',
  styleUrls: ['./editar-carta.component.scss']
})
export class EditarCartaComponent implements OnInit, OnDestroy {
  @ViewChild('ddlGrupo') ddlGrupo: Dropdown;
  @ViewChild('ddlBancoCargo') ddlBancoCargo: Dropdown;
  @ViewChild('ddlCuentaCargo') ddlCuentaCargo: Dropdown;
  @ViewChild('ddlBancoAbono') ddlBancoAbono: Dropdown;
  @ViewChild('ddlCuentaAbono') ddlCuentaAbono: Dropdown;
  loadingTipoReferencia: boolean = false;
  loadingGrupos: boolean = false;
  loadingBancoCargo: boolean = false;
  loadingCuentaCargo: boolean = false;
  loadingBancoAbono: boolean = false;
  loadingCuentaAbono: boolean = false;
  readonlyNombre: boolean = true;
  editorInicio: string = '';
  editorConcepto: string = '';
  editorFinal: string = '';
  title: string;
  isEditar: boolean;
  userCode:any
  idEdit!: number;
  msg!: string;
  cartaOrdenData: ICartaOrdenById;
  carta!: FormGroup;
  idBancCargo!: number;
  unsubscribe$ = new Subject<void>();
  insertarVariable!: ITipoCartaVariables;
  bodyNuevaCartaOrden!: BodyNuevaCartaOrden;
  bodyEditarCartaOrden: BodyEditarCartaOrden
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[] = [];
  listaLocal: Local[] = [];
  listaTipoDocumento: ITipoCartaOrden[] = [];
  listaTipoReferencia: IResponse2[] = [];
  listaGrupos: IResponse2[] = [];
  listaVariables: ITipoCartaVariables[] = [];
  listaBancoCargo: any[] = [];
  listaCuentaCargo: any[] = [];
  listaBancoAbono: any[] = [];
  listaCuentaAbono: any[] = [];
  $editarCartaOrden: Subscription = new Subscription;
  $sharing: Subscription = new Subscription;

  quillInicio!: Quill;
  quillConcepto!: Quill;
  quillFinal!: Quill;

  constructor( 
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private generalService: GeneralService,
    private tipoCartaService: TipoCartaOrdenService,
    private cartaService: CartaOrdenService,
    public funciones: Funciones,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService,
  ) { }

  ngOnInit(): void {
    this._getIdByRoute();
    this.bodyNuevaCartaOrden = new BodyNuevaCartaOrden();
    this.bodyEditarCartaOrden = new BodyEditarCartaOrden();
    this.userCode = localStorage.getItem('user_code')||'';
    this.carta = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoCarta: ['', [ Validators.required ]],
      nroCarta: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]],
      nroSIAF: ['', [ Validators.required ]],
      nroComp: ['', [ Validators.required ]],
      tipoDocuRef: ['', [ Validators.required ]],
      nroDocuRef: ['', [ Validators.required ]],
      grupo: ['', [ Validators.required ]],
      destina: ['', [ Validators.required ]],
      cargo: ['', [ Validators.required ]],
      inicio: ['', [ Validators.required ]],
      concepto: ['', [ Validators.required ]],
      final: ['', [ Validators.required ]],
      bancoCargo: ['', [ Validators.required ]],
      cuentaCargo: ['', [ Validators.required ]],
      nombreCuentaCargo: ['', [ Validators.required ]],
      bancoAbono: ['', [ Validators.required ]],
      cuentaAbono: ['', []],
      nombreCuentaAbono: ['', []],
    });
  }

  private _getIdByRoute(): void {
    this.activatedRoute.params.pipe(takeUntil(this.unsubscribe$))
      .subscribe( params => this._getComprobantePagoById(params['id']))
  }

  private _getComprobantePagoById(id: string): void {
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.cartaService.cartaOrdenDataById(id).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          console.log('dataEdt', data);
          this.cartaOrdenData = data;
          this._handleInput();
          this._inicilializarListas();
          this._buscarTipoDocReferencia();
          this._buscarGruposCarta();
          this._obtenerVariables();
          this._buscarBancosCargo();
          this._buscarBancosAbono();
          this.utilService.onCloseLoading();
        },
        error: ( err ) => {
          console.log('err', err);
          this.utilService.onCloseLoading(); 
        }
      })
  }

  private _handleInput(): void {
    this.carta.get('zona')?.disable();
    this.carta.get('oficina')?.disable();
    this.carta.get('local')?.disable();
    this.carta.get('tipoCarta')?.disable();
    this.carta.get('nroCarta')?.disable();
    this.carta.get('nroCarta')?.setValue( this.cartaOrdenData.nuCartOrde );
    this.carta.get('destina')?.setValue( this.cartaOrdenData.noDest );
    this.carta.get('cargo')?.setValue( this.cartaOrdenData.noCarg );
    this.carta.get('inicio')?.setValue( this.cartaOrdenData.deSeccInic );
    this.carta.get('concepto')?.setValue( this.cartaOrdenData.deSeccConc );
    this.carta.get('final')?.setValue( this.cartaOrdenData.deSeccFina );
    this.carta.get('monto')?.setValue( this.cartaOrdenData.imCartOrde.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    }) );
    this.carta.get('nroSIAF')?.setValue( this.cartaOrdenData.nuSiaf );
    this.carta.get('nroComp')?.setValue( this.cartaOrdenData.nuCompPago );
    this.onChangeBancoCargo({value: this.cartaOrdenData.idBancCarg}, true);
    this.onChangeBancoAbono({value: this.cartaOrdenData.idBancAbon}, true);
    this.carta.get('nroDocuRef')?.setValue( this.cartaOrdenData.nuDocuRefe );
    this.carta.patchValue({ "fecha": new Date(this.cartaOrdenData.feCartOrde)});

    const cuentaSeleccionada = this.listaCuentaAbono.find(value => value.nuCuenBanc === this.cartaOrdenData.nuCuenBancAbon);
    !cuentaSeleccionada ? this.carta.get('cuentaAbono')?.setValue( this.cartaOrdenData.nuCuenBancAbon ) 
                        : this.carta.get('cuentaAbono')?.setValue( '(NINGUNO)' );
    !cuentaSeleccionada ? this.readonlyNombre = false : this.readonlyNombre = true;
  }
  
  private _inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaTipoReferencia = [];
    this.listaGrupos = [];
    this.listaBancoCargo = [];
    this.listaCuentaCargo = [];
    this.listaBancoAbono = [];
    this.listaCuentaAbono = [];

    this.listaZonaRegistral.push({ coZonaRegi: this.cartaOrdenData.coZonaRegi, deZonaRegi: this.cartaOrdenData.deZonaRegi });
    this.listaOficinaRegistral.push({ coOficRegi: this.cartaOrdenData.coOficRegi, deOficRegi: this.cartaOrdenData.deOficRegi });
    this.listaLocal.push({ coLocaAten: this.cartaOrdenData.coLocaAten, deLocaAten: this.cartaOrdenData.deLocaAten }); 
    this.listaTipoDocumento.push({ idTipoCartOrde: this.cartaOrdenData.idTipoCartOrde, noTipoCartOrde: this.cartaOrdenData.noTipoCartOrde });
    this.listaTipoReferencia.push({ ccodigoHijo: '*', cdescri: '(NINGUNO)' });
    this.listaGrupos.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' });
    this.listaBancoCargo.push({ nidInstitucion: '*', cdeInst: '(NINGUNO)' });   
    this.listaCuentaCargo.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });  
    this.listaBancoAbono.push({ nidInstitucion: '*', cdeInst: '(NINGUNO)' });   
    this.listaCuentaAbono.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
  }

  private _buscarTipoDocReferencia(): void {
    this.loadingTipoReferencia = true;
    this.generalService.getCbo_EstadoReferencia().pipe(takeUntil(this.unsubscribe$))
      .subscribe( 
        (data: any[]) => {             
          this.listaTipoReferencia.push(...data);
          this.carta.patchValue({'tipoDocuRef': this.cartaOrdenData.tiDocuRefe });
          this.loadingTipoReferencia = false;            
        }, 
        (err) => {       
          this.loadingTipoReferencia = false;                                 
      });
  }

  private _buscarGruposCarta(): void {
    this.loadingGrupos = true;
    this.generalService.getCbo_GruposCarta().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data ) => {
        this.loadingGrupos = false;    
        if (data.length === 1) {   
          this.listaGrupos.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
          this.carta.patchValue({ "grupo": data[0].ccodigoHijo });
        } else {
          this.listaGrupos.push(...data);
        }

        this.carta.get('grupo')?.setValue( this.cartaOrdenData.coGrup );
      },
      error:( err )=>{
        this.loadingGrupos = false;    
      }
    });
  }

  private _obtenerVariables(): void {
    this.tipoCartaService.getTipoCartaVariables().pipe( takeUntil(this.unsubscribe$) )
      .subscribe({
        next: ( data ) => {
          this.listaVariables = data;
        },
        error: () => {
          this.listaVariables.push({ noCamp: '', deCamp: 'No se cargaron las variables'});
        }
      });
  }

  public onChangeTipoDocReferencia( $event: any ) {
    if ($event.value === '*') {
      this.carta.get('nroDocuRef')?.setValue( '' );
      this.carta.get('nroDocuRef')?.disable();
    } else {
      this.carta.get('nroDocuRef')?.enable();
    }
  }

  private _buscarBancosCargo(): void {
    this.loadingBancoCargo = true;
    this.generalService.getCbo_Bancos().pipe(takeUntil(this.unsubscribe$))
      .subscribe( 
        (data: any[]) => {             
          this.listaBancoCargo.push(...data);
          this.carta.patchValue({'bancoCargo': this.cartaOrdenData.idBancCarg });
          this.loadingBancoCargo = false;            
        }, 
        (err) => {       
          this.loadingBancoCargo = false;                                 
      });
  }

  public onChangeBancoCargo(idBanc: any, primeraCarga: boolean): void {
    primeraCarga ? this.carta.get('nombreCuentaCargo')?.setValue( this.cartaOrdenData.noCuenBancCarg ) : this.carta.get('nombreCuentaCargo')?.setValue( '' );
    this.loadingCuentaCargo = true;
    this.listaCuentaCargo = [];
    this.listaCuentaCargo.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });  
    this.generalService.getCbo_Cuentas(idBanc.value).pipe(takeUntil(this.unsubscribe$))
      .subscribe( 
        (data: any[]) => {             
          this.listaCuentaCargo.push(...data);
          this.carta.patchValue({'cuentaCargo': this.cartaOrdenData.nuCuenBancCarg });
          this.loadingCuentaCargo = false;            
        }, 
        (err) => {       
          this.loadingCuentaCargo = false;                                 
      });
  }

  public onChangeCuentaCargo(event: any): void {
    const cuentaSeleccionada = this.listaCuentaCargo.find(value => value.nuCuenBanc === event.value);
    this.carta.get('nombreCuentaCargo')?.setValue( cuentaSeleccionada.noCuenBanc );
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  private _buscarBancosAbono(): void {
    this.loadingBancoAbono = true;
    this.generalService.getCbo_Bancos().pipe(takeUntil(this.unsubscribe$))
      .subscribe( 
        (data: any[]) => {             
          this.listaBancoAbono.push(...data);
          this.carta.patchValue({'bancoAbono': this.cartaOrdenData.idBancAbon });
          this.loadingBancoAbono = false;            
        }, 
        (err) => {       
          this.loadingBancoAbono = false;                                 
      });
  }

  public onChangeBancoAbono(idBanc: any, primeraCarga: boolean): void {
    primeraCarga ? this.carta.get('nombreCuentaAbono')?.setValue( this.cartaOrdenData.noCuenBancAbon ) : this.carta.get('nombreCuentaAbono')?.setValue( '' );
    if ( !primeraCarga ) { this.carta.get('cuentaAbono')?.setValue('(NINGUNO)') }
    this.loadingCuentaAbono = true;
    this.listaCuentaAbono = [];
    this.listaCuentaAbono.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
    this.generalService.getCbo_Cuentas(idBanc.value).pipe(takeUntil(this.unsubscribe$))
      .subscribe( 
        (data: any[]) => {             
          this.listaCuentaAbono.push(...data);
          
          if ( primeraCarga ) {
            this.carta.patchValue({'cuentaAbono': this.cartaOrdenData.nuCuenBancAbon });
            this._habilitaNombreCuentaAbono();
          }

          this.loadingCuentaAbono = false;            
        }, 
        (err) => {
          this.loadingCuentaAbono = false;                                 
      });
  }

  public onChangeCuentaAbono(event: any): void {
    const cuentaSeleccionada = this.listaCuentaAbono.find(value => value.nuCuenBanc === event.value);
    if ( cuentaSeleccionada ) {
      this.readonlyNombre = true;
      this.carta.get('nombreCuentaAbono')?.setValue( cuentaSeleccionada.noCuenBanc );
    }
    else {
      this.readonlyNombre = false;
      this.carta.get('nombreCuentaAbono')?.setValue('');
    }
  }

  private _habilitaNombreCuentaAbono() {
    const cuentaSeleccionada = this.listaCuentaAbono.find(value => value.nuCuenBanc === this.cartaOrdenData.nuCuenBancAbon);
    !cuentaSeleccionada ? this.readonlyNombre = false : this.readonlyNombre = true;
  }

  public validarMonto(event:any){
    const pattern = /^(-?\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public formatearNumero(event:any){
    if (event.target.value) {
      const nuevoValor = this.funciones.convertirTextoANumero( event.target.value );
      const number = Number(nuevoValor).toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });

      this.carta.get('monto')?.setValue( number );
    }
  }

  public initEditorInicio( event: any ) {
    this.quillInicio = event.editor;
  }

  public selectionChangeInicio( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillInicio.getSelection().index;
      
      this.quillInicio.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillInicio.setSelection(cursorPosition + value, 'user');
	    this.carta.get('inicio')?.setValue( this.quillInicio.root.innerHTML );
      this.insertarVariable = null;
    }
  }

  public initEditorConcepto( event: any ) {
    this.quillConcepto = event.editor;
  }

  public selectionChangeConcepto( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillConcepto.getSelection().index;
      
      this.quillConcepto.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillConcepto.setSelection(cursorPosition + value, 'user');
	    this.carta.get('concepto')?.setValue( this.quillConcepto.root.innerHTML );
      this.insertarVariable = null;
    }
  }

  public initEditorFinal( event: any ) {
    this.quillFinal = event.editor;
  }

  public selectionChangeFinal( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillFinal.getSelection().index;
      
      this.quillFinal.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillFinal.setSelection(cursorPosition + value, 'user');
	    this.carta.get('final')?.setValue( this.quillFinal.root.innerHTML );
      this.insertarVariable = null;
    }
  }

  public async editarCarta() {
    console.log('cuentaAbono', this.carta.get('cuentaAbono')?.value);

    const boldInit = /<strong>/g;
    const boldFin = /<\/strong>/g;
    const italicInit = /<em>/g;
    const italicFin = /<\/em>/g;

    let resultado: boolean = this.validarFormulario();
    if (!resultado) {
      return;
    }
    const dataFiltrosCartaOrden: IDataFiltrosCartaOrden = await this.sharingInformationService.compartirDataFiltrosCartaOrdenObservable.pipe(first()).toPromise(); 
    if (dataFiltrosCartaOrden.esBusqueda) {
      dataFiltrosCartaOrden.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosCartaOrdenObservableData = dataFiltrosCartaOrden;
    }
    this.utilService.onShowProcessSaveEdit();
    this.utilService.onShowProcessLoading("Creando carta");
    const fecha = this.carta.get('fecha')?.value;
    const fechaFormat = (fecha !== "" && fecha !== null) ? this.funciones.convertirFechaDateToString(fecha) : '';
    const tipoDocuRef = this.carta.get('tipoDocuRef')?.value;
    const grupo = this.carta.get('grupo')?.value;
    const montoCartOrde = this.funciones.convertirTextoANumero( this.carta.get('monto')?.value );
    
    const seccionInicio: string = this.carta.get('inicio')?.value;
    const seccionConcep: string = this.carta.get('concepto')?.value;
    const seccionFinal: string = this.carta.get('final')?.value;

    this.bodyEditarCartaOrden.idCartOrde = this.cartaOrdenData.idCartOrde;
    this.bodyEditarCartaOrden.coZonaRegi = this.cartaOrdenData.coZonaRegi;
    this.bodyEditarCartaOrden.coOficRegi = this.cartaOrdenData.coOficRegi;
    this.bodyEditarCartaOrden.coLocaAten = this.cartaOrdenData.coLocaAten;
    this.bodyEditarCartaOrden.idTipoCartOrde = this.cartaOrdenData.idTipoCartOrde;
    this.bodyEditarCartaOrden.feCartOrde = fechaFormat;
    this.bodyEditarCartaOrden.imCartOrde = Number( montoCartOrde );
    this.bodyEditarCartaOrden.nuSiaf = this.carta.get('nroSIAF')?.value;
    this.bodyEditarCartaOrden.nuCompPago = this.carta.get('nroComp')?.value;
    this.bodyEditarCartaOrden.tiDocuRefe = tipoDocuRef === '*' ? '' : tipoDocuRef;
    this.bodyEditarCartaOrden.nuDocuRefe = this.carta.get('nroDocuRef')?.value;
    this.bodyEditarCartaOrden.coGrup = grupo === '*' ? '' : grupo;
    this.bodyEditarCartaOrden.noDest = this.carta.get('destina')?.value;
    this.bodyEditarCartaOrden.noCarg = this.carta.get('cargo')?.value;
    this.bodyEditarCartaOrden.idBancCarg = this.carta.get('bancoCargo')?.value == '*' ? 0 : this.carta.get('bancoCargo')?.value;
    this.bodyEditarCartaOrden.nuCuenBancCarg = this.carta.get('cuentaCargo')?.value == '(NINGUNO)' ? '' : this.carta.get('cuentaCargo')?.value;
    this.bodyEditarCartaOrden.idBancAbon = this.carta.get('bancoAbono')?.value == '*' ? 0 : this.carta.get('bancoAbono')?.value;
    if ( this.carta.get('cuentaAbono')?.value == '(NINGUNO)' ) {
      this.bodyEditarCartaOrden.nuCuenBancAbon = '';
    } else if ( this.carta.get('cuentaAbono')?.value == null ) {
      this.bodyEditarCartaOrden.nuCuenBancAbon = this.cartaOrdenData.nuCuenBancAbon;
    } else {
      this.bodyEditarCartaOrden.nuCuenBancAbon = this.carta.get('cuentaAbono')?.value;
    }
    this.bodyEditarCartaOrden.deSeccInic = seccionInicio.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>');
    this.bodyEditarCartaOrden.deSeccConc = seccionConcep.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>');
    this.bodyEditarCartaOrden.deSeccFina = seccionFinal.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>');
    const cuentaSeleccionada = this.listaCuentaAbono.find(value => value.nuCuenBanc === this.carta.get('cuentaAbono')?.value);
    this.bodyEditarCartaOrden.noCuenBancAbon = cuentaSeleccionada ? '' : this.carta.get('nombreCuentaAbono')?.value;
    console.log('bodyEditarCartaOrden', this.bodyEditarCartaOrden.nuCuenBancAbon)
    //return;
    this.$editarCartaOrden = this.cartaService.editarCartaOrden(this.bodyEditarCartaOrden).subscribe({
      next: ( d ) => {
        if (d.codResult < 0 ) {
          this.utilService.onShowAlert("info", "Atención", d.msgResult);
        } else if (d.codResult === 0) {
          this.utilService.onCloseLoading();
          this.router.navigate(['SARF/mantenimientos/documentos/carta_orden/bandeja']);
        }
      },
      error:( d )=>{
        if (d.error.category === "NOT_FOUND") {
          this.utilService.onShowAlert("error", "Atención", d.error.description);        
        }else {
          this.utilService.onShowMessageErrorSystem();
        }
      }
    })
  }

  private validarFormulario(): boolean {   
    if (this.carta.value.fecha === "" || this.carta.value.fecha === null) {   
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de carta.");
      return false;      
    } else if (this.carta.value.grupo === "" || this.carta.value.grupo === "*" || this.carta.value.grupo === null) {
      this.ddlGrupo.focus();
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un grupo.");
      return false;       
    } else {
      return true;
    }       
  }

  public async cancelar() {
    const dataFiltrosCartaOrden: IDataFiltrosCartaOrden = await this.sharingInformationService.compartirDataFiltrosCartaOrdenObservable.pipe(first()).toPromise(); 
    if (dataFiltrosCartaOrden.esBusqueda) {
      dataFiltrosCartaOrden.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosCartaOrdenObservableData = dataFiltrosCartaOrden;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/carta_orden/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaCartaOrdenObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete(); 
    this.$sharing.unsubscribe();
    this.$editarCartaOrden.unsubscribe();   
  }

}
