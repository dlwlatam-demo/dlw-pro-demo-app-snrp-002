import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { BodyCartaCorreo } from '../../../../../models/mantenimientos/documentos/carta-orden.model';

import { ICartaOrden } from '../../../../../interfaces/carta-orden.interface';

import { Funciones } from '../../../../../core/helpers/funciones/funciones';

import { CartaOrdenService } from '../../../../../services/mantenimientos/documentos/carta-orden.service';

@Component({
  selector: 'app-correo-enviar',
  templateUrl: './correo-enviar.component.html',
  styleUrls: ['./correo-enviar.component.scss'],
  providers: [
    MessageService
  ]
})
export class CorreoEnviarComponent implements OnInit {

  form!: FormGroup;

  cartaOrden!: ICartaOrden[]
  bodyCartaCorreo: BodyCartaCorreo;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private funciones: Funciones,
    private messageService: MessageService,
    private cartaService: CartaOrdenService
  ) { }

  ngOnInit(): void {
    this.bodyCartaCorreo = new BodyCartaCorreo();

    this.cartaOrden = this.config.data.doc as ICartaOrden[];

    this.form = this.fb.group({
      correo: ['', [ Validators.required, Validators.pattern( this.funciones.emailPattern ) ]]
    });
  }

  grabar() {

    this.bodyCartaCorreo.mailTo = this.form.get('correo')?.value;
    this.bodyCartaCorreo.noRutaPdf = 'https://sarf-content-manager-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe/sarfsunarp/api/v1/digital/obtenerAdjunSarf/';
    this.bodyCartaCorreo.idCartasOrdenes = this.cartaOrden.map( co => String( co.idCartOrde ) )

    console.log('this.bodyCartaCorreo', this.bodyCartaCorreo);

    this.cartaService.enviarEmail( this.bodyCartaCorreo ).subscribe({
      next: ( data ) => {
        if ( data.codResult < 0 ) {
          this.messageService.add({
            severity: 'warn',
            summary: data.msgResult
          });

          return;
        }

        this.ref.close('accept');
      },
      error: () => {
        this.ref.close('reject');
      }
    });
  }

  cancelar() {
    this.ref.close();
  }

}
