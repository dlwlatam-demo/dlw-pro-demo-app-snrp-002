import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService } from 'primeng/api';

import { CartaSustentosComponent } from '../carta-sustentos/carta-sustentos.component';
import { CartaSolicitarFirmaComponent } from '../carta-solicitar-firma/carta-solicitar-firma.component';
import { CartaHistorialFirmaComponent } from '../carta-historial-firma/carta-historial-firma.component';
import { CartaAnexosComponent } from '../carta-anexos/carta-anexos.component';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { environment } from '../../../../../../environments/environment';
import { map, Observable, of, Subject, Subscription } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';
import { GeneralService } from '../../../../../services/general.service';
import { CartaOrdenService } from '../../../../../services/mantenimientos/documentos/carta-orden.service';
import { BodyCartaOrden, BodyEliminarActivarCartaOrden, CartaOrden } from '../../../../../models/mantenimientos/documentos/carta-orden.model';
import { TipoCartaOrdenService } from '../../../../../services/mantenimientos/configuracion/tipo-carta-orden.service';
import { ICartaOrden, IDataFiltrosCartaOrden } from 'src/app/interfaces/carta-orden.interface';
import { UtilService } from 'src/app/services/util.service';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { CartaOrdenBandeja } from 'src/app/models/mantenimientos/documentos/carta-orden-bandeja.model';

import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { IBandejaProcess } from '../../../../../interfaces/global.interface';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { CorreoEnviarComponent } from '../correo-enviar/correo-enviar.component';
import { ITipoCartaOrden } from '../../../../../interfaces/configuracion/tipo-carta-orden.interface';

@Component({
  selector: 'app-bandeja-carta',
  templateUrl: './bandeja-carta.component.html',
  styleUrls: ['./bandeja-carta.component.scss'],
  providers: [
    DialogService,
    MessageService
  ]
})
export class BandejaCartaComponent extends BaseBandeja implements OnInit, OnDestroy {
  loadingZonaRegistral: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  loadingTipoCarta: boolean = false;
  loadingGrupos: boolean = false;
  loadingEstado: boolean = false;
  userCode:any
  searchCarta!: FormGroup;
  bodyCartaOrden: BodyCartaOrden;
  bodyEliminarActivarCartaOrden: BodyEliminarActivarCartaOrden;
  $estado: Observable<any>;
  fechaMinima: Date;
  fechaMaxima: Date;  
  listaCartaOrden!: ICartaOrden[];
  loadingCartaOrden: boolean;
  override selectedValues: ICartaOrden[] = [];
  cartaOrdenBandeja!: CartaOrdenBandeja;
  unsubscribe$ = new Subject<void>();

  listaZonaRegistral: ZonaRegistral[];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoCartaOrden: ITipoCartaOrden[];
  listaEstadoCartaOrden: IResponse2[];
  listaGrupos: IResponse2[];

  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaTipoCartaOrdenFiltro: ITipoCartaOrden[];
  listaEstadoCartaOrdenFiltro: IResponse2[];
  listaGruposFiltro: IResponse2[];
  esBusqueda: boolean = false;
  cartaOrdenProceso: IBandejaProcess = {
    idMemo: null,
    proceso: ''
  };
  currentPage: string = environment.currentPage;
  
  guidDocumento: string = '';

  constructor( 
    private fb: FormBuilder,
    private router: Router,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private tipoCartaService: TipoCartaOrdenService,
    private cartaService: CartaOrdenService,
    private funciones: Funciones,  
    private validateService: ValidatorsService,
    private utilService: UtilService,
    private sharingInformationService: SharingInformationService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Carta_Orden;
    this.btnCartaOrden();
    
    this.bodyCartaOrden = new BodyCartaOrden();
    this.bodyEliminarActivarCartaOrden = new BodyEliminarActivarCartaOrden();
    this.userCode = localStorage.getItem('user_code')||'';
    this.fechaMaxima = new Date();
    this.fechaMinima = new Date(this.fechaMaxima.getFullYear(), this.fechaMaxima.getMonth(), 1);
    this.searchCarta = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoCart: ['', [ Validators.required ]],
      fecDes: [this.fechaMinima],
      fecHas: [this.fechaMaxima],
      nroCart: [''],
      nroSiaf: [''],
      usuario: [''],
      grupo: [''],
      estado: [''],
    });
    this._dataFiltrosCartaOrden();
    // this.buscarCarta(true);
  }

  private _buscarZonaRegistral(): void {
    this.loadingZonaRegistral = true;
    this.generalService.getCbo_Zonas_Usuario(this.userCode).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingZonaRegistral = false;    
        if (data.length === 1) {   
          this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });    
          this.searchCarta.patchValue({ "zona": data[0].coZonaRegi});
          this.buscarOficinaRegistral();
        } else {    
          this.listaZonaRegistral.push(...data);
        }        
        // this.buscarCarta(true);  
      },
      error:( err )=>{
        this.loadingZonaRegistral = false;    
      }
    });
  }

  public buscarOficinaRegistral(): void {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.searchCarta.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.searchCarta.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.searchCarta.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.searchCarta.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.searchCarta.get('zona')?.value,this.searchCarta.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.searchCarta.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  private _buscarCartaOrden() {
    this.loadingTipoCarta = true;
    this.tipoCartaService.getBandejaTipoCarta('','A','', '').pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingTipoCarta = false;
        this.listaTipoCartaOrden.push(...data); 
      },
      error:( err )=>{
        this.loadingTipoCarta = false;    
      }
    });
  }

  private _buscarGruposCarta(): void {
    this.loadingGrupos = true;
    this.generalService.getCbo_GruposCarta().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data ) => {
        this.loadingGrupos = false;    
        if (data.length === 1) {   
          this.listaGrupos.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
          this.searchCarta.patchValue({ "grupo": data[0].ccodigoHijo });
        } else {
          this.listaGrupos.push(...data);
        }        
      },
      error:( err )=>{
        this.loadingGrupos = false;    
      }
    });
  }

  public cambioFechaInicio() {   
    if (this.searchCarta.get('fecHas')?.value !== '' && this.searchCarta.get('fecHas')?.value !== null) {
      if (this.searchCarta.get('fecDes')?.value > this.searchCarta.get('fecHas')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.searchCarta.controls['fecDes'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.searchCarta.get('fecDes')?.value !== '' && this.searchCarta.get('fecDes')?.value !== null) {
      if (this.searchCarta.get('fecHas')?.value < this.searchCarta.get('fecDes')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.searchCarta.controls['fecHas'].reset();    
      }      
    }
  }
  
  private _dataFiltrosCartaOrden(): void {
    this.sharingInformationService.compartirDataFiltrosCartaOrdenObservable.pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: IDataFiltrosCartaOrden) => {  
      if (data.esCancelar && data.bodyCartaOrden !== null) {      
        this.utilService.onShowProcessLoading("Cargando la data");
        this._setearCamposFiltro(data);
        this.buscarCarta(false);
      } else {
        this._inicilializarListas();
        this._buscarZonaRegistral();
        this._buscarEstado();
        this._buscarCartaOrden();
        this._buscarGruposCarta();
        this.buscarCarta(true);
      }
    });
  }

  private _setearCamposFiltro(dataFiltro: IDataFiltrosCartaOrden) {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoCartaOrden = [];
    this.listaEstadoCartaOrden = [];
    this.listaGrupos = [];
    this.listaZonaRegistral = dataFiltro.listaZonaRegistral; 
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaTipoCartaOrden = dataFiltro.listaTipoCartaOrden;
    this.listaEstadoCartaOrden = dataFiltro.listaEstadoCartaOrden;
    this.listaGrupos = dataFiltro.listaGrupos;
    this.bodyCartaOrden = dataFiltro.bodyCartaOrden;
    this.searchCarta.patchValue({ "zona": this.bodyCartaOrden.coZonaRegi});
    this.searchCarta.patchValue({ "oficina": this.bodyCartaOrden.coOficRegi});
    this.searchCarta.patchValue({ "local": this.bodyCartaOrden.coLocaAten});
    this.searchCarta.patchValue({ "tipoCart": this.bodyCartaOrden.idTipoCartOrde});
    this.searchCarta.patchValue({ "fecDes": (this.bodyCartaOrden.feDesd !== '' && this.bodyCartaOrden.feDesd !== null) ? this.funciones.convertirFechaStringToDate(this.bodyCartaOrden.feDesd) : this.bodyCartaOrden.feDesd});
    this.searchCarta.patchValue({ "fecHas": (this.bodyCartaOrden.feHast !== '' && this.bodyCartaOrden.feHast !== null) ? this.funciones.convertirFechaStringToDate(this.bodyCartaOrden.feHast) : this.bodyCartaOrden.feHast});
    this.searchCarta.patchValue({ "nroCart": this.bodyCartaOrden.nuCartOrden});
    this.searchCarta.patchValue({ "nroSiaf": this.bodyCartaOrden.nuSiaf});
    this.searchCarta.patchValue({ "usuario": this.bodyCartaOrden.noUsuaCrea});
    this.searchCarta.patchValue({ "grupo": this.bodyCartaOrden.coGrup});
    this.searchCarta.patchValue({ "estado": this.bodyCartaOrden.esDocu});
  }

  private _inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoCartaOrden = [];
    this.listaEstadoCartaOrden = [];
    this.listaGrupos = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    this.listaTipoCartaOrden.push({ idTipoCartOrde: 0, noTipoCartOrde: '(TODOS)' }); 
    this.listaEstadoCartaOrden.push({ ccodigoHijo: '', cdescri: '(TODOS)' });   
    this.listaGrupos.push({ ccodigoHijo: '*', cdescri: '(TODOS)' });
  }

  public buscarCarta(esCargaInicial: boolean): void {
    const fechaDesde = this.searchCarta.get('fecDes')?.value;
    const fechaHasta = this.searchCarta.get('fecHas')?.value;
    if (!fechaDesde && fechaHasta) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha DESDE');  
      return;     
    }
    if (!fechaHasta && fechaDesde) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha HASTA');   
      return;     
    }
    this.selectedValues = [];
    this.listaCartaOrden = [];
    this.loadingCartaOrden = (esCargaInicial) ? false : true;    
    this.guardarListadosParaFiltro();
    this.esBusqueda = (esCargaInicial) ? false : true;    
    this.utilService.onShowProcessLoading("Cargando la data"); 
    const zona = this.searchCarta.get('zona')?.value;
    const oficina = this.searchCarta.get('oficina')?.value;
    const local = this.searchCarta.get('local')?.value;
    const fechaDesdeFormat = (fechaDesde !== "" && fechaDesde !== null) ? this.funciones.convertirFechaDateToString(fechaDesde) : '';
    const fechaHastaFormat = (fechaHasta !== "" && fechaHasta !== null) ? this.funciones.convertirFechaDateToString(fechaHasta) : '';
    const tipoCarta = this.searchCarta.get('tipoCart')?.value;
    const estadoDocumento = this.searchCarta.get('estado')?.value;
    const grupo = this.searchCarta.get('grupo')?.value;
    this.bodyCartaOrden.coZonaRegi = zona === '*' ? '' : zona;
    this.bodyCartaOrden.coOficRegi = oficina === '*' ? '' : oficina;
    this.bodyCartaOrden.coLocaAten = local === '*' ? '' : local;
    this.bodyCartaOrden.idTipoCartOrde = tipoCarta === '*' ? '' : tipoCarta;
    this.bodyCartaOrden.nuCartOrden = this.searchCarta.get('nroCart')?.value;
    this.bodyCartaOrden.feDesd = fechaDesdeFormat;
    this.bodyCartaOrden.feHast = fechaHastaFormat;
    this.bodyCartaOrden.nuSiaf = this.searchCarta.get('nroSiaf')?.value;
    this.bodyCartaOrden.esDocu = estadoDocumento === '*' ? '' : estadoDocumento;
    this.bodyCartaOrden.noUsuaCrea = this.searchCarta.get('usuario')?.value;
    this.bodyCartaOrden.coGrup = grupo === '*' ? '' : grupo;
    this.cartaService.getBandejaCartaOrden(this.bodyCartaOrden).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.selectedValues = [];
          this.cartaOrdenBandeja = data;
          this.listaCartaOrden = data.lstCartaOrden ? data.lstCartaOrden : [];
          this.loadingCartaOrden = false;
          this.utilService.onCloseLoading();  
        },
        error:( d )=>{
          this.loadingCartaOrden = false;
          this.selectedValues = [];
          this.listaCartaOrden= [];
          this.utilService.onCloseLoading();  
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("warning", "Atención", d.error.description);        
          }else {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      });
  }

  private guardarListadosParaFiltro() {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaTipoCartaOrdenFiltro = [...this.listaTipoCartaOrden];
    this.listaEstadoCartaOrdenFiltro = [...this.listaEstadoCartaOrden];
    this.listaGruposFiltro = [...this.listaGrupos];
  }

  private _buscarEstado() {
    this.loadingEstado = true;
    this.generalService.getCbo_EstadoCarta().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingEstado = false;
        this.listaEstadoCartaOrden.push(...data);        
      },
      error:( err )=>{
        this.loadingEstado = false;    
      }
    });
  }

  nuevaCarta() {
    this.cartaOrdenProceso.idMemo = null;
    this.cartaOrdenProceso.proceso = 'nuevo';  
    this.sharingInformationService.compartirCartaOrdenProcesoObservableData = this.cartaOrdenProceso;
    this.router.navigate(['SARF/mantenimientos/documentos/carta_orden/nuevo']);
    this.sharingInformationService.irRutaBandejaCartaOrdenObservableData = false;
  }

  editarCarta() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
      return;
    }

    if ( this.selectedValues?.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
      return;
    }

    if ( this.selectedValues[0].deEsta === 'ANULADO'  || this.selectedValues[0].deEsta === 'PENDIENTE DE FIRMA' 
          || this.selectedValues[0].deEsta === 'FIRMADO' ) {
      this.utilService.onShowAlert("warning", "Atención", "No puede editar un registro ANULADO, PENDIENTE DE FIRMA o FIRMADO");
      return;
    }
    this.cartaOrdenProceso.idMemo = this.selectedValues[0].idCartOrde;
    this.cartaOrdenProceso.proceso = 'editar';     
    this.router.navigate([`SARF/mantenimientos/documentos/carta_orden/editar/${ this.selectedValues[0].idCartOrde }`])
    this.sharingInformationService.irRutaBandejaCartaOrdenObservableData = false;
  }

  anular() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
      return;
    }
    const idCartasOrdenes = this.selectedValues.map( value => value.idCartOrde.toString());
    const textoAnular = (this.selectedValues.length === 1) ? 'el registro' : 'los registros';
    this.utilService.onShowConfirm(`¿Estás seguro que desea anular ${textoAnular}?`).then((result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('anular'); 
        this.bodyEliminarActivarCartaOrden.idCartasOrdenes = idCartasOrdenes;
        this.cartaService.anularCartaOrden(this.bodyEliminarActivarCartaOrden).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0 ) {
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
            } else if (d.codResult === 0 ) {
              this.selectedValues = [];
              this.buscarCarta(false);
            }
          },

          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", d.error.description);        

            }else {
              this.utilService.onShowMessageErrorSystem();        
            }
          }
        });
        
      } else if (result.isDenied) {
        this.utilService.onShowAlert("warning", "Atención", "No se pudieron guardar los cambios.");
      }
    })
  }

  activar() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
      return;
    }
    const idCartasOrdenes = this.selectedValues.map( value => value.idCartOrde.toString());
    const textoActivar = (this.selectedValues.length === 1) ? 'el registro' : 'los registros';
    this.utilService.onShowConfirm(`¿Estás seguro que desea activar ${textoActivar}?`).then((result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('activar'); 
        this.bodyEliminarActivarCartaOrden.idCartasOrdenes = idCartasOrdenes;
        this.cartaService.activarCartaOrden(this.bodyEliminarActivarCartaOrden).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0) {
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
            } else if (d.codResult === 0) {
              this.selectedValues = [];
              this.buscarCarta(false);
            }
          },
          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              Swal.fire( 'Carta orden', `${d.error.description}`, 'info');
              this.utilService.onShowAlert("error", "Atención", d.error.description);        
            }else {
              this.utilService.onShowMessageErrorSystem();        
            }
          }
        });
        
      } else if (result.isDenied) {
        this.utilService.onShowAlert("warning", "Atención", "No se pudieron guardar los cambios.");
      }
    });
  }

  public consultar() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para consultarlo.");
      return;
    }

    if ( this.selectedValues?.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede consultar un registro a la vez.");
      return;
    }
    this.cartaOrdenProceso.idMemo = this.selectedValues[0].idCartOrde;
    this.cartaOrdenProceso.proceso = 'consultar';
    this.router.navigate([`SARF/mantenimientos/documentos/carta_orden/consultar/${ this.selectedValues[0].idCartOrde }`])
    this.sharingInformationService.irRutaBandejaCartaOrdenObservableData = false;
  }

  sustentos() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro!");
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede seleccionar un registro a la vez!");
      return;
    }

    const dialog = this.dialogService.open( CartaSustentosComponent, {
      header: 'Carta Orden - Sustentos',
      width: '45%',
      closable: false,
      data: {
        carta: this.selectedValues,
        add: this.BtnAdicionar_Sustento,
        delete: this.BtnAnular_Sustento,
        bandeja: this.codBandeja
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Los sustentos han sido cargados y grabados correctamente',
          life: 5000
        });

        this.buscarCarta(false);
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al cargar y grabar los sustentos',
          life: 5000
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  anexos() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Carta Orden', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Carta Orden', 'Solo puede seleccionar un registro a la vez!', 'error');
      return;
    }

    const dialog = this.dialogService.open( CartaAnexosComponent, {
      header: 'Carta Orden - Anexos',
      width: '45%',
      closable: false,
      data: {
        carta: this.selectedValues,
        add: this.BtnAdicionar_Anexo,
        delete: this.BtnEliminar_Anexo,
        bandeja: this.codBandeja
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Los anexos han sido cargados y grabados correctamente',
          life: 5000
        });

        this.buscarCarta(false);
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al cargar y grabar los anexos',
          life: 5000
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }
  
  
  verDocumento() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Carta Orden', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Carta Orden', 'Solo puede visualizar un registro a la vez!', 'error');
      return;
    }

	  const cartaOrden  = this.selectedValues;

    if ( cartaOrden[0].idGuidDocu === '' || cartaOrden[0].idGuidDocu === ' ' ) {
      this.cartaService.cartaOrdenPdfById( cartaOrden['0'].idCartOrde! ).subscribe({
        next: ( data ) => {
          console.log('dataVer', data )
          
          this.guidDocumento = data.msgResult;
          console.log('Exportar PDF: ' + this.guidDocumento);
  
          const doc = data.archivoPdf;
          const byteCharacters = atob(doc!);
          const byteNumbers = new Array(byteCharacters.length);
          for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
          }
          const byteArray = new Uint8Array(byteNumbers);
          const blob = new Blob([byteArray], { type: 'application/pdf' });
          const blobUrl = URL.createObjectURL(blob);
          const download = document.createElement('a');
          download.href = blobUrl;
          //download.setAttribute('download', 'Constacia_Abono');
          download.setAttribute("target", "_blank");
          document.body.appendChild( download );
          download.click();
      
      
        },
        error: ( e ) => {
          console.log( e )
          this.utilService.onShowAlert("warning", "Atención", e.error.description);
        }
      });
    }
    else {
      this.guidDocumento = cartaOrden[0].idGuidDocu;

      const download = document.createElement('a');
      download.href =  this.generalService.downloadManager(this.guidDocumento);
      download.setAttribute("target", "_blank");
      document.body.appendChild( download );
      download.click();
    }
	
  }



  solicitarFirma() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Carta Orden', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Carta Orden', 'Solo puede seleccionar un registro a la vez!', 'error');
      return;
    }

	const cartaOrden  = this.selectedValues;
	
    this.cartaService.cartaOrdenPdfById( cartaOrden['0'].idCartOrde! ).subscribe({
      next: ( data ) => {
        console.log('dataPrev', data );
        
        this.guidDocumento = data.msgResult;
        console.log('Exportar PDF: ' + this.guidDocumento);
            
        const dialog = this.dialogService.open( CartaSolicitarFirmaComponent, {
          header: 'Carta Orden - Solicitar Firma',
          width: '45%',
          data: {
          carta: this.selectedValues,
          guidDocumento: data.msgResult,
          doc: data.archivoPdf
          }
        });

        dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
          if ( type == 'accept' ) {
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se solicitó la firma del documento satisfactoriamente.',
              life: 5000
            });
    
            this.selectedValues = [];
            this.buscarCarta(false);
          }
        });
      },
      error: ( e ) => {
        console.log( e );
      }
    });
	
  }

  historialFirmas() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Carta Orden', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Carta Orden', 'Solo puede seleccionar un registro a la vez!', 'error');
      return;
    }

    if ( this.selectedValues[0].esDocu == '001' || this.selectedValues[0].esDocu == '005' ) {
      Swal.fire('Solo puede ver el historial de registros con estado PENDIENTE DE FIRMA, FIRMADO o RECHAZADO.', '', 'warning');
      return;
    }
    
    const dialog = this.dialogService.open( CartaHistorialFirmaComponent, {
      header: 'Carta Orden - Historial de Firmas',
      closable: false,
      width: '50%',
      data: {
        carta: this.selectedValues
      }
    });

    dialog.onClose.subscribe( () => {
      this.selectedValues = [];
    });
  }

  public enviarCorreo() {
    if ( this.selectedValues.length === 0 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar por lo menos un registro');
      return;
    }

    for ( let select of this.selectedValues ) {
      if ( select.esDocu != '003' ) {
        this.utilService.onShowAlert('warning', 'Atención', 'Solo puede enviar Cartas Ordenes con estado FIRMADO');
        return;
      }
    }

    const dialog = this.dialogService.open(CorreoEnviarComponent, {
      header: 'Enviar Correo',
      width: '45%',
      data: {
        doc: this.selectedValues
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'El correo se ha enviado correctamente'
        });

        this.selectedValues = [];
        this.buscarCarta(false);
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al enviar el correo'
        });
      }
    });
  }

  bandejaCheques() {
    this.router.navigate(['SARF/mantenimientos/documentos/comprobantes_de_pago/cheques/bandeja']);
  }

  exportExcel() {
    console.log('Exportar Excel');

    const fechaHoy = new Date();
    const fileName = this.validateService.formatDateWithHoursForExcel( fechaHoy );

    const doc = this.cartaOrdenBandeja.reporteExcel;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', `CartaOrden_${ fileName }`);
    document.body.appendChild( download );
    download.click();
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  private establecerDataFiltrosCartaOrden() : IDataFiltrosCartaOrden {
    const dataFiltrosCartaOrden: IDataFiltrosCartaOrden = {
      listaZonaRegistral: this.listaZonaRegistralFiltro, 
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaTipoCartaOrden: this.listaTipoCartaOrdenFiltro,
      listaEstadoCartaOrden: this.listaEstadoCartaOrdenFiltro,
      listaGrupos: this.listaGruposFiltro,
      bodyCartaOrden: this.bodyCartaOrden,   
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosCartaOrden;
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  formatearMonto( m: string ): string {
    return Number( m ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

  ngOnDestroy(): void {      
    this.unsubscribe$.next();
    this.unsubscribe$.complete();  
    this.sharingInformationService.compartirDataFiltrosCartaOrdenObservableData = this.establecerDataFiltrosCartaOrden(); 
  }

}
