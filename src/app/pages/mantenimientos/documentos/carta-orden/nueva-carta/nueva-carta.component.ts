import { Component, OnDestroy, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { map, Observable, of, Subject, Subscription } from 'rxjs';
import { catchError, first, takeUntil } from 'rxjs/operators';
import { GeneralService } from '../../../../../services/general.service';
import { CartaOrdenService } from '../../../../../services/mantenimientos/documentos/carta-orden.service';
import { TipoCartaOrdenService } from '../../../../../services/mantenimientos/configuracion/tipo-carta-orden.service';
import { BodyNuevaCartaOrden, CartaOrden } from '../../../../../models/mantenimientos/documentos/carta-orden.model';
import Swal from 'sweetalert2';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { UtilService } from 'src/app/services/util.service';
import { Dropdown } from 'primeng/dropdown';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { IDataFiltrosCartaOrden } from 'src/app/interfaces/carta-orden.interface';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { ITipoCartaOrden, ITipoCartaVariables } from '../../../../../interfaces/configuracion/tipo-carta-orden.interface';
import { IResponse2 } from '../../../../../interfaces/general.interface';
import Quill from 'quill';

@Component({
  selector: 'app-nueva-carta',
  templateUrl: './nueva-carta.component.html',
  styleUrls: ['./nueva-carta.component.scss']
})
export class NuevaCartaComponent implements OnInit, OnDestroy {
  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;
  @ViewChild('ddlTipoCarta') ddlTipoCarta: Dropdown;
  @ViewChild('ddlTipoReferencia') ddlTipoReferencia: Dropdown;
  @ViewChild('ddlGrupo') ddlGrupo: Dropdown;
  @ViewChild('ddlBancoCargo') ddlBancoCargo: Dropdown;
  @ViewChild('ddlCuentaCargo') ddlCuentaCargo: Dropdown;
  @ViewChild('ddlBancoAbono') ddlBancoAbono: Dropdown;
  @ViewChild('ddlCuentaAbono') ddlCuentaAbono: Dropdown;

  loadingZonaRegistral: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  loadingTipoCarta: boolean = false;
  loadingTipoReferencia: boolean = false;
  loadingGrupos: boolean = false;
  loadingBancoCargo: boolean = false;
  loadingCuentaCargo: boolean = false;
  loadingBancoAbono: boolean = false;
  loadingCuentaAbono: boolean = false;
  readonlyNombre: boolean = true;
  editorInicio: string = '';
  editorInicioText: string = '';
  editorConcepto: string = '';
  editorFinal: string = '';
  userCode:any;
  fechaActual: Date;
  idEdit!: number;
  msg!: string;
  $createCartaOrden: Subscription = new Subscription;
  carta!: FormGroup;
  idBancCargo!: number;
  tipoCartaOrden!: ITipoCartaOrden;
  insertarVariable!: ITipoCartaVariables;
  bodyNuevaCartaOrden!: BodyNuevaCartaOrden;
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoCartaOrden: ITipoCartaOrden[] = [];
  listaGrupos: IResponse2[] = [];
  listaVariables: ITipoCartaVariables[] = [];
  listaBancoCargo: any[] = [];
  listaCuentaCargo: any[] = [];
  listaBancoAbono: any[] = [];
  listaCuentaAbono: any[] = [];
  unsubscribe$ = new Subject<void>();

  $tipoDocReferencia: Observable<any>;
  $bancosCargo!: Observable<any>;
  $cuentasCargo: Observable<any> = of([{ nuCuenBanc: '(NINGUNO)'}]);
  $bancosAbono!: Observable<any>;
  $cuentasAbono: Observable<any> = of([{ nuCuenBanc: '(NINGUNO)'}]);

  quillInicio!: Quill;
  quillConcepto!: Quill;
  quillFinal!: Quill;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private generalService: GeneralService,
    private tipoCartaService: TipoCartaOrdenService,
    private cartaService: CartaOrdenService,
    public funciones: Funciones,
    private utilService: UtilService,
    private validateService: ValidatorsService,
    private sharingInformationService: SharingInformationService,
  ) { }

  ngOnInit(): void {
    this.bodyNuevaCartaOrden = new BodyNuevaCartaOrden();
    this.userCode = localStorage.getItem('user_code')||'';
    this._inicilializarNuevaCartaOrden();
    this._buscarZonaRegistral();
    this._buscarCartaOrden();
    this._buscarTipoDocReferencia();
    this._buscarGruposCarta();
    this._obtenerVariables();
    this._buscarBancosCargo();
    this._buscarBancosAbono();
    this.fechaActual = new Date();
    this.carta = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoCarta: ['', [ Validators.required ]],
      nroCarta: ['', [ Validators.required ]],
      fecha: [this.fechaActual, [ Validators.required ]],
      monto: ['', [ Validators.required ]],
      nroSIAF: ['', [ Validators.required ]],
      nroComp: ['', [ Validators.required ]],
      tipoDocuRef: ['', [ Validators.required ]],
      nroDocuRef: ['', [ Validators.required ]],
      grupo: ['', [ Validators.required ]],
      destina: ['', [ Validators.required ]],
      cargo: ['', [ Validators.required ]],
      inicio: ['', [ Validators.required ]],
      concepto: ['', [ Validators.required ]],
      final: ['', [ Validators.required ]],
      bancoCargo: ['', []],
      cuentaCargo: ['', []],
      nombreCuentaCargo: ['', []],
      bancoAbono: ['', []],
      cuentaAbono: ['(NINGUNO)', []],
      nombreCuentaAbono: ['', []],
    });
    this.carta.get('nroCarta')?.disable();
    this.carta.get('nroDocuRef')?.disable();
  }

  private _inicilializarNuevaCartaOrden() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoCartaOrden = [];
    this.listaGrupos = [];
    this.listaBancoCargo = [];
    this.listaCuentaCargo = [];
    this.listaBancoAbono = [];
    this.listaCuentaAbono = [];

    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    this.listaTipoCartaOrden.push({ idTipoCartOrde: 0, noTipoCartOrde: '(SELECCIONAR)' });
    this.listaGrupos.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' });
    this.listaBancoCargo.push({ nidInstitucion: '*', cdeInst: '(NINGUNO)' });   
    this.listaCuentaCargo.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });  
    this.listaBancoAbono.push({ nidInstitucion: '*', cdeInst: '(NINGUNO)' });   
    this.listaCuentaAbono.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
  }

  private _buscarZonaRegistral(): void {
    this.loadingZonaRegistral = true;
    this.generalService.getCbo_Zonas_Usuario(this.userCode).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingZonaRegistral = false;    
        if (data.length === 1) {   
          this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });    
          this.carta.patchValue({ "zona": data[0].coZonaRegi});
          this.buscarOficinaRegistral();
        } else {    
          this.listaZonaRegistral.push(...data);
        } 
      },
      error:( err )=>{
        this.loadingZonaRegistral = false;    
      }
    });
  }

  public buscarOficinaRegistral(): void {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.carta.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.carta.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.carta.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal(): void {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.carta.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.carta.get('zona')?.value,this.carta.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.carta.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  private _buscarCartaOrden(): void {
    this.loadingTipoCarta = true;
    this.tipoCartaService.getBandejaTipoCarta('','A','', '').pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingTipoCarta = false;
        this.listaTipoCartaOrden.push(...data); 
      },
      error:( err )=>{
        this.loadingTipoCarta = false;    
      }
    });
  }

  private _buscarTipoDocReferencia(): void {
    this.loadingTipoReferencia = true;  
    const docReferenciaItem: any = { ccodigoHijo  : '*', cdescri: '(NINGUNO)' };
    this.$tipoDocReferencia = this.generalService.getCbo_EstadoReferencia().pipe(
      catchError(error => {
        this.loadingTipoReferencia = false;    
        return of([]);
      }),
      map( d => {
        d.unshift( docReferenciaItem );
        this.loadingTipoReferencia = false;  
        return d;
      })
    );
  }

  private _buscarGruposCarta(): void {
    this.loadingGrupos = true;
    this.generalService.getCbo_GruposCarta().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data ) => {
        this.loadingGrupos = false;    
        if (data.length === 1) {   
          this.listaGrupos.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
          this.carta.patchValue({ "grupo": data[0].ccodigoHijo });
        } else {
          this.listaGrupos.push(...data);
        }        
      },
      error:( err )=>{
        this.loadingGrupos = false;    
      }
    });
  }

  private _obtenerVariables(): void {
    this.tipoCartaService.getTipoCartaVariables().pipe( takeUntil(this.unsubscribe$) )
      .subscribe({
        next: ( data ) => {
          this.listaVariables = data;
        },
        error: () => {
          this.listaVariables.push({ noCamp: '', deCamp: 'No se cargaron las variables'});
        }
      });
  }

  public onChangeTipoDocReferencia( $event: any ) {
    if ($event.value === '*') {
      this.carta.get('nroDocuRef')?.setValue( '' );
      this.carta.get('nroDocuRef')?.disable();
    } else {
      this.carta.get('nroDocuRef')?.enable();
    }
  }

  public onChangeTipoCartaOrden( event: any): void {
    console.log('event', event)
    const tipoCarta = this.listaTipoCartaOrden.find( referencia => event.value === referencia.idTipoCartOrde );
    this.tipoCartaOrden = tipoCarta;
    console.log('tipoCarta', tipoCarta)

    if ( tipoCarta ) {
      this.carta.get('grupo')?.setValue( tipoCarta.coGrup );
      this.carta.get('destina')?.setValue( tipoCarta.noDest );
      this.carta.get('cargo')?.setValue( tipoCarta.noCarg );
      this.carta.get('inicio')?.setValue( tipoCarta.deSeccInic );
      this.carta.get('concepto')?.setValue( tipoCarta.deSeccConc );
      this.carta.get('final')?.setValue( tipoCarta.deSeccFina );
      
      if (
        tipoCarta.idBancCarg === 0 || !tipoCarta.idBancCarg || 
        tipoCarta.nuCuenBancCarg === '' || !tipoCarta.nuCuenBancCarg ||
        tipoCarta.idBancAbon === 0 || !tipoCarta.idBancAbon || 
        tipoCarta.nuCuenBancAbon === '' || !tipoCarta.nuCuenBancAbon
      ) {
        this._buscarBancosCargo();
        this.carta.get('bancoCargo')?.setValue( '*' );
        this.listaCuentaCargo = [];
        this.listaCuentaCargo.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
        this.carta.get('nombreCuentaCargo')?.setValue( '' );
        
        this._buscarBancosAbono();
        this.carta.get('bancoAbono')?.setValue( '*' );
        this.listaCuentaAbono = [];
        this.listaCuentaAbono.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
        this.carta.get('cuentaAbono')?.setValue( '(NINGUNO)' );
        this.carta.get('nombreCuentaAbono')?.setValue( '' );
        return;
      }
      
      this.carta.get('bancoCargo')?.setValue( tipoCarta.idBancCarg );
      this.onChangeBancoCargo({ value: tipoCarta.idBancCarg });
      this.carta.get('nombreCuentaCargo')?.setValue( tipoCarta.noCuenBancCarg );
      
      this.carta.get('bancoAbono')?.setValue( tipoCarta.idBancAbon );
      this.onChangeBancoAbono({ value: tipoCarta.idBancAbon }, true);
      this.carta.get('nombreCuentaAbono')?.setValue( tipoCarta.noCuenBancAbon );
    }
  }

  private _buscarBancosCargo(): void {
    this.loadingBancoCargo = true;    
    this.generalService.getCbo_Bancos().pipe(takeUntil(this.unsubscribe$))
      .subscribe( 
        (data: any[]) => {             
          this.listaBancoCargo.push(...data);
          this.loadingBancoCargo = false;            
        }, 
        (err) => {       
          this.loadingBancoCargo = false;                                 
      });
  }

  public onChangeBancoCargo(idBanc: any): void {
    this.loadingCuentaCargo = true;    
    this.carta.get('nombreCuentaCargo')?.setValue( '' );
    this.listaCuentaCargo = [];
    this.listaCuentaCargo.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });  
    this.generalService.getCbo_Cuentas(idBanc.value).pipe(takeUntil(this.unsubscribe$))
      .subscribe( 
        (data: any[]) => {             
          this.listaCuentaCargo.push(...data);
          this.loadingCuentaCargo = false;

          if ( this.tipoCartaOrden ) {
            this.carta.get('cuentaCargo')?.setValue( this.tipoCartaOrden.nuCuenBancCarg );
          }
        }, 
        (err) => {       
          this.loadingCuentaCargo = false;                                 
      });
  }

  public onChangeCuentaCargo(event: any): void {
    const cuentaSeleccionada = this.listaCuentaCargo.find(value => value.nuCuenBanc === event.value);
    this.carta.get('nombreCuentaCargo')?.setValue( cuentaSeleccionada.noCuenBanc );
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  private _buscarBancosAbono(): void {
    this.loadingBancoAbono = true;    
    this.generalService.getCbo_Bancos().pipe(takeUntil(this.unsubscribe$))
      .subscribe( 
        (data: any[]) => {             
          this.listaBancoAbono.push(...data);
          this.loadingBancoAbono = false;            
        }, 
        (err) => {       
          this.loadingBancoAbono = false;                                 
      });
  }

  public onChangeBancoAbono(idBanc: any, primeraCarga: boolean): void {
    this.loadingCuentaAbono = true;
    this.carta.get('nombreCuentaAbono')?.setValue( '' );
    if ( !primeraCarga ) { this.carta.get('cuentaAbono')?.setValue( '(NINGUNO)' ) }
    this.listaCuentaAbono = [];
    this.listaCuentaAbono.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
    this.generalService.getCbo_Cuentas(idBanc.value).pipe(takeUntil(this.unsubscribe$))
      .subscribe( 
        (data: any[]) => {
          this.listaCuentaAbono.push(...data);
          this.loadingCuentaAbono = false;

          if ( primeraCarga ) {
            this.carta.get('cuentaAbono')?.setValue( this.tipoCartaOrden.nuCuenBancAbon );
          }
        }, 
        (err) => {       
          this.loadingCuentaAbono = false;                                 
      });
  }

  public onChangeCuentaAbono(event: any): void {
    const cuentaSeleccionada = this.listaCuentaAbono.find(value => value.nuCuenBanc === event.value);
    if ( cuentaSeleccionada ) {
      this.readonlyNombre = true;
      this.carta.get('nombreCuentaAbono')?.setValue( cuentaSeleccionada.noCuenBanc );
    }
    else {
      this.readonlyNombre = false;
      this.carta.get('nombreCuentaAbono')?.setValue('');
    }
  }

  public validarMonto(event:any){
    const pattern = /^(-?\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public formatearNumero(event:any){
    if (event.target.value) {
      const nuevoValor = this.funciones.convertirTextoANumero( event.target.value );
      const number = Number(nuevoValor).toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });

      this.carta.get('monto')?.setValue( number );
    }
  }

  public initEditorInicio( event: any ) {
    this.quillInicio = event.editor;
  }

  public selectionChangeInicio( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillInicio.getSelection().index;
      
      this.quillInicio.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillInicio.setSelection(cursorPosition + value, 'user');
      this.insertarVariable = null;
    }
  }

  public initEditorConcepto( event: any ) {
    this.quillConcepto = event.editor;
  }

  public selectionChangeConcepto( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillConcepto.getSelection().index;
      
      this.quillConcepto.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillConcepto.setSelection(cursorPosition + value, 'user');
      this.insertarVariable = null;
    }
  }

  public initEditorFinal( event: any ) {
    this.quillFinal = event.editor;
  }

  public selectionChangeFinal( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillFinal.getSelection().index;
      
      this.quillFinal.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillFinal.setSelection(cursorPosition + value, 'user');
      this.insertarVariable = null;
    }
  }

  public grabar() {

    const boldInit = /<strong>/g;
    const boldFin = /<\/strong>/g;
    const italicInit = /<em>/g;
    const italicFin = /<\/em>/g;

    let resultado: boolean = this.validarFormulario();
    if (!resultado) {
      return;
    }
    this.utilService.onShowProcessLoading("Creando carta");
    const zona = this.carta.get('zona')?.value;
    const oficina = this.carta.get('oficina')?.value;
    const local = this.carta.get('local')?.value;
    const tipoDocuRef = this.carta.get('tipoDocuRef')?.value;
    const grupo = this.carta.get('grupo')?.value;
    const fecha = this.carta.get('fecha')?.value;
    const fechaFormat = this.validateService.formatDate( fecha );
    const montoCartOrde = this.funciones.convertirTextoANumero( this.carta.get('monto')?.value );

    const seccionInicio: string = this.carta.get('inicio')?.value;
    const seccionConcep: string = this.carta.get('concepto')?.value;
    const seccionFinal: string = this.carta.get('final')?.value;

    this.bodyNuevaCartaOrden.coZonaRegi = zona === '*' ? '' : zona;
    this.bodyNuevaCartaOrden.coOficRegi = oficina === '*' ? '' : oficina;
    this.bodyNuevaCartaOrden.coLocaAten = local === '*' ? '' : local;
    this.bodyNuevaCartaOrden.idTipoCartOrde = this.carta.get('tipoCarta')?.value;
    this.bodyNuevaCartaOrden.feCartOrde = fechaFormat;
    this.bodyNuevaCartaOrden.imCartOrde = Number( montoCartOrde );
    this.bodyNuevaCartaOrden.nuSiaf = this.carta.get('nroSIAF')?.value;
    this.bodyNuevaCartaOrden.nuCompPago = this.carta.get('nroComp')?.value;
    this.bodyNuevaCartaOrden.tiDocuRefe = tipoDocuRef === '*' ? '' : tipoDocuRef;
    this.bodyNuevaCartaOrden.nuDocuRefe = this.carta.get('nroDocuRef')?.value;
    this.bodyNuevaCartaOrden.coGrup = grupo == '*' ? '' : grupo;
    this.bodyNuevaCartaOrden.noDest = this.carta.get('destina')?.value;
    this.bodyNuevaCartaOrden.noCarg = this.carta.get('cargo')?.value;
    this.bodyNuevaCartaOrden.idBancCarg = this.carta.get('bancoCargo')?.value == '*' ? 0 : this.carta.get('bancoCargo')?.value;
    this.bodyNuevaCartaOrden.nuCuenBancCarg = this.carta.get('cuentaCargo')?.value == '(NINGUNO)' ? '' : this.carta.get('cuentaCargo')?.value;
    this.bodyNuevaCartaOrden.idBancAbon = this.carta.get('bancoAbono')?.value == '*' ? 0 : this.carta.get('bancoAbono')?.value;
    this.bodyNuevaCartaOrden.nuCuenBancAbon = this.carta.get('cuentaAbono')?.value == '(NINGUNO)' ? '' : this.carta.get('cuentaAbono')?.value;
    this.bodyNuevaCartaOrden.deSeccInic = seccionInicio.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>');
    this.bodyNuevaCartaOrden.deSeccConc = seccionConcep.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>');
    this.bodyNuevaCartaOrden.deSeccFina = seccionFinal.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>')
    
    const cuentaSeleccionada = this.listaCuentaAbono.find(value => value.nuCuenBanc === this.carta.get('cuentaAbono')?.value);
    this.bodyNuevaCartaOrden.noCuenBancAbon = cuentaSeleccionada ? '' : this.carta.get('nombreCuentaAbono')?.value;
    Swal.fire( 'Carta orden', 'Creando carta orden', 'info');
    this.$createCartaOrden = this.cartaService.createCartaOrden(this.bodyNuevaCartaOrden).subscribe({
      next: ( d ) => {
        if (d.codResult < 0 ) {
          Swal.fire( 'Carta orden', `${d.msgResult}`, 'info');
        } else if (d.codResult === 0) {
          this.utilService.onCloseLoading();
          this.router.navigate(['SARF/mantenimientos/documentos/carta_orden/bandeja']);
        }
      },
      error:( d )=>{
        if (d.error.category === "NOT_FOUND") {
          Swal.fire( 'Carta orden', `${d.error.description}`, 'info');
        }else {
          Swal.fire( 'Carta orden', `Ocurrio un error.`, 'error');
        }
      }
    });
  }

  private validarFormulario(): boolean {   
    if (this.carta.value.zona === "" || this.carta.value.zona === "*" || this.carta.value.zona === null) {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una zona registral.");
      return false;  
    } else if (this.carta.value.oficina === "" || this.carta.value.oficina === "*" || this.carta.value.oficina === null) {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una oficina registral.");
      return false;      
    } else if (this.carta.value.local === "" || this.carta.value.local === "*" || this.carta.value.local === null) {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un local.");
      return false;      
    } else if (this.carta.value.tipoCarta === "" || this.carta.value.tipoCarta === "*" || this.carta.value.tipoCarta === null) {      
      this.ddlTipoCarta.focus();
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un tipoCarta.");
      return false;      
    } else if (this.carta.value.fecha === "" || this.carta.value.fecha === null) {                    
      document.getElementById('idFecha').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de carta.");
      return false;      
    } else if (this.carta.value.monto === "" || this.carta.value.monto === null) {
      document.getElementById('idMonto').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese el monto.");
      return false;       
    } else if (this.carta.value.grupo === "" || this.carta.value.grupo === "*" || this.carta.value.grupo === null) {
      this.ddlGrupo.focus();
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un grupo.");
      return false;       
    } else {
      return true;
    }       
  }

  public async cancelar() {
    const dataFiltrosCartaOrden: IDataFiltrosCartaOrden = await this.sharingInformationService.compartirDataFiltrosCartaOrdenObservable.pipe(first()).toPromise(); 
    if (dataFiltrosCartaOrden.esBusqueda) {
      dataFiltrosCartaOrden.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosCartaOrdenObservableData = dataFiltrosCartaOrden;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/carta_orden/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaCartaOrdenObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete(); 
    this.$createCartaOrden.unsubscribe();     
  }

}
