import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';
import { ICartaOrden } from '../../../../../interfaces/carta-orden.interface';

import { ArchivoAdjunto, TramaAnexo } from '../../../../../models/archivo-adjunto.model';
import { BodyCartaAnexo } from '../../../../../models/mantenimientos/documentos/carta-orden.model';

import { GeneralService } from '../../../../../services/general.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { CartaOrdenService } from '../../../../../services/mantenimientos/documentos/carta-orden.service';

import { UploadComponent } from '../../../../upload/upload.component';

@Component({
  selector: 'app-carta-anexos',
  templateUrl: './carta-anexos.component.html',
  styleUrls: ['./carta-anexos.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class CartaAnexosComponent implements OnInit {

  isLoading!: boolean;
  coZonaRegi?: string;
  coOficRegi?: string;
  sizeFiles: number = 0;
  coTipoAdju: string = '003';

  add!: number;
  delete!: number;
  bandeja!: number;

  anexos!: FormGroup;

  files: ArchivoAdjunto[] = [];

  carta!: ICartaOrden[];
  cartaAnexo: TramaAnexo[] = [];
  cartaAnexoTemp: TramaAnexo[] = [];

  bodyCartaAnexo!: BodyCartaAnexo;

  @ViewChild(UploadComponent) upload: UploadComponent;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private generalService: GeneralService,
    private validarService: ValidatorsService,
    private cartaService: CartaOrdenService
  ) { }

  ngOnInit(): void {
    this.bodyCartaAnexo = new BodyCartaAnexo();
    this.carta = this.config.data.carta as ICartaOrden[];

    this.add = this.config.data.add;
    this.delete = this.config.data.delete;
    this.bandeja = this.config.data.bandeja;

    this.anexos = this.fb.group({
      nroCarta: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      nroSIAF: ['', [ Validators.required ]],
      nroComp: ['', [ Validators.required ]],
      docs: this.fb.array([])
    });

    this.hidrate( this.carta['0'] );
    this.anexosCarta();
  }

  get isDisabled() {
    return this.carta[0].esDocu != '001';
  }

  hidrate( d: ICartaOrden ) {

    const fecCarta = this.validarService.reFormateaFecha( d.feCartOrde );

    this.anexos.get('nroCarta')?.setValue( d.nuCartOrde );
    this.anexos.get('fecha')?.setValue( fecCarta );
    this.anexos.get('nroSIAF')?.setValue( d.nuSiaf );
    this.anexos.get('nroComp')?.setValue( d.nuCompPago.trim() );
    this.coZonaRegi = d.coZonaRegi;
    this.coOficRegi = d.coOficRegi;
  }

  anexosCarta() {
    this.cartaService.getCartaOrdenAnexo( this.carta['0'].idCartOrde ).subscribe({
      next: ( anexos ) => {
        console.log('cartAnex', anexos);
        this.cartaAnexoTemp = anexos.map( a => {
          return {
            id: a.idCartOrdeAnex,
            noDocuAnex: a.noDocuAnex,
            idGuidDocu: a.idGuidDocu
          }
        });

        this.upload.anexos = this.cartaAnexoTemp;
        this.upload.goEdit();
      }
    });
  }

  parse() {
    const docs = (this.anexos.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  grabar() {
    this.parse();
    console.log('this.files =', this.files);

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de grabar los anexos cargados?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        if ( this.files.length != 0 ) {
          this.saveAdjuntoInFileServer();
        }
        else {
          this.saveSustentoInBD();
        }
      },
      reject: () => {
        this.files = [];
      }
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.messageService.add({
              severity: 'warn',
              summary: 'Atención',
              detail: data.msgResult,
              sticky: true
            });
            
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            this.addSustento(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.saveSustentoInBD();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          this.files = [];
          this.ref.close('reject');
        }
      });
    }
  }

  addSustento(file?: any, guid?: string) {

    if ( file != '' && guid != '' ) {
      let trama = {} as TramaAnexo;

      trama.id = 0;
      trama.noDocuAnex = file.deDocuAdju;
      trama.idGuidDocu = guid;

      this.cartaAnexoTemp.push( trama );
    }
  }

  saveSustentoInBD() {
    for ( let anexo of this.cartaAnexoTemp ) {
      const index: number = this.anexos.get('docs')?.value.findIndex( ( d: any ) => anexo.noDocuAnex === d.nombre );
      this.cartaAnexo[index] = anexo;
    }

    this.cartaAnexo.forEach( c => {
      c.noDocuAnex = c.noDocuAnex.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
                      .replace(/"/g, '&quot;').replace(/'/g, '&apos;');
    });

    this.bodyCartaAnexo.idCartOrde = this.carta['0'].idCartOrde!;
    this.bodyCartaAnexo.trama = this.cartaAnexo;
    console.log('dataCart', this.bodyCartaAnexo );

    this.cartaService.updateCartaOrdenAnexo( this.bodyCartaAnexo ).subscribe({
      next: ( d: any ) => {

        if ( d.codResult < 0 ) {
          this.messageService.add({
            severity: 'warn',
            summary: 'Atención',
            detail: d.msgResult,
            sticky: true
          });
          
          Swal.close();
          this.files = [];
          return;
        }

        Swal.close();
        this.ref.close('accept');
      },
      error: ( e ) => {
        console.log( e );
        this.messageService.add({
          severity: 'error',
          summary: 'Atención',
          detail: 'Error al guardar sustento.',
          sticky: true
        });

        Swal.close();
        this.files = [];
        this.isLoading = false;
      },
      complete: () => {
        this.files = [];
        this.isLoading = false
      }
    });
  }

  addUploadedFile(file: any) {
    const anexos = this.anexos.get('docs').value.find( ( a: any ) => file.fileId === a.doc );
    if ( !anexos ) {
      (this.anexos.get('docs') as FormArray).push(
        this.fb.group(
          {
            doc: [file.fileId, []],
            nombre: [file.fileName, []],
            idAdjunto: [file?.idAdjunto, []]
          }
        )
      )
    }
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.anexos.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.cartaAnexoTemp.findIndex( f => f.id == fileId );
    this.cartaAnexoTemp.splice( index, 1 );
  }

  cancelar() {
    this.ref.close('');
  }

}
