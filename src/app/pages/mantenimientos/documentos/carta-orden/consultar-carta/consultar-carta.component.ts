import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { CartaOrdenService } from '../../../../../services/mantenimientos/documentos/carta-orden.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { ICartaOrdenById, IDataFiltrosCartaOrden } from 'src/app/interfaces/carta-orden.interface';
import { UtilService } from 'src/app/services/util.service';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { GeneralService } from 'src/app/services/general.service';
import { ITipoCartaOrden, ITipoCartaVariables } from '../../../../../interfaces/configuracion/tipo-carta-orden.interface';
import { TipoCartaOrdenService } from '../../../../../services/mantenimientos/configuracion/tipo-carta-orden.service';
@Component({
  selector: 'app-consultar-carta',
  templateUrl: './consultar-carta.component.html',
  styleUrls: ['./consultar-carta.component.scss']
})
export class ConsultarCartaComponent implements OnInit, OnDestroy {
  loadingTipoReferencia: boolean = false;
  loadingBancoCargo: boolean = false;
  loadingCuentaCargo: boolean = false;
  loadingBancoAbono: boolean = false;
  loadingCuentaAbono: boolean = false;
  loadingGrupos: boolean = false;
  readOnlyEditor: boolean = true;
  title: string;
  isEditar: boolean;
  fechaActual: Date;
  userCode:any
  idEdit!: number;
  msg!: string;
  cartaOrdenData: ICartaOrdenById;
  consultarCarta!: FormGroup;
  idBancCargo!: number;
  unsubscribe$ = new Subject<void>();
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[] = [];
  listaLocal: Local[] = [];
  listaTipoDocumento: ITipoCartaOrden[] = [];
  listaTipoReferencia: IResponse2[] = [];
  listaGrupos: IResponse2[] = [];
  listaVariables: ITipoCartaVariables[] = [];
  listaBancoCargo: any[] = [];
  listaCuentaCargo: any[] = [];
  listaBancoAbono: any[] = [];
  listaCuentaAbono: any[] = [];
  constructor( 
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private tipoCartaService: TipoCartaOrdenService,
    private cartaService: CartaOrdenService,
    private generalService: GeneralService,
    public funciones: Funciones,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService,
  ) { }

  ngOnInit(): void {
    this._getIdByRoute();
    this.userCode = localStorage.getItem('user_code')||'';
    this.fechaActual = new Date();
    this.consultarCarta = this.fb.group({
      zona: [{value:'', disabled: true}, [ Validators.required ]],
      oficina: [{value:'', disabled: true}, [ Validators.required ]],
      local: [{value:'', disabled: true}, [ Validators.required ]],
      tipoCarta: [{value:'', disabled: true}, [ Validators.required ]],
      nroCarta: [{value:'', disabled: true}, [ Validators.required ]],
      fecha: [{value:this.fechaActual, disabled: true}, [ Validators.required ]],
      monto: [{value:'', disabled: true}, [ Validators.required ]],
      nroSIAF: [{value:'', disabled: true}, [ Validators.required ]],
      nroComp: [{value:'', disabled: true}, [ Validators.required ]],
      tipoDocuRef: [{value:'', disabled: true}, [ Validators.required ]],
      nroDocuRef: [{value:'', disabled: true}, [ Validators.required ]],
      grupo: [{value:'', disabled: true}, [ Validators.required ]],
      destina: [{value:'', disabled: true}, [ Validators.required ]],
      cargo: [{value:'', disabled: true}, [ Validators.required ]],
      inicio: [{value:'', disabled: true}, [ Validators.required ]],
      concepto: [{value:'', disabled: true}, [ Validators.required ]],
      final: [{value:'', disabled: true}, [ Validators.required ]],
      bancoCargo: [{value:'', disabled: true}, [ Validators.required ]],
      cuentaCargo: [{value:'', disabled: true}, [ Validators.required ]],
      nombreCuentaCargo: [{value:'', disabled: true}, [ Validators.required ]],
      bancoAbono: [{value:'', disabled: true}, [ Validators.required ]],
      cuentaAbono: [{value:'', disabled: true}, [ Validators.required ]],
      nombreCuentaAbono: [{value:'', disabled: true}, [ Validators.required ]],
    });
  }

  private _getIdByRoute(): void {
    this.activatedRoute.params.pipe(takeUntil(this.unsubscribe$))
      .subscribe( params => this._getComprobantePagoById(params['id']))
  }

  private _getComprobantePagoById(id: string): void {
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.cartaService.cartaOrdenDataById(id).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.cartaOrdenData = data;
          this._handleInput();
          this._inicilializarListas();
          this._buscarTipoDocReferencia();
          this._buscarGruposCarta();
          this._obtenerVariables();
          this.utilService.onCloseLoading(); 
        },
        error: ( err ) => {
          this.utilService.onCloseLoading(); 
        }
      })
  }

  private _handleInput(): void {
    const fecha = this.cartaOrdenData.feCartOrde;
    const fechaFormat = `0${new Date(fecha).toLocaleDateString('es-PE')}`.slice(-10);
    this.consultarCarta.get('nroCarta')?.setValue( this.cartaOrdenData.nuCartOrde );
    this.consultarCarta.get('destina')?.setValue( this.cartaOrdenData.noDest );
    this.consultarCarta.get('cargo')?.setValue( this.cartaOrdenData.noCarg );
    this.consultarCarta.get('inicio')?.setValue( this.cartaOrdenData.deSeccInic );
    this.consultarCarta.get('concepto')?.setValue( this.cartaOrdenData.deSeccConc );
    this.consultarCarta.get('final')?.setValue( this.cartaOrdenData.deSeccFina );
    this.consultarCarta.get('fecha')?.setValue( fechaFormat );
    this.consultarCarta.get('monto')?.setValue( this.cartaOrdenData.imCartOrde );
    this.consultarCarta.get('nroSIAF')?.setValue( this.cartaOrdenData.nuSiaf );
    this.consultarCarta.get('nroComp')?.setValue( this.cartaOrdenData.nuCompPago );
    this.consultarCarta.get('nroDocuRef')?.setValue( this.cartaOrdenData.nuDocuRefe );
    this.consultarCarta.get('nombreCuentaCargo')?.setValue( this.cartaOrdenData.noCuenBancCarg );
    this.consultarCarta.get('nombreCuentaAbono')?.setValue( this.cartaOrdenData.noCuenBancAbon );
  }

  private _inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaTipoReferencia = [];
    this.listaBancoCargo = []
    this.listaCuentaCargo = []
    this.listaBancoAbono = []
    this.listaCuentaAbono = []
    this.listaZonaRegistral.push({ coZonaRegi: this.cartaOrdenData.coZonaRegi, deZonaRegi: this.cartaOrdenData.deZonaRegi });
    this.listaOficinaRegistral.push({ coOficRegi: this.cartaOrdenData.coOficRegi, deOficRegi: this.cartaOrdenData.deOficRegi });
    this.listaLocal.push({ coLocaAten: this.cartaOrdenData.coLocaAten, deLocaAten: this.cartaOrdenData.deLocaAten }); 
    this.listaTipoDocumento.push({ idTipoCartOrde: this.cartaOrdenData.idTipoCartOrde, noTipoCartOrde: this.cartaOrdenData.noTipoCartOrde }); 
    // this.listaTipoReferencia.push({ ccodigoHijo: this.cartaOrdenData.tiDocuRefe, cdescri: this.cartaOrdenData.nuDocuRefe }); 
    this.listaTipoReferencia.push({ ccodigoHijo: '*', cdescri: '(NINGUNO)' }); 
    this.listaBancoCargo.push({ nidInstitucion: this.cartaOrdenData.idBancCarg, cdeInst: this.cartaOrdenData.noBancCarg });   
    this.listaCuentaCargo.push({ idBanc: this.cartaOrdenData.nuCuenBancCarg, nuCuenBanc: this.cartaOrdenData.nuCuenBancCarg });  
    this.listaBancoAbono.push({ nidInstitucion: this.cartaOrdenData.idBancAbon, cdeInst: this.cartaOrdenData.noBancAbon });   
    this.listaCuentaAbono.push({ idBanc: this.cartaOrdenData.nuCuenBancAbon, nuCuenBanc: this.cartaOrdenData.nuCuenBancAbon });
    this.consultarCarta.patchValue({'tipoDocuRef': this.cartaOrdenData.tiDocuRefe });
  }

  private _buscarTipoDocReferencia(): void {
    this.loadingTipoReferencia = true;  
    const docReferenciaItem: any = { ccodigoHijo  : '*', cdescri: '(NINGUNO)' };
    this.generalService.getCbo_EstadoReferencia().pipe(takeUntil(this.unsubscribe$))
    .subscribe({
      next: ( data ) => {
        this.loadingTipoReferencia = false;
        this.listaTipoReferencia.push(...data);
        this.consultarCarta.patchValue({'tipoDocuRef': this.cartaOrdenData.tiDocuRefe });
      },
      error: ( err ) => {
        this.loadingTipoReferencia = false; 
      }
    })
  }

  private _buscarGruposCarta(): void {
    this.loadingGrupos = true;
    this.generalService.getCbo_GruposCarta().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data ) => {
        this.loadingGrupos = false;    
        if (data.length === 1) {   
          this.listaGrupos.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
          this.consultarCarta.patchValue({ "grupo": data[0].ccodigoHijo });
        } else {
          this.listaGrupos.push(...data);
        }

        this.consultarCarta.get('grupo')?.setValue( this.cartaOrdenData.coGrup );
      },
      error:( err )=>{
        this.loadingGrupos = false;    
      }
    });
  }

  private _obtenerVariables(): void {
    this.tipoCartaService.getTipoCartaVariables().pipe( takeUntil(this.unsubscribe$) )
      .subscribe({
        next: ( data ) => {
          this.listaVariables = data;
        },
        error: () => {
          this.listaVariables.push({ noCamp: '', deCamp: 'No se cargaron las variables'});
        }
      });
  }

  public dragStart( variable: any ) {}

  public dragEnd() {}

  public dropInicio() {}

  public dropConcepto() {}

  public dropFinal() {}

  public async cancelar() {
    const dataFiltrosCartaOrden: IDataFiltrosCartaOrden = await this.sharingInformationService.compartirDataFiltrosCartaOrdenObservable.pipe(first()).toPromise(); 
    if (dataFiltrosCartaOrden.esBusqueda) {
      dataFiltrosCartaOrden.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosCartaOrdenObservableData = dataFiltrosCartaOrden;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/carta_orden/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaCartaOrdenObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete(); 
  }

}
