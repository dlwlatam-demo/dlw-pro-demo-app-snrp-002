import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';
import { ICartaOrden } from '../../../../../interfaces/carta-orden.interface';

import { ArchivoAdjunto, TramaSustento } from '../../../../../models/archivo-adjunto.model';
import { BodyCartaSustento } from '../../../../../models/mantenimientos/documentos/carta-orden.model';

import { GeneralService } from '../../../../../services/general.service';
import { CartaOrdenService } from '../../../../../services/mantenimientos/documentos/carta-orden.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';

import { UploadComponent } from '../../../../upload/upload.component';

@Component({
  selector: 'app-carta-sustentos',
  templateUrl: './carta-sustentos.component.html',
  styleUrls: ['./carta-sustentos.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class CartaSustentosComponent implements OnInit {

  isLoading!: boolean;
  coZonaRegi?: string;
  coOficRegi?: string;
  sizeFiles: number = 0;
  coTipoAdju: string = '003';

  add!: number;
  delete!: number;
  bandeja!: number;

  sustento!: FormGroup;

  files: ArchivoAdjunto[] = [];

  carta!: ICartaOrden[];
  cartaSustento: TramaSustento[] = [];
  cartaSustentoTemp: TramaSustento[] = [];

  bodyCartaSustento!: BodyCartaSustento;

  @ViewChild(UploadComponent) upload: UploadComponent;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private validarService: ValidatorsService,
    private cartaService: CartaOrdenService
  ) { }

  ngOnInit(): void {
    this.bodyCartaSustento = new BodyCartaSustento();
    this.carta = this.config.data.carta as ICartaOrden[];

    this.add = this.config.data.add;
    this.delete = this.config.data.delete;
    this.bandeja = this.config.data.bandeja;

    this.sustento = this.fb.group({
      nroCarta: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      nroSIAF: ['', [ Validators.required ]],
      nroComp: ['', [ Validators.required ]],
      docs: this.fb.array([])
    });

    this.hidrate( this.carta['0'] );
    this.sustentosCarta();
  }

  get isDisabled() {
    return this.carta[0].esDocu != '001';
  }

  hidrate( d: ICartaOrden ) {

    const fecCarta = this.validarService.reFormateaFecha( d.feCartOrde );

    this.sustento.get('nroCarta')?.setValue( d.nuCartOrde );
    this.sustento.get('fecha')?.setValue( fecCarta );
    this.sustento.get('nroSIAF')?.setValue( d.nuSiaf );
    this.sustento.get('nroComp')?.setValue( d.nuCompPago.trim() );
    this.coZonaRegi = d.coZonaRegi;
    this.coOficRegi = d.coOficRegi;
  }

  sustentosCarta() {
    this.cartaService.getCartaOrdenSustento( this.carta['0'].idCartOrde ).subscribe({
      next: ( sustentos ) => {
        console.log('cartSust', sustentos);
        this.cartaSustentoTemp = sustentos.map( s => {
          return {
            id: s.idCartOrdeSust,
            noDocuSust: s.noDocuSust,
            idGuidDocu: s.idGuidDocu
          }
        });

        this.upload.sustentos = this.cartaSustentoTemp;
        this.upload.goEdit();
      }
    });
  }

  parse() {
    const docs = (this.sustento.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  grabar() {
    this.parse();
    console.log('this.files =', this.files);

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de grabar los sustentos cargados?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        if ( this.files.length != 0 ) {
          this.saveAdjuntoInFileServer();
        }
        else {
          this.saveSustentoInBD();
        }
      },
      reject: () => {
        this.files = [];
      }
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.messageService.add({
              severity: 'warn',
              summary: 'Atención',
              detail: data.msgResult,
              sticky: true
            });

            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            this.addSustento(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.saveSustentoInBD();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          this.files = [];
          this.ref.close('reject');
        }
      });
    }
  }

  addSustento(file?: any, guid?: string) {

    if ( file != '' && guid != '' ) {
      let trama = {} as TramaSustento

      trama.id = 0,
      trama.noDocuSust = file.deDocuAdju,
      trama.idGuidDocu = guid

      this.cartaSustentoTemp.push( trama );
    }
  }

  saveSustentoInBD() {
    for ( let carta of this.cartaSustentoTemp ) {
      const index: number = this.sustento.get('docs')?.value.findIndex( ( d: any ) => carta.noDocuSust === d.nombre );
      this.cartaSustento[index] = carta;
    }

    this.cartaSustento.forEach( c => {
      c.noDocuSust = c.noDocuSust.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
                      .replace(/"/g, '&quot;').replace(/'/g, '&apos;');
    });

    this.bodyCartaSustento.idCartOrde = this.carta['0'].idCartOrde!;
    this.bodyCartaSustento.trama = this.cartaSustento;
    console.log('dataCart', this.bodyCartaSustento );

    this.cartaService.updateCartaOrdenSustento( this.bodyCartaSustento ).subscribe({
      next: ( d: any ) => {

        if ( d.codResult < 0 ) {
          this.messageService.add({
            severity: 'warn',
            summary: 'Atención',
            detail: d.msgResult,
            sticky: true
          });

          Swal.close();
          this.files = [];
          return;
        }

        Swal.close();
        this.ref.close('accept');
      },
      error: ( e ) => {
        console.log( e );
        this.messageService.add({
          severity: 'error',
          summary: 'Atención',
          detail: 'Error al guardar sustento.',
          sticky: true
        });

        Swal.close();
        this.files = [];
        this.isLoading = false
      },
      complete: () => {
        this.files = [];
        this.isLoading = false
      }
    });
  }

  addUploadedFile(file: any) {
    const sustento = this.sustento.get('docs').value.find( ( d: any ) => file.fileId === d.doc );
    if ( !sustento ) {
      (this.sustento.get('docs') as FormArray).push(
        this.fb.group(
          {
            doc: [file.fileId, []],
            nombre: [file.fileName, []],
            idAdjunto: [file?.idAdjunto, []]
          }
        )
      )
    }
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.sustento.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.cartaSustentoTemp.findIndex( f => f.id == fileId );
    this.cartaSustentoTemp.splice( index, 1 );
  }

  cancelar() {
    this.ref.close('');
  }

}
