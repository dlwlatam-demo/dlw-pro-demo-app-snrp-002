import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { BodyCartaOrden, BodyEliminarActivarCartaOrden, CartaOrden } from '../../../../../models/mantenimientos/documentos/carta-orden.model';
import { ICartaOrden, ICartaOrdenById, INuevaCartaOrden } from 'src/app/interfaces/carta-orden.interface';
import { CartaOrdenService } from '../../../../../services/mantenimientos/documentos/carta-orden.service';
import { EnviaFirma } from '../../../../../models/mantenimientos/documentos/envia-firma.model';
import { DocumentoFirmaService } from '../../../../../services/mantenimientos/documentos/documento-firma.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';
import { GeneralService } from '../../../../../services/general.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-carta-solicitar-firma',
  templateUrl: './carta-solicitar-firma.component.html',
  styleUrls: ['./carta-solicitar-firma.component.scss'],
  providers: [
    MessageService
  ]
})
export class CartaSolicitarFirmaComponent implements OnInit {

  firma!: FormGroup;

  cartaOrden!: ICartaOrden[];
  isLoading!: boolean;
  guidDocumento!: string;
  doc: any;
  
  docVisto: boolean = false;
  roles?: string[];
  perfil?: string;

  constructor(
    private fb: FormBuilder,
    private config: DynamicDialogConfig,
    private ref: DynamicDialogRef,
    private cartaOrdenService: CartaOrdenService,
	  private documentoFirmaService: DocumentoFirmaService,
    private messageService: MessageService,
    private utilService: UtilService,
    private validateService: ValidatorsService,
	  private generalService: GeneralService
  ) { }

  ngOnInit(): void {

    this.cartaOrden = this.config.data.carta as ICartaOrden[];
    this.guidDocumento = this.config.data.guidDocumento;
    this.doc = this.config.data.doc;

    this.firma = this.fb.group({
      nuCartOrde: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      nroSiaf: ['', [ Validators.required ]],
      nroComp: ['', [ Validators.required ]]
    });

    this.cartaOrdenService.getCartaOrdenDataById( this.cartaOrden['0'].idCartOrde! ).subscribe({
      next: ( data ) => {
        this.hidrate( data as ICartaOrdenById )
      },
      error: ( e ) => {
        console.log( e );
      }
    });
  }

  hidrate( d: ICartaOrdenById ) {
    this.firma.disable();

    const fecha = this.validateService.reFormateaFecha( d.feCartOrde );

    this.firma.get('nuCartOrde')?.setValue( d.nuCartOrde );
    this.firma.get('fecha')?.setValue( fecha );
    this.firma.get('nroSiaf')?.setValue( d.nuSiaf );
    this.firma.get('nroComp')?.setValue( d.nuCompPago );
  }

  grabar() {
    this.isLoading = true;
    this.postAdjunto();
  }
  
  postAdjunto() {
    this.roles = JSON.parse( localStorage.getItem('roles_sarf')! );
    this.roles?.forEach( r => {
      this.perfil = r;
    });

    const token = localStorage.getItem('t_sarf')!;
    
    const data = {
      idGuidDocu: this.guidDocumento,
      deDocuAdju: "CartaOrden" + this.cartaOrden['0'].idCartOrde! + ".pdf",
      coTipoAdju: '003', // Carta Orden 
      coZonaRegi: this.cartaOrden['0'].coZonaRegi!,
      coOficRegi: this.cartaOrden['0'].coOficRegi!
    }
    console.log('data', data )

    this.generalService.grabarAdjunto( data ).subscribe({
      next: ( d ) => {
	  
        let dataFirma: EnviaFirma = new EnviaFirma();
        dataFirma.tiDocu = data.coTipoAdju;
        dataFirma.idDocuOrig = this.cartaOrden['0'].idCartOrde!;
        dataFirma.noDocuPdf = data.deDocuAdju;
        dataFirma.noRutaDocuPdf = this.cartaOrden['0'].noRutaDocuPdf!;
        dataFirma.idGuidDocu = data.idGuidDocu;
	
        this.documentoFirmaService.setDocumentoFirma(dataFirma).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0 ) {
              this.generalService.eliminarAdjunto( this.guidDocumento, this.perfil!, token! ).subscribe({
                next: ( _d1 ) => {
                  this.docVisto = false;

                  this.messageService.add({
                    severity: 'info',
                    summary: 'Atención',
                    detail: d.msgResult,
                    sticky: true
                  });
                }
              });
            } else if (d.codResult === 0 ) {

              this.ref.close('accept');
            }
          },
          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              this.messageService.add({
                severity: 'error',
                summary: 'Atención',
                detail: d.error.description,
                sticky: true
              });        

            }else {
              this.messageService.add({
                severity: 'warn',
                summary: 'Atención',
                detail: 'El servicio no esta disponible en estos momentos, inténtelo mas tarde.',
                sticky: true
              });
            }
          },
          complete: () => {
            this.isLoading = false
          }
        });	
      },
      error: ( e ) => {
        console.log('error', e );
        this.isLoading = false;

        this.messageService.add({
          severity: 'warn',
          summary: 'Atención',
          detail: 'Ocurrió un error en el servicio. Por favor, comuniquese con el administrador del sistema.',
          sticky: true
        });
      }
    });
  }
  
  

  cancelar() {
    this.ref.close('');
  }
  
  
  verDocumento() {
    console.log('verDocumento');

    const byteCharacters = atob(this.doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/pdf' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    //download.setAttribute('download', 'Constacia_Abono');
    download.setAttribute("target", "_blank");
    document.body.appendChild( download );
    download.click();
    
    this.docVisto = true;
	
  }
  
  


}
