import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { SortEvent } from 'primeng/api';

import { ICartaOrden } from '../../../../../interfaces/carta-orden.interface';

import { environment } from '../../../../../../environments/environment';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { CartaOrdenService } from '../../../../../services/mantenimientos/documentos/carta-orden.service';

@Component({
  selector: 'app-carta-historial-firma',
  templateUrl: './carta-historial-firma.component.html',
  styleUrls: ['./carta-historial-firma.component.scss']
})
export class CartaHistorialFirmaComponent implements OnInit {

  cartaOrden!: ICartaOrden[];
  historial!: FormGroup;

  listaHistorial!: any[];

  currentPage: string = environment.currentPage;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private validateService: ValidatorsService,
    private cartaService: CartaOrdenService
  ) { }

  ngOnInit(): void {

    this.cartaOrden = this.config.data.carta as ICartaOrden[];

    this.historial = this.fb.group({
      nroCarta: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      nroSiaf: ['', [ Validators.required ]],
      nroComp: ['', [ Validators.required ]]
    });

    this.hidrate( this.cartaOrden[0] );
  }

  hidrate( c: ICartaOrden ) {

    const fecComp = this.validateService.reFormateaFecha( c.feCartOrde );

    this.historial.get('nroCarta')?.setValue( c.nuCartOrde );
    this.historial.get('fecha')?.setValue( fecComp );
    this.historial.get('nroSiaf')?.setValue( c.nuSiaf );
    this.historial.get('nroComp')?.setValue( c.nuCompPago );

    this.cartaService.getHistorialFirmaCartaOrden( c.idDocuFirm ).subscribe({
      next: ( d ) => {
        this.listaHistorial = d;
      },
      error: ( e ) => {
        console.log( e );
        this.listaHistorial = [];
      }
    })
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  regresar() {
    this.ref.close();
  }

}
