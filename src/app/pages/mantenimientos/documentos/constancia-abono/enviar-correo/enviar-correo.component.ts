import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { bodyMail, ConstanciaAbono } from 'src/app/models/mantenimientos/documentos/constancia-abono.model';
import { ConstanciaAbonoService } from 'src/app/services/mantenimientos/documentos/constancia-abono.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-enviar-correo',
  templateUrl: './enviar-correo.component.html',
  styleUrls: ['./enviar-correo.component.scss']
})
export class EnviarCorreoComponent implements OnInit {

  constancia!: ConstanciaAbono[];

  correo!: FormGroup;
  link : string= 'https://sarf-content-manager-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe/sarfsunarp/api/v1/digital/obtenerAdjunSarf/'
  constructor(
    private ref: DynamicDialogRef, 
    private config: DynamicDialogConfig,
    private fb: FormBuilder,
    private constanciaService: ConstanciaAbonoService 

  ) { }

  ngOnInit(): void {


    this.constancia = this.config.data.csta as ConstanciaAbono[];


    this.correo = this.fb.group({
      para: ['', [ Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$') ]],
 
    });
  }


  grabar() {

    const asunto = `CONSTANCIA DE ABONO N° ${ this.constancia[0].nuConsAbon}`;
    const mensaje = `Buenos días Sr ${this.constancia[0].noInstGira}
 
    Por el presente le remito la CONSTANCIA DE ABONO N.° ${ this.constancia[0].nuConsAbon}, correspondiente al Proyecto: ${ this.constancia[0].noProyCtaTerc}.
    del: ${ this.constancia[0].cdeInst}
    
    Descargar la Constancia de Abono:
    ${this.link}${this.constancia[0].idGuidDocu}

    Atentamente,`;
    // <a href="${this.link}${this.constancia[0].idGuidDocu}" target="_blank"> Descargar la Constancia de Abono </a>

    const data:bodyMail ={
      mailMfrom :'aplicacion@sunarp.gob.pe',
      mailMto :this.correo.get('para')?.value,
      mailSubj : asunto,
      mailBody : mensaje,
    }
    this.constanciaService.enviarEmail(data ).subscribe({
      next: ( d:any ) => {
        if (d.codResult < 0 ) {
          Swal.fire(d.msgResult, '', 'info');
          return;
        }
        Swal.fire('Correo enviado!', '', 'success').then(()=>{
          this.ref.close();
        });
      },
      error: ( e ) => {
        Swal.fire('Error al enviar el correo.', '', 'info')
      }
    });

   }
  cancelar() {
    this.ref.close();
  }

}
