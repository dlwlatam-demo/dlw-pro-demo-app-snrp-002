import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ConstanciaAbono } from '../../../../../models/mantenimientos/documentos/constancia-abono.model';
import { EnviaFirma } from '../../../../../models/mantenimientos/documentos/envia-firma.model';
import { ConstanciaAbonoService } from '../../../../../services/mantenimientos/documentos/constancia-abono.service';
import { DocumentoFirmaService } from '../../../../../services/mantenimientos/documentos/documento-firma.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';
import { GeneralService } from '../../../../../services/general.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ValidatorsService } from '../../../../../core/services/validators.service';

@Component({
  selector: 'app-solicitar-firma-constancia',
  templateUrl: './solicitar-firma-constancia.component.html',
  styleUrls: ['./solicitar-firma-constancia.component.scss'],
  providers: [
    MessageService
  ]
})
export class SolicitarFirmaConstanciaComponent implements OnInit {

  isLoading!: boolean;

  firma!: FormGroup;

  constancia!: ConstanciaAbono[];
  guidDocumento!: string;
  doc: any;
  
  docVisto: boolean = false;
  roles?: string[];
  perfil?: string;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private messageService: MessageService,
    private constanciaService: ConstanciaAbonoService,
	  private documentoFirmaService: DocumentoFirmaService,
    private utilService: UtilService,
    private validateService: ValidatorsService,
	  private generalService: GeneralService
  ) { }

  ngOnInit(): void {

    this.constancia = this.config.data.csta as ConstanciaAbono[];
    this.guidDocumento = this.config.data.guidDocumento;
    this.doc = this.config.data.doc;

    this.firma = this.fb.group({
      nroConst: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      nroOpera: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]]
    });

    this.constanciaService.getConstanciaAbonoById( this.constancia['0'].idConsAbon! ).subscribe({
      next: ( data ) => {
        this.hidrate( data as ConstanciaAbono )
      },
      error: ( e ) => {
        console.log( e );
      }
    });
  }

  hidrate( d: ConstanciaAbono ) {
    this.firma.disable();

    const fechaDepo: string = this.validateService.reFormateaFecha( d.feDepo );
    const montoDepo: string = Number( d.moDepo ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });

    this.firma.get('nroConst')?.setValue( d.nuConsAbon );
    this.firma.get('fecha')?.setValue( fechaDepo );
    this.firma.get('nroOpera')?.setValue( d.nuOperaDepo );
    this.firma.get('monto')?.setValue( montoDepo );
  }

  grabar() {
    console.log('Solicitando Firma');

    Swal.showLoading();
    this.isLoading = true;

    this.postAdjunto();
	
  }
  
   postAdjunto() {
    this.roles = JSON.parse( localStorage.getItem('roles_sarf')! );
    this.roles?.forEach( r => {
      this.perfil = r;
    });

    const token = localStorage.getItem('t_sarf')!;
    
    const data = {
      idGuidDocu: this.guidDocumento,
      deDocuAdju: 'ConstanciaAbono' + this.constancia['0'].idConsAbon! + '.pdf',
      coTipoAdju: '004', // Constancia de Abono
      coZonaRegi: this.constancia['0'].coZonaRegi!,
      coOficRegi: this.constancia['0'].coOficRegi!
    }

    this.generalService.grabarAdjunto( data ).subscribe({
      next: ( _d ) => {
	  
        let dataFirma: EnviaFirma = new EnviaFirma();
        dataFirma.tiDocu = data.coTipoAdju;
        dataFirma.idDocuOrig = this.constancia['0'].idConsAbon!;
        dataFirma.noDocuPdf = data.deDocuAdju;
        dataFirma.noRutaDocuPdf = this.constancia['0'].noRutaDocuPdf!;
        dataFirma.idGuidDocu = data.idGuidDocu;
	
       this.documentoFirmaService.setDocumentoFirma(dataFirma).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0 ) {
              this.generalService.eliminarAdjunto( this.guidDocumento, this.perfil!, token! ).subscribe({
                next: ( d1 ) => {
                  Swal.close();
                  this.docVisto = false;

                  this.messageService.add({
                    severity: 'info',
                    summary: 'Atención',
                    detail: d.msgResult,
                    sticky: true
                  });
                }
              });
            } else if (d.codResult === 0 ) {
              Swal.close();
              this.ref.close('accept');
            }
          },
          error:( e )=>{
            if (e.error.category === "NOT_FOUND") {
              this.messageService.add({
                severity: 'info',
                summary: 'Atención',
                detail: e.error.description,
                sticky: true
              });

            }else {
              this.messageService.add({
                severity: 'warn',
                summary: 'Atención',
                detail: 'El servicio no esta disponible en estos momentos, inténtelo mas tarde.',
                sticky: true
              });        
            }
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      },
      error: ( e ) => {
        console.log( e );
        this.ref.close('reject');
      }
    });
  }
  
  

  cancelar() {
    this.ref.close('');
  }
  
  
  verDocumento() {
    console.log('verDocumento');

    const byteCharacters = atob(this.doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/pdf' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    //download.setAttribute('download', 'Constacia_Abono');
    download.setAttribute("target", "_blank");
    document.body.appendChild( download );
    download.click();
    
    this.docVisto = true;
	
  }
  
  

}
