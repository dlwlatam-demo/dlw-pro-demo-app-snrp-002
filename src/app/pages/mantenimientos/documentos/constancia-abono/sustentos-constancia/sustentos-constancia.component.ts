import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';

import { ArchivoAdjunto, TramaSustento } from '../../../../../models/archivo-adjunto.model';
import { ConstanciaAbono } from '../../../../../models/mantenimientos/documentos/constancia-abono.model';

import { ConstanciaAbonoService } from '../../../../../services/mantenimientos/documentos/constancia-abono.service';
import { GeneralService } from '../../../../../services/general.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';

import { UploadComponent } from '../../../../upload/upload.component';

@Component({
  selector: 'app-sustentos-constancia',
  templateUrl: './sustentos-constancia.component.html',
  styleUrls: ['./sustentos-constancia.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class SustentosConstanciaComponent implements OnInit {

  isLoading!: boolean;
  coZonaRegi?: string;
  coOficRegi?: string;
  sizeFiles: number = 0;
  coTipoAdju: string = '004';

  add!: number;
  delete!: number;
  bandeja!: number;

  sustento!: FormGroup;

  files: ArchivoAdjunto[] = [];

  constancia!: ConstanciaAbono[];
  constanciaSustento: TramaSustento[] = [];
  constanciaSustentoTemp: TramaSustento[] = [];

  @ViewChild(UploadComponent) upload: UploadComponent;
  
  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private validarService: ValidatorsService,
    private constanciaService: ConstanciaAbonoService
  ) { }

  ngOnInit(): void {

    this.constancia = this.config.data.csta as ConstanciaAbono[];

    this.add = this.config.data.add;
    this.delete = this.config.data.delete;
    this.bandeja = this.config.data.bandeja;

    this.sustento = this.fb.group({
      nroConst: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      nroOpera: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]],
      docs: this.fb.array([])
    });

    this.constanciaService.getConstanciaAbonoById( this.constancia['0'].idConsAbon! ).subscribe({
      next: ( data ) => {
        console.log('constancia', data)
        this.hidrate( data as ConstanciaAbono )
      },
      error: ( e ) => {
        Swal.fire('Changes are not saved', '', 'info')
      }
    });

    this.constanciaService.getBandejaConstanciaAbonoSustento( this.constancia['0'].idConsAbon! ).subscribe({
      next: ( sustentos ) => {
        console.log('constSust', sustentos);
        this.constanciaSustentoTemp = sustentos.map( s => {
          return {
            id: s.idConsAbonSust, 
            noDocuSust: s.noDocuSust, 
            idGuidDocu: s.idGuidDocu
          }
        });

        this.upload.sustentos = this.constanciaSustentoTemp;
        this.upload.goEdit();
      }
    });
  }

  hidrate( d: ConstanciaAbono ) {
  
    const fechaDepo: string = this.validarService.reFormateaFecha( d.feDepo );
    const montoDepo: string = Number( d.moDepo ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });

    this.sustento.get('nroConst')?.setValue( d.nuConsAbon );
    this.sustento.get('fecha')?.setValue( fechaDepo );
    this.sustento.get('nroOpera')?.setValue( d.nuOperaDepo );
    this.sustento.get('monto')?.setValue( montoDepo );
    this.coZonaRegi = d.coZonaRegi;
    this.coOficRegi = d.coOficRegi;
  }

  addUploadedFile(file: any) {
    const sustento = this.sustento.get('docs').value.find( ( d: any ) => file.fileId === d.doc );
    if ( !sustento ) {
      (this.sustento.get('docs') as FormArray).push(
        this.fb.group(
          {
            doc: [file.fileId, []],
            nombre: [file.fileName, []],
            idAdjunto: [file?.idAdjunto, []]
          }
        )
      )
    }
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.sustento.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.constanciaSustentoTemp.findIndex( f => f.id == Number( fileId ) );
    this.constanciaSustentoTemp.splice( index, 1 );
  }

  parse() {
    const docs = (this.sustento.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  grabar() {
    this.parse();
    console.log('this.files =', this.files);

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de grabar los sustentos cargados?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        if ( this.files.length != 0 ) {
          this.saveAdjuntoInFileServer();
        }
        else {
          this.saveSustentoInBD();
        }
      },
      reject: () => {
        this.files = [];
      }
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.messageService.add({
              severity: 'warn',
              summary: 'Atención',
              detail: data.msgResult,
              sticky: true
            });

            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            this.addSustento(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.saveSustentoInBD();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          this.files = [];
          this.ref.close('reject');
        }
      });
    }
  }

  addSustento(file?: any, guid?: string) {

    if ( file != '' && guid != '' ) {
      let trama = {} as TramaSustento

      trama.id = '0',
      trama.noDocuSust = file.deDocuAdju,
      trama.idGuidDocu = guid

      this.constanciaSustentoTemp.push( trama );
    }
  }

  saveSustentoInBD() {
    for ( let constancia of this.constanciaSustentoTemp ) {
      const index: number = this.sustento.get('docs')?.value.findIndex( ( d: any ) => constancia.noDocuSust === d.nombre );
      this.constanciaSustento[index] = constancia;
    }

    this.constanciaSustento.forEach( c => {
      c.noDocuSust = c.noDocuSust.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
                      .replace(/"/g, '&quot;').replace(/'/g, '&apos;');
    });

    const dataConst = {
      idConsAbon: this.constancia['0'].idConsAbon!,
      trama: this.constanciaSustento
    }
    console.log('dataConst', dataConst );

    this.constanciaService.saveBandejaConstanciaAbonoSustento( dataConst ).subscribe({
      next: ( d: any ) => {

        if ( d.codResult < 0 ) {
          this.messageService.add({
            severity: 'warn',
            summary: 'Atención',
            detail: d.msgResult,
            sticky: true
          });

          this.files = [];
          return;
        }

        Swal.close();
        this.ref.close('accept');
      },
      error: ( e ) => {
        console.log( e );
        this.messageService.add({
          severity: 'error',
          summary: 'Atención',
          detail: 'Error al guardar sustento.',
          sticky: true
        });

        Swal.close();
        this.files = [];
        this.isLoading = false;
      },
      complete: () => {
        this.files = [];
        this.isLoading = false
      }
    });
  }

  get isDisabled() {
    return this.constancia['0'].esDocu != '001'
  }

  cancelar() {
    this.ref.close('');
  }

}
