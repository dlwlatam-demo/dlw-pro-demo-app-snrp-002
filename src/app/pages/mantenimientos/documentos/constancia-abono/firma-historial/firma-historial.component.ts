import { Component, OnInit } from '@angular/core';

import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { SortEvent } from 'primeng/api';

import { environment } from '../../../../../../environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConstanciaAbono, historiaFirmaConstancia } from '../../../../../models/mantenimientos/documentos/constancia-abono.model';
import { ConstanciaAbonoService } from '../../../../../services/mantenimientos/documentos/constancia-abono.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';

@Component({
  selector: 'app-firma-historial',
  templateUrl: './firma-historial.component.html',
  styleUrls: ['./firma-historial.component.scss']
})
export class FirmaHistorialComponent implements OnInit {

  historial!: FormGroup;
  enviadosList!: historiaFirmaConstancia[];
  
  constancia!: ConstanciaAbono[];
  isDisabled:boolean= false;
  currentPage: string = environment.currentPage;

  constructor( 
    private ref: DynamicDialogRef, 
    private config: DynamicDialogConfig,
    private fb: FormBuilder,
    private validateService: ValidatorsService,
    private constanciaService: ConstanciaAbonoService
  ) { }

  ngOnInit(): void {

    this.constancia = this.config.data.csta as ConstanciaAbono[];

    this.historial = this.fb.group({
      nroConst: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      nroOpera: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]]
    });

    this.constanciaService.getConstanciaAbonoById( this.constancia['0'].idConsAbon! ).subscribe({
      next: ( data ) => {
        this.hidrate( data as ConstanciaAbono )
      },
      error: ( e ) => {
        console.log( e );
      }
    });

  }

  hidrate( d: ConstanciaAbono ) {
    this.historial.disable();
    const feDepo =  d.feDepo||'' ;
    const feDepoFormat: string = this.validateService.reFormateaFecha( feDepo );
    const moDepo = Number(d.moDepo).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });

    this.historial.get('nroConst')?.setValue( d.nuConsAbon );
    this.historial.get('fecha')?.setValue( feDepoFormat );
    this.historial.get('nroOpera')?.setValue( d.nuOperaDepo );
    this.historial.get('monto')?.setValue( moDepo);

     this.constanciaService.getDocumentoFirmaHistoricoById(d.idDocuFirm!).subscribe({
      next: ( d ) => {
        console.log('historial', d );
        this.enviadosList = d;
      },
      error:( d )=>{
        this.enviadosList= [];
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  regresar() {
    this.ref.close();
  }

  formatearFecha( date: string ): string {
    return this.validateService.reFormateaFechaConHoras( date );
  }

}
