import { Component, OnInit } from '@angular/core';

import { DialogService } from 'primeng/dynamicdialog';

import { SolicitarFirmaConstanciaComponent } from '../solicitar-firma-constancia/solicitar-firma-constancia.component';
import { EnviarCorreoComponent } from '../enviar-correo/enviar-correo.component';
import { SustentosConstanciaComponent } from '../sustentos-constancia/sustentos-constancia.component';
import { FirmaHistorialComponent } from '../firma-historial/firma-historial.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConstanciaAbono, ConstanciaAbonoFiltro } from '../../../../../models/mantenimientos/documentos/constancia-abono.model';
import { Observable, map, from, of } from 'rxjs';
import { environment } from '../../../../../../environments/environment';
import { GeneralService } from '../../../../../services/general.service';
import { ConstanciaAbonoService } from '../../../../../services/mantenimientos/documentos/constancia-abono.service';
import { SortEvent, MessageService } from 'primeng/api';
import Swal from 'sweetalert2';
import { ConstanciaAbonoBandeja } from 'src/app/models/mantenimientos/documentos/constancia-abono-bandeja.model';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { ResponsePdf } from 'src/app/models/mantenimientos/documentos/response-pdf.model';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { UtilService } from '../../../../../services/util.service';


@Component({
  selector: 'app-bandeja-constancia',
  templateUrl: './bandeja-constancia.component.html',
  styleUrls: ['./bandeja-constancia.component.scss'],
  providers: [
    DialogService,
    MessageService
  ]
})
export class BandejaConstanciaComponent extends BaseBandeja implements OnInit {
  searchConstancia!: FormGroup;
  $userCode:any
  $zonas!: Observable<any>;
  $oficinas: Observable<any> = of([ { coOficRegi  : '*', deOficRegi: '(TODOS)' }]);
  $estadoConstancia!: Observable<any>;
  enviadosList!: ConstanciaAbono[];
  override selectedValues!: ConstanciaAbono[];
  currentPage: string = environment.currentPage;
  constanciaAbonoBandeja!: ConstanciaAbonoBandeja;
  responsePdf!: ResponsePdf;
  
  guidDocumento: string = '';

  constructor(
    private fb: FormBuilder, 
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private utilService: UtilService,
    private validateService: ValidatorsService,
    private constanciaService: ConstanciaAbonoService 
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Constancia_de_Abono;
    this.btnConstanciaDeAbono();
    
    const zonasItem: any = { coZonaRegi : '*', deZonaRegi: '(TODOS)' };
    const estadoConstanciaItem: any = { ccodigoHijo  : '*', cdescri: '(TODOS)' };
    this.$userCode = localStorage.getItem('user_code')||'';
    this.$zonas = this.generalService.getCbo_Zonas_Usuario(this.$userCode).pipe(
      map( d => {
        console.log(d);
        if (d.length === 1) {
          this.searchConstancia.get('zona')?.setValue(d[0].coZonaRegi); 
          this.buscarOficina();
        }
        d.unshift( zonasItem );
        return d;
      })
    );
   
    this.$estadoConstancia = this.generalService.getCbo_EstadoConstancia().pipe(
      map( d => {
        d.unshift( estadoConstanciaItem );
        return d;
      })
    );
    this.searchConstancia = this.fb.group({
      zona: ['', []],
      oficina: ['', []],
      fecDesde: ['', []],
      fecHasta: ['', []],
      estado: ['', []],
      nroConst: ['', []],
      usuario: ['', []]
    });
    const hoy = new Date();
    const cDateD = new Date(hoy.getFullYear(), hoy.getMonth(), 1);
    this.searchConstancia.get('fecDesde')?.setValue( cDateD );
    const cDateH = new Date(hoy.getFullYear(), hoy.getMonth(), hoy.getDate());
    this.searchConstancia.get('fecHasta')?.setValue( cDateH );
    setTimeout(()=>{
     this.buscarConstancia();
    },1000 )
  }

  buscarConstancia() {
    const fechaHasta = this.searchConstancia.get('fecHasta')?.value;
    const fechaDesde = this.searchConstancia.get('fecDesde')?.value;
    if (!fechaDesde && fechaHasta) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha DESDE');  
      return;     
    }
    if (!fechaHasta && fechaDesde) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha HASTA');   
      return;     
    }

    const fechaDesdeFormat:string = (fechaHasta !== "" && fechaHasta !== null) ? this.validateService.formatDate(fechaDesde) : '';
    const fechaHastaFormat:string = (fechaDesde !== "" && fechaDesde !== null) ? this.validateService.formatDate(fechaHasta) : '';
    const coZonaRegi= this.searchConstancia.get('zona')?.value;  
    const coOficRegi= this.searchConstancia.get('oficina')?.value;
    const esDocu= this.searchConstancia.get('estado')?.value;
    const data = {
      coZonaRegi : coZonaRegi === '*'? '': coZonaRegi ,
      coOficRegi : coOficRegi === '*'? '': coOficRegi,
      nuConsAbon: this.searchConstancia.get('nroConst')?.value,
      feDesd: fechaDesdeFormat,
      feHast: fechaHastaFormat,
      esDocu : esDocu === '*'? '': esDocu,
      noUsuaCrea: this.searchConstancia.get('usuario')?.value,
    }
    Swal.showLoading();

 
    
    this.constanciaService.getBandejaConstanciaAbono(data).subscribe({
      next: ( d ) => {
        this.constanciaAbonoBandeja = d;
        this.enviadosList = d.lstConstanciaAbono ? d.lstConstanciaAbono : [];
      },
      error:( d )=>{
        this.enviadosList= [];
        if (d.error.category === "NOT_FOUND") {
            Swal.fire( 'Constancia de Abono', `${d.error.description}`, 'info');
        }else {
           Swal.fire( 'Constancia de Abono', `Ocurrio un error.`, 'error');
        }
      },
      complete:()=>{
        Swal.close();
      }
    });
  }

  buscarOficina() {
    const oficinasItem: any = { coOficRegi  : '*', deOficRegi: '(TODOS)' };
    const zonaValor = this.searchConstancia.get('zona')?.value; 
    if (zonaValor === '*') {
      this.$oficinas = of([oficinasItem]);
    }else {
      this.$oficinas = this.generalService.getCbo_Oficinas_Zonas(this.$userCode,zonaValor).pipe(
        map( d => {
          if (d.length === 1) {
            this.searchConstancia.get('oficina')?.setValue(d[0].coOficRegi); 
          }
          d.unshift( oficinasItem );
          return d;
        })
      );
    }
  }
  
  
  verDocumento() {
	  console.log('verDocumento...');
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Constancia de Abono', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Constancia de Abono', 'Solo puede visualizar un registro a la vez!', 'error');
      return;
    }

	  const constancia  = this.selectedValues;
    
    if ( constancia[0].esDocu == '001' ) {
      this.constanciaService.getConstanciaAbonoPdfById( constancia['0'].idConsAbon! ).subscribe({
        next: ( data ) => {        
          this.guidDocumento = data.msgResult;
          console.log('Exportar PDF: ' + this.guidDocumento);
  
          const doc = data.archivoPdf;
          const byteCharacters = atob(doc!);
          const byteNumbers = new Array(byteCharacters.length);
          for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
          }
          const byteArray = new Uint8Array(byteNumbers);
          const blob = new Blob([byteArray], { type: 'application/pdf' });
          const blobUrl = URL.createObjectURL(blob);
          const download = document.createElement('a');
          download.href =  blobUrl;
          //download.setAttribute('download', 'Constacia_Abono');
          download.setAttribute("target", "_blank");
          document.body.appendChild( download );
          download.click();
  
        },
        error: ( e ) => {
          Swal.fire( 'Atención', e.error.description, 'warning');
        }
      });
    }
    else {
      const blobUrl = this.generalService.downloadManager( constancia[0].idGuidDocu );
      const download = document.createElement('a');
      download.href =  blobUrl;
      //download.setAttribute('download', 'Constacia_Abono');
      download.setAttribute("target", "_blank");
      document.body.appendChild( download );
      download.click();
    }
	
  }


  firma() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Constancia de Abono', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Constancia de Abono', 'Solo puede visualizar un registro a la vez!', 'error');
      this.selectedValues = [];
      return;
    }

    if ( this.selectedValues[0].esDocu != '001' ) {
      Swal.fire('Constancia de Abono', 'Solo puede solicitar firma a Constancias de Abono en estado REGISTRADO', 'error')
      this.selectedValues = [];
      return;
    }

	const constancia  = this.selectedValues;
    this.constanciaService.getConstanciaAbonoPdfById( constancia['0'].idConsAbon! ).subscribe({
      next: ( data ) => {
        
        this.guidDocumento = data.msgResult;
        console.log('Exportar PDF: ' + this.guidDocumento);
            
        const dialog = this.dialogService.open( SolicitarFirmaConstanciaComponent, {
          header: 'Constancia de Abono - Solicitar Firma',
          width: '45%',
          closable: false,
          data: {
            csta: this.selectedValues,
            guidDocumento: data.msgResult,
            doc: data.archivoPdf
          }
        });

        dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
          if ( type == 'accept' ) {
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se solicitó la firma del documento satisfactoriamente.',
              life: 5000
            });
    
            this.buscarConstancia();
            this.selectedValues = [];
          }
          else if ( type == 'reject' ) {
            this.messageService.add({
              severity: 'error',
              summary: 'Ha ocurrido un error al solicitar la firma del documento.',
              life: 5000
            });
    
            this.selectedValues = [];
          }
    
          this.selectedValues = [];
        });
      },
      error: ( e ) => {
        console.log( e );
      }
    });
  }


  enviarCorreo (){
    console.log( this.selectedValues )
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Constancia de Abono', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Constancia de Abono', 'Solo puede visualizar un registro a la vez!', 'error');
      return;
    }

    if ( this.selectedValues['0'].esDocu == '003' || this.selectedValues['0'].esDocu == '004' ) {
      const dialog = this.dialogService.open( EnviarCorreoComponent, {
        header: 'Enviar Correo',
        width: '45%',
        data: {
          csta: this.selectedValues
        }
      });
      dialog.onClose.subscribe( () => {
        this.selectedValues = [];
        this.buscarConstancia();
      }); 
    }
    else {
      Swal.fire('Constancia de Abono', 'Solo se puede enviar correos de Constancia de Abono en estado FIRMADO o RECHAZADO', 'error')
      this.selectedValues = [];
      return;
    } 
  }

  historialFirmas() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Constancia de Abono', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Constancia de Abono', 'Solo puede visualizar un registro a la vez!', 'error');
      return;
    }

    if ( this.selectedValues['0'].esDocu == '001' || this.selectedValues['0'].esDocu == '005' ) {
      Swal.fire('Constancia de Abono', 'No puede ver el historial de un registro en estado REGISTRADO o ANULADO', 'error')
      this.selectedValues = [];
      return;
    }

    const dialog = this.dialogService.open( FirmaHistorialComponent, {
      header: 'Historial de Firmas',
      width: '50%',
      data: {
        csta: this.selectedValues
      }
    });

    dialog.onClose.subscribe( () => {
      this.selectedValues = [];
    });
  }

  sustentos() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Constancia de Abono', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Constancia de Abono', 'Solo puede visualizar un registro a la vez!', 'error');
      return;
    }

    const dialog = this.dialogService.open( SustentosConstanciaComponent, {
      header: 'Sustentos',
      width: '45%',
      closable: false,
      data: {
        csta: this.selectedValues,
        add: this.BtnAdicionar_Sustento,
        delete: this.BtnEliminar_Sustento,
        bandeja: this.codBandeja
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Los sustentos han sido cargados y grabados correctamente',
          life: 5000
        });

        this.buscarConstancia();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al cargar y grabar los sustentos',
          life: 5000
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  exportExcel() {
    console.log('Exportar Excel');

    const fechaHoy = new Date()
    const fileName = this.validateService.formatDateWithHoursForExcel( fechaHoy );

    const doc = this.constanciaAbonoBandeja.reporteExcel;
    const byteCharacters = atob(doc!);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', `ConstanciaAbono_${ fileName }`);
    document.body.appendChild( download );
    download.click();
  }

  descargarPdf() {
    console.log("descargarPdf");
    this.constanciaService.getConstanciaAbonoPdf(this.selectedValues[0].idConsAbon).subscribe({
      next: ( data ) => {
        this.responsePdf = data;

        const doc = this.responsePdf.archivoPdf;
        const byteCharacters = atob(doc!);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const blob = new Blob([byteArray], { type: 'application/pdf' });
        const blobUrl = URL.createObjectURL(blob);
        const download = document.createElement('a');
        download.href =  blobUrl;
        download.setAttribute('download', 'Carta_Orden');
        document.body.appendChild( download );
        download.click();
      },
      error:( d )=>{
        if (d.error.category === "NOT_FOUND") {
          Swal.fire( 'Carta orden', `${d.error.description}`, 'info');
          
        }else {
          Swal.fire( 'Carta orden', `Ocurrio un error.`, 'error');
        }
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }
}
