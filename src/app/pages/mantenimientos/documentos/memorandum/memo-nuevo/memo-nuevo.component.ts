import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Dropdown } from 'primeng/dropdown';
import { forkJoin, Subject } from 'rxjs';
import { first, switchMap, takeUntil } from 'rxjs/operators';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse } from 'src/app/interfaces/general.interface';
import { ICampoMemorandum, IDataFiltrosMemorandum, IMemorandum, IMemorandumProcess } from 'src/app/interfaces/memorandum.interface';
import { TipoMemorandum } from 'src/app/models/mantenimientos/configuracion/tipo-memo.model';
import { BodyNuevoMemorandum } from 'src/app/models/mantenimientos/documentos/memorandum.model';
import { GeneralService } from 'src/app/services/general.service';
import { TipoMemorandumService } from 'src/app/services/mantenimientos/configuracion/tipo-memorandum.service';
import { MemorandumService } from 'src/app/services/mantenimientos/documentos/memorandum.service';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { UtilService } from 'src/app/services/util.service';
import { OPERACION } from 'src/app/models/enum/parameters';

@Component({
  selector: 'app-memo-nuevo',
  templateUrl: './memo-nuevo.component.html',
  styleUrls: ['./memo-nuevo.component.scss']
})
export class MemoNuevoComponent implements OnInit, OnDestroy {  

  formMemorandum: FormGroup;
  unsubscribe$ = new Subject<void>();
  titulo: string = "";
  userCode: any; 
  listaZonaRegistral: ZonaRegistral[];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoMemorandum: TipoMemorandum[];
  listaBancos: any[];
  listaCuentasOrigen: any[];
  listaCuentasDestino: any[];
  fechaMinima: Date;
  fechaMaxima: Date;
  memorandum: BodyNuevoMemorandum;
  memorandumEditar: IMemorandum;
  proceso: string;
  soloLecturaMontoTotal: boolean = true;
  tipoMemorandum: TipoMemorandum;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  loadingCuenta: boolean = false;
  loadingCuentaOrigen: boolean = false;  
  loadingCuentaDestino: boolean = false;
  memorandumProcess: IMemorandumProcess;
  operacion = OPERACION;
 
  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;
  @ViewChild('ddlTipoMemorandum') ddlTipoMemorandum: Dropdown;
  @ViewChild('ddlBancoOrigen') ddlBancoOrigen: Dropdown;
  @ViewChild('ddlCuentaBancoOrigen') ddlCuentaBancoOrigen: Dropdown;  
  @ViewChild('ddlBancoDestino') ddlBancoDestino: Dropdown;
  @ViewChild('ddlCuentaBancoDestino') ddlCuentaBancoDestino: Dropdown;

  constructor( 
    private formBuilder: FormBuilder,
    private router: Router,
    private generalService: GeneralService,
    private tipoMemorandumService: TipoMemorandumService,
    private memorandumService: MemorandumService,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService,
    private funciones: Funciones 
  ) {            
  } 

  async ngOnInit() {  
    this.fechaMaxima = new Date();      
    this.construirFormulario();    
    this.memorandumProcess = await this.sharingInformationService.compartirMemorandumProcesobservable.pipe(first()).toPromise();
    if (this.memorandumProcess.proceso === 'nuevo') {
      this.titulo = 'Memorandum - Nuevo';
      this.memorandumEditar = null;
      this.proceso = 'nuevo';
      this.inicilializarListasNuevoRegistro();
      this.obtenerListadosNuevoRegistro();
    } else {      
      if (this.memorandumProcess.proceso === 'editar') {
        this.titulo = 'Memorandum - Editar';         
      } else {
        this.titulo = 'Memorandum - Consultar';        
      }   
      this.proceso = this.memorandumProcess.proceso;   
      this.obtenerListadosEditarRegistro();
    }     
  }

  private inicilializarListasNuevoRegistro() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoMemorandum = [];    
    this.listaBancos = []; 
    this.listaCuentasOrigen = []; 
    this.listaCuentasDestino = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    this.listaTipoMemorandum.push({ idTipoMemo: 0, noTipoMemo: '(SELECCIONAR)' });    
    this.listaBancos.push({ nidInstitucion: '*', cdeInst: '(NINGUNO)' });
    this.listaCuentasOrigen.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });    
    this.listaCuentasDestino.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
  }

  private construirFormulario() {
    this.formMemorandum = this.formBuilder.group({
      zona: [''],
      oficina: [''],
      local: [''],
      tipoMemorandum: [''], 
      numeroMemorandum: [''],
      nroSistran: [''],
      fechaMemorandum: [this.fechaMaxima],
      camposMemorandum: this.formBuilder.array([]),   
      montoTotal: [''],
      fechaDesde: [''],
      fechaHasta: [''],
      destinatario: [''],
      cargo: [''],
      asunto: [''],
      referencia: [''],
      bancoOrigen: [''], 
      cuentaBancoOrigen: [''],
      nombreCuentaBancoOrigen: [''],  
      bancoDestino: [''], 
      cuentaBancoDestino: [''],
      nombreCuentaBancoDestino: [''],          
    });
  }

  get camposMemorandum() {
    return this.formMemorandum.controls["camposMemorandum"] as FormArray;
  }

  private obtenerListadosNuevoRegistro() {    
    this.utilService.onShowProcessLoading("Cargando la data"); 
    const tipoMemorandum = this.formMemorandum.get('tipoMemorandum')?.value;
    forkJoin(           
      this.generalService.getCbo_Zonas_Usuario( this.userCode),
      this.tipoMemorandumService.getBandejaTipoMemorandum( tipoMemorandum, 'A'),      
      this.generalService.getCbo_Bancos()       
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resZonaRegistral, resTipoMemorandum, resBancosCargo]) => {
      if (resZonaRegistral.length === 1) {   
        this.listaZonaRegistral.push({ coZonaRegi: resZonaRegistral[0].coZonaRegi, deZonaRegi: resZonaRegistral[0].deZonaRegi });    
        this.formMemorandum.patchValue({ "zona": resZonaRegistral[0].coZonaRegi});
        this.buscarOficinaRegistral();
      } else {    
        this.listaZonaRegistral.push(...resZonaRegistral);
      }               
      this.listaTipoMemorandum.push(...this.ordenarListaTipoMemorandum(resTipoMemorandum));
      this.listaBancos.push(...resBancosCargo);
      this.utilService.onCloseLoading();    
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }

  private ordenarListaTipoMemorandum(lista: TipoMemorandum[]): TipoMemorandum[] {
    let listaFinal: TipoMemorandum[] = [];
    listaFinal =  lista.sort( (a, b) => {       
      if (a.noTipoMemo.toLowerCase() < b.noTipoMemo.toLowerCase()) {
        return -1;
      }
      if (a.noTipoMemo.toLowerCase() > b.noTipoMemo.toLowerCase()) {
        return 1;
      }
      return 0;
    });
    return listaFinal;
  }
  
  private obtenerListadosEditarRegistro() { 
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.memorandumService.getBandejaMemorandumById(this.memorandumProcess.idMemo).pipe(
      takeUntil(this.unsubscribe$),
      switchMap( (res: IMemorandum) => {
        this.memorandumEditar = res;
        return forkJoin(                  
          this.tipoMemorandumService.getTipoMemorandumById(res.idTipoMemo),
          this.generalService.getCbo_Bancos(),
          this.generalService.getCbo_Cuentas(this.memorandumEditar.idBancOrig), 
          this.generalService.getCbo_Cuentas(this.memorandumEditar.idBancDest)    
        )
      })
    ).subscribe( ([resTipoMemoranduml, resBancosCargo, resCuentaOrigen, resCuentaDestino]) => {                                    
      this.tipoMemorandum = resTipoMemoranduml;
      this.setearCampos(); 
      if (this.memorandumProcess.proceso === 'consultar') {
        this.deshabilitarFormulario();
      }      
      this.listaBancos.push(...resBancosCargo);
      this.listaCuentasOrigen.push(...resCuentaOrigen);
      this.listaCuentasDestino.push(...resCuentaDestino);
      this.utilService.onCloseLoading(); 
    }, (err: HttpErrorResponse) => {
      this.utilService.onCloseLoading();                        
    })
  }

  deshabilitarFormulario() {
    this.formMemorandum.disable();
    for (let i = 0; i < this.camposMemorandum.controls.length; i++) {
      this.camposMemorandum.controls[i].disable();         
    }
  }
  
  private setearCampos() {
    this.formMemorandum.get('zona').disable();
    this.formMemorandum.get('oficina').disable();
    this.formMemorandum.get('local').disable();
    this.formMemorandum.get('tipoMemorandum').disable();
    this.formMemorandum.get('numeroMemorandum').disable();
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoMemorandum = [];    
    this.listaBancos = [];    
    this.listaCuentasOrigen = []; 
    this.listaCuentasDestino = [];
    this.listaZonaRegistral.push({ coZonaRegi: this.memorandumEditar.coZonaRegi, deZonaRegi: this.memorandumEditar.deZonaRegi });
    this.listaOficinaRegistral.push({ coOficRegi: this.memorandumEditar.coOficRegi, deOficRegi: this.memorandumEditar.deOficRegi });
    this.listaLocal.push({ coLocaAten: this.memorandumEditar.coLocaAten, deLocaAten: this.memorandumEditar.deLocaAten }); 
    this.listaTipoMemorandum.push({ idTipoMemo: this.memorandumEditar.idTipoMemo, noTipoMemo: this.memorandumEditar.noTipoMemo }); 
    this.listaBancos.push({ nidInstitucion: '*', cdeInst: '(NINGUNO)' });
    this.listaCuentasOrigen.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });    
    this.listaCuentasDestino.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
    this.formMemorandum.patchValue({ "numeroMemorandum": this.memorandumEditar.nuMemo});
    this.formMemorandum.patchValue({ "nroSistran": this.memorandumEditar.nuSistran});
    this.formMemorandum.patchValue({ "fechaMemorandum": new Date(this.memorandumEditar.feMemo)});
    this.seleccionarMemorandumEditar();  
    this.formMemorandum.patchValue({ "montoTotal": this.memorandumEditar.imTotal.toFixed(2)});
    this.formMemorandum.patchValue({ "fechaDesde": new Date(this.memorandumEditar.feDesdMemo)});
    this.formMemorandum.patchValue({ "fechaHasta": new Date(this.memorandumEditar.feHastMemo)});
    this.formMemorandum.patchValue({ "destinatario": this.memorandumEditar.noDest});
    this.formMemorandum.patchValue({ "cargo": this.memorandumEditar.noCarg});
    this.formMemorandum.patchValue({ "asunto": this.memorandumEditar.noAsun});
    this.formMemorandum.patchValue({ "referencia": this.memorandumEditar.noRefe});
    this.formMemorandum.patchValue({ "bancoOrigen": this.memorandumEditar.idBancOrig});
    this.formMemorandum.patchValue({ "cuentaBancoOrigen": this.memorandumEditar.nuCuenBancOrig});
    this.formMemorandum.patchValue({ "nombreCuentaBancoOrigen": this.memorandumEditar.noCuenBancOrig});
    this.formMemorandum.patchValue({ "bancoDestino": this.memorandumEditar.idBancDest});
    this.formMemorandum.patchValue({ "cuentaBancoDestino": this.memorandumEditar.nuCuenBancDest});
    this.formMemorandum.patchValue({ "nombreCuentaBancoDestino": this.memorandumEditar.noCuenBancDest});
  }

  public buscarOficinaRegistral() {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formMemorandum.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.formMemorandum.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {      
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.formMemorandum.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }        
        this.loadingOficinaRegistral = false;
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    }
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.formMemorandum.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.formMemorandum.get('zona')?.value,this.formMemorandum.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {           
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formMemorandum.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        }        
        this.loadingLocal = false;      
      }, err => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }   

  public seleccionarTipoMemorandum() {  
    const tipoMemo: TipoMemorandum = this.listaTipoMemorandum.find( (item: TipoMemorandum) => 
      item.idTipoMemo === this.formMemorandum.get('tipoMemorandum')?.value );    
      
    if (tipoMemo.idTipoMemo !== 0) {   
      let lista: ICampoMemorandum[] = this.obtenerCampos(tipoMemo);  
      this.soloLecturaMontoTotal = (lista.length > 0) ? true : false;
      this.addFormsCampos(lista);
      if (this.memorandumProcess.proceso === 'nuevo') {
        this.setearCamposPorDefectoPorTipoDocumento(tipoMemo);
      }     
    } else {
      this.camposMemorandum.clear(); 
      if (this.memorandumProcess.proceso === 'nuevo') {
        this.resetearCamposPorDefectoPorTipoDocumento();
      }               
    } 
    this.formMemorandum.get('montoTotal').reset();    
  }  

  public seleccionarMemorandumEditar() {  
    let lista: ICampoMemorandum[] = this.obtenerCampos(this.tipoMemorandum)           
    this.soloLecturaMontoTotal = (lista.length > 0) ? true : false;
    this.addFormsCampos(lista);      
    if (this.camposMemorandum.controls.length > 0) {
      if (this.camposMemorandum.controls.length === 1) {          
        this.camposMemorandum.controls[0].patchValue({ "valor": this.memorandumEditar.imMemo0001.toFixed(2)});      
      } else if (this.camposMemorandum.controls.length === 2) {
        this.camposMemorandum.controls[0].patchValue({ "valor": this.memorandumEditar.imMemo0001.toFixed(2)}); 
        this.camposMemorandum.controls[1].patchValue({ "valor": this.memorandumEditar.imMemo0002.toFixed(2)});         
      } else if (this.camposMemorandum.controls.length === 3) {
        this.camposMemorandum.controls[0].patchValue({ "valor": this.memorandumEditar.imMemo0001.toFixed(2)}); 
        this.camposMemorandum.controls[1].patchValue({ "valor": this.memorandumEditar.imMemo0002.toFixed(2)});
        this.camposMemorandum.controls[2].patchValue({ "valor": this.memorandumEditar.imMemo0003.toFixed(2)});
      } else if (this.camposMemorandum.controls.length === 4) {
        this.camposMemorandum.controls[0].patchValue({ "valor": this.memorandumEditar.imMemo0001.toFixed(2)}); 
        this.camposMemorandum.controls[1].patchValue({ "valor": this.memorandumEditar.imMemo0002.toFixed(2)});
        this.camposMemorandum.controls[2].patchValue({ "valor": this.memorandumEditar.imMemo0003.toFixed(2)});
        this.camposMemorandum.controls[3].patchValue({ "valor": this.memorandumEditar.imMemo0004.toFixed(2)});
      } else if (this.camposMemorandum.controls.length === 5) {
        this.camposMemorandum.controls[0].patchValue({ "valor": this.memorandumEditar.imMemo0001.toFixed(2)}); 
        this.camposMemorandum.controls[1].patchValue({ "valor": this.memorandumEditar.imMemo0002.toFixed(2)});
        this.camposMemorandum.controls[2].patchValue({ "valor": this.memorandumEditar.imMemo0003.toFixed(2)});
        this.camposMemorandum.controls[3].patchValue({ "valor": this.memorandumEditar.imMemo0004.toFixed(2)});
        this.camposMemorandum.controls[4].patchValue({ "valor": this.memorandumEditar.imMemo0005.toFixed(2)});
      } else if (this.camposMemorandum.controls.length === 6) {
        this.camposMemorandum.controls[0].patchValue({ "valor": this.memorandumEditar.imMemo0001.toFixed(2)}); 
        this.camposMemorandum.controls[1].patchValue({ "valor": this.memorandumEditar.imMemo0002.toFixed(2)});
        this.camposMemorandum.controls[2].patchValue({ "valor": this.memorandumEditar.imMemo0003.toFixed(2)});
        this.camposMemorandum.controls[3].patchValue({ "valor": this.memorandumEditar.imMemo0004.toFixed(2)});
        this.camposMemorandum.controls[4].patchValue({ "valor": this.memorandumEditar.imMemo0005.toFixed(2)});
        this.camposMemorandum.controls[5].patchValue({ "valor": this.memorandumEditar.imMemo0006.toFixed(2)});
      }
    }
  }

  private obtenerCampos(tipoMemorandum: TipoMemorandum): ICampoMemorandum[] {
    let campos: ICampoMemorandum[] = [];
    if (tipoMemorandum.noImpo0001 !== "" && tipoMemorandum.noImpo0001 !== " " && tipoMemorandum.noImpo0001 !== null) {       
      campos.push(
        {
          "titulo": tipoMemorandum.noImpo0001, 
          "accion": tipoMemorandum.tiOper0001
        }
      );
    } 
    if (tipoMemorandum.noImpo0002 !== "" && tipoMemorandum.noImpo0002 !== " " && tipoMemorandum.noImpo0002 !== null) {
      campos.push(     
        {
          "titulo": tipoMemorandum.noImpo0002, 
          "accion": tipoMemorandum.tiOper0002
        }
      );
    } 
    if (tipoMemorandum.noImpo0003 !== "" && tipoMemorandum.noImpo0003 !== " " && tipoMemorandum.noImpo0003 !== null) {
      campos.push(     
        {
          "titulo": tipoMemorandum.noImpo0003, 
          "accion": tipoMemorandum.tiOper0003
        }
      );
    } 
    if (tipoMemorandum.noImpo0004 !== "" && tipoMemorandum.noImpo0004 !== " " && tipoMemorandum.noImpo0004 !== null) {
      campos.push(     
        {
          "titulo": tipoMemorandum.noImpo0004, 
          "accion": tipoMemorandum.tiOper0004
        }
      );
    } 
    if (tipoMemorandum.noImpo0005 !== "" && tipoMemorandum.noImpo0005 !== " " && tipoMemorandum.noImpo0005 !== null) {
      campos.push(     
        {
          "titulo": tipoMemorandum.noImpo0005, 
          "accion": tipoMemorandum.tiOper0005
        }
      );
    } 
    if (tipoMemorandum.noImpo0006 !== "" && tipoMemorandum.noImpo0006 !== " " && tipoMemorandum.noImpo0006 !== null) {
      campos.push(     
        {
          "titulo": tipoMemorandum.noImpo0006, 
          "accion": tipoMemorandum.tiOper0006
        }
      );
    }
    return campos;
  }

  private addFormsCampos(listaCampos: ICampoMemorandum[]) {      
    this.camposMemorandum.clear();
    for (let i = 0; i < listaCampos.length; i++) {
      const campoForm = this.formBuilder.group({
        id: [`campoForm-${i+1}`],       
        titulo: [listaCampos[i].titulo], 
        valor: [''], 
        accion: [listaCampos[i].accion]               
      });
      this.camposMemorandum.push(campoForm);
    }
  }

  private setearCamposPorDefectoPorTipoDocumento(tipoMemo: TipoMemorandum) {    
    this.formMemorandum.patchValue({ "destinatario": tipoMemo.noDest});
    this.formMemorandum.patchValue({ "cargo": tipoMemo.noCarg});
    this.formMemorandum.patchValue({ "asunto": tipoMemo.noAsun});
    this.formMemorandum.patchValue({ "referencia": tipoMemo.noRefe});
    this.formMemorandum.patchValue({ "bancoOrigen": tipoMemo.idBancOrig});
    this.seleccionarBancoOrigen2(tipoMemo.nuCuenBancOrig);
    this.formMemorandum.patchValue({ "nombreCuentaBancoOrigen": tipoMemo.noCuenBancOrig});
    this.formMemorandum.patchValue({ "bancoDestino": tipoMemo.idBancDest});  
    this.seleccionarBancoDestino2(tipoMemo.nuCuenBancDest);
    this.formMemorandum.patchValue({ "nombreCuentaBancoDestino": tipoMemo.noCuenBancDest});
  }
  
  private resetearCamposPorDefectoPorTipoDocumento() {    
    this.formMemorandum.get('destinatario').reset();
    this.formMemorandum.get('cargo').reset();
    this.formMemorandum.get('asunto').reset();
    this.formMemorandum.get('referencia').reset();
    this.formMemorandum.patchValue({ "bancoOrigen": '*'});
    this.formMemorandum.patchValue({ "cuentaBancoOrigen": '*'});    
    this.formMemorandum.get('nombreCuentaBancoOrigen').reset();
    this.formMemorandum.patchValue({ "bancoDestino": '*'});
    this.formMemorandum.patchValue({ "cuentaBancoDestino": '*'});
    this.formMemorandum.get('nombreCuentaBancoDestino').reset();  
  }

  public formatearCamposTipoMemorandum(event: any, i: number) {  
    if (event.target.value) {
      this.camposMemorandum.controls[i].patchValue({ "valor": Number.parseFloat(event.target.value).toFixed(2)});      
    }

    let valor: number = 0;
    for (let i = 0; i < this.camposMemorandum.controls.length; i++) { 
      if (this.camposMemorandum.controls[i].value.valor !== "") {
        let cant = parseFloat(this.camposMemorandum.controls[i].value.valor); 
        if (this.camposMemorandum.controls[i].value.accion === this.operacion.ES_SUMABLE) {
          valor = valor + cant;
        } else if (this.camposMemorandum.controls[i].value.accion === this.operacion.ES_RESTABLE) {
          valor = valor - cant;
        }       
      }      
    }
    this.formMemorandum.patchValue({ "montoTotal": valor});
    this.formMemorandum.get('montoTotal')?.setValue(valor.toFixed(2));
  }

  public formatearMontoTotal(event:any){
    if (event.target.value) {
      this.formMemorandum.get('montoTotal')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
    }
  }

  public cambioFechaInicio() {   
    if (this.formMemorandum.get('fechaHasta')?.value !== '') {
      if (this.formMemorandum.get('fechaDesde')?.value > this.formMemorandum.get('fechaHasta')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.formMemorandum.controls['fecDes'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.formMemorandum.get('fechaDesde')?.value !== '') {
      if (this.formMemorandum.get('fechaHasta')?.value < this.formMemorandum.get('fechaDesde')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.formMemorandum.controls['fechaHasta'].reset();    
      }      
    }
  }
  
  public seleccionarBancoOrigen() {  
    this.loadingCuentaOrigen = true;
    this.listaCuentasOrigen = [];
    this.listaCuentasOrigen.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
    this.formMemorandum.get('nombreCuentaBancoOrigen').reset();
    if (this.formMemorandum.get('bancoOrigen')?.value !== "*") {
      this.generalService.getCbo_Cuentas(this.formMemorandum.get('bancoOrigen')?.value).pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: any[]) => {             
        this.listaCuentasOrigen.push(...data);
        this.loadingCuentaOrigen = false;            
      }, (err: HttpErrorResponse) => {       
        this.loadingCuentaOrigen = false;                                 
      });      
    } else {
      this.loadingCuentaOrigen = false;
    }    
  }

  public seleccionarBancoOrigen2(nuCuenBancOrig: string) {    
    this.loadingCuentaOrigen = true;
    this.listaCuentasOrigen = [];
    this.listaCuentasOrigen.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
    this.formMemorandum.get('nombreCuentaBancoOrigen').reset();
    if (this.formMemorandum.get('bancoOrigen')?.value !== "*") {
      this.generalService.getCbo_Cuentas(this.formMemorandum.get('bancoOrigen')?.value).pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: any[]) => {             
        this.listaCuentasOrigen.push(...data);
        this.formMemorandum.patchValue({ "cuentaBancoOrigen": nuCuenBancOrig});          
        this.loadingCuentaOrigen = false;            
      }, (err: HttpErrorResponse) => {       
        this.loadingCuentaOrigen = false;                                 
      });      
    } else {
      this.loadingCuentaOrigen = false;
    }    
  }

  public seleccionarCuentaBancariaOrigen() { 
    const resultado = this.listaCuentasOrigen.find( (item: any) => item.nuCuenBanc === this.formMemorandum.get('cuentaBancoOrigen')?.value );
    this.formMemorandum.patchValue({ "nombreCuentaBancoOrigen": resultado.noCuenBanc});        
  }

  public seleccionarBancoDestino() {
    this.loadingCuentaDestino = true;   
    this.listaCuentasDestino = [];
    this.listaCuentasDestino.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
    this.formMemorandum.get('nombreCuentaBancoDestino').reset();
    if (this.formMemorandum.get('bancoDestino')?.value !== "*") {
      this.generalService.getCbo_Cuentas(this.formMemorandum.get('bancoDestino')?.value).pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: any[]) => {      
        this.listaCuentasDestino.push(...data);   
        this.loadingCuentaDestino = false;           
      }, (err: HttpErrorResponse) => {     
        this.loadingCuentaDestino = false;                                  
      });
    } else {
      this.loadingCuentaDestino = false;
    }    
  }

  public seleccionarBancoDestino2(nuCuenBancDest: string) {
    this.loadingCuentaDestino = true;   
    this.listaCuentasDestino = [];
    this.listaCuentasDestino.push({ idBanc: '*', nuCuenBanc: '(NINGUNO)' });
    this.formMemorandum.get('nombreCuentaBancoDestino').reset();
    if (this.formMemorandum.get('bancoDestino')?.value !== "*") {
      this.generalService.getCbo_Cuentas(this.formMemorandum.get('bancoDestino')?.value).pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: any[]) => {      
        this.listaCuentasDestino.push(...data); 
        this.formMemorandum.patchValue({ "cuentaBancoDestino": nuCuenBancDest});   
        this.loadingCuentaDestino = false;           
      }, (err: HttpErrorResponse) => {     
        this.loadingCuentaDestino = false;                                  
      });
    } else {
      this.loadingCuentaDestino = false;
    }    
  }

  public seleccionarCuentaBancariaDestino() {   
    const resultado = this.listaCuentasDestino.find( (item: any) => item.nuCuenBanc === this.formMemorandum.get('cuentaBancoDestino')?.value );
    this.formMemorandum.patchValue({ "nombreCuentaBancoDestino": resultado.noCuenBanc});        
  }

  public async grabarMemorandum() {
    let resultado: boolean = await this.validarFormulario();
    if (!resultado) {
      return;
    } 
    this.utilService.onShowProcessSaveEdit(); 
    this.asignarValores(this.formMemorandum.getRawValue());

    if (this.memorandumEditar === null) {
      this.memorandumService.createMemorandum(this.memorandum).pipe(takeUntil(this.unsubscribe$))
      .subscribe( (res: IResponse) => {     
        if (res.codResult < 0 ) {        
          this.utilService.onShowAlert("info", "Atención", res.msgResult);
        } else if (res.codResult === 0) {         
          this.respuestaExitosaGrabarMemorandum(false);
        }         
      }, (err: HttpErrorResponse) => {
        if (err.error.category === "NOT_FOUND") {
          this.utilService.onShowAlert("error", "Atención", err.error.description);        
        } else {
          this.utilService.onShowMessageErrorSystem();        
        }
      })
    } else {
      this.memorandumService.editMemorandum(this.memorandum).pipe(takeUntil(this.unsubscribe$))
      .subscribe( (res: IResponse) => {     
        if (res.codResult < 0 ) {        
          this.utilService.onShowAlert("info", "Atención", res.msgResult);
        } else if (res.codResult === 0) {         
          this.respuestaExitosaGrabarMemorandum(true);
        }         
      }, (err: HttpErrorResponse) => {
        if (err.error.category === "NOT_FOUND") {
          this.utilService.onShowAlert("error", "Atención", err.error.description);        
        } else {
          this.utilService.onShowMessageErrorSystem();        
        }
      })
    }    
  }

  private async respuestaExitosaGrabarMemorandum(esEditar: boolean) {
    const verbo = (this.memorandumEditar === null) ? 'creó' : 'editó';    
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Se ${verbo} satisfactoriamente el memorándum.`);
    if (respuesta.isConfirmed) {
      if (esEditar) {
        let dataFiltrosMemorandum: IDataFiltrosMemorandum = await this.sharingInformationService.compartirDataFiltrosMemorandumObservable.pipe(first()).toPromise(); 
        if (dataFiltrosMemorandum.esBusqueda) {
          dataFiltrosMemorandum.esCancelar = true;
          this.sharingInformationService.compartirDataFiltrosMemorandumObservableData = dataFiltrosMemorandum;
        }
      }      
      this.router.navigate(['SARF/mantenimientos/documentos/memorandum/bandeja']);
    }
  }

   private async validarFormulario(): Promise<boolean> {   
    if (this.formMemorandum.value.zona === "" || this.formMemorandum.value.zona === "*" || this.formMemorandum.value.zona === null) {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una zona registral.");
      return false;      
    } else if (this.formMemorandum.value.oficina === "" || this.formMemorandum.value.oficina === "*" || this.formMemorandum.value.oficina === null) {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una oficina registral.");
      return false;      
    } else if (this.formMemorandum.value.local === "" || this.formMemorandum.value.local === "*" || this.formMemorandum.value.local === null) {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un local.");
      return false;      
    } else if (this.formMemorandum.value.tipoMemorandum === "" || this.formMemorandum.value.tipoMemorandum === "*" || this.formMemorandum.value.tipoMemorandum === null) {      
      this.ddlTipoMemorandum.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un tipo de memorándum.");
      return false;      
    } else if (this.formMemorandum.value.fechaMemorandum === "" || this.formMemorandum.value.fechaMemorandum === null) {                    
      document.getElementById('idFechaMemorandum').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de memorándum.");
      return false;      
    } else if (!this.validarCamposTiposMemorandum()) {
      return false; 
    } else if (this.soloLecturaMontoTotal && (this.formMemorandum.value.montoTotal === "" || this.formMemorandum.value.montoTotal === null)) {
      document.getElementById('idMontoTotal').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese el monto total.");
      return false;       
    } else if (this.soloLecturaMontoTotal && (this.formMemorandum.value.montoTotal === "0" || this.formMemorandum.value.montoTotal === "0.00")) {
      document.getElementById('idMontoTotal').focus();      
      this.utilService.onShowAlert("warning", "Atención", "El monto total tiene que ser mayor de cero.");
      return false;       
    } else if (this.formMemorandum.value.fechaDesde === "" || this.formMemorandum.value.fechaDesde === null) {                    
      let respuesta = await this.utilService.onShowAlertPromise("warning", "Atención", `Seleccione la fecha de inicio.`);
      if (respuesta.isConfirmed) {
        document.getElementById('idFechaDesde').focus();
        return false; 
      } else {
        return true;
      } 
    } else if (this.formMemorandum.value.fechaHasta === "" || this.formMemorandum.value.fechaHasta === null) {                         
      let respuesta = await this.utilService.onShowAlertPromise("warning", "Atención", `Seleccione la fecha de fin.`);
      if (respuesta.isConfirmed) {
        document.getElementById('idFechaHasta').focus();
        return false; 
      } else {
        return true;
      } 
    } else if (this.funciones.eliminarEspaciosEnBlanco(this.formMemorandum.value.destinatario) === "" || this.formMemorandum.value.destinatario === null) {                    
      document.getElementById('idDestinatario').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese el destinatario.");
      return false;      
    } else if (this.funciones.eliminarEspaciosEnBlanco(this.formMemorandum.value.asunto) === "" || this.formMemorandum.value.asunto === null) {                    
      document.getElementById('idAsunto').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese el asunto.");
      return false;      
    } else {
      return true;
    }       
  }

  private validarCamposTiposMemorandum(): boolean {   
    if (this.camposMemorandum.controls.length > 0) {
      for (let i = 0; i < this.camposMemorandum.controls.length; i++) {
        const element = this.camposMemorandum.controls[i];
        if (element.value.valor === "" || element.value.valor === null) {
          document.getElementById(element.value.id).focus();      
          this.utilService.onShowAlert("warning", "Atención", `Ingrese: ${element.value.titulo}`);
          return false;
        }        
      }
    } 
    return true;   
  }

  private asignarValores(registro: any) {
    this.memorandum = new BodyNuevoMemorandum();
    this.memorandum.idMemo = (this.memorandumEditar === null) ? null : this.memorandumEditar.idMemo;
    this.memorandum.coZonaRegi = (this.memorandumEditar === null) ? registro.zona : this.memorandumEditar.coZonaRegi;
    this.memorandum.coOficRegi = (this.memorandumEditar === null) ? registro.oficina : this.memorandumEditar.coOficRegi;
    this.memorandum.coLocaAten = (this.memorandumEditar === null) ? registro.local: this.memorandumEditar.coLocaAten;
    this.memorandum.idTipoMemo = (this.memorandumEditar === null) ? registro.tipoMemorandum : this.memorandumEditar.idTipoMemo;
    this.memorandum.nuSistran = (registro.nroSistran !== '') ? this.funciones.eliminarEspaciosEnBlanco(registro.nroSistran) : '';
    this.memorandum.feMemo = (registro.fechaMemorandum !== "") ? this.funciones.convertirFechaDateToString(registro.fechaMemorandum): "";

    if (this.camposMemorandum.controls.length > 0) {
      if (this.camposMemorandum.controls.length === 1) {
        this.memorandum.imMemo0001 = parseFloat(this.camposMemorandum.controls[0].value.valor);
      } else if (this.camposMemorandum.controls.length === 2) {
        this.memorandum.imMemo0001 = parseFloat(this.camposMemorandum.controls[0].value.valor);
        this.memorandum.imMemo0002 = parseFloat(this.camposMemorandum.controls[1].value.valor);
      } else if (this.camposMemorandum.controls.length === 3) {
        this.memorandum.imMemo0001 = parseFloat(this.camposMemorandum.controls[0].value.valor);
        this.memorandum.imMemo0002 = parseFloat(this.camposMemorandum.controls[1].value.valor);
        this.memorandum.imMemo0003 = parseFloat(this.camposMemorandum.controls[2].value.valor);
      } else if (this.camposMemorandum.controls.length === 4) {
        this.memorandum.imMemo0001 = parseFloat(this.camposMemorandum.controls[0].value.valor);
        this.memorandum.imMemo0002 = parseFloat(this.camposMemorandum.controls[1].value.valor);
        this.memorandum.imMemo0003 = parseFloat(this.camposMemorandum.controls[2].value.valor);
        this.memorandum.imMemo0004 = parseFloat(this.camposMemorandum.controls[3].value.valor);
      } else if (this.camposMemorandum.controls.length === 5) {
        this.memorandum.imMemo0001 = parseFloat(this.camposMemorandum.controls[0].value.valor);
        this.memorandum.imMemo0002 = parseFloat(this.camposMemorandum.controls[1].value.valor);
        this.memorandum.imMemo0003 = parseFloat(this.camposMemorandum.controls[2].value.valor);
        this.memorandum.imMemo0004 = parseFloat(this.camposMemorandum.controls[3].value.valor);
        this.memorandum.imMemo0005 = parseFloat(this.camposMemorandum.controls[4].value.valor);
      } else if (this.camposMemorandum.controls.length === 6) {
        this.memorandum.imMemo0001 = parseFloat(this.camposMemorandum.controls[0].value.valor);
        this.memorandum.imMemo0002 = parseFloat(this.camposMemorandum.controls[1].value.valor);
        this.memorandum.imMemo0003 = parseFloat(this.camposMemorandum.controls[2].value.valor);
        this.memorandum.imMemo0004 = parseFloat(this.camposMemorandum.controls[3].value.valor);
        this.memorandum.imMemo0005 = parseFloat(this.camposMemorandum.controls[4].value.valor);
        this.memorandum.imMemo0006 = parseFloat(this.camposMemorandum.controls[5].value.valor);
      }
  
      let valor: number = 0;
      for (let i = 0; i < this.camposMemorandum.controls.length; i++) {         
        if (this.camposMemorandum.controls[i].value.valor !== "") {
          let cant = parseFloat(this.camposMemorandum.controls[i].value.valor); 
          if (this.camposMemorandum.controls[i].value.accion === this.operacion.ES_SUMABLE) {
            valor = valor + cant;
          } else if (this.camposMemorandum.controls[i].value.accion === this.operacion.ES_RESTABLE) {
            valor = valor - cant;
          }       
        }
      }
      this.formMemorandum.patchValue({ "montoTotal": valor});
      this.formMemorandum.get('montoTotal')?.setValue(valor.toFixed(2));   
      this.memorandum.imTotal = parseFloat(valor.toFixed(2));
    } else {    
      this.memorandum.imTotal = parseFloat(registro.montoTotal);
    }

    this.memorandum.feDesdMemo = (registro.fechaDesde !== "") ? this.funciones.convertirFechaDateToString(registro.fechaDesde): "";
    this.memorandum.feHastMemo = (registro.fechaHasta !== "") ? this.funciones.convertirFechaDateToString(registro.fechaHasta): "";
    this.memorandum.noDest = (registro.destinatario !== '') ? this.funciones.eliminarEspaciosEnBlanco(registro.destinatario) : '';
    this.memorandum.noCarg = (registro.cargo !== '') ? this.funciones.eliminarEspaciosEnBlanco(registro.cargo) : '';
    this.memorandum.noAsun = (registro.asunto !== '') ? this.funciones.eliminarEspaciosEnBlanco(registro.asunto) : '';
    this.memorandum.noRefe = (registro.referencia !== '') ? this.funciones.eliminarEspaciosEnBlanco(registro.referencia) : '';
    this.memorandum.idBancOrig = registro.bancoOrigen == '*' ? '' : registro.bancoOrigen;
    this.memorandum.nuCuenBancOrig = registro.cuentaBancoOrigen == '(NINGUNO)' ? '' : registro.cuentaBancoOrigen;
    this.memorandum.idBancDest = registro.bancoDestino == '*' ? '' : registro.bancoDestino;
    this.memorandum.nuCuenBancDest = registro.cuentaBancoDestino == '(NINGUNO)' ? '' : registro.cuentaBancoDestino;
  } 

  async cancelar() {     
    let dataFiltrosMemorandum: IDataFiltrosMemorandum = await this.sharingInformationService.compartirDataFiltrosMemorandumObservable.pipe(first()).toPromise(); 
    if (dataFiltrosMemorandum.esBusqueda) {
      dataFiltrosMemorandum.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosMemorandumObservableData = dataFiltrosMemorandum;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/memorandum/bandeja']);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();    
  }

}