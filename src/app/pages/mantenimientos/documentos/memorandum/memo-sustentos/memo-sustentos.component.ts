import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';

import Swal from 'sweetalert2';
import { ConfirmationService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';
import { IMemorandum } from '../../../../../interfaces/memorandum.interface';

import { ArchivoAdjunto, TramaSustento } from '../../../../../models/archivo-adjunto.model';
import { BodyMemorandumSustento } from '../../../../../models/mantenimientos/documentos/memorandum.model';

import { GeneralService } from '../../../../../services/general.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { MemorandumService } from '../../../../../services/mantenimientos/documentos/memorandum.service';

import { UploadComponent } from '../../../../upload/upload.component';

@Component({
  selector: 'app-memo-sustentos',
  templateUrl: './memo-sustentos.component.html',
  styleUrls: ['./memo-sustentos.component.scss'],
  providers: [
    ConfirmationService
  ]
})
export class MemoSustentosComponent implements OnInit {

  isLoading!: boolean;
  coZonaRegi?: string;
  coOficRegi?: string;
  sizeFiles: number = 0;
  coTipoAdju: string = '005';

  add!: number;
  delete!: number;
  bandeja!: number;

  sustento!: FormGroup;

  files: ArchivoAdjunto[] = [];

  memorandum!: IMemorandum[]
  memoSustento: TramaSustento[] = [];

  bodyMemorandumSustento!: BodyMemorandumSustento

  @ViewChild(UploadComponent) upload: UploadComponent;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private generalService: GeneralService,
    private validarService: ValidatorsService,
    private memorandumService: MemorandumService
  ) { }

  ngOnInit(): void {
    this.bodyMemorandumSustento = new BodyMemorandumSustento();
    this.memorandum = this.config.data.memo as IMemorandum[];

    this.add = this.config.data.add;
    this.delete = this.config.data.delete;
    this.bandeja = this.config.data.bandeja;

    this.sustento = this.fb.group({
      nroMemo: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      nroSis: ['', [ Validators.required ]],
      docs: this.fb.array([])
    });

    this.hidrate( this.memorandum['0'] );
    this.sustentosMemorandum();
  }

  hidrate( m: IMemorandum ) {
    const fecMemo = this.validarService.reFormateaFecha( m.feMemo );

    this.sustento.get('nroMemo')?.setValue( m.nuMemo );
    this.sustento.get('fecha')?.setValue( fecMemo );
    this.sustento.get('nroSis')?.setValue( m.nuSistran );
    this.coZonaRegi = m.coZonaRegi;
    this.coOficRegi = m.coOficRegi;
  }

  sustentosMemorandum() {
    this.memorandumService.getMemorandumSustento( this.memorandum['0'].idMemo ).subscribe({
      next: ( sustentos ) => {
        console.log('memoSust', sustentos);
        this.memoSustento = sustentos.map( s => {
          return {
            id: s.idMemoSust,
            noDocuSust: s.noDocuSust,
            idGuidDocu: s.idGuidDocu
          }
        });

        this.upload.sustentos = this.memoSustento;
        this.upload.goEdit();
      }
    });
  }

  parse() {
    const docs = (this.sustento.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  grabar() {
    this.parse();
    console.log('this.files =', this.files);

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de grabar los sustentos cargados?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        if ( this.files.length != 0 ) {
          this.saveAdjuntoInFileServer();
        }
        else {
          this.saveSustentoInBD();
        }
      }
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            Swal.fire(`${ data.msgResult }`, '', 'error');
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            this.addSustento(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.saveSustentoInBD();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          this.ref.close('reject');
        }
      });
    }
  }

  addSustento(file?: any, guid?: string) {

    if ( file != '' && guid != '' ) {
      let trama = {} as TramaSustento

      trama.id = 0,
      trama.noDocuSust = file.deDocuAdju,
      trama.idGuidDocu = guid

      this.memoSustento.push( trama );
    }
  }

  saveSustentoInBD() {
    this.bodyMemorandumSustento.idMemo = this.memorandum['0'].idMemo!;
    this.bodyMemorandumSustento.trama = this.memoSustento;
    console.log('dataMemo', this.bodyMemorandumSustento );

    this.memorandumService.updateMemorandumSustento( this.bodyMemorandumSustento ).subscribe({
      next: ( d: any ) => {

        if ( d.codResult < 0 ) {
          Swal.fire( `${ d.msgResult }`, '', 'error');
          return;
        }

        Swal.close();
        this.ref.close('accept');
      },
      error: ( e ) => {
        console.log( e );
        Swal.fire('Error al guardar sustento.', '', 'error')
      },
      complete: () => {
        this.isLoading = false
      }
    });
  }

  addUploadedFile(file: any) {
    (this.sustento.get('docs') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.sustento.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.memoSustento.findIndex( f => f.id == fileId );
    this.memoSustento.splice( index, 1 );
  }

  cancelar() {
    this.ref.close('');
  }

}
