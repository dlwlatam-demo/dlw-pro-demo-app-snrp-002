import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService } from 'primeng/api';
import { environment } from 'src/environments/environment';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { GeneralService } from 'src/app/services/general.service';
import { TipoMemorandumService } from 'src/app/services/mantenimientos/configuracion/tipo-memorandum.service';
import { MemorandumService } from 'src/app/services/mantenimientos/documentos/memorandum.service';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { UtilService } from 'src/app/services/util.service';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IDataFiltrosMemorandum, IMemorandum } from 'src/app/interfaces/memorandum.interface';
import { IResponse, IResponse2 } from 'src/app/interfaces/general.interface';
import { TipoMemorandum } from 'src/app/models/mantenimientos/configuracion/tipo-memo.model';
import { BodyEliminarActivarMemorandum, BodyMemorandum } from 'src/app/models/mantenimientos/documentos/memorandum.model';
import { MemoSustentosComponent } from '../memo-sustentos/memo-sustentos.component';
import { BaseBandeja } from 'src/app/base/base-bandeja.abstract';
import { MenuOpciones } from 'src/app/models/auth/Menu.model';
import { ESTADO_REGISTRO } from 'src/app/models/enum/parameters';
import { IBandejaProcess } from 'src/app/interfaces/global.interface';
import { ValidatorsService } from '../../../../../core/services/validators.service';
@Component({
  selector: 'app-bandeja-memo',
  templateUrl: './bandeja-memo.component.html',
  styleUrls: ['./bandeja-memo.component.scss'],
  providers: [
    DialogService,
    MessageService
  ]
})
export class BandejaMemoComponent extends BaseBandeja implements OnInit, OnDestroy {

  formMemorandum: FormGroup;
  currentPage: string = environment.currentPage;
  unsubscribe$ = new Subject<void>();
  userCode: any;
  reporteExcel!: string;
  listaZonaRegistral: ZonaRegistral[];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoMemorandum: TipoMemorandum[];
  listaEstadoMemorandum: IResponse2[];
  listaMemorandums: IMemorandum[];
  fechaMinima: Date;
  fechaMaxima: Date;  
  bodyMemorandum: BodyMemorandum;  
  selectedValuesMemorandums: IMemorandum[] = []; 
  memorandumProceso: IBandejaProcess = {
    idMemo: null,
    proceso: ''
  };
  loadingMemorandums: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  bodyEliminarActivarMemorandum: BodyEliminarActivarMemorandum;

  //*************** listados para el filtro ***************/
  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaTipoMemorandumFiltro: TipoMemorandum[];
  listaEstadoMemorandumFiltro: IResponse2[];
  esBusqueda: boolean = false;
  estadoRegistro = ESTADO_REGISTRO;

  guidDocumento: string = '';

  constructor( 
    private formBuilder: FormBuilder,
    private dialogService: DialogService,
    private router: Router,
    private messageService: MessageService,
    private generalService: GeneralService,
    private tipoMemorandumService: TipoMemorandumService,
    private memorandumService: MemorandumService,
    private funciones: Funciones,
    private validateService: ValidatorsService,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService 
  ) { 
    super();
  }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Memoramdun;
    this.btnMemorandum();
    this.userCode = localStorage.getItem('user_code')||'';  
    this.fechaMaxima = new Date();
    this.fechaMinima = new Date(this.fechaMaxima.getFullYear(), this.fechaMaxima.getMonth(), 1);          
    this.construirFormulario();   

    this.sharingInformationService.compartirDataFiltrosMemorandumObservable.pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: IDataFiltrosMemorandum) => {  
      if (data.esCancelar && data.bodyMemorandum !== null) {    
        this.utilService.onShowProcessLoading("Cargando la data");
        this.setearCamposFiltro(data);
        this.buscarMemorandum(false);
      } else {
        this.inicilializarListas();
        this.obtenerListados();
      }
    });
  }

  private setearCamposFiltro(dataFiltro: IDataFiltrosMemorandum) {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoMemorandum = [];
    this.listaEstadoMemorandum = [];
    this.listaZonaRegistral = dataFiltro.listaZonaRegistral; 
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaTipoMemorandum = dataFiltro.listaTipoMemorandum;
    this.listaEstadoMemorandum = dataFiltro.listaEstadoMemorandum;
    this.bodyMemorandum = dataFiltro.bodyMemorandum;
    this.formMemorandum.patchValue({ "zona": this.bodyMemorandum.coZonaRegi});
    this.formMemorandum.patchValue({ "oficina": this.bodyMemorandum.coOficRegi});
    this.formMemorandum.patchValue({ "local": this.bodyMemorandum.coLocaAten});
    this.formMemorandum.patchValue({ "tipoMemorandum": this.bodyMemorandum.idTipoMemo});
    this.formMemorandum.patchValue({ "fechaDesde": (this.bodyMemorandum.feDesdMemo !== '' && this.bodyMemorandum.feDesdMemo !== null) ? this.funciones.convertirFechaStringToDate(this.bodyMemorandum.feDesdMemo) : this.bodyMemorandum.feDesdMemo});
    this.formMemorandum.patchValue({ "fechaHasta": (this.bodyMemorandum.feHastMemo !== '' && this.bodyMemorandum.feHastMemo !== null) ? this.funciones.convertirFechaStringToDate(this.bodyMemorandum.feHastMemo) : this.bodyMemorandum.feHastMemo});
    this.formMemorandum.patchValue({ "nroSistran": this.bodyMemorandum.nuSistran});
    this.formMemorandum.patchValue({ "usuario": this.bodyMemorandum.noUsuaCrea});
    this.formMemorandum.patchValue({ "estado": this.bodyMemorandum.esDocu});
  }

  private construirFormulario() {
    this.formMemorandum = this.formBuilder.group({
      zona: [''],
      oficina: [''],
      local: [''],
      tipoMemorandum: [''],     
      fechaDesde: [this.fechaMinima],
      fechaHasta: [this.fechaMaxima],    
      nroSistran: [''],
      usuario: [''],
      estado: ['']         
    });
  }

  private inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoMemorandum = [];
    this.listaEstadoMemorandum = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    this.listaTipoMemorandum.push({ idTipoMemo: 0, noTipoMemo: '(TODOS)' }); 
    this.listaEstadoMemorandum.push({ ccodigoHijo: '', cdescri: '(TODOS)' });   
  }
  
  private obtenerListados() {    
    this.utilService.onShowProcessLoading("Cargando la data"); 
    const tipoMemorandum = this.formMemorandum.get('tipoMemorandum')?.value;
    forkJoin(      
      this.generalService.getCbo_Zonas_Usuario( this.userCode),
      this.tipoMemorandumService.getBandejaTipoMemorandum( tipoMemorandum, 'A'),
      this.generalService.getCbo_EstadoMemorandum()      
    ).pipe(takeUntil(this.unsubscribe$))
    .subscribe(([resZonaRegistral, resTipoMemorandum, resEstadoMemomarndum]) => {   
      if (resZonaRegistral.length === 1) {   
        this.listaZonaRegistral.push({ coZonaRegi: resZonaRegistral[0].coZonaRegi, deZonaRegi: resZonaRegistral[0].deZonaRegi });    
        this.formMemorandum.patchValue({ "zona": resZonaRegistral[0].coZonaRegi});
        this.buscarOficinaRegistral();
      } else {    
        this.listaZonaRegistral.push(...resZonaRegistral);
      }             
      this.listaTipoMemorandum.push(...this.ordenarListaTipoMemorandum(resTipoMemorandum));    
      this.listaEstadoMemorandum.push(...resEstadoMemomarndum);        
      this.buscarMemorandum(true);    
    }, (err: HttpErrorResponse) => {                                  
    }) 
  }

  private ordenarListaTipoMemorandum(lista: TipoMemorandum[]): TipoMemorandum[] {
    let listaFinal: TipoMemorandum[] = [];
    listaFinal =  lista.sort( (a, b) => {       
      if (a.noTipoMemo.toLowerCase() < b.noTipoMemo.toLowerCase()) {
        return -1;
      }
      if (a.noTipoMemo.toLowerCase() > b.noTipoMemo.toLowerCase()) {
        return 1;
      }
      return 0;
    });
    return listaFinal;
  }

  public buscarOficinaRegistral() {    
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formMemorandum.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.formMemorandum.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.formMemorandum.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err: HttpErrorResponse) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    }   
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.formMemorandum.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.formMemorandum.get('zona')?.value,this.formMemorandum.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.formMemorandum.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err: HttpErrorResponse) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  } 

  public seleccionarTipoMemorandum() {
    const tipoMemo: TipoMemorandum = this.listaTipoMemorandum.find( (item: TipoMemorandum) => item.idTipoMemo === this.formMemorandum.get('tipoMemorandum')?.value );  
    this.formMemorandum.patchValue({ "destinatario": tipoMemo.noDest});
    this.formMemorandum.patchValue({ "cargo": tipoMemo.noCarg});
    this.formMemorandum.patchValue({ "asunto": tipoMemo.noAsun});
    this.formMemorandum.patchValue({ "referencia": tipoMemo.noRefe});
    this.formMemorandum.patchValue({ "bancoOrigen": tipoMemo.idBancOrig});
    this.formMemorandum.patchValue({ "cuentaBancoOrigen": tipoMemo.nuCuenBancOrig});
    this.formMemorandum.patchValue({ "nombreCuentaBancoOrigen": tipoMemo.noCuenBancOrig});
    this.formMemorandum.patchValue({ "bancoDestino": tipoMemo.idBancDest});
    this.formMemorandum.patchValue({ "cuentaBancoDestino": tipoMemo.nuCuenBancDest});
    this.formMemorandum.patchValue({ "nombreCuentaBancoDestino": tipoMemo.noCuenBancDest});
  }

  public cambioFechaInicio() {   
    if (this.formMemorandum.get('fechaHasta')?.value !== '' && this.formMemorandum.get('fechaHasta')?.value !== null) {
      if (this.formMemorandum.get('fechaDesde')?.value > this.formMemorandum.get('fechaHasta')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.formMemorandum.controls['fechaDesde'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.formMemorandum.get('fechaDesde')?.value !== '' && this.formMemorandum.get('fechaDesde')?.value !== null) {
      if (this.formMemorandum.get('fechaHasta')?.value < this.formMemorandum.get('fechaDesde')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.formMemorandum.controls['fechaHasta'].reset();    
      }      
    }
  }

  public buscarMemorandum(esCargaInicial: boolean) {   
    let resultado: boolean = this.validarCamporFiltros();
    if (!resultado) {
      return;
    }
    this.listaMemorandums = [];
    this.selectedValuesMemorandums = [];
    this.loadingMemorandums = (esCargaInicial) ? false : true;    
    this.guardarListadosParaFiltro();
    this.establecerBodyMemorandum(); 
    this.esBusqueda = (esCargaInicial) ? false : true;    
    this.memorandumService.getBandejaMemorandum(this.bodyMemorandum).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (data: any) => {
      this.listaMemorandums = data.lista;
      this.reporteExcel = data.reporteExcel;
      this.loadingMemorandums = false;
      this.utilService.onCloseLoading();       
    }, (err: HttpErrorResponse) => {   
      this.loadingMemorandums = false;
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("warning", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();
      }                             
    });
  }

  private validarCamporFiltros(): boolean {
    if (
      (this.formMemorandum.get('fechaDesde')?.value !== "" && this.formMemorandum.get('fechaDesde')?.value !== null) &&
      (this.formMemorandum.get('fechaHasta')?.value === "" || this.formMemorandum.get('fechaHasta')?.value === null)
    ) {
      document.getElementById('idFechaHasta').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de fin.");
      return false; 
    } else if (
      (this.formMemorandum.get('fechaDesde')?.value === "" || this.formMemorandum.get('fechaDesde')?.value === null) &&
      (this.formMemorandum.get('fechaHasta')?.value !== "" && this.formMemorandum.get('fechaHasta')?.value !== null)
    ) {
      document.getElementById('idFechaDesde').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de inicio.");
      return false; 
    } else {
      return true;
    }   
  }

  private establecerBodyMemorandum() {       
    this.bodyMemorandum = new BodyMemorandum();
    this.bodyMemorandum.coZonaRegi = (this.formMemorandum.get('zona')?.value === '*') ? '' : this.formMemorandum.get('zona')?.value;    
    this.bodyMemorandum.coOficRegi = (this.formMemorandum.get('oficina')?.value === '*') ? '' : this.formMemorandum.get('oficina')?.value;    
    this.bodyMemorandum.coLocaAten = (this.formMemorandum.get('local')?.value === '*') ? '' : this.formMemorandum.get('local')?.value;    
    this.bodyMemorandum.idTipoMemo = this.formMemorandum.get('tipoMemorandum')?.value;
    this.bodyMemorandum.feDesdMemo = (this.formMemorandum.get('fechaDesde')?.value !== "" && this.formMemorandum.get('fechaDesde')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formMemorandum.get('fechaDesde')?.value) : ""; 
    this.bodyMemorandum.feHastMemo = (this.formMemorandum.get('fechaHasta')?.value !== "" && this.formMemorandum.get('fechaHasta')?.value !== null) ? this.funciones.convertirFechaDateToString(this.formMemorandum.get('fechaHasta')?.value): ""; 
    this.bodyMemorandum.nuSistran = (this.formMemorandum.get('nroSistran')?.value === '') ? null : this.formMemorandum.get('nroSistran')?.value;
    this.bodyMemorandum.noUsuaCrea = this.formMemorandum.get('usuario')?.value;
    this.bodyMemorandum.esDocu = this.formMemorandum.get('estado')?.value;
  }

  private guardarListadosParaFiltro() {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaTipoMemorandumFiltro = [...this.listaTipoMemorandum];
    this.listaEstadoMemorandumFiltro = [...this.listaEstadoMemorandum];
  }

  private establecerDataFiltrosMemorandum() : IDataFiltrosMemorandum {
    let dataFiltrosMemorandum: IDataFiltrosMemorandum = {
      listaZonaRegistral: this.listaZonaRegistralFiltro, 
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaTipoMemorandum: this.listaTipoMemorandumFiltro,
      listaEstadoMemorandum: this.listaEstadoMemorandumFiltro,
      bodyMemorandum: this.bodyMemorandum,   
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosMemorandum;
  }

  public nuevoMemorandum() {
    this.memorandumProceso.idMemo = null;
    this.memorandumProceso.proceso = 'nuevo';    
    this.sharingInformationService.compartirMemorandumProcesobservableData = this.memorandumProceso;
    this.router.navigate(['SARF/mantenimientos/documentos/memorandum/nuevo']);
    this.sharingInformationService.irRutaBandejaMemorandumObservableData = false;
  }

  public editarMemorandum() {
    if (this.selectedValuesMemorandums.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
    } if (this.selectedValuesMemorandums.length === 1) {
      if (this.selectedValuesMemorandums[0].deEsta === this.estadoRegistro.ANULADO) {
        this.utilService.onShowAlert("warning", "Atención", "No puede EDITAR un registro ANULADO");
      } else {
        this.memorandumProceso.idMemo = this.selectedValuesMemorandums[0].idMemo;
        this.memorandumProceso.proceso = 'editar';      
        this.sharingInformationService.compartirMemorandumProcesobservableData = this.memorandumProceso;
        this.router.navigate(['SARF/mantenimientos/documentos/memorandum/editar']);
        this.sharingInformationService.irRutaBandejaMemorandumObservableData = false;
      }      
    } else if (this.selectedValuesMemorandums.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }    
  }

  public anularMemorandums() { 
    if (this.selectedValuesMemorandums.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosAnulados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ANULAR memorándums en estado REGISTRADO.");
    } else {
      let textoAnular = (this.selectedValuesMemorandums.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea anular ${textoAnular}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('anular'); 
          this.bodyEliminarActivarMemorandum = new BodyEliminarActivarMemorandum();
          let lista: number[] = [];
          for (let i = 0; i < this.selectedValuesMemorandums.length; i++) {
            lista.push(this.selectedValuesMemorandums[i].idMemo);           
          }
          this.bodyEliminarActivarMemorandum.memorandums = lista;
          this.memorandumService.anulaMemorandum(this.bodyEliminarActivarMemorandum).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarMemorandum(false);
              this.selectedValuesMemorandums = [];
            }             
          }, (err: HttpErrorResponse) => {   
            if (err.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", err.error.description);        
            } else {
              this.utilService.onShowMessageErrorSystem();        
            }                                 
          });          
        }
      });
    }    
  }

  private validarRegistrosAnulados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesMemorandums.length; i++) { 
      if (this.selectedValuesMemorandums[i].deEsta === this.estadoRegistro.ANULADO) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public activarMemorandums() {
    if (this.selectedValuesMemorandums.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosActivados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ACTIVAR memorándums en estado ANULADO.");
    } else {
      let textoActivar = (this.selectedValuesMemorandums.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea activar ${textoActivar}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('activar'); 
          this.bodyEliminarActivarMemorandum = new BodyEliminarActivarMemorandum();
          let lista: number[] = [];
          for (let i = 0; i < this.selectedValuesMemorandums.length; i++) {
            lista.push(this.selectedValuesMemorandums[i].idMemo);           
          }
          this.bodyEliminarActivarMemorandum.memorandums = lista;
          this.memorandumService.activateMemorandum(this.bodyEliminarActivarMemorandum).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("info", "Atención", res.msgResult);
            } else if (res.codResult === 0) {         
              this.buscarMemorandum(false);
              this.selectedValuesMemorandums = [];
            }             
          }, (err: HttpErrorResponse) => {   
            if (err.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", err.error.description);        
            } else {
              this.utilService.onShowMessageErrorSystem();        
            }                                 
          });          
        }
      });
    } 
  }

  private validarRegistrosActivados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesMemorandums.length; i++) { 
      if (this.selectedValuesMemorandums[i].deEsta === this.estadoRegistro.REGISTRADO) {
        resultado = false;
      }     
    }
    return resultado;
  }

  public consultarRegistro() {
    if (this.selectedValuesMemorandums.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para consultarlo.");
    } if (this.selectedValuesMemorandums.length === 1) { 
      this.memorandumProceso.idMemo = this.selectedValuesMemorandums[0].idMemo;
      this.memorandumProceso.proceso = 'consultar';        
      this.sharingInformationService.compartirMemorandumProcesobservableData = this.memorandumProceso;
      this.router.navigate(['SARF/mantenimientos/documentos/memorandum/consultar']);
      this.sharingInformationService.irRutaBandejaMemorandumObservableData = false;
    } else if (this.selectedValuesMemorandums.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede CONSULTAR un registro a la vez.");
    } 
  }

  verDocumento() {
    if (this.selectedValuesMemorandums.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro.");
    } if (this.selectedValuesMemorandums.length === 1) {
      if (this.selectedValuesMemorandums[0].deEsta === this.estadoRegistro.ANULADO) {
        this.utilService.onShowAlert("warning", "Atención", "El documento está ANULADO, no se puede ver el documento.");
      } else {
        const memorandum  = this.selectedValuesMemorandums;
        this.memorandumService.getMemorandumPdfById( memorandum['0'].idMemo! ).subscribe({
          next: ( data ) => {			
          this.guidDocumento = data.msgResult;
          const doc = data.archivoPdf;
          const byteCharacters = atob(doc!);
          const byteNumbers = new Array(byteCharacters.length);
          for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
          }
          const byteArray = new Uint8Array(byteNumbers);
          const blob = new Blob([byteArray], { type: 'application/pdf' });
          const blobUrl = URL.createObjectURL(blob);
          const download = document.createElement('a');
          download.href =  blobUrl;
          //download.setAttribute('download', 'Constacia_Abono');
          download.setAttribute("target", "_blank");
          document.body.appendChild( download );
          download.click();		
          },
          error: ( e ) => {
          this.utilService.onShowAlert("warning", "Atención", e.error.description);
          }
        });        
      }      
    } else if (this.selectedValuesMemorandums.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    } 
  }
  
  public sustentos() {
    if (this.selectedValuesMemorandums.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro.");
    } if (this.selectedValuesMemorandums.length === 1) {
      if (this.selectedValuesMemorandums[0].deEsta === this.estadoRegistro.ANULADO) {
        this.utilService.onShowAlert("warning", "Atención", "No puede modificar los sustentos de un memorándum ANULADO");
      } else {
        const dialog = this.dialogService.open( MemoSustentosComponent, {
          header: 'Memorándum - Sustentos',
          width: '45%',
          closable: false,
          data: {
            memo: this.selectedValuesMemorandums,
            add: this.BtnAdicionar_Sustento,
            delete: this.BtnEliminar_Sustento,
            bandeja: this.codBandeja
          }
        });    
        dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
          if ( type == 'accept' ) {
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Los sustentos han sido cargados y grabados correctamente'
            });    
            this.buscarMemorandum(false);
            this.selectedValuesMemorandums = [];
          }
          else if ( type == 'reject' ) {
            this.messageService.add({
              severity: 'error',
              summary: 'Ha ocurrido un error al cargar y grabar los sustentos'
            });    
            this.selectedValuesMemorandums = [];
          }    
          this.selectedValuesMemorandums = [];
        });        
      }      
    } else if (this.selectedValuesMemorandums.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }    
  }

  exportExcel() {
    if ( this.reporteExcel === null || this.reporteExcel === 'null' ) {
      return;
    }

    const fechaHoy = new Date();
    const fileName = this.validateService.formatDateWithHoursForExcel( fechaHoy );;

    const byteCharacters = atob(this.reporteExcel);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', `Memorandum_${ fileName }`);
    document.body.appendChild( download );
    download.click();
  }

  transformMonto( value: string ): string {
    return Number( value ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

  customSort( event: SortEvent ) { 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;
        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
        return (event.order! * result);
    });
  }

  ngOnDestroy(): void {          
    this.unsubscribe$.next();
    this.unsubscribe$.complete();                  
    this.sharingInformationService.compartirDataFiltrosMemorandumObservableData = this.establecerDataFiltrosMemorandum(); 
  }

}
