import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject, Subscription, timer, catchError, of } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';

import { SortEvent, ConfirmationService, MessageService } from 'primeng/api';

import { IDataFiltrosCheques, ICheque, IChequeValue } from '../../../../interfaces/cheque.interface';
import { IResponse2, IResponse } from '../../../../interfaces/general.interface';
import { ZonaRegistral, OficinaRegistral, Local, Banco } from '../../../../interfaces/combos.interface';

import { ChequeConsulta, BodyImprimirAnularDevolverCheques } from '../../../../models/mantenimientos/documentos/cheques.model';
import { CuentaBancaria } from '../../../../models/mantenimientos/maestras/cuenta-bancaria.model';
import { MenuOpciones } from '../../../../models/auth/Menu.model';

import { ChequesService } from '../../../../services/mantenimientos/documentos/cheques.service';
import { GeneralService } from '../../../../services/general.service';
import { UtilService } from '../../../../services/util.service';

import { Funciones } from '../../../../core/helpers/funciones/funciones';
import { SharingInformationService } from '../../../../core/services/sharing-services.service';
import { ValidatorsService } from '../../../../core/services/validators.service';

import { BaseBandeja } from '../../../../base/base-bandeja.abstract';

import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-cheques',
  templateUrl: './cheques.component.html',
  styleUrls: ['./cheques.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class ChequesComponent extends BaseBandeja implements OnInit {

  loadingZonaRegistral: boolean = true;
  loadingOficinaRegistral: boolean = true;
  loadingLocal: boolean = true;
  loadingEstado: boolean = true;
  loadingBancos: boolean = true;
  loadingCuentas: boolean = true;
  loadingCheques: boolean = false;

  esBusqueda: boolean = false;

  listaZonaRegistral!: ZonaRegistral[];
  listaOficinaRegistral!: OficinaRegistral[];
  listaLocal!: Local[];
  listaEstadoCheque!: IResponse2[];
  listaBancos!: Banco[];
  listaCuentas!: CuentaBancaria[];

  listaZonaRegistralFiltro!: ZonaRegistral[];
  listaOficinaRegistralFiltro!: OficinaRegistral[];
  listaLocalFiltro!: Local[];
  listaEstadoChequeFiltro!: IResponse2[];
  listaBancosFiltro!: Banco[];
  listaCuentasFiltro!: CuentaBancaria[];

  fechaDesde!: Date;
  fechaHasta!: Date;

  cheques!: FormGroup;

  bodyCheques!: ChequeConsulta;
  bodyImprimirAnularDevolverCheques!: BodyImprimirAnularDevolverCheques;

  chequesList!: IChequeValue;
  selectedCheques: ICheque[] = [];

  unsubscribe$ = new Subject<void>();
  chequesService$: Subscription = new Subscription;
  timerSubscription: Subscription = new Subscription();

  currentPage: string = environment.currentPage;

  constructor( 
    private fb: FormBuilder,
    private funciones: Funciones,
    private utilService: UtilService,
    private chequeService: ChequesService,
    private generalService: GeneralService,
    private validateService: ValidatorsService,
    private sharingInformationService: SharingInformationService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Cheques;
    this.btnCheques();

    this.bodyCheques = new ChequeConsulta();
    this.bodyImprimirAnularDevolverCheques = new BodyImprimirAnularDevolverCheques();

    this.fechaHasta = new Date();
    this.fechaDesde = new Date( this.fechaHasta.getFullYear(), this.fechaHasta.getMonth(), 1 );

    this.cheques = this.fb.group({
      zona: [{value: '', disabled: true}, []],
      oficina: ['', []],
      local: ['', []],
      estado: [{value: '002', disabled: true}, []],
      banco: [{value: '', disabled: true}, []],
      ctaBanc: ['', []],
      fecDes: [this.fechaDesde, []],
      fecHas: [this.fechaHasta, []],
      nroComp: ['', []],
      nroCheq: ['', []]
    });

    this._dataFiltrosCheques();
  }

  private _dataFiltrosCheques(): void {
    this.sharingInformationService.compartirDataFiltrosChequesObservable.pipe( takeUntil(this.unsubscribe$) )
    .subscribe( ( data: IDataFiltrosCheques ) => {  
      if ( data.esCancelar && data.bodyCheques !== null ) {
        this.utilService.onShowProcessLoading('Cargando la data');
        this._setearCamposFiltro(data);
        this.buscarCheque(false);
      } else {
        this._inicilializarListas()
        this._buscarZonaRegistral();
        this._buscarEstado();
        this._buscarBancos();
        this.buscarCheque(true);
      }
    });
  }

  private _inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaEstadoCheque = [];
    this.listaBancos = [];
    this.listaCuentas = [];

    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    this.listaEstadoCheque.push({ ccodigoHijo: '*', cdescri: '(TODOS)' });   
    this.listaBancos.push({ nidInstitucion: 0, cdeInst: '(TODOS)' });   
    this.listaCuentas.push({ nuCuenBanc: '(TODOS)' }); 
  }

  private _setearCamposFiltro( dataFiltro: IDataFiltrosCheques ) {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaEstadoCheque = [];
    this.listaBancos = [];
    this.listaCuentas = [];

    this.listaZonaRegistral = dataFiltro.listaZonaRegistral;
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaEstadoCheque = dataFiltro.listaEstadoCheque;
    this.listaBancos = dataFiltro.listaBancos;
    this.listaCuentas = dataFiltro.listaCuentas;
    this.bodyCheques = dataFiltro.bodyCheques;
    
    this.cheques.patchValue({ "zona": this.bodyCheques.coZonaRegi });
    this.cheques.patchValue({ "oficina": this.bodyCheques.coOficRegi });
    this.cheques.patchValue({ "local": this.bodyCheques.coLocaAten });
    this.cheques.patchValue({ "estado": this.bodyCheques.esDocu });
    this.cheques.patchValue({ "fecDes": (this.bodyCheques.feDesd !== '' && this.bodyCheques.feDesd !== null) ? this.funciones.convertirFechaStringToDate(this.bodyCheques.feDesd) : this.bodyCheques.feDesd });
    this.cheques.patchValue({ "fecHas": (this.bodyCheques.feHast !== '' && this.bodyCheques.feHast !== null) ? this.funciones.convertirFechaStringToDate(this.bodyCheques.feHast) : this.bodyCheques.feHast });
    this.cheques.patchValue({ "banco": this.bodyCheques.idBanc });
    this.cheques.patchValue({ "ctaBanc": this.bodyCheques.nuCuenBanc });
    this.cheques.patchValue({ "nroComp": this.bodyCheques.nuCompPago });
    this.cheques.patchValue({ "nroCheq": this.bodyCheques.nuCheq });
  }

  private _buscarZonaRegistral(): void {
    this.generalService.getCbo_Zonas_Usuario('').pipe( takeUntil( this.unsubscribe$ ) )
      .subscribe({
        next: ( data: any ) => {
          this.loadingZonaRegistral = false;
          this.cheques.get('zona')?.enable();

          if ( data.length === 1 ) {   
            this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });    
            this.cheques.patchValue({ 'zona': data[0].coZonaRegi });
            this.buscarOficinaRegistral();
          } else {
            this.listaZonaRegistral.push(...data);
          } 
        },
        error:( _err )=>{
          this.cheques.get('zona')?.enable();
        }
    });
  }

  public buscarOficinaRegistral(): void {
    this.cheques.get('oficina')?.disable();
    const zona = this.cheques.get('zona')?.value;

    this.listaOficinaRegistral = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    
    if ( this.cheques.get('zona')?.value !== '*' ) {
      this.generalService.getCbo_Oficinas_Zonas( '', zona )
      .pipe( takeUntil( this.unsubscribe$ ) )
      .subscribe( ( data: OficinaRegistral[] ) => {

        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.cheques.patchValue({ 'oficina': data[0].coOficRegi });
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }

        this.loadingOficinaRegistral = false;
        this.cheques.get('oficina')?.enable();
      }, ( _err ) => {
        this.loadingOficinaRegistral = false;
        this.cheques.get('oficina')?.enable();
      });
    } else {
      this.loadingOficinaRegistral = false;
      this.cheques.get('oficina')?.enable();
    } 
  }

  public buscarLocal() {
    this.cheques.get('local')?.disable();
    const zona = this.cheques.get('zona')?.value;
    const oficina = this.cheques.get('oficina')?.value

    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    
    if ( this.cheques.get('oficina')?.value !== '*' ) {
      this.generalService.getCbo_Locales_Ofic( '', zona, oficina )
      .pipe( takeUntil( this.unsubscribe$ ) )
      .subscribe( ( data: Local[] ) => {

        if ( data.length === 1 ) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.cheques.patchValue({ 'local': data[0].coLocaAten });         
        } else {    
          this.listaLocal.push(...data);          
        }

        this.loadingLocal = false;
        this.cheques.get('local')?.enable();
      }, ( _err ) => {
        this.loadingLocal = false;
        this.cheques.get('local')?.enable();
      });
    } else {
      this.loadingLocal = false;
      this.cheques.get('local')?.enable();
    }
  }

  private _buscarEstado() {
    this.generalService.getCbo_EstadoCheque().pipe( takeUntil( this.unsubscribe$ ) )
    .subscribe({
      next: ( data: IResponse2[] ) => {
        this.loadingEstado = false;
        this.cheques.get('estado')?.enable();

        const index = data.findIndex( (i: any) => i.ccodigoHijo == '002' );
        
        this.listaEstadoCheque.push(...data);
        this.cheques.patchValue({ 'estado': data[index].ccodigoHijo });
      },
      error:( _err )=>{
        this.loadingEstado = false;
        this.cheques.get('estado')?.enable();
      }
    });
  }

  private _buscarBancos() {
    this.generalService.getCbo_Bancos().pipe( takeUntil( this.unsubscribe$ ) )
    .subscribe({
      next: ( data: Banco[] ) => {
        this.loadingBancos = false;
        this.cheques.get('banco')?.enable();

        this.listaBancos.push(...data);
      },
      error:( _err )=>{
        this.loadingBancos = false;
        this.cheques.get('banco')?.enable();
      }
    });
  }

  public buscarCuentas( event: any ) {
    this.cheques.get('ctaBanc')?.disable();

    this.listaCuentas = [];
    this.listaCuentas.push({ nuCuenBanc: '(TODOS)' });

    if ( this.cheques.get('banco')?.value !== 0 ) {
      this.generalService.getCbo_Cuentas( event.value ).pipe( takeUntil( this.unsubscribe$ ) )
      .subscribe( ( data: any[] ) => {
        console.log('dataCuenta', data);
        this.loadingCuentas = false;
        this.cheques.get('ctaBanc')?.enable();

        this.listaCuentas.push(...data);
      }, (_err ) => {
        this.loadingCuentas = false;
        this.cheques.get('ctaBanc')?.enable();
      });
    } else {
      this.loadingCuentas = false;
      this.cheques.get('ctaBanc')?.enable();
    }
  }

  public cambioFechaInicio() {
    if (this.cheques.get('fecHas')?.value !== '' && this.cheques.get('fecHas')?.value !== null) {
      if (this.cheques.get('fecDes')?.value > this.cheques.get('fecHas')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.cheques.controls['fecDes'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.cheques.get('fecDes')?.value !== '' && this.cheques.get('fecDes')?.value !== null) {
      if (this.cheques.get('fecHas')?.value < this.cheques.get('fecDes')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.cheques.controls['fecHas'].reset();    
      }      
    }
  }

  buscarCheque( esCargaInicial: boolean ): void {
    const fechaDesde = this.cheques.get('fecDes')?.value;
    const fechaHasta = this.cheques.get('fecHas')?.value;

    if ( !fechaDesde && fechaHasta ) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha DESDE');  
      return;     
    }

    if ( !fechaHasta && fechaDesde ) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha HASTA');   
      return;     
    }

    this.guardarListadosParaFiltro();
    this.esBusqueda = ( esCargaInicial ) ? false : true;
    this.utilService.onShowProcessLoading('Cargando la data');
    this.loadingCheques = true;

    const zona = this.cheques.get('zona')?.value;
    const oficina = this.cheques.get('oficina')?.value;
    const local = this.cheques.get('local')?.value;
    const fechaDesdeFormat = (fechaDesde !== "" && fechaDesde !== null) ? this.funciones.convertirFechaDateToString(fechaDesde) : '';
    const fechaHastaFormat = (fechaHasta !== "" && fechaHasta !== null) ? this.funciones.convertirFechaDateToString(fechaHasta) : '';
    const estado = this.cheques.get('estado')?.value;
    const banco = this.cheques.get('banco')?.value;
    const ctaBanco = this.cheques.get('ctaBanc')?.value;
    const nroComprobante = this.cheques.get('nroComp')?.value;
    const nroCheque = this.cheques.get('nroCheq')?.value;

    this.bodyCheques.coZonaRegi = zona == '*' ? '' : zona;
    this.bodyCheques.coOficRegi = oficina == '*' ? '' : oficina;
    this.bodyCheques.coLocaAten = local == '*' ? '' : local;
    this.bodyCheques.esDocu = estado == '*' ? '' : estado;
    this.bodyCheques.idBanc = banco;
    this.bodyCheques.nuCuenBanc = ctaBanco == '(TODOS)' ? '' : ctaBanco;
    this.bodyCheques.idCompPago = 0;
    this.bodyCheques.nuCompPago = nroComprobante;
    this.bodyCheques.nuCheq = nroCheque;
    this.bodyCheques.feDesd = fechaDesdeFormat;
    this.bodyCheques.feHast = fechaHastaFormat;

    this.chequesService$ = this.chequeService.getBandejaCheque( this.bodyCheques )
      .subscribe({
        next: ( data ) => {
          this.loadingCheques = false;
          this.chequesList = data;
          this.utilService.onCloseLoading();
        },
        error: ( e ) => {
          this.loadingCheques = false;
          this.chequesList = null;
          this.utilService.onCloseLoading();

          if ( e.error.category === 'NOT_FOUND' ) {
            this.utilService.onShowAlert("warning", "Atención", e.error.description);
          } else {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      });
  }

  private guardarListadosParaFiltro() {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaEstadoChequeFiltro = [...this.listaEstadoCheque];
    this.listaBancosFiltro = [...this.listaBancos];
    this.listaCuentasFiltro = [...this.listaCuentas];
  }

  public solicitarImpresion() {
    if ( this.selectedCheques.length === 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro.");
      return;
    }

    // if ( this.selectedCheques.length > 1 ) {
    //   this.utilService.onShowAlert("warning", "Atención", "Solo puede solicitar la impresión de un registro a la vez.");
    //   return;
    // }

    for ( let cheque of this.selectedCheques ) {
      if ( cheque.esDocu !== '002' ) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede imprimir cheques con estado PENDIENTE DE IMPRESIÓN.");
        this.selectedCheques = [];
        return;
      }
    }

    this.bodyImprimirAnularDevolverCheques.cheques = this.selectedCheques.map( c => String( c.idCheq ) );
    console.log('idCheques', this.bodyImprimirAnularDevolverCheques);

    this.chequeService.impresionDeCheque( this.bodyImprimirAnularDevolverCheques ).subscribe({
      next: ( d: IResponse ) => {
        console.log('d', d);
        if ( d.codResult < 0 ) {
          this.utilService.onShowAlert('info', 'Atención', d.msgResult);
          return;
        }

        this.timerSubscription = timer(0, 1000 * 15).pipe(
          switchMap(() => {
            return this.generalService.chequesEnMemoria().pipe(
              catchError( err => {
                // Handle errors
                console.error(err);
                if ( err.error.category === 'NOT_FOUND' ) {
                  this.utilService.onCloseLoading();				
				          this.timerSubscription.unsubscribe();
                  this.selectedCheques = [];
                  this.buscarCheque(false);
                }
                
                return of(undefined);
              }));
            })
          )
          .subscribe( ( d ) => {
            this.utilService.onShowProcessLoading('En proceso de impresión');
          });
      },
      error: ( e ) => {
        console.log('e', e);
        if ( e.error.category === 'NOT_FOUND' ) {
          this.utilService.onShowAlert('warning', 'Atención', e.error.description);
        }
        else {
          this.utilService.onShowMessageErrorSystem();
        }
      }
    });
  }

  public solicitarAnular() {
    if ( this.selectedCheques.length === 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
      return;
    }

    for ( let cheque of this.selectedCheques ) {
      if ( cheque.esDocu !== '003' ) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puedes solicitar la anulación de cheques con estado IMPRESO.");
        return;
      }
    }

    const cheques = this.selectedCheques.map( cheque => String( cheque.idCheq ) );
    const msgAnular = ( this.selectedCheques.length === 1 ) ? 'del cheque' : 'de los cheques';

    this.utilService.onShowConfirm(`¿Está Ud. seguro que desea solicitar la anulación ${ msgAnular }?`)
      .then( ( result ) => {

        if ( result.isConfirmed ) {
          this.utilService.onShowProcess('anulacion');
          this.bodyImprimirAnularDevolverCheques.cheques = cheques;

          this.chequeService.solicitarAnulacionCheque( this.bodyImprimirAnularDevolverCheques ).subscribe({
            next: ( d: IResponse ) => {
              if ( d.codResult < 0 ) {
                this.utilService.onShowAlert('info', 'Atención', d.msgResult);
                return;
              }

              this.selectedCheques = [];
              this.buscarCheque(false);
            },
            error: ( e ) => {
              if ( e.error.category === 'NOT_FOUND' ) {
                this.utilService.onShowAlert('error', 'Atención', e.error.description);
              }
              else {
                this.utilService.onShowMessageErrorSystem();
              }
            }
          });
        } 
        else if ( result.isDenied ) {
          this.utilService.onShowAlert('warning', 'Atención', 'No se pudieron guardar los cambios.');
        }
      });
  }

  public solicitarDevolucion() {
    if ( this.selectedCheques.length === 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
      return;
    }

    for ( let cheque of this.selectedCheques ) {
      if ( cheque.esDocu !== '002' ) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede devolver cheques con estado PENDIENTE DE IMPRESIÓN.");
        return;
      }
    }

    const cheques = this.selectedCheques.map( cheque => String( cheque.idCheq ) );
    const msgDevolver = ( this.selectedCheques.length === 1 ) ? 'el cheque' : 'los cheques';

    this.utilService.onShowConfirm(`¿Está Ud. seguro que desea devolver ${ msgDevolver }?`)
      .then( ( result ) => {

        if ( result.isConfirmed ) {
          this.utilService.onShowProcess('devolucion');
          this.bodyImprimirAnularDevolverCheques.cheques = cheques;

          this.chequeService.solicitarDevolucionCheque( this.bodyImprimirAnularDevolverCheques ).subscribe({
            next: ( d: IResponse ) => {
              if ( d.codResult < 0 ) {
                this.utilService.onShowAlert('info', 'Atención', d.msgResult);
                return;
              }

              this.selectedCheques = [];
              this.buscarCheque(false);
            },
            error: ( e ) => {
              if ( e.error.category === 'NOT_FOUND' ) {
                this.utilService.onShowAlert('error', 'Atención', e.error.description);
              }
              else {
                this.utilService.onShowMessageErrorSystem();
              }
            }
          });
        } 
        else if ( result.isDenied ) {
          this.utilService.onShowAlert('warning', 'Atención', 'No se pudieron guardar los cambios.');
        }
      });
  }

  public exportExcel() {
    if ( this.chequesList.reporteExcel === null || this.chequesList.reporteExcel === 'null' ) {
      return;
    }

    const fechaHoy = new Date();
    const fileName = this.validateService.formatDateWithHoursForExcel( fechaHoy );

    const byteCharacters = atob(this.chequesList.reporteExcel);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', `Cheques_${ fileName }`);
    document.body.appendChild( download );
    download.click();
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  private establecerDataFiltrosCheques(): IDataFiltrosCheques {
    const dataFiltrosCheques: IDataFiltrosCheques = {
      listaZonaRegistral: this.listaZonaRegistralFiltro, 
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaEstadoCheque: this.listaEstadoChequeFiltro,
      listaBancos: this.listaBancosFiltro,
      listaCuentas: this.listaCuentasFiltro,
      bodyCheques: this.bodyCheques,   
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }

    return dataFiltrosCheques;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();  
    this.chequesService$.unsubscribe();
    this.timerSubscription.unsubscribe();
    this.sharingInformationService.compartirDataFiltrosChequesObservableData = this.establecerDataFiltrosCheques(); 
  }

}
