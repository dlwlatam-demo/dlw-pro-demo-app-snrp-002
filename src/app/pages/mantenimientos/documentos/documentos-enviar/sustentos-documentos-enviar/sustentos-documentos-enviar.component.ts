import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { ArchivoAdjunto, TramaSustento } from '../../../../../models/archivo-adjunto.model';
import { IDocumentosEnviar, IDocuEnviarSustento } from '../../../../../interfaces/documentos-enviar.interface';
import { BodyDocumentosEnviarSustento } from '../../../../../models/mantenimientos/documentos/documentos-enviar.models';
import { UploadComponent } from '../../../../upload/upload.component';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ConfirmationService } from 'primeng/api';
import { DocumentoEnviarService } from '../../../../../services/mantenimientos/documentos/documentos-enviar.service';
import { GeneralService } from '../../../../../services/general.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import Swal from 'sweetalert2';
import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';

@Component({
  selector: 'app-sustentos-documentos-enviar',
  templateUrl: './sustentos-documentos-enviar.component.html',
  styleUrls: ['./sustentos-documentos-enviar.component.scss'],
  providers: [
    ConfirmationService
  ]
})
export class SustentosDocumentosEnviarComponent implements OnInit {

  isLoading!: boolean;
  coZonaRegi?: string;
  coOficRegi?: string;
  sizeFiles: number = 0;
  coTipoAdju: string = '999';

  add!: number;
  delete!: number;
  bandeja!: number;

  sustento!: FormGroup;

  files: ArchivoAdjunto[] = [];

  docuEnviar!: IDocumentosEnviar[];
  docuEnviarSustento: IDocuEnviarSustento[] = [];

  bodyDocuEnviarSustento!: BodyDocumentosEnviarSustento;

  @ViewChild(UploadComponent) upload: UploadComponent;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private generalService: GeneralService,
    private validarService: ValidatorsService,
    private documentoEnviarService: DocumentoEnviarService
  ) { }

  ngOnInit(): void {
    this.bodyDocuEnviarSustento = new BodyDocumentosEnviarSustento();
    this.docuEnviar = this.config.data.varios as IDocumentosEnviar[];

    this.add = this.config.data.add;
    this.delete = this.config.data.delete;
    this.bandeja = this.config.data.bandeja;

    this.sustento = this.fb.group({
      tipoDocumento: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      docs: this.fb.array([])
    });

    this.hidrate( this.docuEnviar['0'] );
    this.sustentoDocumentoEnviar();
  }

  get isDisabled() {
    return this.docuEnviar['0'].esDocu != '001'
  }

  addUploadedFile(file: any) {
    (this.sustento.get('docs') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.sustento.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.docuEnviarSustento.findIndex( f => f.idDocuEnviSust == fileId );
    this.docuEnviarSustento.splice( index, 1 );
  }

  hidrate( d: IDocumentosEnviar ) {
    console.log('DocuVariData', d);
    const fecha = this.validarService.reFormateaFecha( d.feDocu );

    this.sustento.get('tipoDocumento')?.setValue( d.deTipoProc );
    this.sustento.get('fecha')?.setValue( fecha );
    this.coZonaRegi = d.coZonaRegi;
    this.coOficRegi = d.coOficRegi;
  }

  sustentoDocumentoEnviar() {
    this.documentoEnviarService.getDocumentoEnviarSustento( this.docuEnviar['0'].idDocuEnvi ).subscribe({
      next: ( sustentos ) => {
        console.log('docuEnviSust', sustentos);
        if ( sustentos[0].idDocuEnviSust != 0 ) {
          this.docuEnviarSustento = sustentos.map( s => {
            return {
              idDocuEnviSust: s.idDocuEnviSust,
              noDocuSust: s.noDocuSust,
              noRutaSust: s.noRutaSust,
              idGuidDocu: s.idGuidDocu
            }
          });
        }
        else {
          return;
        }

        this.upload.sustDocuEnvi = this.docuEnviarSustento;
        this.upload.goEdit();
      }
    });
  }

  parse() {
    const docs = (this.sustento.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  grabar() {
    this.parse();
    console.log('this.files =', this.files);

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de grabar los sustentos cargados?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        if ( this.files.length != 0 ) {
          this.saveAdjuntoInFileServer();
        }
        else {
          this.saveSustentoInBD();
        }
      },
      reject: () => {
        this.files = [];
      }
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            Swal.fire(`${ data.msgResult }`, '', 'error');
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            this.addSustento(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.saveSustentoInBD();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          Swal.close();
          this.files = [];
          this.ref.close('reject');
        }
      });
    }
  }

  addSustento(file?: any, guid?: string) {

    if ( file != '' && guid != '' ) {
      let trama = {} as any

      trama.idDocuEnviSust = 0,
      trama.noDocuSust = file.deDocuAdju,
      trama.idGuidDocu = guid

      this.docuEnviarSustento.push( trama );
    }
  }

  saveSustentoInBD() {
    this.bodyDocuEnviarSustento.idDocuEnvi = this.docuEnviar['0'].idDocuEnvi!;
    this.bodyDocuEnviarSustento.trama = this.docuEnviarSustento;
    console.log('dataDocuVar', this.bodyDocuEnviarSustento );

    this.documentoEnviarService.updateDocumentoEnviarSustento( this.bodyDocuEnviarSustento ).subscribe({
      next: ( d: any ) => {
        console.log( d );

        if ( d.codResult < 0 ) {
          Swal.fire( `${ d.msgResult }`, '', 'error');
          return;
        }

        Swal.close();
        this.files = [];
        this.ref.close('accept');
      },
      error: ( e ) => {
        console.log( e );
        Swal.fire('Error al guardar sustento.', '', 'error');
        this.files = [];
        this.isLoading = false;
      },
      complete: () => {
        this.files = [];
        this.isLoading = false;
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
