import { UtilService } from '../../../../../services/util.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import { GeneralService } from '../../../../../services/general.service';
import { Local, OficinaRegistral, ZonaRegistral } from '../../../../../interfaces/combos.interface';
import { IResponse2 } from '../../../../../interfaces/general.interface';
import { Funciones } from '../../../../../core/helpers/funciones/funciones';
import { SharingInformationService } from '../../../../../core/services/sharing-services.service';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { IBandejaProcess } from '../../../../../interfaces/global.interface';
import { BodyAnularActivarDocumentosEnviar, BodyDocumentosEnviar } from '../../../../../models/mantenimientos/documentos/documentos-enviar.models';
import { DocumentoEnviarService } from '../../../../../services/mantenimientos/documentos/documentos-enviar.service';
import { IDataFiltrosDocumentosEnviar, IDocumentosEnviar } from '../../../../../interfaces/documentos-enviar.interface';
import { EnviarDocumentosEnviarComponent } from '../enviar-documentos-enviar/enviar-documentos-enviar.component';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { SustentosDocumentosEnviarComponent } from '../sustentos-documentos-enviar/sustentos-documentos-enviar.component';
import { DocumentosEnviarBandeja } from 'src/app/interfaces/documentos-enviar.interface';
import { ValidatorsService } from '../../../../../core/services/validators.service';

@Component({
  selector: 'app-bandeja-documentos-enviar',
  templateUrl: './bandeja-documentos-enviar.component.html',
  styleUrls: ['./bandeja-documentos-enviar.component.scss'],
  providers: [
    DialogService,
    MessageService
  ]
})
export class BandejaDocumentosEnviarComponent extends BaseBandeja implements OnInit, OnDestroy {
  userCode:any
  unsubscribe$ = new Subject<void>();
  
  loadingZonaRegistral: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  loadingTipoDocumento: boolean = false;
  loadingEstado: boolean = false;

  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoDocumento: IResponse2[] = [];
  listaEstadoRecibo: IResponse2[] = [];

  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaTipoDocumentoFiltro: IResponse2[];
  listaEstadoReciboFiltro: IResponse2[];
  esBusqueda: boolean = false;
  fechaMinima: Date;
  fechaMaxima: Date; 
  documentosEnviar!: FormGroup;
  currentPage: string = environment.currentPage;
  bodyDocumentosEnviar: BodyDocumentosEnviar;
  bodyAnularActivarDocumentosEnviar: BodyAnularActivarDocumentosEnviar;
  documentosEnviarList: DocumentosEnviarBandeja;
  documentosEnviarProceso: IBandejaProcess = {
    idMemo: null,
    proceso: ''
  };
  override selectedValues: IDocumentosEnviar[] = [];

  constructor( 
    private fb: FormBuilder,
    private router: Router,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private utilService: UtilService,
    private funciones: Funciones,
    private validateService: ValidatorsService,
    private documentoEnviarService: DocumentoEnviarService,
    private sharingInformationService: SharingInformationService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Enviar_Documentos;
    this.btnEnviarDocumentos();
    
    this.bodyDocumentosEnviar = new BodyDocumentosEnviar();
    this.bodyAnularActivarDocumentosEnviar = new BodyAnularActivarDocumentosEnviar();
    this.userCode = localStorage.getItem('user_code')||'';
    this.fechaMaxima = new Date();
    this.fechaMinima = new Date(this.fechaMaxima.getFullYear(), this.fechaMaxima.getMonth(), 1);
    this.documentosEnviar = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoDocu: [''],
      fecDes: [this.fechaMinima],
      fecHas: [this.fechaMaxima],
      numero: [''],
      usuario: [''],
      estado: ['001'],
    })
    this._dataFiltrosDocumentosEnviar();
  }

  private _buscarZonaRegistral(): void {
    this.loadingZonaRegistral = true;
    this.generalService.getCbo_Zonas_Usuario(this.userCode).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingZonaRegistral = false;    
        if (data.length === 1) {
          this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });    
          this.documentosEnviar.patchValue({ "zona": data[0].coZonaRegi});
          this.buscarOficinaRegistral();
        } else {
          this.listaZonaRegistral.push(...data);
        }
      },
      error:( err )=>{
        this.loadingZonaRegistral = false;    
      }
    });
  }

  public buscarOficinaRegistral(): void {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.selectedValues = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.documentosEnviar.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.documentosEnviar.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.documentosEnviar.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.documentosEnviar.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.documentosEnviar.get('zona')?.value,this.documentosEnviar.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.documentosEnviar.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  private _buscarTipoDocumento() {
    this.loadingTipoDocumento = true;
    this.generalService.getCbo_TipoProcesoEnviar().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        // if (data.length === 1) {
        //   this.listaTipoDocumento.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
        //   this.documentosEnviar.patchValue({ "tipoDocu": data[0].ccodigoHijo});        
        // } else {    
        // }
        this.listaTipoDocumento.push(...data);
        this.loadingTipoDocumento = false;
      },
      error:( err )=>{
        this.loadingTipoDocumento = false;    
      }
    });
  }

  public cambioFechaInicio() {   
    if (this.documentosEnviar.get('fecHas')?.value !== '' && this.documentosEnviar.get('fecHas')?.value !== null) {
      if (this.documentosEnviar.get('fecDes')?.value > this.documentosEnviar.get('fecHas')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.documentosEnviar.controls['fecDes'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.documentosEnviar.get('fecDes')?.value !== '' && this.documentosEnviar.get('fecDes')?.value !== null) {
      if (this.documentosEnviar.get('fecHas')?.value < this.documentosEnviar.get('fecDes')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.documentosEnviar.controls['fecHas'].reset();    
      }      
    }
  }

  private _buscarEstado(): void {
    this.loadingEstado = true;
    this.generalService.getCbo_EstadoDocumentosEnviar().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        if (data.length === 1) {
          this.listaEstadoRecibo.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
          this.documentosEnviar.patchValue({ "estado": data[0].ccodigoHijo});         
        } else {
          const index = data.findIndex( (i: any) => i.ccodigoHijo == '001' );
          
          this.listaEstadoRecibo.push(...data);
          this.documentosEnviar.patchValue({ "estado": data[index].ccodigoHijo});
        }
        this.loadingEstado = false;    
      },
      error:( err )=>{
        this.loadingEstado = false;    
      }
    });
  }

  private _dataFiltrosDocumentosEnviar(): void {
    this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservable.pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: IDataFiltrosDocumentosEnviar) => {
      if (data.esCancelar && data.bodyDocumentosEnviar !== null) {      
        this.utilService.onShowProcessLoading("Cargando la data");
        this._setearCamposFiltro(data);
        this.buscarDocumentosEnviar(false);
      } else {
        this._inicilializarListas();
        this._buscarZonaRegistral();
        this._buscarTipoDocumento();
        this._buscarEstado();
        this.buscarDocumentosEnviar(true);
      }
    });
  }

  private _setearCamposFiltro(dataFiltro: IDataFiltrosDocumentosEnviar): void {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaEstadoRecibo = [];
    this.listaZonaRegistral = dataFiltro.listaZonaRegistral; 
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaTipoDocumento = dataFiltro.listaTipoDocumento;
    this.listaEstadoRecibo = dataFiltro.listaEstadoRecibo;
    this.bodyDocumentosEnviar = dataFiltro.bodyDocumentosEnviar;
    this.documentosEnviar.patchValue({ "zona": this.bodyDocumentosEnviar.coZonaRegi});
    this.documentosEnviar.patchValue({ "oficina": this.bodyDocumentosEnviar.coOficRegi});
    this.documentosEnviar.patchValue({ "local": this.bodyDocumentosEnviar.coLocaAten});
    this.documentosEnviar.patchValue({ "tipoDocu": this.bodyDocumentosEnviar.tiProc});
    this.documentosEnviar.patchValue({ "fecDes": (this.bodyDocumentosEnviar.feDesd !== '' && this.bodyDocumentosEnviar.feDesd !== null) ? this.funciones.convertirFechaStringToDate(this.bodyDocumentosEnviar.feDesd) : this.bodyDocumentosEnviar.feDesd});
    this.documentosEnviar.patchValue({ "fecHas": (this.bodyDocumentosEnviar.feHast !== '' && this.bodyDocumentosEnviar.feHast !== null) ? this.funciones.convertirFechaStringToDate(this.bodyDocumentosEnviar.feHast) : this.bodyDocumentosEnviar.feHast});
    this.documentosEnviar.patchValue({ "numero": this.bodyDocumentosEnviar.idDocuEnvi});
    this.documentosEnviar.patchValue({ "usuario": this.bodyDocumentosEnviar.noUsuaCrea});
    this.documentosEnviar.patchValue({ "estado": this.bodyDocumentosEnviar.esDocu});
  }

  private _inicilializarListas(): void {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaEstadoRecibo = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    this.listaTipoDocumento.push({ ccodigoHijo: '*', cdescri: '(TODOS)' }); 
    this.listaEstadoRecibo.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' });   
  }

  public buscarDocumentosEnviar(esCargaInicial: boolean): void {
    const fechaDesde = this.documentosEnviar.get('fecDes')?.value;
    const fechaHasta = this.documentosEnviar.get('fecHas')?.value;
    if (!fechaDesde && fechaHasta) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha DESDE');  
      return;     
    }
    if (!fechaHasta && fechaDesde) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha HASTA');   
      return;     
    }
    const estadoDocumento = this.documentosEnviar.get('estado')?.value;
    if(estadoDocumento === '') {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar el Estado');  
      return;		
    }
	
    this.guardarListadosParaFiltro();
    this.esBusqueda = (esCargaInicial) ? false : true;    
    this.utilService.onShowProcessLoading("Cargando la data"); 
    const zona = this.documentosEnviar.get('zona')?.value;
    const oficina = this.documentosEnviar.get('oficina')?.value;
    const local = this.documentosEnviar.get('local')?.value;
    const fechaDesdeFormat = (fechaDesde !== "" && fechaDesde !== null) ? this.funciones.convertirFechaDateToString(fechaDesde) : '';
    const fechaHastaFormat = (fechaHasta !== "" && fechaHasta !== null) ? this.funciones.convertirFechaDateToString(fechaHasta) : '';
    const tipoDocu = this.documentosEnviar.get('tipoDocu')?.value;

    this.bodyDocumentosEnviar.coZonaRegi = zona === '*' ? '' : zona;
    this.bodyDocumentosEnviar.coOficRegi = oficina === '*' ? '' : oficina;
    this.bodyDocumentosEnviar.coLocaAten = local === '*' ? '' : local;
    this.bodyDocumentosEnviar.tiProc = tipoDocu === '*' ? '' : tipoDocu;
    this.bodyDocumentosEnviar.idDocuEnvi = this.documentosEnviar.get('numero')?.value;
    this.bodyDocumentosEnviar.feDesd = fechaDesdeFormat;
    this.bodyDocumentosEnviar.feHast = fechaHastaFormat;
    this.bodyDocumentosEnviar.esDocu = estadoDocumento === '*' ? '' : estadoDocumento;
    this.bodyDocumentosEnviar.noUsuaCrea = this.documentosEnviar.get('usuario')?.value;

    this.documentoEnviarService.getBandejaDocumentoEnviar(this.bodyDocumentosEnviar).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.documentosEnviarList = data;
          this.selectedValues = [];
          this.utilService.onCloseLoading();  
        },
        error:( d )=>{
          this.documentosEnviarList = null;
          this.selectedValues = [];
          this.utilService.onCloseLoading();  
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("warning", "Atención", d.error.description);        
          }else {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      });
  }

  private guardarListadosParaFiltro(): void {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaTipoDocumentoFiltro = [...this.listaTipoDocumento];
    this.listaEstadoReciboFiltro = [...this.listaEstadoRecibo];
  }

  public nuevoDocumentosEnviar(): void {    
    this.documentosEnviarProceso.idMemo = null;
    this.documentosEnviarProceso.proceso = 'nuevo';    
    this.sharingInformationService.compartirComprobantePagoProcesoObservableData = this.documentosEnviarProceso;
    this.router.navigate(['SARF/mantenimientos/documentos/documentos_enviar/nuevo']);
    this.sharingInformationService.irRutaBandejaDocumentosEnviarObservableData = false;
  }

  public editarDocumentosEnviar(): void {
    console.log("this.selectedValues", this.selectedValues);
    
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
      return;
    }

    if ( this.selectedValues[0].esDocu !== '001' ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo se puede editar un registro en estado REGISTRADO");
      return;
    }

    this.documentosEnviarProceso.idMemo = this.selectedValues[0].idDocuEnvi;
    this.documentosEnviarProceso.proceso  = 'editar'; 
    this.sharingInformationService.compartirComprobantePagoProcesoObservableData = this.documentosEnviarProceso
    this.router.navigate([`SARF/mantenimientos/documentos/documentos_enviar/editar/${ this.selectedValues[0].idDocuEnvi}`])
    this.sharingInformationService.irRutaBandejaDocumentosEnviarObservableData = false;
  }

  public enviarDocumento() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para enviar.");
      return;
    }

    if ( this.selectedValues[0].esDocu !== '001' ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede enviar un registro con estado REGISTRADO.");
      return;
    }

    const dialog = this.dialogService.open( EnviarDocumentosEnviarComponent, {
      header: 'Enviar Documentos - Personas',
      width: '40%',
      closable: false,
      data: {
        doc: this.selectedValues,
        title: 'enviar'
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'El documento fue enviado satisfactoriamente'
        });

        this.buscarDocumentosEnviar(false);
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al enviar el documento'
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  public reenviarDocumentos() {
    if ( this.selectedValues?.length === 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para reenviar.");
      return;
    }

    if ( this.selectedValues[0].esDocu == '004' || this.selectedValues[0].esDocu == '002' || this.selectedValues[0].esDocu == '005' ) {
      const dialog = this.dialogService.open( EnviarDocumentosEnviarComponent, {
        header: 'Reenviar Documentos - Personas',
        width: '40%',
        closable: false,
        data: {
          doc: this.selectedValues,
          title: 'reenviar'
        }
      });
  
      dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
        if ( type == 'accept' ) {
          this.messageService.add({ 
            severity: 'success', 
            summary: 'El documento fue enviado satisfactoriamente'
          });
  
          this.buscarDocumentosEnviar(false);
          this.selectedValues = [];
        }
        else if ( type == 'reject' ) {
          this.messageService.add({
            severity: 'error',
            summary: 'Ha ocurrido un error al enviar el documento'
          });
  
          this.selectedValues = [];
        }
  
        this.selectedValues = [];
      });
    }
    else {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede reenviar registros con estado ENVIADO, RE-ENVIADO o RECIBIDO.");
      return;
    }

  }

  public sustentos(): void {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para ver los sustentos.");
      return;
    }

    if ( this.selectedValues?.length !== 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ver los sustentos de un registro a la vez.");
      this.selectedValues = [];
      return;
    }

    const dialog = this.dialogService.open( SustentosDocumentosEnviarComponent, {
      header: 'Documentos Enviar  - Sustentos',
      width: '45%',
      closable: false,
      data: {
        varios: this.selectedValues,
        add: this.BtnAdicionar_Sustento,
        delete: this.BtnEliminar_Sustento,
        bandeja: this.codBandeja
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Los sustentos han sido cargados y grabados correctamente'
        });

        this.buscarDocumentosEnviar(false);
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al cargar y grabar los sustentos'
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  verDocumento() {
    console.log('verDocumento');
      if ( !this.selectedValues || !this.selectedValues.length ) {
        this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro.");
        return;
      }
  
      if ( this.selectedValues.length != 1 ) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede visualizar el documento de un registro a la vez.");
        this.selectedValues = [];
        return;
      }

      const blobUrl = this.generalService.downloadManager( this.selectedValues[0].idGuidDocu );
      const download = document.createElement('a');
      download.href =  blobUrl;
      download.setAttribute("target", "_blank");
      document.body.appendChild( download );
      download.click();
    
    }

  public anular(): void {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
      return;
    }

    for ( let registros of this.selectedValues ) {
      if ( registros.esDocu != '001' ) {
        this.utilService.onShowAlert("warning", "Atención", "Solo se puede anular registros en estado REGISTRADO.");
        return;
      }
    }
    
    const idCartasOrdenes = this.selectedValues?.map( value => {
      const trama = {
        idDocuEnvi: value.idDocuEnvi?.toString()
      }
        return trama
    });
    const textoAnular = (this.selectedValues?.length === 1) ? 'el registro' : 'los registros';
    this.utilService.onShowConfirm(`¿Estás seguro que desea anular ${textoAnular}?`).then((result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('anular'); 
        this.bodyAnularActivarDocumentosEnviar.trama = idCartasOrdenes;
        this.documentoEnviarService.anularDocumentoEnviar(this.bodyAnularActivarDocumentosEnviar).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0 ) {
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
            } else if (d.codResult === 0 ) {
              this.selectedValues = [];
              this.buscarDocumentosEnviar(false);
            }
          },

          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", d.error.description);        

            }else {
              this.utilService.onShowMessageErrorSystem();        
            }
          }
        });
        
      } else if (result.isDenied) {
        this.utilService.onShowAlert("warning", "Atención", "No se pudieron guardar los cambios.");
      }
    })
  }

  public activar(): void {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
      return;
    }

    for ( let registros of this.selectedValues ) {
      if ( registros.esDocu != '003' ) {
        this.utilService.onShowAlert("warning", "Atención", "Solo se puede activar un registro en estado ANULADO.");
        return;
      }
    }

    const idCartasOrdenes = this.selectedValues?.map( value => {
      const trama = {
        idDocuEnvi: value.idDocuEnvi?.toString()
      }
        return trama
    });
    const textoActivar = (this.selectedValues?.length === 1) ? 'el registro' : 'los registros';
    this.utilService.onShowConfirm(`¿Estás seguro que desea activar ${textoActivar}?`).then((result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('activar'); 
        this.bodyAnularActivarDocumentosEnviar.trama = idCartasOrdenes;
        this.documentoEnviarService.activarDocumentoEnviar(this.bodyAnularActivarDocumentosEnviar).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0) {
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
            } else if (d.codResult === 0) {
              this.selectedValues = [];
              this.buscarDocumentosEnviar(false);
            }
          },
          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              Swal.fire( 'Carta orden', `${d.error.description}`, 'info');
              this.utilService.onShowAlert("error", "Atención", d.error.description);        
            }else {
              this.utilService.onShowMessageErrorSystem();        
            }
          }
        });
        
      } else if (result.isDenied) {
        this.utilService.onShowAlert("warning", "Atención", "No se pudieron guardar los cambios.");
      }
    });
  }

  public consultar(): void {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para consultarlo.");
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede consultar un registro a la vez.");
      return;
    }
    this.documentosEnviarProceso.idMemo = this.selectedValues[0].idDocuEnvi;
    this.documentosEnviarProceso.proceso = 'consultar';        
    this.sharingInformationService.compartirComprobantePagoProcesoObservableData = this.documentosEnviarProceso;
    this.router.navigate([`SARF/mantenimientos/documentos/documentos_enviar/consultar/${ this.selectedValues[0].idDocuEnvi }`])
    this.sharingInformationService.irRutaBandejaDocumentosEnviarObservableData = false;
  }

  exportExcel() {
    if ( this.documentosEnviarList.reporteExcel === null || this.documentosEnviarList.reporteExcel === 'null' ) {
      return;
    }

    const fechaHoy = new Date();
    const fileName = this.validateService.formatDateWithHoursForExcel( fechaHoy );

    const byteCharacters = atob(this.documentosEnviarList.reporteExcel);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', `EnviarDocumento_${ fileName }`);
    document.body.appendChild( download );
    download.click();
  }

  public customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  private _establecerDataFiltrosDocumentosEnviar() : IDataFiltrosDocumentosEnviar {
    const dataFiltrosRecibos: IDataFiltrosDocumentosEnviar = {
      listaZonaRegistral: this.listaZonaRegistralFiltro, 
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaTipoDocumento: this.listaTipoDocumentoFiltro,
      listaEstadoRecibo: this.listaEstadoReciboFiltro,
      bodyDocumentosEnviar: this.bodyDocumentosEnviar,   
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosRecibos;
  }

  public validarNumero(event:KeyboardEvent): void{
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservableData = this._establecerDataFiltrosDocumentosEnviar(); 
  }

}
