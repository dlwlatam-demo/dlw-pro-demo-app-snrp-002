import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import { IDocumentosEnviar } from '../../../../../interfaces/documentos-enviar.interface';

import { BodyEnviarDocumentosEnviar, ITramaEnviar } from '../../../../../models/mantenimientos/documentos/documentos-enviar.models';
import { IDestinatario } from '../../../../../models/mantenimientos/documentos/documentos-enviar.models';

import { Funciones } from '../../../../../core/helpers/funciones/funciones';

import { DocumentoEnviarService } from '../../../../../services/mantenimientos/documentos/documentos-enviar.service';
import { UsuariosService } from '../../../../../services/seguridad/usuarios.service';
import { UtilService } from '../../../../../services/util.service';

@Component({
  selector: 'app-enviar-documentos-enviar',
  templateUrl: './enviar-documentos-enviar.component.html',
  styleUrls: ['./enviar-documentos-enviar.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class EnviarDocumentosEnviarComponent implements OnInit, OnDestroy {

  unsubscribe$ = new Subject<void>();

  documento!: IDocumentosEnviar[];

  title: string = '';
  motivoObservacion: string = '';
  cantidadMaximaTexto: number = 1000;
  loadingEnviar: boolean = false;
  
  registros: ITramaEnviar[] = [];

  userList: IDestinatario[] = [];
  selectedUser: IDestinatario[] = [];

  bodyEnviarDocumentosEnviar: BodyEnviarDocumentosEnviar;
  
  constructor(
    private funciones: Funciones,
    private config: DynamicDialogConfig,
    private dialogRef: DynamicDialogRef,
    private confirmService: ConfirmationService,
    private messageService: MessageService,
    private utilService: UtilService,
    private usuarioService: UsuariosService,
    private documentoEnviarService: DocumentoEnviarService 
  ) { }

  ngOnInit(): void {
    this.bodyEnviarDocumentosEnviar = new BodyEnviarDocumentosEnviar();

    this.documento = this.config.data.doc as IDocumentosEnviar[];
    this.title = this.config.data.title;
    
    this._getListaUsuarios();
  }

  private _getListaUsuarios(): void {
    this.usuarioService.getUsuariosPorSistema( 0 ).pipe( takeUntil( this.unsubscribe$ ) )
      .subscribe({
        next: ( data ) => {
          let dataUser: IDestinatario = {};

          for ( let d of data ) {

            dataUser = {
              coUsua: String( d.coUsua ),
              noUsua: `${ d.noUsua }`
            }

            this.userList.push( dataUser );
          }
        }
      });
  }

  public solicitarEnviarDocumento(): void {
    if ( this.selectedUser.length < 1 ) {
      this.messageService.add({
        severity: 'warn', 
        summary: 'Atención', 
        detail: 'Debe seleccionar mínimo un usuario',
        sticky: true
      });
      return;
    }

    if ( this.title === 'reenviar' ) {
      if (!this.motivoObservacion || this.funciones.eliminarEspaciosEnBlanco(this.motivoObservacion) === '') {
        this.messageService.add({
          severity: 'warn', 
          summary: 'Atención', 
          detail: 'Ingrese el motivo de la observación',
          sticky: true
        });
        return;
      }
    }

    for ( let user of this.selectedUser ) {
      for ( let doc of this.documento ) {
        this.registros.push({
          idDocuEnvi: doc.idDocuEnvi,
          coPerf: '',
          coUsua: user.coUsua
        });
      }
    }

    this.bodyEnviarDocumentosEnviar.obDocu = this.title === 'reenviar' ? this.motivoObservacion : ''
    this.bodyEnviarDocumentosEnviar.trama = this.registros;

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de enviar el documento a los usuarios seleccionados?',
      accept: () => {
        Swal.showLoading();
        this.loadingEnviar = true;

        this.documentoEnviarService.enviarReenviarDocumento(this.title, this.bodyEnviarDocumentosEnviar).pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: ( data ) => {

            if (data.codResult < 0 ) {
              Swal.close();
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención', 
                detail: data.msgResult,
                sticky: true
              });
              this.loadingEnviar = false;
              
              return;
            }

            this.dialogRef.close('accept');
          },
          error:( d )=>{
            this.utilService.onCloseLoading(); 
            if (d.error.category === "NOT_FOUND") {
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención', 
                detail: d.error.description,
                sticky: true
              });
            } else {
              this.dialogRef.close('reject');
            }
          }
        })
      }
    });
  }
  
  public cancelar(): void {
    this.dialogRef.close('');
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
