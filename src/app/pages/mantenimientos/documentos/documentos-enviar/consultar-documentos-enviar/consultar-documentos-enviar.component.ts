import { IDataFiltrosDocumentosEnviar, IDocumentosEnviar } from 'src/app/interfaces/documentos-enviar.interface';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { UtilService } from 'src/app/services/util.service';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { DocumentoEnviarService } from 'src/app/services/mantenimientos/documentos/documentos-enviar.service';
import { TramaSustento } from '../../../../../models/archivo-adjunto.model';
import { UploadComponent } from '../../../../upload/upload.component';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';

@Component({
  selector: 'app-consultar-documentos-enviar',
  templateUrl: './consultar-documentos-enviar.component.html',
  styleUrls: ['./consultar-documentos-enviar.component.scss']
})
export class ConsultarDocumentosEnviarComponent extends BaseBandeja implements OnInit, OnDestroy {
  loadingDocumentosEnviarData: boolean = false;

  userCode:any;
  fechaActual: Date;
  idEdit!: number;
  msg!: string;
  docuEnviarSustento: TramaSustento[] = [];
  editarDocumentosEnviar!: FormGroup;
  idBancCargo!: number;
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoDocumento: IResponse2[] = [];
  unsubscribe$ = new Subject<void>();

  documentosEnviarData: IDocumentosEnviar;

  @ViewChild(UploadComponent) upload: UploadComponent;

  constructor( 
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private documentoEnviarService: DocumentoEnviarService,
    public funciones: Funciones,
    private utilService: UtilService,
    private sharingInformationService: SharingInformationService,
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Enviar_Documentos;
    this.btnEnviarDocumentos();
    this.userCode = localStorage.getItem('user_code')||'';
    this._getIdByRoute();
    this.fechaActual = new Date();
    this.editarDocumentosEnviar = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoDocumento: ['', [ Validators.required ]],
      fecha: [this.fechaActual, [ Validators.required ]],
      numero: ['', [ Validators.required ]],
      nombreDocumento: ['', [ Validators.required ]],
      nombreRutaDocu: ['', [ Validators.required ]],
      concepto: ['', [ Validators.required ]],
      docs: this.fb.array([])
    });
  }

  private _getIdByRoute(): void {
    this.activatedRoute.params.pipe(takeUntil(this.unsubscribe$))
      .subscribe( params => this._getDocumentosEnviarPagoById(params['id']))
  }

  private _getDocumentosEnviarPagoById(id: string): void {
    this.loadingDocumentosEnviarData = true;
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.documentoEnviarService.getDocumentoEnviarById(id).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.documentosEnviarData = data;
          this._inicilializarEditarCartaOrden();
          this.upload.sustentos = this.docuEnviarSustento;
          this.upload.goEdit();
          this.loadingDocumentosEnviarData = false;
          this.utilService.onCloseLoading(); 
        },
        error: ( err ) => {
          this.loadingDocumentosEnviarData = false;  
          this.utilService.onCloseLoading(); 
        }
      })
  }

  private _inicilializarEditarCartaOrden() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaZonaRegistral.push({ coZonaRegi: this.documentosEnviarData.coZonaRegi, deZonaRegi: this.documentosEnviarData.deZonaRegi });
    this.listaOficinaRegistral.push({ coOficRegi: this.documentosEnviarData.coOficRegi, deOficRegi: this.documentosEnviarData.deOficRegi });
    this.listaLocal.push({ coLocaAten: this.documentosEnviarData.coLocaAten, deLocaAten: this.documentosEnviarData.deLocaAten }); 
    this.listaTipoDocumento.push({ ccodigoHijo: this.documentosEnviarData.tiProc, cdescri: this.documentosEnviarData.deTipoProc }); 
    this.editarDocumentosEnviar.get('numero')?.setValue( this.documentosEnviarData.nuDocu);
    // this.editarDocumentosEnviar.get('nombreDocumento')?.setValue( this.documentosEnviarData.noDocu);
    this.editarDocumentosEnviar.get('concepto')?.setValue( this.documentosEnviarData.obDocu);
    this.editarDocumentosEnviar.patchValue({ "fecha": new Date(this.documentosEnviarData.feDocu)});

    this.docuEnviarSustento = [{
      id: 0,
      noDocuSust: this.documentosEnviarData.noDocu,
      noRutaSust: this.documentosEnviarData.noRutaDocu,
      idGuidDocu: this.documentosEnviarData.idGuidDocu
    }];

    console.log( this.docuEnviarSustento )
    console.log( this.documentosEnviarData )
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  public validarMonto(event:any){
    const pattern = /^(-?\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public formatearNumero(event:any){
    if (event.target.value) {
      this.editarDocumentosEnviar.get('numero')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
    }
  }

  private validarFormulario(): boolean {   
    if (this.editarDocumentosEnviar.value.fecha === "" || this.editarDocumentosEnviar.value.fecha === null) {                    
      document.getElementById('idFecha').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de nuevo documento.");
      return false;      
    } else if (this.editarDocumentosEnviar.value.numero === '' || this.editarDocumentosEnviar.value.numero === null) {
      document.getElementById('numero').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese un número.");
      return false;       
    } else if (this.editarDocumentosEnviar.value.concepto === "" || this.editarDocumentosEnviar.value.concepto === null) {
      document.getElementById('concepto').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese un concepto.");
      return false;       
    } else {
      return true;
    }       
  }

  addUploadedFile(file: any) {
    (this.editarDocumentosEnviar.get('docs') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.editarDocumentosEnviar.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.docuEnviarSustento.findIndex( f => f.id == fileId );
    this.docuEnviarSustento.splice( index, 1 );
  }

  public async cancelar() {
    const dataFiltrosDocumentosEnviar: IDataFiltrosDocumentosEnviar = await this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservable.pipe(first()).toPromise(); 
    if (dataFiltrosDocumentosEnviar.esBusqueda) {
      dataFiltrosDocumentosEnviar.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservableData = dataFiltrosDocumentosEnviar;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/documentos_enviar/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaDocumentosEnviarObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();   
  }

}
