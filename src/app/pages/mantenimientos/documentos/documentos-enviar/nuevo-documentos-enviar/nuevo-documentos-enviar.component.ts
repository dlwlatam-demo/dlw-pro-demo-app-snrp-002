import { IDataFiltrosDocumentosEnviar } from 'src/app/interfaces/documentos-enviar.interface';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { GeneralService } from '../../../../../services/general.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { UtilService } from 'src/app/services/util.service';
import { Dropdown } from 'primeng/dropdown';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { BodyNuevoDocumentosEnviar } from 'src/app/models/mantenimientos/documentos/documentos-enviar.models';
import { DocumentoEnviarService } from 'src/app/services/mantenimientos/documentos/documentos-enviar.service';
import { ArchivoAdjunto } from '../../../../../models/archivo-adjunto.model';
import { environment } from '../../../../../../environments/environment';
import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';

@Component({
  selector: 'app-nuevo-documentos-enviar',
  templateUrl: './nuevo-documentos-enviar.component.html',
  styleUrls: ['./nuevo-documentos-enviar.component.scss']
})
export class NuevoDocumentosEnviarComponent extends BaseBandeja implements OnInit, OnDestroy {
  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;
  @ViewChild('ddlTipoDocumento') ddlTipoDocumento: Dropdown;

  loadingZonaRegistral: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  loadingTipoDocumento: boolean = false;

  $createEnviarDocumento: Subscription = new Subscription;
  userCode:any;
  fechaActual: Date;
  idEdit!: number;
  sizeFiles: number = 0;
  msg!: string;
  files: ArchivoAdjunto[] = [];
  nuevoDocumentosEnviar!: FormGroup;
  idBancCargo!: number;
  bodyNuevoDocumentosEnviar!: BodyNuevoDocumentosEnviar;
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoDocumento: IResponse2[] = [];
  unsubscribe$ = new Subject<void>();

  constructor( 
    private router: Router,
    private fb: FormBuilder,
    private generalService: GeneralService,
    private documentoEnviarService: DocumentoEnviarService,
    public funciones: Funciones,
    private utilService: UtilService,
    private sharingInformationService: SharingInformationService,
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Enviar_Documentos;
    this.btnEnviarDocumentos();
	
    this.bodyNuevoDocumentosEnviar = new BodyNuevoDocumentosEnviar();
    this.userCode = localStorage.getItem('user_code')||'';
    this._buscarZonaRegistral();
    this._buscarCTipoDocumento();
    this.fechaActual = new Date();
    this.nuevoDocumentosEnviar = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoDocumento: ['', [ Validators.required ]],
      fecha: [this.fechaActual, [ Validators.required ]],
      numero: ['', [ Validators.required ]],
      nombreRutaDocu: ['', [ Validators.required ]],
      concepto: ['', [ Validators.required ]],
      docs: this.fb.array([])
    });
    this._inicilializarNuevaCartaOrden();
  }

  private _inicilializarNuevaCartaOrden() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    this.listaTipoDocumento.push({ ccodigoHijo: '', cdescri: '(SELECCIONAR)' }); 
  }

  private _buscarZonaRegistral(): void {
    this.loadingZonaRegistral = true;
    this.generalService.getCbo_Zonas_Usuario(this.userCode).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingZonaRegistral = false;    
        if (data.length === 1) {   
          this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });    
          this.nuevoDocumentosEnviar.patchValue({ "zona": data[0].coZonaRegi});
          this.buscarOficinaRegistral();
        } else {    
          this.listaZonaRegistral.push(...data);
        } 
      },
      error:( err )=>{
        this.loadingZonaRegistral = false;    
      }
    });
  }

  public buscarOficinaRegistral(): void {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.nuevoDocumentosEnviar.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.nuevoDocumentosEnviar.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.nuevoDocumentosEnviar.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal(): void {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.nuevoDocumentosEnviar.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.nuevoDocumentosEnviar.get('zona')?.value,this.nuevoDocumentosEnviar.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.nuevoDocumentosEnviar.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  private _buscarCTipoDocumento(): void {
    this.loadingTipoDocumento = true;
    this.generalService.getCbo_TipoProcesoEnviar().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        if (data.length === 1) {
          this.listaTipoDocumento.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
          this.nuevoDocumentosEnviar.patchValue({ "tipoProceso": data[0].ccodigoHijo});        
        } else {    
          this.listaTipoDocumento.push(...data);
        }
        this.loadingTipoDocumento = false;
      },
      error:( err )=>{
        this.loadingTipoDocumento = false;    
      }
    });
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  public validarMonto(event:any){
    const pattern = /^(-?\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public formatearNumero(event:any){
    if (event.target.value) {
      this.nuevoDocumentosEnviar.get('numero')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
    }
  }

  public grabar() {
    let resultado: boolean = this.validarFormulario();
    if (!resultado) {
      return;
    }

    this.parse();
    console.log('this.files =', this.files);

    if ( this.files.length != 0 ) {
      this.saveAdjuntoInFileServer();
    }
    
  }

  private async guardarDatos() {
    const dataFiltrosDocumentosEnviar: IDataFiltrosDocumentosEnviar = await this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservable.pipe(first()).toPromise(); 
    if (dataFiltrosDocumentosEnviar.esBusqueda) {
      dataFiltrosDocumentosEnviar.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservableData = dataFiltrosDocumentosEnviar;
    }
    this.utilService.onShowProcessLoading("Creando nuevo documento");
    const zona = this.nuevoDocumentosEnviar.get('zona')?.value;
    const oficina = this.nuevoDocumentosEnviar.get('oficina')?.value;
    const local = this.nuevoDocumentosEnviar.get('local')?.value;
    const fecha = this.nuevoDocumentosEnviar.get('fecha')?.value;
    const fechaFormat = this.funciones.convertirFechaDateToString( fecha )
    this.bodyNuevoDocumentosEnviar.coZonaRegi = zona === '*' ? '' : zona;
    this.bodyNuevoDocumentosEnviar.coOficRegi = oficina === '*' ? '' : oficina;
    this.bodyNuevoDocumentosEnviar.coLocaAten = local === '*' ? '' : local;
    this.bodyNuevoDocumentosEnviar.tiProc = this.nuevoDocumentosEnviar.get('tipoDocumento')?.value;
    this.bodyNuevoDocumentosEnviar.feDocu = fechaFormat;
    this.bodyNuevoDocumentosEnviar.nuDocu = this.nuevoDocumentosEnviar.get('numero')?.value;
    // this.bodyNuevoDocumentosEnviar.noDocu = this.nuevoDocumentosEnviar.get('nombreDocumento')?.value;
    this.bodyNuevoDocumentosEnviar.obDocu = this.nuevoDocumentosEnviar.get('concepto')?.value;
    console.log('bodyNuevo', this.bodyNuevoDocumentosEnviar);
    this.$createEnviarDocumento = this.documentoEnviarService.nuevoDocumentoEnviar(this.bodyNuevoDocumentosEnviar).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( d ) => {
          if (d.codResult < 0 ) {
            this.utilService.onShowAlert("info", "Atención", d.msgResult);
          } else if (d.codResult === 0) {
            this.utilService.onCloseLoading();
            this.router.navigate(['SARF/mantenimientos/documentos/documentos_enviar/bandeja']);
          }
        },
        error:( d )=>{
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("error", "Atención", d.error.description);
          }else {
            this.utilService.onShowMessageErrorSystem(); 
          }
        }
      });
  }

  private validarFormulario(): boolean {   
    if (this.nuevoDocumentosEnviar.value.zona === "" || this.nuevoDocumentosEnviar.value.zona === "*" || this.nuevoDocumentosEnviar.value.zona === null) {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una zona registral.");
      return false;  
    } else if (this.nuevoDocumentosEnviar.value.oficina === "" || this.nuevoDocumentosEnviar.value.oficina === "*" || this.nuevoDocumentosEnviar.value.oficina === null) {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una oficina registral.");
      return false;      
    } else if (this.nuevoDocumentosEnviar.value.local === "" || this.nuevoDocumentosEnviar.value.local === "*" || this.nuevoDocumentosEnviar.value.local === null) {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un local.");
      return false;      
    } else if (this.nuevoDocumentosEnviar.value.tipoDocumento === "" || this.nuevoDocumentosEnviar.value.tipoDocumento === "*" || this.nuevoDocumentosEnviar.value.tipoDocumento === null) {      
      this.ddlTipoDocumento.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un tipo de documento.");
      return false;      
    } else if (this.nuevoDocumentosEnviar.value.fecha === "" || this.nuevoDocumentosEnviar.value.fecha === null) {                    
      document.getElementById('idFecha').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de nuevo documento.");
      return false;      
    } else if ( (this.nuevoDocumentosEnviar.get('docs') as FormArray).length == 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe adjuntar un documento.");
      return false;
    } else {
      return true;
    }       
  }

  public async cancelar() {
    const dataFiltrosDocumentosEnviar: IDataFiltrosDocumentosEnviar = await this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservable.pipe(first()).toPromise(); 
    if (dataFiltrosDocumentosEnviar.esBusqueda) {
      dataFiltrosDocumentosEnviar.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservableData = dataFiltrosDocumentosEnviar;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/documentos_enviar/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaDocumentosEnviarObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.$createEnviarDocumento.unsubscribe();
  }

  disableUpload(): boolean {
    return (this.nuevoDocumentosEnviar.get('docs') as FormArray).length == environment.fileLimitAdju;
  }

  addUploadedFile(file: any) {
    (this.nuevoDocumentosEnviar.get('docs') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  deleteUploaded(fileId: string) {
    const ar = <FormArray>this.nuevoDocumentosEnviar.get('docs')
    const index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)
  }

  parse() {
    const docs = (this.nuevoDocumentosEnviar.get('docs') as FormArray)?.controls || [];
	this.files = [];
    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: '999',
          coZonaRegi: this.nuevoDocumentosEnviar.get('zona')?.value,
          coOficRegi: this.nuevoDocumentosEnviar.get('oficina')?.value
        })
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.utilService.onShowAlert("info", "Atención", data.msgResult);
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            this.addAdjunto(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.guardarDatos();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          if (e.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("error", "Atención", e.error.description);
          }else {
            this.utilService.onShowMessageErrorSystem(); 
          }
        }
      });
    }
  }

  addAdjunto(file?: any, guid?: string) {
    if ( file != '' && guid != '' ) {
      this.bodyNuevoDocumentosEnviar.noDocu = file.deDocuAdju;
      this.bodyNuevoDocumentosEnviar.idGuidDocu = guid;
    }
  }

}
