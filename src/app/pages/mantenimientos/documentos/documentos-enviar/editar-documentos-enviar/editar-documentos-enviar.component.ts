import { IDataFiltrosDocumentosEnviar, IDocumentosEnviar } from 'src/app/interfaces/documentos-enviar.interface';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { UtilService } from 'src/app/services/util.service';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { BodyEditarDocumentosEnviar } from 'src/app/models/mantenimientos/documentos/documentos-enviar.models';
import { DocumentoEnviarService } from 'src/app/services/mantenimientos/documentos/documentos-enviar.service';
import { ArchivoAdjunto, TramaSustento } from '../../../../../models/archivo-adjunto.model';
import { UploadComponent } from '../../../../upload/upload.component';
import { environment } from '../../../../../../environments/environment';
import { GeneralService } from '../../../../../services/general.service';
import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';

@Component({
  selector: 'app-editar-documentos-enviar',
  templateUrl: './editar-documentos-enviar.component.html',
  styleUrls: ['./editar-documentos-enviar.component.scss']
})
export class EditarDocumentosEnviarComponent extends BaseBandeja implements OnInit, OnDestroy {
  loadingDocumentosEnviarData: boolean = false;

  userCode:any;
  fechaActual: Date;
  idEdit!: number;
  sizeFiles: number = 0;
  msg!: string;
  files: ArchivoAdjunto[] = [];
  docuEnviarSustento: TramaSustento[] = [];
  editarDocumentosEnviar!: FormGroup;
  idBancCargo!: number;
  bodyEditarDocumentosEnviar!: BodyEditarDocumentosEnviar;
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoDocumento: IResponse2[] = [];
  unsubscribe$ = new Subject<void>();

  documentosEnviarData: IDocumentosEnviar;

  @ViewChild(UploadComponent) upload: UploadComponent;

  constructor( 
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private generalService: GeneralService,
    private documentoEnviarService: DocumentoEnviarService,
    public funciones: Funciones,
    private utilService: UtilService,
    private sharingInformationService: SharingInformationService,
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Enviar_Documentos;
    this.btnEnviarDocumentos();
    this.bodyEditarDocumentosEnviar = new BodyEditarDocumentosEnviar();
    this.userCode = localStorage.getItem('user_code')||'';
    this._getIdByRoute();
    this.fechaActual = new Date();
    this.editarDocumentosEnviar = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoDocumento: ['', [ Validators.required ]],
      fecha: [this.fechaActual, [ Validators.required ]],
      numero: ['', [ Validators.required ]],
      nombreRutaDocu: ['', [ Validators.required ]],
      concepto: ['', []],
      docs: this.fb.array([])
    });
  }

  private _getIdByRoute(): void {
    this.activatedRoute.params.pipe(takeUntil(this.unsubscribe$))
      .subscribe( params => this._getDocumentosEnviarPagoById(params['id']))
  }

  private _getDocumentosEnviarPagoById(id: string): void {
    this.loadingDocumentosEnviarData = true;
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.documentoEnviarService.getDocumentoEnviarById(id).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          console.log('editData', data)
          this.documentosEnviarData = data;
          this._inicilializarEditarCartaOrden();
          this.upload.sustentos = this.docuEnviarSustento;
          this.upload.goEdit();
          this.loadingDocumentosEnviarData = false;
          this.utilService.onCloseLoading(); 
        },
        error: ( err ) => {
          this.loadingDocumentosEnviarData = false;  
          this.utilService.onCloseLoading(); 
        }
      })
  }

  private _inicilializarEditarCartaOrden() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaZonaRegistral.push({ coZonaRegi: this.documentosEnviarData.coZonaRegi, deZonaRegi: this.documentosEnviarData.deZonaRegi });
    this.listaOficinaRegistral.push({ coOficRegi: this.documentosEnviarData.coOficRegi, deOficRegi: this.documentosEnviarData.deOficRegi });
    this.listaLocal.push({ coLocaAten: this.documentosEnviarData.coLocaAten, deLocaAten: this.documentosEnviarData.deLocaAten }); 
    this.listaTipoDocumento.push({ ccodigoHijo: this.documentosEnviarData.tiProc, cdescri: this.documentosEnviarData.deTipoProc }); 
    this.editarDocumentosEnviar.get('numero')?.setValue( this.documentosEnviarData.nuDocu);
    // this.editarDocumentosEnviar.get('nombreDocumento')?.setValue( this.documentosEnviarData.noDocu);
    this.editarDocumentosEnviar.get('concepto')?.setValue( this.documentosEnviarData.obDocu);
    this.editarDocumentosEnviar.patchValue({ "fecha": new Date(this.documentosEnviarData.feDocu)});

    this.docuEnviarSustento = [{
      id: 0,
      noDocuSust: this.documentosEnviarData.noDocu,
      noRutaSust: this.documentosEnviarData.noRutaDocu,
      idGuidDocu: this.documentosEnviarData.idGuidDocu
    }];

    this.bodyEditarDocumentosEnviar.noDocu = this.documentosEnviarData.noDocu;
    this.bodyEditarDocumentosEnviar.idGuidDocu = this.documentosEnviarData.idGuidDocu;
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  public validarMonto(event:any){
    const pattern = /^(-?\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public formatearNumero(event:any){
    if (event.target.value) {
      this.editarDocumentosEnviar.get('numero')?.setValue(Number.parseFloat(event.target.value).toFixed(2));
    }
  }

  public grabar() {
    let resultado: boolean = this.validarFormulario();
    if (!resultado) {
      return;
    }

    this.parse();
    console.log('this.files =', this.files);

    if ( this.files.length != 0 ) {
      this.saveAdjuntoInFileServer();
    }
    else {
      this.guardarDatosEdit();
    }
    
  }

  private async guardarDatosEdit() {
    const dataFiltrosDocumentosEnviar: IDataFiltrosDocumentosEnviar = await this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservable.pipe(first()).toPromise(); 
    if (dataFiltrosDocumentosEnviar.esBusqueda) {
      dataFiltrosDocumentosEnviar.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservableData = dataFiltrosDocumentosEnviar;
    }
    this.utilService.onShowProcessLoading("Creando nuevo documento");
    const fecha = this.editarDocumentosEnviar.get('fecha')?.value;
    const fechaFormat = this.funciones.convertirFechaDateToString( fecha );
    this.bodyEditarDocumentosEnviar.idDocuEnvi = this.documentosEnviarData.idDocuEnvi;
    this.bodyEditarDocumentosEnviar.coZonaRegi = this.documentosEnviarData.coZonaRegi;
    this.bodyEditarDocumentosEnviar.coOficRegi = this.documentosEnviarData.coOficRegi;
    this.bodyEditarDocumentosEnviar.coLocaAten = this.documentosEnviarData.coLocaAten;
    this.bodyEditarDocumentosEnviar.tiProc = this.documentosEnviarData.tiProc;
    this.bodyEditarDocumentosEnviar.feDocu = fechaFormat;
    this.bodyEditarDocumentosEnviar.nuDocu = this.editarDocumentosEnviar.get('numero')?.value;
    // this.bodyEditarDocumentosEnviar.noDocu = this.editarDocumentosEnviar.get('nombreDocumento')?.value;
    this.bodyEditarDocumentosEnviar.obDocu = this.editarDocumentosEnviar.get('concepto')?.value;
    console.log('dataEdit', this.bodyEditarDocumentosEnviar);
    this.documentoEnviarService.editarDocumentoEnviar(this.bodyEditarDocumentosEnviar).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( d ) => {
          if (d.codResult < 0 ) {
            this.utilService.onShowAlert("info", "Atención", d.msgResult);
          } else if (d.codResult === 0) {
            this.utilService.onCloseLoading();
            this.router.navigate(['SARF/mantenimientos/documentos/documentos_enviar/bandeja']);
          }
        },
        error:( d )=>{
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("error", "Atención", d.error.description);
          }else {
            this.utilService.onShowMessageErrorSystem(); 
          }
        }
      });
  }

  private validarFormulario(): boolean {   
    if (this.editarDocumentosEnviar.value.fecha === "" || this.editarDocumentosEnviar.value.fecha === null) {                    
      document.getElementById('idFecha').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione la fecha de nuevo documento.");
      return false;      
    } else if (this.editarDocumentosEnviar.value.numero === '' || this.editarDocumentosEnviar.value.numero === null) {
      document.getElementById('numero').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese un número.");
      return false;       
    } else if ( (this.editarDocumentosEnviar.get('docs') as FormArray).length == 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe adjuntar un documento.");
      return false;
    } else {
      return true;
    }       
  }

  public async cancelar() {
    const dataFiltrosDocumentosEnviar: IDataFiltrosDocumentosEnviar = await this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservable.pipe(first()).toPromise(); 
    if (dataFiltrosDocumentosEnviar.esBusqueda) {
      dataFiltrosDocumentosEnviar.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosDocumentosEnviarObservableData = dataFiltrosDocumentosEnviar;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/documentos_enviar/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaDocumentosEnviarObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();   
  }

  disableUpload(): boolean {
    return (this.editarDocumentosEnviar.get('docs') as FormArray).length == environment.fileLimitAdju;
  }

  addUploadedFile(file: any) {
    (this.editarDocumentosEnviar.get('docs') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.editarDocumentosEnviar.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.docuEnviarSustento.findIndex( f => f.id == fileId );
    this.docuEnviarSustento.splice( index, 1 );
  }

  parse() {
    const docs = (this.editarDocumentosEnviar.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( idAdjunto != 0 )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: '999',
          coZonaRegi: this.documentosEnviarData.coZonaRegi,
          coOficRegi: this.documentosEnviarData.coOficRegi
        })
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.utilService.onShowAlert("info", "Atención", data.msgResult);
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            this.addAdjunto(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.guardarDatosEdit();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          if (e.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("error", "Atención", e.error.description);
          }else {
            this.utilService.onShowMessageErrorSystem(); 
          }
        }
      });
    }
  }

  addAdjunto(file?: any, guid?: string) {
    if ( file != '' && guid != '' ) {
      this.bodyEditarDocumentosEnviar.noDocu = file.deDocuAdju;
      this.bodyEditarDocumentosEnviar.idGuidDocu = guid;
    }
  }

}
