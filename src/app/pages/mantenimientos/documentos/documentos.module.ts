import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { DocumentosRoutingModule } from './documentos-routing.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';
import { PagesModule } from '../../pages.module';

import { SolicitarFirmaComprobanteComponent } from './comprobante-pago/solicitar-firma-comprobante/solicitar-firma-comprobante.component';
import { SolicitarFirmaConstanciaComponent } from './constancia-abono/solicitar-firma-constancia/solicitar-firma-constancia.component';
import { SustentosConstanciaComponent } from './constancia-abono/sustentos-constancia/sustentos-constancia.component';
import { SustentosDocumentoComponent } from './documento-firma/sustentos-documento/sustentos-documento.component';
import { SustentosComprobanteComponent } from './comprobante-pago/sustentos-comprobante/sustentos-comprobante.component';
import { BandejaComprobanteComponent } from './comprobante-pago/bandeja-comprobante/bandeja-comprobante.component';
import { DatosComprobanteComponent } from './comprobante-pago/datos-comprobante/datos-comprobante.component';
import { GenerarChequesComponent } from './comprobante-pago/generar-cheques/generar-cheques.component';
import { HistorialFirmasComponent } from './comprobante-pago/historial-firmas/historial-firmas.component';
import { NoUtilizadoComponent } from './comprobante-pago/no-utilizado/no-utilizado.component';
import { NuevoComprobanteComponent } from './comprobante-pago/nuevo-comprobante/nuevo-comprobante.component';
import { ReservarComponent } from './comprobante-pago/reservar/reservar.component';
import { BandejaConstanciaComponent } from './constancia-abono/bandeja-constancia/bandeja-constancia.component';
import { BandejaDocumentoComponent } from './documento-firma/bandeja-documento/bandeja-documento.component';
import { HistorialFirmaComponent } from './documento-firma/historial-firma/historial-firma.component';
import { FirmaHistorialComponent } from './constancia-abono/firma-historial/firma-historial.component';
import { BandejaReciboComponent } from './recibo-ingreso/bandeja-recibo/bandeja-recibo.component';
import { NuevoReciboComponent } from './recibo-ingreso/nuevo-recibo/nuevo-recibo.component';
import { SustentosReciboComponent } from './recibo-ingreso/sustentos-recibo/sustentos-recibo.component';
import { SolicitarFirmaReciboComponent } from './recibo-ingreso/solicitar-firma-recibo/solicitar-firma-recibo.component';
import { ReservarReciboComponent } from './recibo-ingreso/reservar-recibo/reservar-recibo.component';
import { NoUtilizadoReciboComponent } from './recibo-ingreso/no-utilizado-recibo/no-utilizado-recibo.component';
import { HistorialFirmasReciboComponent } from './recibo-ingreso/historial-firmas-recibo/historial-firmas-recibo.component';
import { CopiarComprobanteComponent } from './comprobante-pago/copiar-comprobante/copiar-comprobante.component';
import { BandejaCartaComponent } from './carta-orden/bandeja-carta/bandeja-carta.component';
import { NuevaCartaComponent } from './carta-orden/nueva-carta/nueva-carta.component';
import { CartaSustentosComponent } from './carta-orden/carta-sustentos/carta-sustentos.component';
import { CartaAnexosComponent } from './carta-orden/carta-anexos/carta-anexos.component';
import { CartaSolicitarFirmaComponent } from './carta-orden/carta-solicitar-firma/carta-solicitar-firma.component';
import { CartaHistorialFirmaComponent } from './carta-orden/carta-historial-firma/carta-historial-firma.component';
import { BandejaMemoComponent } from './memorandum/bandeja-memo/bandeja-memo.component';
import { MemoNuevoComponent } from './memorandum/memo-nuevo/memo-nuevo.component';
import { MemoSustentosComponent } from './memorandum/memo-sustentos/memo-sustentos.component';
import { ChequesComponent } from './cheques/cheques.component';
import { EditarCartaComponent } from './carta-orden/editar-carta/editar-carta.component';
import { EnviarCorreoComponent } from './constancia-abono/enviar-correo/enviar-correo.component';
import { CopiarReciboComponent } from './recibo-ingreso/copiar-recibo/copiar-recibo.component';
import { EditarComprobanteComponent } from './comprobante-pago/editar-comprobante/editar-comprobante.component';
import { ConsultarComprobanteComponent } from './comprobante-pago/consultar-comprobante/consultar-comprobante.component';
import { ConsultarCartaComponent } from './carta-orden/consultar-carta/consultar-carta.component';
import { FirmarDocumentoComponent } from './documento-firma/firmar-documento/firmar-documento.component';
import { BandejaDocumentosVariosComponent } from './documentos-varios-solicitar-firma/bandeja-documentos-varios/bandeja-documentos-varios.component';
import { NuevoDocumentosVariosComponent } from './documentos-varios-solicitar-firma/nuevo-documentos-varios/nuevo-documentos-varios.component';
import { EditarDocumentosVariosComponent } from './documentos-varios-solicitar-firma/editar-documentos-varios/editar-documentos-varios.component';
import { ConsultarDocumentosVariosComponent } from './documentos-varios-solicitar-firma/consultar-documentos-varios/consultar-documentos-varios.component';
import { SolicitarFirmasDocumentosVariosComponent } from './documentos-varios-solicitar-firma/solicitar-firmas-documentos-varios/solicitar-firmas-documentos-varios.component';
import { HistorialFirmaDocumentosVariosComponent } from './documentos-varios-solicitar-firma/historial-firma-documentos-varios/historial-firma-documentos-varios.component';
import { SustentosDocumentosVariosComponent } from './documentos-varios-solicitar-firma/sustentos-documentos-varios/sustentos-documentos-varios.component';

import { BandejaDocumentosEnviarComponent } from './documentos-enviar/bandeja-documentos-enviar/bandeja-documentos-enviar.component';
import { NuevoDocumentosEnviarComponent } from './documentos-enviar/nuevo-documentos-enviar/nuevo-documentos-enviar.component';
import { EditarDocumentosEnviarComponent } from './documentos-enviar/editar-documentos-enviar/editar-documentos-enviar.component';
import { ConsultarDocumentosEnviarComponent } from './documentos-enviar/consultar-documentos-enviar/consultar-documentos-enviar.component';
import { EnviarDocumentosEnviarComponent } from './documentos-enviar/enviar-documentos-enviar/enviar-documentos-enviar.component';
import { SustentosDocumentosEnviarComponent } from './documentos-enviar/sustentos-documentos-enviar/sustentos-documentos-enviar.component';
import { SubirDocumentoComponent } from './documento-firma/subir-documento/subir-documento.component';
import { CorreoEnviarComponent } from './carta-orden/correo-enviar/correo-enviar.component';
import { VentTipoOperacionComponent } from './comprobante-pago/vent-tipo-operacion/vent-tipo-operacion.component';
import { VentBeneficiariosComponent } from './comprobante-pago/vent-beneficiarios/vent-beneficiarios.component';
import { VentanaNombresComponent } from './recibo-ingreso/ventana-nombres/ventana-nombres.component';



@NgModule({
  declarations: [
    BandejaComprobanteComponent,
    DatosComprobanteComponent,
    GenerarChequesComponent,
    HistorialFirmasComponent,
    NoUtilizadoComponent,
    NuevoComprobanteComponent,
    EditarComprobanteComponent,
    ConsultarComprobanteComponent,
    ReservarComponent,
    SolicitarFirmaComprobanteComponent,
    SustentosComprobanteComponent,
    BandejaConstanciaComponent,
    SolicitarFirmaConstanciaComponent,
    SustentosConstanciaComponent,
    BandejaDocumentoComponent,
    BandejaDocumentosVariosComponent,
    NuevoDocumentosVariosComponent,
    EditarDocumentosVariosComponent,
    ConsultarDocumentosVariosComponent,
    SolicitarFirmasDocumentosVariosComponent,
    HistorialFirmaDocumentosVariosComponent,
    HistorialFirmaComponent,
    BandejaDocumentosEnviarComponent,
    NuevoDocumentosEnviarComponent,
    EditarDocumentosEnviarComponent,
    ConsultarDocumentosEnviarComponent,
    EnviarDocumentosEnviarComponent,

    SustentosDocumentoComponent,
    FirmaHistorialComponent,
    BandejaReciboComponent,
    NuevoReciboComponent,
    SustentosReciboComponent,
    SolicitarFirmaReciboComponent,
    ReservarReciboComponent,
    NoUtilizadoReciboComponent,
    HistorialFirmasReciboComponent,
    CopiarComprobanteComponent,
    BandejaCartaComponent,
    NuevaCartaComponent,
    CartaSustentosComponent,
    CartaAnexosComponent,
    CartaSolicitarFirmaComponent,
    CartaHistorialFirmaComponent,
    BandejaMemoComponent,
    MemoNuevoComponent,
    MemoSustentosComponent,
    ChequesComponent,
    EditarCartaComponent,
    ConsultarCartaComponent,
    EnviarCorreoComponent,
    CopiarReciboComponent,
    FirmarDocumentoComponent,
    SustentosDocumentosVariosComponent,
    SustentosDocumentosEnviarComponent,
    SubirDocumentoComponent,
    CorreoEnviarComponent,
    VentTipoOperacionComponent,
    VentBeneficiariosComponent,
    VentanaNombresComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DocumentosRoutingModule,
    NgPrimeModule,
    PagesModule
  ]
})
export class DocumentosModule { }
