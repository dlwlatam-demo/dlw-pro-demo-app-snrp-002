import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { Observable, Subject, takeUntil } from 'rxjs';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { FirmarDocumentosValue } from '../../../../../interfaces/firmar-documentos.interface';
import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';

import { ArchivoAdjunto, TramaSustento } from '../../../../../models/archivo-adjunto.model';
import { BodyFirmaMasivo } from '../../../../../models/mantenimientos/documentos/documento-firma.model';

import { GeneralService } from '../../../../../services/general.service';
import { DocumentoFirmaService } from '../../../../../services/mantenimientos/documentos/documento-firma.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';

import { UploadComponent } from '../../../../upload/upload.component';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-subir-documento',
  templateUrl: './subir-documento.component.html',
  styleUrls: ['./subir-documento.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class SubirDocumentoComponent implements OnInit, OnDestroy {

  coZonaRegi!: string;
  coOficRegi!: string;
  coTipoAdju!: string;
  isLoading!: boolean;
  sizeFiles: number = 0;

  datos!: FormGroup;

  $zonas!: Observable<any>;
  $oficinas!: Observable<any>;
  $locales!: Observable<any>;
  $tipoDocumento!: Observable<any>;

  add!: number;
  delete!: number;
  bandeja!: number;

  files: ArchivoAdjunto[] = [];

  documento!: FirmarDocumentosValue[];
  documentoSustento: TramaSustento[] = [];

  bodyFirmaMasivo!: BodyFirmaMasivo;

  unsubscribe$ = new Subject<void>();

  @ViewChild(UploadComponent) upload: UploadComponent;

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private confirmService: ConfirmationService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private validarService: ValidatorsService,
    private documentoService: DocumentoFirmaService
  ) { }

  ngOnInit(): void {

    this.bodyFirmaMasivo = new BodyFirmaMasivo();

    this.$zonas = this.generalService.getCbo_Zonas_Usuario('');
    this.$tipoDocumento = this.generalService.getCbo_Documento();

    this.documento = this.config.data.doc as FirmarDocumentosValue[];

    this.add = this.config.data.add;
    this.delete = this.config.data.delete;
    this.bandeja = this.config.data.bandeja;

    this.datos = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoDocu: ['', [ Validators.required ]],
      nroDocu: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      docs: this.fb.array([])
    });

    this.coZonaRegi = this.documento[0].coZonaRegi;
    this.coOficRegi = this.documento[0].coOficRegi;
    this.coTipoAdju = this.documento[0].tiDocu;

    this.hidrate( this.documento[0] );
  }

  hidrate( d: FirmarDocumentosValue ) {
    const fecDocu = this.validarService.reFormateaFecha( d.feDocu );
    
    this.datos.get('zona')?.setValue( d.coZonaRegi );

    this.$oficinas = this.generalService.getCbo_Oficinas_Zonas('', d.coZonaRegi);
    this.datos.get('oficina')?.setValue( d.coOficRegi );
    
    this.$locales = this.generalService.getCbo_Locales_Ofic('', d.coZonaRegi, d.coOficRegi);
    this.datos.get('local')?.setValue( d.coLocaAten );

    this.datos.get('tipoDocu')?.setValue( d.tiDocu );
    this.datos.get('nroDocu')?.setValue( d.nuDocuOrig );
    this.datos.get('fecha')?.setValue( fecDocu );
  }

  get disableUpload() {
    return (this.datos.get('docs') as FormArray).length == environment.fileLimitAdju;
  }

  addUploadedFile(file: any) {
    (this.datos.get('docs') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.datos.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)
  }

  parse() {
    const docs = (this.datos.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  grabar() {
    this.bodyFirmaMasivo.trama = this.documento.map( d => { return String( d.idDocuFirm ) });

    this.documentoService.validarDocumentoFirmaMasivo( this.bodyFirmaMasivo ).pipe( takeUntil( this.unsubscribe$ ) )
        .subscribe({
          next: ( data ) => {

            if ( data.codResult < 0 ) {
              this.messageService.add({
                severity: 'warn',
                summary: 'Atención',
                detail: data.msgResult,
                sticky: true
              });

              return;
            }
            else if ( data.codResult === 0 ) {
              this.parse();
              console.log('this.files =', this.files);

              if ( this.files.length === 0 ) {
                this.messageService.add({
                  severity: 'warn',
                  summary: 'Debe adjuntar el documento firmado',
                  sticky: true
                });

                return;
              }

              this.confirmService.confirm({
                message: '¿Está Ud. seguro de grabar los sustentos cargados?',
                accept: () => {
                  this.isLoading = true;

                  if ( this.files.length != 0 ) {
                    this.saveAdjuntoInFileServer();
                  }
                },
                reject: () => {
                  this.files = [];
                }
              });
            }
          },
          error: () => {
            this.messageService.add({
              severity: 'warn',
              summary: 'Atención',
              detail: 'El servicio no esta disponible en estos momentos, inténtelo mas tarde.',
              sticky: true
            });
          }
        });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.messageService.add({
              severity: 'warn',
              summary: 'Atención',
              detail: data.msgResult,
              sticky: true
            });

            this.files = [];
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            // this.addSustento(file, );
            
            if ( this.sizeFiles == 0 ) {
              this.saveSustentoInBD( data.idCarga );
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          this.files = [];
          this.ref.close('reject');
        }
      });
    }
  }

  saveSustentoInBD( idGuid: string ) {
    console.log('idGuid', idGuid);

    this.documentoService.subirDocumentoFirmado( this.documento[0].idDocuFirm, idGuid, '1').subscribe({
      next: ( data ) => {
        console.log('data', data);

        if ( data.codResult < 0 ) {
          this.messageService.add({
            severity: 'warn',
            summary: 'Atención',
            detail: data.msgResult,
            sticky: true
          });

          this.generalService.eliminarAdjunto( idGuid, '', '' ).subscribe({
            next: ( data ) => {
              console.log('dataElimi', data);
            }
          });

          this.files = [];
          return;
        }

        this.ref.close('accept');
      },
      error: ( _e ) => {
        console.log( _e );

        this.files = [];
        this.ref.close('reject');
      },
      complete: () => {
        this.files = [];
        this.isLoading = false;
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
