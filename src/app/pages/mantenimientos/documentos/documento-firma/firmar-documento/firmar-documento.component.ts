import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject, takeUntil } from 'rxjs';

import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';

import { FirmarDocumentosValue } from '../../../../../interfaces/firmar-documentos.interface';

import { BodyFirmaBeanList, BodyFirmaMasivo } from '../../../../../models/mantenimientos/documentos/documento-firma.model';

import { GeneralService } from '../../../../../services/general.service';
import { DocumentoFirmaService } from '../../../../../services/mantenimientos/documentos/documento-firma.service';

@Component({
  selector: 'app-firmar-documento',
  templateUrl: './firmar-documento.component.html',
  styleUrls: ['./firmar-documento.component.scss'],
  providers: [
    MessageService
  ]
})
export class FirmarDocumentoComponent implements OnInit, OnDestroy {

  $zonas!: Observable<any>;
  $oficinas!: Observable<any>;
  $locales!: Observable<any>;
  $tipoDocumento!: Observable<any>;

  roles?: string[];
  perfil?: string;
  idDocuFirm: number = 0;
  isLoading: boolean = false;
  
  documentos!: FirmarDocumentosValue[];
  
  bodyFirmarDocumento!: BodyFirmaBeanList;
  bodyFirmaMasivo!: BodyFirmaMasivo;

  unsubscribe$ = new Subject<void>();
  
  constructor(
    private config: DynamicDialogConfig,
    private ref: DynamicDialogRef,
    private messageService: MessageService,
    private generalService: GeneralService,
    private documentoService: DocumentoFirmaService
  ) { }

  ngOnInit(): void {
    this.bodyFirmarDocumento = new BodyFirmaBeanList();
    this.bodyFirmaMasivo = new BodyFirmaMasivo();

	  this.$zonas = this.generalService.getCbo_Zonas_Usuario('');
    this.$tipoDocumento = this.generalService.getCbo_Documento();
	
	  this.documentos = this.config.data.csta as FirmarDocumentosValue[];
  }

  grabar() {
    this.isLoading = true;

    this.bodyFirmaMasivo.trama = this.documentos.map( s => { return String( s.idDocuFirm )  });

    this.documentoService.validarDocumentoFirmaMasivo( this.bodyFirmaMasivo ).pipe( takeUntil( this.unsubscribe$ ) )
        .subscribe({
          next: ( data ) => {
            if ( data.codResult === 0 ) {
              this.firmar();
            }
            else if ( data.codResult < 0 ) {
              this.messageService.add({
                severity: 'warn',
                summary: 'Atención',
                detail: data.msgResult,
                sticky: true
              });

              this.isLoading = false;
              return;
            }
          },
          error: () => {
            this.messageService.add({
              severity: 'warn',
              summary: 'Atención',
              detail: 'El servicio no esta disponible en estos momentos, inténtelo mas tarde.',
              sticky: true
            });

            this.isLoading = false;
          }
        });
  }
  
  firmar() {

    this.bodyFirmarDocumento.firmaBeanList = this.documentos.map( doc => { 
      return { guid: doc.idGuidDocu, idDocuFirm: doc.idDocuFirm } 
    });

    this.generalService.subeRedis( this.bodyFirmarDocumento ).subscribe({
      next: ( data ) => {
        console.log( data )
        let guid = data.idCarga;
        
        // se continua solo si hay exito en carga archivo a redis:
        let token = localStorage.getItem("t_sarf") || '';
        let jti = localStorage.getItem("t_jsarf") || '';
          
        let mapFormView = document.createElement("form");
          
        mapFormView.method = "POST";
        mapFormView.action = `https://sarf-digital-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe/sarfsunarp/api/v1/firmar?guid=${guid}&token=${token}&jti=${jti}&idDocuFirm=0`;
        document.body.appendChild(mapFormView);
        mapFormView.submit();
		
      },
        error: ( e ) => {
        console.log(e);
        this.messageService.add({
          severity: 'error',
          summary: 'Atención',
          detail: e.error.description,
          sticky: true
        });

        this.isLoading = false;
	    },
      complete: () => {
        this.isLoading = false;
      }
    });

  }

  cancelar() {
    this.ref.close();
    sessionStorage.removeItem('bodyDocFirma');
  }
  
  verDocumento( idGuidDocu: string ) {
  
    console.log('Visor PDF: ' + idGuidDocu);

    const download = document.createElement('a');
    download.href =  this.generalService.downloadManager( idGuidDocu );
    download.setAttribute("target", "_blank");
    document.body.appendChild( download );
    download.click();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  
}
