import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, Subject, Subscription, BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService } from 'primeng/api';

import { BodyDocumentoFirma, DocumentoFirmar, BodyFirmaMasivo } from '../../../../../models/mantenimientos/documentos/documento-firma.model';

import { GeneralService } from '../../../../../services/general.service';
import { DocumentoFirmaService } from '../../../../../services/mantenimientos/documentos/documento-firma.service';

import { HistorialFirmaComponent } from '../historial-firma/historial-firma.component';
import { SustentosDocumentoComponent } from '../sustentos-documento/sustentos-documento.component';
import { FirmarDocumentoComponent } from '../firmar-documento/firmar-documento.component';
import { environment } from '../../../../../../environments/environment';
import Swal from 'sweetalert2';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { UtilService } from 'src/app/services/util.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { PerfilesService } from 'src/app/services/seguridad/perfiles.service';
import { IPerfil } from 'src/app/interfaces/perfiles.interface';
import { FirmarDocumentosValue, IFirmarDocumentos, IDataFiltrosDocumentoFirma } from '../../../../../interfaces/firmar-documentos.interface';
import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { SharingInformationService } from '../../../../../core/services/sharing-services.service';
import { SubirDocumentoComponent } from '../subir-documento/subir-documento.component';

@Component({
  selector: 'app-bandeja-documento',
  templateUrl: './bandeja-documento.component.html',
  styleUrls: ['./bandeja-documento.component.scss'],
  providers: [
    DialogService,
	MessageService
  ]
})
export class BandejaDocumentoComponent extends BaseBandeja implements OnInit, OnDestroy {
  userCode:any
  loadingFirmarDocumentos: boolean = false;
  loadingZonaRegistral: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  loadingTipoDocumento: boolean = false;
  loadingPerfil: boolean = false;
  loadingEstado: boolean = false;

  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoDocumento: IResponse2[];
  listaPerfil: IPerfil[];
  listaEstado: IResponse2[] = [];

  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaTipoDocumentoFiltro: IResponse2[];
  listaPerfilFiltro: IPerfil[];
  listaEstadoFiltro: IResponse2[];

  unsubscribe$ = new Subject<void>();
  esBusqueda: boolean = false;
  fechaMinima: Date;
  fechaMaxima: Date; 
  bodyDocumentoFirma: BodyDocumentoFirma;
  bodyFirmaMasivo: BodyFirmaMasivo;
  searchDocumento!: FormGroup;
  documentosList: FirmarDocumentosValue[];
  enviadosList!: DocumentoFirmar[];
  override selectedValues!: FirmarDocumentosValue[];

  currentPage: string = environment.currentPage;
  
  guidDocumento: string = "";
  
  constructor( 
    private fb: FormBuilder,
    private dialogService: DialogService,
    private generalService: GeneralService,
    private perfilesService: PerfilesService,
    private utilService: UtilService,
    private funciones: Funciones,
    private messageService: MessageService,
    private documentoService: DocumentoFirmaService,
    private sharingInformationService: SharingInformationService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Documentos_para_firmar;
    this.btnDocumentosParaFirmar();
    
    const coPerf = localStorage.getItem('perfil_sarf');
    this.bodyDocumentoFirma = new BodyDocumentoFirma();
    this.bodyFirmaMasivo = new BodyFirmaMasivo();
    this.userCode = localStorage.getItem('user_code')||'';
    this.fechaMaxima = new Date();
    this.fechaMinima = new Date(this.fechaMaxima.getFullYear(), this.fechaMaxima.getMonth(), 1);
    this.searchDocumento = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoDocu: ['', [ Validators.required ]],
      nroDocu: [''],
      fecDes: [this.fechaMinima],
      fecHas: [this.fechaMaxima],
      perfil: [coPerf, [ Validators.required ]],
      estado: ['002', [ Validators.required ]],
      usuario: ['', []]
    });

    this._dataFiltrosDocumentoFirma();
  }

  private _inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaPerfil = [];
    this.listaEstado = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    this.listaTipoDocumento.push({  ccodigoHijo: '*', cdescri: '(TODOS)' }); 
    this.listaPerfil.push({  coPerf: 0, noPerf: '(TODOS)' }); 
    this.listaEstado.push({  ccodigoHijo: '*', cdescri: '(TODOS)' }); 
  }

  private _buscarZonaRegistral(): void {
    this.loadingZonaRegistral = true;
    this.generalService.getCbo_Zonas_Usuario(this.userCode).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingZonaRegistral = false;    
        if (data.length === 1) {   
          this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });    
          this.searchDocumento.patchValue({ "zona": data[0].coZonaRegi});
          this.buscarOficinaRegistral();
        } else {    
          this.listaZonaRegistral.push(...data);
        }        
        // this.buscarDocumento(false);  
      },
      error:( err )=>{
        this.loadingZonaRegistral = false;    
      }
    });
  }

  public buscarOficinaRegistral(): void {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.selectedValues = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.searchDocumento.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.searchDocumento.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.searchDocumento.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' }); 
    if (this.searchDocumento.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.searchDocumento.get('zona')?.value,this.searchDocumento.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.searchDocumento.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  private _buscarTipoDocumento() {
    this.loadingTipoDocumento = true;
    this.generalService.getCbo_TipoDocumentoFirmas().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingTipoDocumento = false;
        this.listaTipoDocumento.push(...data); 
      },
      error:( )=>{
        this.loadingTipoDocumento = false;    
      }
    });
  }

  private _buscarPerfiles() {
    this.loadingPerfil = true;
    const perfil = localStorage.getItem('perfil_sarf');

    this.perfilesService.getPerfiles().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingPerfil = false;
        const index = data.findIndex( ( i: any ) => i.coPerf == perfil );

        this.listaPerfil.push(...data);
        this.searchDocumento.patchValue({ "perfil": data[index].coPerf });
      },
      error:( )=>{
        this.loadingPerfil = false;    
      }
    });
  }

  public cambioFechaInicio() {   
    if (this.searchDocumento.get('fecHas')?.value !== '' && this.searchDocumento.get('fecHas')?.value !== null) {
      if (this.searchDocumento.get('fecDes')?.value > this.searchDocumento.get('fecHas')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.searchDocumento.controls['fecDes'].reset();      
      }      
    }
  }

  public cambioFechaFin() {    
    if (this.searchDocumento.get('fecDes')?.value !== '' && this.searchDocumento.get('fecDes')?.value !== null) {
      if (this.searchDocumento.get('fecHas')?.value < this.searchDocumento.get('fecDes')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.searchDocumento.controls['fecHas'].reset();    
      }      
    }
  }

  private _buscarEstado() {
    this.loadingEstado = true;
    this.generalService.getCbo_EstadoDocumento().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingEstado = false;

        const index = data.findIndex( (i: any) => i.ccodigoHijo == '002' );

        this.listaEstado.push(...data);
        this.searchDocumento.patchValue({'estado': data[index].ccodigoHijo})
      },
      error:( err )=>{
        this.loadingEstado = false;    
      }
    });
  }

  private _dataFiltrosDocumentoFirma(): void {
    const body = JSON.parse( sessionStorage.getItem('bodyDocFirma') );

    this.sharingInformationService.compartirDataFiltrosDocumentoFirmaObservable.pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: IDataFiltrosDocumentoFirma) => {  
      if (data.esCancelar && data.bodyDocumentoFirma !== null) {  
        this.utilService.onShowProcessLoading("Cargando la data");
        this._setearCamposFiltro( data );
        this.buscarDocumento(false);
      } else {
        this._inicilializarListas();
        this._buscarZonaRegistral();
        this._buscarTipoDocumento();
        this._buscarPerfiles();
        this._buscarEstado();
      }
    });

    if ( !body ) {
      this.buscarDocumento(true);
    }else {
      this._setearCamposFiltro( body );
      this.buscarDocumento(false);
    }
  }

  private _setearCamposFiltro(dataFiltro: IDataFiltrosDocumentoFirma) {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoDocumento = [];
    this.listaEstado = [];
    this.listaZonaRegistral = dataFiltro.listaZonaRegistral; 
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaTipoDocumento = dataFiltro.listaTipoDocumento;
    this.listaPerfil = dataFiltro.listaPerfil;
    this.listaEstado = dataFiltro.listaEstado;
    this.bodyDocumentoFirma = dataFiltro.bodyDocumentoFirma;
    this.searchDocumento.patchValue({ "zona": this.bodyDocumentoFirma.coZonaRegi});
    this.searchDocumento.patchValue({ "oficina": this.bodyDocumentoFirma.coOficRegi});
    this.searchDocumento.patchValue({ "local": this.bodyDocumentoFirma.coLocaAten});
    this.searchDocumento.patchValue({ "tipoDocu": this.bodyDocumentoFirma.tiDocu});
    this.searchDocumento.patchValue({ "fecDes": (this.bodyDocumentoFirma.feDesd !== '' && this.bodyDocumentoFirma.feDesd !== null) ? this.funciones.convertirFechaStringToDate(this.bodyDocumentoFirma.feDesd) : this.bodyDocumentoFirma.feDesd});
    this.searchDocumento.patchValue({ "fecHas": (this.bodyDocumentoFirma.feHast !== '' && this.bodyDocumentoFirma.feHast !== null) ? this.funciones.convertirFechaStringToDate(this.bodyDocumentoFirma.feHast) : this.bodyDocumentoFirma.feHast});
    this.searchDocumento.patchValue({ "nroDocu": this.bodyDocumentoFirma.nuDocuOrig});
    this.searchDocumento.patchValue({ "perfil": this.bodyDocumentoFirma.coPerf});
    this.searchDocumento.patchValue({ "usuario": this.bodyDocumentoFirma.noUsuaCrea});
    this.searchDocumento.patchValue({ "estado": this.bodyDocumentoFirma.esDocu});

    sessionStorage.removeItem('bodyDocFirma');
  }

  public buscarDocumento(esCargaInicial: boolean): void {
    const fechaDesde = this.searchDocumento.get('fecDes')?.value;
    const fechaHasta = this.searchDocumento.get('fecHas')?.value;
    if (!fechaDesde && fechaHasta) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha DESDE');  
      return;     
    }
    if (!fechaHasta && fechaDesde) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha HASTA');   
      return;     
    }

    this.selectedValues = [];
    this.loadingFirmarDocumentos = true;
    this.guardarListadosParaFiltro();
    this.esBusqueda = (esCargaInicial) ? false : true;
    this.utilService.onShowProcessLoading("Cargando la data"); 
    const zona = this.searchDocumento.get('zona')?.value;
    const oficina = this.searchDocumento.get('oficina')?.value;
    const local = this.searchDocumento.get('local')?.value;
    const fechaDesdeFormat = (fechaDesde !== "" && fechaDesde !== null) ? this.funciones.convertirFechaDateToString(fechaDesde) : '';
    const fechaHastaFormat = (fechaHasta !== "" && fechaHasta !== null) ? this.funciones.convertirFechaDateToString(fechaHasta) : '';
    const tipoComp = this.searchDocumento.get('tipoDocu')?.value;
    const perfil = this.searchDocumento.get('perfil')?.value;
    const estadoDocumento = this.searchDocumento.get('estado')?.value;
    this.bodyDocumentoFirma.coZonaRegi = zona === '*' ? '' : zona;
    this.bodyDocumentoFirma.coOficRegi = oficina === '*' ? '' : oficina;
    this.bodyDocumentoFirma.coLocaAten = local === '*' ? '' : local;
    this.bodyDocumentoFirma.tiDocu = tipoComp === '*' ? '' : tipoComp;
    this.bodyDocumentoFirma.coPerf = perfil === '*' ? '' : perfil;
    this.bodyDocumentoFirma.idDocuOrig = 0;
    this.bodyDocumentoFirma.nuDocuOrig = this.searchDocumento.get('nroDocu')?.value;
    this.bodyDocumentoFirma.feDesd = fechaDesdeFormat;
    this.bodyDocumentoFirma.feHast = fechaHastaFormat;
    this.bodyDocumentoFirma.noUsuaCrea = this.searchDocumento.get('usuario')?.value;
    this.bodyDocumentoFirma.esDocu = estadoDocumento === '*' ? '' : estadoDocumento;

    this.documentoService.getBandejaDocumentoFirma(this.bodyDocumentoFirma).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.documentosList = data.lstDocumentoFirma;
          this.loadingFirmarDocumentos = false;
          this.utilService.onCloseLoading();  
        },
        error:( d )=>{
          this.loadingFirmarDocumentos = false;
          this.documentosList = [];
          this.utilService.onCloseLoading();  
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("warning", "Atención", d.error.description);        
          }else {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      });
  }

  private guardarListadosParaFiltro() {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaTipoDocumentoFiltro = [...this.listaTipoDocumento];
    this.listaPerfilFiltro = [...this.listaPerfil];
    this.listaEstadoFiltro = [...this.listaEstado];
  }

  private guardarListadosEnSession() {
    const body = {
      listaZonaRegistral: this.listaZonaRegistralFiltro,
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaTipoDocumento: this.listaTipoDocumentoFiltro,
      listaPerfil: this.listaPerfilFiltro,
      listaEstado: this.listaEstadoFiltro,
      bodyDocumentoFirma: this.bodyDocumentoFirma
    }

    sessionStorage.setItem('bodyDocFirma', JSON.stringify( body ));
  }

  verDocumento() {
    if (this.selectedValues.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro.");
      return;
    } 
    else if (this.selectedValues.length != 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar solo un registro.");
      return;
    }
  
    const documento  = this.selectedValues;
    let idGuidDocu = documento['0'].idGuidDocu!;

    if (idGuidDocu == "" || idGuidDocu == " ") {
        this.utilService.onShowAlert("warning", "Atención", "Registro selccionado no cuenta con Documento a visualizar");
        return;
      } 	
    else {
      
      console.log('Visor PDF: ' + idGuidDocu);

      const download = document.createElement('a');
      download.href =  this.generalService.downloadManager(idGuidDocu);
      download.setAttribute("target", "_blank");
      document.body.appendChild( download );
      download.click();
      
    }
	
  }
  
  firmar() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar mínimo un registro");
      return;
    }
    
    for ( let registro of this.selectedValues ) {
      if ( registro.esDocu != '002' ) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede firmar documentos en estado PENDIENTE DE FIRMA");
        this.selectedValues = [];
        return;
      }
    }
    
    this.guardarListadosEnSession();

    for ( let documento of this.selectedValues ) {
      let idGuidDocu = documento.idGuidDocu!;
      if (idGuidDocu == "" || idGuidDocu == " ") {
        const index = this.selectedValues.findIndex( i => i.idGuidDocu == '' || i.idGuidDocu == ' ' );
        this.utilService.onShowAlert(
          "warning", 
          "Atención", 
          `${ this.selectedValues[index].deTipoDocu } Nro ${ this.selectedValues[index].nuDocuOrig } 
           no puede ser firmado, no existe el archivo .pdf (GUID)`
        );
        return;
      }
      console.log('Firmar Documento: ' + idGuidDocu);
  
      let perfil:number = + localStorage.getItem('perfil_sarf') ;
      console.log("compara perfil: " + documento.coPerf! + " ---- " + perfil);
      if ( documento.coPerf! != perfil ) {
        const index = this.selectedValues.findIndex( i => i.coPerf! != perfil );
        this.utilService.onShowAlert(
          "warning", 
          "Atención", 
          `Su perfil no esta configurado para firmar: ${ this.selectedValues[index].deTipoDocu } 
           Nro ${ this.selectedValues[index].nuDocuOrig }`);
        return;
      }
    }

    const dialog = this.dialogService.open( FirmarDocumentoComponent, {
      header: 'Firmar Documento',
      width: '60%',
      closable: false,
      data: {
        csta: this.selectedValues
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Se firmó el documento satisfactoriamente.'
        });
  
        this.buscarDocumento(false);
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al firmar del documento.'
        });
  
        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }



  sustentos() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Documentos para Firmar', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Documentos para Firmar', 'Solo puede visualizar un registro a la vez!', 'error');
      return;
    }

    const dialog = this.dialogService.open( SustentosDocumentoComponent, {
      header: 'Sustentos',
      width: '50%',
      closable: false,
      data: {
        doc: this.selectedValues,
        add: this.BtnAdicionar_Sustento,
        delete: this.BtnEliminar_Sustento,
        bandeja: this.codBandeja
      }
    });

    dialog.onClose.subscribe( () => {
      this.selectedValues = [];
    });
  }

  public subirDocumentoFirmado(): void {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un documento.");
      return;
    }

    if ( this.selectedValues?.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede seleccionar un documento a la vez.");
      return;
    }

    if ( this.selectedValues[0].esDocu != '002' ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede subir documentos firmados en estado PENDIENTE DE FIRMA.");
      return;
    }

    const dialog = this.dialogService.open( SubirDocumentoComponent, {
      header: 'Subir Documento Firmado',
      width: '50%',
      closable: false,
      data: {
        doc: this.selectedValues,
        add: this.BtnAdicionar_Sustento,
        delete: this.BtnEliminar_Sustento,
        bandeja: this.codBandeja
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) { 
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Se firmó el documento satisfactoriamente.'
        });
    
        this.buscarDocumento(false);
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al firmar del documento.'
        });
    
        this.selectedValues = [];
      }
  
      this.selectedValues = [];
    });
  }

  public rechazarFirma(): void {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un documento para rechazarlo.");
      return;
    }

    if ( this.selectedValues?.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede rechazar un documento a la vez.");
      return;
    }

    if ( this.selectedValues[0].esDocu != '002' ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede rechazar documentos en estado PENDIENTE DE FIRMA.");
      return;
    }

    this.utilService.onShowConfirm('¿Estás seguro que desea rechazar el documento').then((result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('Rechazar');
        const idDocuFirm = this.selectedValues[0].idDocuFirm;
        this.documentoService.rechazarDocumentoFirma(idDocuFirm).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0 ) {
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
            } else if (d.codResult === 0 ) {
              this.selectedValues = [];
              this.buscarDocumento(false);
            }
          },

          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", d.error.description);        

            }else {
              this.utilService.onShowMessageErrorSystem();        
            }
          }
        });
        
      } else if (result.isDenied) {
        this.utilService.onShowAlert("warning", "Atención", "No se pudieron guardar los cambios.");
      }
    })
    
  }

  public historialFirma(): void {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Documentos para Firmar', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Documentos para Firmar', 'Solo puede visualizar un registro a la vez!', 'error');
      return;
    }

    const dialog = this.dialogService.open( HistorialFirmaComponent, {
      header: 'Historial de Firmas',
      width: '50%',
      closable: false,
      data: this.selectedValues[0]
    });

    dialog.onClose.subscribe( () => {
      this.selectedValues = [];
    });
  }

  customSort( event: SortEvent ) {
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
          result = -1;
        else if (value1 != null && value2 == null)
          result = 1;
        else if (value1 == null && value2 == null)
          result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
          result = value1.localeCompare(value2);
        else
          result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
        return (event.order! * result);
    });
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  private establecerDataFiltrosRecibos() : IDataFiltrosDocumentoFirma {
    const dataFiltrosRecibos: IDataFiltrosDocumentoFirma = {
      listaZonaRegistral: this.listaZonaRegistralFiltro, 
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaTipoDocumento: this.listaTipoDocumentoFiltro,
      listaPerfil: this.listaPerfil,
      listaEstado: this.listaEstadoFiltro,
      bodyDocumentoFirma: this.bodyDocumentoFirma,   
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosRecibos;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.sharingInformationService.compartirDataFiltrosDocumentoFirmaObservableData = this.establecerDataFiltrosRecibos();
  }

}
