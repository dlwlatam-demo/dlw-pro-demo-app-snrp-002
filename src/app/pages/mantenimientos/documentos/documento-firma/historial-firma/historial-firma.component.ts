import { Component, OnInit, OnDestroy} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import { DocumentoFirmar } from '../../../../../models/mantenimientos/documentos/documento-firma.model';

import { GeneralService } from '../../../../../services/general.service';
import { DocumentoFirmaService } from '../../../../../services/mantenimientos/documentos/documento-firma.service';
import { FirmarDocumentosValue, IFirmarDocumentos, IHistorialFirmas } from 'src/app/interfaces/firmar-documentos.interface';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { UtilService } from 'src/app/services/util.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-historial-firma',
  templateUrl: './historial-firma.component.html',
  styleUrls: ['./historial-firma.component.scss']
})
export class HistorialFirmaComponent implements OnInit, OnDestroy {
  unsubscribe$ = new Subject<void>();
  datos!: FormGroup;
  dataFirmaDocumento: FirmarDocumentosValue;
  loadingHistorialFirmas: boolean = false;
  listaHistorialFirmas: IHistorialFirmas[] = [];

  currentPage: string = `${ environment.currentPage }`;

  constructor( private fb: FormBuilder,
               private config: DynamicDialogConfig,
               private dialogRef: DynamicDialogRef,
               private funciones: Funciones,  
               private utilService: UtilService,
               private documentoService: DocumentoFirmaService ) { }

  ngOnInit(): void {
    this.dataFirmaDocumento = this.config.data;
    const formatfecha = this._customFecha();
    this.datos = this.fb.group({
      zona: [this.dataFirmaDocumento.deZonaRegi, [ Validators.required ]],
      oficina: [this.dataFirmaDocumento.deOficRegi, [ Validators.required ]],
      local: [this.dataFirmaDocumento.deLocaAten, [ Validators.required ]],
      tipoDocu: [this.dataFirmaDocumento.deTipoDocu, [ Validators.required ]],
      nroDocu: [this.dataFirmaDocumento.nuDocuOrig, [ Validators.required ]],
      fecha: [formatfecha, [ Validators.required ]]
    });
    this._getHistoricalFirmas();
  }

  private _customFecha(): string {
    const fecha = new Date(this.dataFirmaDocumento.feDocu);
    const fechaFormat = (fecha !== null) ? this.funciones.convertirFechaDateToString(fecha) : '';
    return fechaFormat;
  }

  private _getHistoricalFirmas(): void {
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.loadingHistorialFirmas = true;
    this.documentoService.getHistoricalFirmas(this.dataFirmaDocumento.idDocuFirm).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.loadingHistorialFirmas = false;
          this.listaHistorialFirmas = data;
          this.utilService.onCloseLoading();  
        },
        error:( e )=>{
          console.log( e );
          this.loadingHistorialFirmas = false;
          this.listaHistorialFirmas = [];
          this.utilService.onCloseLoading(); 
          // if (d.error.category === "NOT_FOUND") {
          //   this.utilService.onShowAlert("warning", "Atención", d.error.description);        
          // }else {
          //   this.utilService.onShowMessageErrorSystem();
          // }
        }
      })
  }

  regresar() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();  
  }

}
