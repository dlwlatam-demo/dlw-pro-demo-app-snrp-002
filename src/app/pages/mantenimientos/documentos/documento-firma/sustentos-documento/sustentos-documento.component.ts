import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import { FirmarDocumentosValue, IFirmarDocumentos } from '../../../../../interfaces/firmar-documentos.interface';

import { TramaSustento } from '../../../../../models/archivo-adjunto.model';

import { GeneralService } from '../../../../../services/general.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { DocumentoFirmaService } from '../../../../../services/mantenimientos/documentos/documento-firma.service';

import { UploadComponent } from '../../../../upload/upload.component';

@Component({
  selector: 'app-sustentos-documento',
  templateUrl: './sustentos-documento.component.html',
  styleUrls: ['./sustentos-documento.component.scss']
})
export class SustentosDocumentoComponent implements OnInit {

  datos!: FormGroup;

  $zonas!: Observable<any>;
  $oficinas!: Observable<any>;
  $locales!: Observable<any>;
  $tipoDocumento!: Observable<any>;

  add!: number;
  delete!: number;
  bandeja!: number;

  documento!: FirmarDocumentosValue[];
  documentoSustento: TramaSustento[] = [];

  @ViewChild(UploadComponent) upload: UploadComponent;

  constructor( 
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private generalService: GeneralService,
    private validarService: ValidatorsService,
    private documentoService: DocumentoFirmaService 
  ) { }

  ngOnInit(): void {

    this.$zonas = this.generalService.getCbo_Zonas_Usuario('');
    this.$tipoDocumento = this.generalService.getCbo_Documento();

    this.documento = this.config.data.doc as FirmarDocumentosValue[];

    this.add = this.config.data.add;
    this.delete = this.config.data.delete;
    this.bandeja = this.config.data.bandeja;

    this.datos = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoDocu: ['', [ Validators.required ]],
      nroDocu: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]]
    });

    this.hidrate( this.documento[0] );
    this.sustentosDocumentoFirma();
  }

  hidrate( d: FirmarDocumentosValue ) {
    const fecDocu = this.validarService.reFormateaFecha( d.feDocu );
    
    this.datos.get('zona')?.setValue( d.coZonaRegi );

    this.$oficinas = this.generalService.getCbo_Oficinas_Zonas('', d.coZonaRegi);
    this.datos.get('oficina')?.setValue( d.coOficRegi );
    
    this.$locales = this.generalService.getCbo_Locales_Ofic('', d.coZonaRegi, d.coOficRegi);
    this.datos.get('local')?.setValue( d.coLocaAten );

    this.datos.get('tipoDocu')?.setValue( d.tiDocu );
    this.datos.get('nroDocu')?.setValue( d.nuDocuOrig );
    this.datos.get('fecha')?.setValue( fecDocu );
  }

  sustentosDocumentoFirma() {
    this.documentoService.getDocumentoFirmaSustentoBandeja( this.documento['0'].idDocuFirm ).subscribe({
      next: ( sustentos ) => {
        console.log('docuSust', sustentos);
        this.documentoSustento = sustentos.map( s => {
          return {
            id: s.idDocuSust,
            noDocuSust: s.noDocuSust,
            idGuidDocu: s.idGuidDocu
          }
        });

        this.upload.sustentos = this.documentoSustento;
        this.upload.goEdit();
      }
    });
  }

  regresar() {
    this.ref.close('');
  }

}
