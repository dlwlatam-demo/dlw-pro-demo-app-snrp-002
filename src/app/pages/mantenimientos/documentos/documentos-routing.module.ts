import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from 'src/app/services/auth/auth-gaurd.service';

import { BandejaComprobanteComponent } from './comprobante-pago/bandeja-comprobante/bandeja-comprobante.component';
import { NuevoComprobanteComponent } from './comprobante-pago/nuevo-comprobante/nuevo-comprobante.component';
import { BandejaConstanciaComponent } from './constancia-abono/bandeja-constancia/bandeja-constancia.component';
import { BandejaDocumentoComponent } from './documento-firma/bandeja-documento/bandeja-documento.component';
import { BandejaReciboComponent } from './recibo-ingreso/bandeja-recibo/bandeja-recibo.component';
import { NuevoReciboComponent } from './recibo-ingreso/nuevo-recibo/nuevo-recibo.component';
import { BandejaCartaComponent } from './carta-orden/bandeja-carta/bandeja-carta.component';
import { NuevaCartaComponent } from './carta-orden/nueva-carta/nueva-carta.component';
import { BandejaMemoComponent } from './memorandum/bandeja-memo/bandeja-memo.component';
import { MemoNuevoComponent } from './memorandum/memo-nuevo/memo-nuevo.component';
import { ChequesComponent } from './cheques/cheques.component';
import { EditarCartaComponent } from './carta-orden/editar-carta/editar-carta.component';
import { EditarComprobanteComponent } from './comprobante-pago/editar-comprobante/editar-comprobante.component';
import { ConsultarComprobanteComponent } from './comprobante-pago/consultar-comprobante/consultar-comprobante.component';
import { ConsultarCartaComponent } from './carta-orden/consultar-carta/consultar-carta.component';
import { RefreshMemorandumGuard } from 'src/app/core/guards/mantenimientos/documentos/refresh-memorandum.guard';
import { RefreshCartaOrdenGuard } from 'src/app/core/guards/mantenimientos/documentos/refresh-carta-orden.guard';
import { RefreshComprobantePagoGuard } from 'src/app/core/guards/mantenimientos/documentos/refresh-comprobante-pago.guard';
import { BandejaDocumentosVariosComponent } from './documentos-varios-solicitar-firma/bandeja-documentos-varios/bandeja-documentos-varios.component';
import { NuevoDocumentosVariosComponent } from './documentos-varios-solicitar-firma/nuevo-documentos-varios/nuevo-documentos-varios.component';
import { RefreshDocumentosVariosGuard } from 'src/app/core/guards/mantenimientos/documentos/refresh-documentos-varios.guard';
import { EditarDocumentosVariosComponent } from './documentos-varios-solicitar-firma/editar-documentos-varios/editar-documentos-varios.component';
import { ConsultarDocumentosVariosComponent } from './documentos-varios-solicitar-firma/consultar-documentos-varios/consultar-documentos-varios.component';

import { BandejaDocumentosEnviarComponent } from './documentos-enviar/bandeja-documentos-enviar/bandeja-documentos-enviar.component';
import { NuevoDocumentosEnviarComponent } from './documentos-enviar/nuevo-documentos-enviar/nuevo-documentos-enviar.component';
import { RefreshDocumentosEnviarGuard } from 'src/app/core/guards/mantenimientos/documentos/refresh-documentos-enviar.guard';
import { EditarDocumentosEnviarComponent } from './documentos-enviar/editar-documentos-enviar/editar-documentos-enviar.component';
import { ConsultarDocumentosEnviarComponent } from './documentos-enviar/consultar-documentos-enviar/consultar-documentos-enviar.component';


const routes: Routes = [
  { path: 'comprobantes_de_pago',
    children: [
      { path: 'bandeja', component: BandejaComprobanteComponent },
      { path: 'nuevo', component: NuevoComprobanteComponent, canActivate: [ RefreshComprobantePagoGuard ] },
      { path: 'editar/:id', component: EditarComprobanteComponent, canActivate: [ RefreshComprobantePagoGuard ]},
      { path: 'consultar/:id', component: ConsultarComprobanteComponent, canActivate: [ RefreshComprobantePagoGuard ]},
      { path: 'cheques', loadChildren: () => import('./comprobante-pago/cheques/cheques.module').then( m => m.ChequesModule ) }
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'constancia_de_abono', children: [{ path: 'bandeja', component: BandejaConstanciaComponent }], canActivate: [ AuthGaurdService ] },
  { path: 'documento_para_firma', children: [{ path: 'bandeja', component: BandejaDocumentoComponent }], canActivate: [ AuthGaurdService ] },
  { path: 'documentos_varios_solicitar_firma',
    children: [
      { path: 'bandeja', component: BandejaDocumentosVariosComponent },
      { path: 'nuevo', component: NuevoDocumentosVariosComponent, canActivate: [ RefreshDocumentosVariosGuard ] },
      { path: 'editar/:id', component: EditarDocumentosVariosComponent, canActivate: [ RefreshDocumentosVariosGuard ] },
      { path: 'consultar/:id', component: ConsultarDocumentosVariosComponent, canActivate: [ RefreshDocumentosVariosGuard ] }
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'documentos_enviar',
    children: [
      { path: 'bandeja', component: BandejaDocumentosEnviarComponent },
      { path: 'nuevo', component: NuevoDocumentosEnviarComponent, canActivate: [ RefreshDocumentosEnviarGuard ] },
      { path: 'editar/:id', component: EditarDocumentosEnviarComponent, canActivate: [ RefreshDocumentosEnviarGuard ] },
      { path: 'consultar/:id', component: ConsultarDocumentosEnviarComponent, canActivate: [ RefreshDocumentosEnviarGuard ] }
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'recibos_de_ingreso',
    children: [
      { path: 'bandeja', component: BandejaReciboComponent },
      { path: 'nuevo', component: NuevoReciboComponent },
      { path: 'editar', component: NuevoReciboComponent },
      { path: 'consultar', component: NuevoReciboComponent }
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'carta_orden',
    children: [
      { path: 'bandeja', component: BandejaCartaComponent },
      { path: 'nuevo', component: NuevaCartaComponent, canActivate: [ RefreshCartaOrdenGuard ]},
      { path: 'editar/:id', component: EditarCartaComponent, canActivate: [ RefreshCartaOrdenGuard ]},
      { path: 'consultar/:id', component: ConsultarCartaComponent, canActivate: [ RefreshCartaOrdenGuard ]}
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'memorandum',
    children: [
      { path: 'bandeja', component: BandejaMemoComponent },
      { path: 'nuevo', component: MemoNuevoComponent, canActivate: [RefreshMemorandumGuard] },
      { path: 'editar', component: MemoNuevoComponent, canActivate: [RefreshMemorandumGuard] },
      { path: 'consultar', component: MemoNuevoComponent, canActivate: [RefreshMemorandumGuard] }
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'cheques', component: ChequesComponent, canActivate: [ AuthGaurdService ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentosRoutingModule { }
