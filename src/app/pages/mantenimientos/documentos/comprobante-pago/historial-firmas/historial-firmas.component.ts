import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { SortEvent } from 'primeng/api';

import { ComprobantePagoValue } from '../../../../../interfaces/comprobante-pago.interface';

import { environment } from '../../../../../../environments/environment';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { ComprobantePagoService } from '../../../../../services/mantenimientos/documentos/comprobante-pago.service';

@Component({
  selector: 'app-historial-firmas',
  templateUrl: './historial-firmas.component.html',
  styleUrls: ['./historial-firmas.component.scss']
})
export class HistorialFirmasComponent implements OnInit {

  currentPage: string = environment.currentPage;
  comprobante!: ComprobantePagoValue[];

  historial!: FormGroup;

  enviadosList!: any[];

  constructor(
    private fb: FormBuilder, 
    private ref: DynamicDialogRef, 
    private config: DynamicDialogConfig,
    private validateService: ValidatorsService,
    private comprobanteService: ComprobantePagoService,
  ) { }

  ngOnInit(): void {

    this.comprobante = this.config.data.comp as ComprobantePagoValue[];

    this.historial = this.fb.group({
      nroComp: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]],
      nroSiaf: ['', [ Validators.required ]]
    });

    this.hidrate( this.comprobante['0'] );
  }

  hidrate( c: ComprobantePagoValue ) {
    const monDepo = Number( c.imCompPago ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
    
    const fecComp = this.validateService.reFormateaFecha( c.feCompPago );

    this.historial.get('nroComp')?.setValue( c.nuCompPago );
    this.historial.get('fecha')?.setValue( fecComp );
    this.historial.get('monto')?.setValue( monDepo );
    this.historial.get('nroSiaf')?.setValue( c.nuSiaf );

    this.comprobanteService.getHistorialFirmaComprobantes( c.idDocuFirm ).subscribe({
      next: ( d ) => {
        this.enviadosList = d;
      },
      error: ( e ) => {
        console.log( e );
        this.enviadosList = [];
      }
    });
  }

  formatearFecha( date: string ): string {
    return this.validateService.reFormateaFechaConHoras( date );
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  regresar() {
    this.ref.close();
  }

}
