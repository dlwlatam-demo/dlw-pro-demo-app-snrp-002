import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { ComprobantePagoValue } from 'src/app/interfaces/comprobante-pago.interface';
import { BodyGenerarCheques } from 'src/app/models/mantenimientos/documentos/comprobante-pago.model';
import { ComprobantePagoService } from 'src/app/services/mantenimientos/documentos/comprobante-pago.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-generar-cheques',
  templateUrl: './generar-cheques.component.html',
  styleUrls: ['./generar-cheques.component.scss']
})
export class GenerarChequesComponent implements OnInit {
  generarCheques: FormGroup;
  unsubscribe$ = new Subject<void>();
  dataComprobantePago: ComprobantePagoValue;
  bodyGenerarCheques: BodyGenerarCheques;

  constructor(
    public config: DynamicDialogConfig,
    private dialogRef: DynamicDialogRef,
    private fb: FormBuilder,
    private comprobantePagoService: ComprobantePagoService,
    private utilService: UtilService,
  ) { }

  ngOnInit(): void {
    this.bodyGenerarCheques = new BodyGenerarCheques()
    this.dataComprobantePago = this.config.data;
    this.generarCheques = this.fb.group({
      cantidad: [0, [ Validators.required ]]
    })
  }

  public grabar(): void {
    let resultado: boolean = this._validarFormulario();
    if (!resultado) {
      return;
    }
    this.utilService.onShowProcessLoading("Generando cheques");
    this.bodyGenerarCheques.idCompPago = this.dataComprobantePago.idCompPago;
    this.bodyGenerarCheques.caCheq = this.generarCheques.get('cantidad')?.value;
    this.comprobantePagoService.generarCheques( this.bodyGenerarCheques ).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (d) => {
          if (d.codResult !== 0) {
            this.utilService.onShowAlert("warning", "Atención", d.msgResult);        
          } else {
            this.utilService.onCloseLoading();  
            this.dialogRef.close();
          }
        },
        error: (d) => {
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("warning", "Atención", d.error.description);        
          }else {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      });
  }

  private _validarFormulario(): boolean {  
    if ((this.generarCheques.value.cantidad === 0 || this.generarCheques.value.cantidad === null)) {
      document.getElementById('idCantidad').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese la cantidad de comprobantes");
      return false;       
    } else {
      return true;
    }       
  }

  public cancelar(): void {
    this.dialogRef.close();
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
