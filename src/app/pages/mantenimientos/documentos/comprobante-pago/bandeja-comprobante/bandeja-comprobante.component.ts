import { UtilService } from 'src/app/services/util.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService } from 'primeng/api';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { SustentosComprobanteComponent } from '../sustentos-comprobante/sustentos-comprobante.component';
import { SolicitarFirmaComprobanteComponent } from '../solicitar-firma-comprobante/solicitar-firma-comprobante.component';
import { HistorialFirmasComponent } from '../historial-firmas/historial-firmas.component';
import { ReservarComponent } from '../reservar/reservar.component';
import { GenerarChequesComponent } from '../generar-cheques/generar-cheques.component';
import { CopiarComprobanteComponent } from '../copiar-comprobante/copiar-comprobante.component';

import { environment } from '../../../../../../environments/environment';
import { GeneralService } from 'src/app/services/general.service';
import { ReciboIngresoService } from 'src/app/services/mantenimientos/documentos/recibo-ingreso.service';
import { ComprobantePagoService } from 'src/app/services/mantenimientos/documentos/comprobante-pago.service';
import { BodyComprobantePago, BodyEliminarActivarComprobantePago } from 'src/app/models/mantenimientos/documentos/comprobante-pago.model';
import { ComprobantePagoValue, IComprobantePago, IDataFiltrosRecibo } from '../../../../../interfaces/comprobante-pago.interface';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { ReciboIngreso } from 'src/app/models/mantenimientos/documentos/recibo-ingreso.model';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { BaseBandeja } from 'src/app/base/base-bandeja.abstract';
import { IBandejaProcess } from '../../../../../interfaces/global.interface';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { BodyCambiarEstadoComprobantePago, BodyValidarComprobante } from '../../../../../models/mantenimientos/documentos/comprobante-pago.model';
import { COMPROBANTE_NUM } from '../../../../../models/enum/comprobante-numeracion';
import { NoUtilizadoComponent } from '../no-utilizado/no-utilizado.component';

@Component({
  selector: 'app-bandeja-comprobante',
  templateUrl: './bandeja-comprobante.component.html',
  styleUrls: ['./bandeja-comprobante.component.scss'],
  providers: [
    DialogService,
    MessageService
  ]
})
export class BandejaComprobanteComponent extends BaseBandeja implements OnInit, OnDestroy {
  loadingZonaRegistral: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  loadingTipoRecibo: boolean = false;
  loadingEstado: boolean = false;
  loadingMediosPago: boolean = false;
  userCode:any
  unsubscribe$ = new Subject<void>();

  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoRecibo: ReciboIngreso[] = [];
  listaEstadoRecibo: IResponse2[] = [];
  listaMediosPago: IResponse2[] = [];

  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaTipoReciboFiltro: ReciboIngreso[];
  listaEstadoReciboFiltro: IResponse2[];
  listaMediosPagoFiltro: IResponse2[];
  esBusqueda: boolean = false;
  fechaMinima: Date;
  fechaMaxima: Date;
  comprobante!: FormGroup;
  currentPage: string = environment.currentPage;
  bodyComprobantePago: BodyComprobantePago;
  bodyEliminarActivarComprobantePago: BodyEliminarActivarComprobantePago;
  bodyCambiarEstado: BodyCambiarEstadoComprobantePago;
  bodyValidarComprobante!: BodyValidarComprobante;
  comprobantePagoService$: Subscription = new Subscription;
  comprobantesList: IComprobantePago;
  comprobantePagoProceso: IBandejaProcess = {
    idMemo: null,
    proceso: ''
  };
  override selectedValues: ComprobantePagoValue[] = [];
  selectSust!: ComprobantePagoValue[];
  selectConst!: ComprobantePagoValue[];

  guidDocumento: string = '';

  BUSCAR: string = COMPROBANTE_NUM.BUSCAR;
  NUEVO: string = COMPROBANTE_NUM.NUEVO;
  EDITAR: string = COMPROBANTE_NUM.EDITAR;
  ANULAR: string = COMPROBANTE_NUM.ANULAR;
  ACTIVAR: string = COMPROBANTE_NUM.ACTIVAR;
  CONSULTAR: string = COMPROBANTE_NUM.CONSULTAR;
  AUTORIZAR_MODIFICACION: string = COMPROBANTE_NUM.AUTORIZAR_MODIFICACION;
  VER_DOCUMENTO: string = COMPROBANTE_NUM.VER_DOCUMENTO;
  VER_SUSTENTO: string = COMPROBANTE_NUM.VER_SUSTENTO;
  SOLICITAR_FIRMA: string = COMPROBANTE_NUM.SOLICITAR_FIRMA;
  VER_HISTORIAL: string = COMPROBANTE_NUM.VER_HISTORIAL;
  COPIAR: string = COMPROBANTE_NUM.COPIAR;
  RESERVAR: string = COMPROBANTE_NUM.RESERVAR;
  NO_UTILIZADO: string = COMPROBANTE_NUM.NO_UTILIZADO;
  GENERAR_CHEQUES: string = COMPROBANTE_NUM.GENERAR_CHEQUES;
  VER_CHEQUES: string = COMPROBANTE_NUM.VER_CHEQUES;
  EXPORTAR_EXCEL: string = COMPROBANTE_NUM.EXPORTAR_EXCEL;

  constructor( private fb: FormBuilder,
               private dialogService: DialogService,
               private messageService: MessageService,
               private router: Router,
               private generalService: GeneralService,
               private reciboService: ReciboIngresoService,
               private utilService: UtilService,
               private funciones: Funciones,
               private validateService: ValidatorsService,
               private comprobantePagoService: ComprobantePagoService,
               private sharingInformationService: SharingInformationService
              ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Comprobante_de_Pago;
    this.btnComprobanteDePago();

    this.bodyComprobantePago = new BodyComprobantePago();
    this.bodyEliminarActivarComprobantePago = new BodyEliminarActivarComprobantePago();
    this.bodyCambiarEstado = new BodyCambiarEstadoComprobantePago();
    this.bodyValidarComprobante = new BodyValidarComprobante();
    this.userCode = localStorage.getItem('user_code')||'';
    this.fechaMaxima = new Date();
    this.fechaMinima = new Date(this.fechaMaxima.getFullYear(), this.fechaMaxima.getMonth(), 1);
    this.comprobante = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoComp: ['', [ Validators.required ]],
      estado: ['', [ Validators.required ]],
      usuaCrea: ['', [ Validators.required ]],
      fecCrea: ['', []],
      usuaModi: ['', [ Validators.required ]],
      fecModi: ['', []],
      usuaAuto: ['', []],
      fecAuto: ['', []],
      fecDes: [this.fechaMinima],
      fecHas: [this.fechaMaxima],
      nroComp: ['', [ Validators.required ]],
      nroSiaf: [''],
      noBene: ['', []],
      monto: ['', []],
      medioPago: ['', []],
      nroMedioPago: ['', []],
      ctaCte: ['', []]
    })
    this._dataFiltrosCartaOrden();
  }

  private _buscarZonaRegistral(): void {
    this.loadingZonaRegistral = true;
    this.generalService.getCbo_Zonas_Usuario(this.userCode).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingZonaRegistral = false;
        if (data.length === 1) {
          this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });
          this.comprobante.patchValue({ "zona": data[0].coZonaRegi});
          this.buscarOficinaRegistral();
        } else {
          this.listaZonaRegistral.push(...data);
        }
        // this.buscarComprobante(false);
      },
      error:( err )=>{
        this.loadingZonaRegistral = false;
      }
    });
  }

  public buscarOficinaRegistral(): void {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.selectedValues = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' });
    if (this.comprobante.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.comprobante.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });
          this.comprobante.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;
      }, (err) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    }
  }

  public buscarLocal() {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' });
    if (this.comprobante.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.comprobante.get('zona')?.value,this.comprobante.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });
          this.comprobante.patchValue({ "local": data[0].coLocaAten});
        } else {
          this.listaLocal.push(...data);
        }
        this.loadingLocal = false;
      }, (err) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  private _buscarTipoComprobante() {
    this.loadingTipoRecibo = true;
    this.reciboService.getTipoReciboIngreso('2').pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingTipoRecibo = false;
        this.listaTipoRecibo.push(...data);
      },
      error:( err )=>{
        this.loadingTipoRecibo = false;
      }
    });
  }

  public cambioFechaInicio() {
    if (this.comprobante.get('fecHas')?.value !== '' && this.comprobante.get('fecHas')?.value !== null) {
      if (this.comprobante.get('fecDes')?.value > this.comprobante.get('fecHas')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de inicio debe ser menor o igual a la fecha de fin.");
        this.comprobante.controls['fecDes'].reset();
      }
    }
  }

  public cambioFechaFin() {
    if (this.comprobante.get('fecDes')?.value !== '' && this.comprobante.get('fecDes')?.value !== null) {
      if (this.comprobante.get('fecHas')?.value < this.comprobante.get('fecDes')?.value) {
        this.utilService.onShowAlert("warning", "Atención", "La fecha de fin debe ser mayor o igual a la fecha de inicio.");
        this.comprobante.controls['fecHas'].reset();
      }
    }
  }

  private _buscarEstado() {
    this.loadingEstado = true;
    this.generalService.getCbo_EstadoComprobante().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingEstado = false;
        this.listaEstadoRecibo.push(...data);
      },
      error:( err )=>{
        this.loadingEstado = false;
      }
    });
  }

  private _buscarMediosPago() {
    this.loadingMediosPago = true;
    this.generalService.getCbo_MediosDePago().pipe( takeUntil(this.unsubscribe$) )
        .subscribe({
          next: ( data ) => {
            this.loadingMediosPago = false;
            this.listaMediosPago.push(...data);
          },
          error: ( err ) => {
            this.loadingMediosPago = false;
          }
        });
  }

  private _dataFiltrosCartaOrden(): void {
    this.sharingInformationService.compartirDataFiltrosComprobantePagoObservable.pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: IDataFiltrosRecibo) => {
      if (data.esCancelar && data.bodyRecibo !== null) {
        this.utilService.onShowProcessLoading("Cargando la data");
        this._setearCamposFiltro(data);
        this.buscarComprobante(false);
      } else {
        this._inicilializarListas();
        this._buscarMediosPago();
        this._buscarZonaRegistral();
        this._buscarEstado();
        this._buscarTipoComprobante();
        this.buscarComprobante(true);
      }
    });
  }

  private _setearCamposFiltro(dataFiltro: IDataFiltrosRecibo) {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoRecibo = [];
    this.listaEstadoRecibo = [];
    this.listaMediosPago = [];
    this.listaZonaRegistral = dataFiltro.listaZonaRegistral;
    this.listaOficinaRegistral = dataFiltro.listaOficinaRegistral;
    this.listaLocal = dataFiltro.listaLocal;
    this.listaTipoRecibo = dataFiltro.listaTipoRecibo;
    this.listaEstadoRecibo = dataFiltro.listaEstadoRecibo;
    this.listaMediosPago = dataFiltro.listaMediosPago;
    this.bodyComprobantePago = dataFiltro.bodyRecibo;
    this.comprobante.patchValue({ "zona": this.bodyComprobantePago.coZonaRegi});
    this.comprobante.patchValue({ "oficina": this.bodyComprobantePago.coOficRegi});
    this.comprobante.patchValue({ "local": this.bodyComprobantePago.coLocaAten});
    this.comprobante.patchValue({ "tipoComp": this.bodyComprobantePago.idTipoCompPago});
    this.comprobante.patchValue({ "fecDes": (this.bodyComprobantePago.feDesd !== '' && this.bodyComprobantePago.feDesd !== null) ? this.funciones.convertirFechaStringToDate(this.bodyComprobantePago.feDesd) : this.bodyComprobantePago.feDesd});
    this.comprobante.patchValue({ "fecHas": (this.bodyComprobantePago.feHast !== '' && this.bodyComprobantePago.feHast !== null) ? this.funciones.convertirFechaStringToDate(this.bodyComprobantePago.feHast) : this.bodyComprobantePago.feHast});
    this.comprobante.patchValue({ "nroComp": this.bodyComprobantePago.nuCompPago});
    this.comprobante.patchValue({ "nroSiaf": this.bodyComprobantePago.nuSiaf});
    this.comprobante.patchValue({ "usuaCrea": this.bodyComprobantePago.noUsuaCrea});
    this.comprobante.patchValue({ "fecCrea": (this.bodyComprobantePago.feCrea !== '' && this.bodyComprobantePago.feCrea !== null) ? this.funciones.convertirFechaStringToDate(this.bodyComprobantePago.feCrea) : this.bodyComprobantePago.feCrea});
    this.comprobante.patchValue({ "usuaModi": this.bodyComprobantePago.noUsuaModi});
    this.comprobante.patchValue({ "fecModi": (this.bodyComprobantePago.feModi !== '' && this.bodyComprobantePago.feModi !== null) ? this.funciones.convertirFechaStringToDate(this.bodyComprobantePago.feModi) : this.bodyComprobantePago.feModi});
    this.comprobante.patchValue({ "usuaAuto": this.bodyComprobantePago.noUsuaAuto});
    this.comprobante.patchValue({ "fecAuto": (this.bodyComprobantePago.feDocuAuto !== '' && this.bodyComprobantePago.feDocuAuto !== null) ? this.funciones.convertirFechaStringToDate(this.bodyComprobantePago.feDocuAuto) : this.bodyComprobantePago.feDocuAuto});
    this.comprobante.patchValue({ "estado": this.bodyComprobantePago.esDocu});
    this.comprobante.patchValue({ "noBene": this.bodyComprobantePago.noBene});
    this.comprobante.patchValue({ "monto": this.bodyComprobantePago.imCompPago});
    this.comprobante.patchValue({ "medioPago": this.bodyComprobantePago.tiDocu});
    this.comprobante.patchValue({ "nroMedioPago": this.bodyComprobantePago.nuDocu});
    this.comprobante.patchValue({ "ctaCte": this.bodyComprobantePago.nuCuenBanc});
  }

  private _inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoRecibo = [];
    this.listaEstadoRecibo = [];
    this.listaMediosPago = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(TODOS)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(TODOS)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(TODOS)' });
    this.listaTipoRecibo.push({ idTipo: 0, noTipo: '(TODOS)' });
    this.listaEstadoRecibo.push({ ccodigoHijo: '*', cdescri: '(TODOS)' });
    this.listaMediosPago.push({ ccodigoHijo: '*', cdescri: '(TODOS)' });
  }

  public buscarComprobante(esCargaInicial: boolean): void {
    const fechaHasta = this.comprobante.get('fecHas')?.value;
    const fechaDesde = this.comprobante.get('fecDes')?.value;
    const fechaCreacion = this.comprobante.get('fecCrea')?.value;
    const fechaModificacion = this.comprobante.get('fecModi')?.value;
    const fechaAutorizacion = this.comprobante.get('fecAuto')?.value;
    const imCompPago = this.comprobante.get('monto')?.value;

    if (!fechaDesde && fechaHasta) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha DESDE');
      return;
    }
    if (!fechaHasta && fechaDesde) {
      this.utilService.onShowAlert("warning", "Atención", 'Debe ingresar la fecha HASTA');
      return;
    }
    this.guardarListadosParaFiltro();
    this.esBusqueda = (esCargaInicial) ? false : true;
    this.utilService.onShowProcessLoading("Cargando la data");
    const zona = this.comprobante.get('zona')?.value;
    const oficina = this.comprobante.get('oficina')?.value;
    const local = this.comprobante.get('local')?.value;
    const fechaDesdeFormat = (fechaDesde !== "" && fechaDesde !== null) ? this.funciones.convertirFechaDateToString(fechaDesde) : '';
    const fechaHastaFormat = (fechaHasta !== "" && fechaHasta !== null) ? this.funciones.convertirFechaDateToString(fechaHasta) : '';
    const fechaCreacionFormat = (fechaCreacion !== "" && fechaCreacion !== null) ? this.funciones.convertirFechaDateToString(fechaCreacion) : '';
    const fechaModificacionFormat = (fechaModificacion !== "" && fechaModificacion !== null) ? this.funciones.convertirFechaDateToString(fechaModificacion) : '';
    const fechaAutorizacionFormat = (fechaAutorizacion !== "" && fechaAutorizacion !== null) ? this.funciones.convertirFechaDateToString(fechaAutorizacion) : '';
    const tipoComp = this.comprobante.get('tipoComp')?.value;
    const estadoDocumento = this.comprobante.get('estado')?.value;
    const medioDePago = this.comprobante.get('medioPago')?.value;
    const montoCompPago = (imCompPago === 0 || imCompPago === '') ? '' : this.funciones.convertirTextoANumero( imCompPago );
    this.bodyComprobantePago.coZonaRegi = zona === '*' ? '' : zona;
    this.bodyComprobantePago.coOficRegi = oficina === '*' ? '' : oficina;
    this.bodyComprobantePago.coLocaAten = local === '*' ? '' : local;
    this.bodyComprobantePago.idTipoCompPago = tipoComp === '*' ? '' : tipoComp;
    this.bodyComprobantePago.nuCompPago = this.comprobante.get('nroComp')?.value;
    this.bodyComprobantePago.feDesd = fechaDesdeFormat;
    this.bodyComprobantePago.feHast = fechaHastaFormat;
    this.bodyComprobantePago.esDocu = estadoDocumento === '*' ? '' : estadoDocumento;
    this.bodyComprobantePago.noUsuaCrea = this.comprobante.get('usuaCrea')?.value;
    this.bodyComprobantePago.feCrea = fechaCreacionFormat;
    this.bodyComprobantePago.noUsuaModi = this.comprobante.get('usuaModi')?.value;
    this.bodyComprobantePago.feModi = fechaModificacionFormat;
    this.bodyComprobantePago.noUsuaAuto = this.comprobante.get('usuaAuto')?.value;
    this.bodyComprobantePago.feDocuAuto = fechaAutorizacionFormat;
    this.bodyComprobantePago.nuSiaf = this.comprobante.get('nroSiaf')?.value;
    this.bodyComprobantePago.noBene = this.comprobante.get('noBene')?.value;
    this.bodyComprobantePago.imCompPago = Number( montoCompPago );;
    this.bodyComprobantePago.tiDocu = medioDePago === '*' ? '': medioDePago;
    this.bodyComprobantePago.nuDocu = this.comprobante.get('nroMedioPago')?.value;
    this.bodyComprobantePago.nuCuenBanc = this.comprobante.get('ctaCte')?.value;

    this.comprobantePagoService$ = this.comprobantePagoService.getBandejaComprobantePago(this.bodyComprobantePago)
      .subscribe({
        next: ( data ) => {
          console.log('data', data);
          this.comprobantesList = data;
          this.selectSust = this.comprobantesList.lstComprobantePago.filter( co => co.inSustRegi == '1' );
          this.selectConst = this.comprobantesList.lstComprobantePago.filter( co => co.inConsPago == '1' );
          this.selectedValues = [];
          this.utilService.onCloseLoading();
        },
        error:( d )=>{
          this.comprobantesList = null;
          this.selectedValues = [];
          this.utilService.onCloseLoading();
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("warning", "Atención", d.error.description);
          }else {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      });
  }

  private guardarListadosParaFiltro() {
    this.listaZonaRegistralFiltro = [...this.listaZonaRegistral];
    this.listaOficinaRegistralFiltro = [...this.listaOficinaRegistral];
    this.listaLocalFiltro = [...this.listaLocal];
    this.listaTipoReciboFiltro = [...this.listaTipoRecibo];
    this.listaEstadoReciboFiltro = [...this.listaEstadoRecibo];
    this.listaMediosPagoFiltro = [...this.listaMediosPago];
  }

  nuevoComprobante() {
    this.comprobantePagoProceso.idMemo = null;
    this.comprobantePagoProceso.proceso = 'nuevo';
    this.sharingInformationService.compartirComprobantePagoProcesoObservableData = this.comprobantePagoProceso;
    this.router.navigate(['SARF/mantenimientos/documentos/comprobantes_de_pago/nuevo']);
    this.sharingInformationService.irRutaBandejaComprobantePagoObservableData = false;
  }

  public editarComprobantePago() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
      return;
    }

    if ( this.selectedValues.length !== 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
      return;
    }

    // if ( this.selectedValues[0].deEsta === 'ANULADO' || this.selectedValues[0].deEsta === 'PENDIENTE DE FIRMA'
    //       || this.selectedValues[0].deEsta === 'FIRMADO' ) {
    //   this.utilService.onShowAlert("warning", "Atención", "No puede editar un registro ANULADO, PENDIENTE DE FIRMA o FIRMADO");
    //   return;
    // }
    this.comprobantePagoProceso.idMemo = this.selectedValues[0].idCompPago;
    this.comprobantePagoProceso.proceso  = 'editar';
    this.sharingInformationService.compartirComprobantePagoProcesoObservableData = this.comprobantePagoProceso;
    this.router.navigate([`SARF/mantenimientos/documentos/comprobantes_de_pago/editar/${ this.selectedValues[0].idCompPago}`]);
    this.sharingInformationService.irRutaBandejaComprobantePagoObservableData = false;
  }

  public autorizarModificacion() {
    if ( this.selectedValues.length === 0 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar mínimo un registro registro.");
      return;
    }

    // for ( let reg of this.selectedValues ) {
    //   if ( reg.esDocu != '003' ) {
    //     this.utilService.onShowAlert("warning", "Atención", "Solo puede autorizar la modificación de uno o más registros en estado FIRMADO.");
    //     return;
    //   }
    // }

    this.bodyCambiarEstado.tramaCuentaContable = this.selectedValues.map( ( value ) => {
      return { id: String( value.idCompPago ) }
    });

    const textoAnular = (this.selectedValues?.length === 1) ? 'del Comprobante' : 'de los Comprobantes';

    this.utilService.onShowConfirm(`¿Estás seguro que deseas AUTORIZAR la modificación ${ textoAnular } de Pago`).then( (result) => {
      if ( result.isConfirmed ) {
        this.utilService.onShowProcess('autorizar');

        this.comprobantePagoService.autorizarModificacionRegistros( this.bodyCambiarEstado ).subscribe({
          next: ( d: any ) => {
            if ( d.codResult < 0 ) {
              this.utilService.onShowAlert('info', 'Atención', d.msgResult);
            } else if ( d.codResult === 0 ) {
              this.utilService.onShowAlert('success', 'Atención', 'Se actualizaron los registros correctamente');
              this.selectedValues = [];
              this.buscarComprobante( false );
            }
          },
          error: ( e ) => {
            if ( e.error.category === 'NOT_FOUND' ) {
              this.utilService.onShowAlert('error', 'Atención', e.error.description);
            } else {
              this.utilService.onShowMessageErrorSystem();
            }
          }
        });
      } else if ( result.isDenied ) {
        this.utilService.onShowAlert('warning', 'Atención', 'No se pudieron guardar los cambios.');
      }
    });
  }

  sustentos() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Comprobante de Pago', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Comprobante de Pago', 'Solo puede visualizar un registro a la vez!', 'error');
      return;
    }

    const disableGrabar: boolean = this.showButtonAct( this.BtnAdicionar_Sustento_CP, this.codBandeja )
                                    ||
                                   this.showButtonAct( this.BtnAdicionar_Sustento_Otro, this.codBandeja);

    const dialog = this.dialogService.open( SustentosComprobanteComponent, {
      header: 'Comprobante de Pago - Sustentos',
      width: '45%',
      height: '80%',
      closable: false,
      data: {
        comp: this.selectedValues,
        add: this.BtnAdicionar_Sustento,
        delete: this.BtnEliminar_Sustento,
        addSiaf: this.BtnAdicionar_Sustento_SIAF,
        deleteSiaf: this.BtnEliminar_Sustento_SIAF,
        addCP: this.BtnAdicionar_Sustento_CP,
        deleteCP: this.BtnEliminar_Sustento_CP,
        addOtro: this.BtnAdicionar_Sustento_Otro,
        deleteOtro: this.BtnEliminar_Sustento_Otro,
        bandeja: this.codBandeja,
        disableButton: disableGrabar
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({
          severity: 'success',
          summary: 'Los sustentos han sido cargados y grabados correctamente',
          life: 5000
        });

        this.buscarComprobante(false);
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al cargar y grabar los sustentos',
          life: 5000
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }


  verDocumento() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Comprobante de Pago', 'Debe seleccionar un registro!', 'error');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      Swal.fire( 'Comprobante de Pago', 'Solo puede visualizar un registro a la vez!', 'error');
      return;
    }

	  const comprobante  = this.selectedValues;

    if ( comprobante[0].esDocu == '001' ) {
      this.comprobantePagoService.getComprobantePagoPdfById( comprobante['0'].idCompPago! ).subscribe({
        next: ( data ) => {

          this.guidDocumento = data.msgResult;

          const doc = data.archivoPdf;
          const byteCharacters = atob(doc!);
          const byteNumbers = new Array(byteCharacters.length);
          for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
          }
          const byteArray = new Uint8Array(byteNumbers);
          const blob = new Blob([byteArray], { type: 'application/pdf' });
          const blobUrl = URL.createObjectURL(blob);
          const download = document.createElement('a');
          download.href =  blobUrl;
          //download.setAttribute('download', 'Constacia_Abono');
          download.setAttribute("target", "_blank");
          document.body.appendChild( download );
          download.click();
        },
        error: ( e ) => {
          this.utilService.onShowAlert("warning", "Atención", e.error.description);
        }
      });
    }
    else {
      const blobUrl = this.generalService.downloadManager( comprobante[0].idGuidDocu );
      const download = document.createElement('a');
      download.href =  blobUrl;
      //download.setAttribute('download', 'Constacia_Abono');
      download.setAttribute("target", "_blank");
      document.body.appendChild( download );
      download.click();
    }

  }


  solicitarFirma() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro.');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Solo puede visualizar un registro a la vez.')
      return;
    }

    // if ( !(this.selectedValues['0'].esDocu == '001' || this.selectedValues['0'].esDocu == '006' || this.selectedValues['0'].esDocu == '007' || this.selectedValues['0'].esDocu == '004') ) {
    //   this.utilService.onShowAlert('warning', 'Atención', 'Solo puede Solicitar Firma a Comprobantes de Pago en estado REGISTRADO, NO UTILIZADO o RESERVADO');
    //   this.selectedValues = [];
    //   return;
    // }

	  // if ( this.selectedValues[0].deEsta === 'ANULADO' ) {
    //   this.utilService.onShowAlert("warning", "Atención", "No puede solicitar firma de un registro ANULADO");
    //   return;
    // }

	const comprobante  = this.selectedValues;
    this.comprobantePagoService.getComprobantePagoPdfById( comprobante['0'].idCompPago! ).subscribe({
      next: ( data ) => {

		this.guidDocumento = data.msgResult;

		const dialog = this.dialogService.open( SolicitarFirmaComprobanteComponent, {
		  header: 'Comprobante de Pago - Solicitar Firma',
		  width: '45%',
      closable: false,
		  data: {
			csta: this.selectedValues,
			guidDocumento: data.msgResult,
			doc: data.archivoPdf
		  }
		});

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {
        this.messageService.add({
          severity: 'success',
          summary: 'Se solicitó la firma del documento satisfactoriamente.',
          life: 5000
        });

        this.buscarComprobante(false);
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al solicitar la firma del documento.',
          life: 5000
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });

      },
      error: ( e ) => {
      }
    });


  }

  historialFirmas() {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro.');
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Solo puede visualizar un registro a la vez.');
      return;
    }

    // if ( !(this.selectedValues['0'].esDocu == '002' || this.selectedValues['0'].esDocu == '003' || this.selectedValues['0'].esDocu == '004') ) {
    //   Swal.fire( 'Solo puede ver el Historial de Firmas a Comprobantes de Pago en estado PENDIENTE DE FIRMA, FIRMADO o RECHAZADO', '', 'error');
    //   this.selectedValues = [];
    //   return;
    // }

    const dialog = this.dialogService.open( HistorialFirmasComponent, {
      header: 'Comprobante de Pago - Historial de Firmas',
      width: '50%',
      closable: false,
      data: {
        comp: this.selectedValues
      }
    });

    dialog.onClose.subscribe( () => {
      this.selectedValues = [];
    });
  }

  reservar() {
    const dialog = this.dialogService.open( ReservarComponent, {
      header: 'Comprobante de Pago - Reservar',
      width: '40%'
    });

    dialog.onClose.subscribe( (data) => {
      data && this.buscarComprobante(false);
      this.selectedValues = [];
    });
  }

  noUtilizado() {

    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro.");
      return;
    }

    // if ( this.selectedValues['0'].esDocu != '006' ) {
    //   Swal.fire( 'Solo puede modificar Comprobantes de Pago en estado RESERVADO', '', 'error');
    //   this.selectedValues = [];
    //   return;
    // }

    const dialog = this.dialogService.open( NoUtilizadoComponent, {
      header: 'Cambiar a estado NO UTILIZADO',
      width: '48%',
      closable: false,
      data: {
        comprobantes: this.selectedValues
      }
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject' ) => {
      if ( type == 'accept' ) {

        this.buscarComprobante(false);
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.utilService.onShowAlert('error', 'Atención', 'Hubo un error al realizar el cambio.');

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  generarCheques() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para generar cheque.");
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
      return;
    }

    const dialog = this.dialogService.open( GenerarChequesComponent, {
      data: this.selectedValues[0],
      header: 'Comprobante de Pago - Generar Cheques',
      width: '35%'
    });

    dialog.onClose.subscribe( () => {
      this.selectedValues = [];
    });
  }

  copiar() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para copiarlo.");
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
      return;
    }

    const dialog = this.dialogService.open( CopiarComprobanteComponent, {
      data: this.selectedValues[0],
      header: 'Comprobante de Pago - Copiar',
      width: '40%'
    });

    dialog.onClose.subscribe( (data) => {
      data && this.buscarComprobante(false);
      this.selectedValues = [];
    });
  }

  bandejaCheques() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para ver cheques.");
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
      return;
    }

    this.sharingInformationService.compartirComprobantePagoObservableData = this.selectedValues[0];
    this.router.navigate(['SARF/mantenimientos/documentos/comprobantes_de_pago/cheques/bandeja']);
    this.sharingInformationService.irRutaBandejaComprobantePagoObservableData = false;
  }

  anular() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
      return;
    }

    const idCartasOrdenes = this.selectedValues?.map( value => {
      const trama = {
        id: value.idCompPago?.toString()
      }
        return trama
    });
    const textoAnular = (this.selectedValues?.length === 1) ? 'el registro' : 'los registros';
    this.utilService.onShowConfirm(`¿Estás seguro que desea anular ${textoAnular}?`).then((result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('anular');
        this.bodyEliminarActivarComprobantePago.tramaCuentaContable = idCartasOrdenes;
        this.comprobantePagoService.anularComprobantePago(this.bodyEliminarActivarComprobantePago).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0 ) {
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
            } else if (d.codResult === 0 ) {
              this.selectedValues = [];
              this.buscarComprobante(false);
            }
          },

          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", d.error.description);

            }else {
              this.utilService.onShowMessageErrorSystem();
            }
          }
        });

      } else if (result.isDenied) {
        this.utilService.onShowAlert("warning", "Atención", "No se pudieron guardar los cambios.");
      }
    })
  }

  activar() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
      return;
    }
    const idCartasOrdenes = this.selectedValues?.map( value => {
      const trama = {
        id: value.idCompPago?.toString()
      }
        return trama
    });
    const textoActivar = (this.selectedValues?.length === 1) ? 'el registro' : 'los registros';
    this.utilService.onShowConfirm(`¿Estás seguro que desea activar ${textoActivar}?`).then((result) => {
      if (result.isConfirmed) {
        this.utilService.onShowProcess('activar');
        this.bodyEliminarActivarComprobantePago.tramaCuentaContable = idCartasOrdenes;
        this.comprobantePagoService.activarComprobantePago(this.bodyEliminarActivarComprobantePago).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0) {
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
            } else if (d.codResult === 0) {
              this.selectedValues = [];
              this.buscarComprobante(false);
            }
          },
          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              Swal.fire( 'Carta orden', `${d.error.description}`, 'info');
              this.utilService.onShowAlert("error", "Atención", d.error.description);
            }else {
              this.utilService.onShowMessageErrorSystem();
            }
          }
        });

      } else if (result.isDenied) {
        this.utilService.onShowAlert("warning", "Atención", "No se pudieron guardar los cambios.");
      }
    });
  }

  public consultar() {
    if (this.selectedValues?.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para consultarlo.");
      return;
    }

    if ( this.selectedValues.length != 1 ) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede consultar un registro a la vez.");
      return;
    }
    this.comprobantePagoProceso.idMemo = this.selectedValues[0].idCompPago;
    this.comprobantePagoProceso.proceso = 'consultar';
    this.sharingInformationService.compartirComprobantePagoProcesoObservableData = this.comprobantePagoProceso;
    this.router.navigate([`SARF/mantenimientos/documentos/comprobantes_de_pago/consultar/${ this.selectedValues[0].idCompPago }`])
    this.sharingInformationService.irRutaBandejaComprobantePagoObservableData = false;
  }

  exportExcel() {
    if ( this.comprobantesList.reporteExcel === null || this.comprobantesList.reporteExcel === 'null' ) {
      return;
    }

    const fechaHoy = new Date();
    const fileName = this.validateService.formatDateWithHoursForExcel( fechaHoy );

    const byteCharacters = atob(this.comprobantesList.reporteExcel);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute('download', `ComprobantePago_${ fileName }`);
    document.body.appendChild( download );
    download.click();
  }

  customSort( event: SortEvent ) {

    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  private establecerDataFiltrosRecibos() : IDataFiltrosRecibo {
    const dataFiltrosRecibos: IDataFiltrosRecibo = {
      listaZonaRegistral: this.listaZonaRegistralFiltro,
      listaOficinaRegistral: this.listaOficinaRegistralFiltro,
      listaLocal: this.listaLocalFiltro,
      listaTipoRecibo: this.listaTipoReciboFiltro,
      listaEstadoRecibo: this.listaEstadoReciboFiltro,
      listaMediosPago: this.listaMediosPagoFiltro,
      bodyRecibo: this.bodyComprobantePago,
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosRecibos;
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  public validarNumeroCuenta(event:KeyboardEvent){
    const pattern = /[0-9-]+/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
  }

  public validarMonto(event:any){
    const pattern = /^(-?\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public formatearNumero(event:any){
    if (event.target.value) {
      const numMonto = this.funciones.convertirTextoANumero( event.target.value )
      this.comprobante.get('monto')?.setValue( Number(numMonto).toFixed(2) );
    }
  }

  public validarBotonComprobante( tipoVali: string ) {
    this.bodyValidarComprobante.tiVali = tipoVali;
    this.bodyValidarComprobante.tramaValidacion = this.selectedValues?.map( value => { return { id: value.idCompPago?.toString() } });

    this.comprobantePagoService.validarAccionComprobantePago( this.bodyValidarComprobante ).pipe( takeUntil(this.unsubscribe$) )
        .subscribe({
          next: ( data ) => {
            if ( data.codResult < 0 ) {
              this.utilService.onShowAlert("warning", "Atención", data.msgResult);
              return;
            }

            this._realizarAccionBoton( tipoVali );
          },
          error: ( err ) => {
            if ( err.error.category === "NOT_FOUND" ) {
              this.utilService.onShowAlert("info", "Atención", err.error.description);
            } else {
              this.utilService.onShowMessageErrorSystem();
            }
          }
        });
  }

  private _realizarAccionBoton( tipoVali: string ) {
    switch ( tipoVali ) {
      case this.NUEVO:
        this.nuevoComprobante();
          break;
      case this.EDITAR:
        this.editarComprobantePago();
          break;
      case this.ANULAR:
        this.anular();
          break;
      case this.ACTIVAR:
        this.activar();
          break;
      case this.CONSULTAR:
        this.consultar();
          break;
      case this.AUTORIZAR_MODIFICACION:
        this.autorizarModificacion();
          break;
      case this.VER_DOCUMENTO:
        this.verDocumento();
          break;
      case this.VER_SUSTENTO:
        this.sustentos();
          break;
      case this.SOLICITAR_FIRMA:
        this.solicitarFirma();
          break;
      case this.VER_HISTORIAL:
        this.historialFirmas();
          break;
      case this.COPIAR:
        this.copiar();
          break;
      case this.RESERVAR:
        this.reservar();
          break;
      case this.NO_UTILIZADO:
        this.noUtilizado();
          break;
      case this.GENERAR_CHEQUES:
        this.generarCheques();
          break;
      case this.VER_CHEQUES:
        this.bandejaCheques();
          break;
      case this.EXPORTAR_EXCEL:
        this.exportExcel();
          break;
      default:
        this.buscarComprobante(false);
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.comprobantePagoService$.unsubscribe();
    this.sharingInformationService.compartirDataFiltrosComprobantePagoObservableData = this.establecerDataFiltrosRecibos();
  }

}
