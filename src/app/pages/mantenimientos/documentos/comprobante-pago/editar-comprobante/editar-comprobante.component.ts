import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Observable, of, Subject, Subscription } from 'rxjs';
import { catchError, first, map, takeUntil } from 'rxjs/operators';
import { Dropdown } from 'primeng/dropdown';

import { environment } from '../../../../../../environments/environment.prod';
import { GeneralService } from 'src/app/services/general.service';
import { ReciboIngresoService } from 'src/app/services/mantenimientos/documentos/recibo-ingreso.service';
import { BodyEditarComprobantePago, ITrama } from 'src/app/models/mantenimientos/documentos/comprobante-pago.model';
import { ComprobantePagoService } from 'src/app/services/mantenimientos/documentos/comprobante-pago.service';
import { UtilService } from 'src/app/services/util.service';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { ComprobantePagoValue, IComprobantePago, IComprobantePagoById, IDataFiltrosRecibo, ITipoRecibo } from 'src/app/interfaces/comprobante-pago.interface';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { CuentaContableService } from 'src/app/services/mantenimientos/maestras/cuenta-contable.service';
import { PlanCuentaContable } from 'src/app/models/mantenimientos/maestras/plan-cuenta.model';
import { ReciboIngreso } from 'src/app/models/mantenimientos/documentos/recibo-ingreso.model';
import { ClasificadorService } from '../../../../../services/mantenimientos/maestras/clasificador.service';
import { IClasificador } from '../../../../../interfaces/configuracion/clasificador.interface';
import { ITramaClasificador, ITramaMediosPago, BodyBeneficiarioBandeja } from '../../../../../models/mantenimientos/documentos/comprobante-pago.model';
import { DialogService } from 'primeng/dynamicdialog';
import { VentBeneficiariosComponent } from '../vent-beneficiarios/vent-beneficiarios.component';
import { VentTipoOperacionComponent } from '../vent-tipo-operacion/vent-tipo-operacion.component';
import { IBeneficiarioBandeja, ITipoOperacionBandeja, IMediosPago } from '../../../../../interfaces/comprobante-pago.interface';
import { ConfigDocumentoService } from '../../../../../services/mantenimientos/configuracion/config-documento.service';


@Component({
  selector: 'app-editar-comprobante',
  templateUrl: './editar-comprobante.component.html',
  styleUrls: ['./editar-comprobante.component.scss'],
  providers: [
    DialogService
  ]
})
export class EditarComprobanteComponent implements OnInit, OnDestroy {
  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;
  @ViewChild('ddlTipoComprobante') ddlTipoComprobante: Dropdown;
  @ViewChild('ddlTipoReferencia') ddlTipoReferencia: Dropdown;
  @ViewChild('ddlBancoCargo') ddlBancoCargo: Dropdown;
  @ViewChild('ddlCuentaAbono') ddlCuentaAbono: Dropdown;
  @ViewChild('imCompPago') imCompPago: ElementRef;
  @ViewChild('idRazoSoc') idRazoSoc: ElementRef;
  @ViewChild('numRxH') numRxH: ElementRef;
  cuentasContables: PlanCuentaContable[];
  clasificadores: IClasificador[];
  checkedString: string;
  disableTipoDoc: boolean = true;
  loadingZonaRegistral: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  loadingTipoComprobante: boolean = false;
  loadingTipoReferencia: boolean = false;
  loadingBancoCargo: boolean = false;
  loadingCuentaCargo: boolean = false;
  loadingCuentaBancaria: boolean = false;
  loadingCuentaAbono: boolean = false;
  loadingMediosPago: boolean = false;
  focusRazoSoc: boolean = false;
  bodyEditarComprobantePago: BodyEditarComprobantePago;
  bodyBeneficiario!: BodyBeneficiarioBandeja;
  tramaContable: ITrama[] = [];
  tramaClasificador: ITramaClasificador[] = [];
  tramaMedioPago: ITramaMediosPago[] = [];
  userCode: any;
  fechaActual: Date;
  editarComprobante!: FormGroup;
  unsubscribe$ = new Subject<void>();
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[] = [];
  listaLocal: Local[] = [];
  listaTipoRecibo: ReciboIngreso[] = [];
  listaEstadoRecibo: IResponse2[] = [];
  listaBancoCargo: any[] = [];
  listaBancoAbono: any[] = [];
  comprobantePagoData: IComprobantePagoById;
  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaTipoReciboFiltro: ComprobantePagoValue[];
  listaEstadoReciboFiltro: IResponse2[];
  listaMediosPagoFiltro: IResponse2[];

  readTipoRecibo!: boolean;

  $tipoDocReferencia: Observable<any>;
  $mediosDePago: Observable<any>;

  $createComprobantePago: Subscription = new Subscription;

  mediosItem: any = { ccodigoHijo: '*', cdescri: '(NINGUNO)' };

  constructor( 
    private router: Router,
    private fb: FormBuilder,
    private funciones: Funciones,  
    private utilService: UtilService,
    private dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private generalService: GeneralService,
    private reciboService: ReciboIngresoService,
    private contableService: CuentaContableService,
    private clasificadorService: ClasificadorService,
    private configuracionService: ConfigDocumentoService,
    private comprobantePagoService: ComprobantePagoService,
    private sharingInformationService: SharingInformationService,
  ) { }

  ngOnInit(): void {
    this.userCode = localStorage.getItem('user_code')||'';
    this.bodyEditarComprobantePago = new BodyEditarComprobantePago();
    this._getIdByRoute();
    this._contables();
    this._clasificadores();
    this.fechaActual = new Date();
    this.editarComprobante = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoComp: ['', [ Validators.required ]],
      nroComp: [{value: '', disabled: true}, []],
      fecha: [this.fechaActual, [ Validators.required ]],
      banco: ['', [ Validators.required ]],
      nroCuenta: ['', [ Validators.required ]],
      nroSIAF: ['', []],
      monto: ['', [ Validators.required ]],
      tipoOper: ['', []],
      nuDocu: ['', []],
      noRazoSoc: ['', [ Validators.required ]],
      nuRxH: ['', []],
      fechaRxH: ['', []],
      checked: [false, []],
      concepto: ['', []],
      detallePresupuestal: this.fb.array([]),
      detallePatrimonial: this.fb.array([]),
      mediosPago: this.fb.array([])
    });
  }

  private _getIdByRoute(): void {
    this.activatedRoute.params.pipe(takeUntil(this.unsubscribe$))
      .subscribe( params => this._getComprobantePagoById(params['id']))
  }

  private _getComprobantePagoById(id: number): void {
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.comprobantePagoService.getComprobantePagoById(id).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          console.log('dataComp', data);
          let flag = true;
          this.readTipoRecibo = true;
          if (data.deEsta === 'RESERVADO' || data.deEsta === 'NO UTILIZADO') {
            this.listaTipoRecibo = [];
            this.listaTipoRecibo.push({ idTipo: 0, noTipo: '(SELECCIONAR)' });
            flag = false;
            this.readTipoRecibo = false;
          }
          this.comprobantePagoData = data;
          this._disableInputs();
          this._inicilializarListas(flag);
          this._buscarTipoComprobante();
          this._buscarBanco();
          this._buscarMediosPago();
          this.utilService.onCloseLoading();
        },
        error: ( err ) => {
          this.utilService.onCloseLoading();
        }
      })
  }

  private _contables(): void {
    this.contableService.getBandejaCuentaContable('', '', 'A').pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.cuentasContables = data
        }
      });
  }

  private _clasificadores(): void {
    this.clasificadorService.getBandejaClasificadores(null, null, 'A').subscribe({
      next: ( data: IClasificador[] ) => {
        this.clasificadores = data;
      }
    });
  }
  
  private _disableInputs(): void {
    this.editarComprobante.get('zona')?.disable();
    this.editarComprobante.get('oficina')?.disable();
    this.editarComprobante.get('local')?.disable();
    this.editarComprobante.get('tipoComp')?.disable();
    this.editarComprobante.get('nroComp')?.disable();

    this.editarComprobante.get('nroComp')?.setValue( this.comprobantePagoData.nuCompPago );
    this.editarComprobante.patchValue({ "fecha": new Date(this.comprobantePagoData.feCompPago) });
    this.onChangeBanco({value: this.comprobantePagoData.idBanc}, true);
    this.editarComprobante.get('nroSIAF')?.setValue( this.comprobantePagoData.nuSiaf );
    this.editarComprobante.get('monto')?.setValue( this.comprobantePagoData.imCompPago?.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    }));

    this.editarComprobante.get('tipoOper')?.setValue( this.comprobantePagoData.noTipoOper );
    
    this.editarComprobante.get('nuDocu')?.setValue( this.comprobantePagoData.nuBene );
    this.editarComprobante.get('noRazoSoc')?.setValue( this.comprobantePagoData.noBene );

    const retencionRecibo = this.comprobantePagoData.inReciHonoRete === '1' ? true : false;
    this.editarComprobante.get('nuRxH')?.setValue( this.comprobantePagoData.nuReciHono.trim() );
    this.comprobantePagoData.feReciHono ? this.editarComprobante.patchValue({ "fechaRxH": new Date(this.comprobantePagoData.feReciHono)}) : this.editarComprobante.get('fechaRxH')?.setValue( null );
    this.editarComprobante.get('checked')?.setValue( retencionRecibo );

    this.editarComprobante.get('concepto')?.setValue( this.comprobantePagoData.obConc);

    this.comprobantePagoData.documentos.map( mp => {
      const bloquear: boolean = (mp.tiDocu === '003' || mp.caDocu === '0') ? true : false;
      const bloqNum: boolean = mp.tiDocu === '003' ? true : false;
      const caDocu: string = mp.caDocu === '0' ? '' : mp.caDocu;

      this.medioPago.push(
        this.fb.group({
          id: [ mp.idCompPagoDocu ],
          tipoDocu: [{ value: mp.tiDocu, disabled: bloqNum}, []],
          cantidad: [{ value: caDocu, disabled: bloquear}, []],
          numero: [{ value: mp.nuDocu, disabled: bloqNum}, []],
          importe: [ Number( mp.imDocu )?.toLocaleString( 'en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          }), []]
        })
      )
    });
  }

  private _buscarTipoComprobante(): void {
    this.loadingTipoComprobante = true;
    this.reciboService.getTipoReciboIngreso('2').pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.loadingTipoComprobante = false;
          this.listaTipoRecibo.push(...data);
          
          this.editarComprobante.patchValue({'tipoComp': this.comprobantePagoData.idTipoCompPago });
        },
        error:( err )=>{
          this.loadingTipoComprobante = false;    
        }
      });
  }

  private _inicilializarListas(flag: boolean) {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    if (flag) {
      this.listaTipoRecibo = [];
    }
    this.listaEstadoRecibo = [];
    this.listaBancoCargo = [];
    this.listaBancoAbono = [];
    this.listaZonaRegistral.push({ coZonaRegi: this.comprobantePagoData.coZonaRegi, deZonaRegi: this.comprobantePagoData.deZonaRegi });
    this.listaOficinaRegistral.push({ coOficRegi: this.comprobantePagoData.coOficRegi, deOficRegi: this.comprobantePagoData.deOficRegi });
    this.listaLocal.push({ coLocaAten: this.comprobantePagoData.coLocaAten, deLocaAten: this.comprobantePagoData.deLocaAten });
    this.listaEstadoRecibo.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' });   
    this.listaBancoCargo.push({ nidInstitucion: '*', cdeInst: '(SELECCIONAR)' });   
    this.listaBancoAbono.push({ idBanc: '*', nuCuenBanc: '(SELECCIONAR)' });
    
    this.comprobantePagoData.cuentasClasificador.map( clsf => {
      this.cuentaPres.push( 
        this.fb.group({ 
          id: [ clsf.idCompPagoClsf ],
          clasificador: [ clsf.coClsf, [ Validators.required ]],
          detClasificador: [ clsf.deClsf, [ Validators.required ]],
          debe: [ Number( clsf.imDebe )?.toLocaleString('en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          }), [ Validators.required ]],
          haber: [ Number( clsf.imHabe )?.toLocaleString('en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          }), [ Validators.required ]]
        })
      )
    });
    
    this.comprobantePagoData.cuentasContables.map( cont => {
      this.cuentaPatr.push( 
        this.fb.group({ 
          id: [ cont.idCompPagoCont ],
          cuenta: [ cont.coCuenCont, [ Validators.required ]],
          descripcion: [ cont.noCuenCont, [ Validators.required ]],
          debe: [ cont.imDebe?.toLocaleString('en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          }), [ Validators.required ]],
          haber: [ cont.imHabe?.toLocaleString('en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          }), [ Validators.required ]]
        })
      )
    });
  }

  public onChangeTipoComprobante( event: any): void {
    this.cuentaPres.clear();
    this.cuentaPatr.clear();

    const tipoComprobante = this.listaTipoRecibo.find( (item: ReciboIngreso) => item.idTipo === event.value);

    if ( tipoComprobante.idTipo === 0 ) {
      this.editarComprobante.get('banco')?.setValue(0);
      this.editarComprobante.get('nroCuenta')?.setValue('');
      this.editarComprobante.get('concepto')?.setValue('');
      return;
    }

    this.editarComprobante.get('concepto')?.setValue( tipoComprobante.obConc );
    this.editarComprobante.patchValue({ "banco": tipoComprobante.idBanc });
    this._seleccionarCuenta(tipoComprobante.nuCuenBanc);

    this.configuracionService.getConfiguracionById('2', tipoComprobante.idTipo).subscribe({
      next: ( data ) => {
        data.listTipoDocumentoClsf.forEach( clsf => {
          this.cuentaPres.push( 
            this.fb.group({ 
              clasificador: [ clsf.coClsf, [ Validators.required ]],
              detClasificador: [ {
                value: clsf.deClsf,
                disabled: true 
            }, [ Validators.required ]],
              debe: [ Number('0').toFixed(2), [ Validators.required ]],
              haber: [ Number('0').toFixed(2) , [ Validators.required ]],
              id:[ 0, [ Validators.required]],

            })
          )
        });

        data.listTipoDocumentoCont.forEach( cont => {
          this.cuentaPatr.push( 
            this.fb.group({ 
              cuenta: [ cont.coCuenCont, [ Validators.required ]],
              descripcion: [ {
                value:  cont.noCuenCont,
                disabled: true 
            }, [ Validators.required ]],
              debe: [ Number('0').toFixed(2), [ Validators.required ]],
              haber: [ Number('0').toFixed(2), [ Validators.required ]],
              id:[ 0, [ Validators.required]],
            })
          )
        });
      },
      error: ( e ) => {
        this.cuentaPres.clear();
        this.cuentaPatr.clear();
      }
    });  
  }

  private _seleccionarCuenta(nuCuenBanc: string) {  
    this.loadingCuentaAbono = true;
    this.listaBancoAbono = [];
    this.listaBancoAbono.push({ idBanc: '*', nuCuenBanc: '(SELECCIONAR)' });
    this.editarComprobante.get('nroCuenta')?.setValue('');
    if (this.editarComprobante.get('banco')?.value !== "*") {
      this.generalService.getCbo_Cuentas(this.editarComprobante.get('banco')?.value).pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: any[]) => {
        this.listaBancoAbono.push(...data);
        this.editarComprobante.patchValue({ "nroCuenta": nuCuenBanc});
        this.loadingCuentaAbono = false;
      }, (err) => {
        this.loadingCuentaAbono = false;                                 
      });      
    } else {
      this.loadingCuentaAbono = false;
    } 
  }

  private _buscarBanco(): void {
    this.loadingBancoCargo = true;
    this.generalService.getCbo_Bancos().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.listaBancoCargo.push(...data); 
        this.editarComprobante.patchValue({'banco': this.comprobantePagoData.idBanc });
        this.loadingBancoCargo = false;
      },
      error:( err )=>{
        this.loadingBancoCargo = false;    
      }
    });
  }

  public onChangeBanco(idBanc: any, cargaInicial: boolean): void {
    this.loadingCuentaAbono = true;

    this.listaBancoAbono = [];
    this.listaBancoAbono.push({ idBanc: '*', nuCuenBanc: '(SELECCIONAR)' });
    if (idBanc.value !== 0) {
      this.generalService.getCbo_Cuentas(idBanc.value).pipe(takeUntil(this.unsubscribe$))
        .subscribe( 
          (data: any[]) => {
            this.listaBancoAbono.push(...data);
            this.editarComprobante.patchValue({'nroCuenta': this.comprobantePagoData.nuCuenBanc });
            cargaInicial;
            this.loadingCuentaAbono = false;            
          }, 
          (err) => {       
            this.loadingCuentaAbono = false;
        });   
    } else {
      this.loadingCuentaAbono = false;
    }
  }

  private _buscarMediosPago() {
    this.loadingMediosPago = true;
    this.$mediosDePago = this.generalService.getCbo_MediosDePago().pipe(
      map( mp => {
        mp.unshift( this.mediosItem );
        this.loadingMediosPago = false;
        return mp
      })
    );
  }

  public onChangeMediosPago( event: any, form: any, index: number ) {
    if ( event.value !== '*' ) {
      form.controls.mediosPago.controls[index].controls.numero.enable();
      form.controls.mediosPago.controls[index].controls.importe.enable();

      if ( event.value === '003' ) {
        form.controls.mediosPago.controls[index].controls.cantidad.enable();

        form.controls.mediosPago.controls[index].controls.numero.disable();
        form.controls.mediosPago.controls[index].controls.numero.setValue('');
      } else {
        form.controls.mediosPago.controls[index].controls.numero.enable();

        form.controls.mediosPago.controls[index].controls.cantidad.disable();
        form.controls.mediosPago.controls[index].controls.cantidad.setValue('');
      }
    } else {
      form.controls.mediosPago.controls[index].controls.numero.disable();
      form.controls.mediosPago.controls[index].controls.numero.setValue('');

      form.controls.mediosPago.controls[index].controls.importe.disable();
      form.controls.mediosPago.controls[index].controls.importe.setValue('');
      
      form.controls.mediosPago.controls[index].controls.cantidad.disable();
      form.controls.mediosPago.controls[index].controls.cantidad.setValue('');
    }
  }
  
  get detallePresupuestalArray(): FormArray | null { return  this.editarComprobante.get('detallePresupuestal') as FormArray }
  get detallePatrimonialArray(): FormArray | null { return  this.editarComprobante.get('detallePatrimonial') as FormArray }
  get cuentasPresupuestales(): FormGroup[] {
    return <FormGroup[]>this.detallePresupuestalArray?.controls
  }
  get cuentasPatrimoniales(): FormGroup[] {
    return <FormGroup[]>this.detallePatrimonialArray?.controls
  }
  get cuentaPres(): FormArray { return <FormArray>this.editarComprobante.get('detallePresupuestal') }
  get cuentaPatr(): FormArray { return <FormArray>this.editarComprobante.get('detallePatrimonial')}

  get mediosDePagoArray(): FormArray | null { return this.editarComprobante.get('mediosPago') as FormArray }
  get mediosDePago(): FormGroup[] {
    return <FormGroup[]>this.mediosDePagoArray?.controls
  }
  get medioPago(): FormArray { return <FormArray>this.editarComprobante.get('mediosPago') }

  public addcuentaPresupuestal() {
    const ar = ( this.editarComprobante.get('detallePresupuestal') as FormArray )
    if ( environment.cantidadCuentas > ar.length )
      ar.push(
        this.fb.group({
          id: [0],
          clasificador: ['', [ Validators.required ]],
          detClasificador: ['', [ Validators.required ]],
          debe: ['', [ Validators.required ]],
          haber: ['', [ Validators.required ]]
        })
      )
  }

  public deletecuentaPresupuestal( index: number ) {
    ( this.editarComprobante.get('detallePresupuestal') as FormArray ).removeAt(index)
  }

  public addcuentaPatrimonial() {
    const ar = ( this.editarComprobante.get('detallePatrimonial') as FormArray )
    if ( environment.cantidadCuentas > ar.length )
      ar.push(
        this.fb.group({
          id:[0],
          cuenta: ['', [ Validators.required ]],
          descripcion: ['', [ Validators.required ]],
          debe: ['', [ Validators.required ]],
          haber: ['', [ Validators.required ]]
        })
      )
  }

  public deletecuentaPatrimonial( index: number ) {
    ( this.editarComprobante.get('detallePatrimonial') as FormArray ).removeAt(index)
  }

  public addMediosDePago() {
    const ar = ( this.editarComprobante.get('mediosPago') as FormArray )
    if ( ar.length < 4 )
      ar.push(
        this.fb.group({
          id: [0],
          tipoDocu: ['', []],
          cantidad: [{value: '', disabled: true}, []],
          numero: [{value: '', disabled: true}, []],
          importe: [{value: '', disabled: true}, []]
        })
      )
  }

  public deleteMediosPago( index: number ) {
    ( this.editarComprobante.get('mediosPago') as FormArray ).removeAt(index)
  }

  private _buscarClasificadores(event:any,$tipoCuenta:string,i:number){
    if (event.target.value !== '') {
      const controlName = ( this.editarComprobante.get($tipoCuenta) as FormArray );
      const valorArr = controlName.value;
      const valorArrAux = valorArr.map(( val: any, indx: number ) => {
        const clsfDes = this.clasificadores.find((clsf: any) => clsf.coClsf === val.clasificador);
        val.detClasificador =  clsfDes ? clsfDes.deClsf : val.deClsf;

        const debe = this.funciones.convertirTextoANumero(val.debe);
        val.debe  = Number(debe).toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });

        const haber = this.funciones.convertirTextoANumero(val.haber);
        val.haber = Number(haber).toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        return val;
      });
      controlName.setValue(valorArrAux);
    }
  }

  public buscarCuentabancariaPresupuestal (event:KeyboardEvent,$event1:any,i:number){
    // const pattern = /[0-9]/;
    // const inputChar = String.fromCharCode(event.charCode);
    // if (!pattern.test(inputChar)) {
    //     event.preventDefault();
    // }
    if (event.keyCode === 13) {
      this._buscarClasificadores($event1,'detallePresupuestal',i);
    }
  }

  public buscarCuentabancariaPresupuestalBlur(event:any,i:number){
    this._buscarClasificadores(event,'detallePresupuestal',i);
  }

  private _buscarCuentaContable(event:any,$tipoCuenta:string,i:number): void {
    if (event.target.value !== '') {
      const controlName = ( this.editarComprobante.get($tipoCuenta) as FormArray );
      const valorArr = controlName.value;
      const valorArrAux= valorArr.map((val:any,indx:number)=>{
        const cuentaDes = this.cuentasContables.find((cuent:any)=>  Number(cuent.coCuenCont) === Number(val.cuenta)) ;
        val.descripcion =  cuentaDes ? cuentaDes.noCuenCont : val.descripcion;

        const debe = this.funciones.convertirTextoANumero(val.debe);
        val.debe  = Number(debe).toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });

        const haber = this.funciones.convertirTextoANumero(val.haber);
        val.haber = Number(haber).toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        return val;
      });
      controlName.setValue(valorArrAux);
    }
  }

  public buscarCuentabancariaPatrimoniales (event:KeyboardEvent,$event1:any,i:number): void {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
    if (event.keyCode === 13) {
      this._buscarCuentaContable($event1,'detallePatrimonial',i);
    }
  }

  public buscarCuentabancariaPatrimonialesBlur(event:any,i:number): void {
    this._buscarCuentaContable(event,'detallePatrimonial',i);
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  public validarMedioPagoPattern( event: any ) {
    const pattern = /[0-9, -]+/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  public validarMonto(event:any){
    const pattern = /^(\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public formatearNumero(event:any){
    if (event.target.value) {
      const nuevoValor = this.funciones.convertirTextoANumero( event.target.value );
      const number = Number(nuevoValor).toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });

      this.editarComprobante.get('monto')?.setValue( number );
    }
  }

  public formatearNumeroHabe(event:any){
    if (event.target.value) {
      const nuevoValor = this.funciones.convertirTextoANumero( event.target.value );
      event.target.value = Number(nuevoValor).toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });
    } 
  }

  public obtenerBeneficiarioPorDocumento( event: any ) {
    this.bodyBeneficiario = new BodyBeneficiarioBandeja();

    if ( event.target.value === '' ) {
      return;
    }

    this.bodyBeneficiario.nuBene = event.target.value;
    this.bodyBeneficiario.noBene = '';

    this.comprobantePagoService.getBeneficiariosById( this.bodyBeneficiario ).pipe( takeUntil(this.unsubscribe$) )
        .subscribe({
          next: ( data ) => {
            this.editarComprobante.get('nuDocu')?.setValue( data.nuBene );
            this.editarComprobante.get('noRazoSoc')?.setValue( data.noBene );
          },
          error: ( _err ) => {
          }
        });
  }

  public obtenerBeneficiarioPorNombre( event: any ) {
    this.bodyBeneficiario = new BodyBeneficiarioBandeja();

    if ( event.target.value === '' ) {
      return;
    }

    this.bodyBeneficiario.nuBene = '';
    this.bodyBeneficiario.noBene = event.target.value;

    this.comprobantePagoService.getBeneficiariosById( this.bodyBeneficiario ).pipe( takeUntil(this.unsubscribe$) )
        .subscribe({
          next: ( data ) => {
            this.editarComprobante.get('nuDocu')?.setValue( data.nuBene );
            this.editarComprobante.get('noRazoSoc')?.setValue( data.noBene );
          },
          error: ( _err ) => {
          }
        });
  }

  public async editarCarta() {
    this.tramaContable = [];
    this.tramaClasificador = [];
    this.tramaMedioPago = [];

    let resultado: boolean = this.validarFormulario();
    if (!resultado) {
      return;
    }

    const dataFiltrosRecibo: IDataFiltrosRecibo = await this.sharingInformationService.compartirDataFiltrosComprobantePagoObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecibo.esBusqueda) {
      dataFiltrosRecibo.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosComprobantePagoObservableData = dataFiltrosRecibo;
    }
    this.utilService.onShowProcessSaveEdit(); 
    const zona = this.editarComprobante.get('zona')?.value;
    const oficina = this.editarComprobante.get('oficina')?.value;
    const local = this.editarComprobante.get('local')?.value;
    const fecha = this.editarComprobante.get('fecha')?.value;
    const fechaFormat = (fecha !== "" && fecha !== null) ? this.funciones.convertirFechaDateToString(fecha) : '';
    const checked = this.editarComprobante.get('checked')?.value;
    const valueChecked = checked ? '1' : '0';
    const fechaRxH = this.editarComprobante.get('fechaRxH')?.value;
    const fechaFormatRxH = (fechaRxH !== "" && fechaRxH !== null) ? this.funciones.convertirFechaDateToString(fechaRxH) : '';
    const detallePresupuestal = this.editarComprobante.get('detallePresupuestal')?.value;
    const detallePatrimonial = this.editarComprobante.get('detallePatrimonial')?.value;
    const mediosDePago = this.editarComprobante.get('mediosPago')?.value;
    const montoCompPago = this.funciones.convertirTextoANumero( this.editarComprobante.get('monto')?.value );
    
    const documento: IMediosPago = this.comprobantePagoData.documentos.find( mp => mp.tiDocu === '003' );
    
    mediosDePago.map( (mp: any) => {
      if ( mp.tipoDocu !== '' || mp.tipoDocu !== '*' ) {
        const importe = this.funciones.convertirTextoANumero(mp.importe);

        let cantidad: string = '';
        if (mp.cantidad === undefined && mp.tipoDocu === undefined) {
          cantidad = documento.caDocu;
        } else if ( mp.tipoDocu === '003' ) {
          cantidad = mp.cantidad;
        } else {
          cantidad = '0'
        }

        this.tramaMedioPago.push({
          id: mp.id ? mp.id : 0,
          tiDocu: mp.tipoDocu === undefined ? documento.tiDocu : mp.tipoDocu,
          caDocu: cantidad,
          nuDocu: (mp.numero === undefined && mp.tipoDocu === undefined) ? documento.nuDocu : mp.numero,
          imDocu: Number(importe).toFixed(2)
        });
      }
    });

    detallePresupuestal.map( (clsf: any) => {
      if(clsf.clasificador !== '' &&  clsf.detClasificador !== '') {
        const haber = this.funciones.convertirTextoANumero(clsf.haber);
        const debe = this.funciones.convertirTextoANumero(clsf.debe);

        this.tramaClasificador.push({
          id: clsf.id ? clsf.id : 0,
          coClsf: clsf.clasificador,
          imHabe: Number(haber).toFixed(2),
          imDebe: Number(debe).toFixed(2)
        });
      }
    });

    detallePatrimonial.map( (cont: any) => {
      if(cont.cuenta !== '' &&  cont.descripcion !== '') {
        const haber = this.funciones.convertirTextoANumero(cont.haber);
        const debe = this.funciones.convertirTextoANumero(cont.debe);

        this.tramaContable.push({
          id: cont.id ? cont.id : 0,
          coCuenCont: cont.cuenta,
          imHabe: Number(haber).toFixed(2),
          imDebe: Number(debe).toFixed(2)
        });
      }
    })
    
    this.bodyEditarComprobantePago.idCompPago = this.comprobantePagoData.idCompPago;
    this.bodyEditarComprobantePago.coZonaRegi = zona === '*' ? '' : zona;
    this.bodyEditarComprobantePago.coOficRegi = oficina === '*' ? '' : oficina;
    this.bodyEditarComprobantePago.coLocaAten = local === '*' ? '' : local;
    this.bodyEditarComprobantePago.idTipoCompPago = this.editarComprobante.get('tipoComp')?.value;
    this.bodyEditarComprobantePago.feCompPago = fechaFormat;
    this.bodyEditarComprobantePago.idBanc = this.editarComprobante.get('banco')?.value;
    this.bodyEditarComprobantePago.nuCuenBanc = this.editarComprobante.get('nroCuenta')?.value;
    this.bodyEditarComprobantePago.nuSiaf = this.editarComprobante.get('nroSIAF')?.value;
    this.bodyEditarComprobantePago.imCompPago = Number( montoCompPago );
    this.bodyEditarComprobantePago.noTipoOper = this.editarComprobante.get('tipoOper')?.value;
    this.bodyEditarComprobantePago.nuBene = this.editarComprobante.get('nuDocu')?.value;
    this.bodyEditarComprobantePago.noBene = this.editarComprobante.get('noRazoSoc')?.value;
    this.bodyEditarComprobantePago.nuReciHono = this.editarComprobante.get('nuRxH')?.value;
    this.bodyEditarComprobantePago.feReciHono = fechaFormatRxH;
    this.bodyEditarComprobantePago.inReciHonoRete = valueChecked;
    this.bodyEditarComprobantePago.obConc = this.editarComprobante.get('concepto')?.value;
    this.bodyEditarComprobantePago.tramaCuentaContable = this.tramaContable;
    this.bodyEditarComprobantePago.tramaClasificadores = this.tramaClasificador;
    this.bodyEditarComprobantePago.tramaMedioPago = this.tramaMedioPago;
    console.log('this.bodyEditarComprobantePago', this.bodyEditarComprobantePago);
    this.$createComprobantePago = this.comprobantePagoService.editarComprobantePago(this.bodyEditarComprobantePago).subscribe({
      next: ( d ) => {
        console.log('dRespo', d);
        if (d.codResult < 0 ) {
          this.utilService.onShowAlert("info", "Atención", d.msgResult);
        } else if (d.codResult === 0) {
          this.utilService.onCloseLoading();
          this.router.navigate(['SARF/mantenimientos/documentos/comprobantes_de_pago/bandeja']);
        }
      },
      error:( d )=>{
        console.log('error', d);
        if (d.error.category === "NOT_FOUND") {
          this.utilService.onShowAlert("error", "Atención", d.error.description);        
        }else {
          this.utilService.onShowMessageErrorSystem();
        }
      }
    });
  }

  private validarFormulario(): boolean {   
    if (this.editarComprobante.value.zona === "" || this.editarComprobante.value.zona === "*" || this.editarComprobante.value.zona === null) {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una Zona Registral.");
      return false;  
    } else if (this.editarComprobante.value.oficina === "" || this.editarComprobante.value.oficina === "*" || this.editarComprobante.value.oficina === null) {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una Oficina Registral.");
      return false;      
    } else if (this.editarComprobante.value.local === "" || this.editarComprobante.value.local === "*" || this.editarComprobante.value.local === null) {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Local.");
      return false;      
    } else if (this.editarComprobante.value.tipoRecibo === "" || this.editarComprobante.value.tipoRecibo === "*" || this.editarComprobante.value.tipoRecibo === null) {      
      this.ddlTipoComprobante.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Tipo de Comprobante.");
      return false;      
    } else if (this.editarComprobante.value.fecha === "" || this.editarComprobante.value.fecha === "*" || this.editarComprobante.value.fecha === null) {      
      document.getElementById('idFecha').focus();            
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una Fecha.");
      return false;      
    } else if (this.editarComprobante.value.banco === "" || this.editarComprobante.value.banco === "*" || this.editarComprobante.value.banco === null) {      
      this.ddlBancoCargo.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Banco.");
      return false;      
    } else if (this.editarComprobante.value.nroCuenta === "" || this.editarComprobante.value.nroCuenta === "*" || this.editarComprobante.value.nroCuenta === null) {      
      this.ddlCuentaAbono.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Número de Cuenta.");
      return false;      
    } else if (this.editarComprobante.value.noRazoSoc === "" || this.editarComprobante.value.noRazoSoc === "*" || this.editarComprobante.value.noRazoSoc === null) {      
      this.idRazoSoc.nativeElement.focus();
      this.utilService.onShowAlert("warning", "Atención", "Seleccione o digite una Razón Social/Nombre-Apellido.");
      return false;      
    } else if (this.editarComprobante.value.nuRxH === '' && !(this.editarComprobante.value.fechaRxH === '' || this.editarComprobante.value.fechaRxH === null) ) {      
      console.log('FechaRxH', this.editarComprobante.value.fechaRxH);
      this.numRxH.nativeElement.focus();
      this.utilService.onShowAlert("warning", "Atención", "Debe ingresar el número de Recibo por Honorario.");
      return false;      
    } else if ( (this.editarComprobante.value.fechaRxH === '' || this.editarComprobante.value.fechaRxH === null) && this.editarComprobante.value.nuRxH !== '' ) {      
      this.utilService.onShowAlert("warning", "Atención", "Debe ingresar la fecha del Recibo por Honorario.");
      return false;      
    } else {
      return true;
    }       
  }

  public abrirVentanaBeneficiario() {
    const dialog = this.dialogService.open( VentBeneficiariosComponent, {
      header: 'Consulta de Beneficiarios',
      width: '50%',
      closable: false
    });

    dialog.onClose.pipe( takeUntil( this.unsubscribe$ ) )
          .subscribe( ( bene: IBeneficiarioBandeja ) => {
            if ( bene !== null ) {
              this.editarComprobante.get('nuDocu')?.setValue( bene.nuBene );
              this.editarComprobante.get('noRazoSoc')?.setValue( bene.noBene );
            }
          });
  }

  public abrirVentanaTipoOper() {
    const dialog = this.dialogService.open( VentTipoOperacionComponent, {
      header: 'Consultar Tipo de Operación',
      width: '40%',
      closable: false
    });

    dialog.onClose.pipe( takeUntil( this.unsubscribe$ ) )
          .subscribe( ( tipoOper: ITipoOperacionBandeja ) => {
            if ( tipoOper !== null ) {
              this.editarComprobante.get('tipoOper')?.setValue( tipoOper.noTipoOper );
            }
          });
  }

  public async cancelar() {
    const dataFiltrosRecibo: IDataFiltrosRecibo = await this.sharingInformationService.compartirDataFiltrosComprobantePagoObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecibo.esBusqueda) {
      dataFiltrosRecibo.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosComprobantePagoObservableData = dataFiltrosRecibo;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/comprobantes_de_pago/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaComprobantePagoObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete(); 
    this.$createComprobantePago.unsubscribe(); 
  }

}
