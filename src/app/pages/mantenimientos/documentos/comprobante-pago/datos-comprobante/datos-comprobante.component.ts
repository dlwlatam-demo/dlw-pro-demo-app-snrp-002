import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { ComprobantePagoValue } from '../../../../../interfaces/comprobante-pago.interface';

import { ValidatorsService } from '../../../../../core/services/validators.service';

@Component({
  selector: 'app-datos-comprobante',
  templateUrl: './datos-comprobante.component.html',
  styleUrls: ['./datos-comprobante.component.scss']
})
export class DatosComprobanteComponent implements OnInit {

  @Input()
  comprobante!: ComprobantePagoValue[];

  @Input()
  datos!: FormGroup;

  constructor(
    private validarService: ValidatorsService
  ) { }

  ngOnInit(): void {
    this.hidrate( this.comprobante['0'] );
  }

  hidrate( c: ComprobantePagoValue ) {

    const monDepo = Number( c.imCompPago ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
    
    const fecComp = this.validarService.reFormateaFecha( c.feCompPago );

    this.datos.get('nroComp')?.setValue( c.nuCompPago );
    this.datos.get('fecha')?.setValue( fecComp );
    this.datos.get('monto')?.setValue( monDepo );
    this.datos.get('nroSiaf')?.setValue( c.nuSiaf );
  }

}
