import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ComprobantePagoService } from 'src/app/services/mantenimientos/documentos/comprobante-pago.service';
import { BodyComprobantePago } from 'src/app/models/mantenimientos/documentos/comprobante-pago.model';
import { EnviaFirma } from '../../../../../models/mantenimientos/documentos/envia-firma.model';
import { DocumentoFirmaService } from '../../../../../services/mantenimientos/documentos/documento-firma.service';
import { UtilService } from 'src/app/services/util.service';
import Swal from 'sweetalert2';
import { GeneralService } from '../../../../../services/general.service';
import { IComprobantePago, IComprobantePagoById } from 'src/app/interfaces/comprobante-pago.interface';
import { ValidatorsService } from '../../../../../core/services/validators.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-solicitar-firma-comprobante',
  templateUrl: './solicitar-firma-comprobante.component.html',
  styleUrls: ['./solicitar-firma-comprobante.component.scss'],
  providers: [
    MessageService
  ]
})
export class SolicitarFirmaComprobanteComponent implements OnInit {

  firma!: FormGroup;

  isLoading!: boolean;

  documento!: BodyComprobantePago[];
  guidDocumento!: string;
  doc: any;
  
  docVisto: boolean = false;
  roles?: string[];
  perfil?: string;
  
  constructor(
    private fb: FormBuilder,
    private config: DynamicDialogConfig,
    private ref: DynamicDialogRef,
    private messageService: MessageService,
    private comprobantePagoService: ComprobantePagoService,
	  private documentoFirmaService: DocumentoFirmaService,
    private validateService: ValidatorsService,
    private utilService: UtilService,
	  private generalService: GeneralService) { }

  ngOnInit(): void {
  
  this.documento = this.config.data.csta as BodyComprobantePago[];
	this.guidDocumento = this.config.data.guidDocumento;
	this.doc = this.config.data.doc;

    this.firma = this.fb.group({
      nroComprob: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      nroSiaf: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]]
    });

    this.comprobantePagoService.getComprobantePagoById( this.documento['0'].idCompPago! ).subscribe({
      next: ( data ) => {
        this.hidrate( data as IComprobantePagoById )
      },
      error: ( e ) => {
        console.log( e );
      }
    });
  }


  hidrate( d: IComprobantePagoById ) {
    this.firma.disable();

    const monDepo = Number( d.imCompPago ).toFixed(2);
    const fecha = this.validateService.reFormateaFecha( d.feCompPago );

    this.firma.get('nroComprob')?.setValue( d.nuCompPago );
    this.firma.get('fecha')?.setValue( fecha );
    this.firma.get('nroSiaf')?.setValue( d.nuSiaf );
    this.firma.get('monto')?.setValue( monDepo );
  }
  
 
  
  
  grabar() {
    console.log('Solicitando Firma');
	
    this.isLoading = true;

    this.postAdjunto();
	
  }
  
   postAdjunto() {
    this.roles = JSON.parse( localStorage.getItem('roles_sarf')! );
    this.roles?.forEach( r => {
      this.perfil = r;
    });

    const token = localStorage.getItem('t_sarf')!;
	
	let nomDocuPdf = this.documento['0'].noDocuPdf!;
	if(nomDocuPdf == null || nomDocuPdf.trim() == "") {
		nomDocuPdf = "ComprobantePago" + this.documento['0'].idCompPago! + ".pdf";
	}
    
    const data = {
      idGuidDocu: this.guidDocumento,
      deDocuAdju: nomDocuPdf,
      coTipoAdju: '002', // Comprobante de Pago
      coZonaRegi: this.documento['0'].coZonaRegi!,
      coOficRegi: this.documento['0'].coOficRegi!
    }

    this.generalService.grabarAdjunto( data ).subscribe({
      next: ( d ) => {
	  
		let dataFirma: EnviaFirma = new EnviaFirma();
		dataFirma.tiDocu = data.coTipoAdju;
		dataFirma.idDocuOrig = this.documento['0'].idCompPago!;
		dataFirma.noDocuPdf = data.deDocuAdju;
		dataFirma.noRutaDocuPdf = this.documento['0'].noRutaDocuPdf!;
		dataFirma.idGuidDocu = data.idGuidDocu;
	
       this.documentoFirmaService.setDocumentoFirma(dataFirma).subscribe({
          next: ( d:any ) => {
            if (d.codResult < 0 ) {
				this.generalService.eliminarAdjunto( this.guidDocumento, this.perfil!, token! ).subscribe({
					next: ( d1 ) => { 
						this.docVisto = false;
						this.messageService.add({
              severity: 'info',
              summary: 'Atención',
              detail: d.msgResult,
              sticky: true
            });
					}
					});
			} else if (d.codResult === 0 ) {
				  this.ref.close('accept');
            }
          },

          error:( d )=>{
            if (d.error.category === "NOT_FOUND") {
              this.messageService.add({
                severity: 'info',
                summary: 'Atención',
                detail: d.error.description,
                sticky: true
              });

            }else {
              this.messageService.add({
                severity: 'warn',
                summary: 'Atención',
                detail: 'El servicio no esta disponible en estos momentos, inténtelo mas tarde.',
                sticky: true
              });
            }
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      },
      error: ( e ) => {
        console.log( e );
      }
    });
  }
  
  

  cancelar() {
    this.ref.close('');
  }
  
  verDocumento() {
	console.log('verDocumento');

	const byteCharacters = atob(this.doc!);
	const byteNumbers = new Array(byteCharacters.length);
	for (let i = 0; i < byteCharacters.length; i++) {
	  byteNumbers[i] = byteCharacters.charCodeAt(i);
	}
	const byteArray = new Uint8Array(byteNumbers);
	const blob = new Blob([byteArray], { type: 'application/pdf' });
	const blobUrl = URL.createObjectURL(blob);
	const download = document.createElement('a');
	download.href =  blobUrl;
	//download.setAttribute('download', 'Constacia_Abono');
	download.setAttribute("target", "_blank");
	document.body.appendChild( download );
	download.click();
	
	this.docVisto = true;
	
  }

}
