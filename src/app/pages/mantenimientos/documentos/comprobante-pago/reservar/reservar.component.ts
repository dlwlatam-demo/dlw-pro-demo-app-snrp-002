import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Dropdown } from 'primeng/dropdown';

import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { BodyReservarComprobantePago } from 'src/app/models/mantenimientos/documentos/comprobante-pago.model';
import { GeneralService } from 'src/app/services/general.service';
import { ComprobantePagoService } from 'src/app/services/mantenimientos/documentos/comprobante-pago.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-reservar',
  templateUrl: './reservar.component.html',
  styleUrls: ['./reservar.component.scss'],
  styles: [`
    #borde {
      padding: 5px;
      border: black 1px solid;
    }
  `]
})
export class ReservarComponent implements OnInit {
  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;
  reservarComprobantePago: FormGroup;
  unsubscribe$ = new Subject<void>();
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  loadingZonaRegistral: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  userCode:any;
  bodyReservarComprobantePago: BodyReservarComprobantePago;

  constructor(
    private dialogRef: DynamicDialogRef,
    private fb: FormBuilder,
    private generalService: GeneralService,
    private comprobantePagoService: ComprobantePagoService,
    private utilService: UtilService,
  ) { }

  ngOnInit(): void {
    this.bodyReservarComprobantePago = new BodyReservarComprobantePago()
    this.userCode = localStorage.getItem('user_code')||'';
    this._buscarZonaRegistral();
    this.reservarComprobantePago = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      cantidad: [0, [ Validators.required ]],
    })
    this._inicilializarListas();
  }

  private _inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' });    
  }

  private _buscarZonaRegistral(): void {
    this.loadingZonaRegistral = true;
    this.generalService.getCbo_Zonas_Usuario(this.userCode).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingZonaRegistral = false;    
        if (data.length === 1) {   
          this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });    
          this.reservarComprobantePago.patchValue({ "zona": data[0].coZonaRegi});
          this.buscarOficinaRegistral();
        } else {    
          this.listaZonaRegistral.push(...data);
        }
      },
      error:( err )=>{
        this.loadingZonaRegistral = false;    
      }
    });
  }

  public buscarOficinaRegistral(): void {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.reservarComprobantePago.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.reservarComprobantePago.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.reservarComprobantePago.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal(): void {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.reservarComprobantePago.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode, this.reservarComprobantePago.get('zona')?.value, this.reservarComprobantePago.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.reservarComprobantePago.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);
        } 
        this.loadingLocal = false;       
      }, (err) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  public grabar(): void {
    let resultado: boolean = this._validarFormulario();
    if (!resultado) {
      return;
    }
    this.utilService.onShowProcessLoading("Generando comprobante(s) de pago"); 
    this.bodyReservarComprobantePago.coZonaRegi = this.reservarComprobantePago.get('zona')?.value;
    this.bodyReservarComprobantePago.coOficRegi = this.reservarComprobantePago.get('oficina')?.value;
    this.bodyReservarComprobantePago.coLocaAten = this.reservarComprobantePago.get('local')?.value;
    this.bodyReservarComprobantePago.caCompPago = this.reservarComprobantePago.get('cantidad')?.value;
    this.comprobantePagoService.reservarComprobantePago( this.bodyReservarComprobantePago ).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (d) => {
          if (d.codResult !== 0) {
            this.utilService.onShowAlert("warning", "Atención", d.msgResult);        
          } else {
            this.utilService.onCloseLoading();  
            this.dialogRef.close(true);
          }
        },
        error: (d) => {
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("warning", "Atención", d.error.description);        
          }else {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      });
  }

  private _validarFormulario(): boolean {   
    if (this.reservarComprobantePago.value.zona === "" || this.reservarComprobantePago.value.zona === "*" || this.reservarComprobantePago.value.zona === null) {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una zona registral.");
      return false;  
    } else if (this.reservarComprobantePago.value.oficina === "" || this.reservarComprobantePago.value.oficina === "*" || this.reservarComprobantePago.value.oficina === null) {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una oficina registral.");
      return false;      
    } else if (this.reservarComprobantePago.value.local === "" || this.reservarComprobantePago.value.local === "*" || this.reservarComprobantePago.value.local === null) {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un local.");
      return false;      
    } else if ((this.reservarComprobantePago.value.cantidad === 0 || this.reservarComprobantePago.value.cantidad === null)) {
      document.getElementById('idCantidad').focus();      
      this.utilService.onShowAlert("warning", "Atención", "Ingrese la cantidad de comprobantes");
      return false;       
    } else {
      return true;
    }       
  }

  cancelar() {
    this.dialogRef.close(false);
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
