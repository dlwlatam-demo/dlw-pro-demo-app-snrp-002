import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IResponse } from 'src/app/interfaces/general.interface';
import { ChequeOperations } from 'src/app/models/mantenimientos/documentos/cheques.model';
import { ChequesService } from 'src/app/services/mantenimientos/documentos/cheques.service';
import { UtilService } from 'src/app/services/util.service';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { GeneralService } from '../../../../../../services/general.service';
import { IResponse2 } from '../../../../../../interfaces/general.interface';
@Component({
  selector: 'app-anular-cheque',
  templateUrl: './anular-cheque.component.html',
  styleUrls: ['./anular-cheque.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class AnularChequeComponent implements OnInit, OnDestroy  {

  unsubscribe$ = new Subject<void>();
  cantidadMaximaTexto = 300;
  chequeOperations: ChequeOperations;
  motivoAnulacion: string = '';

  constructor(
    private dialogRef: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private chequesService: ChequesService,
    private funciones: Funciones, 
    private utilService: UtilService
  ) { }

  ngOnInit(): void {
  }

  public anular() {
    if (!this.motivoAnulacion || this.funciones.eliminarEspaciosEnBlanco(this.motivoAnulacion) === '') {
      this.utilService.onShowAlert("warning", "Atención", 'Ingrese el motivo de anulación.');
      return;
    }

    let ids: string[] = [];
    for (let i = 0; i < this.config.data.selectedValuesCheques.length; i++) {
      ids.push(this.config.data.selectedValuesCheques[i].idCheq);      
    }
    this.chequeOperations = new ChequeOperations();
    this.chequeOperations.obMotiAnul = this.motivoAnulacion;
    this.chequeOperations.cheques = ids;    
    this.utilService.onShowProcess('anular');
    this.chequesService.anulaCheque(this.chequeOperations).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (res: IResponse) => {    
      if (res.codResult < 0 ) {        
        this.utilService.onShowAlert("info", "Atención", res.msgResult);
      } else if (res.codResult === 0) {  
        this.respuestaExitosaAnularCheque();                             
      }         
    }, (err: HttpErrorResponse) => {
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("error", "Atención", err.error.description);        
      } else {
        this.utilService.onShowMessageErrorSystem();        
      }
    })
  }  

  private async respuestaExitosaAnularCheque() { 
    let mensaje: string = '';
    if (this.config.data.selectedValuesCheques.length === 0) {
      mensaje = 'Se anuló el cheque satisfactoriamente';
    } else {
      mensaje = 'Se anularon los cheques satisfactoriamente';
    }  
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", mensaje);
    if (respuesta.isConfirmed) {
      this.dialogRef.close(true); 
    }
  }

  public cancelar() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();    
  }

}
