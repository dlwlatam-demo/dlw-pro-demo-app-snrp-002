import { Component, OnInit, OnDestroy } from '@angular/core';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NuevoChequeComponent } from '../nuevo-cheque/nuevo-cheque.component';
import { AnularChequeComponent } from '../anular-cheque/anular-cheque.component';
import { ChequesService } from 'src/app/services/mantenimientos/documentos/cheques.service';
import { ChequeConsulta, ChequeOperations } from 'src/app/models/mantenimientos/documentos/cheques.model';
import { ICheque } from 'src/app/interfaces/cheque.interface';
import { UtilService } from 'src/app/services/util.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ESTADO_REGISTRO, TIPO_DOCUMENTO_IDENTIDAD } from 'src/app/models/enum/parameters';
import { IResponse } from 'src/app/interfaces/general.interface';
import { ComprobantePagoValue } from 'src/app/interfaces/comprobante-pago.interface';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { BaseBandeja } from '../../../../../../base/base-bandeja.abstract';
import { MenuOpciones } from '../../../../../../models/auth/Menu.model';
@Component({
  selector: 'app-bandeja',
  templateUrl: './bandeja.component.html',
  styleUrls: ['./bandeja.component.scss'],
  providers: [
    DialogService,
    MessageService
  ]
})
export class BandejaComponent extends BaseBandeja implements OnInit, OnDestroy {

  currentPage: string = environment.currentPage;
  chequeConsulta: ChequeConsulta;
  unsubscribe$ = new Subject<void>();
  listaCheques: ICheque[] = [];
  selectedValuesCheques: ICheque[] = [];
  nombreBanco: string = '';
  totalCheques: string = '';
  loadingCheques: boolean = false;
  tipoDocumentoIdentidad = TIPO_DOCUMENTO_IDENTIDAD;
  estadoRegistro = ESTADO_REGISTRO;
  comprobanteDePago: ComprobantePagoValue;

  constructor( 
    private dialogService: DialogService,
    private chequesService: ChequesService,
    private router: Router,
    private messageService: MessageService,
    private sharingInformationService: SharingInformationService,
    private utilService: UtilService    
  ) { super() }

  async ngOnInit() {    
    this.codBandeja = MenuOpciones.Comprobante_de_Pago;
    this.btnComprobanteDePagoCheques();    
    this.comprobanteDePago = await this.sharingInformationService.compartirComprobantePagoObservable.pipe(first()).toPromise();      
    this.chequeConsulta = new ChequeConsulta();   
    this.chequeConsulta.idCompPago = this.comprobanteDePago.idCompPago;
    this.nombreBanco = this.comprobanteDePago.noBanc;
    this.obtenerChequesDeComprobanteDePago();
  }

  obtenerChequesDeComprobanteDePago() {
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.loadingCheques = true;
    this.chequesService.getBandejaCheque(this.chequeConsulta).pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: any) => {
      this.listaCheques = data.lstCheques;
      this._calcularTotales();   
      this.utilService.onCloseLoading();
      this.loadingCheques = false;
    }, (err: HttpErrorResponse) => {        
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onShowAlert("warning", "Atención", err.error.description);
        this.loadingCheques = false;        
      } else {
        this.utilService.onShowMessageErrorSystem();
        this.loadingCheques = false;
      }
    });
  }

  public nuevoCheque() {
    const dialog = this.dialogService.open( NuevoChequeComponent, {
      data: {
        idCompPago: this.chequeConsulta.idCompPago,
        nombreBanco: this.nombreBanco
      },
      header: 'Comprobante de Pago - Cheques - Nuevo',
      width: '75%'         
    });
    dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe((result: boolean) => {
      if (result) {
        this.obtenerChequesDeComprobanteDePago();
        this.messageService.add({severity: 'success', summary: 'Se creo satisfactoriamente el cheque.'});
      }
    });     
  }

  public editarCheque() {
    if (this.selectedValuesCheques.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro para editarlo.");
    } if (this.selectedValuesCheques.length === 1) {
      if (this.selectedValuesCheques[0].deEsta === this.estadoRegistro.ANULADO || this.selectedValuesCheques[0].deEsta === this.estadoRegistro.PENDIENTE_IMPRESIÓN
          || this.selectedValuesCheques[0].deEsta === this.estadoRegistro.NO_UTILIZADO) {
        this.utilService.onShowAlert("warning", "Atención", "No puede EDITAR un registro en estado ANULADO, PENDIENTE DE IMPRESIÓN o NO UTILIZADO.");
      } else {
        const dialog = this.dialogService.open( NuevoChequeComponent, {
          data: {
            idCompPago: this.chequeConsulta.idCompPago,
            nombreBanco: this.nombreBanco,
            chequeSeleccionado: this.selectedValuesCheques[0]
          },
          header: 'Comprobante de Pago - Cheques - Editar',
          width: '75%'         
        });
        dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe((result: boolean) => {
          if (result) {
            this.obtenerChequesDeComprobanteDePago();
            this.messageService.add({severity: 'success', summary: 'Se editó satisfactoriamente el cheque.'});
          }
        }); 
        this.selectedValuesCheques = [];
      }      
    } else if (this.selectedValuesCheques.length > 1) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar un registro a la vez.");
    }      
  }

  public anularCheques() {   
    if (this.selectedValuesCheques.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else {
      const dialog = this.dialogService.open( AnularChequeComponent, {
        data: {
          selectedValuesCheques: this.selectedValuesCheques
        },
        header: 'Comprobante de Pago - Cheque - Anular',
        width: '600px'
      });
      dialog.onClose.pipe(takeUntil(this.unsubscribe$)).subscribe((result: boolean) => {
        if (result) {
          this.selectedValuesCheques = [];
          this.obtenerChequesDeComprobanteDePago();
        }
      }); 
    }
  }

  private validarRegistrosAnulados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesCheques.length; i++) {       
      if (!(this.selectedValuesCheques[i].deEsta === this.estadoRegistro.REGISTRADO || this.selectedValuesCheques[i].deEsta === this.estadoRegistro.DEVUELTO
        || this.selectedValuesCheques[i].deEsta === this.estadoRegistro.PENDIENTE_ANULACIÓN)) {
        resultado = false;
      }             
    }
    return resultado;
  }

  activarCheques() {
    if (this.selectedValuesCheques.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosActivados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede ACTIVAR cheques en estado ANULADO.");
    } 
    else {
      let textoActivar = (this.selectedValuesCheques.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea ACTIVAR ${textoActivar}?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess('activar'); 
          let chequeOperations = new ChequeOperations();
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesCheques.length; i++) {
            lista.push(this.selectedValuesCheques[i].idCheq.toString());           
          }
          chequeOperations.cheques = lista;
          this.chequesService.activateCheque(chequeOperations).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("warning", "Atención", res.msgResult);
            } else if (res.codResult === 0) {
              this.respuestaExitosaActivarCheque();
            }             
          }, (err: HttpErrorResponse) => {   
            if (err.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", err.error.description);        
            } else {
              this.utilService.onShowMessageErrorSystem();        
            }                                 
          });          
        }
      });
    }    
  }

  private validarRegistrosActivados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesCheques.length; i++) { 
      if (!(this.selectedValuesCheques[i].deEsta === this.estadoRegistro.ANULADO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  private async respuestaExitosaActivarCheque() { 
    let mensaje: string = '';
    if (this.selectedValuesCheques.length === 1) {
      mensaje = 'Se activó el cheque satisfactoriamente';
    } else {
      mensaje = 'Se activaron los cheques satisfactoriamente';
    }  
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", mensaje);
    if (respuesta.isConfirmed) {
      this.selectedValuesCheques = [];
      this.obtenerChequesDeComprobanteDePago();
    }
  }

  noUtilizado() {
    if (this.selectedValuesCheques.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (!this.validarRegistrosNoUtilizados()) {
      this.utilService.onShowAlert("warning", "Atención", "Solo puede cambiar al estado NO UTLIZADO a los cheques que estén en estado REGISTRADO o DEVUELTO");
    } else {
      let texto = (this.selectedValuesCheques.length === 1) ? 'el registro' : 'los registros';
      this.utilService.onShowConfirm(`¿Estás seguro que desea cambiar ${texto} al estado NO UTILIZADO?`).then( (result) => {
        if (result.isConfirmed) {
          this.utilService.onShowProcess(`Actualizando ${texto} al estado NO UTILIZADO`); 
          let chequeOperations = new ChequeOperations();
          let lista: string[] = [];
          for (let i = 0; i < this.selectedValuesCheques.length; i++) {
            lista.push(this.selectedValuesCheques[i].idCheq.toString());           
          }
          chequeOperations.cheques = lista;
          this.chequesService.notUsedCheque(chequeOperations).pipe(takeUntil(this.unsubscribe$))
          .subscribe( (res: IResponse) => {       
            if (res.codResult < 0 ) {        
              this.utilService.onShowAlert("warning", "Atención", res.msgResult);
            } else if (res.codResult === 0) {
              this.respuestaExitosaNoUtilizadoCheque();
            }             
          }, (err: HttpErrorResponse) => {   
            if (err.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", err.error.description);        
            } else {
              this.utilService.onShowMessageErrorSystem();        
            }                                 
          });          
        }
      });
    }
  }

  private validarRegistrosNoUtilizados(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesCheques.length; i++) { 
      if (!(this.selectedValuesCheques[i].deEsta === this.estadoRegistro.REGISTRADO || this.selectedValuesCheques[i].deEsta === this.estadoRegistro.DEVUELTO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  private async respuestaExitosaNoUtilizadoCheque() { 
    let mensaje: string = '';
    if (this.selectedValuesCheques.length === 1) {
      mensaje = 'Se actualizó el estado del cheque satisfactoriamente';
    } else {
      mensaje = 'Se actualizó el estado de los cheques satisfactoriamente';
    }  
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", mensaje);
    if (respuesta.isConfirmed) {
      this.selectedValuesCheques = [];
      this.obtenerChequesDeComprobanteDePago();
    }
  }

  solicitarImpresion() {   
    if (this.selectedValuesCheques.length === 0) {
      this.utilService.onShowAlert("warning", "Atención", "Debe seleccionar al menos un registro.");
    } else if (this.selectedValuesCheques.length > 0) {
      if (!this.validarRegistrosSolicitarImpresion()) {
        this.utilService.onShowAlert("warning", "Atención", "Solo puede SOLICITAR IMPRESIÓN a los cheques que estén en estado REGISTRADO o DEVUELTO");
      } else {
        let texto = (this.selectedValuesCheques.length === 1) ? 'del registro' : 'de los registros';
        this.utilService.onShowConfirm(`¿Estás seguro que desea SOLICITAR IMPRESIÓN ${texto}?`).then( (result) => {
          if (result.isConfirmed) {
            this.utilService.onShowProcess(`Solicitando la impresión ${texto}`); 
            let chequeOperations = new ChequeOperations();
            let lista: string[] = [];
            for (let i = 0; i < this.selectedValuesCheques.length; i++) {
              lista.push(this.selectedValuesCheques[i].idCheq.toString());           
            }
            chequeOperations.cheques = lista;
            this.chequesService.requestPrintingCheque(chequeOperations).pipe(takeUntil(this.unsubscribe$))
            .subscribe( (res: IResponse) => {       
              if (res.codResult < 0 ) {        
                this.utilService.onShowAlert("warning", "Atención", res.msgResult);
              } else if (res.codResult === 0) {
                this.respuestaExitosaSolicitarImpresionCheque();
              }             
            }, (err: HttpErrorResponse) => {   
              if (err.error.category === "NOT_FOUND") {
                this.utilService.onShowAlert("error", "Atención", err.error.description);        
              } else {
                this.utilService.onShowMessageErrorSystem();        
              }                                 
            });          
          }
        });
      }      
    } 
  }

  private validarRegistrosSolicitarImpresion(): boolean {
    let resultado: boolean = true;
    for (let i = 0; i < this.selectedValuesCheques.length; i++) { 
      if (!(this.selectedValuesCheques[i].deEsta === this.estadoRegistro.REGISTRADO || this.selectedValuesCheques[i].deEsta === this.estadoRegistro.DEVUELTO)) {
        resultado = false;
      }     
    }
    return resultado;
  }

  private _calcularTotales() {
    let total = 0;
    for(let cheques of this.listaCheques) {
      if ( cheques.deEsta !== 'ANULADO' ) {
        total += cheques.imCheq;
      }
    }

    this.totalCheques = total.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
}

  private async respuestaExitosaSolicitarImpresionCheque() { 
    let mensaje: string = '';
    if (this.selectedValuesCheques.length === 1) {
      mensaje = 'Se actualizó el estado del cheque satisfactoriamente';
    } else {
      mensaje = 'Se actualizó el estado de los cheques satisfactoriamente';
    }  
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", mensaje);
    if (respuesta.isConfirmed) {
      this.selectedValuesCheques = [];
      this.obtenerChequesDeComprobanteDePago();
    }
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  public regresar() {
    this.router.navigate(['SARF/mantenimientos/documentos/comprobantes_de_pago/bandeja']);
  }

  ngOnDestroy(): void {      
    this.unsubscribe$.next();
    this.unsubscribe$.complete();      
  }

}
