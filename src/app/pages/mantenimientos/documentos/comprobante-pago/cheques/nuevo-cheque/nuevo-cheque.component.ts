import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Dropdown } from 'primeng/dropdown';
import { HttpErrorResponse } from '@angular/common/http';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { IPersona, IResponse, IResponse2 } from 'src/app/interfaces/general.interface';
import { TIPO_DOCUMENTO_IDENTIDAD } from 'src/app/models/enum/parameters';
import { ChequeRegister } from 'src/app/models/mantenimientos/documentos/cheques.model';
import { GeneralService } from 'src/app/services/general.service';
import { ChequesService } from 'src/app/services/mantenimientos/documentos/cheques.service';
import { UtilService } from 'src/app/services/util.service';
import { ICheque } from 'src/app/interfaces/cheque.interface';
@Component({
  selector: 'app-nuevo-cheque',
  templateUrl: './nuevo-cheque.component.html',
  styleUrls: ['./nuevo-cheque.component.scss'],
  encapsulation:ViewEncapsulation.None,
  providers: [
    MessageService
  ]
})
export class NuevoChequeComponent implements OnInit, OnDestroy {

  formCheque: FormGroup;
  unsubscribe$ = new Subject<void>();
  fechaActual: Date = new Date();
  chequeRegister: ChequeRegister;
  listaTipoDocumentos: IResponse2[] = [];
  longitudTextoNroDocIdentidad = '';
  tipoDocumentoIdentidad = TIPO_DOCUMENTO_IDENTIDAD;
  tipoDocumentoSeleccionado: string = '';
  esBuscarEntidad: boolean = false;
  isLoading: boolean = false;
  datosPersonaDNI: IPersona;
  chequeEditar: ICheque;
  idCompPago: number;

  @ViewChild('ddlTipoDocumento') ddlTipoDocumento: Dropdown;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private messageService: MessageService,
    private generalService: GeneralService,
    private chequesService: ChequesService,  
    private utilService: UtilService,
    private funciones: Funciones
  ) { }

  ngOnInit(): void {
    this.construirFormulario(); 
    this.resetearNombreCompleto();
    this.idCompPago = this.config.data.idCompPago;       
    if (this.config.data.chequeSeleccionado === undefined) {
      this.chequeEditar = null; 
    } else {
      this.chequeEditar = this.config.data.chequeSeleccionado;
    }
    this.obtenerTipoDocumentos();      
  }

  private construirFormulario() {
    this.formCheque = this.formBuilder.group({               
      nombreBanco: [this.config.data.nombreBanco],
      numeroCheque: [''],
      fechaEmision: [this.fechaActual],
      monto: [''],
      tipoDocumento: [null],
      numeroDocumento: [''],
      nombres: [''],
      razonSocial: [''],
      numeroReferenciaCheque: [null]
    });
    this.formCheque.get('nombreBanco').disable();
    this.formCheque.get('numeroCheque').disable();
    this.formCheque.get('numeroDocumento').disable();   
  }

  resetearNombreCompleto() {
    this.datosPersonaDNI = {
      dni: '',
      apellidoPaterno: '',
      apellidoMaterno: '',
      nombres: ''   
    }  
  }

  private setearCampos() {
    console.log('this.chequeEditar', this.chequeEditar);
    this.formCheque.patchValue({ "nombreBanco": this.chequeEditar.noBanc });
    this.formCheque.patchValue({ "numeroCheque": this.chequeEditar.nuCheq });
    this.formCheque.patchValue({ "fechaEmision": new Date(this.chequeEditar.feCheq) });
    this.formCheque.patchValue({ "monto": this.chequeEditar.imCheq.toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    }) });
    this.formCheque.patchValue({ "tipoDocumento": this.chequeEditar.tiDocuIden });
    this.formCheque.patchValue({ "numeroDocumento": this.chequeEditar.nuBene });
    this.tipoDocumentoSeleccionado = this.chequeEditar.tiDocuIden;    
    this.formCheque.get('numeroDocumento').enable();
    if (this.chequeEditar.tiDocuIden && !(this.chequeEditar.tiDocuIden === this.tipoDocumentoIdentidad.RUC)) {
      this.formCheque.patchValue({ "nombres": this.chequeEditar.noBene });

      if (this.chequeEditar.tiDocuIden === this.tipoDocumentoIdentidad.DNI) {        
        this.esBuscarEntidad = true;        
        this.formCheque.get('nombres').enable();
        // this.datosPersonaDNI.dni = this.chequeEditar.nuBene;
        // this.datosPersonaDNI.nombres = (this.chequeEditar.noNombres) ? this.chequeEditar.noNombres.toUpperCase() : this.chequeEditar.noNombres;
        // this.datosPersonaDNI.apellidoPaterno = (this.chequeEditar.noApelPate) ? this.chequeEditar.noApelPate.toUpperCase() : this.chequeEditar.noApelPate;
        // this.datosPersonaDNI.apellidoMaterno = (this.chequeEditar.noApelMat) ? this.chequeEditar.noApelMat.toUpperCase() : this.chequeEditar.noApelMat;
      } else {        
        this.formCheque.get('nombres').enable();
      }
    } else {      
      this.formCheque.patchValue({ "razonSocial": this.chequeEditar.noBene });
    }
    this.formCheque.patchValue({ "numeroReferenciaCheque": this.chequeEditar.nuRefeCheq });
  }

  obtenerTipoDocumentos() {
    this.utilService.onShowProcessLoading("Cargando la data");
    this.listaTipoDocumentos = [];
    this.generalService.getCbo_TipoDocumentoIdentidad().pipe(takeUntil(this.unsubscribe$))
    .subscribe((data: IResponse2[]) => {
      this.listaTipoDocumentos = data; 
      if (this.chequeEditar !== null) {
        this.setearCampos();
      }       
      this.utilService.onCloseLoading();          
    }, (err: HttpErrorResponse) => {        
      if (err.error.category === "NOT_FOUND") {
        this.utilService.onCloseLoading();        
      } else {
        this.utilService.onShowMessageErrorSystem();
      }                             
    });
  }

  public formatearMonto(event:any){
    if (event.target.value) {
      const nuevoValor = this.funciones.convertirTextoANumero( event.target.value );
      const number = Number(nuevoValor).toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });

      this.formCheque.get('monto')?.setValue( number );
    }
  }

  private resetarControles() {
    this.formCheque.get('numeroDocumento').reset();
    this.formCheque.get('nombres').reset();
    this.formCheque.get('razonSocial').reset();
  }

  public seleccionarTipoDocumento() {
    this.tipoDocumentoSeleccionado = this.formCheque.get('tipoDocumento').value;
    this.resetearNombreCompleto(); 
    this.esBuscarEntidad = false;    
    this.formCheque.get('numeroDocumento').enable();  
    this.resetarControles(); 

    switch(this.tipoDocumentoSeleccionado) {
      case this.tipoDocumentoIdentidad.DNI:
        this.longitudTextoNroDocIdentidad = '8';
        this.formCheque.get('nombres').enable();
        break;
      case this.tipoDocumentoIdentidad.CARNET_EXTRANJERIA:
        this.longitudTextoNroDocIdentidad = '12';
        this.formCheque.get('nombres').enable();
        break;
      case this.tipoDocumentoIdentidad.PASAPORTE:
        this.longitudTextoNroDocIdentidad = '12';
        this.formCheque.get('nombres').enable();
        break;
      case this.tipoDocumentoIdentidad.RUC:
        this.longitudTextoNroDocIdentidad = '11';
        break;
      case this.tipoDocumentoIdentidad.OTROS:
        this.longitudTextoNroDocIdentidad = '';
        this.formCheque.get('nombres').enable();
        break;
      default:
        this.longitudTextoNroDocIdentidad = '';
        this.resetearNombreCompleto();
        this.formCheque.get('numeroDocumento').disable();
    }
  }

  public buscarEntidad(e: PointerEvent) {    
    e.stopPropagation();
    this.esBuscarEntidad = false;
    this.resetearNombreCompleto(); 
    const tipoDocumento: string = this.formCheque.get('tipoDocumento').value;
    const numeroDocumento = this.formCheque.get('numeroDocumento').value;

    if (tipoDocumento === this.tipoDocumentoIdentidad.DNI && (numeroDocumento === '' || numeroDocumento === null)) {
      document.getElementById('idNroDocIdentidad').focus();  
      this.messageService.add({ severity: 'warn', summary: 'Debe de ingresar el número de DNI.', sticky: true});
      return;
    } else if (tipoDocumento === this.tipoDocumentoIdentidad.DNI && (numeroDocumento.length !== 8)) {
      document.getElementById('idNroDocIdentidad').focus();
      this.messageService.add({ severity: 'warn', summary: 'El número de DNI debe de tener ocho dígitos.', sticky: true});
      return;
    } else if (tipoDocumento === this.tipoDocumentoIdentidad.RUC && (numeroDocumento === '' || numeroDocumento === null)) {
      document.getElementById('idNroDocIdentidad').focus();
      this.messageService.add({ severity: 'warn', summary: 'Debe de ingresar el número de RUC..', sticky: true});
      return;
    } else if (tipoDocumento === this.tipoDocumentoIdentidad.RUC && (numeroDocumento.length !== 11)) {
      document.getElementById('idNroDocIdentidad').focus();
      this.messageService.add({ severity: 'warn', summary: 'El número de RUC debe de tener once dígitos.', sticky: true});
      return;
    } 

    this.utilService.onShowProcess('Buscando el registro');
    if (tipoDocumento === this.tipoDocumentoIdentidad.DNI) {      
      this.buscarPorDNI(numeroDocumento);
    }       
  }

  keyupNroDoc(event: any) {    
    if (!this.formCheque.get('numeroDocumento').pristine && this.datosPersonaDNI.nombres) {  
      this.resetearNombreCompleto();    
      this.formCheque.get('nombres').reset();
      this.esBuscarEntidad = false;           
    }
  }

  private buscarPorDNI(numeroDni: string) {
    this.generalService.obtenerDatosPorDNI(numeroDni).pipe(takeUntil(this.unsubscribe$))
    .subscribe( (data: IPersona) => {   
      this.datosPersonaDNI.dni = data.dni;
      this.datosPersonaDNI.nombres = data.nombres;
      this.datosPersonaDNI.apellidoPaterno = data.apellidoPaterno;
      this.datosPersonaDNI.apellidoMaterno = data.apellidoMaterno;

      const nombreBeneficiario: string = `${data.nombres} ${data.apellidoPaterno} ${data.apellidoMaterno}`;
      this.formCheque.patchValue({ "nombres": nombreBeneficiario });
      this.esBuscarEntidad = true;
      this.utilService.onCloseLoading(); 
    }, (err: HttpErrorResponse) => {            
      if (err.error.category === "BAD_REQUEST" || err.error.category === "NOT_FOUND") {
        this.utilService.onCloseLoading();
        this.messageService.add({ severity: 'warn', summary: `No se encontró a la persona con el número de documento: ${numeroDni}`, sticky: true});
      } else {
        this.utilService.onCloseLoading();
        this.messageService.add({ 
          severity: 'warn',
          summary: `El servicio no esta disponible en estos momentos, inténtelo mas tarde.`,
          sticky: true
        });
      }
    });        
  }

  public async grabarCheque() {
    this.messageService.clear();
    
    let resultado: boolean = await this.validarFormulario();
    if (!resultado) {
      this.esBuscarEntidad = false;
      return;
    }     

    this.isLoading = true;
    this.asignarValores(this.formCheque.getRawValue());

    if (this.chequeEditar === null) {
      this.chequesService.createCheque(this.chequeRegister).pipe(takeUntil(this.unsubscribe$))
      .subscribe( (res: IResponse) => {    
        if (res.codResult < 0 ) {
          this.isLoading = false;
          this.messageService.add({ severity: 'warn', summary: res.msgResult, sticky: true});
        } else if (res.codResult === 0) {
          this.isLoading = false;         
          this.respuestaExitosaGrabarCheque();
        }         
      }, (err: HttpErrorResponse) => {
        if (err.error.category === "NOT_FOUND") {
          this.isLoading = false;
          this.messageService.add({ severity: 'error', summary: err.error.description, sticky: true});
        } else {
          this.isLoading = false;
          this.messageService.add({ 
            severity: 'warn',
            summary: `El servicio no esta disponible en estos momentos, inténtelo mas tarde.`,
            sticky: true
          });
        }
      })
    } else {
      this.chequesService.editCheque(this.chequeRegister).pipe(takeUntil(this.unsubscribe$))
      .subscribe( (res: IResponse) => {    
        if (res.codResult < 0 ) {
          this.messageService.add({ 
            severity: 'info',
            summary: res.msgResult,
            sticky: true
          });

          this.isLoading = false;
        } else if (res.codResult === 0) {
          this.isLoading = false;     
          this.dialogRef.close(true);
          // this.respuestaExitosaGrabarCheque();        
        }
      }, (err: HttpErrorResponse) => {
        if (err.error.category === "NOT_FOUND") {
          this.isLoading = false;
          this.messageService.add({ severity: 'error', summary: err.error.description, sticky: true});
        } else {
          this.isLoading = false;
          this.messageService.add({ 
            severity: 'warn',
            summary: `El servicio no esta disponible en estos momentos, inténtelo mas tarde.`,
            sticky: true
          });        
        }
      })
    }    
  }

  private async respuestaExitosaGrabarCheque() {
    const verbo = (this.chequeEditar === null) ? 'creó' : 'editó'
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Se ${verbo} satisfactoriamente el cheque.`);
    if (respuesta.isConfirmed) {
      this.dialogRef.close(true); 
    }
  }

  private async validarFormulario(): Promise<boolean> {    
    let registro = this.formCheque.getRawValue();
    if (registro.nombreBanco === "" || registro.nombreBanco === null) {                    
      document.getElementById('idBanco').focus();
      this.messageService.add({ severity: 'warn', summary: 'El nombre del banco no esta figurando.', sticky: true});
      return false;      
    } else if (registro.fechaEmision === "" || registro.fechaEmision === null) {
      let respuesta = await this.utilService.onShowAlertPromise("warning", "Atención", `Seleccione la fecha de emisión.`);
      if (respuesta.isConfirmed) {
        document.getElementById('idFechaEmision').focus();
        return false; 
      } else {
        return true;
      }    
    } else if ( registro.monto === "" || registro.monto === null) {
      document.getElementById('idMonto').focus();
      this.messageService.add({ severity: 'warn', summary: 'Ingrese el monto.', sticky: true});
      return false;       
    } else if (registro.tipoDocumento === "" || registro.tipoDocumento === "*" || registro.tipoDocumento === null) {      
      this.ddlTipoDocumento.focus();
      this.messageService.add({ severity: 'warn', summary: 'Seleccione un tipo de documento de identidad.', sticky: true});
      return false;
    } else if (this.tipoDocumentoSeleccionado && !(this.tipoDocumentoSeleccionado === this.tipoDocumentoIdentidad.RUC)) {      
      if (registro.nombres === null || this.funciones.eliminarEspaciosEnBlanco(registro.nombres) === "") {
        document.getElementById('idNombres').focus();
        this.messageService.add({ severity: 'warn', summary: 'Ingresar Apellidos y Nombres, es obligatorio..', sticky: true});
        return false;
      } else {
        return true;
      }
    } else if (this.tipoDocumentoSeleccionado === this.tipoDocumentoIdentidad.RUC && 
      (registro.razonSocial === null || this.funciones.eliminarEspaciosEnBlanco(registro.razonSocial) === "")) {
      document.getElementById('idRazonSocial').focus();
      this.messageService.add({ severity: 'warn', summary: 'Ingresar Razón Social, es obligatorio.', sticky: true});
      return false;
    }  else {
      return true;
    }    
  }

  private asignarValores(registro: any) {    
    this.chequeRegister = new ChequeRegister();
    this.chequeRegister.idCheq = (this.chequeEditar === null) ? null : this.chequeEditar.idCheq;
    this.chequeRegister.idCompPago = this.idCompPago;
    this.chequeRegister.feCheq = (registro.fechaEmision !== "") ? this.funciones.convertirFechaDateToString(registro.fechaEmision): "";
    let monto: string = this.funciones.convertirTextoANumero(registro.monto);
    this.chequeRegister.imCheq = parseFloat( Number(monto).toFixed(2) );
    this.chequeRegister.tiDocuIden = registro.tipoDocumento;
    if (registro.tipoDocumento === this.tipoDocumentoIdentidad.DNI) {
      this.chequeRegister.nuBene = registro.numeroDocumento;
      this.chequeRegister.noBene = (registro.nombres !== '' && registro.nombres !== null) ? (this.funciones.eliminarEspaciosEnBlanco(registro.nombres)).toUpperCase() : registro.nombres;
    } else if (registro.tipoDocumento === this.tipoDocumentoIdentidad.RUC) {
      const razonSocial: string = this.funciones.eliminarEspaciosEnBlanco(registro.razonSocial);   
      this.chequeRegister.nuBene = (registro.numeroDocumento === null) ? '' : registro.numeroDocumento;;
      this.chequeRegister.noBene = razonSocial.toUpperCase();
    } else {
      this.chequeRegister.nuBene = (registro.numeroDocumento === null) ? '' : registro.numeroDocumento;
      this.chequeRegister.noBene = (registro.nombres !== '' && registro.nombres !== null) ? (this.funciones.eliminarEspaciosEnBlanco(registro.nombres)).toUpperCase() : registro.nombres;
    }  
    this.chequeRegister.nuRefeCheq = registro.numeroReferenciaCheque;   
  }

  public cancelar() {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();    
  }

}
