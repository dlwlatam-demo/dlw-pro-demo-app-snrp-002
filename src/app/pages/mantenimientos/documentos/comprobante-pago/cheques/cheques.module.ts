import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChequesRoutingModule } from './cheques-routing.module';
import { NgPrimeModule } from 'src/app/ng-prime/ng-prime.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BandejaComponent } from './bandeja/bandeja.component';
import { NuevoChequeComponent } from './nuevo-cheque/nuevo-cheque.component';
import { AnularChequeComponent } from './anular-cheque/anular-cheque.component';
import { DigitOnlyModule } from '@uiowa/digit-only';
@NgModule({
  declarations: [
    BandejaComponent,
    NuevoChequeComponent,
    AnularChequeComponent
  ],
  imports: [
    CommonModule,
    ChequesRoutingModule,
    NgPrimeModule,
    FormsModule,
    ReactiveFormsModule,
    DigitOnlyModule
  ]
})
export class ChequesModule { }
