import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RefreshComprobantePagoGuard } from 'src/app/core/guards/mantenimientos/documentos/refresh-comprobante-pago.guard';
import { BandejaComponent } from './bandeja/bandeja.component';

const routes: Routes = [
  { path: 'bandeja', component: BandejaComponent, canActivate: [ RefreshComprobantePagoGuard ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChequesRoutingModule { }
