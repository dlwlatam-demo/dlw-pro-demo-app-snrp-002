import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Observable, Subject, Subscription } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { DialogService } from 'primeng/dynamicdialog';
import { Dropdown } from 'primeng/dropdown';

import { environment } from '../../../../../../environments/environment.prod';
import { GeneralService } from 'src/app/services/general.service';
import { CuentaContableService } from 'src/app/services/mantenimientos/maestras/cuenta-contable.service';
import { ReciboIngresoService } from 'src/app/services/mantenimientos/documentos/recibo-ingreso.service';
import { BodyNuevoComprobantePago, ITrama } from 'src/app/models/mantenimientos/documentos/comprobante-pago.model';
import { ComprobantePagoService } from 'src/app/services/mantenimientos/documentos/comprobante-pago.service';
import { UtilService } from 'src/app/services/util.service';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { ReciboIngreso } from 'src/app/models/mantenimientos/documentos/recibo-ingreso.model';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { IDataFiltrosRecibo } from 'src/app/interfaces/comprobante-pago.interface';
import { Funciones } from 'src/app/core/helpers/funciones/funciones';
import { PlanCuentaContable } from 'src/app/models/mantenimientos/maestras/plan-cuenta.model';
import { ConfigDocumentoService } from '../../../../../services/mantenimientos/configuracion/config-documento.service';
import { IClasificador } from '../../../../../interfaces/configuracion/clasificador.interface';
import { ClasificadorService } from '../../../../../services/mantenimientos/maestras/clasificador.service';
import { ITramaClasificador, ITramaMediosPago, BodyBeneficiarioBandeja } from '../../../../../models/mantenimientos/documentos/comprobante-pago.model';
import { IBeneficiarioBandeja, ITipoOperacionBandeja } from '../../../../../interfaces/comprobante-pago.interface';
import { VentBeneficiariosComponent } from '../vent-beneficiarios/vent-beneficiarios.component';
import { VentTipoOperacionComponent } from '../vent-tipo-operacion/vent-tipo-operacion.component';
import { TipoCuentaContable } from '../../../../../interfaces/configuracion/configurar-documento.interface';


@Component({
  selector: 'app-nuevo-comprobante',
  templateUrl: './nuevo-comprobante.component.html',
  styleUrls: ['./nuevo-comprobante.component.scss'],
  providers: [
    DialogService
  ]
})
export class NuevoComprobanteComponent implements OnInit, OnDestroy {
  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;
  @ViewChild('ddlTipoComprobante') ddlTipoComprobante: Dropdown;
  @ViewChild('ddlTipoReferencia') ddlTipoReferencia: Dropdown;
  @ViewChild('ddlBancoCargo') ddlBancoCargo: Dropdown;
  @ViewChild('ddlCuentaAbono') ddlCuentaAbono: Dropdown;
  @ViewChild('imCompPago') imCompPago: ElementRef;
  @ViewChild('idRazoSoc') idRazoSoc: ElementRef;
  @ViewChild('numRxH') numRxH: ElementRef;
  cuentasContables: PlanCuentaContable[];
  clasificadores: IClasificador[];
  disableTipoDoc: boolean = true;
  loadingZonaRegistral: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  loadingTipoComprobante: boolean = false;
  loadingTipoReferencia: boolean = false;
  loadingBancoCargo: boolean = false;
  loadingCuentaCargo: boolean = false;
  loadingCuentaBancaria: boolean = false;
  loadingCuentaAbono: boolean = false;
  loadingMediosPago: boolean = false;
  focusRazoSoc: boolean = false;
  bodyNuevoComprobantePago!: BodyNuevoComprobantePago;
  bodyBeneficiario!: BodyBeneficiarioBandeja;
  tramaContable: ITrama[] = [];
  tramaClasificador: ITramaClasificador[] = [];
  tramaMedioPago: ITramaMediosPago[] = [];
  userCode:any;
  fechaActual: Date;
  nuevoRecibo!: FormGroup;
  unsubscribe$ = new Subject<void>();
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  listaTipoRecibo: ReciboIngreso[] = [];
  listaEstadoRecibo: IResponse2[] = [];
  listaMediosPago: IResponse2[] = [];
  listaBancoCargo: any[] = [];
  listaBancoAbono: any[] = [];

  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaTipoReciboFiltro: ReciboIngreso[];
  listaEstadoReciboFiltro: IResponse2[];
  listaMediosPagoFiltro: IResponse2[];

  dataCuentaContable: TipoCuentaContable[] = [];

  $tipoDocReferencia: Observable<any>;
  $createComprobantePago: Subscription = new Subscription;

  constructor( 
    private router: Router,
    private fb: FormBuilder,
    private funciones: Funciones,
    private utilService: UtilService,
    private dialogService: DialogService,
    private generalService: GeneralService,
    private reciboService: ReciboIngresoService,
    private contableService: CuentaContableService,
    private clasificadorService: ClasificadorService,
    private configuracionService: ConfigDocumentoService,
    private comprobantePagoService: ComprobantePagoService,
    private sharingInformationService: SharingInformationService,
  ) { }

  ngOnInit(): void {
    this.userCode = localStorage.getItem('user_code')||'';
    this.bodyNuevoComprobantePago = new BodyNuevoComprobantePago();
    this._inicilializarListas();
    this._buscarZonaRegistral();
    this._buscarTipoComprobante();
    this._buscarMediosPago();
    this._contables();
    this._clasificadores();
    this._buscarBanco();
    this.fechaActual = new Date();
    this.nuevoRecibo = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]],
      tipoComp: ['', [ Validators.required ]],
      nroComp: [{value: '', disabled: true}, []],
      fecha: [this.fechaActual, [ Validators.required ]],
      banco: ['', [ Validators.required ]],
      nroCuenta: ['', [ Validators.required ]],
      nroSIAF: ['', []],
      monto: ['', [ Validators.required ]],
      tipoOper: ['', []],
      nuDocu: ['', []],
      noRazoSoc: ['', [ Validators.required ]],
      nuRxH: ['', []],
      fechaRxH: ['', []],
      checked: [false, []],
      concepto: ['', []],
      detallePresupuestal: this.fb.array([]),
      detallePatrimonial: this.fb.array([]),
      mediosPago: this.fb.array([])
    });
  }

  private _contables(): void {
    this.contableService.getBandejaCuentaContable('', '', 'A').pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          this.cuentasContables = data
        }
      });
  }

  private _clasificadores(): void {
    this.clasificadorService.getBandejaClasificadores(null, null, 'A').subscribe({
      next: ( data: IClasificador[] ) => {
        this.clasificadores = data;
      }
    });
  }

  private _inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoRecibo = [];
    this.listaEstadoRecibo = [];
    this.listaBancoCargo = [];
    this.listaBancoAbono = [];
    this.listaMediosPago = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    this.listaTipoRecibo.push({ idTipo: 0, noTipo: '(SELECCIONAR)' }); 
    this.listaEstadoRecibo.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' });   
    this.listaBancoCargo.push({ nidInstitucion: '*', cdeInst: '(SELECCIONAR)' });   
    this.listaBancoAbono.push({ idBanc: '*', nuCuenBanc: '(SELECCIONAR)' });   
    this.listaMediosPago.push({ ccodigoHijo: '*', cdescri: '(NINGUNO)' });   
  }

  private _buscarZonaRegistral(): void {
    this.loadingZonaRegistral = true;
    this.generalService.getCbo_Zonas_Usuario(this.userCode).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingZonaRegistral = false;    
        if (data.length === 1) {   
          this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });    
          this.nuevoRecibo.patchValue({ "zona": data[0].coZonaRegi});
          this.buscarOficinaRegistral();
        } else {    
          this.listaZonaRegistral.push(...data);
        }
      },
      error:( err )=>{
        this.loadingZonaRegistral = false;    
      }
    });
  }

  public buscarOficinaRegistral(): void {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.nuevoRecibo.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, this.nuevoRecibo.get('zona')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          this.nuevoRecibo.patchValue({ "oficina": data[0].coOficRegi});
          this.buscarLocal();
        } else {       
          this.listaOficinaRegistral.push(...data);
        }
        this.loadingOficinaRegistral = false;     
      }, (err) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal(): void {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.nuevoRecibo.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode,this.nuevoRecibo.get('zona')?.value,this.nuevoRecibo.get('oficina')?.value)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          this.nuevoRecibo.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);          
        } 
        this.loadingLocal = false;       
      }, (err) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  private _buscarMediosPago() {
    this.loadingMediosPago = true;
    this.generalService.getCbo_MediosDePago().pipe( takeUntil(this.unsubscribe$) )
        .subscribe({
          next: ( data ) => {
            this.loadingMediosPago = false;
            this.listaMediosPago.push(...data);
          },
          error: ( err ) => {
            this.loadingMediosPago = false;
          }
        });
  }

  public onChangeMediosPago( event: any, form: any, index: number ) {
    if ( event.value !== '*' ) {
      form.controls.mediosPago.controls[index].controls.numero.enable();
      form.controls.mediosPago.controls[index].controls.importe.enable();

      if ( event.value === '003' ) {
        form.controls.mediosPago.controls[index].controls.cantidad.enable();
        
        form.controls.mediosPago.controls[index].controls.numero.disable();
        form.controls.mediosPago.controls[index].controls.numero.setValue('');
      } else {
        form.controls.mediosPago.controls[index].controls.numero.enable();

        form.controls.mediosPago.controls[index].controls.cantidad.disable();
        form.controls.mediosPago.controls[index].controls.cantidad.setValue('');
      }
    } else {
      form.controls.mediosPago.controls[index].controls.numero.disable();
      form.controls.mediosPago.controls[index].controls.numero.setValue('');

      form.controls.mediosPago.controls[index].controls.importe.disable();
      form.controls.mediosPago.controls[index].controls.importe.setValue('');
      
      form.controls.mediosPago.controls[index].controls.cantidad.disable();
      form.controls.mediosPago.controls[index].controls.cantidad.setValue('');
    }
  }

  private _buscarTipoComprobante(): void {
    this.loadingTipoComprobante = true;
    this.reciboService.getTipoReciboIngreso('2').pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingTipoComprobante = false;
        this.listaTipoRecibo.push(...data); 
      },
      error:( err )=>{
        this.loadingTipoComprobante = false;    
      }
    });
  }

  public onChangeTipoComprobante( event: any): void {
    this.cuentaPres.clear();
    this.cuentaPatr.clear();

    const montoComp = this.nuevoRecibo.get('monto')?.value;
    const tipoComprobante = this.listaTipoRecibo.find( (item: ReciboIngreso) => item.idTipo === event.value);
    console.log('tipoComprobante', tipoComprobante)

    this.nuevoRecibo.patchValue({ "banco": tipoComprobante.idBanc });
    this._seleccionarCuenta(tipoComprobante.nuCuenBanc);
    this.nuevoRecibo.get('concepto')?.setValue( tipoComprobante.obConc );

    this.configuracionService.getConfiguracionById('2', tipoComprobante.idTipo).subscribe({
      next: ( data ) => {
        const monto = !(montoComp === '' || montoComp === '0.00') ? montoComp : Number('0').toFixed(2);

        data.listTipoDocumentoClsf.forEach( clsf => {
          this.cuentaPres.push( 
            this.fb.group({ 
              clasificador: [ clsf.coClsf, [ Validators.required ]],
              detClasificador: [ {
                value: clsf.deClsf,
                disabled: true 
            }, [ Validators.required ]],
              debe: [ monto, [ Validators.required ]],
              haber: [ monto , [ Validators.required ]],
              id:[ 0, [ Validators.required]],

            })
          )
        });

        data.listTipoDocumentoCont.forEach( cont => {
          const impDebe = cont.tiDebeHabe === 'D' ? monto : Number('0').toFixed(2);
          const impHabe = cont.tiDebeHabe === 'H' ? monto : Number('0').toFixed(2);

          this.cuentaPatr.push( 
            this.fb.group({ 
              cuenta: [ cont.coCuenCont, [ Validators.required ]],
              descripcion: [ {
                value:  cont.noCuenCont,
                disabled: true 
            }, [ Validators.required ]],
              debe: [ impDebe, [ Validators.required ]],
              haber: [ impHabe, [ Validators.required ]],
              id:[ 0, [ Validators.required]],
            })
          )
        });

        this.dataCuentaContable = data.listTipoDocumentoCont;
      },
      error: ( e ) => {
        this.cuentaPres.clear();
        this.cuentaPatr.clear();
      }
    });    
  }

  private _buscarBanco(): void {
    this.loadingBancoCargo = true;
    this.generalService.getCbo_Bancos().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingBancoCargo = false;
        this.listaBancoCargo.push(...data); 
      },
      error:( err )=>{
        this.loadingBancoCargo = false;    
      }
    });
  }

  public onChangeBanco(idBanc: any): void {
    this.loadingCuentaAbono = true;
    this.listaBancoAbono = [];
    this.listaBancoAbono.push({ idBanc: '*', nuCuenBanc: '(SELECCIONAR)' });
    this.nuevoRecibo.get('nombreCuentaCargo')?.setValue( '' );
    if (this.nuevoRecibo.get('bancoOrigen')?.value !== "*") {
      this.generalService.getCbo_Cuentas(idBanc.value).pipe(takeUntil(this.unsubscribe$))
      .subscribe( 
        (data: any[]) => {             
          this.listaBancoAbono.push(...data);
          this.loadingCuentaAbono = false;            
        }, 
        (err) => {       
          this.loadingCuentaAbono = false;                                 
      });      
    } else {
      this.loadingCuentaAbono = false;
    } 
  }

  private _seleccionarCuenta(nuCuenBanc: string) {  
    this.loadingCuentaAbono = true;
    this.listaBancoAbono = [];
    this.listaBancoAbono.push({ idBanc: '*', nuCuenBanc: '(SELECCIONAR)' });
    this.nuevoRecibo.get('nroCuenta')?.setValue('');
    if (this.nuevoRecibo.get('banco')?.value !== "*") {
      this.generalService.getCbo_Cuentas(this.nuevoRecibo.get('banco')?.value).pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: any[]) => {
        this.listaBancoAbono.push(...data);
        this.nuevoRecibo.patchValue({ "nroCuenta": nuCuenBanc});          
        this.loadingCuentaAbono = false;            
      }, (err) => {       
        this.loadingCuentaAbono = false;
      });      
    } else {
      this.loadingCuentaAbono = false;
    }
  }
  
  get detallePresupuestalArray(): FormArray | null { return  this.nuevoRecibo.get('detallePresupuestal') as FormArray }
  get detallePatrimonialArray(): FormArray | null { return  this.nuevoRecibo.get('detallePatrimonial') as FormArray }
  get cuentasPresupuestales(): FormGroup[] {
    return <FormGroup[]>this.detallePresupuestalArray?.controls
  }
  get cuentasPatrimoniales(): FormGroup[] {
    return <FormGroup[]>this.detallePatrimonialArray?.controls
  }
  get cuentaPres(): FormArray { return <FormArray>this.nuevoRecibo.get('detallePresupuestal') }
  get cuentaPatr(): FormArray { return <FormArray>this.nuevoRecibo.get('detallePatrimonial')}

  get mediosDePagoArray(): FormArray | null { return this.nuevoRecibo.get('mediosPago') as FormArray }
  get mediosDePago(): FormGroup[] {
    return <FormGroup[]>this.mediosDePagoArray?.controls
  }
  get medioPago(): FormArray { return <FormArray>this.nuevoRecibo.get('mediosPago') }

  addcuentaPresupuestal() {
    const ar = ( this.nuevoRecibo.get('detallePresupuestal') as FormArray )
    if ( environment.cantidadCuentas > ar.length )
      ar.push(
        this.fb.group({
          clasificador: ['', [ Validators.required ]],
          detClasificador: ['', [ Validators.required ]],
          debe: ['', [ Validators.required ]],
          haber: ['', [ Validators.required ]],
          id: [0, []],
        })
      )
  }

  deletecuentaPresupuestal( index: number ) {
    ( this.nuevoRecibo.get('detallePresupuestal') as FormArray ).removeAt(index)
  }

  addcuentaPatrimonial() {
    const ar = ( this.nuevoRecibo.get('detallePatrimonial') as FormArray )
    if ( environment.cantidadCuentas > ar.length )
      ar.push(
        this.fb.group({
          cuenta: ['', [ Validators.required ]],
          descripcion: ['', [ Validators.required ]],
          debe: ['', [ Validators.required ]],
          haber: ['', [ Validators.required ]],
          id:[0, []],
        })
      )
  }

  deletecuentaPatrimonial( index: number ) {
    ( this.nuevoRecibo.get('detallePatrimonial') as FormArray ).removeAt(index)
  }

  addMediosDePago() {
    const ar = ( this.nuevoRecibo.get('mediosPago') as FormArray )
    if ( ar.length < 4 )
      ar.push(
        this.fb.group({
          tipoDocu: ['', []],
          cantidad: [{value: '', disabled: true}, []],
          numero: [{value: '', disabled: true}, []],
          importe: [{value: '', disabled: true}, []],
          id: [0]
        })
      )
  }

  deleteMediosPago( index: number ) {
    ( this.nuevoRecibo.get('mediosPago') as FormArray ).removeAt(index)
  }

  public validarNumero(event:KeyboardEvent) {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  public validarMedioPagoPattern( event: any ) {
    const pattern = /[0-9, -]+/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  public validarMonto(event:any){
    const pattern = /^(\d*)((\.(\d{0,2})?)?)$/i;
    const inputChar = String.fromCharCode( event.which);
    const valorDecimales = `${event.target.value}`;
    let count=0;
    for (let index = 0; index < valorDecimales.length; index++) {
      const element = valorDecimales.charAt(index);
      if (element === '.') count++;
    }
    if (inputChar === '.') count++;
    if (!pattern.test(inputChar) || count === 2) {
        event.preventDefault();
    }
  }

  public formatearNumero(event:any){
    if (event.target.value) {
      const nuevoValor = this.funciones.convertirTextoANumero( event.target.value );
      const number = Number(nuevoValor).toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });

      this.nuevoRecibo.get('monto')?.setValue( number );

      this.cuentasPresupuestales.forEach( pres => {
        pres.get('debe')?.setValue( number );
        pres.get('haber')?.setValue( number );
      });

      const contablesList = this.dataCuentaContable.entries();
      for ( const [idx, cont] of contablesList ) {
        if ( cont.tiDebeHabe === 'D' ) {
          this.cuentasPatrimoniales[idx].get('debe')?.setValue( number );    
        }

        if ( cont.tiDebeHabe === 'H' ) {
          this.cuentasPatrimoniales[idx].get('haber')?.setValue( number );
        }
      }
    } else {
      this.cuentasPresupuestales.forEach( pres => {
        pres.get('debe')?.setValue( '0.00' );
        pres.get('haber')?.setValue( '0.00' );
      });

      this.cuentasPatrimoniales.forEach( patr => {
        patr.get('debe')?.setValue( '0.00' );
        patr.get('haber')?.setValue( '0.00' );
      });
    }
  }

  public formatearNumeroHabe(event:any){
    if (event.target.value) {
      const nuevoValor = this.funciones.convertirTextoANumero( event.target.value );
      event.target.value = Number(nuevoValor).toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });
    } 
  }

  public obtenerBeneficiarioPorDocumento( event: any ) {
    this.bodyBeneficiario = new BodyBeneficiarioBandeja();

    if ( event.target.value === '' ) {
      return;
    }

    this.bodyBeneficiario.nuBene = event.target.value;
    this.bodyBeneficiario.noBene = '';

    this.comprobantePagoService.getBeneficiariosById( this.bodyBeneficiario ).pipe( takeUntil(this.unsubscribe$) )
        .subscribe({
          next: ( data ) => {
            this.nuevoRecibo.get('nuDocu')?.setValue( data.nuBene );
            this.nuevoRecibo.get('noRazoSoc')?.setValue( data.noBene );
          },
          error: ( _err ) => {
          }
        });
  }

  public obtenerBeneficiarioPorNombre( event: any ) {
    this.bodyBeneficiario = new BodyBeneficiarioBandeja();

    if ( event.target.value === '' ) {
      return;
    }

    this.bodyBeneficiario.nuBene = '';
    this.bodyBeneficiario.noBene = event.target.value;

    this.comprobantePagoService.getBeneficiariosById( this.bodyBeneficiario ).pipe( takeUntil(this.unsubscribe$) )
        .subscribe({
          next: ( data ) => {
            this.nuevoRecibo.get('nuDocu')?.setValue( data.nuBene );
            this.nuevoRecibo.get('noRazoSoc')?.setValue( data.noBene );
          },
          error: ( _err ) => {
          }
        });
  }

  public grabar() {
    this.tramaContable = [];
    this.tramaClasificador = [];
    this.tramaMedioPago = [];

    let resultado: boolean = this._validarFormulario();
    if (!resultado) {
      return;
    }

    this.utilService.onShowProcessSaveEdit(); 
    const zona = this.nuevoRecibo.get('zona')?.value;
    const oficina = this.nuevoRecibo.get('oficina')?.value;
    const local = this.nuevoRecibo.get('local')?.value;
    const fecha = this.nuevoRecibo.get('fecha')?.value;
    const fechaFormat = (fecha !== "" && fecha !== null) ? this.funciones.convertirFechaDateToString(fecha) : '';
    const checked = this.nuevoRecibo.get('checked')?.value;
    const valueChecked = checked ? '1' : '0';
    const fechaRxH = this.nuevoRecibo.get('fechaRxH')?.value;
    const fechaFormatRxH = (fechaRxH !== "" && fechaRxH !== null) ? this.funciones.convertirFechaDateToString(fechaRxH) : '';
    const detallePresupuestal = this.nuevoRecibo.get('detallePresupuestal')?.value;
    const detallePatrimonial = this.nuevoRecibo.get('detallePatrimonial')?.value;
    const mediosDePago = this.nuevoRecibo.get('mediosPago')?.value;
    const montoCompPago: string = this.funciones.convertirTextoANumero( this.nuevoRecibo.get('monto')?.value );
    
    mediosDePago.map( (mp: any) => {
      if ( mp.tipoDocu !== '' || mp.tipoDocu !== '*' ) {
        const importe = this.funciones.convertirTextoANumero(mp.importe);

        this.tramaMedioPago.push({
          tiDocu: mp.tipoDocu,
          caDocu: mp.cantidad === undefined ? 0 : mp.cantidad,
          nuDocu: mp.numero,
          imDocu: Number(importe).toFixed(2)
        });
      }
    });

    detallePresupuestal.map( (value: any) => {
      if(value.clasificador !== '' &&  value.detClasificador !== '') {
        const haber = this.funciones.convertirTextoANumero(value.haber);
        const debe = this.funciones.convertirTextoANumero(value.debe);

        this.tramaClasificador.push({
          coClsf: value.clasificador,
          imHabe: Number(haber).toFixed(2),
          imDebe: Number(debe).toFixed(2),
        });
      }
    });

    detallePatrimonial.map( (value: any) => {
      if(value.cuenta !== '' &&  value.descripcion !== '') {
        const haber = this.funciones.convertirTextoANumero(value.haber);
        const debe = this.funciones.convertirTextoANumero(value.debe);

        this.tramaContable.push({
          coCuenCont: value.cuenta,
          imHabe: Number(haber).toFixed(2),
          imDebe: Number(debe).toFixed(2),
        });
      }
    });
    
    this.bodyNuevoComprobantePago.coZonaRegi = zona === '*' ? '' : zona;
    this.bodyNuevoComprobantePago.coOficRegi = oficina === '*' ? '' : oficina;
    this.bodyNuevoComprobantePago.coLocaAten = local === '*' ? '' : local;
    this.bodyNuevoComprobantePago.idTipoCompPago = this.nuevoRecibo.get('tipoComp')?.value;
    this.bodyNuevoComprobantePago.feCompPago = fechaFormat;
    this.bodyNuevoComprobantePago.idBanc = this.nuevoRecibo.get('banco')?.value;
    this.bodyNuevoComprobantePago.nuCuenBanc = this.nuevoRecibo.get('nroCuenta')?.value;
    this.bodyNuevoComprobantePago.nuSiaf = this.nuevoRecibo.get('nroSIAF')?.value;
    this.bodyNuevoComprobantePago.imCompPago = Number( montoCompPago );
    this.bodyNuevoComprobantePago.noTipoOper = this.nuevoRecibo.get('tipoOper')?.value;
    this.bodyNuevoComprobantePago.nuBene = this.nuevoRecibo.get('nuDocu')?.value;
    this.bodyNuevoComprobantePago.noBene = this.nuevoRecibo.get('noRazoSoc')?.value;
    this.bodyNuevoComprobantePago.nuReciHono = this.nuevoRecibo.get('nuRxH')?.value;
    this.bodyNuevoComprobantePago.feReciHono = fechaFormatRxH;
    this.bodyNuevoComprobantePago.inReciHonoRete = valueChecked;
    this.bodyNuevoComprobantePago.obConc = this.nuevoRecibo.get('concepto')?.value;
    this.bodyNuevoComprobantePago.tramaCuentaContable = this.tramaContable;
    this.bodyNuevoComprobantePago.tramaClasificadores = this.tramaClasificador;
    this.bodyNuevoComprobantePago.tramaMedioPago = this.tramaMedioPago;
    console.log('this.bodyNuevoComprobantePago', this.bodyNuevoComprobantePago);
    this.$createComprobantePago = this.comprobantePagoService.createComprobantePago(this.bodyNuevoComprobantePago).subscribe({
      next: ( d ) => {
        console.log('d', d);
        if (d.codResult < 0 ) {
          this.utilService.onShowAlert("info", "Atención", d.msgResult);
        } else if (d.codResult === 0) {
          this.utilService.onCloseLoading();
          this.router.navigate(['SARF/mantenimientos/documentos/comprobantes_de_pago/bandeja']);
        }
      },
      error:( d )=>{
        if (d.error.category === "NOT_FOUND") {
          this.utilService.onShowAlert("error", "Atención", d.error.description);        
        }else {
          this.utilService.onShowMessageErrorSystem();        
        }
      }
    });
  }

  private _validarFormulario(): boolean {   
    if (this.nuevoRecibo.value.zona === "" || this.nuevoRecibo.value.zona === "*" || this.nuevoRecibo.value.zona === null) {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una Zona Registral.");
      return false;  
    } else if (this.nuevoRecibo.value.oficina === "" || this.nuevoRecibo.value.oficina === "*" || this.nuevoRecibo.value.oficina === null) {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una Oficina Registral.");
      return false;      
    } else if (this.nuevoRecibo.value.local === "" || this.nuevoRecibo.value.local === "*" || this.nuevoRecibo.value.local === null) {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Local.");
      return false;      
    } else if (this.nuevoRecibo.value.tipoRecibo === "" || this.nuevoRecibo.value.tipoRecibo === "*" || this.nuevoRecibo.value.tipoRecibo === null) {      
      this.ddlTipoComprobante.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Tipo de Comprobante.");
      return false;      
    } else if (this.nuevoRecibo.value.fecha === "" || this.nuevoRecibo.value.fecha === "*" || this.nuevoRecibo.value.fecha === null) {      
      document.getElementById('idFecha').focus();            
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una Fecha.");
      return false;      
    } else if (this.nuevoRecibo.value.banco === "" || this.nuevoRecibo.value.banco === "*" || this.nuevoRecibo.value.banco === null) {      
      this.ddlBancoCargo.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Banco.");
      return false;      
    } else if (this.nuevoRecibo.value.nroCuenta === "" || this.nuevoRecibo.value.nroCuenta === "*" || this.nuevoRecibo.value.nroCuenta === null) {      
      this.ddlCuentaAbono.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un Número de Cuenta.");
      return false;      
    } else if (this.nuevoRecibo.value.monto === "" || this.nuevoRecibo.value.monto === "*" || this.nuevoRecibo.value.monto === null) {      
      this.imCompPago.nativeElement.focus();
      this.utilService.onShowAlert("warning", "Atención", "Debe ingresar el Monto.");
      return false;      
    } else if (this.nuevoRecibo.value.noRazoSoc === "" || this.nuevoRecibo.value.noRazoSoc === "*" || this.nuevoRecibo.value.noRazoSoc === null) {      
      this.idRazoSoc.nativeElement.focus();
      this.utilService.onShowAlert("warning", "Atención", "Seleccione o digite una Razón Social/Nombre-Apellido.");
      return false;      
    } else if (this.nuevoRecibo.value.nuRxH === '' && !(this.nuevoRecibo.value.fechaRxH === '' || this.nuevoRecibo.value.fechaRxH === null)) {      
      this.numRxH.nativeElement.focus();
      this.utilService.onShowAlert("warning", "Atención", "Debe ingresar el número de Recibo por Honorario.");
      return false;      
    } else if ( (this.nuevoRecibo.value.fechaRxH === '' || this.nuevoRecibo.value.fechaRxH === null) && this.nuevoRecibo.value.nuRxH !== '' ) {      
      this.utilService.onShowAlert("warning", "Atención", "Debe ingresar la fecha del Recibo por Honorario.");
      return false;      
    } else {
      return true;
    }
  }

  private _buscarClasificadores(event:any,$tipoCuenta:string,i:number){
    if (event.target.value !== '') {
      const controlName = ( this.nuevoRecibo.get($tipoCuenta) as FormArray );
      const valorArr = controlName.value;
      const valorArrAux = valorArr.map(( val: any, indx: number ) => {
        const clsfDes = this.clasificadores.find((clsf: any) => clsf.coClsf === val.clasificador);
        val.detClasificador =  clsfDes ? clsfDes.deClsf : val.deClsf;

        const debe = this.funciones.convertirTextoANumero(val.debe);
        val.debe  = Number(debe).toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });

        const haber = this.funciones.convertirTextoANumero(val.haber);
        val.haber = Number(haber).toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        return val;
      });
      controlName.setValue(valorArrAux);
    }
  }

  public buscarCuentabancariaPresupuestal (event:KeyboardEvent,$event1:any,i:number){
    // const pattern = /[0-9]/;
    // const inputChar = String.fromCharCode(event.charCode);
    // if (!pattern.test(inputChar)) {
    //     event.preventDefault();
    // }
    if (event.keyCode === 13) {
      this._buscarClasificadores($event1,'detallePresupuestal',i);
    }
  }

  public buscarCuentabancariaPresupuestalBlur(event:any,i:number){
    this._buscarClasificadores(event,'detallePresupuestal',i);
  }

  private _buscarCuentaContable(event:any,$tipoCuenta:string,i:number): void {
    if (event.target.value !== '') {
      const controlName = ( this.nuevoRecibo.get($tipoCuenta) as FormArray );
      const valorArr = controlName.value;
      const valorArrAux= valorArr.map((val:any,indx:number)=>{
        const cuentaDes = this.cuentasContables.find((cuent:any)=>  Number(cuent.coCuenCont) === Number(val.cuenta)) ;
        val.descripcion =  cuentaDes ? cuentaDes.noCuenCont : val.descripcion;

        const debe = this.funciones.convertirTextoANumero(val.debe);
        val.debe  = Number(debe).toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });

        const haber = this.funciones.convertirTextoANumero(val.haber);
        val.haber = Number(haber).toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        });
        return val;
      });
      controlName.setValue(valorArrAux);
    }
  }

  public buscarCuentabancariaPatrimoniales (event:KeyboardEvent,$event1:any,i:number): void {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
        event.preventDefault();
    }
    if (event.keyCode === 13) {
      this._buscarCuentaContable($event1,'detallePatrimonial',i);
    }
  }

  public buscarCuentabancariaPatrimonialesBlur(event:any,i:number): void {
    this._buscarCuentaContable(event,'detallePatrimonial',i);
  }

  public abrirVentanaBeneficiario() {
    const dialog = this.dialogService.open( VentBeneficiariosComponent, {
      header: 'Consulta de Beneficiarios',
      width: '50%',
      closable: false
    });

    dialog.onClose.pipe( takeUntil( this.unsubscribe$ ) )
          .subscribe( ( bene: IBeneficiarioBandeja ) => {
            if ( bene !== null ) {
              this.nuevoRecibo.get('nuDocu')?.setValue( bene.nuBene );
              this.nuevoRecibo.get('noRazoSoc')?.setValue( bene.noBene );
            }
          });
  }

  public abrirVentanaTipoOper() {
    const dialog = this.dialogService.open( VentTipoOperacionComponent, {
      header: 'Consultar Tipo de Operación',
      width: '40%',
      closable: false
    });

    dialog.onClose.pipe( takeUntil( this.unsubscribe$ ) )
          .subscribe( ( tipoOper: ITipoOperacionBandeja ) => {
            if ( tipoOper !== null ) {
              this.nuevoRecibo.get('tipoOper')?.setValue( tipoOper.noTipoOper );
            }
          });
  }

  public async cancelar() {
    const dataFiltrosRecibo: IDataFiltrosRecibo = await this.sharingInformationService.compartirDataFiltrosComprobantePagoObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecibo.esBusqueda) {
      dataFiltrosRecibo.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosComprobantePagoObservableData = dataFiltrosRecibo;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/comprobantes_de_pago/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaComprobantePagoObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.$createComprobantePago.unsubscribe(); 
  }

}
