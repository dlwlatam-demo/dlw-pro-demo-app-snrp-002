import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { IArchivoAdjunto, IRowReorder, IOnSelect } from '../../../../../interfaces/archivo-adjunto.interface';
import { ComprobantePagoValue } from '../../../../../interfaces/comprobante-pago.interface';

import { ArchivoAdjunto, TramaSustentoComp } from '../../../../../models/archivo-adjunto.model';
import { BodyComprobanteSustento } from '../../../../../models/mantenimientos/documentos/comprobante-pago.model';

import { GeneralService } from '../../../../../services/general.service';
import { ComprobantePagoService } from '../../../../../services/mantenimientos/documentos/comprobante-pago.service';

import { environment } from '../../../../../../environments/environment';

import { UploadComponent } from '../../../../upload/upload.component';
import { SiafComponent } from '../../../../upload/siaf/siaf.component';
import { ConstanciaComponent } from '../../../../upload/constancia/constancia.component';
import { OtrosComponent } from '../../../../upload/otros/otros.component';

@Component({
  selector: 'app-sustentos-comprobante',
  templateUrl: './sustentos-comprobante.component.html',
  styleUrls: ['./sustentos-comprobante.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class SustentosComprobanteComponent implements OnInit {

  isLoading!: boolean;
  coZonaRegi?: string;
  coOficRegi?: string;
  sizeFiles: number = 0;
  disableButton!: boolean;
  coTipoAdju: string = '002';

  add!: number;
  delete!: number;
  addSiaf!: number;
  deleteSiaf!: number;
  addCP!: number;
  deleteCP!: number;
  addOtro!: number;
  deleteOtro!: number;
  bandeja!: number;

  sustento!: FormGroup;

  files: ArchivoAdjunto[] = [];

  comprobante!: ComprobantePagoValue[];
  comprobanteSustento: TramaSustentoComp[] = [];
  comprobanteSustentoTemp: TramaSustentoComp[] = [];

  bodyComprobanteSustento!: BodyComprobanteSustento;

  @ViewChild(UploadComponent) upload: UploadComponent;
  @ViewChild(SiafComponent) uploadSiaf: SiafComponent;
  @ViewChild(ConstanciaComponent) uploadConst: ConstanciaComponent;
  @ViewChild(OtrosComponent) uploadOtros: OtrosComponent;

  constructor( 
    private fb: FormBuilder,
    private ref: DynamicDialogRef, 
    private config: DynamicDialogConfig,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private generalService: GeneralService,
    private comprobanteService: ComprobantePagoService
  ) { }

  ngOnInit(): void {
    this.bodyComprobanteSustento = new BodyComprobanteSustento();
    this.comprobante = this.config.data.comp as ComprobantePagoValue[];
    this.disableButton = this.config.data.disableButton;

    this.add = this.config.data.add;
    this.delete = this.config.data.delete;
    this.addSiaf = this.config.data.addSiaf;
    this.deleteSiaf = this.config.data.deleteSiaf;
    this.addCP = this.config.data.addCP;
    this.deleteCP = this.config.data.deleteCP;
    this.addOtro = this.config.data.addOtro;
    this.deleteOtro = this.config.data.deleteOtro;
    this.bandeja = this.config.data.bandeja;

    this.sustento = this.fb.group({
      nroComp: ['', [ Validators.required ]],
      fecha: ['', [ Validators.required ]],
      monto: ['', [ Validators.required ]],
      nroSiaf: ['', [ Validators.required ]],
      docs: this.fb.array([]),
      docSiaf: this.fb.array([]),
      docConst: this.fb.array([]),
      docOtros: this.fb.array([])
    });

    this.coZonaRegi = this.comprobante['0'].coZonaRegi;
    this.coOficRegi = this.comprobante['0'].coOficRegi;

    this.sustentosComprobante();
  }

  disableGrabar(): boolean {
    let disabled: boolean;

    if ( this.disableButton ) {
      disabled = false;
    } else if ( !this.disableButton && this.isDisabled ) {
      disabled = true;
    }

    return disabled;
  }

  get isDisabled() {
    return !(this.comprobante['0'].esDocu == '001' || this.comprobante['0'].esDocu == '006' || this.comprobante['0'].esDocu == '007'
            || this.comprobante['0'].esDocu == '004')
  }

  get disabledSiaf() {
    return !(this.comprobante['0'].esDocu == '001' || this.comprobante['0'].esDocu == '006' || this.comprobante['0'].esDocu == '007'
            || this.comprobante['0'].esDocu == '004') || (this.sustento.get('docSiaf') as FormArray).length == environment.fileLimitAdju;
  }

  get disabledConst() {
    return (this.sustento.get('docConst') as FormArray).length == environment.fileLimitAdju;
  }

  addUploadedFile(file: any) {
    const sustento = this.sustento.get('docs').value.find( ( d: any ) => file.fileId === d.doc );
    if ( !sustento ) {
      (this.sustento.get('docs') as FormArray).push(
        this.fb.group(
          {
            doc: [file.fileId, []],
            nombre: [file.fileName, []],
            idAdjunto: [file?.idAdjunto, []]
          }
        )
      )
    }
  }

  addUploadedFileSIAF(file: any) {
    (this.sustento.get('docSiaf') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  addUploadedFileConstancia(file: any) {
    (this.sustento.get('docConst') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  addUploadedFileOtros(file: any) {
    (this.sustento.get('docOtros') as FormArray).push(
      this.fb.group(
        {
          doc: [file.fileId, []],
          nombre: [file.fileName, []],
          idAdjunto: [file?.idAdjunto, []]
        }
      )
    )
  }

  deleteUploaded(fileId: string) {
    let index: number;

    const ar = <FormArray>this.sustento.get('docs')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.comprobanteSustentoTemp.findIndex( f => f.id == Number( fileId ) );
    this.comprobanteSustentoTemp.splice( index, 1 );
  }

  deleteUploadedSIAF(fileId: string) {
    let index: number;

    const ar = <FormArray>this.sustento.get('docSiaf')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.comprobanteSustentoTemp.findIndex( f => f.id == Number( fileId ) );
    this.comprobanteSustentoTemp.splice( index, 1 );
  }

  deleteUploadedConstancia(fileId: string) {
    let index: number;

    const ar = <FormArray>this.sustento.get('docConst')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.comprobanteSustentoTemp.findIndex( f => f.id == Number( fileId ) );
    this.comprobanteSustentoTemp.splice( index, 1 );
  }

  deleteUploadedOtros(fileId: string) {
    let index: number;

    const ar = <FormArray>this.sustento.get('docOtros')
    index = ar.controls.findIndex((ctl: AbstractControl) => {
      return ctl.get('doc')?.value == fileId
    })

    ar.removeAt(index)

    index = this.comprobanteSustentoTemp.findIndex( f => f.id == Number( fileId ) );
    this.comprobanteSustentoTemp.splice( index, 1 );
  }

  reordenarSustentos( event: IRowReorder ) {
    (this.sustento.get('docs') as FormArray).clear();
  }

  sustentosComprobante() {
    this.comprobanteService.getComprobanteSustento( this.comprobante[0].idCompPago ).subscribe({
      next: ( sustentos ) => {
        console.log('sustentos', sustentos);
        this.comprobanteSustentoTemp = sustentos.map( s => {
          return {
            id: s.idCompPagoSust,
            noDocuSust: s.noDocuSust,
            inCompPagoSiaf: s.inCompPagoSiaf,
            inConsPago: s.inConsPago,
            inSustOtro: s.inSustOtro,
            nuOrde: s.nuOrde,
            idGuidDocu: s.idGuidDocu
          }
        });

        this.upload.sustentosComp = this.comprobanteSustentoTemp.filter( s => (s.inCompPagoSiaf != '1' && s.inConsPago != '1' && s.inSustOtro != '1') ).sort(
          ( a, b ) => a.nuOrde < b.nuOrde ? -1 : a.nuOrde > b.nuOrde ? 1 : 0
        );
        this.uploadSiaf.sustentosSiaf = this.comprobanteSustentoTemp.filter( s => s.inCompPagoSiaf == '1' );
        this.uploadConst.sustentoConst = this.comprobanteSustentoTemp.filter( s => s.inConsPago == '1' );
        this.uploadOtros.sustentosOtros = this.comprobanteSustentoTemp.filter( s => s.inSustOtro == '1' );

        this.upload.goEdit();
        this.uploadSiaf.goEdit();
        this.uploadConst.goEdit();
        this.uploadOtros.goEdit();
      }
    });
  }

  parse() {
    const docs = (this.sustento.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  parseSiaf() {
    const docSiaf = (this.sustento.get('docSiaf') as FormArray)?.controls || [];

    docSiaf.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  parseConst() {
    const docConst = (this.sustento.get('docConst') as FormArray)?.controls || [];

    docConst.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  parseOtros() {
    const docConst = (this.sustento.get('docOtros') as FormArray)?.controls || [];

    docConst.forEach(g => {
      let idCarga = g.get('doc')?.value
      const nombre = g.get('nombre')?.value
      const idAdjunto = g.get('idAdjunto')?.value
      if ( !idAdjunto )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.coTipoAdju,
          coZonaRegi: this.coZonaRegi,
          coOficRegi: this.coOficRegi
        })
    });
  }

  grabar() {
    this.messageService.clear();
    
    this.parse();
    this.parseSiaf();
    this.parseConst();
    this.parseOtros();
    console.log('this.files =', this.files);

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de grabar los sustentos cargados?',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        if ( this.files.length != 0 ) {
          this.saveAdjuntoInFileServer();
        }
        else {
          this.saveSustentoInBD();
        }
      },
      reject: () => {
        this.files = [];
      }
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.messageService.add({
              severity: 'warn',
              summary: 'Atención',
              detail: data.msgResult,
              sticky: true
            });
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);

            this.addSustento(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.saveSustentoInBD();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          this.files = [];
          this.ref.close('reject');
        },
        complete: () => {
          Swal.close();
        }
      });
    }
  }

  addSustento(file?: any, guid?: string) {

    if ( file != '' && guid != '' ) {
      let trama = {} as TramaSustentoComp

      const docSiaf = this.sustento.get('docSiaf')?.value;
      const docConst = this.sustento.get('docConst')?.value;
      const docOtros = this.sustento.get('docOtros')?.value;

      if ( docSiaf.length !== 0 ) {
        if ( docSiaf[0].doc == guid ) {
          trama.inCompPagoSiaf = '1';
          trama.nuOrde = 0;
        }
        else {
          trama.inCompPagoSiaf = '0';
        }
      }
      else {
        trama.inCompPagoSiaf = '0';
      }

      if ( docConst.length !== 0 ) {
        if ( docConst[0].doc == guid ) {
          trama.inConsPago = '1';
          trama.nuOrde = 0;
        }
        else {
          trama.inConsPago = '0';
        }
      }
      else {
        trama.inConsPago = '0';
      }

      if ( docOtros.length !== 0 ) {
        const sustOtro = docOtros.find( ( o: any ) => o.doc === guid );
        if ( sustOtro ) {
          trama.inSustOtro = '1';
        }
        else {
          trama.inSustOtro = '0';
        }
      }
      else {
        trama.inSustOtro = '0';
      }
      
      trama.id = 0;
      trama.noDocuSust = file.deDocuAdju;
      trama.idGuidDocu = guid;

      this.comprobanteSustentoTemp.push( trama );
    }
  }

  saveSustentoInBD() {

    let indexOrder: number = 1;

    console.log('this.comprobanteSustentoTemp', this.comprobanteSustentoTemp);
    for ( let comprobante of this.comprobanteSustentoTemp ) {
      const index: number = this.sustento.get('docs')?.value.findIndex( ( d: any ) => comprobante.noDocuSust === d.nombre );
      this.comprobanteSustento[index] = comprobante;
    }

    const comprobanteOtros = this.comprobanteSustentoTemp.filter( c => c.inSustOtro === '1' );

    for ( let otros of comprobanteOtros ) {
      const index: number = this.sustento.get('docOtros')?.value
                                .findIndex( ( d: any ) => otros.noDocuSust === d.nombre ) + 
                                (this.sustento.get('docs') as FormArray).length;
      this.comprobanteSustento[index] = otros;
    }

    const inCompPagoSiaf = this.comprobanteSustentoTemp.find( c => c.inCompPagoSiaf === '1' );
    const inConsPago = this.comprobanteSustentoTemp.find( c => c.inConsPago === '1' );

    if ( inCompPagoSiaf ) {
      this.comprobanteSustento.unshift( inCompPagoSiaf );
    }

    if ( inConsPago ) {
      this.comprobanteSustento.splice( 1, 0, inConsPago );
    }

    this.comprobanteSustento.forEach( c => {
      c.noDocuSust = c.noDocuSust.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
                      .replace(/"/g, '&quot;').replace(/'/g, '&apos;');
    });

    this.comprobanteSustento.forEach( c => {
      if ( c.inCompPagoSiaf === '0' && c.inConsPago === '0' ) {
        c.nuOrde = indexOrder++
      }
    });

    this.bodyComprobanteSustento.idCompPago = this.comprobante['0'].idCompPago!
    this.bodyComprobanteSustento.trama = this.comprobanteSustento 
    console.log('bodyComprobanteSustento', this.bodyComprobanteSustento );

    this.comprobanteService.updateComprobantePagoSustento( this.bodyComprobanteSustento ).subscribe({
      next: ( d: any ) => {

        if ( d.codResult < 0 ) {
          this.messageService.add({
            severity: 'warn',
            summary: 'Atención',
            detail: d.msgResult,
            sticky: true
          });

          Swal.close();
          this.files = [];
          return;
        }

        Swal.close();
        this.ref.close('accept');
      },
      error: ( e ) => {
        console.log( e );
        this.messageService.add({
          severity: 'error',
          summary: 'Atención',
          detail: 'Error al guardar sustento.',
          sticky: true
        });

        Swal.close();
        this.files = [];
        this.isLoading = false;
      },
      complete: () => {
        this.files = [];
        this.isLoading = false
        Swal.close();
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
