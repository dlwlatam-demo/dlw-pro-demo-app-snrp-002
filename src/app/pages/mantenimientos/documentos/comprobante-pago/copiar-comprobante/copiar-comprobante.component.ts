import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Dropdown } from 'primeng/dropdown';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { ComprobantePagoValue } from 'src/app/interfaces/comprobante-pago.interface';
import { BodyCopiarComprobantePago } from 'src/app/models/mantenimientos/documentos/comprobante-pago.model';
import { GeneralService } from 'src/app/services/general.service';
import { ComprobantePagoService } from 'src/app/services/mantenimientos/documentos/comprobante-pago.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-copiar-comprobante',
  templateUrl: './copiar-comprobante.component.html',
  styleUrls: ['./copiar-comprobante.component.scss'],
  styles: [`
    #borde {
      padding: 5px;
      border: black 1px solid;
    }
  `]
})
export class CopiarComprobanteComponent implements OnInit {
  @ViewChild('ddlZona') ddlZona: Dropdown;
  @ViewChild('ddlOficinaRegistral') ddlOficinaRegistral: Dropdown;
  @ViewChild('ddlLocal') ddlLocal: Dropdown;
  copiarComprobantePago: FormGroup;
  unsubscribe$ = new Subject<void>();
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[];
  listaLocal: Local[];
  loadingZonaRegistral: boolean = false;
  loadingOficinaRegistral: boolean = false;
  loadingLocal: boolean = false;
  userCode:any;
  dataComprobantePago: ComprobantePagoValue;
  bodyCopiarComprobantePago: BodyCopiarComprobantePago;

  constructor(
    public config: DynamicDialogConfig,
    private dialogRef: DynamicDialogRef,
    private fb: FormBuilder,
    private generalService: GeneralService,
    private comprobantePagoService: ComprobantePagoService,
    private utilService: UtilService,
  ) { }

  ngOnInit(): void {
    this.bodyCopiarComprobantePago = new BodyCopiarComprobantePago()
    this.userCode = localStorage.getItem('user_code')||'';
    this.dataComprobantePago = this.config.data;
    this._buscarZonaRegistral();
    this.copiarComprobantePago = this.fb.group({
      zona: ['', [ Validators.required ]],
      oficina: ['', [ Validators.required ]],
      local: ['', [ Validators.required ]]
    })
    this._inicilializarListas();
  }

  private _inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaZonaRegistral.push({ coZonaRegi: '*', deZonaRegi: '(SELECCIONAR)' });
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' });    
  }

  private _buscarZonaRegistral(): void {
    this.loadingZonaRegistral = true;
    this.generalService.getCbo_Zonas_Usuario(this.userCode).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data: any ) => {
        this.loadingZonaRegistral = false;    
        if (data.length === 1) {   
          this.listaZonaRegistral.push({ coZonaRegi: data[0].coZonaRegi, deZonaRegi: data[0].deZonaRegi });    
          this.copiarComprobantePago.patchValue({ "zona": data[0].coZonaRegi});
          this.buscarOficinaRegistral(this.copiarComprobantePago.get('zona')?.value, true);
        } else {    
          this.listaZonaRegistral.push(...data);
          this.copiarComprobantePago.patchValue({'zona': this.dataComprobantePago.coZonaRegi});
          this.buscarOficinaRegistral(this.dataComprobantePago.coZonaRegi, true);
        }
      },
      error:( err )=>{
        this.loadingZonaRegistral = false;    
      }
    });
  }

  public buscarOficinaRegistral(codigoZona: string, primeraCarga: boolean): void {
    this.loadingOficinaRegistral = true;
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaOficinaRegistral.push({ coOficRegi: '*', deOficRegi: '(SELECCIONAR)' });
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.copiarComprobantePago.get('zona')?.value !== "*") {
      this.generalService.getCbo_Oficinas_Zonas(this.userCode, codigoZona)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: OficinaRegistral[]) => {  
        if (data.length === 1) {
          this.listaOficinaRegistral.push({ coOficRegi: data[0].coOficRegi, deOficRegi: data[0].deOficRegi });    
          primeraCarga && this.copiarComprobantePago.patchValue({ "oficina": data[0].coOficRegi});
          primeraCarga && this.buscarLocal(this.copiarComprobantePago.get('oficina')?.value, true);
        } else {       
          this.listaOficinaRegistral.push(...data);
          primeraCarga && this.copiarComprobantePago.patchValue({'oficina': this.dataComprobantePago.coOficRegi});
          primeraCarga && this.buscarLocal(this.dataComprobantePago.coOficRegi, true);
        }
        this.loadingOficinaRegistral = false;     
      }, (err) => {
        this.loadingOficinaRegistral = false;
      });
    } else {
      this.loadingOficinaRegistral = false;
    } 
  }

  public buscarLocal(codigoOficina: string, primeraCarga: boolean): void {
    this.loadingLocal = true;
    this.listaLocal = [];
    this.listaLocal.push({ coLocaAten: '*', deLocaAten: '(SELECCIONAR)' }); 
    if (this.copiarComprobantePago.get('oficina')?.value !== "*") {
      this.generalService.getCbo_Locales_Ofic(this.userCode, this.copiarComprobantePago.get('zona')?.value, codigoOficina)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( (data: Local[]) => {    
        if (data.length === 1) {
          this.listaLocal.push({ coLocaAten: data[0].coLocaAten, deLocaAten: data[0].deLocaAten });    
          primeraCarga && this.copiarComprobantePago.patchValue({ "local": data[0].coLocaAten});         
        } else {    
          this.listaLocal.push(...data);        
          primeraCarga && this.copiarComprobantePago.patchValue({'local': this.dataComprobantePago.coLocaAten});
        } 
        this.loadingLocal = false;       
      }, (err) => {
        this.loadingLocal = false;
      });
    } else {
      this.loadingLocal = false;
    }
  }

  public grabar(): void {
    let resultado: boolean = this._validarFormulario();
    if (!resultado) {
      return;
    }
    this.utilService.onShowProcessLoading("Copiando comprobante de pago"); 
    this.bodyCopiarComprobantePago.coZonaRegi = this.copiarComprobantePago.get('zona')?.value;
    this.bodyCopiarComprobantePago.coOficRegi = this.copiarComprobantePago.get('oficina')?.value;
    this.bodyCopiarComprobantePago.coLocaAten = this.copiarComprobantePago.get('local')?.value;
    this.bodyCopiarComprobantePago.idCompPago = this.dataComprobantePago.idCompPago;
    this.comprobantePagoService.copiarComprobantePago( this.bodyCopiarComprobantePago ).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: (d) => {
          if (d.codResult !== 0) {
            this.utilService.onShowAlert("warning", "Atención", d.msgResult);        
          } else {
            this.utilService.onCloseLoading();  
            this.dialogRef.close(true);
          }
        },
        error: (d) => {
          if (d.error.category === "NOT_FOUND") {
            this.utilService.onShowAlert("warning", "Atención", d.error.description);        
          }else {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      });
  }

  private _validarFormulario(): boolean {   
    if (this.copiarComprobantePago.value.zona === "" || this.copiarComprobantePago.value.zona === "*" || this.copiarComprobantePago.value.zona === null) {      
      this.ddlZona.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una zona registral.");
      return false;  
    } else if (this.copiarComprobantePago.value.oficina === "" || this.copiarComprobantePago.value.oficina === "*" || this.copiarComprobantePago.value.oficina === null) {      
      this.ddlOficinaRegistral.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione una oficina registral.");
      return false;      
    } else if (this.copiarComprobantePago.value.local === "" || this.copiarComprobantePago.value.local === "*" || this.copiarComprobantePago.value.local === null) {      
      this.ddlLocal.focus();      
      this.utilService.onShowAlert("warning", "Atención", "Seleccione un local.");
      return false;      
    } else {
      return true;
    }       
  }

  public cancelar(): void {
    this.dialogRef.close(false);
  }

}
