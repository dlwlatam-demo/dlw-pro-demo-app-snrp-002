import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';

import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';

import { ITipoOperacionBandeja } from '../../../../../interfaces/comprobante-pago.interface';

import { BodyTipoOperBandeja } from '../../../../../models/mantenimientos/documentos/comprobante-pago.model';

import { ComprobantePagoService } from '../../../../../services/mantenimientos/documentos/comprobante-pago.service';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-vent-tipo-operacion',
  templateUrl: './vent-tipo-operacion.component.html',
  styleUrls: ['./vent-tipo-operacion.component.scss'],
  providers: [
    MessageService
  ]
})
export class VentTipoOperacionComponent implements OnInit {

  public loadingTipoOper: boolean = false;

  public form!: FormGroup;

  public bodyTipoOper!: BodyTipoOperBandeja;

  public selectTipoOper: ITipoOperacionBandeja[] = [];
  public listTipoOperacionBandeja!: ITipoOperacionBandeja[];

  public currentPage: string = environment.currentPage;

  public unsubscribe$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private messageService: MessageService,
    private comprobanteService: ComprobantePagoService
  ) { }

  ngOnInit(): void {
    this.bodyTipoOper = new BodyTipoOperBandeja();

    this.form = this.fb.group({
      noTipoOper: ['', []]
    });
  }

  public buscarTipoOperacion() {
    const tipoOper = this.form.get('noTipoOper')?.value;

    if ( tipoOper === '' ) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Ingrese el Tipo de Operación.',
        sticky: true
      });

      return;
    }

    this.bodyTipoOper.noTipoOper = tipoOper;

    this._obtenerTipoOperacion( this.bodyTipoOper );
  }

  private _obtenerTipoOperacion( body: BodyTipoOperBandeja ) {
    this.selectTipoOper = [];
    this.loadingTipoOper = true;

    this.comprobanteService.getTipoOperacionBandeja( body ).pipe( takeUntil( this.unsubscribe$ ) )
        .subscribe({
          next: ( data ) => {
            this.loadingTipoOper = false;
            this.listTipoOperacionBandeja = data;
          },
          error: ( _err ) => {
            this.loadingTipoOper = false;
            this.listTipoOperacionBandeja = [];
          }
        });
  }

  public aceptar() {
    if ( this.selectTipoOper.length === 0 ) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Debe seleccionar un registro de la tabla.',
        sticky: true
      });

      return;
    }

    if ( this.selectTipoOper.length !== 1 ) {
      this.messageService.add({
        severity: 'warn',
        summary: 'No puede seleccionar más de un registro.',
        sticky: true
      });

      return;
    }

    this.ref.close( this.selectTipoOper[0] );
  }

  public cancelar() {
    this.ref.close( null );
  }

}
