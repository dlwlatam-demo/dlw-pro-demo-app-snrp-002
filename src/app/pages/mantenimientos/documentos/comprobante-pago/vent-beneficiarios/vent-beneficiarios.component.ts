import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';

import { IBeneficiarioBandeja } from '../../../../../interfaces/comprobante-pago.interface';

import { BodyBeneficiarioBandeja } from '../../../../../models/mantenimientos/documentos/comprobante-pago.model';

import { ComprobantePagoService } from '../../../../../services/mantenimientos/documentos/comprobante-pago.service';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-vent-beneficiarios',
  templateUrl: './vent-beneficiarios.component.html',
  styleUrls: ['./vent-beneficiarios.component.scss'],
  providers: [
    MessageService
  ]
})
export class VentBeneficiariosComponent implements OnInit {

  public loadingBeneficiarios: boolean = false;

  public form!: FormGroup;

  public bodyBeneficiario!: BodyBeneficiarioBandeja;

  public selectBeneficiario: IBeneficiarioBandeja[] = [];
  public listBeneficiarioBandeja!: IBeneficiarioBandeja[];

  public currentPage: string = environment.currentPage;

  public unsubscribe$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private messageService: MessageService,
    private comprobanteService: ComprobantePagoService
  ) { }

  ngOnInit(): void {
    this.bodyBeneficiario = new BodyBeneficiarioBandeja();

    this.form = this.fb.group({
      nuDocu: ['', []],
      noRazoSoc: ['', []]
    });
  }

  public buscarBeneficiario() {
    const nuDocu = this.form.get('nuDocu')?.value;
    const noRazoSoc = this.form.get('noRazoSoc')?.value;

    if ( nuDocu === '' && noRazoSoc === '' ) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Ingrese el Número de Documento o la Razón Social/Nombre-Apellido.',
        sticky: true
      });

      return;
    }

    this.bodyBeneficiario.nuBene = nuDocu;
    this.bodyBeneficiario.noBene = noRazoSoc;

    this._obtenerBeneficiarios( this.bodyBeneficiario );
  }

  private _obtenerBeneficiarios( body: BodyBeneficiarioBandeja ) {
    this.selectBeneficiario = [];
    this.loadingBeneficiarios = true;

    this.comprobanteService.getBeneficiariosComprobanteBandeja( body ).pipe( takeUntil(this.unsubscribe$) )
        .subscribe({
          next: ( data ) => {
            this.loadingBeneficiarios = false;
            this.listBeneficiarioBandeja = data
          },
          error: ( err ) => {
            this.loadingBeneficiarios = false;
            this.listBeneficiarioBandeja = [];
          }
        });
  }

  public aceptar() {
    if ( this.selectBeneficiario.length === 0 ) {
      this.messageService.add({
        severity: 'warn',
        summary: 'Debe seleccionar un registro de la tabla.',
        sticky: true
      });

      return;
    }

    if ( this.selectBeneficiario.length !== 1 ) {
      this.messageService.add({
        severity: 'warn',
        summary: 'No puede seleccionar más de un registro.',
        sticky: true
      });

      return;
    }

    this.ref.close( this.selectBeneficiario[0] );
  }

  public cancelar() {
    this.ref.close( null );
  }

}
