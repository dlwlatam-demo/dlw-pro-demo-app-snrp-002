import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { MessageService, ConfirmationService } from 'primeng/api';

import { ComprobantePagoService } from 'src/app/services/mantenimientos/documentos/comprobante-pago.service';
import { UsuariosService } from '../../../../../services/seguridad/usuarios.service';
import { IUsuarioSistema } from '../../../../../interfaces/seguridad/usuario.interface';
import { BodyNoUtilizadoComprobante } from '../../../../../models/mantenimientos/documentos/comprobante-pago.model';
import { ComprobantePagoValue } from '../../../../../interfaces/comprobante-pago.interface';

@Component({
  selector: 'app-no-utilizado',
  templateUrl: './no-utilizado.component.html',
  styleUrls: ['./no-utilizado.component.scss'],
  styles: [`
    #borde {
      padding: 10px;
      border: 1px solid #ced4da;
      border-radius: 6px;
    }
  `],
  providers: [
    MessageService,
    ConfirmationService
  ]
})
export class NoUtilizadoComponent implements OnInit {

  codigoUsuario: number = 0;
  isLoading: boolean = false;
  loadingUsuario: boolean = false;

  comprobantes!: ComprobantePagoValue[];
  listaUsuarios: IUsuarioSistema[] = [];

  bodyCambiarEstado!: BodyNoUtilizadoComprobante;

  unsubscribe$ = new Subject<void>();

  constructor(
    private dialogRef: DynamicDialogRef,
    private dialogConfig: DynamicDialogConfig,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private usuarioService: UsuariosService,
    private comprobantePagoService: ComprobantePagoService,
  ) { }

  ngOnInit(): void {
    this.bodyCambiarEstado = new BodyNoUtilizadoComprobante();

    this.comprobantes = this.dialogConfig.data.comprobantes;

    this.loadingUsuario = true;
    this.listaUsuarios.push({ coUsua: 0, noUsua: '(TODOS)' })
    this.usuarioService.getUsuariosPorSistema( 0 ).pipe( takeUntil( this.unsubscribe$ ) )
      .subscribe({
        next: ( data ) => {
          this.listaUsuarios.push(...data);
          this.loadingUsuario = false;
        },
        error: () => {
          this.loadingUsuario = false;
        }
      });
  }

  public grabar(): void {
    this.bodyCambiarEstado.coUsuaNout = this.codigoUsuario;
    this.bodyCambiarEstado.tramaCuentaContable = this.comprobantes?.map( value => {
      return { id: value.idCompPago?.toString() }
    });

    console.log('this.bodyCambiarEstado', this.bodyCambiarEstado)

    const textoAnular = (this.comprobantes?.length === 1) ? 'el Comprobante' : 'los Comprobantes';

    this.confirmService.confirm({
      message: `¿Estás seguro que deseas cambiar a estado NO UTILIZADO ${textoAnular} de Pago?`,
      accept: () => {
        this.isLoading = true;

        this.comprobantePagoService.cambiarEstadoReservadoNoUtilizado(this.bodyCambiarEstado).subscribe({
          next: ( d:any ) => {
            console.log('d', d);
            if (d.codResult < 0 ) {
              this.messageService.add({
                severity: 'warn',
                summary: 'Atención',
                detail: d.msgResult,
                sticky: true
              });

              this.isLoading = false;
              this.bodyCambiarEstado.onReset();
              return;

            } else if (d.codResult === 0 ) {
              this.isLoading = false;
              this.dialogRef.close('accept');
            }
          },

          error:( d )=>{
            this.dialogRef.close('reject');
          },
          complete: () => {
            this.isLoading = false;
            this.bodyCambiarEstado.onReset();
          }
        });
      },
      reject: () => {
        this.bodyCambiarEstado.onReset();
      }
    });
  }

  cancelar() {    
    this.dialogRef.close('');
  }

  public validarNumero(event:KeyboardEvent){
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
