import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Observable, Subject, Subscription } from 'rxjs';
import { first, takeUntil, map } from 'rxjs/operators';

import { ITrama } from 'src/app/models/mantenimientos/documentos/comprobante-pago.model';
import { ComprobantePagoService } from 'src/app/services/mantenimientos/documentos/comprobante-pago.service';
import { UtilService } from 'src/app/services/util.service';
import { Local, OficinaRegistral, ZonaRegistral } from 'src/app/interfaces/combos.interface';
import { IResponse2 } from 'src/app/interfaces/general.interface';
import { SharingInformationService } from 'src/app/core/services/sharing-services.service';
import { ComprobantePagoValue, IComprobantePagoById, IDataFiltrosRecibo, ITipoRecibo } from 'src/app/interfaces/comprobante-pago.interface';
import { GeneralService } from 'src/app/services/general.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';


@Component({
  selector: 'app-consultar-comprobante',
  templateUrl: './consultar-comprobante.component.html',
  styleUrls: ['./consultar-comprobante.component.scss']
})
export class ConsultarComprobanteComponent implements OnInit, OnDestroy {
  checkedString: string;
  $contables: any = [];
  disableTipoDoc: boolean = true;
  planComprobante: ITrama[] = [];
  fechaActual: Date;
  consultarRecibo!: FormGroup;
  unsubscribe$ = new Subject<void>();
  listaZonaRegistral: ZonaRegistral[] = [];
  listaOficinaRegistral: OficinaRegistral[] = [];
  listaLocal: Local[] = [];
  listaTipoRecibo: ITipoRecibo[] = [];
  listaMediosPago: IResponse2[] = [];
  listaBancoCargo: any[] = [];
  listaBancoAbono: any[] = [];
  comprobantePagoData: IComprobantePagoById;
  listaZonaRegistralFiltro: ZonaRegistral[];
  listaOficinaRegistralFiltro: OficinaRegistral[];
  listaLocalFiltro: Local[];
  listaTipoReciboFiltro: ComprobantePagoValue[];
  listaEstadoReciboFiltro: IResponse2[];
  listaMediosPagoFiltro: IResponse2[];
  loadingMediosPago: boolean = false;

  $mediosDePago: Observable<any>;
  mediosItem: any = { ccodigoHijo: '*', cdescri: '(NINGUNO)' };

  constructor( private router: Router,
               private activatedRoute: ActivatedRoute,
               private fb: FormBuilder,
               private comprobantePagoService: ComprobantePagoService,
               private generalService: GeneralService,
               private utilService: UtilService,
               private validateService: ValidatorsService,
               private sharingInformationService: SharingInformationService,
              ) { }

  ngOnInit(): void {
    this._getIdByRoute();
    this.fechaActual = new Date();
    this.consultarRecibo = this.fb.group({
      zona: [{value:'', disabled: true}, [ Validators.required ]],
      oficina: [{value:'', disabled: true}, [ Validators.required ]],
      local: [{value:'', disabled: true}, [ Validators.required ]],
      tipoComp: [{value:'', disabled: true}, [ Validators.required ]],
      nroComp: [{value:'', disabled: true}],
      fecha: [{value:this.fechaActual, disabled: true}, [ Validators.required ]],
      banco: [{value:'', disabled: true}, [ Validators.required ]],
      nroCuenta: [{value:'', disabled: true}, [ Validators.required ]],
      nroSIAF: [{value:'', disabled: true}],
      monto: [{value:'', disabled: true}, [ Validators.required ]],
      tipoOper: [{value:'', disabled: true}, []],
      nuDocu: [{value:'', disabled: true}, []],
      noRazoSoc: [{value:'', disabled: true}, [ Validators.required ]],
      nuRxH: [{value:'', disabled: true}, [ Validators.required ]],
      fechaRxH: [{value:'', disabled: true}, [ Validators.required ]],
      checked: [{value:false, disabled: true}, [ Validators.required ]],
      concepto: [{value:'', disabled: true}, [ Validators.required ]],
      detallePresupuestal: this.fb.array([]),
      detallePatrimonial: this.fb.array([]),
      mediosPago: this.fb.array([])
    });
  }

  private _getIdByRoute(): void {
    this.activatedRoute.params.pipe(takeUntil(this.unsubscribe$))
      .subscribe( params => this._getComprobantePagoById(params['id']))
    
  }

  private _getComprobantePagoById(id: number): void {
    this.utilService.onShowProcessLoading("Cargando la data"); 
    this.comprobantePagoService.getComprobantePagoById(id).pipe(takeUntil(this.unsubscribe$))
      .subscribe({
        next: ( data ) => {
          console.log('dataConsu', data);
          this.comprobantePagoData = data;
          this._handleInput();
          this._inicilializarListas();
          this.utilService.onCloseLoading(); 
        },
        error: ( err ) => {
          this.utilService.onCloseLoading(); 
        }
      })
  }
  
  private _handleInput(): void {
    const fechaComprobante = this.comprobantePagoData.feCompPago;
    const fechaFormat = this.validateService.reFormateaFecha(fechaComprobante);
    const retencionRecibo = this.comprobantePagoData.inReciHonoRete === '1' ? true : false;
    
    this.consultarRecibo.get('nroComp')?.setValue( this.comprobantePagoData.nuCompPago);
    this.consultarRecibo.get('fecha')?.setValue( fechaFormat );
    this.consultarRecibo.get('nroSIAF')?.setValue( this.comprobantePagoData.nuSiaf );
    this.consultarRecibo.get('monto')?.setValue( this.comprobantePagoData.imCompPago?.toFixed(2) );
    this.consultarRecibo.get('tipoOper')?.setValue( this.comprobantePagoData.noTipoOper );
    this.consultarRecibo.get('nuDocu')?.setValue( this.comprobantePagoData.nuBene );
    this.consultarRecibo.get('noRazoSoc')?.setValue( this.comprobantePagoData.noBene );
    this.consultarRecibo.get('nuRxH')?.setValue( this.comprobantePagoData.nuReciHono);
    this.comprobantePagoData.feReciHono ? this.consultarRecibo.patchValue({ "fechaRxH": new Date(this.comprobantePagoData.feReciHono)}) : this.consultarRecibo.get('fechaRxH')?.setValue( null );
    this.consultarRecibo.get('checked')?.setValue( retencionRecibo );
    this.consultarRecibo.get('concepto')?.setValue( this.comprobantePagoData.obConc);
  }

  private _inicilializarListas() {
    this.listaZonaRegistral = [];
    this.listaOficinaRegistral = [];
    this.listaLocal = [];
    this.listaTipoRecibo = [];
    this.listaMediosPago = [];
    this.listaBancoCargo = [];
    this.listaBancoAbono = [];
    this.listaZonaRegistral.push({ coZonaRegi: this.comprobantePagoData.coZonaRegi, deZonaRegi: this.comprobantePagoData.deZonaRegi });
    this.listaOficinaRegistral.push({ coOficRegi: this.comprobantePagoData.coOficRegi, deOficRegi: this.comprobantePagoData.deOficRegi });
    this.listaLocal.push({ coLocaAten: this.comprobantePagoData.coLocaAten, deLocaAten: this.comprobantePagoData.deLocaAten }); 
    this.listaTipoRecibo.push({ idTipo: this.comprobantePagoData.idTipoCompPago, noTipo: this.comprobantePagoData.noTipoCompPago }); 
    this.listaBancoCargo.push({ nidInstitucion: this.comprobantePagoData.idBanc, cdeInst: this.comprobantePagoData.noBanc });   
    this.listaBancoAbono.push({ idBanc: this.comprobantePagoData.nuCuenBanc, nuCuenBanc: this.comprobantePagoData.nuCuenBanc }); 
    
    this.comprobantePagoData.documentos.map( mp => {
      this.medioPago.push(
        this.fb.group({
          id: [ mp.idCompPagoDocu ],
          tipoDocu: [ {value: mp.noDocu, disabled: true}, []],
          cantidad: [ {value: mp.caDocu, disabled: true}, []],
          numero: [{value: mp.nuDocu, disabled: true}, []],
          importe: [ {value: Number( mp.imDocu )?.toLocaleString( 'en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          }), disabled: true}, []]
        })
      )
    });

    this.comprobantePagoData.cuentasClasificador.map( clsf => {
      this.cuentaPres.push( 
        this.fb.group({ 
          clasificador: [ {value: clsf.coClsf, disabled: true}, [ Validators.required ]],
          detClasificador: [ {value: clsf.deClsf, disabled: true}, [ Validators.required ]],
          debe: [ {value: Number(clsf.imDebe)?.toLocaleString( 'en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          }), disabled: true}, [ Validators.required ]],
          haber: [ {value: Number(clsf.imHabe)?.toLocaleString( 'en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          }), disabled: true}, [ Validators.required ]],
          id: [ clsf.idCompPagoClsf ]
        })
      )
    });

    this.comprobantePagoData.cuentasContables.map( value => {
      this.cuentaPatr.push( 
        this.fb.group({ 
          cuenta: [ {value: value.coCuenCont, disabled: true}, [ Validators.required ]],
          descripcion: [ {value: value.noCuenCont, disabled: true}, [ Validators.required ]],
          debe: [ {value: value.imDebe?.toLocaleString( 'en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          }), disabled: true}, [ Validators.required ]],
          haber: [ {value: value.imHabe?.toLocaleString( 'en-US', {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
          }), disabled: true}, [ Validators.required ]],
          idCuenCont: [ value.idCompPagoCont ]
        })
      )
    });
  }
  
  get detallePresupuestalArray(): FormArray | null { return  this.consultarRecibo.get('detallePresupuestal') as FormArray }
  get detallePatrimonialArray(): FormArray | null { return  this.consultarRecibo.get('detallePatrimonial') as FormArray }
  get cuentasPresupuestales(): FormGroup[] {
    return <FormGroup[]>this.detallePresupuestalArray?.controls
  }
  get cuentasPatrimoniales(): FormGroup[] {
    return <FormGroup[]>this.detallePatrimonialArray?.controls
  }
  get cuentaPres(): FormArray { return <FormArray>this.consultarRecibo.get('detallePresupuestal') }
  get cuentaPatr(): FormArray { return <FormArray>this.consultarRecibo.get('detallePatrimonial')}
  get mediosDePagoArray(): FormArray | null { return this.consultarRecibo.get('mediosPago') as FormArray }
  get mediosDePago(): FormGroup[] {
    return <FormGroup[]>this.mediosDePagoArray?.controls
  }
  get medioPago(): FormArray { return <FormArray>this.consultarRecibo.get('mediosPago') }

  public async cancelar() {
    const dataFiltrosRecibo: IDataFiltrosRecibo = await this.sharingInformationService.compartirDataFiltrosComprobantePagoObservable.pipe(first()).toPromise(); 
    if (dataFiltrosRecibo.esBusqueda) {
      dataFiltrosRecibo.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosComprobantePagoObservableData = dataFiltrosRecibo;
    }
    this.router.navigate(['SARF/mantenimientos/documentos/comprobantes_de_pago/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaComprobantePagoObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete(); 
  }

}
