import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ConfiguracionRoutingModule } from './configuracion-routing.module';
import { NgPrimeModule } from '../../../ng-prime/ng-prime.module';
import { PagesModule } from '../../pages.module';

import { BandejaConfigComponent } from './configurar-documentos/bandeja-config/bandeja-config.component';
import { ConceptoConfigComponent } from './configurar-documentos/concepto-config/concepto-config.component';
import { NuevaConfiguracionComponent } from './configurar-documentos/nueva-configuracion/nueva-configuracion.component';
import { EditarConfiguracionComponent } from './configurar-documentos/editar-configuracion/editar-configuracion.component';

import { FirmaDigitalComponent } from './firma-digital/firma-digital.component';
import { AsignarPersonasComponent } from './firma-digital/asignar-personas/asignar-personas.component';

import { BandejaTipoCartaComponent } from './tipo-carta-orden/bandeja-tipo-carta/bandeja-tipo-carta.component';
import { NuevoTipoComponent } from './tipo-carta-orden/nuevo-tipo/nuevo-tipo.component';
import { EditarTipoComponent } from './tipo-carta-orden/editar-tipo/editar-tipo.component';

import { BandejaTipoMemoComponent } from './tipo-memorandum/bandeja-tipo-memo/bandeja-tipo-memo.component';
import { NuevoTipoMemoComponent } from './tipo-memorandum/nuevo-tipo-memo/nuevo-tipo-memo.component';
import { EditarTipoMemoComponent } from './tipo-memorandum/editar-tipo-memo/editar-tipo-memo.component';

import { BandejaLogosComponent } from './configurar-logos/bandeja-logos/bandeja-logos.component';


@NgModule({
  declarations: [
    BandejaConfigComponent,
    ConceptoConfigComponent,
    NuevaConfiguracionComponent,
    EditarConfiguracionComponent,
    FirmaDigitalComponent,
    BandejaTipoCartaComponent,
    NuevoTipoComponent,
    EditarTipoComponent,
    BandejaTipoMemoComponent,
    NuevoTipoMemoComponent,
    EditarTipoMemoComponent,
    AsignarPersonasComponent,
    BandejaLogosComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ConfiguracionRoutingModule,
    NgPrimeModule,
    PagesModule
  ]
})
export class ConfiguracionModule { }
