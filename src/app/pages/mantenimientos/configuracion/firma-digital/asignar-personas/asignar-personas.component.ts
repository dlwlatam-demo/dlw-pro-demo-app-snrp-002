import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Observable, map, of } from 'rxjs';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { ITramaFirma } from '../../../../../interfaces/configuracion/firma-digital.interface';
import { IPerfilUsuario } from '../../../../../interfaces/seguridad/usuario.interface';

import { BodyFirmaDigital } from '../../../../../models/mantenimientos/configuracion/firma-digital.model';

import { FirmaDigitalService } from '../../../../../services/mantenimientos/configuracion/firma-digital.service';
import { UsuariosService } from '../../../../../services/seguridad/usuarios.service';
import { ValidatorsService } from '../../../../../core/services/validators.service';

import { environment } from '../../../../../../environments/environment';
import { GeneralService } from '../../../../../services/general.service';

@Component({
  selector: 'app-asignar-personas',
  templateUrl: './asignar-personas.component.html',
  styleUrls: ['./asignar-personas.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class AsignarPersonasComponent implements OnInit {

  idFirma!: number;
  
  perfil!: string;
  
  tipoDocu!: string;
  deDocu!: string;
  
  isLoading!: boolean;

  $usuarios: Observable<any> = of([{ coUsua: 0, noUsua: '(SELECCIONAR)' }]);
  $motivos: Observable<any> = of([{ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' }]);

  listaUsuarios!: IPerfilUsuario[];

  tramaFirma: ITramaFirma[] = [];

  bodyFirma!: BodyFirmaDigital;

  fechaDesde!: Date;
  fechaHasta!: Date;

  form!: FormGroup;

  userItem: any = { coUsua: 0, noUsua: '(SELECCIONAR)' };
  motivoItem: any = { ccodigoHijo: '*', cdescri: '(SELECCIONAR)' };

  constructor(
    private fb: FormBuilder,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private generalService: GeneralService,
    private messageService: MessageService,
    private confirmService: ConfirmationService,
    private usuarioService: UsuariosService,
    private validateService: ValidatorsService,
    private firmaService: FirmaDigitalService,
  ) { }

  ngOnInit(): void {
    this.bodyFirma = new BodyFirmaDigital();

    this.idFirma = this.config.data.idFirma;
    this.perfil = this.config.data.firma;
    this.tipoDocu = this.config.data.tipoDocu;
    this.deDocu = this.config.data.deDocu;

    this.fechaHasta = new Date();
    this.fechaDesde = new Date( this.fechaHasta.getFullYear(), this.fechaHasta.getMonth(), 1 );

    this.form = this.fb.group({
      deDocu: ['', [ Validators.required ]],
      perfil: ['', [ Validators.required ]],
      periodos: this.fb.array([
        this.fb.group({
          usuario: ['', [ Validators.required ]],
          fecDesde: [this.fechaDesde, [ Validators.required ]],
          fecHasta: [this.fechaHasta, [ Validators.required ]],
          check: ['', []],
          motivo: [{value: '', disabled: true}, []],
          id: [0, []]
        })
      ])
    });

    this.periodoUser.clear();

    this.form.get('deDocu')?.setValue( this.deDocu );
    this.form.get('perfil')?.setValue( this.perfil );

    this._perfilesDocumento();
  }

  private _perfilesDocumento() {

    this.firmaService.getPerfilPorDocumento( this.tipoDocu ).subscribe({
      next: ( data ) => {
        const index = data.findIndex( i => i.idConfFirm == this.idFirma );

        this._onChangeUsuarios( data[index].coPerf );
        this._onChangeMotivo();
        this._obtenerUsuarios();
      }
    });

  }

  private _obtenerUsuarios() {
    
    this.firmaService.getPerfilPorFirma( this.idFirma ).subscribe({
      next: ( data ) => {

        if ( data?.length! > 0 ) {
          this.periodoUser.clear();
        }

        data.map( user => {
          const fecDesde: string = this.validateService.reFormateaFecha( user.feDesd );
          const fecHasta: string = this.validateService.reFormateaFecha( user.feHast );
          const disableCheck: boolean = user.inLice === '0' ? false : true;
          const disableMotivo: boolean = user.inLice === '0' ? true : false;

          this.periodoUser.push(
            this.fb.group({
              usuario: [user.coUsua, [ Validators.required ]],
              fecDesde: [fecDesde, [ Validators.required ]],
              fecHasta: [fecHasta, [ Validators.required ]],
              check: [disableCheck, []],
              motivo: [{value: user.noLica, disabled: disableMotivo}, []],
              id: [user.idConfFirmUsua, []]
            })
          )
        });
      }
    });
  }

  private _onChangeUsuarios( coPerf: number ): void {
    this.$usuarios = this.usuarioService.getUsuarioPorPerfil( coPerf ).pipe(
      map( user => {
        user.unshift( this.userItem );
        return user;
      })
    );
  }

  private _onChangeMotivo(): void {
    this.$motivos = this.generalService.getCbo_MotivoLicencia().pipe(
      map( motivo => {
        motivo.unshift( this.motivoItem );
        return motivo;
      })
    )
  }

  public onChangeClickCheck( event: any, form: any, index: number ) {
    if ( event.checked === true ) {
      form.controls.periodos.controls[index].controls.motivo.enable();
    } else {
      form.controls.periodos.controls[index].controls.motivo.disable();
      form.controls.periodos.controls[index].controls.motivo.setValue('*');
    }
  }

  get periodosArray(): FormArray | null { return this.form.get('periodos') as FormArray };

  get periodos(): FormGroup[] {
    return <FormGroup[]> this.periodosArray?.controls;
  }

  get periodoUser(): FormArray { return <FormArray>this.form.get('periodos') }

  addPeriodos() {
    const ar = ( this.form.get('periodos') as FormArray );
    if ( environment.cantidadUsuarios > ar.length )
      ar.push(
        this.fb.group({
          usuario: ['', [ Validators.required ]],
          fecDesde: [this.fechaDesde, [ Validators.required ]],
          fecHasta: [this.fechaHasta, [ Validators.required ]],
          check: [false, []],
          motivo: [{value: '', disabled: true}, []],
          id: [0, []]
        })
      )
  }

  deletePeriodos( index: number ) {
    ( this.form.get('periodos') as FormArray ).removeAt( index )
  }

  grabar() {

    for ( let p of this.form.get('periodos')?.value ) {
      if ( p.check && p.motivo === '*' ) {
        this.messageService.add({ severity: 'warn', summary: 'Debe seleccionar un motivo de licencia', sticky: true });
        this.tramaFirma = [];
        return;
      }

      this.tramaFirma.push({
        idConfFirmUsua: p.id ? p.id : 0,
        coUsua: String( p.usuario ),
        feDesd: typeof p.fecDesde === 'string' ? p.fecDesde : this.validateService.formatDate2( p.fecDesde ),
        feHast: typeof p.fecHasta === 'string' ? p.fecHasta : this.validateService.formatDate2( p.fecHasta ),
        inLice: p.check === false ? '0' : '1',
        coLice: p.motivo === undefined ? '' : p.motivo
      });
    }

    this.bodyFirma.idConfFirm = String( this.idFirma );
    this.bodyFirma.trama = this.tramaFirma;

    console.log('this.bodyFirma', this.bodyFirma);
    
    this.confirmService.confirm({
      message: '¿Está Ud. seguro de grabar los siguientes periodos?',
      accept: () => {
        this.isLoading = true;

        this.firmaService.updateUsuarioFirmaDocumento( this.bodyFirma ).subscribe({
          next: ( d ) => {

            if ( d.codResult < 0 ) {
              this.messageService.add({
                severity: 'warn',
                summary: 'Atención',
                detail: d.msgResult,
                sticky: true
              });

              this.tramaFirma = [];
              return;
            }

            this.ref.close('accept');
          },
          error: ( _e ) => {
            console.log('_e', _e);
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      },
      reject: () => {
        this.tramaFirma = [];
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
