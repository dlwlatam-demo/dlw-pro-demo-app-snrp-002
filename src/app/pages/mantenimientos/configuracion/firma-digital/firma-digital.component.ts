import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, map } from 'rxjs';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService } from 'primeng/api';

import { FirmaDigital } from '../../../../models/mantenimientos/configuracion/firma-digital.model';

import { IFirmaDigital } from '../../../../interfaces/configuracion/firma-digital.interface';

import { GeneralService } from '../../../../services/general.service';
import { FirmaDigitalService } from '../../../../services/mantenimientos/configuracion/firma-digital.service';
import { UtilService } from '../../../../services/util.service';

import { environment } from '../../../../../environments/environment';
import { AsignarPersonasComponent } from './asignar-personas/asignar-personas.component';


@Component({
  selector: 'app-firma-digital',
  templateUrl: './firma-digital.component.html',
  styleUrls: ['./firma-digital.component.scss'],
  providers: [
    DialogService,
    MessageService
  ]
})
export class FirmaDigitalComponent implements OnInit {

  form!: FormGroup;
  enviadosList!: FirmaDigital[];

  selectedFirma1: IFirmaDigital[] = [];
  selectedFirma2: IFirmaDigital[] = [];
  selectedFirma3: IFirmaDigital[] = [];
  selectedFirma4: IFirmaDigital[] = [];

  $tipoDocumento!: Observable< any >;
  
  currentPage: string = environment.currentPage;

  constructor(
    private fb: FormBuilder,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private utilService: UtilService,
    private firmaService: FirmaDigitalService 
  ) { }

  ngOnInit(): void {

    const documentoItem: any = { ccodigoHijo: '*', cdescri: '(TODOS)' };
    
    this.$tipoDocumento = this.generalService.getCbo_Documento().pipe(
      map( d => {
        d.unshift( documentoItem );
        return d;
      })
    );

    this.form = this.fb.group({
      tipoDocu: ['', [ Validators.required ]]
    });

    this.buscaBandeja();
  }

  buscaBandeja() {
    let tipoDocu = this.form.get('tipoDocu')?.value;
    tipoDocu = tipoDocu == '*' ? '' : tipoDocu;

    this.buscarBandeja( tipoDocu );
  }

  buscarBandeja( tipoDocu: string ) {
    this.utilService.onShowProcessLoading('Cargando información');

    this.firmaService.getBandejaFirmaDigital( tipoDocu ).subscribe({
      next: ( data ) => {
        this.enviadosList = data;
      },
      error: () => {
        this.utilService.onCloseLoading();
        this.utilService.onShowAlert('info', 'Atención', 'No se encontraron registros');

        this.enviadosList = [];
      },
      complete: () => {
        this.utilService.onCloseLoading();
      }
    });
  }

  asignarPersonas() {

    if ( this.selectedFirma1.length === 0 && this.selectedFirma2.length === 0
          && this.selectedFirma3.length === 0 && this.selectedFirma4.length === 0 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro');
      return;
    }

    if ( this.selectedFirma1.length === 1 && (this.selectedFirma2.length >= 1
          || this.selectedFirma3.length >= 1 || this.selectedFirma4.length >= 1) ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede modificar más de un registro a la vez');
      return;
    }

    if ( this.selectedFirma2.length === 1 && (this.selectedFirma1.length >= 1
          || this.selectedFirma3.length >= 1 || this.selectedFirma4.length >= 1) ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede modificar más de un registro a la vez');
      return;
    }

    if ( this.selectedFirma3.length === 1 && (this.selectedFirma1.length >= 1
          || this.selectedFirma2.length >= 1 || this.selectedFirma4.length >= 1) ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede modificar más de un registro a la vez');
      return;
    }
    
    if ( this.selectedFirma4.length === 1 && (this.selectedFirma1.length >= 1
          || this.selectedFirma2.length >= 1 || this.selectedFirma3.length >= 1) ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede modificar más de un registro a la vez');
      return;
    }

    if ( this.selectedFirma1.length > 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede modificar más de un registro a la vez');
      return;
    }

    if ( this.selectedFirma2.length > 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede modificar más de un registro a la vez');
      return;
    }

    if ( this.selectedFirma3.length > 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede modificar más de un registro a la vez');
      return;
    }

    if ( this.selectedFirma4.length > 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede modificar más de un registro a la vez');
      return;
    }

    let dataFirma: any = {};

    if ( this.selectedFirma1.length === 1 ) {
      dataFirma = {
        tipoDocu: this.selectedFirma1[0].tiDocu,
        deDocu: this.selectedFirma1[0].deDocu,
        firma: this.selectedFirma1[0].firmaNro1,
        idFirma: this.selectedFirma1[0].idConfFirm1
      }
    } else if ( this.selectedFirma2.length === 1 ) {
      dataFirma = {
        tipoDocu: this.selectedFirma2[0].tiDocu,
        deDocu: this.selectedFirma2[0].deDocu,
        firma: this.selectedFirma2[0].firmaNro2,
        idFirma: this.selectedFirma2[0].idConfFirm2
      }
    } else if ( this.selectedFirma3.length === 1 ) {
      dataFirma = {
        tipoDocu: this.selectedFirma3[0].tiDocu,
        deDocu: this.selectedFirma3[0].deDocu,
        firma: this.selectedFirma3[0].firmaNro3,
        idFirma: this.selectedFirma3[0].idConfFirm3
      }
    } else if ( this.selectedFirma4.length === 1 ) {
      dataFirma = {
        tipoDocu: this.selectedFirma4[0].tiDocu,
        deDocu: this.selectedFirma4[0].deDocu,
        firma: this.selectedFirma4[0].firmaNro4,
        idFirma: this.selectedFirma4[0].idConfFirm4
      }
    }

    const dialog = this.dialogService.open( AsignarPersonasComponent, {
      header: 'Configuración Firma Digital - Personas',
      width: '65%',
      closable: false,
      data: dataFirma
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha grabado correctamente'
        });

        this.buscaBandeja();
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al crear su registro'
        });
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
