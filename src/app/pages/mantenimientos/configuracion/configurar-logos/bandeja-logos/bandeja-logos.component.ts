import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';

import { IArchivoAdjunto } from '../../../../../interfaces/archivo-adjunto.interface';
import { IResponse2 } from '../../../../../interfaces/general.interface';
import { IDataLogo, DataConfigLogo } from '../../../../../interfaces/configuracion/configurar-logos';

import { ArchivoAdjunto } from '../../../../../models/archivo-adjunto.model';
import { ListImagenesLogos, BodyConfigLogos, LogoImgTemp } from '../../../../../models/mantenimientos/configuracion/config-logos.model';
import { POSICION_LOGO } from '../../../../../models/enum/posicion-logo';

import { ConfigLogosService } from '../../../../../services/mantenimientos/configuracion/config-logos.service';
import { GeneralService } from '../../../../../services/general.service';
import { UtilService } from '../../../../../services/util.service';

@Component({
  selector: 'app-bandeja-logos',
  templateUrl: './bandeja-logos.component.html',
  styleUrls: ['./bandeja-logos.component.scss']
})
export class BandejaLogosComponent implements OnInit, OnDestroy {

  formLogos!: FormGroup;

  private sizeFiles: number = 0;

  public showPiePagina: boolean = true;

  public uploadEncabezadoUno: boolean = true;
  public uploadEncabezadoTres: boolean = false;

  public uploadPiePaginaUno: boolean = true;
  public uploadPiePaginaTres: boolean = false;

  public logoIzquierda: string = 'Izquierda';
  public logoCentro: string = 'Centro';
  public logoDerecha: string = 'Derecha';

  public disableEncabezado: boolean = true;
  public disableEncabezadoIz: boolean = true;
  public disableEncabezadoCe: boolean = true;
  public disableEncabezadoDe: boolean = true;
  public disablePiePagina: boolean = true;
  public disablePiePaginaIz: boolean = true;
  public disablePiePaginaCe: boolean = true;
  public disablePiePaginaDe: boolean = true;

  public loadingTipoDocumento: boolean = false;

  public ENCABEZADO = POSICION_LOGO.ENCABEZADO;
  public ENCABEZADO_IZQUIERDA = POSICION_LOGO.ENCABEZADO_IZQUIERDA;
  public ENCABEZADO_CENTRO = POSICION_LOGO.ENCABEZADO_CENTRO;
  public ENCABEZADO_DERECHA = POSICION_LOGO.ENCABEZADO_DERECHA;
  public PIE_PAGINA = POSICION_LOGO.PIE_PAGINA;
  public PIE_PAGINA_IZQUIERDA = POSICION_LOGO.PIE_PAGINA_IZQUIERDA;
  public PIE_PAGINA_CENTRO = POSICION_LOGO.PIE_PAGINA_CENTRO;
  public PIE_PAGINA_DERECHA = POSICION_LOGO.PIE_PAGINA_DERECHA;

  private files: ArchivoAdjunto[] = []

  public listaTipoDocumentos: IResponse2[] = [];

  private listaLogoEncabezado: ListImagenesLogos[] = [];
  private listaLogoPiePagina: ListImagenesLogos[] = [];

  private imagenTemporal: LogoImgTemp = {};

  private bodyConfigLogos!: BodyConfigLogos;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private utilService: UtilService,
    private generalService: GeneralService,
    private configLogosService: ConfigLogosService
  ) { }

  ngOnInit(): void {
    this.bodyConfigLogos = new BodyConfigLogos();

    this.formLogos = this.fb.group({
      tipoDocumento: [{value: '', disabled: true}, [ Validators.required ]],
      encabezado: ['1', []],
      encabeImg: ['', []],
      encabeImgIz: ['', []],
      encabeImgCe: ['', []],
      encabeImgDe: ['', []],
      piePagina: ['1', []],
      pieImg: ['', []],
      pieImgIz: ['', []],
      pieImgCe: ['', []],
      pieImgDe: ['', []],
      docs: this.fb.array([])
    });

    this._buscarTipoDocumento();
  }

  private _buscarTipoDocumento() {
    this.loadingTipoDocumento = true;
    this.listaTipoDocumentos.push({ ccodigoHijo: '*', cdescri: '(SELECCIONAR)' });

    this.generalService.getCbo_TipoDocumentoLogos().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data ) => {
        this.loadingTipoDocumento = false;
        this.formLogos.get('tipoDocumento')?.enable();

        if (data.length === 1) {   
          this.listaTipoDocumentos.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
          this.formLogos.patchValue({ 'tipoDocumento': data[0].ccodigoHijo });
        } else {
          this.listaTipoDocumentos.push(...data);
        }
      },
      error:( err )=>{
        this.loadingTipoDocumento = false;
        this.formLogos.get('tipoDocumento')?.enable();
      }
    });
  }

  public onChangeTipoDocumento( event: any ) {
    if ( event.value === '*' ) {
      this._onResetForm();
    }

    if ( event.value === '100' ) {
      this.formLogos.get('encabezado')?.setValue('1');
      this.onClickEncabezado();

      this.showPiePagina = false;
      // this.formLogos.get('piePagina')?.disable();
      // this.formLogos.get('piePagina')?.setValue('1');
      // this.onClickPiePagina();
      // this.uploadPiePaginaUno = false;
    }
    else {
      this.showPiePagina = true;
      // this.formLogos.get('piePagina')?.enable();
    }

    this._obtenerDataLogoDocumentos( event.value );
  }

  public onClickEncabezado() {
    const value = this.formLogos.get('encabezado')?.value;
    
    if ( value === '3' ) {
      if ( this.formLogos.get('encabeImg')?.value !== '' ) {
        this._eliminarLogoDeLista( this.formLogos.get('encabeImg')?.value );
      }

      if ( this.formLogos.get('encabeImgIz')?.value === '' ) {
        this.disableEncabezadoIz = true;
      }

      if ( this.formLogos.get('encabeImgCe')?.value === '' ) {
        this.disableEncabezadoCe = true;
      }

      if ( this.formLogos.get('encabeImgDe')?.value === '' ) {
        this.disableEncabezadoDe = true;
      }

      this.uploadEncabezadoUno = false;
      this.uploadEncabezadoTres = true;
      this.disableEncabezado = true;
      this.formLogos.get('encabeImg')?.setValue('');

      this.listaLogoEncabezado = [];
    }
    else if ( value === '1' ) {
      if ( this.formLogos.get('encabeImg')?.value === '' ) {
        this.disableEncabezado = true;
      }

      if ( this.formLogos.get('encabeImgIz')?.value !== '' ) {
        this._eliminarLogoDeLista( this.formLogos.get('encabeImgIz')?.value );
      }

      if ( this.formLogos.get('encabeImgCe')?.value !== '' ) {
        this._eliminarLogoDeLista( this.formLogos.get('encabeImgCe')?.value );
      }

      if ( this.formLogos.get('encabeImgDe')?.value !== '' ) {
        this._eliminarLogoDeLista( this.formLogos.get('encabeImgDe')?.value );
      }
      
      this.uploadEncabezadoUno = true;
      this.uploadEncabezadoTres = false;
      this.disableEncabezadoIz = true;
      this.disableEncabezadoCe = true;
      this.disableEncabezadoDe = true;

      this.formLogos.get('encabeImgIz')?.setValue('');
      this.formLogos.get('encabeImgCe')?.setValue('');
      this.formLogos.get('encabeImgDe')?.setValue('');

      this.listaLogoEncabezado = [];
    }
  }

  public onClickPiePagina() {
    const value = this.formLogos.get('piePagina')?.value;

    if ( value === '3' ) {
      if ( this.formLogos.get('pieImg')?.value !== '' ) {
        this._eliminarLogoDeLista( this.formLogos.get('pieImg')?.value );
      }

      if ( this.formLogos.get('pieImgIz')?.value === '' ) {
        this.disablePiePaginaIz = true;
      }

      if ( this.formLogos.get('pieImgCe')?.value === '' ) {
        this.disablePiePaginaCe = true;
      }

      if ( this.formLogos.get('pieImgDe')?.value === '' ) {
        this.disablePiePaginaDe = true;
      }

      this.uploadPiePaginaUno = false;
      this.uploadPiePaginaTres = true;
      this.disablePiePagina = true;
      this.formLogos.get('pieImg')?.setValue('');

      this.listaLogoPiePagina = [];
    }
    else if ( value === '1' ) {
      if ( this.formLogos.get('pieImg')?.value === '' ) {
        this.disablePiePagina = true;
      }

      if ( this.formLogos.get('pieImgIz')?.value !== '' ) {
        this._eliminarLogoDeLista( this.formLogos.get('pieImgIz')?.value );
      }

      if ( this.formLogos.get('pieImgCe')?.value !== '' ) {
        this._eliminarLogoDeLista( this.formLogos.get('pieImgCe')?.value );
      }

      if ( this.formLogos.get('pieImgDe')?.value !== '' ) {
        this._eliminarLogoDeLista( this.formLogos.get('pieImgDe')?.value );
      }

      this.uploadPiePaginaUno = true;
      this.uploadPiePaginaTres = false;
      this.disablePiePaginaIz = true;
      this.disablePiePaginaCe = true;
      this.disablePiePaginaDe = true;

      this.formLogos.get('pieImgIz')?.setValue('');
      this.formLogos.get('pieImgCe')?.setValue('');
      this.formLogos.get('pieImgDe')?.setValue('');

      this.listaLogoPiePagina = [];
    }
  }

  private _obtenerDataLogoDocumentos( tipoDocumento: string ) {
    if ( tipoDocumento === '*' ) {
      return;
    }

    this._onResetInputs();

    this.configLogosService.getLogosDocumentos( tipoDocumento ).pipe( takeUntil( this.unsubscribe$ ) )
      .subscribe({
        next: ( data ) => {
          console.log('dataGet', data);

          for ( let logo of data.listaDatosImagenes ) {
            if ( logo.inPagiUbic === '1' ) {
              const seccion = logo.inPagiAlin == '0' ? '1' : '3';
              this.formLogos.get('encabezado')?.setValue( seccion );
              
              this.onClickEncabezado();
              this._setDataEncabezado( logo );
            }
            else if ( logo.inPagiUbic === '2' ) {
              const seccion = logo.inPagiAlin == '0' ? '1' : '3';
              this.formLogos.get('piePagina')?.setValue( seccion );

              this.onClickPiePagina();
              this._setDataPiePagina( logo );
            }
          }

          this.listaLogoEncabezado = data.listaDatosImagenes.filter( e => e.inPagiUbic === '1' ).map( encabezado => {
            delete encabezado.data;
            delete encabezado.tiDocu;

            return encabezado;
          });

          this.listaLogoPiePagina = data.listaDatosImagenes.filter( p => p.inPagiUbic === '2' ).map( piePagina => {
            delete piePagina.data;
            delete piePagina.tiDocu;

            return piePagina;
          });
        },
        error: ( error ) => {
          if ( error.error.category === 'NOT_FOUND' ) {
            this.utilService.onShowAlert('info', 'Atención', error.error.description);
          }
          else if ( error.error.code === '500' ) {
            this.utilService.onShowMessageErrorSystem();
          }
        }
      });
  }

  public obtenerLogoCargado( file: any) {
    (this.formLogos.get('docs') as FormArray).push(
      this.fb.group({
        doc: [file.fileId, []],
        nombre: [file.fileName, []],
        idAdjunto: [file?.idAdjunto, []]
      })
    )
  }

  public obtenerDataFileLogo( event: IDataLogo ) {
    let tramaLogoData = {} as ListImagenesLogos;

    switch ( event.posicionDeLogo ) {
      case this.ENCABEZADO:
        if ( this.formLogos.get('encabeImg')?.value != '' ) {
          const imgInicio: ListImagenesLogos = this.listaLogoEncabezado.find( img => img.inPagiAlin === '0' );
          this._eliminarImgExistenteEncabezado( imgInicio.idGuidDocu );
        }

        this.formLogos.get('encabeImg')?.setValue( event.nameFile );
        this.imagenTemporal.encabezado = event.imgReader;
        this.disableEncabezado = false;
        
        tramaLogoData.inPagiUbic = '1';
        tramaLogoData.inPagiAlin = '0';
        tramaLogoData.noImag = event.nameFile;
        tramaLogoData.idGuidDocu = event.idGuid;
        this.listaLogoEncabezado.push( tramaLogoData );
          break;
      case this.ENCABEZADO_IZQUIERDA:
        if ( this.formLogos.get('encabeImgIz')?.value != '' ) {
          const imgIzquierda: ListImagenesLogos = this.listaLogoEncabezado.find( img => img.inPagiAlin === '1' );
          this._eliminarImgExistenteEncabezado( imgIzquierda.idGuidDocu );
        }

        this.formLogos.get('encabeImgIz')?.setValue( event.nameFile );
        this.imagenTemporal.encabezadoIzquierda = event.imgReader;
        this.disableEncabezadoIz = false;

        tramaLogoData.inPagiUbic = '1';
        tramaLogoData.inPagiAlin = '1';
        tramaLogoData.noImag = event.nameFile;
        tramaLogoData.idGuidDocu = event.idGuid;
        this.listaLogoEncabezado.push( tramaLogoData );
          break;
      case this.ENCABEZADO_CENTRO:
        if ( this.formLogos.get('encabeImgCe')?.value != '' ) {
          const imgCentro: ListImagenesLogos = this.listaLogoEncabezado.find( img => img.inPagiAlin === '2' );
          this._eliminarImgExistenteEncabezado( imgCentro.idGuidDocu );
        }

        this.formLogos.get('encabeImgCe')?.setValue( event.nameFile );
        this.imagenTemporal.encabezadoCentro = event.imgReader;
        this.disableEncabezadoCe = false;

        tramaLogoData.inPagiUbic = '1';
        tramaLogoData.inPagiAlin = '2';
        tramaLogoData.noImag = event.nameFile;
        tramaLogoData.idGuidDocu = event.idGuid;
        this.listaLogoEncabezado.push( tramaLogoData );
          break;
      case this.ENCABEZADO_DERECHA:
        if ( this.formLogos.get('encabeImgDe')?.value != '' ) {
          const imgDerecha: ListImagenesLogos = this.listaLogoEncabezado.find( img => img.inPagiAlin === '3' );
          this._eliminarImgExistenteEncabezado( imgDerecha.idGuidDocu );
        }

        this.formLogos.get('encabeImgDe')?.setValue( event.nameFile );
        this.imagenTemporal.encabezadoDerecha = event.imgReader;
        this.disableEncabezadoDe = false;

        tramaLogoData.inPagiUbic = '1';
        tramaLogoData.inPagiAlin = '3';
        tramaLogoData.noImag = event.nameFile;
        tramaLogoData.idGuidDocu = event.idGuid;
        this.listaLogoEncabezado.push( tramaLogoData );
          break;
      case this.PIE_PAGINA:
        if ( this.formLogos.get('pieImg')?.value != '' ) {
          const imgInicio: ListImagenesLogos = this.listaLogoPiePagina.find( img => img.inPagiAlin === '0' );
          this._eliminarImgExistentePiePagina( imgInicio.idGuidDocu );
        }

        this.formLogos.get('pieImg')?.setValue( event.nameFile );
        this.imagenTemporal.piePagina = event.imgReader;
        this.disablePiePagina = false;

        tramaLogoData.inPagiUbic = '2';
        tramaLogoData.inPagiAlin = '0';
        tramaLogoData.noImag = event.nameFile;
        tramaLogoData.idGuidDocu = event.idGuid;
        this.listaLogoPiePagina.push( tramaLogoData );
          break;
      case this.PIE_PAGINA_IZQUIERDA:
        if ( this.formLogos.get('pieImgIz')?.value != '' ) {
          const imgIzquierda: ListImagenesLogos = this.listaLogoPiePagina.find( img => img.inPagiAlin === '1' );
          this._eliminarImgExistentePiePagina( imgIzquierda.idGuidDocu );
        }

        this.formLogos.get('pieImgIz')?.setValue( event.nameFile );
        this.imagenTemporal.piePaginaIzquierda = event.imgReader;
        this.disablePiePaginaIz = false;

        tramaLogoData.inPagiUbic = '2';
        tramaLogoData.inPagiAlin = '1';
        tramaLogoData.noImag = event.nameFile;
        tramaLogoData.idGuidDocu = event.idGuid;
        this.listaLogoPiePagina.push( tramaLogoData );
          break;
      case this.PIE_PAGINA_CENTRO:
        if ( this.formLogos.get('pieImgCe')?.value != '' ) {
          const imgCentro: ListImagenesLogos = this.listaLogoPiePagina.find( img => img.inPagiAlin === '2' );
          this._eliminarImgExistentePiePagina( imgCentro.idGuidDocu );
        }

        this.formLogos.get('pieImgCe')?.setValue( event.nameFile );
        this.imagenTemporal.piePaginaCentro = event.imgReader;
        this.disablePiePaginaCe = false;

        tramaLogoData.inPagiUbic = '2';
        tramaLogoData.inPagiAlin = '2';
        tramaLogoData.noImag = event.nameFile;
        tramaLogoData.idGuidDocu = event.idGuid;
        this.listaLogoPiePagina.push( tramaLogoData );
          break;
      case this.PIE_PAGINA_DERECHA:
        if ( this.formLogos.get('pieImgDe')?.value != '' ) {
          const imgDerecha: ListImagenesLogos = this.listaLogoPiePagina.find( img => img.inPagiAlin === '3' );
          this._eliminarImgExistentePiePagina( imgDerecha.idGuidDocu );
        }

        this.formLogos.get('pieImgDe')?.setValue( event.nameFile );
        this.imagenTemporal.piePaginaDerecha = event.imgReader;
        this.disablePiePaginaDe = false;

        tramaLogoData.inPagiUbic = '2';
        tramaLogoData.inPagiAlin = '3';
        tramaLogoData.noImag = event.nameFile;
        tramaLogoData.idGuidDocu = event.idGuid;
        this.listaLogoPiePagina.push( tramaLogoData );
          break;
      default:
        this.formLogos.get('encabeImg')?.setValue('');
        this.formLogos.get('encabeImgIz')?.setValue('');
        this.formLogos.get('encabeImgCe')?.setValue('');
        this.formLogos.get('encabeImgDe')?.setValue('');
        this.formLogos.get('pieImg')?.setValue('');
        this.formLogos.get('pieImgIz')?.setValue('');
        this.formLogos.get('pieImgCe')?.setValue('');
        this.formLogos.get('pieImgDe')?.setValue('');
    }
  }

  private _eliminarLogoDeLista( nombre: string ) {
    const listaLogos = this.formLogos.get('docs')?.value
    
    const index = listaLogos.findIndex( ( f: any ) => f.nombre == nombre );
    if ( index > -1 ) {
      listaLogos.splice( index, 1 );
    }
  }

  private _eliminarImgExistenteEncabezado( idGuid: string ) {
    const index = this.listaLogoEncabezado.findIndex( f => f.idGuidDocu == idGuid );
    if ( index > -1 ) {
      this.listaLogoEncabezado.splice( index, 1 );
    }
  }

  private _eliminarImgExistentePiePagina( idGuid: string ) {
    const index = this.listaLogoPiePagina.findIndex( f => f.idGuidDocu == idGuid );
    if ( index > -1 ) {
      this.listaLogoPiePagina.splice( index, 1 );
    }
  }

  public eliminarImagenes( posicion: string, nombre: string ) {
    switch ( posicion ) {
      case this.ENCABEZADO:
        const imgInicio: ListImagenesLogos = this.listaLogoEncabezado.find( img => img.inPagiAlin === '0' );
        this._eliminarLogoDeLista( nombre );
        this._eliminarImgExistenteEncabezado( imgInicio.idGuidDocu );
        this.formLogos.get('encabeImg')?.setValue('');
        this.disableEncabezado = true;
          break;
      case this.ENCABEZADO_IZQUIERDA:
        const imgIzquierda: ListImagenesLogos = this.listaLogoEncabezado.find( img => img.inPagiAlin === '1' );
        this._eliminarLogoDeLista( nombre );
        this._eliminarImgExistenteEncabezado( imgIzquierda.idGuidDocu );
        this.formLogos.get('encabeImgIz')?.setValue('');
        this.disableEncabezadoIz = true;
          break;
      case this.ENCABEZADO_CENTRO:
        const imgCentro: ListImagenesLogos = this.listaLogoEncabezado.find( img => img.inPagiAlin === '2' );
        this._eliminarLogoDeLista( nombre );
        this._eliminarImgExistenteEncabezado( imgCentro.idGuidDocu );
        this.formLogos.get('encabeImgCe')?.setValue('');
        this.disableEncabezadoCe = true;
          break;
      case this.ENCABEZADO_DERECHA:
        const imgDerecha: ListImagenesLogos = this.listaLogoEncabezado.find( img => img.inPagiAlin === '3' );
        this._eliminarLogoDeLista( nombre );
        this._eliminarImgExistenteEncabezado( imgDerecha.idGuidDocu );
        this.formLogos.get('encabeImgDe')?.setValue('');
        this.disableEncabezadoDe = true;
          break;
      case this.PIE_PAGINA:
        const pieInicio: ListImagenesLogos = this.listaLogoPiePagina.find( img => img.inPagiAlin === '0' );
        this._eliminarLogoDeLista( nombre );
        this._eliminarImgExistentePiePagina( pieInicio.idGuidDocu );
        this.formLogos.get('pieImg')?.setValue('');
        this.disablePiePagina = true;
          break;
      case this.PIE_PAGINA_IZQUIERDA:
        const pieIzquierda: ListImagenesLogos = this.listaLogoPiePagina.find( img => img.inPagiAlin === '1' );
        this._eliminarLogoDeLista( nombre );
        this._eliminarImgExistentePiePagina( pieIzquierda.idGuidDocu );
        this.formLogos.get('pieImgIz')?.setValue('');
        this.disablePiePaginaIz = true;
          break;
      case this.PIE_PAGINA_CENTRO:
        const pieCentro: ListImagenesLogos = this.listaLogoPiePagina.find( img => img.inPagiAlin === '2' );
        this._eliminarLogoDeLista( nombre );
        this._eliminarImgExistentePiePagina( pieCentro.idGuidDocu );
        this.formLogos.get('pieImgCe')?.setValue('');
        this.disablePiePaginaCe = true;
          break;
      case this.PIE_PAGINA_DERECHA:
        const pieDerecha: ListImagenesLogos = this.listaLogoPiePagina.find( img => img.inPagiAlin === '3' );
        this._eliminarLogoDeLista( nombre );
        this._eliminarImgExistentePiePagina( pieDerecha.idGuidDocu );
        this.formLogos.get('pieImgDe')?.setValue('');
        this.disablePiePaginaDe = true;
          break;
      default:
        this._eliminarLogoDeLista('');
        this._eliminarImgExistenteEncabezado('');
        this._eliminarImgExistentePiePagina('');
    }
  }

  public previsualizarImagen( posicion: string ) {
    switch ( posicion ) {
      case this.ENCABEZADO:
        this._mostrarLogoCargado( this.imagenTemporal.encabezado );
          break;
      case this.ENCABEZADO_IZQUIERDA:
        this._mostrarLogoCargado( this.imagenTemporal.encabezadoIzquierda );
          break;
      case this.ENCABEZADO_CENTRO:
        this._mostrarLogoCargado( this.imagenTemporal.encabezadoCentro );
          break;
      case this.ENCABEZADO_DERECHA:
        this._mostrarLogoCargado( this.imagenTemporal.encabezadoDerecha );
          break;
      case this.PIE_PAGINA:
        this._mostrarLogoCargado( this.imagenTemporal.piePagina );
          break;
      case this.PIE_PAGINA_IZQUIERDA:
        this._mostrarLogoCargado( this.imagenTemporal.piePaginaIzquierda );
          break;
      case this.PIE_PAGINA_CENTRO:
        this._mostrarLogoCargado( this.imagenTemporal.piePaginaCentro );
          break;
      case this.PIE_PAGINA_DERECHA:
        this._mostrarLogoCargado( this.imagenTemporal.piePaginaDerecha );
          break;
      default:
        this._mostrarLogoCargado('');
    }
  }

  private _mostrarLogoCargado( data: any | string ) {

    if ( data.length > 36 ) {
      const doc = data;
      const byteCharacters = atob(doc!);
      const byteNumbers = new Array(byteCharacters.length);
      for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      const blob = new Blob([byteArray], { type: 'image/png' });
      const blobUrl = URL.createObjectURL(blob);
      const download = document.createElement('a');
      download.href =  blobUrl;
      download.setAttribute("target", "_blank");
      document.body.appendChild( download );
      download.click();
    }
    else {
	  this.generalService.downloadManagerImagen( data ).subscribe({
        next: ( response: any ) => {

			    let dataType = response.type;
          let binaryData = [];
          binaryData.push(response);

          const blob = new Blob(binaryData, { type: 'image/png' });
          const blobUrl = URL.createObjectURL(blob);
          const download = document.createElement('a');
          download.href =  blobUrl;
          download.setAttribute("target", "_blank");
          document.body.appendChild( download );
          download.click();
			  
        },
        error: ( e ) => {
          console.log( e );

        }
      });
	  
	  
    }

  }
  

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();  
  }

  parse() {
    const docs = (this.formLogos.get('docs') as FormArray)?.controls || [];

    docs.forEach(g => {
      let idCarga = g.get('doc')?.value;
      const nombre = g.get('nombre')?.value;
      const idAdjunto = g.get('idAdjunto')?.value;

      if ( idAdjunto === null )
        this.files.push({
          idGuidDocu: idCarga,
          deDocuAdju: nombre,
          coTipoAdju: this.formLogos.get('tipoDocumento')?.value,
          coZonaRegi: '',
          coOficRegi: ''
        })
    });
  }

  grabar() {

    if ( this.formLogos.invalid ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un Tipo de Documento');
      return;
    }

    this.parse();
    console.log('this.files =', this.files);

    this.utilService.onShowConfirm('¿Está Ud. seguro de grabar los logos cargados?').then( ( result ) => {
      if ( result.isConfirmed ) {
        this.utilService.onShowProcessLoading('Guardando la información');

        if ( this.files.length != 0 ) {
          this.saveAdjuntoInFileServer();
        }
        else {
          this.saveSustentoInBD();
        }
      }
      else if ( result.isDenied ) {
        this.files = [];
      }
    });
  }

  saveAdjuntoInFileServer() {
	  console.log("size = " + this.files.length);
	  this.sizeFiles = this.files.length;
	
    for ( let file of this.files ) {
      this.generalService.grabarAdjunto( file ).subscribe({
        next: ( data: IArchivoAdjunto ) => {

          if ( data.codResult < 0 ) {
            this.utilService.onShowAlert('warning', 'Atención', data.msgResult);
            return;
          }
          else {
            this.sizeFiles = this.sizeFiles - 1;
            console.log("size = " + this.sizeFiles);
            
            // this.addSustento(file, data.idCarga);
            
            if ( this.sizeFiles == 0 ) {
              this.saveSustentoInBD();
            }
          }
        },
        error: ( e ) => {
          console.log( e );

          this.files = [];
          this.utilService.onShowAlert('error', 'Atención', 'Hubo un error al grabar la imagen');
        }
      });
    }
  }

  saveSustentoInBD() {

    this.bodyConfigLogos.tiDocu = this.formLogos.get('tipoDocumento')?.value;
    this.bodyConfigLogos.listaImagenesPorTpoDoc = this.listaLogoEncabezado.concat( this.listaLogoPiePagina );
    console.log('bodyConfigLogos', this.bodyConfigLogos );
    
    this.configLogosService.updateLogosDocumentos( this.bodyConfigLogos ).subscribe({
      next: ( d: any ) => {

        if ( d.codResult < 0 ) {
          this.utilService.onCloseLoading();
          this.utilService.onShowAlert('warning', 'Atención', d.msgResult);
          
          this.files = [];
          return;
        }

        this.utilService.onCloseLoading();
        this.respuestaExitosaGrabar();
      },
      error: ( e ) => {
        console.log( e );
        this.utilService.onCloseLoading();

        if ( e.error.code === '500' ) {
          this.utilService.onShowMessageErrorSystem();
          return;
        }

        this.utilService.onShowAlert('error', 'Atención', 'Ocurrió un error al momento de grabar las imágenes');
        
        this.files = [];
      },
      complete: () => {
        this.files = [];
      }
    });
  }

  private async respuestaExitosaGrabar() {
    let respuesta = await this.utilService.onShowAlertPromise('success', 'Atención', 'Las imágenes se grabaron correctamente');
    if (respuesta.isConfirmed) {
      this.onChangeTipoDocumento({ value: '*' });
      this.formLogos.get('tipoDocumento')?.setValue('*');
      (this.formLogos.get('docs') as FormArray).clear();
    }
  }

  private _onResetInputs() {
    this.formLogos.get('encabeImg')?.setValue('');
    this.formLogos.get('encabeImgIz')?.setValue('');
    this.formLogos.get('encabeImgCe')?.setValue('');
    this.formLogos.get('encabeImgDe')?.setValue('');
    this.formLogos.get('pieImg')?.setValue('');
    this.formLogos.get('pieImgIz')?.setValue('');
    this.formLogos.get('pieImgCe')?.setValue('');
    this.formLogos.get('pieImgDe')?.setValue('');

    this.disableEncabezado = true;
    this.disableEncabezadoIz = true;
    this.disableEncabezadoCe = true;
    this.disableEncabezadoDe = true;
    this.disablePiePagina = true;
    this.disablePiePaginaIz = true;
    this.disablePiePaginaCe = true;
    this.disablePiePaginaDe = true;

    this.listaLogoEncabezado = [];
    this.listaLogoPiePagina = [];
  }

  private _onResetForm() {
    this.formLogos.get('encabezado')?.setValue('1');
    this.formLogos.get('piePagina')?.setValue('1');
    this._onResetInputs();

    this.uploadEncabezadoUno = true;
    this.uploadEncabezadoTres = false;
    this.uploadPiePaginaUno = true;
    this.uploadPiePaginaTres = false;
  }

  private _setDataEncabezado( logo: DataConfigLogo ) {
    switch ( logo.inPagiAlin ) {
      case '1':
        this.formLogos.get('encabeImgIz')?.setValue( logo.noImag );
        this.imagenTemporal.encabezadoIzquierda = logo.idGuidDocu
        this.disableEncabezadoIz = false;
          break;
      case '2':
        this.formLogos.get('encabeImgCe')?.setValue( logo.noImag );
        this.imagenTemporal.encabezadoCentro = logo.idGuidDocu
        this.disableEncabezadoCe = false;
          break;
      case '3':
        this.formLogos.get('encabeImgDe')?.setValue( logo.noImag );
        this.imagenTemporal.encabezadoDerecha = logo.idGuidDocu
        this.disableEncabezadoDe = false;
          break;
      default:
        this.formLogos.get('encabeImg')?.setValue( logo.noImag );
        this.imagenTemporal.encabezado = logo.idGuidDocu;
        this.disableEncabezado = false;
    }
  }

  private _setDataPiePagina( logo: DataConfigLogo ) {
    switch ( logo.inPagiAlin ) {
      case '1':
        this.formLogos.get('pieImgIz')?.setValue( logo.noImag );
        this.imagenTemporal.piePaginaIzquierda = logo.idGuidDocu
        this.disablePiePaginaIz = false;
          break;
      case '2':
        this.formLogos.get('pieImgCe')?.setValue( logo.noImag );
        this.imagenTemporal.piePaginaCentro = logo.idGuidDocu
        this.disablePiePaginaCe = false;
          break;
      case '3':
        this.formLogos.get('pieImgDe')?.setValue( logo.noImag );
        this.imagenTemporal.piePaginaDerecha = logo.idGuidDocu
        this.disablePiePaginaDe = false;
          break;
      default:
        this.formLogos.get('pieImg')?.setValue( logo.noImag );
        this.imagenTemporal.piePagina = logo.idGuidDocu
        this.disablePiePagina = false;
    }
  }

}
