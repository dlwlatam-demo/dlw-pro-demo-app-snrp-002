import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';

import { Documento, Caja, Banco } from '../../../../../interfaces/combos.interface';
import { IClasificador } from '../../../../../interfaces/configuracion/clasificador.interface';

import { CuentaBancaria } from '../../../../../models/mantenimientos/maestras/cuenta-bancaria.model';
import { ListClasificadores, ListCuentaContable } from '../../../../../models/mantenimientos/configuracion/config-documento.model';
import { PlanCuentaContable } from '../../../../../models/mantenimientos/maestras/plan-cuenta.model';

import { ConfigDocumentoService } from '../../../../../services/mantenimientos/configuracion/config-documento.service';
import { CuentaContableService } from '../../../../../services/mantenimientos/maestras/cuenta-contable.service';
import { ClasificadorService } from '../../../../../services/mantenimientos/maestras/clasificador.service';
import { GeneralService } from '../../../../../services/general.service';
import { UtilService } from '../../../../../services/util.service';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-nueva-configuracion',
  templateUrl: './nueva-configuracion.component.html',
  styleUrls: ['./nueva-configuracion.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class NuevaConfiguracionComponent implements OnInit {

  isLoading!: boolean;
  loading: boolean = false;
  loadLazyTimeout!: any;

  loadingBancos: boolean = true;
  loadingCuentas: boolean = true;
  loadingContables: boolean = true;
  loadingClasificador: boolean = true;

  listaTipoDocumento!: Documento[];
  listaBancos!: Banco[];
  listaCuentas!: CuentaBancaria[];
  listaClasificador!: IClasificador[];
  listaContables!: PlanCuentaContable[];
  listaCajas!: Caja[];

  nuevo!: FormGroup;

  listContables: ListCuentaContable[] = [];
  listClasificadores: ListClasificadores[] = [];

  constructor( 
    private fb: FormBuilder,
    private router: Router,
    private generalService: GeneralService,
    private confirmService: ConfirmationService,
    private utilService: UtilService,
    private contableService: CuentaContableService,
    private clasificadorService: ClasificadorService,
    private configuracionService: ConfigDocumentoService 
  ) { }

  ngOnInit(): void {

    this.nuevo = this.defConfiguracionForm();

    this.cuentaPres.clear();
    this.cuentaPatr.clear();

    this._inicializarListas();
    this._buscarTipoDocumento();
    this._buscarBancos();
    this._buscarCuentaContable();
    this._buscarClasificadores();
    this._buscarCaja();
  }

  // @HostListener('document:keydown', ['$event'])
  // handleKeyboardEvent(event: KeyboardEvent): void {
  //   if ( event.key === 'Enter' ) {
  //     this.grabar();
  //   } else if ( event.key === 'Escape' ) {
  //     this.cancelar();
  //   }
  // }

  private _inicializarListas() {
    this.listaTipoDocumento = [];
    this.listaBancos = [];
    this.listaCuentas = [];
    this.listaClasificador = [];
    this.listaContables = [];
    this.listaCajas = [];

    this.listaTipoDocumento.push({ coTipoDocu: '*', deTipoDocu: '(SELECCIONAR)' });
    this.listaBancos.push({ nidInstitucion: 0, cdeInst: '(SELECCIONAR)' });
    this.listaCuentas.push({ nuCuenBanc: '(SELECCIONAR)' });
    this.listaClasificador.push({ coClsf: '(NINGUNO)' });
    this.listaContables.push({ coCuenCont: '(NINGUNO)' });
    this.listaCajas.push({ coCaja: '*', deCaja: '(NINGUNO)' });
  }

  private defConfiguracionForm() {
    return this.fb.group({
      tipoDocu: ['', [ Validators.required ]],
      nomTipo: ['', [ Validators.required ]],
      banco: [{value: '', disabled: true}, [ Validators.required ]],
      nroCuent: ['', [ Validators.required ]],
      concepto: ['', []],
      cuenta: ['', []],
      nroDocu: ['', []],
      ope: ['', []],
      ruc:['', []],
      rxh:['', []],
      detallePresupuestal: this.fb.array([
        this.fb.group({
          clasificador: ['', []],
          detClasificador: ['', []]
        })
      ]),
      detallePatrimonial: this.fb.array([
        this.fb.group({
          cuenta: ['', []],
          descripcion: ['', []],
          caja: ['', []]
        })
      ])
    });
  }

  private _buscarTipoDocumento(): void {
    const tipoDocumento = this.generalService.getCbo_TipoDocumento();

    for ( let tiDocu of tipoDocumento ) {
      this.listaTipoDocumento.push( tiDocu )
    }
  }

  private _buscarBancos(): void {
    this.generalService.getCbo_Bancos().subscribe({
      next: ( data: Banco[] ) => {
        this.loadingBancos = false;
        this.nuevo.get('banco')?.enable();

        this.listaBancos.push( ...data );
      },
      error: ( _err ) => {
        this.loadingBancos = false;
        this.nuevo.get('banco')?.enable();
      }
    });
  }

  public buscarCuentas( event: any ): void {
    this.nuevo.get('nroCuent')?.disable();
    this.nuevo.get('cuenta')?.setValue('');

    this.listaCuentas = [];
    this.listaCuentas.push({ nuCuenBanc: '(SELECCIONAR)' });

    if ( this.nuevo.get('banco')?.value !== 0 ) {
      this.generalService.getCbo_Cuentas( event.value ).subscribe({
        next: ( data: any[] ) => {
          this.loadingCuentas = false;
          this.nuevo.get('nroCuent')?.enable();

          this.listaCuentas.push( ...data );
        },
        error: ( _err ) => {
          this.loadingCuentas = false;
          this.nuevo.get('nroCuent')?.enable();
        }
      });
    } else {
      this.loadingCuentas = false;
      this.nuevo.get('nroCuent')?.enable();
    }
  }

  private _buscarClasificadores(): void {
    this.clasificadorService.getBandejaClasificadores(null, null, 'A').subscribe({
      next: ( data: IClasificador[] ) => {
        this.loadingClasificador = false;
        this.listaClasificador.push( ...data );
      },
      error: ( _err ) => {
        this.loadingClasificador = false;
      }
    });
  }

  private _buscarCuentaContable(): void {
    this.contableService.getBandejaCuentaContable('', '', 'A').subscribe({
      next: ( data: PlanCuentaContable[] ) => {
        this.loadingContables = false;

        this.listaContables.push( ...data );
      },
      error: ( _err ) => {
        this.loadingContables = false;
      }
    });
  }

  private _buscarCaja(): void {
    const cajas = this.generalService.getCbo_Caja();

    for ( let c of cajas ) {
      this.listaCajas.push( c );
    }
  }

  public onChangeDescription( event: any ) {
    for ( let description of this.listaCuentas ) {
      if ( event.value == description.nuCuenBanc ) {
        this.nuevo.get('cuenta')?.setValue( description.noCuenBanc )
      }
    }
  }

  campoEsValido( campo: string ) {
    return this.nuevo.controls[campo].errors && 
              this.nuevo.controls[campo].touched
  }

  get detallePresupuestalArray(): FormArray | null { return  this.nuevo.get('detallePresupuestal') as FormArray }
  get detallePatrimonialArray(): FormArray | null { return  this.nuevo.get('detallePatrimonial') as FormArray }

  get cuentasPresupuestales(): FormGroup[] {
    return <FormGroup[]>this.detallePresupuestalArray?.controls
  }
  get cuentasPatrimoniales(): FormGroup[] {
    return <FormGroup[]>this.detallePatrimonialArray?.controls
  }

  get cuentaPres(): FormArray { return <FormArray>this.nuevo.get('detallePresupuestal') }
  get cuentaPatr(): FormArray { return <FormArray>this.nuevo.get('detallePatrimonial')}

  addcuentaPresupuestal() {
    const ar = ( this.nuevo.get('detallePresupuestal') as FormArray )
    if ( environment.cantidadCuentas > ar.length )
      ar.push(
        this.fb.group({
          clasificador: ['', []],
          detClasificador: ['', []]
        })
      )
  }

  deletecuentaPresupuestal( index: number ) {
    ( this.nuevo.get('detallePresupuestal') as FormArray ).removeAt(index)
  }

  addcuentaPatrimonial() {
    const ar = ( this.nuevo.get('detallePatrimonial') as FormArray )
    if ( environment.cantidadCuentas > ar.length )
      ar.push(
        this.fb.group({
          cuenta: ['', []],
          descripcion: ['', []],
          caja: ['', []]
        })
      )
  }

  deletecuentaPatrimonial( index: number ) {
    ( this.nuevo.get('detallePatrimonial') as FormArray ).removeAt(index)
  }

  grabar() {

    if ( this.nuevo.invalid ) {
      this.nuevo.markAllAsTouched();
      return;
    }

    this.listClasificadores = [];
    this.listContables = [];

    for ( let clsf of this.nuevo.get('detallePresupuestal')?.value ) {
      if ( clsf.clasificador !== '' && clsf.detClasificador !== '' ) {
        this.listClasificadores.push({
          coClsf: clsf.clasificador
        });
      }
    }

    for ( let cta of this.nuevo.get('detallePatrimonial')?.value ) {
      if ( cta.cuenta !== '' && cta.descripcion !== '' ) {
        this.listContables.push({
          coCuenCont: cta.cuenta,
          tiDebeHabe: cta.caja,
        });
      }
    }

    const data = {
      tiDocu: this.nuevo.get('tipoDocu')?.value,
      noTipo: this.nuevo.get('nomTipo')?.value,
      idBanc: this.nuevo.get('banco')?.value,
      nuCuenBanc: this.nuevo.get('nroCuent')?.value,
      obConce: this.nuevo.get('concepto')?.value,
      inTipoDocu: this.nuevo.get('nroDocu')?.value == true ? '1' : '0',
      inNumeOpe: this.nuevo.get('ope')?.value == true ? '1' : '0',
      inReciHono: this.nuevo.get('rxh')?.value == true ? '1' : '0',
      inNumeRuc: this.nuevo.get('ruc')?.value == true ? '1' : '0',
      listaCuentacontable: this.listContables,
      listaClasificadores: this.listClasificadores
    }

    console.log('newData', data);
    
    this.confirmService.confirm({
      message: '¿Está Ud. seguro de crear el siguiente registro?',
      accept: () => {
        this.utilService.onShowProcessSaveEdit();
        this.isLoading = true;

        this.configuracionService.createOrUpdateDocumento( 0, data  ).subscribe({
          next: ( d ) => {
            
            if ( d.codResult < 0 ) {
              Swal.close();
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
              return;
            }

            this.utilService.onCloseLoading();
            this._respuestaExitosa();
          },
          error: ( e ) => {

            if (e.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", e.error.description);        
            }else {
              this.utilService.onShowMessageErrorSystem();        
            }
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    }) 
  }

  onChangePresupuestales(event: any, index: any) {
    for ( let clsf of this.listaClasificador ) {
      if ( clsf.coClsf == event.value ) {
        this.cuentasPresupuestales[index].get('detClasificador').setValue( clsf.deClsf );
        return;
      }
    }
  }

  onChangePatrimoniales(event: any, index: any) {
    for ( let contable of this.listaContables ) {
      if ( contable.coCuenCont == event.value ) {
        this.cuentasPatrimoniales[index].get('descripcion').setValue( contable.noCuenCont );
        return;
      }
    }
  }

  private async _respuestaExitosa() {
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Su registro se grabó satisfactoriamente.`);
    
    if ( respuesta.isConfirmed ) {
      this.router.navigate(['SARF/mantenimientos/configuracion/configurar_documentos/bandeja']);
    }
  }

  cancelar() {
    this.router.navigate(['SARF/mantenimientos/configuracion/configurar_documentos/bandeja']);
  }

  get mostrarCheckboxs(): boolean {
    return this.nuevo.get('tipoDocu')?.value == '2';
  }

}
