import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService, ConfirmationService } from 'primeng/api';

import { Estado, Documento } from '../../../../../interfaces/combos.interface';
import { IBandejaConfiguracion } from '../../../../../interfaces/global.interface';

import { BodyConfiguracion, ConfigurarDocumento } from '../../../../../models/mantenimientos/configuracion/config-documento.model';
import { MenuOpciones } from '../../../../../models/auth/Menu.model';

import { ConfigDocumentoService } from '../../../../../services/mantenimientos/configuracion/config-documento.service';
import { GeneralService } from '../../../../../services/general.service';
import { SharingInformationService } from '../../../../../core/services/sharing-services.service';

import { ConceptoConfigComponent } from '../concepto-config/concepto-config.component';

import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';

import { environment } from '../../../../../../environments/environment';
import { IDataFiltrosConfiguracion } from '../../../../../interfaces/configuracion/configurar-documento.interface';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-bandeja-config',
  templateUrl: './bandeja-config.component.html',
  styleUrls: ['./bandeja-config.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class BandejaConfigComponent extends BaseBandeja implements OnInit, OnDestroy {

  disabledEdit!: boolean;
  registros: string[] = [];
  esBusqueda: boolean = false;

  documentos!: FormGroup;
  
  $estado!: Estado[];
  $tipoDocumento!: Documento[];
  enviadosList!: ConfigurarDocumento[];

  bodyConfigDocu!: BodyConfiguracion;

  configurarDocumentoProceso: IBandejaConfiguracion = {
    configDocu: null,
    tipoDocu: '',
    proceso: ''
  }

  unsubscribe$ = new Subject<void>();

  currentPage: string = environment.currentPage;

  constructor( 
    private fb: FormBuilder,
    private router: Router,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private confirmService: ConfirmationService,
    private configuracionService: ConfigDocumentoService,
    private sharingInformationService: SharingInformationService 
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Configuracion_de_Documentos;
    this.btnConfigurarDocumentos();

    this.bodyConfigDocu = new BodyConfiguracion();

    let estadoItem: Estado = { deEstado: '(TODOS)', coEstado: '*' };

    this.$tipoDocumento = this.generalService.getCbo_TipoDocumento();;
    this.$estado = this.generalService.getCbo_Estado();
    this.$estado.unshift( estadoItem );

    this.documentos = this.fb.group({
      tipoDocu: ['', [ Validators.required ]],
      descripcion: ['', []],
      estado: ['A', [ Validators.required ]]
    });

    this._dataFiltrosConfiguracion()
  }

  private _dataFiltrosConfiguracion(): void {
    this.sharingInformationService.compartirDataFiltrosConfiguracionDocumentoObservable.pipe( takeUntil( this.unsubscribe$ ) )
        .subscribe( ( data: IDataFiltrosConfiguracion ) => {
          if ( data.esCancelar && data.bodyConfig !== null ) {
            this._setearCamposFiltro(data);
            this.buscaBandeja(false);
          } else {
            this.buscaBandeja(true);
          }
        });
  }

  private _setearCamposFiltro(dataFiltro: IDataFiltrosConfiguracion) {
    this.bodyConfigDocu = dataFiltro.bodyConfig;
    this.documentos.patchValue({ "tipoDocu": this.bodyConfigDocu.tiDocu});
    this.documentos.patchValue({ "descripcion": this.bodyConfigDocu.noTipo});
    this.documentos.patchValue({ "estado": this.bodyConfigDocu.inRegi});
  }

  buscaBandeja( esCargaInicial: boolean ) {
    let tipoDocu = this.documentos.get('tipoDocu')?.value;
    tipoDocu = tipoDocu == '' ? '1' : tipoDocu;

    const descripcion = this.documentos.get('descripcion')?.value;

    let estado = this.documentos.get('estado')?.value;
    if ( estado == '' ) {
      estado = 'A';
    } else if ( estado == '*' ) {
      estado = '';
    }

    this.esBusqueda = (esCargaInicial) ? false : true;

    this.bodyConfigDocu.tiDocu = tipoDocu;
    this.bodyConfigDocu.noTipo = descripcion;
    this.bodyConfigDocu.inRegi = estado;

    this.buscarBandeja( tipoDocu, descripcion, estado );
  }

  buscarBandeja( tipo: string, des: string, est: string ) {
    Swal.showLoading();
    this.configuracionService.getBandejaConfiguracion( tipo, des, est ).subscribe({
      next: ( data ) => {

        for ( let d of data ) {
          if ( d.inRegi == 'A' )
            d.inRegi = 'Activo';
          else
            d.inRegi = 'Inactivo';
        }

        this.enviadosList = data;
      },
      error: () => {
        Swal.close()
        Swal.fire(
          'No se encuentran registros con los datos ingresados',
          'Configurar Documentos',
          'info'
        );

        this.enviadosList = [];
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  nuevaConfiguracion() {
    // document.getElementById('btnNuevo').blur();

    this.router.navigate(['SARF/mantenimientos/configuracion/configurar_documentos/nuevo']);

    // const dialog = this.dialogService.open( NuevaConfiguracionComponent, {
    //   header: 'Registrar Nueva Configuración',
    //   width: '60%',
    //   closable: false
    // });

    // dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
    //   if ( type == 'accept' ) {
    //     this.messageService.add({ 
    //       severity: 'success', 
    //       summary: 'Su registro se ha grabado correctamente'
    //     });

    //     this.buscaBandeja();
    //   }
    //   else if ( type == 'reject' ) {
    //     this.messageService.add({
    //       severity: 'error',
    //       summary: 'Ha ocurrido un problema al crear su registro'
    //     });
    //   }
    // });
  }

  editOrView( value: string ) {
    document.getElementById('btnEditar').blur();
    
    if ( !this.validateSelect( value ) )
      return;

    this.configurarDocumentoProceso.configDocu = this.selectedValues[0];
    this.configurarDocumentoProceso.tipoDocu = this.documentos.get('tipoDocu')?.value;
    this.configurarDocumentoProceso.proceso = value == 'edit' ? 'editar' : 'consultar';

    this.sharingInformationService.compartirConfigurarDocumentoProcesoObservableData = this.configurarDocumentoProceso;
    this.router.navigate(['SARF/mantenimientos/configuracion/configurar_documentos/editar']);
    this.sharingInformationService.irRutaBandejaConfigurarDocumentoObservableData = false;

    // const dataDialog = {
    //   tipo: this.documentos.get('tipoDocu')?.value,
    //   vista: value == 'edit' ? false : true,
    //   doc: this.selectedValues
    // };

    // const dialog = this.dialogService.open( EditarConfiguracionComponent, {
    //   header: value == 'edit' ? 'Editar Configuración' : 'Consultar Configuración',
    //   width: '60%',
    //   closable: false,
    //   data: dataDialog
    // });

    // dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
    //   if ( type == 'accept' ) {
    //     this.messageService.add({ 
    //       severity: 'success', 
    //       summary: 'Su registro se ha actualizado correctamente'
    //     });

    //     this.buscaBandeja();
    //     this.selectedValues = [];
    //   }
    //   else if ( type == 'reject' ) {
    //     this.messageService.add({
    //       severity: 'error',
    //       summary: 'Ha ocurrido un error al actualizar su registro'
    //     });

    //     this.selectedValues = [];
    //   }

    //   this.selectedValues = [];
    // });
  }

  verConcepto( conf: ConfigurarDocumento ) {
    const tipoDocu = this.documentos.get('tipoDocu')?.value;
    let titulo: string;

    if ( tipoDocu == '' || tipoDocu == '1' )
      titulo = 'Recibo de Ingreso';
    else
      titulo = 'Comprobante de Pago';

    this.dialogService.open( ConceptoConfigComponent, {
      header: `${ titulo }: ${ conf.noTipo }`,
      width: '40%',
      data: {
        detalle: conf.obConce
      }
    });
  }

  anular() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Inactivo' ) {
        Swal.fire('No puede anular un registro ya inactivo', '', 'warning');
        return;
      }

      this.registros.push( String( registro.idTipo ) );
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro que desea anular el registro?',
      accept: () => {
        Swal.showLoading();
        this.loadingAnu = true;

        let tipoDocu = this.documentos.get('tipoDocu')?.value;
        tipoDocu = tipoDocu == '' ? '1' : tipoDocu;

        const data = {
          tiDocu: tipoDocu,
          idTipoDocumentos: this.registros
        }

        this.configuracionService.anularDocumentos( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se anularon los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al anular los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAnu = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscaBandeja(false);
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  activar() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Activo' ) {
        Swal.fire('No puede habilitar un registro ya activo', '', 'warning');
        return;
      }

      this.registros.push( String( registro.idTipo ) );
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro que desea activar los registros?',
      accept: () => {
        Swal.showLoading();
        this.loadingAct = true;

        let tipoDocu = this.documentos.get('tipoDocu')?.value;
        tipoDocu = tipoDocu == '' ? '1' : tipoDocu;

        const data = {
          tiDocu: tipoDocu,
          idTipoDocumentos: this.registros
        }

        this.configuracionService.activarDocumentos( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se activaron los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al activar los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAct = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscaBandeja(false);
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  private establecerDataFiltrosConfig() : IDataFiltrosConfiguracion {
    const dataFiltrosConfig: IDataFiltrosConfiguracion = {
      bodyConfig: this.bodyConfigDocu,
      esBusqueda: this.esBusqueda,
      esCancelar: false
    }
    return dataFiltrosConfig;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.sharingInformationService.compartirDataFiltrosConfigurarDocumentoObservableData = this.establecerDataFiltrosConfig();
  }

}
