import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-concepto-config',
  templateUrl: './concepto-config.component.html',
  styleUrls: ['./concepto-config.component.scss']
})
export class ConceptoConfigComponent implements OnInit {

  form!: FormGroup
  value = this.config.data;

  constructor( private fb: FormBuilder,
               private ref: DynamicDialogRef,
               private config: DynamicDialogConfig ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      concepto: ['']
    });

    this.hidrate();
  }

  hidrate() {
    this.form.get('concepto')?.setValue( this.value['detalle'] );
  }

}
