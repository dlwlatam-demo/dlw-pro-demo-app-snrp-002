import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Observable, first, map, of } from 'rxjs';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';

import { Banco, Caja, Documento } from '../../../../../interfaces/combos.interface';
import { IBandejaConfiguracion } from '../../../../../interfaces/global.interface';
import { IClasificador } from '../../../../../interfaces/configuracion/clasificador.interface';
import { IConfigDocumento, IDataFiltrosConfiguracion } from '../../../../../interfaces/configuracion/configurar-documento.interface';

import { 
  ConfigurarDocumento, 
  ListClasificadores, 
  ListCuentaContable 
} from '../../../../../models/mantenimientos/configuracion/config-documento.model';
import { CuentaBancaria } from '../../../../../models/mantenimientos/maestras/cuenta-bancaria.model';
import { PlanCuentaContable } from '../../../../../models/mantenimientos/maestras/plan-cuenta.model';

import { ClasificadorService } from '../../../../../services/mantenimientos/maestras/clasificador.service';
import { ConfigDocumentoService } from '../../../../../services/mantenimientos/configuracion/config-documento.service';
import { CuentaContableService } from '../../../../../services/mantenimientos/maestras/cuenta-contable.service';
import { GeneralService } from '../../../../../services/general.service';
import { UtilService } from '../../../../../services/util.service';

import { SharingInformationService } from '../../../../../core/services/sharing-services.service';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-editar-configuracion',
  templateUrl: './editar-configuracion.component.html',
  styleUrls: ['./editar-configuracion.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class EditarConfiguracionComponent implements OnInit {

  titulo!: string;
  tipoDocu!: string;

  consultar!: boolean;
  isLoading!: boolean;

  loadingBancos: boolean = true;
  loadingCuentas: boolean = true;
  loadingContables: boolean = true;
  loadingClasificador: boolean = true;

  editar!: FormGroup;

  $clasificadores!: Observable<any>;
  $cuentaContable!: Observable<any>;

  listaTipoDocumento!: Documento[];
  listaBancos!: Banco[];
  listaCuentas!: CuentaBancaria[];
  listaCajas!: Caja[];

  listContables: ListCuentaContable[] = [];
  listClasificadores: ListClasificadores[] = [];

  documento!: ConfigurarDocumento;
  docDetalles!: IConfigDocumento;

  configurarDocumentoProcess: IBandejaConfiguracion;

  cuentaItem: any = { nuCuenBanc: '(SELECCIONAR)' };
  clasificadorItem: any = { coClsf: '(NINGUNO)' };
  contableItem: any = { coCuenCont: '(NINGUNO)' };

  constructor( 
    private fb: FormBuilder,
    private router: Router,
    private confirmService: ConfirmationService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private utilService: UtilService,
    private contableService: CuentaContableService,
    private clasificadorService: ClasificadorService,
    private configuracionService: ConfigDocumentoService,
    private sharingInformationService: SharingInformationService
  ) { }

  async ngOnInit() {

    this.editar = this.defConfiguracionForm();

    this.cuentaPres.clear();
    this.cuentaPatr.clear();

    this.configurarDocumentoProcess = await this.sharingInformationService.compartirConfigurarDocumentoObservable.pipe( first() ).toPromise();
    
    this.documento = this.configurarDocumentoProcess.configDocu;
    this.tipoDocu = this.configurarDocumentoProcess.tipoDocu;

    if ( !this.documento ) { 
      this.cancelar();
      return;
    }

    if ( this.tipoDocu == '' ) { this.tipoDocu = '1' }

    if ( this.configurarDocumentoProcess.proceso == 'editar' ) {
      this.titulo = 'Editar';
    }
    else {
      this.titulo = 'Consultar';
      this.consultar = true;
    }

    this._inicializarListas();
    this._buscarTipoDocumento();
    this._buscarBancos();
    this._buscarClasificadores();
    this._buscarCuentaContable();
    this._buscarCaja();

    this.configuracionService.getConfiguracionById( this.tipoDocu, this.documento.idTipo! ).subscribe({
      next: ( data ) => {
        this.hidrate( data as IConfigDocumento );
      },
      error: ( e ) => {
        console.log( e );
      }
    });
  }

  private _inicializarListas() {
    this.listaTipoDocumento = [];
    this.listaBancos = [];
    this.listaCuentas = [];
    this.listaCajas = [];

    this.listaTipoDocumento.push({ coTipoDocu: '*', deTipoDocu: '(SELECCIONAR)' });
    this.listaBancos.push({ nidInstitucion: 0, cdeInst: '(SELECCIONAR)' });
    this.listaCuentas.push({ nuCuenBanc: '(SELECCIONAR)' });
    this.listaCajas.push({ coCaja: '*', deCaja: '(NINGUNO)' });
  }

  defConfiguracionForm() {
    return this.fb.group({
      tipoDocu: [{value: '', disabled: true}, [ Validators.required ]],
      nomTipo: ['', [ Validators.required ]],
      banco: [{value: '', disabled: true}, [ Validators.required ]],
      nroCuent: [{value: '', disabled: true}, [ Validators.required ]],
      concepto: ['', []],
      cuenta: ['', []],
      nroDocu: ['', [ Validators.required ]],
      ope: ['', [ Validators.required ]],
      ruc:['', [ Validators.required ]],
      rxh:['', [ Validators.required ]],
      detallePresupuestal: this.fb.array([
        this.fb.group({
          clasificador: ['', []],
          detClasificador: ['', []],
          id: [0, []]
        })
      ]),
      detallePatrimonial: this.fb.array([
        this.fb.group({
          cuenta: ['', []],
          descripcion: ['', []],
          caja: ['', []],
          id: [0, []]
        })
      ])
    });
  }

  private _buscarTipoDocumento(): void {
    const tipoDocumento = this.generalService.getCbo_TipoDocumento();

    for ( let tiDocu of tipoDocumento ) {
      this.listaTipoDocumento.push( tiDocu )
    }
  }

  private _buscarBancos(): void {
    this.generalService.getCbo_Bancos().subscribe({
      next: ( data: Banco[] ) => {
        this.loadingBancos = false;
        this.editar.get('banco')?.enable();

        this.listaBancos.push( ...data );
      },
      error: ( _err ) => {
        this.loadingBancos = false;
        this.editar.get('banco')?.enable();
      }
    });
  }

  public buscarCuentas( event: any ): void {
    this.editar.get('nroCuent')?.disable();
    this.editar.get('cuenta')?.setValue('');

    this.listaCuentas = [];
    this.listaCuentas.push({ nuCuenBanc: '(SELECCIONAR)' });
    if ( this.editar.get('banco')?.value !== 0 ) {
      this.generalService.getCbo_Cuentas( event.value ).subscribe({
        next: ( data: any[] ) => {
          this.loadingCuentas = false;
          this.editar.get('nroCuent')?.enable();

          this.listaCuentas.push( ...data );

          this.editar.patchValue({'nroCuent': this.documento.nuCuenBanc });
          if ( this.consultar )
            this.editar.get('nroCuent')?.disable();
          this.onChangeDescription({ value: this.documento.nuCuenBanc });
        },
        error: ( _err ) => {
          this.loadingCuentas = false;
          this.editar.get('nroCuent')?.enable();
        }
      });
    } else {
      this.loadingCuentas = false;
      this.editar.get('nroCuent')?.enable();
    }
  }

  private _buscarClasificadores(): void {
    this.$clasificadores = this.clasificadorService.getBandejaClasificadores(null, null, 'A').pipe(
      map( clsf => {
        clsf.unshift( this.clasificadorItem );
        this.loadingClasificador = false;
        return clsf;
      })
    )
  }

  private _buscarCuentaContable(): void {
    this.$cuentaContable = this.contableService.getBandejaCuentaContable('', '', 'A').pipe(
      map( cont => {
        cont.unshift( this.contableItem );
        this.loadingContables = false;
        return cont;
      })
    )
  }

  private _buscarCaja(): void {
    const cajas = this.generalService.getCbo_Caja();

    for ( let c of cajas ) {
      this.listaCajas.push( c );
    }
  }

  hidrate( data: IConfigDocumento ) {
    if ( this.consultar )
      this.editar.disable();
    
    const lstTipoDocu = data.listTipoDocumento;
    const lstClsf = data.listTipoDocumentoClsf;
    console.log('lstClsf', lstClsf.length === 0 );
    const lstCuenCont = data.listTipoDocumentoCont;

    this.editar.get('tipoDocu')?.setValue( this.tipoDocu );
    this.editar.get('nomTipo')?.setValue( lstTipoDocu[0].noTipo );
    this.editar.get('banco')?.setValue( lstTipoDocu[0].idBanc );
    this.editar.get('nroDocu')?.setValue( lstTipoDocu[0].inTipoDocu == '0' ? false : true );
    this.editar.get('ope')?.setValue( lstTipoDocu[0].inNumeOpe == '0' ? false : true );
    this.editar.get('ruc')?.setValue( lstTipoDocu[0].inNumeRuc == '0' ? false : true );
    this.editar.get('rxh')?.setValue( lstTipoDocu[0].inReciHono == '0' ? false : true );

    this.buscarCuentas({ value: lstTipoDocu[0].idBanc });
    
    this.editar.get('concepto')?.setValue( lstTipoDocu[0].obConce );

    if ( lstClsf?.length != 0 ) {
      this.cuentaPres.clear();
    }

    if ( lstCuenCont?.length != 0 ) {
      this.cuentaPatr.clear();
    }

    lstClsf.map( clsf => {
      this.cuentaPres.push(
        this.fb.group({
          clasificador: [{value: clsf.coClsf, disabled: this.consultar}, []],
          detClasificador: [{value: clsf.deClsf, disabled: this.consultar}, []],
          id: [{value: clsf.idTipoClsf, disabled: this.consultar}, []]
        })
      )
    });

    lstCuenCont.map( cont => {
      this.cuentaPatr.push(
        this.fb.group({
          cuenta: [{value: cont.coCuenCont, disabled: this.consultar}, []],
          descripcion: [{value: cont.noCuenCont, disabled: this.consultar}, []],
          caja: [{value: cont.tiDebeHabe, disabled: this.consultar}, []],
          id: [{value: cont.idTipoCont, disabled: this.consultar}, []]
        })
      )
    });
  }

  onChangeDescription( event: any ) {
    for ( let description of this.listaCuentas ) {
      if ( event.value == description.nuCuenBanc ) {
        this.editar.get('cuenta')?.setValue( description.noCuenBanc )
      }
    }
  }

  campoEsValido( campo: string ) {
    return this.editar.controls[campo].errors && 
              this.editar.controls[campo].touched
  }

  get detallePresupuestalArray(): FormArray | null { return  this.editar.get('detallePresupuestal') as FormArray }
  get detallePatrimonialArray(): FormArray | null { return  this.editar.get('detallePatrimonial') as FormArray }

  get cuentasPresupuestales(): FormGroup[] {
    return <FormGroup[]>this.detallePresupuestalArray?.controls
  }
  get cuentasPatrimoniales(): FormGroup[] {
    return <FormGroup[]>this.detallePatrimonialArray?.controls
  }

  get cuentaPres(): FormArray { return <FormArray>this.editar.get('detallePresupuestal') }
  get cuentaPatr(): FormArray { return <FormArray>this.editar.get('detallePatrimonial')}

  addcuentaPresupuestal() {
    const ar = ( this.editar.get('detallePresupuestal') as FormArray )
    if ( environment.cantidadCuentas > ar.length )
      ar.push(
        this.fb.group({
          clasificador: ['', []],
          detClasificador: ['', []],
          id: [0, []]
        })
      )
  }

  deletecuentaPresupuestal( index: number ) {
    ( this.editar.get('detallePresupuestal') as FormArray ).removeAt(index)
  }

  addcuentaPatrimonial() {
    const ar = ( this.editar.get('detallePatrimonial') as FormArray )
    if ( environment.cantidadCuentas > ar.length )
      ar.push(
        this.fb.group({
          cuenta: ['', []],
          descripcion: ['', []],
          caja: ['', []],
          id: [0, []]
        })
      )
  }

  deletecuentaPatrimonial( index: number ) {
    ( this.editar.get('detallePatrimonial') as FormArray ).removeAt(index)
  }

  public async update() {

    if ( this.editar.invalid ) {
      this.editar.markAllAsTouched();
      return;
    }

    const dataFiltrosConfig: IDataFiltrosConfiguracion = await this.sharingInformationService.compartirDataFiltrosConfiguracionDocumentoObservable.pipe( first() ).toPromise();
    if ( dataFiltrosConfig.esBusqueda ) {
      dataFiltrosConfig.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosConfigurarDocumentoObservableData = dataFiltrosConfig
    }

    this.listClasificadores = [];
    this.listContables = [];
    
    for ( let clsf of this.editar.get('detallePresupuestal')?.value ) {
      if ( clsf.clasificador !== '' && clsf.detClasificador !== '' ) {
        this.listClasificadores.push({
          coClsf: clsf.clasificador,
          idTipoDocuClsf: clsf.id
        });
      }
    }

    for ( let cta of this.editar.get('detallePatrimonial')?.value ) {
      if ( cta.cuenta !== '' && cta.descripcion !== '' ) {
        this.listContables.push({
          coCuenCont: cta.cuenta,
          tiDebeHabe: cta.caja,
          idTipoDocuCont: cta.id
        });
      }
    }

    const data = {
      idTipo: this.documento.idTipo,
      tiDocu: this.tipoDocu,
      noTipo: this.editar.get('nomTipo')?.value,
      idBanc: this.editar.get('banco')?.value,
      nuCuenBanc: this.editar.get('nroCuent')?.value,
      obConce: this.editar.get('concepto')?.value,
      inTipoDocu: this.editar.get('nroDocu')?.value == true ? '1' : '0',
      inNumeOpe: this.editar.get('ope')?.value == true ? '1' : '0',
      inReciHono: this.editar.get('rxh')?.value == true ? '1' : '0',
      inNumeRuc: this.editar.get('ruc')?.value == true ? '1' : '0',
      listaCuentacontable: this.listContables,
      listaClasificadores: this.listClasificadores
    }

    console.log('dataEdit', data );
    
    this.confirmService.confirm({
      message: '¿Está Ud. seguro de actualizar el registro?',
      accept: () => {
        this.utilService.onShowProcessSaveEdit();
        this.isLoading = true;

        this.configuracionService.createOrUpdateDocumento( this.documento.idTipo!, data ).subscribe({
          next: ( d ) => {

            if ( d.codResult < 0 ) {
              Swal.close();
              this.utilService.onShowAlert("info", "Atención", d.msgResult);
              return;
            }

            this.utilService.onCloseLoading();
            this._respuestaExitosa();
          },
          error: ( e ) => {

            if (e.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", e.error.description);        
            } else {
              this.utilService.onShowMessageErrorSystem();        
            }
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  onChangePresupuestales(event: any, index: any) {
    this.$clasificadores.forEach( clasificador => {
      for ( let clsf of clasificador ) {
        if ( event.value == clsf.coClsf ) {
          this.cuentasPresupuestales[index].get('detClasificador').setValue( clsf.deClsf );
          // this.editar.get('cuenta')?.setValue( c.noCuenBanc )
        }
      }
    });
  }

  onChangePatrimoniales(event: any, index: any) {
    this.$cuentaContable.forEach( contables => {
      for ( let cont of contables ) {
        if ( event.value == cont.coCuenCont ) {
          this.cuentasPatrimoniales[index].get('descripcion').setValue( cont.noCuenCont );  
        }
      }
    });
  }

  private async _respuestaExitosa() {
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Su registro se grabó satisfactoriamente.`);
    
    if ( respuesta.isConfirmed ) {
      this.router.navigate(['SARF/mantenimientos/configuracion/configurar_documentos/bandeja']);
    }
  }

  public async cancelar() {
    const dataFiltrosConfig: IDataFiltrosConfiguracion = await this.sharingInformationService.compartirDataFiltrosConfiguracionDocumentoObservable.pipe( first() ).toPromise();
    if ( dataFiltrosConfig.esBusqueda ) {
      dataFiltrosConfig.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosConfigurarDocumentoObservableData = dataFiltrosConfig
    }
    this.router.navigate(['SARF/mantenimientos/configuracion/configurar_documentos/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaConfigurarDocumentoObservableData = true;
  }

}
