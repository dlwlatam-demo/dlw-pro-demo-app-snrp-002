import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { SortEvent, MessageService, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';

import { Estado } from '../../../../../interfaces/combos.interface';

import { MenuOpciones } from '../../../../../models/auth/Menu.model';
import { TipoMemorandum } from '../../../../../models/mantenimientos/configuracion/tipo-memo.model';

import { GeneralService } from '../../../../../services/general.service';
import { TipoMemorandumService } from '../../../../../services/mantenimientos/configuracion/tipo-memorandum.service';

import { NuevoTipoMemoComponent } from '../nuevo-tipo-memo/nuevo-tipo-memo.component';
import { EditarTipoMemoComponent } from '../editar-tipo-memo/editar-tipo-memo.component';

import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-bandeja-tipo-memo',
  templateUrl: './bandeja-tipo-memo.component.html',
  styleUrls: ['./bandeja-tipo-memo.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class BandejaTipoMemoComponent extends BaseBandeja implements OnInit {

  disabledEdit!: boolean;
  registros: string[] = [];

  tipoMemo!: FormGroup;

  $estado!: Estado[];
  enviadosList!: TipoMemorandum[];

  currentPage: string = environment.currentPage;

  constructor( 
    private fb: FormBuilder,
    private confirmService: ConfirmationService,
    private dialogService: DialogService,
    private messageService: MessageService,
    private generalService: GeneralService,
    private tipoMemoService: TipoMemorandumService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Tipo_de_Memorandum;
    this.btnTipoMemorandum();

    let estadoItem: Estado = { deEstado: '(TODOS)', coEstado: '*' };

    this.$estado = this.generalService.getCbo_Estado();
    this.$estado.unshift( estadoItem );

    this.tipoMemo = this.fb.group({
      descripcion: ['', []],
      estado: ['A', [ Validators.required ]]
    });

    this.buscarTipo();
  }

  buscarTipo() {
    const descripcion = this.tipoMemo.get('descripcion')?.value;

    let estado = this.tipoMemo.get('estado')?.value;
    if ( estado == '' ) {
      estado = 'A';
    } else if ( estado == '*' ) {
      estado = ''
    }

    this.buscarBandeja( descripcion, estado );
  }

  buscarBandeja( des: string, est: string ) {
    Swal.showLoading();
    this.tipoMemoService.getBandejaTipoMemorandum( des, est ).subscribe({
      next: ( data ) => {
        this.disabledEdit = est == 'I' ? true : false;

        for ( let d of data ) {
          if ( d.inRegi == 'A' )
            d.inRegi = 'Activo';
          else
           d.inRegi = 'Inactivo';
        }

        this.enviadosList = data;
      },
      error: () => {
        Swal.close()
        Swal.fire(
          'No se encuentran registros con los datos ingresados',
          'Tipo de Memorándum',
          'info'
        );

        this.enviadosList = [];
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  nuevoTipo() {
    document.getElementById('btnNuevo').blur();

    const dialog = this.dialogService.open( NuevoTipoMemoComponent, {
      header: 'Registrar Nuevo Tipo Memorandum',
      width: '60%',
      closable: false
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha creado correctamente'
        });

        this.buscarTipo();
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un problema al crear su registro'
        });
      }
    });
  }

  editOrView( value: string ) {
    document.getElementById('btnEditar').blur();

    if ( !this.validateSelect( value ) )
      return;

    const dataDialog = {
      tipo: this.selectedValues,
      vista: value == 'edit' ? false : true
    };

    const dialog = this.dialogService.open( EditarTipoMemoComponent, {
      header: value == 'edit' ? 'Editar Tipo Memorandum' : 'Consultar Tipo Memorandum',
      width: '60%',
      closable: false,
      data: dataDialog
    });

    dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
      if ( type == 'accept' ) {
        this.messageService.add({ 
          severity: 'success', 
          summary: 'Su registro se ha actualizado correctamente'
        });

        this.buscarTipo();
        this.selectedValues = [];
      }
      else if ( type == 'reject' ) {
        this.messageService.add({
          severity: 'error',
          summary: 'Ha ocurrido un error al actualizar su registro'
        });

        this.selectedValues = [];
      }

      this.selectedValues = [];
    });
  }

  anular() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Inactivo' ) {
        Swal.fire('No puede anular un registro ya inactivo', '', 'warning');
        return;
      }

      this.registros.push( String( registro.idTipoMemo ) );
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea anular ${ msg1 } ${ msg2 }?`,
      accept: () => {
        Swal.showLoading();
        this.loadingAnu = true;

        const data = {
          idTipoMemorandums: this.registros
        }

        this.tipoMemoService.anularTipoMemorandum( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se anularon los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al anular los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAnu = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscarTipo();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  activar() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Activo' ) {
        Swal.fire('No puede habilitar un registro ya activo', '', 'warning');
        return;
      }

      this.registros.push( String( registro.idTipoMemo ) );
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea activar ${ msg1 } ${ msg2 }?`,
      accept: () => {
        Swal.showLoading();
        this.loadingAct = true;

        const data = {
          idTipoMemorandums: this.registros
        }

        this.tipoMemoService.activarTipoMemorandum( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se activaron los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al activar los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAct = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscarTipo();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

}
