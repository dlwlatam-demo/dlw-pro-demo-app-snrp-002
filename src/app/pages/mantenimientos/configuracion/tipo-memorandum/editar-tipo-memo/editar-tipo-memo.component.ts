import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, map } from 'rxjs';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';

import { TipoMemorandum } from '../../../../../models/mantenimientos/configuracion/tipo-memo.model';

import { GeneralService } from '../../../../../services/general.service';
import { TipoMemorandumService } from '../../../../../services/mantenimientos/configuracion/tipo-memorandum.service';

@Component({
  selector: 'app-editar-tipo-memo',
  templateUrl: './editar-tipo-memo.component.html',
  styleUrls: ['./editar-tipo-memo.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class EditarTipoMemoComponent implements OnInit {

  consultar!: boolean;
  isLoading!: boolean;
  loadingBancos: boolean = true;
  loadingCuenta1: boolean = true;
  loadingCuenta2: boolean = true;

  editar!: FormGroup;
  tipoMemo!: TipoMemorandum[];

  $bancos!: Observable< any >;
  $cuenta1!: Observable< any >;
  $cuenta2!: Observable< any >;

  cuentaItem: any = { nuCuenBanc: '(NINGUNO)' };

  constructor( private fb: FormBuilder,
               private ref: DynamicDialogRef,
               private config: DynamicDialogConfig,
               private messageService: MessageService,
               private confirmService: ConfirmationService,
               private generalService: GeneralService,
               private tipoMemoService: TipoMemorandumService ) { }

  ngOnInit(): void {

    this.editar = this.defTipoMemoForm();

    this.tipoMemo = this.config.data.tipo as TipoMemorandum[];
    this.consultar = this.config.data.vista;

    const bancoItem: any = { nidInstitucion: 0, cdeInst: '(NINGUNO)' };

    this.$bancos = this.generalService.getCbo_Bancos().pipe(
      map( b => {
        b.unshift( bancoItem );
        this.loadingBancos = false;

        if ( !this.consultar ) {
          this.editar.get('banco1')?.enable();
          this.editar.get('banco2')?.enable();
        }

        return b;
      })
    );

    this.tipoMemoService.getTipoMemorandumById( this.tipoMemo['0'].idTipoMemo! ).subscribe({
      next: ( data ) => {
        this.hidrate( data as TipoMemorandum );
      },
      error: ( e ) => {
        console.log( e );
      }
    });

  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent): void {
    if ( event.key === 'Enter' ) {
      this.update();
    } else if ( event.key === 'Escape' ) {
      this.cancelar();
    }
  }

  defTipoMemoForm() {
    return this.fb.group({
      descripcion: ['', [ Validators.required ]],
      para: ['', [ Validators.required ]],
      cargo: ['', []],
      asunto: ['', []],
      referencia: ['', []],
      banco1: [{value: '', disabled: true}, []],
      nroCuenta1: ['', []],
      banco2: [{value: '', disabled: true}, []],
      nroCuenta2: ['', []],
      cuenta1: ['', []],
      cuenta2: ['', []]
    });
  }

  hidrate( d: TipoMemorandum ) {
    if ( this.consultar )
      this.editar.disable();

    this.editar.get('descripcion')?.setValue( d.noTipoMemo );
    this.editar.get('para')?.setValue( d.noDest );
    this.editar.get('cargo')?.setValue( d.noCarg );
    this.editar.get('asunto')?.setValue( d.noAsun );
    this.editar.get('referencia')?.setValue( d.noRefe );
    this.editar.get('banco1')?.setValue( d.idBancOrig );
    this.editar.get('nroCuenta1')?.setValue( d.nuCuenBancOrig );
    this.editar.get('banco2')?.setValue( d.idBancDest );
    this.editar.get('nroCuenta2')?.setValue( d.nuCuenBancDest );

    this.$cuenta1 = this.generalService.getCbo_Cuentas( d.idBancOrig! ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta1 = false;

        if ( !this.consultar )
          this.editar.get('nroCuenta1')?.enable();
        
        return c;
      })
    );

    this.$cuenta1.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( d.nuCuenBancOrig == c.nuCuenBanc ) {
          this.editar.get('cuenta1')?.setValue( c.noCuenBanc )
        }
      }
    })

    this.$cuenta2 = this.generalService.getCbo_Cuentas( d.idBancDest! ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta2 = false;

        if ( !this.consultar )
          this.editar.get('nroCuenta2')?.enable();
        
        return c;
      })
    );

    this.$cuenta2.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( d.nuCuenBancDest == c.nuCuenBanc ) {
          this.editar.get('cuenta2')?.setValue( c.noCuenBanc )
        }
      }
    })
  }

  onChangeCuenta1( event: any ) {

    this.editar.get('nroCuenta1')?.disable();
    this.editar.get('cuenta1')?.setValue('');
    this.loadingCuenta1 = true;

    this.$cuenta1 = this.generalService.getCbo_Cuentas( event.value ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta1 = false;
        this.editar.get('nroCuenta1')?.enable();
        return c;
      })
    );
  }

  onChangeDescription1( event: any ) {

    this.$cuenta1.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( event.value == c.nuCuenBanc ) {
          this.editar.get('cuenta1')?.setValue( c.noCuenBanc )
        }
      }
    })
  }

  onChangeCuenta2( event: any ) {

    this.editar.get('nroCuenta2')?.disable();
    this.editar.get('cuenta2')?.setValue('');
    this.loadingCuenta2 = true;

    this.$cuenta2 = this.generalService.getCbo_Cuentas( event.value ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta2 = false;
        this.editar.get('nroCuenta2')?.enable();
        return c;
      })
    );
  }

  onChangeDescription2( event: any ) {

    this.$cuenta2.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( event.value == c.nuCuenBanc ) {
          this.editar.get('cuenta2')?.setValue( c.noCuenBanc )
        }
      }
    })
  }

  campoEsValido( campo: string ) {
    return this.editar.controls[campo].errors && 
              this.editar.controls[campo].touched
  }

  update() {

    if ( this.editar.invalid ) {
      this.editar.markAllAsTouched();
      return;
    }

    const data = {
      idTipoMemo: this.tipoMemo['0'].idTipoMemo!,
      noTipoMemo: this.editar.get('descripcion')?.value,
      noDest: this.editar.get('para')?.value,
      noCarg: this.editar.get('cargo')?.value,
      noAsun: this.editar.get('asunto')?.value,
      noRefe: this.editar.get('referencia')?.value,
      idBancOrig: this.editar.get('banco1')?.value,
      nuCuenBancOrig: this.editar.get('nroCuenta1')?.value == '(NINGUNO)' ? '' : this.editar.get('nroCuenta1')?.value,
      idBancDest: this.editar.get('banco2')?.value,
      nuCuenBancDest: this.editar.get('nroCuenta2')?.value == '(NINGUNO)' ? '' : this.editar.get('nroCuenta2')?.value
    }
    
    this.confirmService.confirm({
      message: '¿Está Ud. seguro de actualizar el registro?',
      acceptButtonStyleClass: 'p-button-buscar p-button-sm ml-2',
      rejectButtonStyleClass: 'p-button-anular p-button-sm',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        this.tipoMemoService.createOrUpdate( this.tipoMemo['0'].idTipoMemo!, data ).subscribe({
          next: ( d ) => {

            if ( d.codResult < 0 ) {
              Swal.close();
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención!', 
                detail: d.msgResult,
                sticky: true
              });

              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            Swal.close();
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
