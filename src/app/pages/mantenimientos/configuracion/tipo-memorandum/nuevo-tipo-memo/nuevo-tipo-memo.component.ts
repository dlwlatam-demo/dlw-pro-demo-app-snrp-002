import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, map, of } from 'rxjs';

import Swal from 'sweetalert2';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/dynamicdialog';

import { GeneralService } from '../../../../../services/general.service';
import { TipoMemorandumService } from '../../../../../services/mantenimientos/configuracion/tipo-memorandum.service';

@Component({
  selector: 'app-nuevo-tipo-memo',
  templateUrl: './nuevo-tipo-memo.component.html',
  styleUrls: ['./nuevo-tipo-memo.component.scss'],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class NuevoTipoMemoComponent implements OnInit {

  isLoading!: boolean;
  loadingBancos: boolean = true;
  loadingCuenta1: boolean = false;
  loadingCuenta2: boolean = false;

  nuevo!: FormGroup;

  $bancos!: Observable< any >;
  $cuenta1: Observable< any > = of([{ nuCuenBanc: '(NINGUNO)' }]);
  $cuenta2: Observable< any > = of([{ nuCuenBanc: '(NINGUNO)' }]);

  cuentaItem: any = { nuCuenBanc: '(NINGUNO)' };

  constructor( private fb: FormBuilder,
               private ref: DynamicDialogRef,
               private messageService: MessageService,
               private confirmService: ConfirmationService,
               private generalService: GeneralService,
               private tipoMemoService: TipoMemorandumService ) { }

  ngOnInit(): void {

    this.nuevo = this.fb.group({
      descripcion: ['', [ Validators.required ]],
      para: ['', [ Validators.required ]],
      cargo: ['', []],
      asunto: ['', []],
      referencia: ['', []],
      banco1: [{value: '', disabled: true}, []],
      nroCuenta1: ['', []],
      banco2: [{value: '', disabled: true}, []],
      nroCuenta2: ['', []],
      cuenta1: ['', []],
      cuenta2: ['', []]
    });

    const bancoItem: any = { nidInstitucion: 0, cdeInst: '(NINGUNO)' };

    this.$bancos = this.generalService.getCbo_Bancos().pipe(
      map( b => {
        b.unshift( bancoItem );
        this.loadingBancos = false;
        this.nuevo.get('banco1')?.enable();
        this.nuevo.get('banco2')?.enable();
        return b;
      })
    );
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent): void {
    if ( event.key === 'Enter' ) {
      this.grabar();
    } else if ( event.key === 'Escape' ) {
      this.cancelar();
    }
  }

  onChangeCuenta1( event: any ) {

    this.nuevo.get('nroCuenta1')?.disable();
    this.nuevo.get('cuenta1')?.setValue('');
    this.loadingCuenta1 = true;
    
    this.$cuenta1 = this.generalService.getCbo_Cuentas( event.value ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta1 = false;
        this.nuevo.get('nroCuenta1')?.enable();
        return c;
      })
    );
  }

  onChangeDescription1( event: any ) {

    this.$cuenta1.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( event.value == c.nuCuenBanc ) {
          this.nuevo.get('cuenta1')?.setValue( c.noCuenBanc )
        }
      }
    })
  }

  onChangeCuenta2( event: any ) {

    this.nuevo.get('nroCuenta2')?.disable();
    this.nuevo.get('cuenta2')?.setValue('');
    this.loadingCuenta2 = true;

    this.$cuenta2 = this.generalService.getCbo_Cuentas( event.value ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta2 = false;
        this.nuevo.get('nroCuenta2')?.enable();
        return c;
      })
    );
  }

  onChangeDescription2( event: any ) {

    this.$cuenta2.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( event.value == c.nuCuenBanc ) {
          this.nuevo.get('cuenta2')?.setValue( c.noCuenBanc )
        }
      }
    })
  }

  campoEsValido( campo: string ) {
    return this.nuevo.controls[campo].errors && 
              this.nuevo.controls[campo].touched
  }

  grabar() {

    if ( this.nuevo.invalid ) {
      this.nuevo.markAllAsTouched();
      return;
    }

    const data = {
      noTipoMemo: this.nuevo.get('descripcion')?.value,
      noDest: this.nuevo.get('para')?.value,
      noCarg: this.nuevo.get('cargo')?.value,
      noAsun: this.nuevo.get('asunto')?.value,
      noRefe: this.nuevo.get('referencia')?.value,
      idBancOrig: this.nuevo.get('banco1')?.value,
      nuCuenBancOrig: this.nuevo.get('nroCuenta1')?.value == '(NINGUNO)' ? '' : this.nuevo.get('nroCuenta1')?.value,
      idBancDest: this.nuevo.get('banco2')?.value,
      nuCuenBancDest: this.nuevo.get('nroCuenta2')?.value == '(NINGUNO)' ? '' : this.nuevo.get('nroCuenta2')?.value
    }

    this.confirmService.confirm({
      message: '¿Está Ud. seguro de crear el siguiente registro?',
      acceptButtonStyleClass: 'p-button-buscar p-button-sm ml-2',
      rejectButtonStyleClass: 'p-button-anular p-button-sm',
      accept: () => {
        Swal.showLoading();
        this.isLoading = true;

        this.tipoMemoService.createOrUpdate( 0, data ).subscribe({
          next: ( d ) => {

            if ( d.codResult < 0 ) {
              Swal.close();
              this.messageService.add({
                severity: 'warn', 
                summary: 'Atención!', 
                detail: d.msgResult,
                sticky: true
              });

              return;
            }

            this.ref.close('accept');
          },
          error: ( e ) => {
            console.log( e );

            Swal.close();
            this.ref.close('reject');
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  cancelar() {
    this.ref.close('');
  }

}
