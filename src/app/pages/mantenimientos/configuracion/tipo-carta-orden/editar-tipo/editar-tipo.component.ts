import { Component, OnInit, HostListener, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, first, map, of, takeUntil } from 'rxjs';

import Quill from 'quill';

import { IResponse2 } from '../../../../../interfaces/general.interface';
import { ITipoCartaVariables, IDataFiltrosTipoCartaOrden, ITipoCartaById } from '../../../../../interfaces/configuracion/tipo-carta-orden.interface';

import { BodyTipoCartaOrden } from '../../../../../models/mantenimientos/configuracion/tipo-carta-orden.model';

import { Funciones } from '../../../../../core/helpers/funciones/funciones';

import { TipoCartaOrdenService } from '../../../../../services/mantenimientos/configuracion/tipo-carta-orden.service';
import { GeneralService } from '../../../../../services/general.service';
import { UtilService } from '../../../../../services/util.service';
import { SharingInformationService } from '../../../../../core/services/sharing-services.service';

@Component({
  selector: 'app-editar-tipo',
  templateUrl: './editar-tipo.component.html',
  styleUrls: ['./editar-tipo.component.scss']
})
export class EditarTipoComponent implements OnInit, OnDestroy {

  @ViewChild('deTipo') descripcion: ElementRef;
  @ViewChild('deDocumento') documento: ElementRef;

  consultar!: boolean;
  isLoading!: boolean;
  titulo!: string;
  loadingBancos: boolean = true;
  loadingCuenta1: boolean = true;
  loadingCuenta2: boolean = true;
  loadingGrupos: boolean = false;
  readOnlyEditor: boolean = false;
  editorInicio: string = '';
  editorConcepto: string = '';
  editorFinal: string = '';
  
  editar!: FormGroup;
  
  listaGrupos: IResponse2[] = [];
  
  insertarVariable!: ITipoCartaVariables;
  dataTipoCarta: ITipoCartaById;
  listaVariables: ITipoCartaVariables[] = [];

  $bancos!: Observable< any >;
  $cuentas1!: Observable< any >;
  $cuentas2!: Observable< any >;
  $grupos: Observable< any > = of([{ grupo: '(SELECCIONAR)' }]);

  cuentaItem: any = { nuCuenBanc: '(NINGUNO)' };

  bodyTipoCarta!: BodyTipoCartaOrden;

  quillInicio!: Quill;
  quillConcepto!: Quill;
  quillFinal!: Quill;

  unsubscribe$ = new Subject<void>();

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private funciones: Funciones,
    private activatedRoute: ActivatedRoute,
    private utilService: UtilService,
    private generalService: GeneralService,
    private tipoCartaService: TipoCartaOrdenService,
    private sharingInformationService: SharingInformationService
  ) { }

  async ngOnInit() {
    this.bodyTipoCarta = new BodyTipoCartaOrden();
    
    this.editar = this.defTipoCartaForm();

    const tipoCartaProceso = await this.sharingInformationService.compartirTipoCartaOrdenProcesoObservable.pipe( first() ).toPromise();
    if ( tipoCartaProceso.proceso === 'editar' ) {
      this.titulo = 'Editar';
      this.consultar = false;
    }
    else if ( tipoCartaProceso.proceso === 'consultar') {
      this.titulo = 'Consultar';
      this.consultar = true;
    }
    else { this.cancelar(); }

    this._buscarBancos();
    this._buscarGruposCarta();
    this._getIdByRoute();
  }

  private _getIdByRoute(): void {
    this.activatedRoute.params.pipe( takeUntil( this.unsubscribe$ ) )
      .subscribe( params => this._getComprobantePagoById( params['id'] ) );
  }

  private _getComprobantePagoById( id: number ): void {
    this.utilService.onShowProcessLoading('Cargando información');
    this.tipoCartaService.getTipoCartaById( id ).pipe( takeUntil( this.unsubscribe$ ) ).subscribe({
      next: ( data ) => {
        console.log('dataTipo', data);
        this.listaVariables = data.liVariDocu;
        this.hidrate( data as ITipoCartaById );
        this.dataTipoCarta = data;

        this.utilService.onCloseLoading();
      },
      error: ( e ) => {
        console.log( e );
        this.utilService.onCloseLoading();
      }
    });

  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent): void {
    if ( event.key === 'Enter' ) {
      this.update();
    } else if ( event.key === 'Escape' ) {
      this.cancelar();
    }
  }

  defTipoCartaForm() {
    return this.fb.group({
      nomDocu: ['', [ Validators.required ]],
      descripcion: ['', [ Validators.required ]],
      grupo: ['', []],
      anio: ['', []],
      decenio: ['', []],
      titulo: ['', []],
      sigAdm: ['', []],
      destinatario: ['', []],
      cargo: ['', []],
      bancCarg: [{value: '', disabled: true}, []],
      nroCuenCarg: [{value: '', disabled: true}, []],
      bancAbon: [{value: '', disabled: true}, []],
      nroCuenAbon: [{value: '', disabled: true}, []],
      inicio: ['', []],
      concepto: ['', []],
      final: ['', []],
      cuenta1: ['', []],
      cuenta2: ['', []]
    });
  }

  private _buscarBancos() {
    const bancoItem: any = { nidInstitucion: 0, cdeInst: '(NINGUNO)' };

    this.$bancos = this.generalService.getCbo_Bancos().pipe(
      map( b => {
        b.unshift( bancoItem );
        this.loadingBancos = false;

        if ( !this.consultar ) {
          this.editar.get('bancCarg')?.enable();
          this.editar.get('bancAbon')?.enable();
        }

        return b;
      })
    );
  }

  private _buscarGruposCarta(): void {
    this.loadingGrupos = true;
    this.listaGrupos.push({ ccodigoHijo: '*', cdescri: '(NINGUNO)' });

    this.generalService.getCbo_GruposCarta().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data ) => {
        this.loadingGrupos = false;
        if (data.length === 1) {
          this.listaGrupos.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });
          this.editar.patchValue({ "grupo": data[0].ccodigoHijo });
        } else {
          this.listaGrupos.push(...data);
        }
      },
      error:( err )=>{
        this.loadingGrupos = false;
      }
    });
  }

  hidrate( d: ITipoCartaById ) {
    if ( this.consultar ) {
      this.editar.disable();
      this.readOnlyEditor = true;
    }

    this.editar.get('nomDocu')?.setValue( d.noDocu );
    this.editar.get('descripcion')?.setValue( d.noTipoCartOrde );
    this.editar.get('grupo')?.setValue( d.coGrup );
    this.editar.get('anio')?.setValue( d.noAnio );
    this.editar.get('decenio')?.setValue( d.noDece );
    this.editar.get('titulo')?.setValue( d.deTitu );
    this.editar.get('sigAdm')?.setValue( d.deSiglOfic );
    this.editar.get('destinatario')?.setValue( d.noDest );
    this.editar.get('cargo')?.setValue( d.noCarg );
    this.editar.get('bancCarg')?.setValue( d.idBancCarg );
    this.editar.get('nroCuenCarg')?.setValue( d.nuCuenBancCarg );
    this.editar.get('bancAbon')?.setValue( d.idBancAbon );
    this.editar.get('nroCuenAbon')?.setValue( d.nuCuenBancAbon );
    this.editar.get('inicio')?.setValue( d.deSeccInic );
    this.editar.get('concepto')?.setValue( d.deSeccConc );
    this.editar.get('final')?.setValue( d.deSeccFina );

    this.$cuentas1 = this.generalService.getCbo_Cuentas( d.idBancCarg! ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta1 = false;

        if ( !this.consultar )
          this.editar.get('nroCuenCarg')?.enable();

        return c;
      })
    );

    this.$cuentas1.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( d.nuCuenBancCarg == c.nuCuenBanc ) {
          this.editar.get('cuenta1')?.setValue( c.noCuenBanc )
        }
      }
    })

    this.$cuentas2 = this.generalService.getCbo_Cuentas( d.idBancAbon! ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta2 = false;

        if ( !this.consultar )
          this.editar.get('nroCuenAbon')?.enable();

        return c;
      })
    );

    this.$cuentas2.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( d.nuCuenBancAbon == c.nuCuenBanc ) {
          this.editar.get('cuenta2')?.setValue( c.noCuenBanc )
        }
      }
    })
  }

  onChangeCuenta1( event: any ) {
    this.editar.get('nroCuenCarg')?.disable();
    this.editar.get('cuenta1')?.setValue('');
    this.loadingCuenta1 = true;

    this.$cuentas1 = this.generalService.getCbo_Cuentas( event.value ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta1 = false;
        this.editar.get('nroCuenCarg')?.enable();
        return c;
      })
    );
  }

  onChangeDescription1( event: any ) {
    this.$cuentas1.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( event.value == c.nuCuenBanc ) {
          this.editar.get('cuenta1')?.setValue( c.noCuenBanc )
        }
      }
    })
  }

  onChangeCuenta2( event: any ) {
    this.editar.get('nroCuenAbon')?.disable();
    this.editar.get('cuenta2')?.setValue('');
    this.loadingCuenta2 = true;

    this.$cuentas2 = this.generalService.getCbo_Cuentas( event.value ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta2 = false;
        this.editar.get('nroCuenAbon')?.enable();
        return c;
      })
    );
  }

  onChangeDescription2( event: any ) {
    this.$cuentas2.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( event.value == c.nuCuenBanc ) {
          this.editar.get('cuenta2')?.setValue( c.noCuenBanc )
        }
      }
    })
  }

  campoEsValido( campo: string ) {
    return this.editar.controls[campo].errors &&
              this.editar.controls[campo].touched
  }

  public initEditorInicio( event: any ) {
    this.quillInicio = event.editor;
  }

  public selectionChangeInicio( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillInicio.getSelection().index;
      
      this.quillInicio.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillInicio.setSelection(cursorPosition + value, 'user');
      this.editar.get('inicio')?.setValue( this.quillInicio.root.innerHTML );
      this.insertarVariable = null;
    }
  }

  public initEditorConcepto( event: any ) {
    this.quillConcepto = event.editor;
  }

  public selectionChangeConcepto( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillConcepto.getSelection().index;
      
      this.quillConcepto.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillConcepto.setSelection(cursorPosition + value, 'user');
      this.editar.get('concepto')?.setValue( this.quillConcepto.root.innerHTML );
      this.insertarVariable = null;
    }
  }

  public initEditorFinal( event: any ) {
    this.quillFinal = event.editor;
  }

  public selectionChangeFinal( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillFinal.getSelection().index;
      
      this.quillFinal.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillFinal.setSelection(cursorPosition + value, 'user');
      this.editar.get('final')?.setValue( this.quillFinal.root.innerHTML );
      this.insertarVariable = null;
    }
  }

  update() {

    const boldInit = /<strong>/g;
    const boldFin = /<\/strong>/g;
    const italicInit = /<em>/g;
    const italicFin = /<\/em>/g;

    if ( !this._validarFormulario() ) {
      this.editar.markAllAsTouched();
      return;
    }

    let grupo = this.editar.get('grupo')?.value;
    grupo = grupo == '*' ? '' : grupo;

    let bancCarg = this.editar.get('bancCarg')?.value;
    bancCarg = bancCarg == '*' ? '' : bancCarg;

    let bancAbon = this.editar.get('bancAbon')?.value;
    bancAbon = bancAbon == '*' ? '' : bancAbon;

    let cuenBancCarg = this.editar.get('nroCuenCarg')?.value;
    cuenBancCarg = cuenBancCarg == '(NINGUNO)' ? '' : cuenBancCarg;

    let cuenBancAbon = this.editar.get('nroCuenAbon')?.value;
    cuenBancAbon = cuenBancAbon == '(NINGUNO)' ? '' : cuenBancAbon;

    const seccionInicio: string = this.editar.get('inicio')?.value;
    const seccionConcep: string = this.editar.get('concepto')?.value;
    const seccionFinal: string = this.editar.get('final')?.value;

    this.bodyTipoCarta.idTipoCartOrde = this.dataTipoCarta.idTipoCartOrde;
    this.bodyTipoCarta.noDocu = this.editar.get('nomDocu')?.value;
    this.bodyTipoCarta.noTipoCartOrde = this.editar.get('descripcion')?.value;
    this.bodyTipoCarta.coGrup = grupo;
    this.bodyTipoCarta.noAnio = this.editar.get('anio')?.value;
    this.bodyTipoCarta.noDece = this.editar.get('decenio')?.value;
    this.bodyTipoCarta.deTitu = this.editar.get('titulo')?.value;
    this.bodyTipoCarta.deSiglOfic = this.editar.get('sigAdm')?.value;
    this.bodyTipoCarta.noDest = this.editar.get('destinatario')?.value;
    this.bodyTipoCarta.noCarg = this.editar.get('cargo')?.value;
    this.bodyTipoCarta.idBancCarg = bancCarg;
    this.bodyTipoCarta.nuCuenBancCarg = cuenBancCarg;
    this.bodyTipoCarta.idBancAbon = bancAbon;
    this.bodyTipoCarta.nuCuenBancAbon = cuenBancAbon;
    this.bodyTipoCarta.deSeccInic = seccionInicio.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>');
    this.bodyTipoCarta.deSeccConc = seccionConcep.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>');
    this.bodyTipoCarta.deSeccFina = seccionFinal.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>');

    console.log('bodyTipoCarta', this.bodyTipoCarta);

    this.utilService.onShowConfirm(`¿Está Ud. seguro de actualizar el registro?`).then( (result) => {
      if ( result.isConfirmed ) {
        this.utilService.onShowProcessLoading('Guardando la información');
        this.isLoading = true;

        this.tipoCartaService.createOrUpdateCartaOrden( 1, this.bodyTipoCarta ).subscribe({
          next: ( d ) => {
            console.log( d );

            if ( d.codResult < 0 ) {
              this.utilService.onCloseLoading();
              this.utilService.onShowAlert("info", "Atención", d.msgResult);

              return;
            }

            this.utilService.onCloseLoading();
            this._respuestaExitosa();
          },
          error: ( e ) => {
            console.log( e );

            this.utilService.onCloseLoading();

            if (e.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", e.error.description);        
            } else {
              this.utilService.onShowMessageErrorSystem();        
            }
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  private async _respuestaExitosa() {
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Su registro se grabó satisfactoriamente.`);
    
    if ( respuesta.isConfirmed ) {
      this.router.navigate(['SARF/mantenimientos/configuracion/tipo_de_carta_orden/bandeja']);
    }
  }

  private _validarFormulario(): boolean {
    let resultado: boolean = true;

    if ( this.editar.value.nomDocu === '' || this.editar.value.nomDocu === null ) {
      this.documento.nativeElement.focus();
      this.utilService.onShowAlert('warning', 'Atención', 'Debe ingresar el Nombre del Documento, es obligatorio');
      resultado = false;
    } else if ( this.editar.value.descripcion === '' || this.editar.value.descripcion === null ) {
      this.descripcion.nativeElement.focus();
      this.utilService.onShowAlert('warning', 'Atención', 'Debe ingresar la Descripción, es obligatorio');
      resultado = false;
    }

    return resultado;
  }

  public async cancelar() {
    let dataFiltrosTipoCarta: IDataFiltrosTipoCartaOrden = await this.sharingInformationService.compartirDataFiltrosTipoCartaOrdenObservable.pipe( first() ).toPromise();
    if ( dataFiltrosTipoCarta.esBusqueda ) {
      dataFiltrosTipoCarta.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosTipoCartaOrdenObservableData = dataFiltrosTipoCarta;
    }
    this.router.navigate(['SARF/mantenimientos/configuracion/tipo_de_carta_orden/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaTipoCartaOrdenObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();    
  }



}
