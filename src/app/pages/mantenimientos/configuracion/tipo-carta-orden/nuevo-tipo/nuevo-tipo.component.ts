import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, map, of, Subject, forkJoin } from 'rxjs';
import { takeUntil, first } from 'rxjs/operators';

import Quill from 'quill';

import { IResponse2 } from '../../../../../interfaces/general.interface';
import { IDataFiltrosTipoCartaOrden, ITipoCartaVariables } from '../../../../../interfaces/configuracion/tipo-carta-orden.interface';

import { BodyTipoCartaOrden } from '../../../../../models/mantenimientos/configuracion/tipo-carta-orden.model';

import { Funciones } from '../../../../../core/helpers/funciones/funciones';

import { GeneralService } from '../../../../../services/general.service';
import { TipoCartaOrdenService } from '../../../../../services/mantenimientos/configuracion/tipo-carta-orden.service';
import { SharingInformationService } from '../../../../../core/services/sharing-services.service';
import { UtilService } from '../../../../../services/util.service';


@Component({
  selector: 'app-nuevo-tipo',
  templateUrl: './nuevo-tipo.component.html',
  styleUrls: ['./nuevo-tipo.component.scss']
})
export class NuevoTipoComponent implements OnInit, OnDestroy {

  @ViewChild('deTipo') descripcion: ElementRef;
  @ViewChild('deDocumento') documento: ElementRef;

  isLoading!: boolean;
  loadingBancos: boolean = true;
  loadingCuenta1: boolean = false;
  loadingCuenta2: boolean = false;
  loadingGrupos: boolean = false;
  editorInicio: string = '';
  editorConcepto: string = '';
  editorFinal: string = '';
  
  nuevo!: FormGroup;
  
  insertarVariable!: ITipoCartaVariables;
  listaVariables: ITipoCartaVariables[] = [];
  listaGrupos: IResponse2[] = [];

  $bancos!: Observable< any >;
  $cuentas1: Observable< any > = of([{ nuCuenBanc: '(NINGUNO)' }]);
  $cuentas2: Observable< any > = of([{ nuCuenBanc: '(NINGUNO)' }]);

  cuentaItem: any = { nuCuenBanc: '(NINGUNO)' };

  bodyTipoCarta!: BodyTipoCartaOrden;

  quillInicio!: Quill;
  quillConcepto!: Quill;
  quillFinal!: Quill;

  unsubscribe$ = new Subject<void>();

  constructor( 
    private fb: FormBuilder,
    private router: Router,
    private funciones: Funciones,
    private utilService: UtilService,
    private generalService: GeneralService,
    private tipoCartaService: TipoCartaOrdenService,
    private sharingInformationService: SharingInformationService
  ) { }

  ngOnInit(): void {
    this.bodyTipoCarta = new BodyTipoCartaOrden();

    this.nuevo = this.fb.group({
      nomDocu: ['', [ Validators.required ]],
      descripcion: ['', [ Validators.required ]],
      grupo: ['', []],
      anio: ['', []],
      decenio: ['', []],
      titulo: ['', []],
      sigAdm: ['', []],
      destinatario: ['', []],
      cargo: ['', []],
      bancCarg: [{value: '', disabled: true}, []],
      nroCuenCarg: [{value: '', disabled: true}, []],
      bancAbon: [{value: '', disabled: true}, []],
      nroCuenAbon: [{value: '', disabled: true}, []],
      inicio: ['', []],
      concepto: ['', []],
      final: ['', []],
      cuenta1: ['', []],
      cuenta2: ['', []]
    });

    const bancoItem: any = { nidInstitucion: 0, cdeInst: '(NINGUNO)' };

    this.$bancos = this.generalService.getCbo_Bancos().pipe(
      map( b => {
        b.unshift( bancoItem );
        this.loadingBancos = false;
        this.nuevo.get('bancCarg')?.enable();
        this.nuevo.get('bancAbon')?.enable();
        return b;
      })
    );

    this._buscarGruposCarta();
    this._obtenerVariables();
    this._obtenerNombreAnioDecenio();
  }

  // @HostListener('document:keydown', ['$event'])
  // handleKeyboardEvent(event: KeyboardEvent): void {
  //   if ( event.key === 'Enter' ) {
  //     this.grabar();
  //   } else if ( event.key === 'Escape' ) {
  //     this.cancelar();
  //   }
  // }

  private _buscarGruposCarta(): void {
    this.loadingGrupos = true;
    this.listaGrupos.push({ ccodigoHijo: '*', cdescri: '(NINGUNO)' });

    this.generalService.getCbo_GruposCarta().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data ) => {
        this.loadingGrupos = false;    
        if (data.length === 1) {   
          this.listaGrupos.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
          this.nuevo.patchValue({ "grupo": data[0].ccodigoHijo });
        } else {
          this.listaGrupos.push(...data);
        }        
      },
      error:( err )=>{
        this.loadingGrupos = false;    
      }
    });
  }

  private _obtenerVariables(): void {
    this.tipoCartaService.getTipoCartaVariables().pipe( takeUntil(this.unsubscribe$) )
      .subscribe({
        next: ( data ) => {
          this.listaVariables = data;
        },
        error: () => {
          this.listaVariables.push({ noCamp: '', deCamp: 'No se cargaron las variables'});
        }
      });
  }

  onChangeCuenta1( event: any ) {
    this.nuevo.get('nroCuenCarg')?.disable();
    this.nuevo.get('cuenta1')?.setValue('');
    this.loadingCuenta1 = true;

    this.$cuentas1 = this.generalService.getCbo_Cuentas( event.value ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta1 = false;
        this.nuevo.get('nroCuenCarg')?.enable();
        return c;
      })
    );
  }

  onChangeDescription1( event: any ) {
    this.$cuentas1.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( event.value == c.nuCuenBanc ) {
          this.nuevo.get('cuenta1')?.setValue( c.noCuenBanc )
        }
      }
    })
  }

  onChangeCuenta2( event: any ) {
    this.nuevo.get('nroCuenAbon')?.disable();
    this.nuevo.get('cuenta2')?.setValue('');
    this.loadingCuenta2 = true;

    this.$cuentas2 = this.generalService.getCbo_Cuentas( event.value ).pipe(
      map( c => {
        c.unshift( this.cuentaItem );
        this.loadingCuenta2 = false;
        this.nuevo.get('nroCuenAbon')?.enable();
        return c;
      })
    );
  }

  onChangeDescription2( event: any ) {    
    this.$cuentas2.forEach( cuenta => {
      for ( let c of cuenta ) {
        if ( event.value == c.nuCuenBanc ) {
          this.nuevo.get('cuenta2')?.setValue( c.noCuenBanc )
        }
      }
    })
  }

  campoEsValido( campo: string ) {
    return this.nuevo.controls[campo].errors && 
              this.nuevo.controls[campo].touched
  }

  public initEditorInicio( event: any ) {
    this.quillInicio = event.editor;
  }

  public selectionChangeInicio( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillInicio.getSelection().index;
      
      this.quillInicio.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillInicio.setSelection(cursorPosition + value, 'user');
      this.insertarVariable = null;
    }
  }

  public initEditorConcepto( event: any ) {
    this.quillConcepto = event.editor;
  }

  public selectionChangeConcepto( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillConcepto.getSelection().index;
      
      this.quillConcepto.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillConcepto.setSelection(cursorPosition + value, 'user');
      this.insertarVariable = null;
    }
  }

  public initEditorFinal( event: any ) {
    this.quillFinal = event.editor;
  }

  public selectionChangeFinal( event: any ) {
    if ( this.insertarVariable && event.range !== null ) {
      const cursorPosition = this.quillFinal.getSelection().index;
      
      this.quillFinal.insertText(cursorPosition, `[${ this.insertarVariable.deCamp }]`);

      const value: any = `[${ this.insertarVariable.deCamp }]`.length
      
      this.quillFinal.setSelection(cursorPosition + value, 'user');
      this.insertarVariable = null;
    }
  }

  grabar() {
    
    const boldInit = /<strong>/g;
    const boldFin = /<\/strong>/g;
    const italicInit = /<em>/g;
    const italicFin = /<\/em>/g;

    if ( !this._validarFormulario() ) {
      this.nuevo.markAllAsTouched()
      return;
    }

    let grupo = this.nuevo.get('grupo')?.value;
    grupo = grupo == '*' ? '' : grupo;

    let bancCarg = this.nuevo.get('bancCarg')?.value;
    bancCarg = bancCarg == '*' ? '' : bancCarg;

    let bancAbon = this.nuevo.get('bancAbon')?.value;
    bancAbon = bancAbon == '*' ? '' : bancAbon;

    let cuenBancCarg = this.nuevo.get('nroCuenCarg')?.value;
    cuenBancCarg = cuenBancCarg == '(NINGUNO)' ? '' : cuenBancCarg;

    let cuenBancAbon = this.nuevo.get('nroCuenAbon')?.value;
    cuenBancAbon = cuenBancAbon == '(NINGUNO)' ? '' : cuenBancAbon; 

    const seccionInicio: string = this.nuevo.get('inicio')?.value;
    const seccionConcep: string = this.nuevo.get('concepto')?.value;
    const seccionFinal: string = this.nuevo.get('final')?.value;

    this.bodyTipoCarta.noDocu = this.nuevo.get('nomDocu')?.value;
    this.bodyTipoCarta.noTipoCartOrde = this.nuevo.get('descripcion')?.value;
    this.bodyTipoCarta.coGrup = grupo;
    this.bodyTipoCarta.noAnio = this.nuevo.get('anio')?.value;
    this.bodyTipoCarta.noDece = this.nuevo.get('decenio')?.value;
    this.bodyTipoCarta.deTitu = this.nuevo.get('titulo')?.value;
    this.bodyTipoCarta.deSiglOfic = this.nuevo.get('sigAdm')?.value;
    this.bodyTipoCarta.noDest = this.nuevo.get('destinatario')?.value;
    this.bodyTipoCarta.noCarg = this.nuevo.get('cargo')?.value;
    this.bodyTipoCarta.idBancCarg = bancCarg;
    this.bodyTipoCarta.nuCuenBancCarg = cuenBancCarg;
    this.bodyTipoCarta.idBancAbon = bancAbon;
    this.bodyTipoCarta.nuCuenBancAbon = cuenBancAbon;
    this.bodyTipoCarta.deSeccInic = seccionInicio.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>')
    this.bodyTipoCarta.deSeccConc = seccionConcep.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>')
    this.bodyTipoCarta.deSeccFina = seccionFinal.replace(boldInit, '<b>').replace(boldFin, '</b>').replace(italicInit, '<i>').replace(italicFin, '</i>')

    console.log('bodyTipoCarta', this.bodyTipoCarta);

    this.utilService.onShowConfirm(`¿Está Ud. seguro de crear el siguiente registro?`).then( (result) => {
      if ( result.isConfirmed ) {
        this.utilService.onShowProcessLoading('Guardando la información');
        this.isLoading = true;

        this.tipoCartaService.createOrUpdateCartaOrden( 0, this.bodyTipoCarta ).subscribe({
          next: ( d ) => {
            console.log( d );

            if ( d.codResult < 0 ) {
              this.utilService.onCloseLoading();
              this.utilService.onShowAlert("info", "Atención", d.msgResult);

              return;
            }

            this.utilService.onCloseLoading();
            this._respuestaExitosa();
          },
          error: ( e ) => {
            console.log( e );

            this.utilService.onCloseLoading();

            if (e.error.category === "NOT_FOUND") {
              this.utilService.onShowAlert("error", "Atención", e.error.description);        
            } else {
              this.utilService.onShowMessageErrorSystem();        
            }
          },
          complete: () => {
            this.isLoading = false;
          }
        });
      }
    });
  }

  private async _respuestaExitosa() {
    let respuesta = await this.utilService.onShowAlertPromise("success", "Atención", `Su registro se grabó satisfactoriamente.`);
    
    if ( respuesta.isConfirmed ) {
      this.router.navigate(['SARF/mantenimientos/configuracion/tipo_de_carta_orden/bandeja']);
    }
  }

  private _obtenerNombreAnioDecenio() {
    forkJoin(
      this.generalService.getParametrosDelSistema(9),
      this.generalService.getParametrosDelSistema(10)
    ).pipe( takeUntil(this.unsubscribe$) )
     .subscribe(([ resDecenio, resAnio ]) => {
       this.nuevo.get('anio')?.setValue( resAnio[0].deParm );
      this.nuevo.get('decenio')?.setValue( resDecenio[0].deParm );
    }, ( _err: HttpErrorResponse ) => {
      this.nuevo.get('anio')?.setValue( '' );
      this.nuevo.get('decenio')?.setValue( '' );
    });
  }

  private _validarFormulario(): boolean {
    let resultado: boolean = true;

    if ( this.nuevo.value.nomDocu === '' || this.nuevo.value.nomDocu === null ) {
      this.documento.nativeElement.focus();
      this.utilService.onShowAlert('warning', 'Atención', 'Debe ingresar el Nombre del Documento, es obligatorio');
      resultado = false;
    } else if ( this.nuevo.value.descripcion === '' || this.nuevo.value.descripcion === null ) {
      this.descripcion.nativeElement.focus();
      this.utilService.onShowAlert('warning', 'Atención', 'Debe ingresar la Descripción, es obligatorio');
      resultado = false;
    }

    return resultado;
  }

  public async cancelar() {
    const dataFiltrosTipoCarta: IDataFiltrosTipoCartaOrden = await this.sharingInformationService.compartirDataFiltrosTipoCartaOrdenObservable.pipe( first() ).toPromise();
    if ( dataFiltrosTipoCarta.esBusqueda ) {
      dataFiltrosTipoCarta.esCancelar = true;
      this.sharingInformationService.compartirDataFiltrosTipoCartaOrdenObservableData = dataFiltrosTipoCarta;
    }
    this.router.navigate(['SARF/mantenimientos/configuracion/tipo_de_carta_orden/bandeja']);
  }

  ngOnDestroy(): void {
    this.sharingInformationService.irRutaBandejaComprobantePagoObservableData = true;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();  
  }

}
