import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';

import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { SortEvent, MessageService, ConfirmationService } from 'primeng/api';

import { Estado } from '../../../../../interfaces/combos.interface';
import { IBandejaProcess } from '../../../../../interfaces/global.interface';
import { IResponse2 } from '../../../../../interfaces/general.interface';
import { ITipoCartaOrden } from '../../../../../interfaces/configuracion/tipo-carta-orden.interface';

import { MenuOpciones } from '../../../../../models/auth/Menu.model';

import { GeneralService } from '../../../../../services/general.service';
import { TipoCartaOrdenService } from '../../../../../services/mantenimientos/configuracion/tipo-carta-orden.service';
import { UtilService } from '../../../../../services/util.service';
import { SharingInformationService } from '../../../../../core/services/sharing-services.service';

import { BaseBandeja } from '../../../../../base/base-bandeja.abstract';

import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-bandeja-tipo-carta',
  templateUrl: './bandeja-tipo-carta.component.html',
  styleUrls: ['./bandeja-tipo-carta.component.scss'],
  providers: [
    ConfirmationService,
    DialogService,
    MessageService
  ]
})
export class BandejaTipoCartaComponent extends BaseBandeja implements OnInit, OnDestroy {

  disabledEdit!: boolean;
  registros: string[] = [];
  loadingGrupos: boolean = false;

  tipoCarta!: FormGroup;

  $estado!: Estado[];
  listaGrupos: IResponse2[] = [];

  enviadosList!: ITipoCartaOrden[];
  override selectedValues: ITipoCartaOrden[] = [];

  tipoCartaOrdenProceso: IBandejaProcess = {
    idMemo: null,
    proceso: ''
  }

  currentPage: string = environment.currentPage;

  unsubscribe$ = new Subject<void>();

  constructor( 
    private fb: FormBuilder, 
    private router: Router,
    private confirmService: ConfirmationService,
    private dialogService: DialogService,
    private messageService: MessageService,
    private utilService: UtilService,
    private generalService: GeneralService,
    private tipoCartaService: TipoCartaOrdenService,
    private sharingInformationService: SharingInformationService
  ) { super() }

  ngOnInit(): void {
    this.codBandeja = MenuOpciones.Tipo_de_Carta_Orden;
    this.btnTipoCartaOrden();

    let estadoItem: Estado = { deEstado: '(TODOS)', coEstado: '*' };

    this.$estado = this.generalService.getCbo_Estado();
    this.$estado.unshift( estadoItem );

    this.tipoCarta = this.fb.group({
      nomDocu: ['', []],
      descripcion: ['', []],
      grupo: ['', []],
      estado: ['A', [ Validators.required ]]
    });

    this._buscarGruposCarta();
    this.buscarTipo();
  }

  private _buscarGruposCarta(): void {
    this.loadingGrupos = true;
    this.listaGrupos.push({ ccodigoHijo: '*', cdescri: '(TODOS)' });

    this.generalService.getCbo_GruposCarta().pipe(takeUntil(this.unsubscribe$))
      .subscribe({
      next: ( data ) => {
        this.loadingGrupos = false;    
        if (data.length === 1) {   
          this.listaGrupos.push({ ccodigoHijo: data[0].ccodigoHijo, cdescri: data[0].cdescri });    
          this.tipoCarta.patchValue({ "grupo": data[0].ccodigoHijo });
        } else {
          this.listaGrupos.push(...data);
        }        
      },
      error:( err )=>{
        this.loadingGrupos = false;    
      }
    });
  }

  buscarTipo() {
    const nomDocu = this.tipoCarta.get('nomDocu')?.value;
    const descripcion = this.tipoCarta.get('descripcion')?.value;

    let grupo = this.tipoCarta.get('grupo')?.value;
    grupo = grupo == '*' ? '' : grupo;

    let estado = this.tipoCarta.get('estado')?.value;
    if ( estado == '' ) {
      estado = 'A';
    } else if ( estado == '*' ){
      estado = '';
    }

    this.buscarBandeja( descripcion, estado, grupo, nomDocu );
  }

  buscarBandeja( tipo: string, inRegi: string, grupo: string, noDocu: string ) {
    Swal.showLoading();
    this.tipoCartaService.getBandejaTipoCarta( tipo, inRegi, grupo, noDocu ).subscribe({
      next: ( data ) => {
        
        for ( let d of data ) {
          if ( d.inRegi == 'A' )
            d.inRegi = 'Activo';
          else
           d.inRegi = 'Inactivo';
        }

        this.enviadosList = data;
      },
      error: () => {
        Swal.close()
        Swal.fire(
          'No se encuentran registros con los datos ingresados',
          'Tipo de Carta Orden',
          'info'
        );

        this.enviadosList = [];
      },
      complete: () => {
        Swal.close();
      }
    });
  }

  nuevoTipo() {
    this.tipoCartaOrdenProceso.idMemo = null;
    this.tipoCartaOrdenProceso.proceso = 'nuevo';
    this.router.navigate(['SARF/mantenimientos/configuracion/tipo_de_carta_orden/nuevo']);
    this.sharingInformationService.irRutaBandejaTipoCartaOrdenObservableData = false;
    // document.getElementById('btnNuevo').blur();

    // const dialog = this.dialogService.open( NuevoTipoComponent, {
    //   header: 'Registrar Nuevo Tipo Carta Orden',
    //   width: '60%',
    //   closable: false
    // });

    // dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
    //   if ( type == 'accept' ) {
    //     this.messageService.add({ 
    //       severity: 'success', 
    //       summary: 'Su registro se ha creado correctamente'
    //     });

    //     this.buscarTipo();
    //   }
    //   else if ( type == 'reject' ) {
    //     this.messageService.add({
    //       severity: 'error',
    //       summary: 'Ha ocurrido un problema al crear su registro'
    //     });
    //   }
    // });
  }

  editOrView( value: string ) {
    // document.getElementById('btnEditar').blur();

    if ( this.selectedValues?.length === 0 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'Debe seleccionar un registro');
      return;
    }

    if ( this.selectedValues.length !== 1 ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede editar más de un registro a la vez');
      return;
    }

    if ( this.selectedValues[0].deEsta === 'Inactivo' && value !== 'view' ) {
      this.utilService.onShowAlert('warning', 'Atención', 'No puede editar un registro en estado INACTIVO');
      return;
    }

    this.tipoCartaOrdenProceso.idMemo = this.selectedValues[0].idTipoCartOrde;
    this.tipoCartaOrdenProceso.proceso = value == 'edit' ? 'editar' : 'consultar';
    this.sharingInformationService.compartirTipoCartaOrdenProcesoObservableData = this.tipoCartaOrdenProceso;
    this.router.navigate([`SARF/mantenimientos/configuracion/tipo_de_carta_orden/editar/${ this.selectedValues[0].idTipoCartOrde }`]);
    this.sharingInformationService.irRutaBandejaTipoCartaOrdenObservableData = false;
    // const dataDialog = {
    //   tipo: this.selectedValues,
    //   vista: value == 'edit' ? false : true
    // };

    // const dialog = this.dialogService.open( EditarTipoComponent, {
    //   header: value == 'edit' ? 'Editar Tipo Carta Orden' : 'Consultar Tipo Carta Orden',
    //   width: '60%',
    //   closable: false,
    //   data: dataDialog
    // });

    // dialog.onClose.subscribe( (type: 'accept' | 'reject') => {
    //   if ( type == 'accept' ) {
    //     this.messageService.add({ 
    //       severity: 'success', 
    //       summary: 'Su registro se ha actualizado correctamente'
    //     });

    //     this.buscarTipo();
    //     this.selectedValues = [];
    //   }
    //   else if ( type == 'reject' ) {
    //     this.messageService.add({
    //       severity: 'error',
    //       summary: 'Ha ocurrido un error al actualizar su registro'
    //     });

    //     this.selectedValues = [];
    //   }

    //   this.selectedValues = [];
    // });
  }

  anular() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Inactivo' ) {
        Swal.fire('No puede anular un registro ya inactivo', '', 'warning');
        return;
      }

      this.registros.push( String( registro.idTipoCartOrde ) );
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea anular ${ msg1 } ${ msg2 }?`,
      accept: () => {
        Swal.showLoading();
        this.loadingAnu = true;

        const data = {
          idTipoCartaOrden: this.registros
        }

        this.tipoCartaService.anularCartasOrden( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se anularon los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al anular los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAnu = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscarTipo();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  activar() {
    if ( !this.validateSelect('') )
      return;

    for ( let registro of this.selectedValues ) {
      if ( registro.inRegi == 'Activo' ) {
        Swal.fire('No puede habilitar un registro ya activo', '', 'warning');
        return;
      }

      this.registros.push( String( registro.idTipoCartOrde ) );
    }

    const msg1: string = this.selectedValues.length > 1 ? 'los' : 'el';
    const msg2: string = this.selectedValues.length > 1 ? 'registros' : 'registro';

    this.confirmService.confirm({
      message: `¿Está Ud. seguro que desea activar ${ msg1 } ${ msg2 }?`,
      accept: () => {
        Swal.showLoading();
        this.loadingAct = true;

        const data = {
          idTipoCartaOrden: this.registros
        }

        this.tipoCartaService.activarCartasOrden( data ).subscribe({
          next: ( d ) => {
            console.log( d );
            this.messageService.add({ 
              severity: 'success', 
              summary: 'Se activaron los registros correctamente', 
            });
          },
          error: ( e ) => {
            console.log( e );
            this.messageService.add({ 
              severity: 'error', 
              summary: 'Ocurrió un error al activar los registros', 
            });
          },
          complete: () => {
            Swal.close();
            this.loadingAct = false;
            this.registros = [];
            this.selectedValues = [];
            this.buscarTipo();
          }
        });
      },
      reject: () => {
        this.registros = [];
        this.selectedValues = [];
      }
    });
  }

  customSort( event: SortEvent ) {
 
    event.data!.sort((data1, data2) => {
        let value1 = data1[event.field!];
        let value2 = data2[event.field!];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order! * result);
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();  
  }

}
