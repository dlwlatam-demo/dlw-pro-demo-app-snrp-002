import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from '../../../services/auth/auth-gaurd.service';

import { BandejaConfigComponent } from './configurar-documentos/bandeja-config/bandeja-config.component';
import { NuevaConfiguracionComponent } from './configurar-documentos/nueva-configuracion/nueva-configuracion.component';
import { EditarConfiguracionComponent } from './configurar-documentos/editar-configuracion/editar-configuracion.component';

import { BandejaTipoCartaComponent } from './tipo-carta-orden/bandeja-tipo-carta/bandeja-tipo-carta.component';
import { NuevoTipoComponent } from './tipo-carta-orden/nuevo-tipo/nuevo-tipo.component';
import { EditarTipoComponent } from './tipo-carta-orden/editar-tipo/editar-tipo.component';

import { BandejaTipoMemoComponent } from './tipo-memorandum/bandeja-tipo-memo/bandeja-tipo-memo.component';

import { FirmaDigitalComponent } from './firma-digital/firma-digital.component';
import { BandejaLogosComponent } from './configurar-logos/bandeja-logos/bandeja-logos.component';

const routes: Routes = [
  { path: 'configurar_documentos', 
    children: [
      { path: 'bandeja', component: BandejaConfigComponent },
      { path: 'nuevo', component: NuevaConfiguracionComponent },
      { path: 'editar', component: EditarConfiguracionComponent }
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'configurar_logo_documento',
    children: [
      { path: 'bandeja', component: BandejaLogosComponent }
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'tipo_de_carta_orden',
    children: [
      { path: 'bandeja', component: BandejaTipoCartaComponent },
      { path: 'nuevo', component: NuevoTipoComponent },
      { path: 'editar/:id', component: EditarTipoComponent}
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'tipo_memorandum',
    children: [
      { path: 'bandeja', component: BandejaTipoMemoComponent }
    ],
    canActivate: [ AuthGaurdService ]
  },
  { path: 'firma_digital', component: FirmaDigitalComponent, canActivate: [ AuthGaurdService ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionRoutingModule { }
