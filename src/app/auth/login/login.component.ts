import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';
import { environment } from '../../../environments/environment';
import { UtilService } from 'src/app/services/util.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form_login: FormGroup;

  constructor(
    protected fb: FormBuilder, 
    private router: Router,
    private authService: AuthService,
    private utilService: UtilService
  ) { }

  ngOnInit(): void {
    this.form_login = this.fb.group({
      usuario: new FormControl('', [ Validators.required ]),
      pass: new FormControl('', [ Validators.required ])
    });
  }

  login() {   
    this.utilService.onShowProcessLoading("Consultando las credenciales de acceso");    
    const username = this.form_login.get('usuario')?.value;
    const pass = this.form_login.get('pass')?.value;
    this.authService.login(username, pass).subscribe(
      (response) => {
         this.authService.guardarToken(response.access_token,response.jti);   
         this.authService.guardarUsuario(response.access_token);
         this.router.navigate(['/menu']);
         this.utilService.onCloseLoading();
      }, (err) => {    
        if (err.status != 500) {
          let icon_error = "error" ;
          let icon_error_des_title = "Error no específico al momento de ingresar al módulo";
          let icon_error_des = `Por favor comunicarse con soporte del ${environment.no_sistema}`;
          switch (err.status) {
            case 400:       
              icon_error_des_title = err.error.error_description;
              if (err.error.error == "invalid_grant") {//valida si usuario o contraseña no coincide
                icon_error_des = "Por favor verificar si sus credenciales son correctas";
              }
              break;
            case 401:
              icon_error = "info";
              icon_error_des_title = err.error.error_description;
              icon_error_des = "Por favor enviar este mensaje a la mesa de ayuda de su Zona Registral para mejor detalle";
              break;
            case 403:
              icon_error = "warning";
              icon_error_des_title = err.error.error_description;
              break;
            case 0:
              icon_error_des_title = "El servidor del sistema se encuentra en actualización o apagado";
              break;
          }          
          this.utilService.onShowAlert(icon_error, "Atención", `${icon_error_des_title}<br><small><b>${icon_error_des}</b></small>`);
        }
        else {          
          this.utilService.onShowAlert("error", "Atención", "Se ha producido un error al intentar ingresar al módulo, esto puede ser debido por una desconexión a la base de datos o algún error en el programa.<br><small><b>Intente nuevamente; si persiste el error, por favor comunicarse con Mesa de Ayuda o con el personal de soporte del Módulo de Mantenimiento de Secciones Registrales</b></small>");
        }
      }
    );
  }
}