import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../shared/shared.module';

import { MainComponent } from './main/main.component';
import { MenuComponent } from './menu/menu.component';


@NgModule({
  declarations: [
    MainComponent,
    MenuComponent
  ],
  exports: [
    MainComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule
  ]
})
export class HomeModule { }
