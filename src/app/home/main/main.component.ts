import { Component, OnInit } from '@angular/core';

import { SideNavToggle } from '../../interfaces/sidenav.interface';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  isSideNavCollapsed = false;
  screenWidth = 0;

  constructor() { }

  ngOnInit(): void {
  }

  onToggleSideNav( data: SideNavToggle ): void {
    this.screenWidth = data.screeWidth;
    this.isSideNavCollapsed = data.collapsed;
  }

  getBodyClass(): string {
    let styleClass = '';

    if ( this.isSideNavCollapsed && this.screenWidth > 768 ) {
      styleClass = 'body-trimmed';
    }
    else if ( this.isSideNavCollapsed && this.screenWidth <= 768 && this.screenWidth > 0 ) {
      styleClass = 'body-md-screen';
    }

    return styleClass;
  }

}
