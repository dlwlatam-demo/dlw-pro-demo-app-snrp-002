import { Component, OnInit } from '@angular/core';

import { SideNavToggle } from '../../interfaces/sidenav.interface';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  isSideNavCollapsed = false;
  screenWidth = 0;

  constructor() {}

  ngOnInit(): void {}

  onToggleSideNav( data: SideNavToggle ): void {
    this.screenWidth = data.screeWidth;
    this.isSideNavCollapsed = data.collapsed;
  }

}
