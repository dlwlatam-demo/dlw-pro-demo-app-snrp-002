import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { SidenavService } from '../services/sidenav.service';
import { UsuariosService } from '../services/seguridad/usuarios.service';
import { IUsuarioPerfil } from '../interfaces/seguridad/usuario.interface';

export abstract class BaseMenuUser {

    protected router: Router;
    protected sidenavService: SidenavService;
    protected usuarioService: UsuariosService;

    perfil?: string;
    perfilList?: IUsuarioPerfil[];
    roles?: string[] = JSON.parse( localStorage.getItem('roles_sarf')! );
    nombre_completo?: string = localStorage.getItem('nombre_sarf')!;

    menuPadrPadr: number[] = [];
    menuPadr: number[] = [];
    menuOpci: number[] = [];
    menuAcci: any[] = [];

    SFMENUITEM?: Observable<any>[] = [];

    showUserRol( code: number ): boolean {
        return this.roles?.includes( String( code ) )!;
    }

    showPerfilMenu( code: string ) {
        localStorage.setItem('perfil_sarf', code);
        this.router.navigate(['/SARF']);
    }

    initMenuOptions() {
        for ( let r of this.roles ) {
            this.SFMENUITEM.push( this.sidenavService.getMenuOptions( r ) )
        }
    }

    showMenuOptions( coPerf: string ) {
        const index = this.roles.findIndex( (r: any) => r == coPerf );

        this.SFMENUITEM[index].subscribe({
            next: ( menu ) => { this.getmenuOptions( menu ); }
        });
    }

    getmenuOptions( menu: any ) {
        this.menuPadrPadr = menu.map( (m: any) => m.coOpciPadrPadr );
        this.menuPadr = menu.map( (m: any) => m.coOpciPadr );
        this.menuOpci = menu.map( (m: any) => m.coOpci );
        this.menuAcci = menu.map( (m: any) => { return {coOpci: m.coOpci, actions: m.acciones} });
        localStorage.setItem('sarf_acciones', JSON.stringify(this.menuAcci))
    }
}