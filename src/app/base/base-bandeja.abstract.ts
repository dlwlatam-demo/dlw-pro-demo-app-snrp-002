import Swal from 'sweetalert2';

import { MenuOpciones } from '../models/auth/Menu.model';

import { environment } from '../../environments/environment';

export abstract class BaseBandeja {

  loadingAnu!: boolean;
  loadingAct!: boolean;

  codBandeja!: number;

  acciones!: any[];
  selectedValues!: any[];

  BtnNuevo!: number;
  BtnConsultar!: number;
  BtnEditar!: number;
  BtnAnular!: number;
  BtnActivar!: number;
  BtnBuscar!: number;
  BtnExportar!: number;
  BtnImportar!: number;
  BtnAdicionar_Sustento!: number;
  BtnSustentos!: number;
  BtnVer_Documento!: number;
  BtnSolicitar_Firma!: number;
  BtnEnviar_Correo!: number;
  BtnEliminar_Sustento!: number;
  BtnNo_Utilizado!: number;
  BtnReservar!: number;
  BtnCopiar!: number;
  BtnHistorial_Firmas!: number;
  BtnVer_Cheques!: number;
  BtnGenerar_Cheques!: number;
  BtnVer_Historial!: number;
  BtnExcel!: number;
  BtnSolicitar_Impresion!: number;
  BtnEliminar_Anexo!: number;
  BtnAdicionar_Anexo!: number;
  BtnAnexos!: number;
  BtnAnular_Sustento!: number;
  BtnDevolver!: number;
  BtnSolicitar_Anulacion!: number;
  BtnImprimir!: number;
  BtnEnviar_Documento!: number;
  BtnFirmar!: number;
  BtnRechazar!: number;
  BtnGenerar!: number;
  BtnPDF!: number;
  BtnCerrar!: number;
  BtnAbrir!: number;
  BtnAbrir_Conciliacion!: number;
  BtnCerrar_Conciliacion!: number;
  BtnTotal_Por_Oficina!: number;
  BtnReProcesar!: number;
  BtnDescargar_Conciliados!: number;
  BtnDescargar_No_Conciliados!: number;
  BtnDescargar_Anulaciones!: number;
  BtnDescargar_Contracargos!: number;
  BtnDescargar_Conciliados_Manual!: number;
  BtnConciliar_Por_Monto!: number;
  BtnIngresos_Nuevo!: number;
  BtnIngresos_Editar!: number;
  BtnObservaciones_Grabar!: number;
  BtnNUBIZ_Descargar_Ventas_Conciliadas!: number;
  BtnNUBIZ_Descargar_Ventas_No_Conciliadas!: number;
  BtnNUBIZ_Descargar_Anulaciones!: number;
  BtnNUBIZ_Descargar_Contracargos!: number;
  BtnTransac_SPRL_Descargar_Conciliados!: number;
  BtnTransac_SPRL_Descargar_No_Conciliados!: number;
  BtnTransac_SPRL_Descargar_Conciliado_Man!: number;
  BtnTransac_SPRL_Editar!: number;
  BtnTransac_NIUBIZ_Editar!: number;
  BtnSPRL_Pagalo_pe_Descargar_Conciliados!: number;
  BtnSPRL_Pagalo_pe_Descargar_No_Conciliados!: number;
  BtnSFTP_BN_Descargar_Conciliados!: number;
  BtnSFTP_BN_Descargar_No_Conciliados!: number;
  BtnSPRL_MACMYPE_Descargar_Conciliados!: number;
  BtnSPRL_MACMYPE_Descargar_No_Conciliados!: number;
  BtnDepos_Hermes_Descargar_Conciliados!: number;
  BtnDepos_Hermes_Descargar_No_Conciliados!: number;
  BtnEstado_Banc_Descargar_Conciliados!: number;
  BtnEstado_Banc_Descargar_No_Conciliados!: number;
  BtnPago_Elec_SCUNAC_Descarga_Conciliados!: number;
  BtnPago_Elec_SCUNAC_Descarga_No_Conciliado!: number;
  BtnSFTP_BN_Descargar_Archivo!: number;
  BtnEstado_Banc_Descargar_Archivo!: number;
  BtnRecaudacion_SPRL_Descargar_Archivo!: number;
  BtnDepos_HERMES_Descarga_Conciliados!: number;
  BtnDepos_HERMES_Descarga_No_Conciliado!: number;
  BtnEstado_Banc_Descargar_No_Conciliado!: number;
  BtnEditar_EEBB!: number;
  BtnLibro_Bancos_Descargar_Conciliados!: number;
  BtnLibro_Bancos_Descargar_No_Conciliados!: number;
  BtnAnulados_Descargar_Archivo!: number;
  BtnExtornos_Descargar_Archivo!: number;
  BtnDepos_No_Identif_Descargar_Archivo!: number;
  BtnIngr_Meses_Anteriores_Descargar_Archivo!: number;
  BtnAsignar_Opciones!: number;
  BtnAsignar_Perfiles!: number;
  BtnAsignar_Locales!: number;
  BtnAutorizar_Modificacion!: number;
  BtnAdicionar_Sustento_SIAF!: number
  BtnEliminar_Sustento_SIAF!: number;
  BtnAdicionar_Sustento_CP!: number;
  BtnEliminar_Sustento_CP!: number;
  BtnAdicionar_Sustento_Otro!: number;
  BtnEliminar_Sustento_Otro!: number;
  BtnAdicionar_Sustento_DocRecibidos!: number;
  BtnReenviar_Documento!: number;
  BtnSubir_Documento!: number;

  showButtonAct( code: number, codBandeja: number ): boolean {
    this.acciones = JSON.parse( localStorage.getItem('sarf_acciones')! );
    let botones: number[] = [];      
    if (this.acciones !== null ) {
      this.acciones.filter( ( a ) => {
        if ( a.coOpci == codBandeja ) {
          a.actions.map( ( b: any ) => {
            botones.push( b.coAcci );
          });
        }
      });
    } 
    return botones.includes( code );
  }

  validateSelect( valor: string ): boolean {
    if ( !this.selectedValues || !this.selectedValues.length ) {
      Swal.fire( 'Debe seleccionar un registro', '', 'error');
      this.selectedValues = [];
      return false;
    }

    if ( valor == 'edit' || valor == 'view' ) {
      if ( this.selectedValues.length != 1 ) {
        Swal.fire( 'Solo puede editar o consultar un registro a la vez', '', 'error');
        this.selectedValues = [];
        return false;
      }

      if ( valor == 'edit' ) {
        for ( let registro of this.selectedValues ) {
          if ( registro.inRegi == 'Inactivo' ) {
            Swal.fire('No puede editar un registro anulado', '', 'error');
            this.selectedValues = [];
            return false;
          }
        }
      }
    }
    else {
      if ( this.selectedValues.length > environment.registerLimit ) {
        Swal.fire( 'No puede seleccionar más de 40 registros', '', 'error');
        this.selectedValues = [];
        return false;
      }
    }

    return true;
  }

  formatearMontoImporte( monto: string ): string {
    return Number( monto ).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    });
  }

  visualizarReporte( reporte: any ) {
    const byteCharacters = atob(reporte);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/pdf' });
    const blobUrl = URL.createObjectURL(blob);
    const download = document.createElement('a');
    download.href =  blobUrl;
    download.setAttribute("target", "_blank");
    document.body.appendChild( download );
    download.click();
  }

  btnConfigurarDocumentos() {
    this.BtnNuevo = MenuOpciones.CD_Nuevo;
    this.BtnEditar = MenuOpciones.CD_Editar;
    this.BtnAnular = MenuOpciones.CD_Anular;
    this.BtnActivar = MenuOpciones.CD_Activar;
    this.BtnConsultar = MenuOpciones.CD_Consultar;
    this.BtnBuscar = MenuOpciones.CD_Buscar;
  }

  btnTipoCartaOrden() {
    this.BtnNuevo = MenuOpciones.TC_Nuevo;
    this.BtnConsultar = MenuOpciones.TC_Consultar;
    this.BtnEditar = MenuOpciones.TC_Editar;
    this.BtnAnular = MenuOpciones.TC_Anular;
    this.BtnActivar = MenuOpciones.TC_Activar;
    this.BtnBuscar = MenuOpciones.TC_Buscar;
  }

  btnTipoMemorandum() {
    this.BtnNuevo = MenuOpciones.TM_Nuevo;
    this.BtnConsultar = MenuOpciones.TM_Consultar;
    this.BtnEditar = MenuOpciones.TM_Editar;
    this.BtnAnular = MenuOpciones.TM_Anular;
    this.BtnActivar = MenuOpciones.TM_Activar;
    this.BtnBuscar = MenuOpciones.TM_Buscar;
  }

  btnCuentasBancarias() {
    this.BtnNuevo = MenuOpciones.CB_Nuevo;
    this.BtnConsultar = MenuOpciones.CB_Consultar;
    this.BtnEditar = MenuOpciones.CB_Editar;
    this.BtnAnular = MenuOpciones.CB_Anular;
    this.BtnActivar = MenuOpciones.CB_Activar;
    this.BtnBuscar = MenuOpciones.CB_Buscar;
  }

  btnClasificadores() {
    this.BtnNuevo = MenuOpciones.Clsf_Nuevo
    this.BtnConsultar = MenuOpciones.Clsf_Consultar
    this.BtnEditar = MenuOpciones.Clsf_Editar
    this.BtnAnular = MenuOpciones.Clsf_Anular
    this.BtnActivar = MenuOpciones.Clsf_Activar
    this.BtnBuscar = MenuOpciones.Clsf_Buscar
  }

  btnPlanDeCuenta() {
    this.BtnNuevo = MenuOpciones.PC_Nuevo;
    this.BtnConsultar = MenuOpciones.PC_Consultar;
    this.BtnEditar = MenuOpciones.PC_Editar;
    this.BtnAnular = MenuOpciones.PC_Anular;
    this.BtnActivar = MenuOpciones.PC_Activar;
    this.BtnBuscar = MenuOpciones.PC_Buscar;
  }

  btnTasasDistribucion() {
    this.BtnNuevo = MenuOpciones.TD_Nuevo;
    this.BtnConsultar = MenuOpciones.TD_Consultar;
    this.BtnEditar = MenuOpciones.TD_Editar;
    this.BtnAnular = MenuOpciones.TD_Anular;
    this.BtnActivar = MenuOpciones.TD_Activar;
    this.BtnBuscar = MenuOpciones.TD_Buscar;
  }

  btnInformacionDistribucion() {
    this.BtnBuscar = MenuOpciones.ID_Buscar;
    this.BtnExportar = MenuOpciones.ID_Exportar;
    this.BtnImportar = MenuOpciones.ID_Importar;
  }

  btnConstanciaDeAbono() {
    this.BtnAdicionar_Sustento = MenuOpciones.CA_Adicionar_Sustento
    this.BtnSustentos = MenuOpciones.CA_Sustentos
    this.BtnVer_Documento = MenuOpciones.CA_Ver_Documento
    this.BtnExportar = MenuOpciones.CA_Exportar
    this.BtnBuscar = MenuOpciones.CA_Buscar
    this.BtnSolicitar_Firma = MenuOpciones.CA_Solicitar_Firma
    this.BtnEnviar_Correo = MenuOpciones.CA_Enviar_Correo
    this.BtnEliminar_Sustento = MenuOpciones.CA_Eliminar_Sustento
    this.BtnHistorial_Firmas = MenuOpciones.CA_Historial_Firma
  }

  btnReciboDeIngreso() {
    this.BtnSolicitar_Firma = MenuOpciones.RI_Solicitar_Firma
    this.BtnEliminar_Sustento = MenuOpciones.RI_Eliminar_Sustento
    this.BtnAdicionar_Sustento = MenuOpciones.RI_Adicionar_Sustento
    this.BtnSustentos = MenuOpciones.RI_Sustentos
    this.BtnVer_Documento = MenuOpciones.RI_Ver_Documento
    this.BtnActivar = MenuOpciones.RI_Activar
    this.BtnAnular = MenuOpciones.RI_Anular
    this.BtnEditar = MenuOpciones.RI_Editar
    this.BtnNuevo = MenuOpciones.RI_Nuevo
    this.BtnConsultar = MenuOpciones.RI_Consultar
    this.BtnExportar = MenuOpciones.RI_Exportar
    this.BtnBuscar = MenuOpciones.RI_Buscar
    this.BtnNo_Utilizado = MenuOpciones.RI_No_Utilizado
    this.BtnReservar = MenuOpciones.RI_Reservar
    this.BtnCopiar = MenuOpciones.RI_Copiar
    this.BtnHistorial_Firmas = MenuOpciones.RI_Historial_Firmas
    this.BtnAutorizar_Modificacion = MenuOpciones.RI_Autorizar_Modificacion
    this.BtnAdicionar_Sustento_DocRecibidos = MenuOpciones.RI_Adicionar_Sustento_DocRecibidos
  }

  btnComprobanteDePago() {
    this.BtnBuscar = MenuOpciones.CP_Buscar;
    this.BtnExcel = MenuOpciones.CP_Excel;
    this.BtnConsultar = MenuOpciones.CP_Consultar;
    this.BtnNuevo = MenuOpciones.CP_Nuevo;
    this.BtnEditar = MenuOpciones.CP_Editar;
    this.BtnAnular = MenuOpciones.CP_Anular;
    this.BtnActivar = MenuOpciones.CP_Activar;
    this.BtnVer_Documento = MenuOpciones.CP_Ver_Documento;
    this.BtnSustentos = MenuOpciones.CP_Sustentos;
    this.BtnSolicitar_Firma = MenuOpciones.CP_Solicitar_Firma;
    this.BtnVer_Historial = MenuOpciones.CP_Ver_Historial;
    this.BtnCopiar = MenuOpciones.CP_Copiar;
    this.BtnReservar = MenuOpciones.CP_Reservar;
    this.BtnNo_Utilizado = MenuOpciones.CP_No_Utilizado;
    this.BtnGenerar_Cheques = MenuOpciones.CP_Generar_Cheques;
    this.BtnVer_Cheques = MenuOpciones.CP_Ver_Cheques;
    this.BtnAdicionar_Sustento = MenuOpciones.CP_Adicionar_Sustento;
    this.BtnEliminar_Sustento = MenuOpciones.CP_Eliminar_Sustento;
    this.BtnAutorizar_Modificacion = MenuOpciones.CP_Autorizar_Modificacion;
    this.BtnAdicionar_Sustento_SIAF = MenuOpciones.CP_Adicionar_Sustento_SIAF;
    this.BtnEliminar_Sustento_SIAF = MenuOpciones.CP_Eliminar_Sustento_SIAF;
    this.BtnAdicionar_Sustento_CP = MenuOpciones.CP_Adicionar_Sustento_CP;
    this.BtnEliminar_Sustento_CP = MenuOpciones.CP_Eliminar_Sustento_CP;
    this.BtnAdicionar_Sustento_Otro = MenuOpciones.CP_Adicionar_Sustento_Otro;
    this.BtnEliminar_Sustento_Otro = MenuOpciones.CP_Eliminar_Sustento_Otro;
  }

  btnComprobanteDePagoCheques() {
    this.BtnNuevo = MenuOpciones.CP_Cheque_Nuevo;
    this.BtnConsultar = MenuOpciones.CP_Cheque_Consultar;
    this.BtnEditar = MenuOpciones.CP_Cheque_Editar;
    this.BtnAnular = MenuOpciones.CP_Cheque_Anular;
    this.BtnActivar = MenuOpciones.CP_Cheque_Activar;
    this.BtnNo_Utilizado = MenuOpciones.CP_Cheque_No_Utilizado;
    this.BtnSolicitar_Impresion = MenuOpciones.CP_Cheque_Solicitar_Impresion;
  }

  btnCartaOrden() {
    this.BtnVer_Historial = MenuOpciones.CO_Ver_Historial
    this.BtnSolicitar_Firma = MenuOpciones.CO_Solicitar_Firma
    this.BtnEliminar_Anexo = MenuOpciones.CO_Eliminar_Anexo
    this.BtnAdicionar_Anexo = MenuOpciones.CO_Adicionar_Anexo
    this.BtnAnexos = MenuOpciones.CO_Anexos
    this.BtnAnular_Sustento = MenuOpciones.CO_Anular_Sustento
    this.BtnAdicionar_Sustento = MenuOpciones.CO_Adicionar_Sustento
    this.BtnSustentos = MenuOpciones.CO_Sustentos
    this.BtnVer_Documento = MenuOpciones.CO_Ver_Documento
    this.BtnActivar = MenuOpciones.CO_Activar
    this.BtnAnular = MenuOpciones.CO_Anular
    this.BtnEditar = MenuOpciones.CO_Editar
    this.BtnNuevo = MenuOpciones.CO_Nuevo
    this.BtnConsultar = MenuOpciones.CO_Consultar
    this.BtnExcel = MenuOpciones.CO_Excel
    this.BtnBuscar = MenuOpciones.CO_Buscar
  }

  btnMemorandum() {
    this.BtnEliminar_Sustento = MenuOpciones.M_Eliminar_Sustento
    this.BtnAdicionar_Sustento = MenuOpciones.M_Adicionar_Sustento
    this.BtnSustentos = MenuOpciones.M_Sustentos
    this.BtnVer_Documento = MenuOpciones.M_Ver_Documento
    this.BtnActivar = MenuOpciones.M_Activar
    this.BtnAnular = MenuOpciones.M_Anular
    this.BtnEditar = MenuOpciones.M_Editar
    this.BtnNuevo = MenuOpciones.M_Nuevo
    this.BtnConsultar = MenuOpciones.M_Consultar
    this.BtnExportar = MenuOpciones.M_Exportar
    this.BtnBuscar = MenuOpciones.M_Buscar
  }

  btnCheques() {
    this.BtnBuscar = MenuOpciones.C_Buscar;
    this.BtnConsultar = MenuOpciones.C_Consultar;
    this.BtnExcel = MenuOpciones.C_Excel;
    this.BtnImprimir = MenuOpciones.C_Imprimir;
    this.BtnSolicitar_Anulacion = MenuOpciones.C_Solicitar_Anulacion;
    this.BtnDevolver = MenuOpciones.C_Devolver;
  }

  btnDocumentosParaFirmar() {
    this.BtnBuscar = MenuOpciones.DF_Buscar;
    this.BtnConsultar = MenuOpciones.DF_Consultar;
    this.BtnVer_Documento = MenuOpciones.DF_Ver_Documento;
    this.BtnSustentos = MenuOpciones.DF_Ver_Sustentos;
    this.BtnAdicionar_Sustento = MenuOpciones.DF_Adicionar_Sustento;
    this.BtnEliminar_Sustento = MenuOpciones.DF_Eliminar_Sustento;
    this.BtnFirmar = MenuOpciones.DF_Firmar;
    this.BtnRechazar = MenuOpciones.DF_Rechazar;
    this.BtnVer_Historial = MenuOpciones.DF_Ver_Historial;
    this.BtnSubir_Documento = MenuOpciones.DF_Subir_Documento;
  }

  btnDocumentosVariosSolicitarFirma() {
    this.BtnBuscar = MenuOpciones.DVF_Buscar;
    this.BtnConsultar = MenuOpciones.DVF_Consultar;
    this.BtnNuevo = MenuOpciones.DVF_Nuevo;
    this.BtnEditar = MenuOpciones.DVF_Editar;
    this.BtnAnular = MenuOpciones.DVF_Anular;
    this.BtnActivar = MenuOpciones.DVF_Activar;
    this.BtnVer_Documento = MenuOpciones.DVF_Ver_Documento;
    this.BtnSustentos = MenuOpciones.DVF_Sustentos;
    this.BtnAdicionar_Sustento = MenuOpciones.DVF_Adicionar_Sustento;
    this.BtnEliminar_Sustento = MenuOpciones.DVF_Eliminar_Sustento;
    this.BtnSolicitar_Firma = MenuOpciones.DVF_Solicitar_Firma;
    this.BtnVer_Historial = MenuOpciones.DVF_Ver_Historial;
    this.BtnExportar = MenuOpciones.DVF_Exportar;
  }

  btnEnviarDocumentos() {
    this.BtnBuscar = MenuOpciones.ED_Buscar;
    this.BtnConsultar = MenuOpciones.ED_Consultar;
    this.BtnNuevo = MenuOpciones.ED_Nuevo;
    this.BtnEditar = MenuOpciones.ED_Editar;
    this.BtnAnular = MenuOpciones.ED_Anular;
    this.BtnActivar = MenuOpciones.ED_Activar;
    this.BtnVer_Documento = MenuOpciones.ED_Ver_Documento;
    this.BtnSustentos = MenuOpciones.ED_Sustentos;
    this.BtnAdicionar_Sustento = MenuOpciones.ED_Adicionar_Sustento;
    this.BtnEliminar_Sustento = MenuOpciones.ED_Eliminar_Sustento;
    this.BtnEnviar_Documento = MenuOpciones.ED_Enviar_Documento;
    this.BtnExportar = MenuOpciones.ED_Exportar;
    this.BtnReenviar_Documento = MenuOpciones.ED_Reenviar_Documento;
  }

  btnCerrarAbrirMes() {
    this.BtnCerrar = MenuOpciones.Cerrar;
    this.BtnAbrir = MenuOpciones.Abrir;
  }

  btnConsolidacionMediosElectronicos() {
    this.BtnBuscar = MenuOpciones.RE_Buscar;
    this.BtnConsultar = MenuOpciones.RE_Consultar;
    this.BtnNuevo = MenuOpciones.RE_Nuevo;
    this.BtnEditar = MenuOpciones.RE_Editar;
    this.BtnAnular = MenuOpciones.RE_Anular;
    this.BtnActivar = MenuOpciones.RE_Activar;
    this.BtnAbrir_Conciliacion = MenuOpciones.RE_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.RE_Cerrar_Conciliacion;
    this.BtnTotal_Por_Oficina = MenuOpciones.RE_Total_Por_Oficina;
    this.BtnReProcesar = MenuOpciones.RE_ReProcesar;
    this.BtnDescargar_Conciliados = MenuOpciones.RE_Descargar_Conciliados;
    this.BtnDescargar_No_Conciliados = MenuOpciones.RE_Descargar_No_Conciliados;
    this.BtnDescargar_Contracargos = MenuOpciones.RE_Descargar_Contracargos;
    this.BtnDescargar_Anulaciones = MenuOpciones.RE_Descargar_Anulaciones;
    this.BtnConciliar_Por_Monto = MenuOpciones.RE_Conciliar_Por_Monto;
    this.BtnIngresos_Nuevo = MenuOpciones.RE_Ingresos_Nuevo;
    this.BtnIngresos_Editar = MenuOpciones.RE_Ingresos_Editar;
    this.BtnObservaciones_Grabar = MenuOpciones.RE_Observaciones_Grabar;
  }

  btnConsolidacionAPPSunarp() {
    this.BtnBuscar = MenuOpciones.RA_Buscar;
    this.BtnConsultar = MenuOpciones.RA_Consultar;
    this.BtnNuevo = MenuOpciones.RA_Nuevo;
    this.BtnEditar = MenuOpciones.RA_Editar;
    this.BtnAnular = MenuOpciones.RA_Anular;
    this.BtnActivar = MenuOpciones.RA_Activar;
    this.BtnAbrir_Conciliacion = MenuOpciones.RA_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.RA_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.RA_ReProcesar;
    this.BtnNUBIZ_Descargar_Ventas_No_Conciliadas = MenuOpciones.RA_NUBIZ_Descargar_Ventas_No_Conciliadas;
    this.BtnNUBIZ_Descargar_Ventas_Conciliadas = MenuOpciones.RA_NUBIZ_Descargar_Ventas_Conciliadas;
    this.BtnNUBIZ_Descargar_Anulaciones = MenuOpciones.RA_NUBIZ_Descargar_Anulaciones;
    this.BtnNUBIZ_Descargar_Contracargos = MenuOpciones.RA_NUBIZ_Descargar_Contracargos;
    this.BtnTransac_SPRL_Descargar_No_Conciliados = MenuOpciones.RA_Transac_SPRL_Descargar_No_Conciliados;
    this.BtnTransac_SPRL_Descargar_Conciliados = MenuOpciones.RA_Transac_SPRL_Descargar_Conciliados;
  }

  btnConsolidacionSPRLVirtual() {
    this.BtnBuscar = MenuOpciones.RS_Buscar;
    this.BtnNuevo = MenuOpciones.RS_Nuevo;
    this.BtnEditar = MenuOpciones.RS_Editar;
    this.BtnAnular = MenuOpciones.RS_Anular;
    this.BtnActivar = MenuOpciones.RS_Activar;
    this.BtnConsultar = MenuOpciones.RS_Consultar;
    this.BtnAbrir_Conciliacion = MenuOpciones.RS_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.RS_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.RS_ReProcesar;
    this.BtnDescargar_No_Conciliados = MenuOpciones.RS_Descargar_Ventas_No_Conciliadas;
    this.BtnDescargar_Conciliados = MenuOpciones.RS_Descargar_Ventas_Conciliadas;
    this.BtnDescargar_Anulaciones = MenuOpciones.RS_Descargar_Anulaciones;
    this.BtnDescargar_Contracargos = MenuOpciones.RS_Descargar_Contracargos;
    this.BtnDescargar_Conciliados_Manual = MenuOpciones.RS_Descargar_Conciliados_Manual;
    this.BtnTransac_SPRL_Editar = MenuOpciones.RS_Transac_SPRL_Editar;
    this.BtnTransac_NIUBIZ_Editar = MenuOpciones.RS_Transac_NIUBIZ_Editar;
  }

  btnConsolidacionPagaloPe() {
    this.BtnBuscar = MenuOpciones.RP_Buscar;
    this.BtnConsultar = MenuOpciones.RP_Consultar;
    this.BtnNuevo = MenuOpciones.RP_Nuevo;
    this.BtnEditar = MenuOpciones.RP_Editar;
    this.BtnAnular = MenuOpciones.RP_Anular;
    this.BtnActivar = MenuOpciones.RP_Activar;
    this.BtnAbrir_Conciliacion = MenuOpciones.RP_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.RP_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.RP_ReProcesar;
    this.BtnSPRL_Pagalo_pe_Descargar_Conciliados = MenuOpciones.RP_SPRL_Pagalo_pe_Descargar_Conciliados;
    this.BtnSPRL_Pagalo_pe_Descargar_No_Conciliados = MenuOpciones.RP_SPRL_Pagalo_pe_Descargar_No_Conciliados;
    this.BtnSFTP_BN_Descargar_Conciliados = MenuOpciones.RP_SFTP_BN_Descargar_Conciliados;
    this.BtnSFTP_BN_Descargar_No_Conciliados = MenuOpciones.RP_SFTP_BN_Descargar_No_Conciliados;
  }

  btnConsolidacionMACMYPE() {
    this.BtnBuscar = MenuOpciones.RM_Buscar;
    this.BtnConsultar = MenuOpciones.RM_Consultar;
    this.BtnNuevo = MenuOpciones.RM_Nuevo;
    this.BtnEditar = MenuOpciones.RM_Editar;
    this.BtnAnular = MenuOpciones.RM_Anular;
    this.BtnActivar = MenuOpciones.RM_Activar;
    this.BtnAbrir_Conciliacion = MenuOpciones.RM_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.RM_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.RM_ReProcesar;
    this.BtnSFTP_BN_Descargar_Conciliados = MenuOpciones.RM_SFTP_BN_Descargar_Conciliados;
    this.BtnSFTP_BN_Descargar_No_Conciliados = MenuOpciones.RM_SFTP_BN_Descargar_No_Conciliados;
    this.BtnSPRL_MACMYPE_Descargar_Conciliados = MenuOpciones.RM_SPRL_MACMYPE_Descargar_Conciliados;
    this.BtnSPRL_MACMYPE_Descargar_No_Conciliados = MenuOpciones.RM_SPRL_MACMYPE_Descargar_No_Conciliados;
  }

  btnConsolidacionSCUNAC_Hermes() {
    this.BtnBuscar = MenuOpciones.RSI_Buscar;
    this.BtnNuevo = MenuOpciones.RSI_Nuevo;
    this.BtnEditar = MenuOpciones.RSI_Editar;
    this.BtnAnular = MenuOpciones.RSI_Anular;
    this.BtnActivar = MenuOpciones.RSI_Activar;
    this.BtnConsultar = MenuOpciones.RSI_Consultar;
    this.BtnAbrir_Conciliacion = MenuOpciones.RSI_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.RSI_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.RSI_ReProcesar;
    this.BtnDepos_Hermes_Descargar_Conciliados = MenuOpciones.RSI_Depos_Hermes_Descargar_Conciliados;
    this.BtnDepos_Hermes_Descargar_No_Conciliados = MenuOpciones.RSI_Depos_Hermes_Descargar_No_Conciliados;
    this.BtnEstado_Banc_Descargar_Conciliados = MenuOpciones.RSI_Estado_Banc_Descargar_Conciliados;
    this.BtnEstado_Banc_Descargar_No_Conciliados = MenuOpciones.RSI_Estado_Banc_Descargar_No_Conciliados;
  }

  btnConsolidacionBancariaMediosElectronicos() {
    this.BtnBuscar = MenuOpciones.RME_Buscar;
    this.BtnNuevo = MenuOpciones.RME_Nuevo;
    this.BtnEditar = MenuOpciones.RME_Editar;
    this.BtnAnular = MenuOpciones.RME_Anular;
    this.BtnActivar = MenuOpciones.RME_Activar;
    this.BtnConsultar = MenuOpciones.RME_Consultar;
    this.BtnAbrir_Conciliacion = MenuOpciones.RME_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.RME_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.RME_ReProcesar;
    this.BtnEstado_Banc_Descargar_Conciliados = MenuOpciones.RME_Estado_Banc_Descargar_Conciliados;
    this.BtnEstado_Banc_Descargar_No_Conciliados = MenuOpciones.RME_Estado_Banc_Descargar_No_Conciliados;
    this.BtnPago_Elec_SCUNAC_Descarga_Conciliados = MenuOpciones.RME_Pago_Elec_SCUNAC_Descarga_Conciliados;
    this.BtnPago_Elec_SCUNAC_Descarga_No_Conciliado = MenuOpciones.RME_Pago_Elec_SCUNAC_Descarga_No_Conciliado;
  }

  btnConsolidacionBancariaAPPSunarp() {
    this.BtnBuscar = MenuOpciones.RAQS_Buscar;
    this.BtnNuevo = MenuOpciones.RAQS_Nuevo;
    this.BtnEditar = MenuOpciones.RAQS_Editar;
    this.BtnAnular = MenuOpciones.RAQS_Anular;
    this.BtnActivar = MenuOpciones.RAQS_Activar;
    this.BtnConsultar = MenuOpciones.RAQS_Consultar;
    this.BtnAbrir_Conciliacion = MenuOpciones.RAQS_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.RAQS_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.RAQS_ReProcesar;
    this.BtnEstado_Banc_Descargar_Conciliados = MenuOpciones.RAQS_Estado_Banc_Descargar_Conciliados;
    this.BtnEstado_Banc_Descargar_No_Conciliados = MenuOpciones.RAQS_Estado_Banc_Descargar_No_Conciliados;
    this.BtnDescargar_Conciliados = MenuOpciones.RAQS_Descargar_Conciliados;
    this.BtnDescargar_No_Conciliados = MenuOpciones.RAQS_Descargar_No_Conciliados;
    this.BtnDescargar_Contracargos = MenuOpciones.RAQS_Descargar_Contracargos;
    this.BtnDescargar_Anulaciones = MenuOpciones.RAQS_Descargar_Anulaciones;
  }

  btnConsolidacionBancariaPagaloPe() {
    this.BtnBuscar = MenuOpciones.RPP_Buscar;
    this.BtnNuevo = MenuOpciones.RPP_Nuevo;
    this.BtnEditar = MenuOpciones.RPP_Editar;
    this.BtnAnular = MenuOpciones.RPP_Anular;
    this.BtnActivar = MenuOpciones.RPP_Activar;
    this.BtnConsultar = MenuOpciones.RPP_Consultar;
    this.BtnAbrir_Conciliacion = MenuOpciones.RPP_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.RPP_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.RPP_ReProcesar;
    this.BtnDescargar_Conciliados = MenuOpciones.RPP_Descargar_Conciliados;
    this.BtnDescargar_No_Conciliados = MenuOpciones.RPP_Descargar_No_Conciliados;
  }

  btnConsolidacionBancariaMACMYPE() {
    this.BtnBuscar = MenuOpciones.REM_Buscar;
    this.BtnNuevo = MenuOpciones.REM_Nuevo;
    this.BtnEditar = MenuOpciones.REM_Editar;
    this.BtnAnular = MenuOpciones.REM_Anular;
    this.BtnActivar = MenuOpciones.REM_Activar;
    this.BtnConsultar = MenuOpciones.REM_Consultar;
    this.BtnAbrir_Conciliacion = MenuOpciones.REM_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.REM_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.REM_ReProcesar;
    this.BtnDescargar_Conciliados = MenuOpciones.REM_Descargar_Conciliados;
    this.BtnDescargar_No_Conciliados = MenuOpciones.REM_Descargar_No_Conciliados;
  }

  btnConsolidacionBancariaTransferenciaCCI() {
    this.BtnBuscar = MenuOpciones.RT_Buscar;
    this.BtnNuevo = MenuOpciones.RT_Nuevo;
    this.BtnEditar = MenuOpciones.RT_Editar;
    this.BtnAnular = MenuOpciones.RT_Anular;
    this.BtnActivar = MenuOpciones.RT_Activar;
    this.BtnConsultar = MenuOpciones.RT_Consultar;
    this.BtnAbrir_Conciliacion = MenuOpciones.RT_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.RT_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.RT_ReProcesar;
    this.BtnDescargar_Conciliados = MenuOpciones.RT_Descargar_Conciliados;
    this.BtnDescargar_No_Conciliados = MenuOpciones.RT_Descargar_No_Conciliados;
  }

  btnConsolidacionBancariaDepositoHermes() {
    this.BtnBuscar = MenuOpciones.DH_Buscar;
    this.BtnNuevo = MenuOpciones.DH_Nuevo;
    this.BtnEditar = MenuOpciones.DH_Editar;
    this.BtnAnular = MenuOpciones.DH_Anular;
    this.BtnActivar = MenuOpciones.DH_Activar;
    this.BtnConsultar = MenuOpciones.DH_Consultar;
    this.BtnAbrir_Conciliacion = MenuOpciones.DH_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.DH_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.DH_ReProcesar;
    this.BtnDepos_HERMES_Descarga_Conciliados = MenuOpciones.DH_Depos_HERMES_Descarga_Conciliados;
    this.BtnDepos_HERMES_Descarga_No_Conciliado = MenuOpciones.DH_Depos_HERMES_Descarga_No_Conciliado;
    this.BtnEstado_Banc_Descargar_Conciliados = MenuOpciones.DH_Estado_Banc_Descargar_Conciliados;
    this.BtnEstado_Banc_Descargar_No_Conciliado = MenuOpciones.DH_Estado_Banc_Descargar_No_Conciliado;
  }

  btnConsolidacionBancariaLibroBanco() {
    this.BtnBuscar = MenuOpciones.LB_Buscar;
    this.BtnNuevo = MenuOpciones.LB_Nuevo;
    this.BtnEditar = MenuOpciones.LB_Editar;
    this.BtnAnular = MenuOpciones.LB_Anular;
    this.BtnActivar = MenuOpciones.LB_Activar;
    this.BtnConsultar = MenuOpciones.LB_Consultar;
    this.BtnAbrir_Conciliacion = MenuOpciones.LB_Abrir_Conciliacion;
    this.BtnCerrar_Conciliacion = MenuOpciones.LB_Cerrar_Conciliacion;
    this.BtnReProcesar = MenuOpciones.LB_ReProcesar;
    this.BtnEditar_EEBB = MenuOpciones.LB_Editar_EEBB;
    this.BtnLibro_Bancos_Descargar_Conciliados = MenuOpciones.LB_Libro_Bancos_Descargar_Conciliados;
    this.BtnLibro_Bancos_Descargar_No_Conciliados = MenuOpciones.LB_Libro_Bancos_Descargar_No_Conciliados;
    this.BtnAnulados_Descargar_Archivo = MenuOpciones.LB_Anulados_Descargar_Archivo;
    this.BtnEstado_Banc_Descargar_Conciliados = MenuOpciones.LB_Estado_Banc_Descargar_Conciliados;
    this.BtnEstado_Banc_Descargar_No_Conciliados = MenuOpciones.LB_Estado_Banc_Descargar_No_Conciliados;
    this.BtnExtornos_Descargar_Archivo = MenuOpciones.LB_Extornos_Descargar_Archivo;
    this.BtnDepos_No_Identif_Descargar_Archivo = MenuOpciones.LB_Depos_No_Identif_Descargar_Archivo;
    this.BtnIngr_Meses_Anteriores_Descargar_Archivo = MenuOpciones.LB_Ingr_Meses_Anteriores_Descargar_Archivo;
  }

  btnReporteReciboHonorario() {
    this.BtnGenerar = MenuOpciones.RH_Generar;
    this.BtnPDF = MenuOpciones.RH_PDF;
    this.BtnExcel = MenuOpciones.RH_Excel;
  }

  btnReporteCuentaContable() {
    this.BtnGenerar = MenuOpciones.CC_Generar;
    this.BtnPDF = MenuOpciones.CC_PDF;
    this.BtnExcel = MenuOpciones.CC_Excel;
  }

  btnReporteClasificadores() {
    this.BtnGenerar = MenuOpciones.Clsf_Generar;
    this.BtnPDF = MenuOpciones.Clsf_PDF;
    this.BtnExcel = MenuOpciones.Clsf_Excel;
  }

  btnReporteDiarioPOS() {
    this.BtnGenerar = MenuOpciones.RPO_Generar;
    this.BtnPDF = MenuOpciones.RPO_PDF;
    this.BtnExcel = MenuOpciones.RPO_Excel;
  }

  btnReporteConsolidadoPOS() {
    this.BtnGenerar = MenuOpciones.CR_Generar;
    this.BtnPDF = MenuOpciones.CR_PDF;
    this.BtnExcel = MenuOpciones.CR_Excel;
  }

  btnReporteRecaudacionAPP() {
    this.BtnGenerar = MenuOpciones.RA_Generar;
    this.BtnPDF = MenuOpciones.RA_PDF;
    this.BtnExcel = MenuOpciones.RA_Excel;
  }

  btnReporteRecaudacionQR() {
    this.BtnGenerar = MenuOpciones.RQ_Generar;
    this.BtnPDF = MenuOpciones.RQ_PDF;
    this.BtnExcel = MenuOpciones.RQ_Excel;
  }

  btnReporteRecaudacionSID() {
    this.BtnGenerar = MenuOpciones.RS_Generar;
    this.BtnPDF = MenuOpciones.RS_PDF;
    this.BtnExcel = MenuOpciones.RS_Excel;
  }

  btnReporteDistribucionAPP() {
    this.BtnGenerar = MenuOpciones.DA_Generar;
    this.BtnPDF = MenuOpciones.DA_PDF;
    this.BtnExcel = MenuOpciones.DA_Excel;
  }

  btnReporteDistribucionQR() {
    this.BtnGenerar = MenuOpciones.DQ_Generar;
    this.BtnPDF = MenuOpciones.DQ_PDF;
    this.BtnExcel = MenuOpciones.DQ_Excel;
  }

  btnReporteDistribucionSID() {
    this.BtnGenerar = MenuOpciones.DS_Generar;
    this.BtnPDF = MenuOpciones.DS_PDF;
    this.BtnExcel = MenuOpciones.DS_Excel;
  }

  btnReporteSPRL() {
    this.BtnGenerar = MenuOpciones.RSP_Generar;
    this.BtnPDF = MenuOpciones.RSP_PDF;
    this.BtnExcel = MenuOpciones.RSP_Excel;
  }

  btnReportePublicidadSPRL() {
    this.BtnGenerar = MenuOpciones.PS_Generar;
    this.BtnPDF = MenuOpciones.PS_PDF;
    this.BtnExcel = MenuOpciones.PS_Excel;
  }

  btnReportePublicidadSinIdent() {
    this.BtnGenerar = MenuOpciones.PI_Generar;
    this.BtnPDF = MenuOpciones.PI_PDF;
    this.BtnExcel = MenuOpciones.PI_Excel;
  }

  btnReporteServicioInscripcion() {
    this.BtnGenerar = MenuOpciones.SI_Generar;
    this.BtnPDF = MenuOpciones.SI_PDF;
    this.BtnExcel = MenuOpciones.SI_Excel;
  }

  btnReporteDistribucionSPRL() {
    this.BtnGenerar = MenuOpciones.DCP_Generar;
    this.BtnPDF = MenuOpciones.DCP_PDF;
    this.BtnExcel = MenuOpciones.DCP_Excel;
  }

  btnReporteIngresosCuenta() {
    this.BtnGenerar = MenuOpciones.IP_Generar;
    this.BtnPDF = MenuOpciones.IP_PDF;
    this.BtnExcel = MenuOpciones.IP_Excel;
  }

  btnReporteRecaudacionRegistrada() {
    this.BtnGenerar = MenuOpciones.RP_Generar;
    this.BtnPDF = MenuOpciones.RP_PDF;
    this.BtnExcel = MenuOpciones.RP_Excel;
  }

  btnReporteConciliacionMACMYPE() {
    this.BtnGenerar = MenuOpciones.CM_Generar;
    this.BtnPDF = MenuOpciones.CM_PDF;
    this.BtnExcel = MenuOpciones.CM_Excel;
  }

  btnReporteCuadroMACMYPE() {
    this.BtnGenerar = MenuOpciones.RM_Generar;
    this.BtnPDF = MenuOpciones.RM_PDF;
    this.BtnExcel = MenuOpciones.RM_Excel;
  }

  btnReporteGastosMACMYPE() {
    this.BtnGenerar = MenuOpciones.GM_Generar;
    this.BtnPDF = MenuOpciones.GM_PDF;
    this.BtnExcel = MenuOpciones.GM_Excel;
  }

  btnReporteAbonoCuentaSPRL() {
    this.BtnGenerar = MenuOpciones.AS_Generar;
    this.BtnPDF = MenuOpciones.AS_PDF;
    this.BtnExcel = MenuOpciones.AS_Excel;
  }

  btnReporteTransferenciaViaCCI() {
    this.BtnGenerar = MenuOpciones.TC_Generar;
    this.BtnPDF = MenuOpciones.TC_PDF;
    this.BtnExcel = MenuOpciones.TC_Excel;
  }

  btnReporteConsolidadoMensual() {
    this.BtnGenerar = MenuOpciones.RCM_Generar;
    this.BtnPDF = MenuOpciones.RCM_PDF;
    this.BtnExcel = MenuOpciones.RCM_Excel;
  }

  btnReporteRecaudacionDiario() {
    this.BtnGenerar = MenuOpciones.RD_Generar;
    this.BtnPDF = MenuOpciones.RD_PDF;
    this.BtnExcel = MenuOpciones.RD_Excel;
  }

  btnReporteTasasRegistrales() {
    this.BtnGenerar = MenuOpciones.TR_Generar;
    this.BtnPDF = MenuOpciones.TR_PDF;
    this.BtnExcel = MenuOpciones.TR_Excel;
  }

  btnReporteConciliacionLibroBanco() {
    this.BtnGenerar = MenuOpciones.CLB_Generar;
    this.BtnPDF = MenuOpciones.CLB_PDF;
    this.BtnExcel = MenuOpciones.CLB_Excel;
  }

  btnPerfiles() {
    this.BtnBuscar = MenuOpciones.PE_Buscar;
    this.BtnNuevo = MenuOpciones.PE_Nuevo;
    this.BtnEditar = MenuOpciones.PE_Editar;
    this.BtnAnular = MenuOpciones.PE_Anular;
    this.BtnActivar = MenuOpciones.PE_Activar;
    this.BtnConsultar = MenuOpciones.PE_Consultar;
    this.BtnAsignar_Opciones = MenuOpciones.PE_Asignar_Opciones;
  }

  btnUsuarios() {
    this.BtnBuscar = MenuOpciones.US_Buscar;
    this.BtnNuevo = MenuOpciones.US_Nuevo;
    this.BtnEditar = MenuOpciones.US_Editar;
    this.BtnAnular = MenuOpciones.US_Anular;
    this.BtnActivar = MenuOpciones.US_Activar;
    this.BtnConsultar = MenuOpciones.US_Consultar;
    this.BtnAsignar_Perfiles = MenuOpciones.US_Asignar_Perfiles;
    this.BtnAsignar_Locales = MenuOpciones.US_Asignar_Locales;
  }

}