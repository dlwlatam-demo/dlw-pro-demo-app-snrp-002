import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGaurdService } from './services/auth/auth-gaurd.service';

import { MainComponent } from './home/main/main.component';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./auth/auth.module').then( m => m.AuthModule )
  },
  {
    path: 'menu',
    loadChildren: () => import('./home/home.module').then( m => m.HomeModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: 'SARF',
    component: MainComponent,
    loadChildren: () => import('./pages/pages.module').then( m => m.PagesModule ),
    canActivate: [ AuthGaurdService ]
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'menu'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
