export interface ITramaCodigos {
    ccodigoMaster: string;
    ccodigoHijo: string;
}

export class BodyAsignarEstados {
    coPerf: number;
    tramaEstDocumentos: ITramaCodigos[];

    constructor() {
        this.coPerf = 0;
        this.tramaEstDocumentos = [];
    }

    onReset() {
        this.coPerf = 0;
        this.tramaEstDocumentos = [];
    }
}