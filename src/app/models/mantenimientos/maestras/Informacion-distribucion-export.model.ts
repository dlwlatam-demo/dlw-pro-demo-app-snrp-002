import { InformacionDistribucion } from "./informacion-distribucion.model";

export class InformacionDistribucionExport {
    lstInformacionDistribucion?: InformacionDistribucion[];
    reporteExcel: string;
}