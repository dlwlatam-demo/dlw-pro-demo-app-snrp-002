export class InformacionDistribucion {
    coZonaRegi?: string;
    tiReca?: string;
    feDistMont?: string;
    imRegu?: string;
    imDevo?: string;
    imAnul?: string;
    imContCarg?: string;
    inRegi?: string;
    coUsuaCrea?: string;
    feCrea?: string;
    coUsuaModi?: string;
    feModi?: string;
    deZonaRegi?: string;
    deEsta?: string;
    etipoReca?: string;
}