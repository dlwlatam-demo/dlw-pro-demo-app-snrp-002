export class BodyClasificador {
    coClsf: string;
    deClsf: string;
    deDetaClsf: string

    constructor() {
        this.coClsf = '';
        this.deClsf = '';
        this.deDetaClsf = '';
    }

    onReset() {
        this.coClsf = '';
        this.deClsf = '';
        this.deDetaClsf = '';
    }
}

export class BodyEliminateActivateClsf {
    listaClasificador: string[];

    constructor() {
        this.listaClasificador = [];
    }

    onReset() {
        this.listaClasificador = [];
    }
}
    