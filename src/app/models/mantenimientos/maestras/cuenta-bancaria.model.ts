export class CuentaBancaria {
    idBanc?: number
    nuCuenBanc?: string
    noCuenBanc?: string
    noCuenBancCort?: string
    coCuenInte?: string
    nuPrefCartOrde?: number
    nuCheqInic?: number
    nuCheqFina?: number
    nuCheqUtil?: number
    inRegi?: string
    noBanc?: string
    cuentasContables?: Array< CuentaBancariaContable > = new Array< CuentaBancariaContable >();
}

export class CuentaBancariaContable {
    idCuenBancCont?: number;
    id?: number;
    coCuenCont?: number;
    noCuenCont?: string;
    tiCuenCont?: string;
    tiDebeHabe?: string;
}