export class ConfigurarDocumento {
    idTipo?: number;
    tiDocu?: string;
    noTipo?: string;
    idBanc?: number;
    nuCuenBanc?: string;
    noCuenBanc?: string;
    obConce?: string;
    inRegi?: string;
    inTipoDocu?: string
    inNumeOpe?: string
    inReciHono?: string
    inNumeRuc?: string
}

export class ListCuentaContable {
    idTipoDocuCont?: number;
    coCuenCont: string;
    tiDebeHabe: string;
}

export class ListClasificadores {
    idTipoDocuClsf?: number;
    coClsf: string;
}

export class BodyConfiguracion {
    tiDocu: string;
    noTipo: string;
    inRegi: string;
}