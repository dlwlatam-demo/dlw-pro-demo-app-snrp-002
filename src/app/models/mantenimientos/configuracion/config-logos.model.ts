export class LogoImgTemp {
    encabezado?: any | string;
    encabezadoIzquierda?: any | string;
    encabezadoCentro?: any | string;
    encabezadoDerecha?: any | string;
    piePagina?: any | string;
    piePaginaIzquierda?: any | string;
    piePaginaCentro?: any | string;
    piePaginaDerecha?: any | string;
}

export class ListImagenesLogos {
    inPagiUbic: string;
    inPagiAlin: string;
    noImag: string;
    idGuidDocu: string;
}

export class BodyConfigLogos {
    tiDocu: string;
    listaImagenesPorTpoDoc: ListImagenesLogos[];

    constructor() {
        this.tiDocu = '';
        this.listaImagenesPorTpoDoc = [];
    }

    onReset() {
        this.tiDocu = '';
        this.listaImagenesPorTpoDoc = [];
    }
}