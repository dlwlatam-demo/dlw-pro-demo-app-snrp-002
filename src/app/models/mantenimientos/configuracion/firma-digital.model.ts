import { ITramaFirma } from "src/app/interfaces/configuracion/firma-digital.interface";

export class FirmaDigital {
    tiDocu?: string;
    deDocu?: string;
    firmaNro1?: string;
    firmaNro2?: string;
    firmaNro3?: string;
}

export class BodyFirmaDigital {
    idConfFirm: string;
    trama: ITramaFirma[];

    constructor() {
        this.idConfFirm = '';
        this.trama = [];
    }

    onReset() {
        this.idConfFirm = '';
        this.trama = [];
    }
}