// export class TipoMemorandum {
//     idTipoMemo?: number;
//     noTipoMemo?: string;
//     noDest?: string;
//     noCarg?: string;
//     noAsun?: string;
//     noRefe?: string;
//     idBancOrig?: number;
//     nuCuenBancOrig?: string;
//     idBancDest?: number;
//     nuCuenBancDest?: string;
//     inRegi?: string;
// }

export class TipoMemorandum {
    idTipoMemo?: number;
    noTipoMemo?: string;
    noDest?: string;
    noCarg?: string;
    noAsun?: string;
    noRefe?: string;
    idBancOrig?: number;
    nuCuenBancOrig?: string;
    idBancDest?: number;
    nuCuenBancDest?: string;
    noImpo0001?: string;
    noImpo0002?: string;
    noImpo0003?: string;
    noImpo0004?: string;
    noImpo0005?: string;
    noImpo0006?: string;
    tiOper0001?: string;
    tiOper0002?: string;
    tiOper0003?: string;
    tiOper0004?: string;
    tiOper0005?: string;
    tiOper0006?: string;
    inRegi?: string;
    coUsuaCrea?: number;
    feCrea?: string;
    coUsuaModi?: number;
    feModi?: string;
    noBancOrig?: string;
    noCuenBancOrig?: string;
    noBancDest?: string;
    noCuenBancDest?: string;
    deEsta?: string;
}