export class BodyBandejaTipoCarta {
    nombre: string;
    inRegi: string;
    coGrup: string;

    constructor() {
        this.nombre = '';
        this.inRegi = '';
        this.coGrup = '';
    }

    onReset() {
        this.nombre = '';
        this.inRegi = '';
        this.coGrup = '';
    }
}

export class BodyTipoCartaOrden {
    idTipoCartOrde?: number;
    noDocu: string;
    noTipoCartOrde: string;
    noDest: string;
    noCarg: string;
    idBancCarg: number;
    nuCuenBancCarg: string;
    idBancAbon: number;
    nuCuenBancAbon: string;
    deTitu: string;
    deSiglOfic: string;
    coGrup: string;
    deSeccInic: string;
    deSeccConc: string;
    deSeccFina: string;
    noDece: string;
    noAnio: string;

    constructor() {
        this.idTipoCartOrde = 0;
        this.noDocu = '';
        this.noTipoCartOrde = '';
        this.noDest = '';
        this.noCarg = '';
        this.idBancCarg = 0;
        this.nuCuenBancCarg = '';
        this.idBancAbon = 0;
        this.nuCuenBancAbon = '';
        this.deTitu = '';
        this.deSiglOfic = '';
        this.coGrup = '';
        this.deSeccInic = '';
        this.deSeccConc = '';
        this.deSeccFina = '';
        this.noDece = '';
        this.noAnio = '';
    }

    onReset() {
        this.idTipoCartOrde = 0;
        this.noDocu = '';
        this.noTipoCartOrde = '';
        this.noDest = '';
        this.noCarg = '';
        this.idBancCarg = 0;
        this.nuCuenBancCarg = '';
        this.idBancAbon = 0;
        this.nuCuenBancAbon = '';
        this.deTitu = '';
        this.deSiglOfic = '';
        this.coGrup = '';
        this.deSeccInic = '';
        this.deSeccConc = '';
        this.deSeccFina = '';
        this.noDece = '';
        this.noAnio = '';
    }
}