import { TramaSustento } from '../../archivo-adjunto.model';
import { IDocuVariosSustento } from '../../../interfaces/documentos-varios.interface';

export class BodyDocumentosVarios {
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    tiDocu: string;
    idDocuVari: number;
    noUsuaCrea: string;
    feDesd: string;
    feHast: string;
    esDocu: string;

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiDocu = '';
        this.idDocuVari = 0;
        this.noUsuaCrea = '';
        this.feDesd = '';
        this.feHast = '';
        this.esDocu = '';
    }
  
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiDocu = '';
        this.idDocuVari = 0;
        this.noUsuaCrea = '';
        this.feDesd = '';
        this.feHast = '';
        this.esDocu = '';
    }
}

export class BodyNuevoDocumentosVarios {
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    tiDocu: string;
    nuDocu: string;
    feDocu: string;
    noDocuPdf: string;
    idGuidDocu: string;
    noRutaDocuPdf: string;
    obDocu: string;

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiDocu = '';
        this.nuDocu = '';
        this.feDocu = '';
        this.noDocuPdf = '';
        this.idGuidDocu = '';
        this.noRutaDocuPdf = '';
        this.obDocu = '';
    }
  
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiDocu = '';
        this.nuDocu = '';
        this.feDocu = '';
        this.noDocuPdf = '';
        this.idGuidDocu = '';
        this.noRutaDocuPdf = '';
        this.obDocu = '';
    }
}

export class BodyEditarDocumentosVarios {
    idDocuVari: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    tiDocu: string;
    nuDocu: string;
    feDocu: string;
    noDocuPdf: string;
    idGuidDocu: string;
    noRutaDocuPdf: string;
    obDocu: string;

    constructor() {
        this.idDocuVari = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiDocu = '';
        this.nuDocu = '';
        this.feDocu = '';
        this.noDocuPdf = '';
        this.idGuidDocu = '';
        this.noRutaDocuPdf = '';
        this.obDocu = '';
    }
  
    onReset() {
        this.idDocuVari = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiDocu = '';
        this.nuDocu = '';
        this.feDocu = '';
        this.noDocuPdf = '';
        this.idGuidDocu = '';
        this.noRutaDocuPdf = '';
        this.obDocu = '';
    }
}

export interface ITramaId {
    idDocuVari: string;
}

export class BodyAnularActivarDocumentosVarios  {
    trama: ITramaId[];

    constructor() {
        this.trama = null;
    }
  
    onReset() {
        this.trama = null;
    }
}

export interface ITramaSolicitarFirmas {
    coPerf: string;
    nuSecuFirm: string;
}

export class BodySolicitarFirmasDocumentosVarios  {
    idDocuVari: number;
    trama: ITramaSolicitarFirmas[];

    constructor() {
        this.idDocuVari = 0;
        this.trama = null;
    }
  
    onReset() {
        this.idDocuVari = 0;
        this.trama = null;
    }
}

export class BodyGrabarFirmasDocumentosVarios  {
    tiDocu: string;
    idDocuOrig: number;
    noDocuPdf: string;
    noRutaDocuPdf: string;
    idGuidDocu: string;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    coPerf: number;
    nuDocuOrig: number;
    feDesd: string;
    feHast: string;
    esDocu: string;

    constructor() {
        this.tiDocu = '';
        this.idDocuOrig = 0;
        this.noDocuPdf = '';
        this.noRutaDocuPdf = '';
        this.idGuidDocu = '';
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.coPerf = 0;
        this.nuDocuOrig = 0;
        this.feDesd = '';
        this.feHast = '';
        this.esDocu = '';
    }
  
    onReset() {
        this.tiDocu = '';
        this.idDocuOrig = 0;
        this.noDocuPdf = '';
        this.noRutaDocuPdf = '';
        this.idGuidDocu = '';
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.coPerf = 0;
        this.nuDocuOrig = 0;
        this.feDesd = '';
        this.feHast = '';
        this.esDocu = '';
    }
}

export class BodyDocumentosVariosSustento  {
    idDocuVari: number;
    trama: IDocuVariosSustento[];

    constructor() {
        this.idDocuVari = 0;
        this.trama = [];
    }
  
    onReset() {
        this.idDocuVari = 0;
        this.trama = [];
    }
}