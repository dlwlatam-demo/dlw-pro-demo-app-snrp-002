import { TramaSustento, TramaAnexo } from '../../archivo-adjunto.model';

export class CartaOrden {
    id?: number;
    co_zona_regi?: string;
    co_ofic_regi?: string;
    co_loca_aten?: string;
    id_tipo_cart_orde?: number;
    nu_cart_orde?: string;
    fe_cart_orde?: string;
    im_cart_orde?: number;
    nu_siaf?: number;
    nu_comp_pago?: string;
    ti_docu_refe?: string;
    nu_docu_refe?: string;
    no_dest?: string;
    no_carg?: string;
    id_banc_carg?: number;
    nu_cuen_banc_carg?: string;
    id_banc_abon?: number;
    nu_cuen_banc_abon?: string;
    de_conc?: string;
    es_docu?: string;
    no_docu_pdf?: string;
    no_ruta_docu_pdf?: string;
    id_docu_firm?: number;
    no_usua_crea?: string;
    in_regi?: string;
}

export class BodyCartaOrden {
    coZonaRegi: string;
	coOficRegi: string;
	coLocaAten: string;
	idTipoCartOrde: number;
	nuCartOrden: string;
	feDesd: string;
	feHast: string;
	nuSiaf: number;
	esDocu: string;
	noUsuaCrea: string;
    coGrup: string

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCartOrde = 0;
        this.nuCartOrden = '';
        this.feDesd = '';
        this.feHast = '';
        this.nuSiaf = 0;
        this.esDocu = '';
        this.noUsuaCrea = '';
        this.coGrup = '';
    }
  
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCartOrde = 0;
        this.nuCartOrden = '';
        this.feDesd = '';
        this.feHast = '';
        this.nuSiaf = 0;
        this.esDocu = '';
        this.noUsuaCrea = '';
        this.coGrup = '';
    }
}

export class BodyNuevaCartaOrden {
    coZonaRegi: string
    coOficRegi: string
    coLocaAten: string;
    idTipoCartOrde: number;
    feCartOrde: string;
    imCartOrde: number;
    nuSiaf: number;
    nuCompPago: string;
    tiDocuRefe: string;
    nuDocuRefe: string;
    noDest: string;
    noCarg: string;
    idBancCarg: number;
    nuCuenBancCarg: string;
    idBancAbon: number;
    nuCuenBancAbon: string;
    coGrup: string;
    deSeccInic: string;
    deSeccConc: string;
    deSeccFina: string;
    noCuenBancAbon: string;

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCartOrde = 0;
        this.feCartOrde = '';
        this.imCartOrde = 0;
        this.nuSiaf = 0;
        this.nuCompPago = '';
        this.tiDocuRefe = '';
        this.nuDocuRefe = '';
        this.noDest = '';
        this.noCarg = '';
        this.idBancCarg = 0;
        this.nuCuenBancCarg = '';
        this.idBancAbon = 0;
        this.nuCuenBancAbon = '';
        this.coGrup = '';
        this.deSeccInic = '';
        this.deSeccConc = '';
        this.deSeccFina = '';
        this.noCuenBancAbon = '';
    }
  
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCartOrde = 0;
        this.feCartOrde = '';
        this.imCartOrde = 0;
        this.nuSiaf = 0;
        this.nuCompPago = '';
        this.tiDocuRefe = '';
        this.nuDocuRefe = '';
        this.noDest = '';
        this.noCarg = '';
        this.idBancCarg = 0;
        this.nuCuenBancCarg = '';
        this.idBancAbon = 0;
        this.nuCuenBancAbon = '';
        this.coGrup = '';
        this.deSeccInic = '';
        this.deSeccConc = '';
        this.deSeccFina = '';
        this.noCuenBancAbon = '';
    }
}

export class BodyEditarCartaOrden  {
    idCartOrde: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idTipoCartOrde: number;
    feCartOrde: string;
    imCartOrde: number;
    nuSiaf: number;
    nuCompPago: string;
    tiDocuRefe: string;
    nuDocuRefe: string;
    noDest: string;
    noCarg: string;
    idBancCarg: number;
    nuCuenBancCarg: string;
    idBancAbon: number;
    nuCuenBancAbon: string;
    coGrup: string;
    deSeccInic: string;
    deSeccConc: string;
    deSeccFina: string;
    noCuenBancAbon: string;

    constructor() {
        this.idCartOrde = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCartOrde = 0;
        this.feCartOrde = '';
        this.imCartOrde = 0;
        this.nuSiaf = 0;
        this.nuCompPago = '';
        this.tiDocuRefe = '';
        this.nuDocuRefe = '';
        this.noDest = '';
        this.noCarg = '';
        this.idBancCarg = 0;
        this.nuCuenBancCarg = '';
        this.idBancAbon = 0;
        this.nuCuenBancAbon = '';
        this.coGrup = '';
        this.deSeccInic = '';
        this.deSeccConc = '';
        this.deSeccFina = '';
        this.noCuenBancAbon = '';
    }
  
    onReset() {
        this.idCartOrde = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCartOrde = 0;
        this.feCartOrde = '';
        this.imCartOrde = 0;
        this.nuSiaf = 0;
        this.nuCompPago = '';
        this.tiDocuRefe = '';
        this.nuDocuRefe = '';
        this.noDest = '';
        this.noCarg = '';
        this.idBancCarg = 0;
        this.nuCuenBancCarg = '';
        this.idBancAbon = 0;
        this.nuCuenBancAbon = '';
        this.coGrup = '';
        this.deSeccInic = '';
        this.deSeccConc = '';
        this.deSeccFina = '';
        this.noCuenBancAbon = '';
    }
}

export class BodyEliminarActivarCartaOrden  {
    idCartasOrdenes: string[];

    constructor() {
        this.idCartasOrdenes = [];
    }
  
    onReset() {
        this.idCartasOrdenes = [];
    }
}

export class BodyCartaSustento {
    idCartOrde: number;
    trama: TramaSustento[];

    constructor() {
        this.idCartOrde = 0;
        this.trama = [];
    }

    onReset() {
        this.idCartOrde = 0;
        this.trama = [];
    }
}

export class BodyCartaAnexo {
    idCartOrde: number;
    trama: TramaAnexo[];

    constructor() {
        this.idCartOrde = 0;
        this.trama = [];
    }

    onReset() {
        this.idCartOrde = 0;
        this.trama = [];
    }
}

export class BodyCartaCorreo {
    mailTo: string;
    noRutaPdf: string;
    idCartasOrdenes: string[]

    constructor() {
        this.mailTo = '';
        this.noRutaPdf = '';
        this.idCartasOrdenes = []
    }
}
