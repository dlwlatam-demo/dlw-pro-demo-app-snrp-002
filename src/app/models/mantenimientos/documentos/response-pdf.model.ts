export class ResponsePdf {
    id?: number;
    codResult?: number;
    msgResult?: string;
    archivoPdf?: string;
}