import { TramaSustento } from '../../archivo-adjunto.model';
export class BodyMemorandum {
    coZonaRegi: string;
	coOficRegi: string;
	coLocaAten: string;
	idTipoMemo: number;
	feDesdMemo: string;
	feHastMemo: string;
	nuSistran: string;
	esDocu: string;
	noUsuaCrea: string;

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoMemo = 0; 
        this.feDesdMemo = '';
        this.feHastMemo = '';
        this.nuSistran = '0';
        this.esDocu = '';
        this.noUsuaCrea = '';
    }
  
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoMemo = 0;     
        this.feDesdMemo = '';
        this.feHastMemo = '';
        this.nuSistran = '0';
        this.esDocu = '';
        this.noUsuaCrea = '';
    }
}

export class BodyNuevoMemorandum {
    idMemo: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idTipoMemo: number;
    nuSistran: string;
    feMemo: string;    
    imMemo0001?: number;
    imMemo0002?: number;
    imMemo0003?: number;
    imMemo0004?: number;
    imMemo0005?: number;
    imMemo0006?: number;
    imTotal: number;
    feDesdMemo: string;
    feHastMemo: string;   
    noDest: string;
    noCarg: string;
    noAsun: string;
    noRefe: string;
    idBancOrig: number;
    nuCuenBancOrig: string;
    idBancDest: number;
    nuCuenBancDest: string;  

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoMemo = 0;
        this.nuSistran = "";
        this.feMemo = "";    
        this.imMemo0001 = 0;
        this.imMemo0002 = 0;
        this.imMemo0003 = 0;
        this.imMemo0004 = 0;
        this.imMemo0005 = 0;
        this.imMemo0006 = 0;
        this.imTotal = 0;
        this.feDesdMemo = "";
        this.feHastMemo = "";   
        this.noDest = "";
        this.noCarg = "";
        this.noAsun = "";
        this.noRefe = "";
        this.idBancOrig = 0;
        this.nuCuenBancOrig = "";
        this.idBancDest = 0;
        this.nuCuenBancDest = ""; 
    }
  
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoMemo = 0;
        this.nuSistran = "";
        this.feMemo = "";    
        this.imMemo0001 = 0;
        this.imMemo0002 = 0;
        this.imMemo0003 = 0;
        this.imMemo0004 = 0;
        this.imMemo0005 = 0;
        this.imMemo0006 = 0;
        this.imTotal = 0;
        this.feDesdMemo = "";
        this.feHastMemo = "";   
        this.noDest = "";
        this.noCarg = "";
        this.noAsun = "";
        this.noRefe = "";
        this.idBancOrig = 0;
        this.nuCuenBancOrig = "";
        this.idBancDest = 0;
        this.nuCuenBancDest = ""; 
    }
}

export class BodyEliminarActivarMemorandum  {
    memorandums: number[];

    constructor() {
        this.memorandums = [];
    }
  
    onReset() {
        this.memorandums = [];
    }
}

export class BodyMemorandumSustento {
    idMemo: number;
    trama: TramaSustento[];

    constructor() {
        this.idMemo = 0;
        this.trama = [];
    }

    onReset() {
        this.idMemo = 0;
        this.trama = [];
    }
}
