export class ConstanciaAbonoSustento {
    idConsAbonSust?: number
    idConsAbon?: number
    noDocuSust?: string
    noRutaSust?: string
    idGuidDocu?: string
    inRegi?: string
    coUsuaCrea?: number
    feCrea?: string
    coUsuaModi?: number
    feModi?: string
    nuConsAbon?: string
    feDepo?: string
    moDepo?: number
}