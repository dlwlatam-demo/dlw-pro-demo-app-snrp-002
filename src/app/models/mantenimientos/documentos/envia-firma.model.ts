export class EnviaFirma {
    tiDocu?: string;
    idDocuOrig?: number;
    noDocuPdf?: string;
    noRutaDocuPdf?: string;
	idGuidDocu?: string;
}
