import { IDocuEnviarSustento } from '../../../interfaces/documentos-enviar.interface';

export class BodyDocumentosEnviar {
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    tiProc: string;
    idDocuEnvi: number;
    noUsuaCrea: string;
    feDesd: string;
    feHast: string;
    esDocu: string;

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiProc = '';
        this.idDocuEnvi = 0;
        this.noUsuaCrea = '';
        this.feDesd = '';
        this.feHast = '';
        this.esDocu = '';
    }
  
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiProc = '';
        this.idDocuEnvi = 0;
        this.noUsuaCrea = '';
        this.feDesd = '';
        this.feHast = '';
        this.esDocu = '';
    }
}

export class BodyNuevoDocumentosEnviar {
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    tiProc: string;
    nuDocu: string;
    feDocu: string;
    noDocu: string;
    idGuidDocu: string;
    noRutaDocu: string;
    obDocu: string;

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiProc = '';
        this.nuDocu = '';
        this.feDocu = '';
        this.noDocu = '';
        this.idGuidDocu = '';
        this.noRutaDocu = '';
        this.obDocu = '';
    }
  
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiProc = '';
        this.nuDocu = '';
        this.feDocu = '';
        this.noDocu = '';
        this.idGuidDocu = '';
        this.noRutaDocu = '';
        this.obDocu = '';
    }
}

export class BodyEditarDocumentosEnviar {
    idDocuEnvi: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    tiProc: string;
    nuDocu: string;
    feDocu: string;
    noDocu: string;
    idGuidDocu: string;
    noRutaDocu: string;
    obDocu: string;

    constructor() {
        this.idDocuEnvi = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiProc = '';
        this.nuDocu = '';
        this.feDocu = '';
        this.noDocu = '';
        this.idGuidDocu = '';
        this.noRutaDocu = '';
        this.obDocu = '';
    }
  
    onReset() {
        this.idDocuEnvi = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.tiProc = '';
        this.nuDocu = '';
        this.feDocu = '';
        this.noDocu = '';
        this.idGuidDocu = '';
        this.noRutaDocu = '';
        this.obDocu = '';
    }
}

export interface ITramaId {
    idDocuEnvi: string;
}

export class BodyAnularActivarDocumentosEnviar  {
    trama: ITramaId[];

    constructor() {
        this.trama = null;
    }
  
    onReset() {
        this.trama = null;
    }
}

export interface ITramaEnviar {
    coPerf: string;
    coUsua: string;
    idDocuEnvi: number;
}

export class BodyEnviarDocumentosEnviar  {
    obDocu?: string
    trama: ITramaEnviar[];

    constructor() {
        this.obDocu = '';
        this.trama = null;
    }
  
    onReset() {
        this.obDocu = '';
        this.trama = null;
    }
}

export class BodyGrabarFirmasDocumentosEnviar  {
    tiProc: string;
    idDocuOrig: number;
    noDocu: string;
    noRutaDocu: string;
    idGuidDocu: string;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    coPerf: number;
    nuDocuOrig: number;
    feDesd: string;
    feHast: string;
    esDocu: string;

    constructor() {
        this.tiProc = '';
        this.idDocuOrig = 0;
        this.noDocu = '';
        this.noRutaDocu = '';
        this.idGuidDocu = '';
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.coPerf = 0;
        this.nuDocuOrig = 0;
        this.feDesd = '';
        this.feHast = '';
        this.esDocu = '';
    }
  
    onReset() {
        this.tiProc = '';
        this.idDocuOrig = 0;
        this.noDocu = '';
        this.noRutaDocu = '';
        this.idGuidDocu = '';
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.coPerf = 0;
        this.nuDocuOrig = 0;
        this.feDesd = '';
        this.feHast = '';
        this.esDocu = '';
    }
}

export class BodyDocumentosEnviarSustento  {
    idDocuEnvi: number;
    trama: IDocuEnviarSustento[];

    constructor() {
        this.idDocuEnvi = 0;
        this.trama = [];
    }
  
    onReset() {
        this.idDocuEnvi = 0;
        this.trama = [];
    }
}

export interface IDestinatario {
    coUsua?: string;
    noUsua?: string;
}