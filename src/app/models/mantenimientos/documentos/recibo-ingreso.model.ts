export class ReciboIngreso {
    idReciIngr?: number;
    idTipo?: number;
    noTipo?: string;
    coZonaRegi?: string;
    coOficRegi?: string;
    coLocaAten?: string;
    idTipoReciIngr?: number;
    nuReciIngr?: string;
    feReciIngr?: string;
    imReciIngr?: number;
    idBanc?: number;
    nuCuenBanc?: string;
    nuSiaf?: number;
    obConc?: string;
    esDocu?: string;
    noDocuPdf?: string;
    noRutaDocuPdf?: string;
    idDocuFirm?: number;
    idGuidDocu?: string;
    idReciIngrCopy?: number;
    inReciRese?: number;
    inReciNout?: number;
    noUsuaCrea?: string;
    noUsuaModi?: string;
    noUsuaAuto?: string;
    inDocuAuto?: string;
    coUsuaAuto?: number;
    feDocuAuto?: string;
    inRegi?: string;
    coUsuaCrea?: number;
    feCrea?: string;
    coUsuaModi?: number;
    feModi?: string;
    deZonaRegi?: string;
    deOficRegi?: string;
    deLocaAten?: string;
    noTipoReciIngr?: string;
    deEsta?: string;
    noCuenBanc?: string;
    noBanc?: string;
    sustRegi?: null;
    inSustRegi?: string;
    noUnid?: string;
    noSubu?: string;
    noCarg1?: string;
    noCarg2?: string;
    noCarg3?: string;
    cuentasContables?: IModelCuentaContable[];
    detallePresupuestal?: [];
    detallePatrimonial?: [];
    detalleClasificadores?: IModelClasificador[];
    noBene?: string;
    idReciIngrBene?: number
}

export type IModelCuentaContable = {
    idReciIngrCont: number,
    coCuenCont: string,
    noCuenCont: string,
    tiCuenCont: null,
    imDebe: number,
    imHabe: number,
    totalDebe: number,
    totalHabe: number
}

export type IModelClasificador = {
    coClsf: string,
    deClsf: string,
    imDebe: string,
    imHabe: string,
    idReciIngrClsf: number
}

export class fileSustentoRecibo {
    fileId?: string| undefined
    fileName?: string| undefined
    idGuidDocu?: string
}


export class historiaFirmaReciboIngreso {
    fileId?: number| undefined
    fileName?: string| undefined
}

export interface TramaID {
    id: string;
}

export class BodyEstadoReciboIngreso {
    trama: TramaID[];

    constructor() {
        this.trama = null
    }

    onReset() {
        this.trama = null;
    }
}

export class BodyValidarRecibo {
    tipoVali: number;
    tramaReciIngr: number[];

    constructor() {
        this.tipoVali = 0;
        this.tramaReciIngr = [];
    }

    onReset() {
        this.tipoVali = 0;
        this.tramaReciIngr = [];
    }
}

export class BodyNoUtilizadoRecibo {
    coUsuaNout: number;
    trama: TramaID[];

    constructor() {
        this.coUsuaNout = 0;
        this.trama = [];
    }

    onReset() {
        this.coUsuaNout = 0;
        this.trama = [];
    }
}

export class BodyNombreRecibo {
    noBene: string;
    inRegi: string;

    constructor() {
        this.noBene = '';
        this.inRegi = 'A';
    }

    onReset() {
        this.noBene = '';
        this.inRegi = 'A';
    }
}