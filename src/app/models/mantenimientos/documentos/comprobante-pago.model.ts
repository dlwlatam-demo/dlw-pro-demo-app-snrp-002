import { TramaSustento } from '../../archivo-adjunto.model';

export class ComprobantePago {
    id?: string;
    co_zona_regi?: string;
    co_ofic_regi?: string;
    co_loca_aten?: string;
    id_tipo_comp_pago?: string;
    nu_comp_pago?: string;
    fe_comp_pago?: string;
    id_banc?: string;
    nu_cuen_banc?: string;
    nu_siaf?: string;
    ob_conc?: string;
    es_docu?: string;
    no_docu_pdf?: string;
    no_ruta_docu_pdf?: string;
    ti_docu?: string;
    nu_docu?: string;
    ca_cheq?: string;
    nu_ope?: string;
    nu_reci_hono?: string;
    fe_reci_hono?: string;
    in_reci_hono_rete?: string;
    nu_ruc?: string;
    in_comp_rese?: string;
    in_comp_nout?: string;
    id_docu_firm?: string;
    no_usua_crea?: string;
    in_regi?: string;
}

export class BodyComprobantePago {
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idTipoCompPago: number;
    nuCompPago: string;
    feDesd: string;
    feHast: string;
    nuSiaf: number;
    esDocu:string;
    noUsuaCrea: string;
    feCrea: string;
    noUsuaModi: string;
    feModi: string;
    noUsuaAuto: string;
    feDocuAuto: string;
    noBene: string;
	imCompPago: number;
	idCompPago: number;
    tiDocu: string;
    nuDocu: string;
    nuCuenBanc: string;
    noDocuPdf: string;
    noRutaDocuPdf: string;

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCompPago = 0;
        this.nuCompPago = '';
        this.feDesd = '';
        this.feHast = '';
        this.nuSiaf = 0;
        this.esDocu = '';
        this.noUsuaCrea = '';
        this.feCrea = '';
        this.noUsuaModi = '';
        this.feModi = '';
        this.noUsuaAuto = '';
        this.feDocuAuto = '';
        this.noBene = '';
		this.imCompPago = 0;
        this.idCompPago = 0;
        this.tiDocu = '';
        this.nuDocu = '';
        this.nuCuenBanc = '';
		this.noDocuPdf = '';
		this.noRutaDocuPdf = '';
    }
  
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCompPago = 0;
        this.nuCompPago = '';
        this.feDesd = '';
        this.feHast = '';
        this.nuSiaf = 0;
        this.esDocu = '';
        this.noUsuaCrea = '';
        this.feCrea = '';
        this.noUsuaModi = '';
        this.feModi = '';
        this.noUsuaAuto = '';
        this.feDocuAuto = '';
        this.noBene = '';
		this.imCompPago = 0;
        this.idCompPago = 0;
        this.tiDocu = '';
        this.nuDocu = '';
        this.nuCuenBanc = '';
		this.noDocuPdf = '';
		this.noRutaDocuPdf = '';
    }
}

export interface ITrama {
    id?: number;
    coCuenCont: string;
    imDebe: string;
    imHabe: string;
}

export interface ITramaClasificador {
    id?: number;
    coClsf: string, 
    imDebe: string,
    imHabe: string
}

export interface ITramaMediosPago {
    id?: number;
    tiDocu: string;
    caDocu: string;
    nuDocu: string;
    imDocu: string;
}

export class BodyNuevoComprobantePago {
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idTipoCompPago: number;
    feCompPago: string;
    imCompPago: number;
    idBanc: number;
    nuCuenBanc: string;
    nuSiaf: number;
    obConc: string;
    nuReciHono: string;
    feReciHono: string;
    inReciHonoRete: string;
    nuBene: string;
    noBene: string;
    noTipoOper: string;
    tramaCuentaContable: ITrama[];
    tramaClasificadores: ITramaClasificador[];
    tramaMedioPago: ITramaMediosPago[]

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCompPago = 0;
        this.feCompPago = '';
        this.imCompPago = 0;
        this.idBanc = 0;
        this.nuCuenBanc = '';
        this.nuSiaf = 0;
        this.obConc = '';
        this.nuReciHono = '';
        this.feReciHono = '';
        this.inReciHonoRete = '';
        this.tramaCuentaContable = [];
        this.tramaClasificadores = [];
        this.tramaMedioPago = [];
    }
    
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCompPago = 0;
        this.feCompPago = '';
        this.imCompPago = 0;
        this.idBanc = 0;
        this.nuCuenBanc = '';
        this.nuSiaf = 0;
        this.obConc = '';
        this.nuReciHono = '';
        this.feReciHono = '';
        this.inReciHonoRete = '';
        this.tramaCuentaContable = [];
        this.tramaClasificadores = [];
        this.tramaMedioPago = [];
    }
}

export class BodyEditarComprobantePago  {
    idCompPago: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idTipoCompPago: number;
    feCompPago: string;
    imCompPago: number;
    idBanc: number;
    nuCuenBanc: string;
    nuSiaf: number;
    obConc: string;
    nuReciHono: string;
    feReciHono: string;
    inReciHonoRete: string;
    nuBene: string;
    noBene: string;
    noTipoOper: string;
    tramaCuentaContable: ITrama[];
    tramaClasificadores: ITramaClasificador[];
    tramaMedioPago: ITramaMediosPago[];

    constructor() {
        this.idCompPago = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCompPago = 0;
        this.feCompPago = '';
        this.imCompPago = 0;
        this.idBanc = 0;
        this.nuCuenBanc = '';
        this.nuSiaf = 0;
        this.obConc = '';
        this.nuReciHono = '';
        this.feReciHono = '';
        this.inReciHonoRete = '';
        this.tramaCuentaContable = [];
        this.tramaClasificadores = [];
        this.tramaMedioPago = [];
    }
  
    onReset() {
        this.idCompPago = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idTipoCompPago = 0;
        this.feCompPago = '';
        this.imCompPago = 0;
        this.idBanc = 0;
        this.nuCuenBanc = '';
        this.nuSiaf = 0;
        this.obConc = '';
        this.nuReciHono = '';
        this.feReciHono = '';
        this.inReciHonoRete = '';
        this.tramaCuentaContable = [];
        this.tramaClasificadores = [];
        this.tramaMedioPago = [];
    }
}

export interface ITramaId {
    id: string;
}

export class BodyEliminarActivarComprobantePago  {
    tramaCuentaContable: ITramaId[];

    constructor() {
        this.tramaCuentaContable = null;
    }
  
    onReset() {
        this.tramaCuentaContable = null;
    }
}

export class BodyCambiarEstadoComprobantePago  {
    tramaCuentaContable: ITramaId[];

    constructor() {
        this.tramaCuentaContable = null;
    }
  
    onReset() {
        this.tramaCuentaContable = null;
    }
}

export class BodyNoUtilizadoComprobante {
    coUsuaNout: number;
    tramaCuentaContable: ITramaId[];

    constructor() {
        this.coUsuaNout = 0;
        this.tramaCuentaContable = [];
    }
  
    onReset() {
        this.coUsuaNout = 0;
        this.tramaCuentaContable = [];
    }
}

export class BodyCopiarComprobantePago  {
    idCompPago: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;

    constructor() {
        this.idCompPago = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
    }
  
    onReset() {
        this.idCompPago = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
    }
}

export class BodyReservarComprobantePago  {
    caCompPago: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;

    constructor() {
        this.caCompPago = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
    }
  
    onReset() {
        this.caCompPago = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
    }
}

export class BodyGenerarCheques  {
    idCompPago: number;
    caCheq: number

    constructor() {
        this.idCompPago = 0;
        this.caCheq = 0;
    }
  
    onReset() {
        this.idCompPago = 0;
        this.caCheq = 0;
    }
}

export class BodyComprobanteSustento {
    idCompPago: number;
    trama: TramaSustento[];

    constructor() {
        this.idCompPago = 0;
        this.trama = [];
    }

    onReset() {
        this.idCompPago = 0;
        this.trama = [];
    }
}

export class BodyValidarComprobante {
    tiVali: string;
    tramaValidacion: ITramaId[];

    constructor() {
        this.tiVali = '';
        this.tramaValidacion = [];
    }

    onReset() {
        this.tiVali = '';
        this.tramaValidacion = [];
    }
}

export class BodyBeneficiarioBandeja {
    nuBene: string;
    noBene: string;
    inRegi: string;

    constructor() {
        this.nuBene = '';
        this.noBene = '';
        this.inRegi = 'A';
    }

    onReset() {
        this.nuBene = '';
        this.noBene = '';
        this.inRegi = 'A';
    }
}

export class BodyTipoOperBandeja {
    noTipoOper: string;
    inRegi: string;

    constructor() {
        this.noTipoOper = '';
        this.inRegi = 'A';
    }

    onReset() {
        this.noTipoOper = '';
        this.inRegi = 'A';
    }
}