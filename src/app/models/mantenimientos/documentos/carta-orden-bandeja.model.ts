import { ICartaOrden } from "src/app/interfaces/carta-orden.interface";

export class CartaOrdenBandeja {
    lstCartaOrden?: ICartaOrden[];
    reporteExcel: string;
}