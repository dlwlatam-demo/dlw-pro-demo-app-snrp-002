export class ConstanciaAbono {
     idConsAbon?: number
     coZonaRegi?: string
     coOficRegi?: string
     nidInstitucion?: number
     ccoDocu?: string
     cnuDocu?: string
     cdeInst?: string
     nsCtaTerc?: number
     noProyCtaTerc?: string
     nuSecuDepo?: number
     moDepo?: number
     nidInstBnco?: number
     cdeInstBnco?: string
     idDocuPago?: string
     nuOperaDepo?: string
     feDepo?: string
     noInstGira?: string
     nuConsAbon?: string
     esDocu?: string
     noDocuPdf?: string
     noRutaDocuPdf?: string
     caEmaiEnvi?: number
     coUsuaEmai?: number
     feEnviEmai?: string
     idDocuFirm?: number
     noUsuaCrea?: string
     inRegi?: string
     coUsuaCrea?: number
     feCrea?: string
     coUsuaModi?: number
     feModi?: string
     deZonaRegi?: string
     deOficRegi?: string
     deEsta?: string
     idGuidDocu?: string
}

export class fileSustentoConstancia {
    fileId?: string| undefined
    fileName?: string| undefined
    idGuidDocu?: string
}
export class ConstanciaAbonoFiltro {
}


export class historiaFirmaConstancia {
    deEsta?: string
    inRegi?: string

}

export class bodyMail {
     mailMfrom ?: string
     mailMto ?: string
     mailMcopy ?: string
     mailMbcc ?: string
     mailSubj ?: string
     mailBody ?: string
}