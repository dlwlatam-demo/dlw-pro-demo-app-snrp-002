export class DocumentoFirmar {
    id?: number;
    co_zona_regi?: string;
    co_ofic_regi?: string;
    co_loca_aten?: string;
    ti_docu?: string;
    id_docu_orig?: number;
    nu_docu_orig?: string;
    fe_docu?: string;
    no_docu_pdf?: string;
    no_ruta_docu_pdf?: string;
    nu_secu_firm?: number;
    co_perf?: number;
    es_docu?: string;
    in_regi?: string;
}

export class BodyDocumentoFirma {
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    coPerf: number;
    tiDocu: string;
    idDocuOrig: number;
    nuDocuOrig: number;
    feDesd: string;
    feHast: string;
    noUsuaCrea: string;
    esDocu: string;

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.coPerf = 0;
        this.tiDocu = '';
        this.idDocuOrig = 0;
        this.nuDocuOrig = 0;
        this.feDesd = '';
        this.feHast = '';
        this.noUsuaCrea = '';
        this.esDocu = '';
    }
  
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.coPerf = 0;
        this.tiDocu = '';
        this.idDocuOrig = 0;
        this.nuDocuOrig = 0;
        this.feDesd = '';
        this.feHast = '';
        this.noUsuaCrea = '';
        this.esDocu = '';
    }
}

export interface IFirmaBean {
    guid: string;
    idDocuFirm: number;
}

export class BodyFirmaBeanList {
    firmaBeanList: IFirmaBean[];

    constructor() {
        this.firmaBeanList = [];
    }

    onReset() {
        this.firmaBeanList = [];
    }
}

export class BodyFirmaMasivo {
    trama: string[];

    constructor() {
        this.trama = [];
    }

    onReset() {
        this.trama = [];
    }
}