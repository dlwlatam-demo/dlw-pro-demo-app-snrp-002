import { ReciboIngreso } from "./recibo-ingreso.model";

export class ReciboIngresoBandeja {
    lstReciboIngreso?: ReciboIngreso[];
    reporteExcel: string;
    reportePdf: string;
}