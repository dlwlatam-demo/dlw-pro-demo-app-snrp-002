export class ChequeConsulta {
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idCompPago: number;
    nuCompPago: string;
    nuCheq: number;
    feDesd: string;
    feHast: string;
    idBanc: number;
    nuCuenBanc: string;
    esDocu: string;

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idCompPago = null;
        this.nuCompPago = '';
        this.nuCheq = null;
        this.feDesd = '';
        this.feHast = '';
        this.idBanc = null;
        this.nuCuenBanc = '';
        this.esDocu = '';
    }

    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idCompPago = null;
        this.nuCompPago = '';
        this.nuCheq = null;
        this.feDesd = '';
        this.feHast = '';
        this.idBanc = null;
        this.nuCuenBanc = '';
        this.esDocu = '';
    }

}

export class ChequeRegister {
    idCheq: number;
    idCompPago: number;
    feCheq: string;
    imCheq: number;
    tiDocuIden: string;
    nuBene: string;
    noBene: string
    nuRefeCheq: number;  

    constructor() {
        this.idCheq = null
        this.idCompPago = null;
        this.feCheq = '';
        this.imCheq = null;
        this.tiDocuIden = '';
        this.nuBene = '';
        this.noBene = '';
        this.nuRefeCheq = null;        
    }

    onReset() {
        this.idCheq = null
        this.idCompPago = null;
        this.feCheq = '';
        this.imCheq = null;
        this.tiDocuIden = '';
        this.nuBene = '';
        this.noBene = '';
        this.nuRefeCheq = null;
    }
}

export class ChequeOperations {
    obMotiAnul?: string; 
    cheques: string[];

    constructor() {
        this.obMotiAnul = '';       
        this.cheques = [];               
    }
}

export class BodyImprimirAnularDevolverCheques {
    cheques: string[];

    constructor() {
        this.cheques = [];
    }

    onReset() {
        this.cheques = [];
    }
}

