export class ArchivoAdjunto {
    idGuidDocu: string;
    deDocuAdju: string;
    coTipoAdju: string;
    coZonaRegi: string;
    coOficRegi: string;
}

export class TramaSustento {
    id: number | string;
    noDocuSust: string;
    noRutaSust?: string;
    idGuidDocu: string;
}

export class ImagenesEnviarDocumento {
    idDocuEnviSust: number;
    idDocuEnvi: number;
    noDocuSust: string;
    noRutaSust: string;
    idGuidDocu: string;
    coUsuaCrea: number;
    coUsuaModi: number;
}

export class TramaSustentoComp {
    id: number;
    noDocuSust: string;
    inCompPagoSiaf: string;
    inConsPago: string;
    inSustOtro: string;
    nuOrde: number;
    idGuidDocu: string;
}

export class TramaAnexo {
    id: number | string;
    noDocuAnex: string;
    idGuidDocu: string;
}