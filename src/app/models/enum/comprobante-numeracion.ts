export enum COMPROBANTE_NUM {
    BUSCAR = '1',
    NUEVO = '2',
    EDITAR = '3',
    ANULAR = '4',
    ACTIVAR = '5',
    CONSULTAR = '6',
    AUTORIZAR_MODIFICACION = '7',
    VER_DOCUMENTO = '8',
    VER_SUSTENTO = '9',
    SOLICITAR_FIRMA = '10',
    VER_HISTORIAL = '11',
    COPIAR = '12',
    RESERVAR = '13',
    NO_UTILIZADO = '14',
    GENERAR_CHEQUES = '15',
    VER_CHEQUES = '16',
    EXPORTAR_EXCEL = '17'
}