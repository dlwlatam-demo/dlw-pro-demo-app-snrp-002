import { IFileNiubiz } from "src/app/interfaces/consolidacion-hermes";

export class FiltroRecaudacionHermes {
	idTipoCncl: number;
    coZonaRegi: string;
	coOficRegi: string;
	coLocaAten: string;	
    esDocu: string;
	feDesd: string;
	feHast: string;    	

    constructor() {
		this.idTipoCncl = 13;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.esDocu = '';        
        this.feDesd = '';
        this.feHast = '';
    }
  
    onReset() {
		this.idTipoCncl = 13;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.esDocu = '';          
        this.feDesd = '';
        this.feHast = ''; 
    }
}

export class RecaudacionHermes {
    tiProc: string;
    idCnclSiaf: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    feCnclDesd: string;
    feCnclHast: string;
    noAdju0001: string;
    idGuidDocu01: string;
    trama01: IFileNiubiz[];
    noAdju0002: string;
    idGuidDocu02: string;
    trama02: IFileNiubiz[];

    constructor() {
        this.tiProc = '';
        this.idCnclSiaf = 0;
        this.idTipoCncl = 13;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.feCnclDesd = '';
        this.feCnclHast = '';
        this.noAdju0001 = '';
        this.idGuidDocu01 = '';
        this.trama01 = [];
        this.noAdju0002 = '';
        this.idGuidDocu02 = '';
        this.trama02 = [];
    }
}


export class RecaudacionHermes2 {
    trama: string[];
    constructor() {
        this.trama = [];
    }
}