import { IFileSftpText, IFileNiubiz } from "src/app/interfaces/consolidacion-pagalo-pe";

export class FiltroRecaudacionPagaloPe {
	idTipoCncl: number;
    coZonaRegi: string;
	coOficRegi: string;
	coLocaAten: string;	
    esDocu: string;
	feDesd: string;
	feHast: string;    	

    constructor() {
		this.idTipoCncl = 10;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.esDocu = '';        
        this.feDesd = '';
        this.feHast = '';
    }
  
    onReset() {
		this.idTipoCncl = 10;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.esDocu = '';          
        this.feDesd = '';
        this.feHast = ''; 
    }
}

export class FileSFTP {
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
}

export class RecaudacionPagaloPe {
    tiProc: string;
    idCncl: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    feCncl: string;
    noAdju0001: string;
    idGuidDocu01: string;
    trama01: IFileSftpText[];
    noAdju0002: string;
    idGuidDocu02: string;
    trama02: IFileNiubiz[];

    constructor() {
        this.tiProc = '';
        this.idCncl = 0;
        this.idTipoCncl = 10;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.feCncl = '';
        this.noAdju0001 = '';
        this.idGuidDocu01 = '';
        this.trama01 = [];
        this.noAdju0002 = '';
        this.idGuidDocu02 = '';
        this.trama02 = [];
    }
}


export class RecaudacionPagaloPe2 {
    trama: string[];
    constructor() {
        this.trama = [];
    }
}

