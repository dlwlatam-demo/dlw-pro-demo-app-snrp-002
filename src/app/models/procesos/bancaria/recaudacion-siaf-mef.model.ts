import { IFileNiubiz } from "src/app/interfaces/consolidacion";

export class FiltroRecaudacionSiafMef {
	idTipoCncl: number;
    coZonaRegi: string;
	coOficRegi: string;
	coLocaAten: string;
    idOperPos: number;
    esDocu: string;
	feDesd: string;
	feHast: string;    	

    constructor() {
		this.idTipoCncl = 14;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.esDocu = '';        
        this.feDesd = '';
        this.feHast = '';
    }
  
    onReset() {
		this.idTipoCncl = 14;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.esDocu = '';          
        this.feDesd = '';
        this.feHast = ''; 
    }
}

export class FileNiubiz {
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;
    m: string;
    n: string;
    o: string;
    p: string;
    q: string;
    r: string;
    s: string;
    t: string;
    u: string;
    v: string;
    w: string;
    x: string;
}

export class RecaudacionSiafMef {
    tiProc: string;
    idCnclSiaf: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    feCnclDesd: string;
    feCnclHast: string;
    noAdju0001: string;
    noAdju0002: string;
    noAdju0003: string;
    idGuidDocu01: string;
    idGuidDocu02: string;
    idGuidDocu03: string;
    trama01: IFileNiubiz[];
    trama02: IFileNiubiz[];

    constructor() {
        this.tiProc = '';
        this.idCnclSiaf = 0;
        this.idTipoCncl = 14;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.feCnclDesd = '';
        this.feCnclHast = '';
        this.noAdju0001 = '';
        this.noAdju0002 = '';
        this.noAdju0003 = '';
        this.idGuidDocu01 = '';
        this.idGuidDocu02 = '';
        this.idGuidDocu03 = '';
        this.trama01 = [];
        this.trama02 = [];
    }
}

export class ConciliacionManual {
	idCnclSiaf: number;
    nuSecu: number;
    coClav: string;
    nuExpe: string;	
    coCiclo: string;
    feDocu: string;	
    coDocu: string;
    nuDocu: string;
    noRazo: string;
    imDebe: number;
    imHabe: number;
    imSaldo: number;	
    deMone: string;
    coEsta: string;
    inCncl: string;
    nuAnoEnto: string;	
    nuEjecSec: string;
	tiOper: string;
    nuOrdeCicl: string;	
    inRegi: string;
    obCncl: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;	

    constructor() {
        this.nuSecu = null;
        this.coClav = '';
        this.nuExpe = '';
        this.coCiclo = '';
        this.feDocu = '';
        this.coDocu = '';
        this.noRazo = '';
        this.imDebe = null;
        this.imHabe = null;
        this.imSaldo = null;
        this.deMone = '';
        this.coEsta = '';
		this.inCncl = '';
        this.nuAnoEnto = '';
        this.nuEjecSec = '';
        this.tiOper = '';
        this.nuOrdeCicl = '';
        this.obCncl = '';
    }
} 

export class ConciliacionManual2 {
    idCnclSiaf: number;
    nuFila: number;
    feBanc: string;
    deTran: string;
    coTran: string;	
    imCarg: number;
    imAbon: number;
    inCncl: string;
    inRegi: string;
    obCncl: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;	
	
    constructor() {
        this.nuFila = null;
        this.feBanc = '';
        this.deTran = '';
        this.coTran = '';
        this.imCarg = null;
        this.imAbon = null;
        this.inCncl = '';
        this.obCncl = '';
    }
}

export class RecaudacionSiafMef2 {
    trama: string[];
    constructor() {
        this.trama = [];
    }
}