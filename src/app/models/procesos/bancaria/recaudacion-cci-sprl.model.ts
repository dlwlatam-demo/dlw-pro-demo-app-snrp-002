import { IFileNiubiz } from "src/app/interfaces/consolidacion-cci-sprl";

export class FiltroRecaudacionCciSprl {
	idTipoCncl: number;
    coZonaRegi: string;
	coOficRegi: string;
	coLocaAten: string;	
    esDocu: string;
	feDesd: string;
	feHast: string;    	

    constructor() {
		this.idTipoCncl = 12;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.esDocu = '';        
        this.feDesd = '';
        this.feHast = '';
    }
  
    onReset() {
		this.idTipoCncl = 12;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.esDocu = '';          
        this.feDesd = '';
        this.feHast = ''; 
    }
}


export class RecaudacionCciSprl {
    tiProc: string;
    idCncl: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    feCncl: string;
    noAdju0001: string;
    idGuidDocu01: string;
    trama01: IFileNiubiz[];

    constructor() {
        this.tiProc = '';
        this.idCncl = 0;
        this.idTipoCncl = 12;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.feCncl = '';
        this.noAdju0001 = '';
        this.idGuidDocu01 = '';
        this.trama01 = [];
    }
}


export class RecaudacionCciSprl2 {
    trama: string[];
    constructor() {
        this.trama = [];
    }
}

export class ConciliacionManualMefCci {
    idCncl: number;
    nuFila: number;
    feBanc: string;
    deTran: string;
    coTran: string;
    imCarg: number;
    imAbon: number;
    inCncl: string;
    deEstaCncl: string;
    obCncl: string;
    inCnclAnte: string;
    inRegi: string;
    coUsuaCrea: string;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;

    constructor() {
        this.idCncl = null;
        this.nuFila = null;
        this.feBanc = '';
        this.deTran = '';
        this.coTran = '';
        this.imCarg = null;
        this.imAbon = null;
        this.inCncl = '';
        this.deEstaCncl = '';
        this.obCncl = '';
        this.inCnclAnte = '';
        this.inRegi = '';
        this.coUsuaCrea = '';
        this.feCrea = '';
        this.coUsuaModi = null;
        this.feModi = '';    
    }

    onReset() {
        this.idCncl = null;
        this.nuFila = null;
        this.feBanc = '';
        this.deTran = '';
        this.coTran = '';
        this.imCarg = null;
        this.imAbon = null;
        this.inCncl = '';
        this.deEstaCncl = '';
        this.obCncl = '';
        this.inCnclAnte = '';
        this.inRegi = '';
        this.coUsuaCrea = '';
        this.feCrea = '';
        this.coUsuaModi = null;
        this.feModi = '';    
    }
}

export class ConciliacionManualSprlCci {
    idCncl: number;
    nuSecu: number;
    personaId: number;
    usrId: string;
    usrCaja: string;
    tsCrea: string;
    abonoId: number;
    tpoPers: string;
    noRazoSoci: string;
    numOperacionBan: number;
    fecOperacionBan: string;
    bancoId: number;
    monto: number;
    inCncl: string;
    deEstaCncl: string;
    obCncl: string;
    inCnclAnte: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;

    constructor() {
        this.idCncl = null;
        this.nuSecu = null;
        this.personaId = null;
        this.usrId = '';
        this.usrCaja = '';
        this.tsCrea = '';
        this.abonoId = null;
        this.tpoPers = '';
        this.noRazoSoci = '';
        this.numOperacionBan = null;
        this.fecOperacionBan = '';
        this.bancoId = null;
        this.monto = null;
        this.inCncl = '';
        this.deEstaCncl = '';
        this.obCncl = '';
        this.inCnclAnte = '';
        this.inRegi = '';
        this.coUsuaCrea = null;
        this.feCrea = '';
        this.coUsuaModi = null;
        this.feModi = '';
    }

    onReset() {
        this.idCncl = null;
        this.nuSecu = null;
        this.personaId = null;
        this.usrId = '';
        this.usrCaja = '';
        this.tsCrea = '';
        this.abonoId = null;
        this.tpoPers = '';
        this.noRazoSoci = '';
        this.numOperacionBan = null;
        this.fecOperacionBan = '';
        this.bancoId = null;
        this.monto = null;
        this.inCncl = '';
        this.deEstaCncl = '';
        this.obCncl = '';
        this.inCnclAnte = '';
        this.inRegi = '';
        this.coUsuaCrea = null;
        this.feCrea = '';
        this.coUsuaModi = null;
        this.feModi = '';
    }
}