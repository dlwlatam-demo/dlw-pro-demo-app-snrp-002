import { IFileNiubiz } from "../../../interfaces/consolidacion";

export class FiltroBancariaAppQrSid {
	idTipoCncl: number;
    coZonaRegi: string;
	coOficRegi: string;
	coLocaAten: string;
    idOperPos: number;
    esDocu: string;
	feDesd: string;
	feHast: string;    	

    constructor() {
		this.idTipoCncl = 15;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.esDocu = '';        
        this.feDesd = '';
        this.feHast = '';
    }
  
    onReset() {
		this.idTipoCncl = 15;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.esDocu = '';          
        this.feDesd = '';
        this.feHast = ''; 
    }
}

export class BancariaAppQrSid {
    tiProc: string;
    idCncl: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    feCncl: string;
    noAdju0001: string;
    noAdju0002: string;
    noAdju0003: string;
    idGuidDocu01: string;
    idGuidDocu02: string;
    idGuidDocu03: string;
    trama01: IFileNiubiz[];

    constructor() {
        this.tiProc = '';
        this.idCncl = 0;
        this.idTipoCncl = 15;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.feCncl = '';
        this.noAdju0001 = '';
        this.noAdju0002 = '';
        this.noAdju0003 = '';
        this.idGuidDocu01 = '';
        this.idGuidDocu02 = '';
        this.idGuidDocu03 = '';
        this.trama01 = [];
    }
}

export class TramaBancariaAppQrSid {
    trama: string[];
    constructor() {
        this.trama = [];
    }
}