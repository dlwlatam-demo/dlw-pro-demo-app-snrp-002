export class RecaudacionMediosElectronicosDiners {
    nuFila?: number;
    coCome?: string;
    noCome?: string;
    fePago?: Date;
    coMoneCons?: string;
    imConsu?: number;
    imComi?: number;
    imComiMane?: number;
    imIgv?: number;
    imCarg?: number;
    coMonePago?: string;
    imNetoPaga?: number;
    deDocuAuto?: string;
}