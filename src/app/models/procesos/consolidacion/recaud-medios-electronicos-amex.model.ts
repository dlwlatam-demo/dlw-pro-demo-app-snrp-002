export class RecaudacionMediosElectronicosAmex {
    nuFila?: number;
    coEstb?: string;
    feDepo?: Date;
    coMone?: string;
    imTota?: number;
    imComiTota?: number;
    imComiMerc?: number;
    imIgvComiMerc?: number;
    imNetoDepo?: number;
    imDescServ?: number;
    imMontDepo?: number;
    deBanc?: string;
    deTipoCuen?: string;
    nuCuen?: string;
    deEsta?: string;
}