import { IFileNiubiz } from "src/app/interfaces/consolidacion";

export class FiltroRecaudacionAppQrSid {
	idTipoCncl: number;
    coZonaRegi: string;
	coOficRegi: string;
	coLocaAten: string;
    idOperPos: number;
    esDocu: string;
	feDesd: string;
	feHast: string;    	

    constructor() {
		this.idTipoCncl = 2;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.esDocu = '';        
        this.feDesd = '';
        this.feHast = '';
    }
  
    onReset() {
		this.idTipoCncl = 2;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.esDocu = '';          
        this.feDesd = '';
        this.feHast = ''; 
    }
}

export class FileNiubiz {
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;
    m: string;
    n: string;
    o: string;
    p: string;
    q: string;
    r: string;
    s: string;
    t: string;
    u: string;
    v: string;
    w: string;
    x: string;
}

export class RecaudacionAppQrSid {
    tiProc: string;
    idCncl: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    feCncl: string;
    noAdju0001: string;
    noAdju0002: string;
    noAdju0003: string;
    idGuidDocu01: string;
    idGuidDocu02: string;
    idGuidDocu03: string;
    trama01: IFileNiubiz[];

    constructor() {
        this.tiProc = '';
        this.idCncl = 0;
        this.idTipoCncl = 2;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.feCncl = '';
        this.noAdju0001 = '';
        this.noAdju0002 = '';
        this.noAdju0003 = '';
        this.idGuidDocu01 = '';
        this.idGuidDocu02 = '';
        this.idGuidDocu03 = '';
        this.trama01 = [];
    }
}

export class ConciliacionManual {
	idCncl: number;
    nuSecu: number;
    feCncl: string;
    idUsua: string;
    razSoc: string;
    primerApellido: string;
    segundoApellido: string;
    nombres: string;	
    monto: number;
    idPagoLinea: number;
    eTicket: string;
    inCncl: string;
    obCnclAppQrSid: string;

    constructor() {
        this.nuSecu = null;
        this.feCncl = '';
        this.idUsua = '';
        this.razSoc = '';
        this.primerApellido = '';
        this.segundoApellido = '';
        this.nombres = '';
        this.monto = null;
        this.idPagoLinea = null;
        this.eTicket = '';
        this.inCncl = '';
        this.obCnclAppQrSid = '';
    }
} 

export class ConciliacionManual2 {
    idCncl: number;
    nuSecu: number;
    feCncl: string;
    idUsua: string;
    razSoc: string;
    primerApellido: string;
    segundoApellido: string;
    nombres: string;	
    monto: number;
    idPagoLinea: number;
    eTicket: string;
    inCncl: string;
    obCnclAppQrSid: string;

    constructor() {
        this.nuSecu = null;
        this.feCncl = '';
        this.idUsua = '';
        this.razSoc = '';
        this.primerApellido = '';
        this.segundoApellido = '';
        this.nombres = '';
        this.monto = null;
        this.idPagoLinea = null;
        this.eTicket = '';
        this.inCncl = '';
        this.obCnclAppQrSid = '';
    }
}

export class RecaudacionAppQrSid2 {
    trama: string[];
    constructor() {
        this.trama = [];
    }
}