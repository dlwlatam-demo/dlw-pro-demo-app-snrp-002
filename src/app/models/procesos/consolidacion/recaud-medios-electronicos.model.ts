import { RecaudacionMediosElectronicosTrama01 } from '../../../models/procesos/consolidacion/recaud-medios-electronicos-trama01.model';
import { RecaudacionMediosElectronicosTrama02 } from '../../../models/procesos/consolidacion/recaud-medios-electronicos-trama02.model';
import { RecaudacionMediosElectronicosTrama03 } from '../../../models/procesos/consolidacion/recaud-medios-electronicos-trama03.model';

export class RecaudacionMediosElectronicos {
    codResult?: number;
    msgResult?: string;
    
    tiProc?: string;
    idCncl?: number;
    idTipoCncl?: number;
    coZonaRegi?: string;
    coOficRegi?: string;
    coLocaAten?: string;
    idOperPos?: number;
    nuCncl?: string;
    feCncl?: string;
    imCncl?: number;
    noAdju0001?: string;
    noAdju0002?: string;
    noAdju0003?: string;
    esDocu?: string;
    noUsuaCrea?: string;
    inRegi?: string;
    coUsuaCrea?: number;
    feCrea?: string;
    coUsuaModi?: number;
    feModi?: string;
    deZonaRegi?: string;
    deOficRegi?: string;
    deLocaAten?: string;
    noOperPos?: string;
    deEsta?: string;
    trama01?: RecaudacionMediosElectronicosTrama01[];
    trama02?: RecaudacionMediosElectronicosTrama02[];
    trama03?: RecaudacionMediosElectronicosTrama03[];
}