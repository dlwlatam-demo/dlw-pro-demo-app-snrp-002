import { IFileNiubiz } from "src/app/interfaces/consolidacion";

export class FiltroRecaudacionSprl {
	idTipoCncl: number;
    coZonaRegi: string;
	coOficRegi: string;
	coLocaAten: string;
    idOperPos: number;
    esDocu: string;
	feDesd: string;
	feHast: string;    	

    constructor() {
		this.idTipoCncl = 5;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.esDocu = '';        
        this.feDesd = '';
        this.feHast = '';
    }
  
    onReset() {
		this.idTipoCncl = 5;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.esDocu = '';          
        this.feDesd = '';
        this.feHast = ''; 
    }
}

export class FileNiubiz {
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;
    m: string;
    n: string;
    o: string;
    p: string;
    q: string;
    r: string;
    s: string;
    t: string;
    u: string;
    v: string;
    w: string;
    x: string;
}

export class RecaudacionSprl {
    tiProc: string;
    idCncl: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    feCncl: string;
    noAdju0001: string;
    noAdju0002: string;
    noAdju0003: string;
    idGuidDocu01: string;
    idGuidDocu02: string;
    idGuidDocu03: string;
    trama01: IFileNiubiz[];

    constructor() {
        this.tiProc = '';
        this.idCncl = 0;
        this.idTipoCncl = 5;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.feCncl = '';
        this.noAdju0001 = '';
        this.noAdju0002 = '';
        this.noAdju0003 = '';
        this.idGuidDocu01 = '';
        this.idGuidDocu02 = '';
        this.idGuidDocu03 = '';
        this.trama01 = [];
    }
}

export class ConciliacionManual {
	idCncl: number;
    nuSecu: number;
    feCncl: string;
    idUsua: string;
    razSoc: string;
    primerApellido: string;
    segundoApellido: string;
    nombres: string;	
    monto: number;
    idPagoLinea: number;
    eTicket: string;
    inCncl: string;
    obCnclSprl: string;

    constructor() {
        this.nuSecu = null;
        this.feCncl = '';
        this.idUsua = '';
        this.razSoc = '';
        this.primerApellido = '';
        this.segundoApellido = '';
        this.nombres = '';
        this.monto = null;
        this.idPagoLinea = null;
        this.eTicket = '';
        this.inCncl = '';
        this.obCnclSprl = '';
    }
} 

export class ConciliacionManual2 {
    idCncl: number;
    nuSecu: number;
    feCncl: string;
    idUsua: string;
    razSoc: string;
    primerApellido: string;
    segundoApellido: string;
    nombres: string;	
    monto: number;
    idPagoLinea: number;
    eTicket: string;
    inCncl: string;
    obCnclSprl: string;

    constructor() {
        this.nuSecu = null;
        this.feCncl = '';
        this.idUsua = '';
        this.razSoc = '';
        this.primerApellido = '';
        this.segundoApellido = '';
        this.nombres = '';
        this.monto = null;
        this.idPagoLinea = null;
        this.eTicket = '';
        this.inCncl = '';
        this.obCnclSprl = '';
    }
}

export class ConciliacionNIUBIZ {
    idCncl: number;
    nuFila: number;
    nuRuc: string;
    noRazoSoci: string;
    coCome: string;
    noCome: string;
    feOper: string;
    feDepo: string;
    noProd: string;
    noTipoOper: string;
    nuTarj: string;
    inOrigTarj: string;
    deTipoTarj: string;
    noMarcTarj: string;
    noMone: string;
    imOper: number;
    deEsDcc: string;
    imDcc: number;
    imComiTota: number;
    imComiNiub: number;
    im_igv: number;
    imSumaDepo: number;
    deEsta: string;
    idOper: string;
    coCuenBancPaga: string;
    noBancPaga: string;
    nuVouc: string;
    nuAuth: string;
    inCncl: string;
    deEstaCncl: string;
    obCnclNiub: string;

    constructor() {
        this.idCncl = null;
        this.nuFila = null;
        this.nuRuc = '';
        this.noRazoSoci = '';
        this.coCome = '';
        this.noCome = '';
        this.feOper = '';
        this.feDepo = '';
        this.noProd = '';
        this.noTipoOper = '';
        this.nuTarj = '';
        this.inOrigTarj = '';
        this.deTipoTarj = '';
        this.noMarcTarj = '';
        this.noMone = '';
        this.imOper = null;
        this.deEsDcc = '';
        this.imDcc = null;
        this.imComiTota = null;
        this.imComiNiub = null;
        this.im_igv = null;
        this.imSumaDepo = null;
        this.deEsta = '';
        this.idOper = '';
        this.coCuenBancPaga = '';
        this.noBancPaga = '';
        this.nuVouc = '';
        this.nuAuth = '';
        this.inCncl = '';
        this.deEstaCncl = '';
        this.obCnclNiub = '';    
    }
}

export class RecaudacionSprl2 {
    trama: string[];
    constructor() {
        this.trama = [];
    }
}