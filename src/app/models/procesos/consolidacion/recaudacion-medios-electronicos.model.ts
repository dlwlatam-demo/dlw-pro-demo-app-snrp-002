import { IFileAmex, IFileDiners, IFileNiubiz } from "src/app/interfaces/consolidacion";

export class FiltroRecaudacionME {
    coZonaRegi: string;
	coOficRegi: string;
	coLocaAten: string;	
    esDocu: string;
	feDesd: string;
	feHast: string;
    idTipoCncl: number;
    idOperPos: number;   	

    constructor() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.esDocu = '';        
        this.feDesd = '';
        this.feHast = '';
        this.idTipoCncl = 1;
        this.idOperPos = 0;
    }
  
    onReset() {
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.esDocu = '';          
        this.feDesd = '';
        this.feHast = '';
        this.idTipoCncl = 1;
        this.idOperPos = 0;
    }
}

export class FileNiubiz {
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;
    m: string;
    n: string;
    o: string;
    p: string;
    q: string;
    r: string;
    s: string;
    t: string;
    u: string;
    v: string;
    w: string;
    x: string;
    y: string;
    z: string;
}

export class FileDiners { 
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;   
}

export class FileAmex { 
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;
    m: string;
    n: string;    
}

export class RecaudacionMediosElectronicos {
    tiProc: string;
    idCncl: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    feCncl: string;
    noAdju0001: string;
    noAdju0002: string;
    noAdju0003: string;
    idGuidDocu01: string;
    idGuidDocu02: string;
    idGuidDocu03: string;
    trama01: IFileNiubiz[];
    trama02: IFileDiners[];
    trama03: IFileAmex[];

    constructor() {
        this.tiProc = '';
        this.idCncl = 0;
        this.idTipoCncl = 0;
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.idOperPos = 0;
        this.feCncl = '';
        this.noAdju0001 = '';
        this.noAdju0002 = '';
        this.noAdju0003 = '';
        this.idGuidDocu01 = '';
        this.idGuidDocu02 = '';
        this.idGuidDocu03 = '';
        this.trama01 = [];
        this.trama02 = [];
        this.trama03 = [];
    }
}

export class ConciliacionPorMontoDetalle {
    nuFila: number;
    servicioDePago: string;
    idOper: string;
    feOper: string;
    imSumaDepo: number;
    imOper: number;
    deEstaCncl: string;

    constructor() {
        this.nuFila = null;
        this.servicioDePago = '';
        this.idOper = '';
        this.feOper = '';
        this.imSumaDepo = null;
        this.imOper = null;
        this.deEstaCncl = '';
    }
} 

export class ConciliacionPorMonto {
    idCncl: number;
    nuFila: string;
    trama: TramaConciliacionPorMonto[];
}

export class TramaConciliacionPorMonto {
    nuSecu: string;
    moPago: number;
}

export class ConciliacionMovimiento {
    idCnclMovi: number;
    idCncl: number;
    tiMovi: string;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    obMovi: string;
    imReca: string;
    imComi: string;
    imIgv: string;
    imAlquEqui: string;
    imDeudAnte: string;
    imDeudSigu: string;
    imTotal: string
    inRegi: string;

    constructor() {
        this.idCnclMovi = null;
        this.idCncl = null;
        this.tiMovi = '';
        this.coZonaRegi = '';
        this.coOficRegi = '';
        this.coLocaAten = '';
        this.obMovi = '';
        this.imReca = '';
        this.imComi = '';
        this.imIgv = '';
        this.imAlquEqui = '';
        this.imDeudAnte = '';
        this.imDeudSigu = '';
        this.imTotal = '';
        this.inRegi = '';
    }
}

export class RecaudacionMediosElectronicos2 {
    trama: string[];
    constructor() {
        this.trama = [];
    }
}