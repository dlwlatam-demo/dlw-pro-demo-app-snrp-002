import { ConfigurarDocumento } from '../models/mantenimientos/configuracion/config-documento.model';

export interface IBandejaProcess {
    idMemo: number,
    proceso: string
}

export interface IBandejaConfiguracion {
    configDocu: ConfigurarDocumento,
    tipoDocu: string,
    proceso: string
}