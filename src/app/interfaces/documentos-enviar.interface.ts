import { BodyDocumentosEnviar } from "../models/mantenimientos/documentos/documentos-enviar.models";
import { Local, OficinaRegistral, ZonaRegistral } from "./combos.interface";
import { IResponse2 } from "./general.interface";

export interface IDocumentosEnviar {
    idDocuEnvi: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    tiProc: string;
    nuDocu: string;
    feDocu: string;
    noDocu: string;
    noRutaDocu: string;
    obDocu: string;
    idDocuFirm: number;
    idGuidDocu: string;
    esDocu: string;
    noUsuaCrea: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    deEsta: string;
    deTipoProc: string;
}

export interface DocumentosEnviarBandeja {
    lstEnviarDocumentos?: IDocumentosEnviar[];
    reporteExcel: string;
}

export interface IHistorialFirmasDocumentosEnviar {
    idDocuFirmUsu: number;
    nuSecuFirm: number;
    coPerf: number;
    noPerf: string;
    coUsuaCrea: number;
    noUsua: string;
    feCrea: string;
    deEsta: string;
}

export interface IDataFiltrosDocumentosEnviar {
    listaZonaRegistral: ZonaRegistral[];  
    listaOficinaRegistral: OficinaRegistral[];
    listaLocal: Local[];
    listaTipoDocumento: IResponse2[];
    listaEstadoRecibo: IResponse2[];
    bodyDocumentosEnviar: BodyDocumentosEnviar; 
    esBusqueda: boolean; 
    esCancelar: boolean
}

export interface IDocumentosEnviarSustento {
    idDocuEnviSust: number,
    idDocuEnvi: number,
    deTipoProc: string,
    feDocu: string,
    noDocuSust: string,
    noRutaSust: string,
    idGuidDocu: string,
    coUsuaCrea: number,
    coUsuaModi: number
}

export interface IDocuEnviarSustento {
    idDocuEnviSust: number | string;
    noDocuSust: string;
    noRutaSust?: string;
    idGuidDocu: string;
}