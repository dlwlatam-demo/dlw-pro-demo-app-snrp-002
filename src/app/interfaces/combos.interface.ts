export interface Documento {
    coTipoDocu?: string;
    deTipoDocu?: string;
}

export interface Estado {
    coEstado?: string;
    deEstado?: string;
}

export interface Banco {
    nidInstitucion?: number;
    cdeInst?: string;
}

export interface Cuenta {
    coCuenta?: string;
    deCuenta?: string;
}

export interface ZonaRegistral {
    coZonaRegi?: string;
    deZonaRegi?: string;
    deRuc?: string;
    deRazSoc?: string;
    abZonaRegi?: string;
}

export interface OficinaRegistral {
    coZonaRegi?: string;
    coOficRegi?: string;
    deOficRegi?: string;
}

export interface Local {
    coZonaRegi?: string;
    coOficRegi?: string;
    coLocaAten?: string;
    deLocaAten?: string;
    locaDefe?: string;
}

export interface Caja {
    coCaja?: string;
    deCaja?: string;
}

export interface MesAnio {
    coMes?: string;
    deMes?: string;
}