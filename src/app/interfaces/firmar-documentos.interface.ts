import { ZonaRegistral, OficinaRegistral, Local } from './combos.interface';
import { IResponse2 } from './general.interface';
import { BodyDocumentoFirma } from '../models/mantenimientos/documentos/documento-firma.model';
import { IPerfil } from 'src/app/interfaces/perfiles.interface';
export interface FirmarDocumentosValue {
    idDocuFirm: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    tiDocu: string;
    idDocuOrig: number;
    nuDocuOrig: string;
    feDocu: string;
    noDocuPdf: string;
    noRutaDocuPdf: string;
    idGuidDocu: string;
    nuSecuFirm: number;
    coPerf: number;
    esDocu: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: null,
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    noPerf: string;
    deTipoDocu: string;
    noUsuaCrea: string;
    deEsta: string;
}

export interface IFirmarDocumentos {
    lstDocumentoFirma: FirmarDocumentosValue[];
    reporteExcel: string;
}

export interface IHistorialFirmas {
    idDocuFirmUsu: number;
    nuSecuFirm: number;
    coPerf: number;
    noPerf: string;
    coUsuaCrea: number;
    noUsua: string;
    feCrea: string;
    deEsta: string;
}

export interface IDocumentoSustento {
    idDocuSust: number,
    idDocu: number,
    noDocuSust: string,
    noRutaSust: string,
    idGuidDocu: string,
    inRegi: string,
    coUsuaCrea: number,
    feCrea: string,
    coUsuaModi: number,
    feModi: string,
    nuDocu: string,
    feDocu: string,
    imDocu: number,
    nuSiaf: number
}

export interface IDataFiltrosDocumentoFirma {
    listaZonaRegistral: ZonaRegistral[];  
    listaOficinaRegistral: OficinaRegistral[];
    listaLocal: Local[];
    listaTipoDocumento: IResponse2[];
    listaPerfil: IPerfil[];
    listaEstado: IResponse2[];
    bodyDocumentoFirma: BodyDocumentoFirma; 
    esBusqueda: boolean; 
    esCancelar: boolean
}