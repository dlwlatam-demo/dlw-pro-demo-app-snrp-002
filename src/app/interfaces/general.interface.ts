export interface IResponse{
    codResult: number;
    msgResult: null
}

export interface IResponse2{
    ccodigoHijo: string;
    cdescri: string
}

export interface IResponse3{
    ccodigoHijo: number;
    cdescri: string
}

export interface IPersona {
    type?: string;
    dni: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    nombres: string;
    fechaNacimiento?: string;
    fechaEmision?: string;
}

export interface IParametros {
    idParm: number,
    deParm: string,
    noValo1: string,
    noValo2: string,
    noValo3: string,
    noValo4: string,
    inRegi: string,
    coUsuaCrea: string,
    feCrea: string,
    coUsuaModi: string,
    feModi: string
}