export interface IReciboSustento {
    idReciIngrSust: number,
    idReciIngr: number,
    noDocuSust: string,
    noRutaSust: string,
    idGuidDocu: string,
    inRegi: string,
    coUsuaCrea: string,
    feCrea: string,
    coUsuaModi: string,
    feModi: string,
    nuReciIngr: string,
    feReciIngr: string,
    imReciIngr: number,
    nuSiaf: number
}

export interface IBeneRecibo {
    idReciIngrBene: number,
    noBene: string,
    deEsta: string
}

export interface IListBeneRecibo {
    lstReciboIngresoBene: IBeneRecibo[],
    reporteExcel: null,
    reportePdf: null
}