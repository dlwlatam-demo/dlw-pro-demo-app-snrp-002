export interface IZonaRegistral {
    coZonaRegi?: string;
    deZonaRegi?: string;
    deRuc?: string;
    deRazSoc?: string;
    abZonaRegi?: string;
    inEstd?: string;
}

export interface IOficinaRegistral {
    coZonaRegi?: string;
    coOficRegi?: string;
    deOficRegi?: string;
    inEstd?: string;
}

export interface ILocalAtencion {
    coZonaRegi?: string;
    coOficRegi?: string;
    coLocaAten?: string;
    deLocaAten?: string;
    inEstd?: string;
}