import { FiltroRecaudacionScunacRemesas } from "../models/procesos/consolidacion/recaudacion-scunac-remesas.model";
import { Local, OficinaRegistral, ZonaRegistral } from "./combos.interface";
import { IResponse2 } from "./general.interface";

export interface IFileNiubiz { 
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;
    m: string;
    n: string;
    o: string;
    p: string;
    q: string;
    r: string;
    s: string;
    t: string;
    u: string;
    v: string;
    w: string;
    x: string;
}

export interface IDataFileExcel {
    servicioDePago: string;
    nameFile: string;
    idGuid?: string;
    lista: any[];
}

export interface IRecaudacionScunacRemesas {
    idCncl: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    nuCncl: string;
    feCncl: string;
    imCncl: number;
    noAdju0001: string;
    noAdju0002: string;
    noAdju0003: string;
    idGuidDocu01: string;
    idGuidDocu02: string;
    idGuidDocu03: string;
    esDocu: string;
    noUsuaCrea: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: null;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    noOperPos: string;
    deEsta: string;
}

export interface IConciliacionManual {    
	idCncl: number;
    nuSecu: number;
    feCncl: string;
    idUsua: string;
    razSoc: string;
    primerApellido: string;
    segundoApellido: string;
    nombres: string;	
    monto: number;
    idPagoLinea: number;
    eTicket: string;
    inCncl: string;
    obCnclScunacRemesas: string;

}

export interface IConciliacionManual2 {
    idCncl: number;
    nuSecu: number;
    coZonaRegi: string;
    coOficRegi: string;
    idUsua: string;
    razSoc: string;
    primerApellido: string;
    segundoApellido: string;
    nombres: string;
    monto: number;
    idPagoLinea: number;
    eticket: string;
    tsModi: string;
    tipoAbono: string;
    tipoUsr: string;
    inCncl: string;
	nuFila: number;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;
    feCncl: string;
    obCnclScunacRemesas: string;
	
}

export interface IResConciliacionManual2 {
    operacionNiubiz: IConciliacionManual2;
    codResult: number;
    msgResult: string;
}

export interface ITotalesNiubizScunacRemesas {
    idCnclMovi: number;
    tiMovi: string;
    deMovi: string;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    obMovi: string;
    imReca: number;
    imComi: number;
    imIgv: number;
    imAlquEqui: number;    
    imDeudAnte: number;
    imDeudSigu: number,
    imTotal: number;
    inRegi: string;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    deEsta: string
}

export interface IDataTransacScunacRemesas {
    lstConciliacionSPRL: any[];
    reporteExcel: any;
}
export interface ITotalPorOficina {
    noOperPos: string,
    feCncl: string,
    deLocaAten: string,
    imSumaDepo: number
}

export interface IDataFiltrosRecaudacionScunacRemesas {       
    listaZonaRegistral: ZonaRegistral[];  
    listaOficinaRegistral: OficinaRegistral[];
    listaLocal: Local[];
    listaEstado: IResponse2[];   
    bodyRecaudacionScunacRemesas: FiltroRecaudacionScunacRemesas; 
    esBusqueda: boolean; 
    esCancelar: boolean;
};

export interface IOperacionScunac {
    item: number,
    tipoPago: string,
    operador: string,
    monto: 42,
    id: string,
    ap: string,
    terminal: string,
    anioMovimiento: string,
    numeroMovimiento: number,
    caja: number,
    deLocaAten: string,
    fecha: string,
    montoTotal: number,
    coZonaRegi: string,
    nuSecuDeta: number
}

export interface IDetalleOperacionScunac {
    aaMvto: string,
    coZonaRegi: string,
    nuMvto: number,
    nuSecuDeta: number,
    idTransPos: string
}

export interface IPostOperacionScunac {
    trama: IDetalleOperacionScunac[]
}

export interface IOtrosOperHermes {
    idOperHermOtro: number,
    idCncl: number,
    feOper: string,
    coZonaRegi: string,
    deZonaRegi: string,
    coOficRegi: string,
    deOficRegi: string,
    coLocaAten: string,
    deLocaAten: string,
    imOper: number,
    obHerm: string,
    inRegi: string,
    coUsuaCrea: string,
    feCrea: string,
    coUsuaModi: string,
    feModi: string,
    deEsta: string
}

export interface IListOtrosHermes {
    lista: IOtrosOperHermes[],
    reporteExcel: null
}