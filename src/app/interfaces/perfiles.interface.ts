export interface IPerfil {
    coPerf: number;
    noPerf: string;
    inEstd?: string;
    deEstd?: string;
    inTipoPerf?: string;
    deTipoPerf?: string;
    coSist?: number;
    deSist?: string;
    caUsua?: number;
}

export interface IDocumentoEstado {
    ccodigoMaster: string,
    ccodigoHijo: string,
    inPadr: string,
    coSel: string,
    cdescri: string
}