import { BodyComprobantePago } from "../models/mantenimientos/documentos/comprobante-pago.model";
import { ReciboIngreso } from "../models/mantenimientos/documentos/recibo-ingreso.model";
import { Local, OficinaRegistral, ZonaRegistral } from "./combos.interface";
import { IResponse2 } from "./general.interface";

export interface ComprobantePagoValue {
    idCompPago: number,
    coZonaRegi: string,
    coOficRegi: string,
    coLocaAten: string,
    idTipoCompPago: number,
    nuCompPago: number,
    feCompPago: string,
    imCompPago: number,
    idBanc: number,
    nuCuenBanc: string,
    nuSiaf: number,
    obConc: string,
    esDocu: string,
    noDocuPdf: string,
    noRutaDocuPdf: string,
    idGuidDocu: string,
    nuReciHono: string,
    feReciHono: string,
    inReciHonoRete: string,
    idDocuFirm: number,
    idCompPagoCopy: number,
    inCompRese: string,
    inCompNout: string,
    noUsuaCrea: string,
    noUsuaModi: string,
    inRegi: string,
    coUsuaCrea: number,
    feCrea: string,
    coUsuaModi: number,
    feModi: string,
    deZonaRegi: string,
    deOficRegi: string,
    deLocaAten: string,
    noTipoCompPago: string,
    deEsta: string,
    noCuenBanc: string,
    noBanc: string,
    inSustRegi: string,
    inCompPagoSiaf: string,
    inConsPago: string,
    inDocuAuto: string,
    coUsuaAuto: number,
    feDocuAuto: string,
    noUsuaAuto: string,
    idBene: number,
    nuBene: string,
    noBene: string,
    idTipoOper: number,
    noTipoOper: string,
    nuMediPago: string,
    noMediPago: string,
    cuentasContables: null,
    cuentasClasificador: null,
    detallePatrimonial: null,
    documentos: null
}

export interface IComprobantePago {
    lstComprobantePago: ComprobantePagoValue[];
    reporteExcel: string;
}

export interface IVerEditarComprobantePago {
    comprobantePago: ComprobantePagoValue[];
    isEditar: boolean;
}

export interface IDataFiltrosRecibo {
    listaZonaRegistral: ZonaRegistral[];  
    listaOficinaRegistral: OficinaRegistral[];
    listaLocal: Local[];
    listaTipoRecibo: ReciboIngreso[];
    listaEstadoRecibo: IResponse2[];
    listaMediosPago: IResponse2[];
    bodyRecibo: BodyComprobantePago; 
    esBusqueda: boolean; 
    esCancelar: boolean
}

export interface IComprobantePagoById {
    idCompPago: number,
    coZonaRegi: string,
    coOficRegi: string,
    coLocaAten: string,
    idTipoCompPago: number,
    nuCompPago: number,
    feCompPago: string,
    imCompPago: number,
    idBanc: number,
    nuCuenBanc: string,
    nuSiaf: number,
    obConc: string,
    esDocu: string,
    noDocuPdf: string,
    noRutaDocuPdf: string,
    idGuidDocu: string,
    nuReciHono: string,
    feReciHono: string,
    inReciHonoRete: string,
    idDocuFirm: number,
    idCompPagoCopy: number,
    inCompRese: string,
    inCompNout: string,
    noUsuaCrea: string,
    noUsuaModi: string,
    inRegi: string,
    coUsuaCrea: number,
    feCrea: string,
    coUsuaModi: number,
    feModi: string,
    deZonaRegi: string,
    deOficRegi: string,
    deLocaAten: string,
    noTipoCompPago: string,
    deEsta: string,
    noCuenBanc: string,
    noBanc: string,
    inSustRegi: string,
    inCompPagoSiaf: string,
    inConsPago: string,
    inDocuAuto: string,
    coUsuaAuto: number,
    feDocuAuto: string,
    noUsuaAuto: string,
    idBene: number,
    nuBene: string,
    noBene: string,
    idTipoOper: number,
    noTipoOper: string,
    cuentasContables: ICuentasContables[];
    cuentasClasificador: ICuentasClasificador[];
    documentos: IMediosPago[];
}

export interface IComprobanteSustento {
    idCompPagoSust: number,
    idCompPago: number,
    noDocuSust: string,
    noRutaSust: string,
    idGuidDocu: string,
    inCompPagoSiaf: string,
    inConsPago: string,
    inSustOtro: string,
    inRegi: string,
    coUsuaCrea: number,
    feCrea: string,
    coUsuaModi: number,
    feModi: string,
    nuCompPago: string,
    feCompPago: string,
    imCompPago: number,
    nuOrde: number,
    nuSiaf: number
}

export interface ICuentasClasificador {
    idCompPagoClsf: number,
    coClsf: string,
    deClsf: string,
    imDebe: string,
    imHabe: string
}

export interface ICuentasContables {
    idCompPagoCont: number;
    coCuenCont: string;
    noCuenCont: string;
    tiCuenCont: string;
    imDebe: number;
    imHabe: number;
}

export interface IMediosPago {
    idCompPagoDocu: number;
    tiDocu: string;
    noDocu: string;
    caDocu: string;
    nuDocu: string;
    imDocu: string;
}

export interface ITipoRecibo {
    idTipo: number;
    noTipo: string;
    idBanc?: number;
    nuCuenBanc?: string;
    obConce?: string;
    noBanc?: string;
}

export interface IRazonSocial {
    nuRuc: string;
    noRazoSoci: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;
}

export interface IBeneficiarioBandeja {
    idBene: number,
    nuBene: string,
    noBene: string,
    deEsta: string
}

export interface ITipoOperacionBandeja {
    idTipoOper: number,
    noTipoOper: string,
    deEsta: string
}