export interface IClasificador {
    coClsf?: string,
    deClsf?: string,
    deDetaClsf?: string,
    inRegi?: string,
    coUsuaCrea?: number,
    feCrea?: string,
    coUsuaModi?: number
}