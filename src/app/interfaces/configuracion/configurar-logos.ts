export interface IDataLogo {
    posicionDeLogo: string;
    nameFile: string;
    idGuid?: string;
    imgReader?: any;
}

export interface DataConfigLogo {
    tiDocu: string,
    inPagiUbic: string,
    inPagiAlin: string,
    noImag: string,
    idGuidDocu: string,
    data?: null
}

export interface IConfigLogo {
    listaDatosImagenes: DataConfigLogo[];
}