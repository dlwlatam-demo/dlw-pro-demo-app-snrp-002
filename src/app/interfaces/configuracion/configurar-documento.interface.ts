import { BodyConfiguracion, ConfigurarDocumento } from '../../models/mantenimientos/configuracion/config-documento.model';

export interface IConfigDocumento {
    listTipoDocumento: ConfigurarDocumento[];
    listTipoDocumentoClsf: TipoClasificadores[];
    listTipoDocumentoCont: TipoCuentaContable[];
}

export interface TipoCuentaContable {
    id: number,
    coCuenCont: string,
    tiDebeHabe: string,
    idBanc: number,
    idTipoCont: number,
    idTipo: number,
    noCuenCont: string
}

export interface TipoClasificadores {
    coClsf: string,
    idTipoClsf: number,
    idTipo: number,
    deClsf: string
}

export interface IDataFiltrosConfiguracion {
    bodyConfig: BodyConfiguracion;
    esBusqueda: boolean;
    esCancelar: boolean;
}