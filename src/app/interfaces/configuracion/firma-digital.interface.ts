export interface IFirmaDigital {
    tiDocu: string;
    deDocu: string;
    idConfFirm1: number;
    firmaNro1: string;
    idConfFirm2: number;
    firmaNro2: string;
    idConfFirm3: number;
    firmaNro3: string;
    idConfFirm4: number;
    firmaNro4: string;
}

export interface IFirmaPerfil {
    idConfFirmUsua: number,
    idConfFirm: number,
    tiDocu: string,
    deDocu: string,
    coPerf: number,
    noPerf: string,
    coUsua: number,
    noUsua: string,
    feDesd: string,
    feHast: string,
    inRegi: string,
    inLice: string,
    noLica: string,
    coUsuaCrea: number,
    feCrea: string,
    coUsuaModi: number,
    feModi: string
}

export interface IPerfilDocumento {
    idConfFirm: number,
    tiDocu: string,
    nuSecuFirm: number,
    coPerf: number,
    noPerf: string
}

export interface ITramaFirma {
    idConfFirmUsua: string,
    coUsua: string,
    feDesd: string,
    feHast: string,
    inLice: string,
    coLice: string
}

