import { BodyBandejaTipoCarta } from '../../models/mantenimientos/configuracion/tipo-carta-orden.model';

export interface ITipoCartaOrden {
    idTipoCartOrde?: number,
    noTipoCartOrde?: string,
    noDest?: string,
    noCarg?: string,
    idBancCarg?: number,
    nuCuenBancCarg?: string,
    idBancAbon?: number,
    nuCuenBancAbon?: string,
    deTitu?: string,
    deSiglOfic?: string,
    coGrup?: string,
    noGrup?: string,
    deSeccInic?: string,
    deSeccConc?: string,
    deSeccFina?: string,
    noDece?: null,
    noAnio?: null,
    inRegi?: string,
    coUsuaCrea?: number,
    feCrea?: string,
    coUsuaModi?: number,
    feModi?: string,
    noBancCarg?: string,
    noCuenBancCarg?: string,
    noBancAbon?: string,
    noCuenBancAbon?: string,
    deEsta?: string,
    noDocu?: string,
}

export interface IDataFiltrosTipoCartaOrden {
    bodyRecibo: BodyBandejaTipoCarta;
    esBusqueda: boolean;
    esCancelar: boolean;
}

export interface ITipoCartaVariables {
    deCamp: string,
    noCamp: string
}

export interface ITipoCartaById {
    idTipoCartOrde: number,
    noTipoCartOrde: string,
    noDest: string,
    noCarg: string,
    idBancCarg: number,
    nuCuenBancCarg: string,
    idBancAbon: number,
    nuCuenBancAbon: string,
    deTitu: string,
    deSiglOfic: string,
    coGrup: string,
    noGrup: string,
    deSeccInic: string,
    deSeccConc: string,
    deSeccFina: string,
    noDece: string,
    noAnio: string,
    inRegi: string,
    coUsuaCrea: number,
    feCrea: string,
    coUsuaModi: number,
    feModi: string,
    noBancCarg: string,
    noCuenBancCarg: string,
    noBancAbon: string,
    noCuenBancAbon: string,
    deEsta: string,
    noDocu: string,
    liVariDocu: ITipoCartaVariables[]
}