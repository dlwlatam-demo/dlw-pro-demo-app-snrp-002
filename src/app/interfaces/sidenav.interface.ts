import { animate, style, transition, trigger } from "@angular/animations";

export interface SideNavToggle {
    screeWidth: number;
    collapsed: boolean;
}

export interface INavbarData {
  routerLink?: string;
  icon?: string;
  label: string;
  codigo?: number;
  expanded?: boolean;
  items?: INavbarData[];
}

export interface Perfil {
  label: string;
  codigo?: string;
}

export const fadeInOut = trigger('fadeInOut', [
  transition(':enter', [
    style({ opacity: 0 }),
    animate('350ms',
      style({ opacity: 1 })
    )
  ]),
  transition(':leave', [
    style({ opacity: 1 }),
    animate('350ms',
      style({ opacity: 0 })
    )
  ])
])