import { FiltroRecaudacionSiafMef } from "../models/procesos/bancaria/recaudacion-siaf-mef.model";
import { Local, OficinaRegistral, ZonaRegistral } from "./combos.interface";
import { IResponse2 } from "./general.interface";

export interface IFileNiubiz { 
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;
    m: string;
    n: string;
    o: string;
    p: string;
    q: string;
    r: string;
    s: string;
    t: string;
    u: string;
    v: string;
    w: string;
    x: string;
}

export interface IDataFileExcel {
    servicioDePago: string;
    nameFile: string;
    idGuid?: string;
    lista: any[];
}

export interface IRecaudacionSiafMef {
    idCnclSiaf: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    nuCncl: string;
    feCnclDesd: string;
    feCnclHast: string;
    imCncl: number;
    noAdju0001: string;
    noAdju0002: string;
    noAdju0003: string;
    idGuidDocu01: string;
    idGuidDocu02: string;
    idGuidDocu03: string;
    esDocu: string;
    inCncl: string;
    noUsuaCrea: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: null;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    noOperPos: string;
    deEsta: string;
}

export interface IConciliacionManual {    
	idCnclSiaf: number;
    nuSecu: number;
    coClav: string;
    nuExpe: string;	
    coCiclo: string;
    feDocu: string;	
    coDocu: string;
    nuDocu: string;
    noRazo: string;
    imDebe: number;
    imHabe: number;
    imSaldo: number;	
    deMone: string;
    coEsta: string;
    inCncl: string;
    nuAnoEnto: string;	
    nuEjecSec: string;
	tiOper: string;
    nuOrdeCicl: string;	
    inRegi: string;
    obCncl: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;	

}

export interface IConciliacionManual2 {
    idCnclSiaf: number;
    nuFila: number;
    feBanc: string;
    deTran: string;
    coTran: string;	
    imCarg: number;
    imAbon: number;
    inCncl: string;
    inRegi: string;
    obCncl: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;	
	
}

export interface IResConciliacionManual2 {
    operacionNiubiz: IConciliacionManual2;
    codResult: number;
    msgResult: string;
}

export interface ITotalesNiubizSiafMef {
	fecha: string;
	libroBanco: string;
	estadoBancario: string;
	libroBancoNoConc: string;
	estadoBancarioNoConc: string;
	libroBancoAnulado: string;
	estadoBancarioExtorno: string;
	estadoBancarioNoIden: string;
	estadoBancarioMesAnt: string;	

}

export interface IDataTransacSiafMef {
    lstConciliacionSPRL: any[];
    reporteExcel: any;
}
export interface ITotalPorOficina {
    noOperPos: string,
    feCncl: string,
    deLocaAten: string,
    imSumaDepo: number
}

export interface IDataFiltrosRecaudacionSiafMef {       
    listaZonaRegistral: ZonaRegistral[];  
    listaOficinaRegistral: OficinaRegistral[];
    listaLocal: Local[];
    listaOperador: IResponse2[];
    listaEstado: IResponse2[];   
    bodyRecaudacionSiafMef: FiltroRecaudacionSiafMef; 
    esBusqueda: boolean; 
    esCancelar: boolean;
};

export interface IOperacionScunac {
    item: number,
    tipoPago: string,
    operador: string,
    monto: 42,
    id: string,
    ap: string,
    terminal: string,
    anioMovimiento: string,
    numeroMovimiento: number,
    caja: number,
    deLocaAten: string,
    fecha: string,
    montoTotal: number,
    coZonaRegi: string,
    nuSecuDeta: number
}

export interface IDetalleOperacionScunac {
    aaMvto: string,
    coZonaRegi: string,
    nuMvto: number,
    nuSecuDeta: number,
    idTransPos: string
}

export interface IPostOperacionScunac {
    trama: IDetalleOperacionScunac[]
}