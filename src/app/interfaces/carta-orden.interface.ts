import { ITipoCartaOrden, ITipoCartaVariables } from './configuracion/tipo-carta-orden.interface';
import { BodyCartaOrden } from "../models/mantenimientos/documentos/carta-orden.model";
import { Local, OficinaRegistral, ZonaRegistral } from "./combos.interface";
import { IResponse2 } from "./general.interface";

export interface ICartaOrden {
    idCartOrde: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idTipoCartOrde: number;
    nuCartOrde: string;
    feCartOrde: string;
    imCartOrde: number;
    nuSiaf: number;
    nuCompPago:  string;
    tiDocuRefe:  string;
    nuDocuRefe:  string;
    noDest:  string;
    noCarg:  string;
    idBancCarg: number;
    nuCuenBancCarg: string;
    idBancAbon: number;
    nuCuenBancAbon: string;
    esDocu: string;
    noDocuPdf:  string;
    noRutaDocuPdf:  string;
    idGuidDocu: string;
    idDocuFirm: number;
    noUsuaCrea: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    noTipoCartOrde: string;
    deEsta: string;
    noCuenBancCarg:  string;
    noBancCarg: string;
    noCuenBancAbon:  string;
    noBancAbon: string;
    coGrup: string;
    noGrup: string;
    deTitu: string;
    deSiglOfic: string;
    deSeccInic: string;
    deSeccConc: string;
    deSeccFina: string;
    noDece: null;
    noAnio: null;
    nuRuc: null;
    coCuenInteCarg: null;
    coCuenInteAbon: null
}

export interface IVerEditarCartaOrden {
    cartaOrden: ICartaOrden[];
    isEditar: boolean;
}

export interface INuevaCartaOrden {
    codResult: number;
    msgResult: null
}

export interface IDataFiltrosCartaOrden {
    listaZonaRegistral: ZonaRegistral[];  
    listaOficinaRegistral: OficinaRegistral[];
    listaLocal: Local[];
    listaTipoCartaOrden: ITipoCartaOrden[];
    listaEstadoCartaOrden: IResponse2[];
    listaGrupos: IResponse2[];
    bodyCartaOrden: BodyCartaOrden; 
    esBusqueda: boolean; 
    esCancelar: boolean
}

export interface ICartaOrdenById {
    idCartOrde: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idTipoCartOrde: number;
    nuCartOrde: string;
    feCartOrde: string;
    imCartOrde: number;
    nuSiaf: number;
    nuCompPago: string;
    tiDocuRefe: string;
    nuDocuRefe: string;
    noDest: string;
    noCarg: string;
    idBancCarg: number;
    nuCuenBancCarg: string;
    idBancAbon: number;
    nuCuenBancAbon: string;
    esDocu: string;
    noDocuPdf: string;
    noRutaDocuPdf: string;
    idGuidDocu: string;
    idDocuFirm: number;
    noUsuaCrea: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    noTipoCartOrde: string;
    deEsta: string;
    noCuenBancCarg: string;
    noBancCarg: string;
    noCuenBancAbon: string;
    noBancAbon: string;
    coGrup: string,
    noGrup: string,
    deTitu: string,
    deSiglOfic: string,
    deSeccInic: string,
    deSeccConc: string,
    deSeccFina: string,
    noDece: string,
    noAnio: string,
    nuRuc: string,
    coCuenInteCarg: string,
    coCuenInteAbon: string,
    liVariDocu: ITipoCartaVariables[]
}

export interface ICartaSustento {
    idCartOrdeSust: number,
    idCartOrde: number,
    noDocuSust: string,
    noRutaSust: string,
    idGuidDocu: string,
    inRegi: string,
    coUsuaCrea: number,
    feCrea: string,
    coUsuaModi: number,
    feModi: string,
    nuCartOrde: string,
    feCartOrde: string,
    imCartOrde: number,
    nuSiaf: number
}

export interface ICartaAnexo {
    idCartOrdeAnex: number,
    idCartOrde: number,
    noDocuAnex: string,
    noRutaAnex: string,
    idGuidDocu: string,
    inRegi: string,
    coUsuaCrea: number,
    feCrea: string,
    coUsuaModi: number,
    feModi: string,
    nuCartOrde: string,
    feCartOrde: string,
    imCartOrde: number,
    nuSiaf: number
}