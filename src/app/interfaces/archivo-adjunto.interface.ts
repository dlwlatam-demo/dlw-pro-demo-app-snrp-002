export interface IArchivoAdjunto {
    idCarga: string, 
    codResult: number, 
    msgResult: string
}

export interface IRowReorder {
    dragIndex: number,
    dropIndex: number
}

export interface IOnSelect {
    originalEvent: Event,
    files: FileList,
    currentFiles: File[]
}

export interface ICargaArchivo {
    idCarga: string,
    codResult: number,
    msgResult: string,
    archivoCarga: null,
    idDocu: number
}