export interface IUsuario {
    coUsua: number,
    idUsua: string,
    coEmpl: string,
    noEmpl: string,
    apPateEmpl: string,
    apMateEmpl: string,
    perfiles: null,
    correoUsua: string,
    feIngr: string,
    feBaja: string,
    feInaInic: null,
    feInaFina: null,
    coZonaRegi: string,
    coOficRegi: string,
    coLocaAten: string,
    coCaja: number,
    feCambClave: string,
    inEstd: string,
    inAsigCaja: string,
    inHuelDact: string,
    deServDomi: string,
    deUsuaDomi: string,
    tiDocuIden: string,
    nuDocuIden: string,
    coCarg: string,
    coCondEmpl: string,
    deZonaRegi: string,
    deOficRegi: string,
    deLocaAten: string,
    usuarioPerfiles: IUsuarioPerfil[],
    usuarioLocales: IUsuarioLocal[]
}

export interface IUsuarioPerfil {
    nuSecu: number,
    coPerf: number,
    noPerf: string,
    inCheck: string
}

export interface IUsuarioLocal {
    coUsua: number,
    coZonaRegi: string,
    coOficRegi: string,
    coLocaAten: string,
    deZonaRegi: string,
    deOficRegi: string,
    deLocaAten: string,
    inCheckDefa: string,
    inCheckSele: string,
    feInic: string,
    feFina: string,
    inEstd: string,
    deEsta: string
}

export interface IEmpleado {
    coEmpl: number,
    tiDocuIden: string,
    nuDocuIden: string,
    apPateEmpl: string,
    apMateEmpl: string,
    noEmpl: string,
    coCarg: string,
    inEstd: string,
    coCondEmpl: string
}

export interface IPerfilUsuario {
    coUsua: number,
    noUsua: string
}

export interface IUsuarioSistema {
    coUsua?: number;
    idUsua?: string;
    noUsua?: string;
}