export interface IPerfil {
    coPerf: number,
    noPerf: string,
    inEstd: string,
    deEstd: string,
    inTipoPerf: string,
    deTipoPerf: string,
    coSist: number,
    deSist: string,
    caUsua: number,
}

export interface IPerfilOption {
    coOpci: number,
    deOpci: string,
    tiOpci: string,
    nuOrde: number,
    coPadr: number,
    coSel: number
}