import { TipoMemorandum } from "../models/mantenimientos/configuracion/tipo-memo.model";
import { BodyMemorandum } from "../models/mantenimientos/documentos/memorandum.model";
import { Local, OficinaRegistral, ZonaRegistral } from "./combos.interface";
import { IResponse2 } from "./general.interface";

export interface IMemorandum {
    coLocaAten: string;
    coOficRegi: string;
    coUsuaCrea: number;
    coUsuaModi: number;
    coZonaRegi: string;
    deEsta: string;
    deLocaAten: string;    
    deOficRegi: string;
    deZonaRegi: string;
    feCrea: string;
    feDesdMemo: string;
    feHastMemo: string; 
    feMemo: string;   
    feModi: string;
    idBancDest: number;
    idBancOrig: number;
    idMemo: number;
    idTipoMemo: number;
    imMemo0001: number;
    imMemo0002: number;
    imMemo0003: number;
    imMemo0004: number;
    imMemo0005: number;
    imMemo0006: number;
    imTotal: number;   
    inRegi: string; 
    noAsun: string;
    noBancDest: string;
    noBancOrig: string;
    noCarg: string;
    noCuenBancDest: string;
    noCuenBancOrig: string;
    noDest: string;
    noRefe: string;
    noTipoMemo: string; 
    noUsuaCrea: string; 
    nuCuenBancDest: string; 
    nuCuenBancOrig: string;   
    nuMemo: string;
    nuSistran: string; 
}

export interface IMemorandumProcess {
    idMemo: number,
    proceso: string
}

export interface ICampoMemorandum {   
    titulo: string, 
    accion: string;  
};

export interface IDataFiltrosMemorandum {       
    listaZonaRegistral: ZonaRegistral[];  
    listaOficinaRegistral: OficinaRegistral[];
    listaLocal: Local[];
    listaTipoMemorandum: TipoMemorandum[];
    listaEstadoMemorandum: IResponse2[];
    bodyMemorandum: BodyMemorandum; 
    esBusqueda: boolean; 
    esCancelar: boolean;
};

export interface IMemorandumSustento {
    idMemoSust: number,
    idMemo: number,
    noDocuSust: string,
    noRutaSust: string,
    idGuidDocu: string,
    inRegi: string,
    coUsuaCrea: number,
    feCrea: string,
    coUsuaModi: number,
    feModi: string,
    nuMemo: string,
    feMemo: string
}

