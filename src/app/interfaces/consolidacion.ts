import { FiltroRecaudacionME } from "../models/procesos/consolidacion/recaudacion-medios-electronicos.model";
import { Local, OficinaRegistral, ZonaRegistral } from "./combos.interface";
import { IResponse2 } from "./general.interface";

export interface IFileNiubiz { 
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;
    m: string;
    n: string;
    o: string;
    p: string;
    q: string;
    r: string;
    s: string;
    t: string;
    u: string;
    v: string;
    w: string;
    x: string;
}

export interface IFileDiners { 
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;   
}

export interface IFileAmex { 
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;
    m: string;
    n: string;    
}

export interface IDataFileExcel {
    servicioDePago: string;
    nameFile: string;
    idGuid?: string;
    lista: any[];
}

export interface IRecaudacionMediosElectronicos {
    idCncl: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    nuCncl: string;
    feCncl: string;
    imCncl: number;
    noAdju0001: string;
    noAdju0002: string;
    noAdju0003: string;
    idGuidDocu01: string;
    idGuidDocu02: string;
    idGuidDocu03: string;
    esDocu: string;
    noUsuaCrea: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: null;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    noOperPos: string;
    deEsta: string;
}

export interface IConciliacionPorMonto {    
    idCncl: number;
    servicioDePago: string;
    item: number; 
    opcionOperacion: number;
}

export interface IConciliacionPorMonto2 {
    nuFila: number;
    nuRuc: string;
    noRazoSoci: string;
    coCome: string;
    noCome: string;
    feOper: string;
    feDepo: string;
    noProd: string;
    noTipoOper: string;
    nuTarj: string;
    inOrigTarj: string;
    deTipoTarj: string;
    noMarcTarj: string;
    noMone: string;
    imOper: number;
    deEsDcc: string;
    imDcc: number;
    imComiTota: number;
    imComiNiub: number;
    im_igv: number;
    imSumaDepo: number;
    deEsta: string;
    idOper: string;
    coCuenBancPaga: string;
    noBancPaga: string;
    nuVouc: string;
    nuAuth: string;
    inCncl: string;
    deEstaCncl: string;   
}

export interface IResConciliacionPorMonto2 {
    operacionNiubiz: IConciliacionPorMonto2;
    codResult: number;
    msgResult: string;
}

export interface IConciliacionPorMonto3 {
    idCncl: number;
    nuSecu: number;
    coZonaRegi: string;
    deZonaRegi: string;
    coOficRegi: string;
    deOficRegi: string;
    coLocaAten: string;
    deLocaAten: string;
    coCaja: number;
    idDiar: number;
    coTipoPago: string;
    deTipoPago: string;
    tiOperPos: string;
    deOperPos: string;
    moPago: number;
    idTranPos: string;
    idAproPos: string;
    nuTermPos: string;
    tsOper: string;
}

export interface IVariosEfectivoAnulacion {
    idCnclMovi: number;
    tiMovi: string;
    deMovi: string;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    obMovi: string;
    imReca: number;
    imComi: number;
    imIgv: number;
    imAlquEqui: number;    
    imDeudAnte: number;
    imDeudSigu: number,
    imTotal: number;
    inRegi: string;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    deEsta: string
}

export interface ITotalPorOficina {
    noOperPos: string,
    feCncl: string,
    deLocaAten: string,
    imSumaDepo: number
}

export interface IDataFiltrosRecaudacionME {       
    listaZonaRegistral: ZonaRegistral[];  
    listaOficinaRegistral: OficinaRegistral[];
    listaLocal: Local[];
    listaEstado: IResponse2[];   
    bodyRecaudacionME: FiltroRecaudacionME; 
    esBusqueda: boolean; 
    esCancelar: boolean;
};

export interface IOperacionScunac {
    item: number,
    tipoPago: string,
    operador: string,
    monto: 42,
    id: string,
    ap: string,
    terminal: string,
    anioMovimiento: string,
    numeroMovimiento: number,
    caja: number,
    deLocaAten: string,
    fecha: string,
    montoTotal: number,
    coZonaRegi: string,
    nuSecuDeta: number
}

export interface IDetalleOperacionScunac {
    aaMvto: string,
    coZonaRegi: string,
    nuMvto: number,
    nuSecuDeta: number,
    idTransPos: string
}

export interface IPostOperacionScunac {
    trama: IDetalleOperacionScunac[]
}