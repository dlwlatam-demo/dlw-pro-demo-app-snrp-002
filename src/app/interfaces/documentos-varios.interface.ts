import { BodyDocumentosVarios } from "../models/mantenimientos/documentos/documentos-varios.models";
import { Local, OficinaRegistral, ZonaRegistral } from "./combos.interface";
import { IResponse2 } from "./general.interface";

export interface IDocumentosVarios {
    idDocuVari: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    tiDocu: string;
    nuDocu: string;
    feDocu: string;
    noDocuPdf: string;
    noRutaDocuPdf: string;
    obDocu: string;
    idDocuFirm: number;
    idGuidDocu: string;
    esDocu: string;
    noUsuaCrea: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    deEsta: string;
    deTipoDocu: string;
}

export interface DocumentosVariosBandeja {
    lstDocumentoVarios?: IDocumentosVarios[];
    reporteExcel: string;
}

export interface IHistorialFirmasDocumentosVarios {
    idDocuFirmUsu: number;
    nuSecuFirm: number;
    coPerf: number;
    noPerf: string;
    coUsuaCrea: number;
    noUsua: string;
    feCrea: string;
    deEsta: string;
}

export interface IDataFiltrosDocumentosVarios {
    listaZonaRegistral: ZonaRegistral[];  
    listaOficinaRegistral: OficinaRegistral[];
    listaLocal: Local[];
    listaTipoDocumento: IResponse2[];
    listaEstadoRecibo: IResponse2[];
    bodyDocumentosVarios: BodyDocumentosVarios; 
    esBusqueda: boolean; 
    esCancelar: boolean
}

export interface IDocumentosVariosSustento {
    idDocuVariSust: number,
    idDocuVari: number,
    deTipoDocu: string,
    feDocu: string,
    noDocuSust: string,
    noRutaSust: string,
    idGuidDocu: string,
    coUsuaCrea: number,
    coUsuaModi: number
}

export interface IDocuVariosSustento {
    idDocuVariSust: number | string;
    noDocuSust: string;
    noRutaSust?: string;
    idGuidDocu: string;
}