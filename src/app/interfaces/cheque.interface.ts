import { CuentaBancaria } from '../models/mantenimientos/maestras/cuenta-bancaria.model';
import { ChequeConsulta } from '../models/mantenimientos/documentos/cheques.model';

import { ZonaRegistral, OficinaRegistral, Local, Banco } from './combos.interface';
import { IResponse2 } from './general.interface';

export interface ICheque {
    idCheq: number;
    idCompPago: number;
    nuCompPago: number;
    nuCheq: number;
    feCheq: string;
    imCheq: number;
    tiDocuIden: string;
    nuBene: string;
    noBene: string;
    obMotiAnul: string;
    nuRefeCheq: number;
    esDocu: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;
    feAnul: string;
    noUsuaAnul: string;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    noBanc: string;
    deEsta: string;
    nuCuenBanc: string;
}

export interface IDataFiltrosCheques {
    listaZonaRegistral: ZonaRegistral[];
    listaOficinaRegistral: OficinaRegistral[];
    listaLocal: Local[];
    listaEstadoCheque: IResponse2[];
    listaBancos: Banco[];
    listaCuentas: CuentaBancaria[];
    bodyCheques: ChequeConsulta;
    esBusqueda: boolean;
    esCancelar: boolean;
}

export interface IChequeValue {
    lstCheques: ICheque[];
    reporteExcel: string;
}