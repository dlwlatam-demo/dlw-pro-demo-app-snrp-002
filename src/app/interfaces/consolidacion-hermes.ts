import { FiltroRecaudacionHermes } from "../models/procesos/bancaria/recaudacion-hermes.model";
import { Local, OficinaRegistral, ZonaRegistral } from "./combos.interface";
import { IResponse2 } from "./general.interface";

export interface IFileNiubiz { 
    fila: string;
    a: string;
    b: string;
    c: string;
    d: string;
    e: string;
    f: string;
    g: string;
    h: string;
    i: string;
    j: string;
    k: string;
    l: string;
    m: string;
    n: string;
    o: string;
    p: string;
    q: string;
    r: string;
    s: string;
    t: string;
    u: string;
    v: string;
    w: string;
    x: string;
}

export interface IDataFileExcel {
    servicioDePago: string;
    nameFile: string;
    idGuid?: string;
    lista: any[];
}

export interface IRecaudacionHermes {
    idCnclSiaf: number;
    idTipoCncl: number;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    idOperPos: number;
    nuCncl: string;
    feCnclDesd: string;
    feCnclHast: string;
    imCncl: number;
    noAdju0001: string;
    noAdju0002: string;
    noAdju0003: string;
    idGuidDocu01: string;
    idGuidDocu02: string;
    idGuidDocu03: string;
    esDocu: string;
    noUsuaCrea: string;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: null;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    noOperPos: string;
    deEsta: string;
}

export interface IConciliacionManual {    
	idCncl: number;
    nuSecu: number;
    feCncl: string;
    idUsua: string;
    razSoc: string;
    primerApellido: string;
    segundoApellido: string;
    nombres: string;	
    monto: number;
    idPagoLinea: number;
    eTicket: string;
    inCncl: string;
    obCnclHermes: string;

}

export interface IConciliacionManual2 {
    idCncl: number;
    nuSecu: number;
    coZonaRegi: string;
    coOficRegi: string;
    idUsua: string;
    razSoc: string;
    primerApellido: string;
    segundoApellido: string;
    nombres: string;
    monto: number;
    idPagoLinea: number;
    eticket: string;
    tsModi: string;
    tipoAbono: string;
    tipoUsr: string;
    inCncl: string;
	nuFila: number;
    inRegi: string;
    coUsuaCrea: number;
    feCrea: string;
    coUsuaModi: number;
    feModi: string;
    feCncl: string;
    obCnclHermes: string;
	
}

export interface IResConciliacionManual2 {
    operacionNiubiz: IConciliacionManual2;
    codResult: number;
    msgResult: string;
}

export interface ITotalesNiubizHermes {
    idCnclMovi: number;
    tiMovi: string;
    deMovi: string;
    coZonaRegi: string;
    coOficRegi: string;
    coLocaAten: string;
    obMovi: string;
    imReca: number;
    imComi: number;
    imIgv: number;
    imAlquEqui: number;    
    imDeudAnte: number;
    imDeudSigu: number,
    imTotal: number;
    inRegi: string;
    deZonaRegi: string;
    deOficRegi: string;
    deLocaAten: string;
    deEsta: string
}

export interface IDataTransacHermes {
    lstConciliacionSPRL: any[];
    reporteExcel: any;
}
export interface ITotalPorOficina {
    noOperPos: string,
    feCncl: string,
    deLocaAten: string,
    imSumaDepo: number
}

export interface IDataFiltrosRecaudacionHermes {       
    listaZonaRegistral: ZonaRegistral[];  
    listaOficinaRegistral: OficinaRegistral[];
    listaLocal: Local[];
    listaEstado: IResponse2[];   
    bodyRecaudacionHermes: FiltroRecaudacionHermes; 
    esBusqueda: boolean; 
    esCancelar: boolean;
};

export interface IOperacionScunac {
    item: number,
    tipoPago: string,
    operador: string,
    monto: 42,
    id: string,
    ap: string,
    terminal: string,
    anioMovimiento: string,
    numeroMovimiento: number,
    caja: number,
    deLocaAten: string,
    fecha: string,
    montoTotal: number,
    coZonaRegi: string,
    nuSecuDeta: number
}

export interface IDetalleOperacionScunac {
    aaMvto: string,
    coZonaRegi: string,
    nuMvto: number,
    nuSecuDeta: number,
    idTransPos: string
}

export interface IPostOperacionScunac {
    trama: IDetalleOperacionScunac[]
}


export class ITotales {
    coSecc: string;
    deSecc: string;
    nuRegi: number;
    imCarg: number;
    imAbon: number;
    imTota: number;
}
