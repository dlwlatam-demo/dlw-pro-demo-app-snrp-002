// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  no_sistema:'Sistema de Administración de Recursos Financieros',
  //apiUrl: 'http://localhost:3000',
  apiUrlDigital: 'https://sarf-digital-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe', 
  apiUrlManager: 'https://sarf-content-manager-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe',
  apiUrlMtoSeguridad: 'https://sarf-mto-seguridad-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe',
  // apiUrlMtoSeguridad: 'https://sarf-mto-seguridad-v2-sunarp-fabrica.apps.paas.sunarp.gob.pe',
  apiUrlSeguridad: 'https://sarf-seguridad-sunarp-fabrica.apps.paas.sunarp.gob.pe',
  apiUrlMantenimiento: 'https://sarf-mantenimientos-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe',
  // apiUrlMantenimiento: 'https://sarf-mantenimientos-v2-sunarp-fabrica.apps.paas.sunarp.gob.pe',
  apiUrlParametros: 'https://sarf-parametros-sunarp-fabrica.apps.paas.sunarp.gob.pe',
  apiUrlProcesos: 'https://sarf-procesos-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe',
  // apiUrlProcesos: 'https://sarf-procesos-v2-sunarp-fabrica.apps.paas.sunarp.gob.pe',
  apiUrlReportes: 'https://sarf-reportes-ms-sunarp-fabrica.apps.paas.sunarp.gob.pe',
  // apiUrlReportes: 'https://sarf-reportes-v2-sunarp-fabrica.apps.paas.sunarp.gob.pe',
  currentPage: 'Mostrando del {first} al {last} de un total de {totalRecords} registros',
  invalidFileSizeSummary: '{0}: Tamaño de archivo no válido',
  invalidFileSizeDetail: 'El tamaño máximo de carga es {0}',
  invalidFileTypeSummary: '{0}: Tipo de archivo no válido',
  invalidFileTypeDetail: 'Tipos de archivo permitidos: {0}',
  invalidFileLimitSummary: 'Número máximo de archivos superados',
  invalidFileLimitDetail: 'El límite es {0} como máximo',
  idioma: './assets/i18n/',
  cantidadCuentas: 5,
  cantidadUsuarios: 10,
  fileLimit: 50,
  fileLimitAdju: 1,
  registerLimit: 40,
  codeUsuario: 855,
  sistema: 20,
  maxFileSize: 209715200,
  apiTimeOut: 300000
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
